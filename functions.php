<?php

require_once(__DIR__.'/_classes/Loader.class.php'); // Sets up scripts/css/pages
global $ALR;
if (!$ALR)
	$ALR = new AH\Loader();

global $queuedScripts;
$queuedScripts = [];
global $queuedCss;
$queuedCss = [];
global $queuedVariables;
$queuedVariables = [];

global $masterEnqueueCount;
$masterEnqueueCount = 1;
global $masterVarCount;
$masterVarCount = 1;

// flush_rules() if our rules are not yet included
function alr_flush_rules(){
	$rules = get_option('rewrite_rules');
	if (
		!isset($rules['/([^/]*)$']) ||
		!isset($rules['/([^/]*)/([^/]*)$']) ||
		!isset($rules['(home)/([^/]*)$']) ||
		!isset($rules['(home)/([^/]*)/([^/]*)$']) ||
		!isset($rules['(sellers-management)/([^/]*)$']) || 
		!isset($rules['(special-register)/([^/]*)$']) || 
		!isset($rules['(register)/([^/]*)$']) || 
		!isset($rules['(listings-viewer)/([^/]*)$']) || 
		!isset($rules['(listing)/([^/]*)$']) ||
		!isset($rules['(listing)/([^/]*)/([^/]*)$']) ||
		!isset($rules['(listing)/([^/]*)/([^/]*)/([^/]*)$']) ||
		!isset($rules['(listing-new)/([^/]*)$']) ||
		!isset($rules['(listing-new)/([^/]*)/([^/]*)$']) ||
		!isset($rules['(listing-new)/([^/]*)/([^/]*)/([^/]*)$']) ||
		!isset($rules['(explore-the-area)/([^/]*)$']) || 
		!isset($rules['(explore-the-area-new)/([^/]*)$']) || 
		!isset($rules['(quiz)/([^/]*)$']) ||
		!isset($rules['(quiz-results)/([^/]*)$']) ||
		!isset($rules['(quiz-results)/([^/]*)/([^/]*)$']) ||
		!isset($rules['(quiz-results-new)/([^/]*)/$']) ||
		!isset($rules['(quiz-results-new)/([^/]*)/([^/]*)$']) ||
		!isset($rules['(agent)/([^/]*)$']) ||
		// !isset($rules['(home)/([^/]*)$']) ||
		!isset($rules['(product)/([^/]*)$']) ||
		!isset($rules['(portal-landing)/([^/]*)$']) ||
		!isset($rules['(portal-landing)/([^/]*)/([^/]*)$']) ||
		!isset($rules['(portal-landing)/([^/]*)/([^/]*)/([^/]*)$']) || // can take redirect/extra/extra2
		!isset($rules['(agent-reservation)/([^/]*)$']) ||
		!isset($rules['(agent-reserve-portal)/([^/]*)$']) ||
		!isset($rules['(agent-get-portal)/([^/]*)$']) ||
		!isset($rules['(agent-get-portal)/([^/]*)/([^/]*)$']) ||
		!isset($rules['(agent-get-lifestyle)/([^/]*)$']) ||
		!isset($rules['(agent-get-lifestyle)/([^/]*)/([^/]*)$']) ||
		!isset($rules['(agent-benefits)/([^/]*)$'])  ||
		!isset($rules['(agent-basic-lifestyle-info)/([^/]*)$'])  ||
		!isset($rules['(seller-create)/([^/]*)$'])  ||
		!isset($rules['(sellers)/([^/]*)$']) 
	)
		{global $wp_rewrite; $wp_rewrite->flush_rules();}
}
// Adding a new rule, NOTE: update alr_insert_query_vars() below for new input types!!!!
function alr_insert_rewrite_rules( $rules ) {
	$newrules = array();
	$newrules['/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['/([^/]*)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]&extra=$matches[3]';
	$newrules['(home)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(home)/([^/]*)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]&extra=$matches[3]';
	$newrules['(sellers-management)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(special-register)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(register)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(listings-viewer)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(listing)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(listing)/([^/]*)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]&extra=$matches[3]';
	$newrules['(listing)/([^/]*)/([^/]*)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]&extra=$matches[3]&extra2=$matches[4]';
	$newrules['(listing-new)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(listing-new)/([^/]*)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]&extra=$matches[3]';
	$newrules['(listing-new)/([^/]*)/([^/]*)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]&extra=$matches[3]&extra2=$matches[4]';
	$newrules['(explore-the-area)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(explore-the-area-new)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(quiz)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(quiz-results)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(quiz-results)/([^/]*)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]&extra=$matches[3]';
	$newrules['(quiz-results-new)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(quiz-results-new)/([^/]*)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]&extra=$matches[3]';
	$newrules['(agent)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	// $newrules['(home)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(product)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(portal-landing)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(portal-landing)/([^/]*)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]&extra=$matches[3]';
	$newrules['(portal-landing)/([^/]*)/([^/]*)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]&extra=$matches[3]&extra2=$matches[4]';
	$newrules['(agent-reservation)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(agent-reserve-portal)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(agent-get-portal)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(agent-get-portal)/([^/]*)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]&extra=$matches[3]';
	$newrules['(agent-get-lifestyle)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(agent-get-lifestyle)/([^/]*)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]&extra=$matches[3]';
	$newrules['(agent-benefits)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(agent-basic-lifestyle-info)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(seller-create)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	$newrules['(sellers)/([^/]*)$'] = 'index.php?pagename=$matches[1]&id=$matches[2]';
	return $newrules+$rules;
}
// Adding the id var so that WP recognizes it
function alr_insert_query_vars($vars){array_push($vars, 'id', 'extra', 'extra2');return $vars;}

function restrict_admin()
{
	if ( ! current_user_can( 'manage_options' ) && strpos($_SERVER['PHP_SELF'], '/wp-admin/admin-ajax.php') === false ) {
                wp_redirect( site_url() );
	}
}

function my_login_logo() { ?>
    <style type="text/css">
    	body.login {
    		background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/_img/page-register/background.jpg);
    		background-size: 100% 100%; overflow: hidden;
    	}
    	body.login div#login h1 {
    		position: relative;
        	left: -45px;
        }
        body.login div#login h1 a{
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/_img/allure_logo_clear_med.png);
            background-size: 100% 100%; overflow: hidden;
            padding-bottom: 30px;
            width: 400px;
        }
     </style>
<?php }

function wpbeTags($to_replace) {
	$to_replace['lead_name'] = get_option( 'leadname' );
	$to_replace['lead_email'] = get_option( 'leademail' );
	$to_replace['lead_phone'] = get_option( 'leadphone' );
	$to_replace['lead_message'] = get_option( 'leadmessage' );
	$to_replace['email_banner_img'] = get_option( 'email_banner_img' );
	// must have something
    if (empty($to_replace['email_banner_img']))
        $to_replace['email_banner_img'] = 'http://s7.postimg.org/g3lf5v9jv/email_banner_2016.jpg';

    return $to_replace;
}

add_action( 'login_enqueue_scripts', 'my_login_logo' );
add_action( 'admin_init', 'restrict_admin', 1 );
add_filter('rewrite_rules_array', 'alr_insert_rewrite_rules');
add_filter('query_vars', 'alr_insert_query_vars');
add_action('wp_loaded', 'alr_flush_rules');
add_action( 'password_reset', 'password_reset', 10, 2 );
add_filter('jetpack_enable_open_graph', '__return_false', 99);
add_filter( 'wpbe_tags', 'wpbeTags', 10, 1 );

// woocommerce
// function processPayment($result, $order_id) {
// 	require_once(__DIR__.'/_classes/Sellers.class.php'); $Sellers = new AH\Sellers();
// 	$user = wp_get_current_user();
// 	$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;

// 	$subscriptions = WC_Subscriptions_Manager::get_users_subscriptions();

// 	return $result;
// }

// woocommerce
function cartItemRemoved($cart_item_key, $cart) {
	require_once(__DIR__.'/_classes/Sellers.class.php'); $Sellers = new AH\Sellers();
	require_once(__DIR__.'/_classes/Logger.class.php'); $Logger = new AH\Logger(1, 'sales');
	$user = wp_get_current_user();
	$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
	$seller = $Sellers->get((object)['where'=>['author_id'=>$userId]]);
	if (empty($seller)) {
		$Logger->log("cartItemRemoved - failed to find seller with author_id:$userId");
		return;
	}

	$seller = array_pop($seller);
	if (empty($seller->meta)) {
		$Logger->log("cartItemRemoved - failed to find meta data for seller with author_id:$userId");
		return;
	}

	$amOrder = null;
	$metas = [];
	foreach($seller->meta as $meta) {
		if ($meta->action == SELLER_AGENT_ORDER) {
			$amOrder = $meta;
		}
		else
			$metas[] = $meta;
	}
	if (empty($amOrder)) {
		$Logger->log("cartItemRemoved - failed to find AM order data for seller with author_id:$userId");
		return;
	}

	if (empty($amOrder->item))  {
		$Logger->log("cartItemRemoved - failed to find items for AM order data for seller with author_id:$userId");
		return;
	}

	$itemList = [];
	$gotOne = false;
	foreach($amOrder->item as $i=>&$item) {
		$item = (object)$item;
		if ($item->cart_item_key == $cart_item_key) {
			$gotOne = true;
			$item->mode = ORDER_BUYING;
		}
		$itemList[] = $item;
	}

	if ($gotOne) {
		$amOrder->item = $itemList;
		$metas[] = $amOrder;
		try {
			$x = $Sellers->set([(object)['where'=>['author_id'=>$userId],
									 'fields'=>['meta'=>$metas]]]);
			$Logger->log("cartItemRemoved - removed item:$cart_item_key, have ".count($itemList)." items in order, update was ".(!empty($x) ? "successful": "a failure"));
		}
		catch( \Exception $e) {
			$Logger->log("cartItemRemoved - caught exception while trying to update seller: $userId");
		}
	}
	else
		$Logger->log("cartItemRemoved - did not remove item:$cart_item_key, have ".count($itemList)." items in order");
}

function cartItemRestored($cart_item_key, $cart) {
	require_once(__DIR__.'/_classes/Sellers.class.php'); $Sellers = new AH\Sellers();
	require_once(__DIR__.'/_classes/Logger.class.php'); $Logger = new AH\Logger(1, 'sales');
	$user = wp_get_current_user();
	$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
	$seller = $Sellers->get((object)['where'=>['author_id'=>$userId]]);
	if (empty($seller)) {
		$Logger->log("cartItemRestored - failed to find seller with author_id:$userId");
		return;
	}

	$seller = array_pop($seller);
	if (empty($seller->meta)) {
		$Logger->log("cartItemRestored - failed to find meta data for seller with author_id:$userId");
		return;
	}

	$amOrder = null;
	$metas = [];
	foreach($seller->meta as $meta) {
		if ($meta->action == SELLER_AGENT_ORDER) {
			$amOrder = $meta;
		}
		else
			$metas[] = $meta;
	}
	if (empty($amOrder)) {
		$Logger->log("cartItemRestored - failed to find AM order data for seller with author_id:$userId");
		return;
	}

	if (empty($amOrder->item))  {
		$Logger->log("cartItemRestored - failed to find items for AM order data for seller with author_id:$userId");
		return;
	}

	$itemList = [];
	$gotOne = false;
	foreach($amOrder->item as $i=>&$item) {
		$item = (object)$item;
		if ($item->cart_item_key == $cart_item_key) {
			$gotOne = true;
			$item->mode = ORDER_BUYING;
		}
		$itemList[] = $item;
	}

	if ($gotOne) {
		$amOrder->item = $itemList;
		$metas[] = $amOrder;
		try {
			$x = $Sellers->set([(object)['where'=>['author_id'=>$userId],
									 'fields'=>['meta'=>$metas]]]);
			$Logger->log("cartItemRestored - restored item:$cart_item_key, have ".count($itemList)." items in order, update was ".(!empty($x) ? "successful": "a failure"));
		}
		catch( \Exception $e) {
			$Logger->log("cartItemRemoved - caught exception while trying to update seller: $userId");
		}
	}
	else
		$Logger->log("cartItemRestored - did not restore item:$cart_item_key, have ".count($itemList)." items in order");
}

function newOrder($order_id) {
	require_once(__DIR__.'/_classes/Sellers.class.php'); $Sellers = new AH\Sellers();
	require_once(__DIR__.'/_classes/Logger.class.php'); $Logger = new AH\Logger(1, 'sales');
	$user = wp_get_current_user();
	$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
	$seller = $Sellers->get((object)['where'=>['author_id'=>$userId]]);
	if (empty($seller)) {
		$Logger->log("newOrder - order_id:$order_id, failed to find seller with author_id:$userId");
		return;
	}

	$seller = array_pop($seller);
	$flags = 0;
	$Logger->log("newOrder - order_id:$order_id, for seller-id:{$seller->id}, author_id:$userId, seller flags:$seller->flags");

	$metas = [];
	$needUpdate = false;
	$gotOne = false;
	if (!empty($seller->meta)) foreach($seller->meta as $meta) {
		if ($meta->action == SELLER_NEW_ORDER) {
			$gotOne = true;
			if ($meta->flags != $flags) {
				$meta->flags = $flags;
				$needUpdate = true;
				$metas[] = $meta;
			}
		}
		else
			$metas[] = $meta;
	}
	if (!$gotOne) {
		$meta = new \stdClass();
		$meta->action = SELLER_NEW_ORDER;
		$meta->flags = $flags;
		$metas[] = $meta;
		$needUpdate = true;
	}
	if ($needUpdate) {
		$Logger->log("newOrder - updated meta data for order_id:$order_id, for seller-id:{$seller->id}, author_id:$userId, order-flags:$flags, seller flags:$seller->flags");
		try {
			$Sellers->set([(object)['where'=>['author_id'=>$userId],
									'fields'=>['meta'=>$metas]]]);
		}
		catch( \Exception $e) {
			$Seller->log("newOrder - caught exception while trying to update seller: $userId");
		}
	}
}

function addOrderItem( $item_id, $values ) {
	require_once(__DIR__.'/_classes/Sellers.class.php'); $Sellers = new AH\Sellers();
	require_once(__DIR__.'/_classes/Logger.class.php'); $Logger = new AH\Logger(1, 'sales');
	$user = wp_get_current_user();
	$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
	$seller = $Sellers->get((object)['where'=>['author_id'=>$userId]]);
	if (empty($seller)) {
		$Logger->log("addOrderItem - item_id:$item_id, failed to find seller with author_id:$userId");
		return;
	}

	$seller = array_pop($seller);
	$flags = 0;

	$item = new WC_Product($values['product_id']);
	$sku = $item->get_sku();
	$Logger->log("addOrderItem - item_id:$item_id, for seller-id:{$seller->id}, author_id:$userId, seller flags:$seller->flags, productId:{$values['product_id']}, sku:$sku");

	if (strpos($sku, "0000-") !== false)
		$flags |= SELLER_IS_PREMIUM_LEVEL_1;
	else if (strpos($sku, "0001-") !== false)
		$flags |= SELLER_IS_PREMIUM_LEVEL_2;

	$metas = [];
	$needUpdate = false;
	$gotOne = false;
	if (!empty($seller->meta)) foreach($seller->meta as $meta) {
		if ($meta->action == SELLER_NEW_ORDER) {
			$gotOne = true;
			if ($meta->flags != $flags) {
				$meta->flags |= $flags;
				$needUpdate = true;
				$metas[] = $meta;
			}
		}
		else
			$metas[] = $meta;
	}
	if (!$gotOne) {
		$meta = new \stdClass();
		$meta->action = SELLER_NEW_ORDER;
		$meta->flags = $flags;
		$metas[] = $meta;
		$needUpdate = true;
	}
	if ($needUpdate) {
		$Logger->log("addOrderItem - updated meta data for item_id:$item_id, for seller-id:{$seller->id}, author_id:$userId, order-flags:$flags, seller flags:$seller->flags");
		try {
			$Sellers->set([(object)['where'=>['author_id'=>$userId],
									'fields'=>['meta'=>$metas]]]);
		}
		catch( \Exception $e) {
			$Seller->log("addOrderItem - caught exception while trying to update seller: $userId");
		}
	}
}

define('UPDATE_AM_ORDER', 1 << 0);
define('UPDATE_NICKNAME', 1 << 1);
define('UPDATE_STRIPPED_NEW_ORDER', 1 << 2);

function orderStatus($status, $orderId, $Logger = null) {
	if (!$Logger)
		require_once(__DIR__.'/_classes/Logger.class.php'); $Logger = new AH\Logger(1, 'sales');

	$Logger->log("orderStatus got status:$status for order:$orderId");
}

function paymentComplete($order_id, $Sellers = null, $Logger = null) {
	if (!$Sellers) {
		require_once(__DIR__.'/_classes/Sellers.class.php'); $Sellers = new AH\Sellers();
	}
	if (!$Logger)
		require_once(__DIR__.'/_classes/Logger.class.php'); $Logger = new AH\Logger(1, 'sales');

	require_once(__DIR__.'/_classes/Options.class.php'); $Options = new AH\Options();
	require_once(__DIR__.'/_classes/SellersTags.class.php'); $SellersTags = new AH\SellersTags();
	require_once(__DIR__.'/_classes/Reservations.class.php'); $Reservations = new AH\Reservations();
	require_once(__DIR__.'/_classes/Email.class.php'); $Email = new AH\Email();
	$user = wp_get_current_user();
	$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
	$seller = $Sellers->get((object)['where'=>['author_id'=>$userId]]);
	if ( empty($seller) ) {
		$Logger->log("paymentComplete - order_id:$order_id, failed to find seller with author_id:$userId");
		return;
	}

	$seller = array_pop($seller);
	$Logger->log("paymentComplete - order_id:$order_id, processing for seller with author_id:$userId, id:$seller->id, seller flags:$seller->flags");
	$flags = 0;
	$order = new WC_Order($order_id);
	$items = $order->get_items();
	$needUpdate = 0;
	$metas = [];
	$amData = null;
	$nicknameMeta = null;
	if (!empty($seller->meta)) foreach($seller->meta as $meta) {
		if ($meta->action != SELLER_AGENT_ORDER) {
			if ($meta->action != SELLER_NEW_ORDER) {// get rid of it, since an order is getting processed
				if ($meta->action == SELLER_NICKNAME)
					$nicknameMeta = $meta;
				else
					$metas[] = $meta;
			}
			else
				$needUpdate |= UPDATE_STRIPPED_NEW_ORDER;
		}
		else
			$amData = $meta;
	}

	if ($amData &&
		$amData->order_id_last_purchased != $order_id) {
		$amData->order_id_last_purchased = $order_id;
		$needUpdate |= UPDATE_AM_ORDER;
	}

	$flags = 0;
	foreach($items as $item) {
		$product = new WC_Product($item['product_id']);
		$sku = $product->get_sku();
		if (strpos($sku, "0001-") !== false) {
			$allOk = true;
			$flags |= SELLER_IS_PREMIUM_LEVEL_2;
			if (isset($item['location']) &&
				isset($item['specialty'])) {
				if ( $SellersTags->exists(['author_id'=>$userId,
											'city_id'=>$item['location'],
											'tag_id'=>$item['specialty']])) {
					$tag = $SellersTags->get((object)['where'=>['author_id'=>$userId,
																'city_id'=>$item['location'],
																'tag_id'=>$item['specialty']]]);
					if (!empty($tag) && $tag[0]->type != 1) {
						try {
							$SellersTags->set([(object)['where'=>['author_id'=>$userId,
																  'city_id'=>$item['location'],
																  'tag_id'=>$item['specialty']],
														'fields'=>['type'=>1]]]);
							$Logger->log("paymentComplete - updated tag for author_id:$userId, city_id:{$item['location']}, city:{$item['locationStr']}, specialty:{$item['specialty']},{$item['specialtyStr']}, score:{$tag[0]->score}, type:1");
						}
						catch( \Exception $e) {
							$Logger->log("paymentComplete - caught exception while trying to update sellerTags for: $userId");
						}
					}
					else
						$Logger->log("paymentComplete - tag already exists for author_id:$userId, city_id:{$item['location']}, city:{$item['locationStr']}, specialty:{$item['specialty']},{$item['specialtyStr']}, score:{$tag[0]->score}, type:1");
				}
				else {
					$tagId = intval($item['specialty']);
					$tagList = [];

				
					$sql = "SELECT a.*, c.type FROM {$SellersTags->getTableName('cities-tags')} AS a ";
					$sql.= "INNER JOIN {$SellersTags->getTableName('cities')} AS b ON b.id = a.city_id ";
					$sql.= "INNER JOIN {$SellersTags->getTableName('tags')} as c ON c.id = a.tag_id ";
					$sql.= "WHERE b.id={$item['location']} AND a.tag_id=$tagId";
					$tag = $SellersTags->rawQuery($sql);				
				
					$score = (!empty($tag) && $tag[0]->type == 1 ? $tag[0]->score : 0);
					$toAdd = [	'author_id'=>$userId,
								'city_id'=>$item['location'],
								'tag_id'=>$item['specialty'],
								'score'=>$score,
								'type'=>1];
					$row = $SellersTags->add($toAdd);
					$baseMsg = "author_id:$userId, city_id:{$item['location']}, city:{$item['locationStr']}, specialty:{$item['specialty']},{$item['specialtyStr']}, score:$score, type:1";
					if (!empty($row))
						$Logger->log("paymentComplete - added tag for $baseMsg on row:$row");
					else {
						$Logger->log("paymentComplete - failed to add tag for $baseMsg");
						if ($SellersTags->exists($toAdd))
							$Logger->log("paymentComplete - this tag already exists for $baseMsg");
						else {
							sleep(1); // doze for a sec and retry..
							$row = $SellersTags->add($toAdd);
							if (!empty($row))
								$Logger->log("paymentComplete - added tag on second try for $baseMsg on row:$row");
							else {
								$Email->sendMail(SUPPORT_EMAIL,
												'Technical Failure!',
												"Failed to add tag for order_id:$order_id, $baseMsg",
												"ALERT",
												"Require immediate attention!");
								$allOk = false;
							}
						}
					}
					unset($toAdd);
				}
			}	
			else {
				$allOk = false;
				$Email->sendMail(SUPPORT_EMAIL,
								'Technical Failure!',
								"For order_id:$order_id, it doesn't seem to have the city_id and tag values set.",
								"ALERT",
								"Require immediate attention!");
			}

			if ($amData) {
				foreach($amData->item as &$sellerItem) {
					if ($sellerItem->type == ORDER_AGENT_MATCH &&
						$sellerItem->location == $item['location'] &&
						$sellerItem->specialty == $item['specialty']) {
						$sellerItem->mode = ORDER_BOUGHT;
						$sellerItem->order_id = $order_id;
						$needUpdate |= UPDATE_AM_ORDER;
					}
				}	
			}
			if ($allOk)
				$Email->sendMail(SUPPORT_EMAIL,
								'Received Payment!',
								"For order_id:$order_id, city_id:{$item['location']}, tag_id:{$item['specialty']}, seller_id:$seller->id, $seller->first_name $seller->last_name",
								"LifestyleAgent",
								"Product is paid for.");
		}
		else if (strpos($sku, "0000-") !== false) {
			$flags |= SELLER_IS_PREMIUM_LEVEL_1;
			if (empty($nicknameMeta)) {
				if ($amData) foreach($amData->item as &$sellerItem) {
					if ($sellerItem->type == ORDER_PORTAL) {
						if (isset($sellerItem->nickname)) {
							$nicknameMeta = new \stdClass();
							$nicknameMeta->action = SELLER_NICKNAME;
							$nicknameMeta->nickname = $sellerItem->nickname;
							$needUpdate |= UPDATE_NICKNAME; // flags and inviteCode updated below
						}
						break;
					}
				}
			}

			if (empty($nicknameMeta)) {
				$msg = "paymentComplete - order_id:$order_id, for seller-id:{$seller->id}, author_id:$userId, DOES NOT HAVE ANY NICKNAME META DATA!  Cannot set nickname!";
				$Logger->log($msg);
				// try to send email
				update_option( 'blogurl', get_home_url());
				update_option( 'blogname', "WARNING!");
				update_option( 'blogdescription', "Order processing failure!");
				update_option( 'admin_email', SUPPORT_EMAIL); 

				$headers = ['From: Lifestyled Listings <'.SUPPORT_EMAIL.'>' . "\r\n"]; //,
							// 'Content-Type: text/html; charset=UTF-8' . "\r\n",
							// 'Content-Transfer-Encoding: 8bit'."\r\n"];
			  	$success = wp_mail( SUPPORT_EMAIL, "Paid Portal not set!", '<p>'.$msg.'</p>', $headers); 

			  	update_option( 'blogname', 'Welcome!');
			  	update_option( 'blogdescription', 'The best luxury lifestyled site!');
			}
			else {
				$status = wp_update_user( array( 'ID' => $user->ID, 'nickname' => $nicknameMeta->nickname ) );
				if ( is_wp_error($status) ) {
					$Logger->log("paymentComplete - Failed @1 to update user:$userId, status: ".$status->get_error_message());
				}
				$status = wp_update_user( array( 'ID' => $user->ID, 'user_nicename' => $nicknameMeta->nickname ) );
				if ( is_wp_error($status) ) {
					$Logger->log("paymentComplete - Failed @2 to update user:$userId, status: ".$status->get_error_message());
				}
				$Logger->log("paymentComplete - user:$userId, completed nickname:{$nicknameMeta->nickname} processing");
				$nicknameMeta->flags = IC_BOUGHT_PORTAL;
				$needUpdate |= UPDATE_NICKNAME;
				$nicknameMeta->inviteCode = "PURCHASED";
				$Email->sendMail(SUPPORT_EMAIL,
								'Received Payment!',
								"For order_id:$order_id, portal:{$nicknameMeta->nickname}, seller_id:$seller->id, $seller->first_name $seller->last_name",
								"Portal",
								"Product is paid for.");
			}
			if ($amData) {
				foreach($amData->item as &$sellerItem) {
					if ($sellerItem->type == ORDER_PORTAL &&
						$sellerItem->mode != ORDER_BOUGHT) {
						$sellerItem->mode = ORDER_BOUGHT;
						if ($nicknameMeta)
							$sellerItem->nickname = $nicknameMeta->nickname;
						$sellerItem->order_id = $order_id;
						$needUpdate |= UPDATE_AM_ORDER;
					}
				}	
			}
		}
		else if (strpos($sku, "0002-") !== false) {
			if ($amData) {
				foreach($amData->item as &$sellerItem) {
					if ($sellerItem->type == ORDER_SIGN_UP) {
						$sellerItem->mode = ORDER_BOUGHT;
						$sellerItem->order_id = $order_id;
						$needUpdate |= UPDATE_AM_ORDER;
					}
				}	
			}
			$Email->sendMail(SUPPORT_EMAIL,
								'Received Payment!',
								"For order_id:$order_id, SIGN_UP fee, seller_id:$seller->id, $seller->first_name $seller->last_name",
								"Sign up fee",
								"Product is paid for.");
		}
	}

	if (!($needUpdate & UPDATE_AM_ORDER) && $amData) // then restore it
		$metas[] = $amData;

	if ($flags) {
		if ($Reservations->exists(['seller_id'=>$seller->id])) {
			$res = $Reservations->get((object)['where'=>['seller_id'=>$seller->id]]);
			$update = $flags & SELLER_IS_PREMIUM_LEVEL_2 ? RESERVATION_CONVERTED_LIFESTYLE : 0;
			$update |= $flags & SELLER_IS_PREMIUM_LEVEL_1 ? RESERVATION_CONVERTED_PORTAL : 0;
			$currentFlags = $res[0]->flags & (RESERVATION_CONVERTED_LIFESTYLE | RESERVATION_CONVERTED_PORTAL);
			if ($currentFlags != $update) 
				try {
					$Reservations->set([(object)['where'=>['seller_id'=>$seller->id],
												 'fields'=>['flags'=>$res[0]->flags | $update]]]);
				}
				catch( \Exception $e) {
					$Logger->log("paymentComplete - caught exception while trying to update reservation for $seller->id");
				}
		}
	}

	$sellerFlags = $seller->flags & (SELLER_IS_PREMIUM_LEVEL_1 | SELLER_IS_PREMIUM_LEVEL_2);
	if ($sellerFlags != $flags ||
		$needUpdate) {
		$q = new \stdClass();
		$q->where = ['author_id'=>$userId];
		$q->fields = [];
		if ($sellerFlags != $flags) {
			$sellerFlags = $seller->flags | $flags;
			$q->fields['flags'] = $sellerFlags;
			$Logger->log("paymentComplete - updating seller-id:{$seller->id} with new flags:$sellerFlags");
		}
		if ($nicknameMeta) // restore it, one way or another
			$metas[] = $nicknameMeta;

		if ($needUpdate) {
			$Logger->log("paymentComplete - updating seller-id:{$seller->id} with new meta");
			if ($needUpdate & UPDATE_AM_ORDER) $metas[] = $amData;
			$q->fields['meta'] = $metas;
		}

		try {
			$x = $Sellers->set([$q]);
		}
		catch( \Exception $e) {
			$Logger->log("paymentComplete - caught exception while trying to update seller: $seller->id");
		}
		$Logger->log("paymentComplete - after updating seller-id:{$seller->id}, user:$userId, needUpdate:$needUpdate it was a ".(empty($x) ? "failure" : "success"));
	}

	if ( ($flags & (SELLER_IS_PREMIUM_LEVEL_2 | SELLER_IS_LIFETIME)) ) {
		require_once(__DIR__.'/_classes/SellersTags.class.php'); $SellersTags = new AH\SellersTags();
		if ( $SellersTags->exists(['author_id'=>$userId,
									'tag_id'=>170,
									'type'=>0]) )
			try {
				$SellersTags->set([(object)['where'=>['author_id'=>$userId,
													 'tag_id'=>170],
											'fields'=>['type'=>1]]]);
			}
			catch( \Exception $e) {
				$Logger->log("paymentComplete - caught exception while trying to update sellerTags for : $userId");
			}
	}
	return $flags;
}

function cancelOrder($order_id, $Sellers = null, $Logger = null) {
	if (!$Sellers) {
		require_once(__DIR__.'/_classes/Sellers.class.php'); $Sellers = new AH\Sellers();
	}
	if (!$Logger)
		require_once(__DIR__.'/_classes/Logger.class.php'); $Logger = new AH\Logger(1, 'sales');

	require_once(__DIR__.'/_classes/Options.class.php'); $Options = new AH\Options();
	require_once(__DIR__.'/_classes/SellersTags.class.php'); $SellersTags = new AH\SellersTags();
	$user = wp_get_current_user();
	$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
	$seller = $Sellers->get((object)['where'=>['author_id'=>$userId]]);
	if (empty($seller)) {
		$Logger->log("cancelOrder - order_id:$order_id, failed to find seller with author_id:$userId");
		return;
	}

	$seller = array_pop($seller);
	$flags = 0;
	$order = new WC_Order($order_id);
	$items = $order->get_items();
	foreach($items as $item) {
		$item = new WC_Product($item['product_id']);
		$sku = $item->get_sku();
		if (strpos($sku, "0000-") !== false)
			$flags |= SELLER_IS_PREMIUM_LEVEL_1;
		else if (strpos($sku, "0001-") !== false)
			$flags |= SELLER_IS_PREMIUM_LEVEL_2;
		unset($item);
	}
	$Logger->log("cancelOrder - order_id:$order_id, for seller-id:{$seller->id}, author_id:$userId, order-flags:$flags, seller flags:$seller->flags");

	$needUpdate = 0;
	$metas = [];
	$amData = null;
	$nicknameMeta = null;
	if (!empty($seller->meta)) foreach($seller->meta as $meta) {
		if ($meta->action == SELLER_NICKNAME)
			$nicknameMeta = $meta;
		elseif ($meta->action == SELLER_AGENT_ORDER)
			$amData = $meta;
		else
			$metas[] = $meta;			
	}

	foreach($items as $item) {
		$product = new WC_Product($item['product_id']);
		$sku = $product->get_sku();
		if (strpos($sku, "0001-") !== false) {
			$flags |= SELLER_IS_PREMIUM_LEVEL_2;
			if (isset($item['location']) &&
				isset($item['specialty'])) {
				if ( $SellersTags->exists(['author_id'=>$userId,
											'city_id'=>$item['location'],
											'tag_id'=>$item['specialty']])) {
					try {
						$x = $SellersTags->set([(object)['where'=>['author_id'=>$userId,
																	'city_id'=>$item['location'],
																	'tag_id'=>$item['specialty']],
														'fields'=>['type'=>0]]]);
						$Logger->log("cancelOrder - ".($x ? "updated" : "did not update")." tag for author_id:$userId, city_id:{$item['location']}, city:".(isset($item['locationStr']) ? $item['locationStr'] : "N/A").", specialty:{$item['specialty']},".(isset($item['specialtyStr']) ? $item['specialtyStr'] : "N/A")." to type 0");
					}
					catch( \Exception $e) {
						$Logger->log("cancelOrder - caught exception while trying to update sellerTags for : $userId");
					}
				}
				else {
					$Logger->log("cancelOrder - did not find tag to update for author_id:$userId, city_id:{$item['location']}, city:".(isset($item['locationStr']) ? $item['locationStr'] : "N/A").", specialty:{$item['specialty']},".(isset($item['specialtyStr']) ? $item['specialtyStr'] : "N/A")." to type 0");
				}
			}	
		}
		else if ( strpos($sku, "0000-") !== false) {  // did we lose a Portal Agent?
			$flags |= SELLER_IS_PREMIUM_LEVEL_1;
			$Logger->log("cancelOrder - deactivating portal for seller-id:{$seller->id}, author_id:$userId");
			$nickname = '';

			if (empty($nicknameMeta)) {
				if ($amData) foreach($amData->item as &$sellerItem) {
					if ($sellerItem->type == ORDER_PORTAL) {
						if (isset($sellerItem->nickname)) {
							$nicknameMeta = new \stdClass();
							$nicknameMeta->action = SELLER_NICKNAME;
							$nicknameMeta->nickname = $sellerItem->nickname;
							$needUpdate |= UPDATE_NICKNAME; // flags and inviteCode updated below
							$Logger->log("cancelOrder - adding missing meta data SELLER_NICKNAME with nickname:$nicknameMeta->nickname from SellerItem");
						}
						break;
					}
				}
			}
			if (empty($nicknameMeta)) { // still empty?
				$user_info = get_userdata($user->ID);
				$nicknameMeta = new \stdClass();
				$nicknameMeta->action = SELLER_NICKNAME;
				$nicknameMeta->nickname = isset($user_info->user_nicename) && strpos($user_info->user_nicename, 'unknown') === false ? $user_info->user_nicename :
										  isset($user_info->nickname) && strpos($user_info->nickname, 'unknown') === false ? $user_info->nickname : strtolower($seller->first_name.$seller->last_name);
				$Logger->log("cancelOrder - adding missing meta data SELLER_NICKNAME with nickname:$nicknameMeta->nickname from userdata");
			}

			if (empty($nicknameMeta))
				$Logger->log("cancelOrder - subscription:{$subLost['product_id']}, for seller-id:{$seller->id}, author_id:$userId, DOES NOT HAVE SELLER_NICKNAME META DATA!");
			else {
				$nickname = $nicknameMeta->nickname;	
				$nicknameMeta->flags = 0;
				$nicknameMeta->inviteCode = 'CANCELLED';
			}
			
			$needUpdate |= UPDATE_NICKNAME;
			// $status = wp_update_user( array( 'ID' => $user->ID, 'nickname' => 'unknown' ) );
			// if ( is_wp_error($status) ) {
			// 	$Sellers->log("cancelOrder - Failed @1 to update user:$userId, status: ".$status->get_error_message());
			// }
			$status = wp_update_user( array( 'ID' => $user->ID, 'user_nicename' => 'unknown' ) );
			update_user_meta($user->ID, 
							'nickname', 
							'unknown');	
			if ( is_wp_error($status) ) {
				$Logger->log("cancelOrder - Failed @2 to update user:$userId, status: ".$status->get_error_message());
			}
			$Logger->log("cancelOrder - user:$userId, removed nickname:$nickname");
		}
	}

	if ($amData) {
		$amData->order_id_last_cancelled = $order_id;
		foreach($amData->item as &$sellerItem) {
			if ($sellerItem->order_id == $order_id) {
				$sellerItem->mode = ORDER_IDLE;
				$sellerItem->cart_item_key = 0;
				$needUpdate |= UPDATE_AM_ORDER;
			}
		}
		if (!($needUpdate & UPDATE_AM_ORDER)) // restore it
			$metas[] = $amData;
	}	

	// what seller has now
	$sellerFlags = $seller->flags & (SELLER_IS_PREMIUM_LEVEL_1 | SELLER_IS_PREMIUM_LEVEL_2);
	// what is left over after we strip off the flags no longer applying to seller
	$oppositeFlags = ~$flags & $sellerFlags;
	if ($sellerFlags != $oppositeFlags ||
		$needUpdate) {
		$q = new \stdClass();
		$q->where = ['author_id'=>$userId];
		$q->fields = [];
		if ($sellerFlags != $oppositeFlags) {
			$Logger->log("cancelOrder - updating seller with new flags:$oppositeFlags");
			$sellerFlags = ($seller->flags & ~$flags);
			$q->fields['flags'] = $sellerFlags;
		}

		if ($nicknameMeta) // restore it, one way or another
			$metas[] = $nicknameMeta;

		if ($needUpdate) {
			$Logger->log("cancelOrder - updating seller with new meta");
			if ($needUpdate & UPDATE_AM_ORDER) $metas[] = $amData;
			$q->fields['meta'] = $metas;
		}

		try {
			$x = $Sellers->set([$q]);
		}
		catch( \Exception $e) {
			$Logger->log("cancelOrder - caught exception while trying to update seller: $userId");
		}
		$Logger->log("cancelOrder - after updating user:$userId needUpdate:$needUpdate, it was a ".(empty($x) ? "failure" : "success"));
	}

	$Logger->log("cancelOrder - exiting for user:$userId with old flags:$sellerFlags, new flags:$oppositeFlags");
	return $sellerFlags;
}

function loginInUser($seller) {
	$password = '';
	foreach($seller->meta as $meta) {
		if ($meta->action == SELLER_KEY) {
			$key = json_decode($meta->key);
			$len = count($key); // strlen($meta->key);
			for($i = 0; $i < $len; $i++)
				$password .= chr( $key[$i] ^ ($i + ord('.')) );
		}
	}

	if (empty($password))
		return false;

	$user_info = get_userdata(intval($seller->author_id));

	wp_logout(); // just in case...
	wp_set_current_user(0);

	$creds = array();
	$creds['user_login'] = $user_info->user_login;
	$creds['user_password'] = $password;
	$creds['remember'] = true;
	$user = wp_signon( $creds, false );
	if ( is_wp_error($user) ) {
		return false;
	}
	wp_set_current_user( $user->ID, $user->user_login );
	wp_set_auth_cookie( $user->ID );
	do_action('wp_login', $user->user_login, $user);

	return true;
}

function cancelledSubscription($user_id, $subscription_key) {
	require_once(__DIR__.'/_classes/Sellers.class.php'); $Sellers = new AH\Sellers();
	require_once(__DIR__.'/_classes/Logger.class.php'); $Logger = new AH\Logger(1, 'sales');
	$user = wp_get_current_user();
	$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
	$seller = $Sellers->get((object)['where'=>['author_id'=>$userId]]);
	$needLogout = false;
	if (empty($seller)) {
		$Logger->log("cancelledSubscription - subscription_key:$subscription_key, failed to find seller with author_id:$userId");
		global $wpdb;
		$sql = "Select * from ".$wpdb->prefix."users WHERE post_title='scheduled_subscription_payment'";
		$subscriptions = $Sellers->rawQuery($sql);
		if (!empty($subscriptions)) {
			$userId = 0;
			foreach($subscriptions as $sale) {
				$data = json_decode($sale->post_content);
				if ($data->subscription_key == $subscription_key) {
					$userId = $data->user_id;
					break;
				}
			}
			if ($userId) {
				$seller = $Sellers->get((object)['where'=>['author_id'=>$userId]]);
				if (!empty($seller)) {
					$Logger->log("cancelledSubscription - subscription_key:$subscription_key, found seller with author_id:$userId in POSTS table");
					$needLogout = loginInUser($seller[0]);
					if (!$needLogout) {
						$Logger->log("cancelledSubscription - subscription_key:$subscription_key, failed to login user:$userId");
						return false;
					}
				}
				else
					return;
			}
			else
				return;
		}
		else
			return;
	}

	$seller = array_pop($seller);
	$subLost = WC_Subscriptions_Manager::get_subscription( $subscription_key );
	$Logger->log("cancelledSubscription - subscription_key:$subscription_key, author_id:$userId, cancelOrder({$subLost['order_id']})");
	$flags = cancelOrder($subLost['order_id'], $Sellers, $Logger); // clean up this order
	$Logger->log("cancelledSubscription - after cancelOrder call - subscription:$subscription_key, order:{$subLost['order_id']}, for seller-id:{$seller->id}, author_id:$userId, flags:$flags");

	// get all subscriptions and update seller flags
	$subscriptions = WC_Subscriptions_Manager::get_users_subscriptions();
	$flags = 0;
	foreach($subscriptions as $sub) {
		if ($sub['status'] == 'active') {
			$Logger->log("cancelledSubscription - making sure subscription:  is active - paymentComplete({$sub['order_id']})");
			$flags |= paymentComplete($sub['order_id'], $Sellers, $Logger);			
		}
	}

	if ( !($flags & (SELLER_IS_PREMIUM_LEVEL_2 | SELLER_IS_LIFETIME)) ) {
		require_once(__DIR__.'/_classes/SellersTags.class.php'); $SellersTags = new AH\SellersTags();
		if ( $SellersTags->exists(['author_id'=>$userId,
									'type'=>1]) )
			try {
				$SellersTags->set([(object)['where'=>['author_id'=>$userId],
											'fields'=>['type'=>0]]]);
			}
			catch( \Exception $e) {
				$Logger->log("cancelledSubscription - caught exception while trying to update sellerTags for : $userId");
			}
	}

	$Logger->log("cancelledSubscription - final, after activating all active ones - subscription:$subscription_key, order:{$subLost['order_id']}, for seller-id:{$seller->id}, author_id:$userId, active flags:$flags, needLogout:$needLogout");
	if ($needLogout) {
		wp_logout(); // just in case...
		wp_set_current_user(0);
	}
}

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);
// add_filter('woocommerce_payment_successful_result', 'processPayment', 10, 2);
add_action('woocommerce_cart_item_removed', 'cartItemRemoved', 10, 2);
add_action('woocommerce_cart_item_restored', 'cartItemRestored', 10, 2);
add_action('woocommerce_new_order', 'newOrder', 10, 1);
add_action('woocommerce_add_order_item_meta', 'addOrderItem', 10, 2);
add_action('woocommerce_payment_complete', 'paymentComplete', 10, 1);
add_action('woocommerce_payment_complete_order_status', 'orderStatus', 10, 2);
add_action('woocommerce_cancelled_order', 'cancelOrder', 10, 1);
add_action('cancelled_subscription', 'cancelledSubscription', 10, 2);
add_action( 'wp_print_styles',  'my_deregister_styles', 100 );

function my_deregister_styles()    { 
   // wp_deregister_style( 'dashicons' ); 
   if ( !is_page('Seller Admin') ) {
		wp_deregister_script( 'woocommerce' );
		wp_deregister_script( 'wc-add-to-cart' );
		wp_deregister_script( 'wc-cart-fragments' );
		wp_deregister_style( 'woocommerce-layout' ); 
		wp_deregister_style( 'woocommerce' ); 
		wp_deregister_style( 'woocommerce-smallscreen' ); 
	}
}

function my_theme_wrapper_start() {
  echo '<section id="main_section">';
  // echo '<p>Begin Woocommerce</p>';
}

function my_theme_wrapper_end() {
	// echo '<p>End Woocommerce</p>';
  echo '</section>';
}

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}


function password_reset( $user, $new_pass ) {
	require_once(__DIR__.'/_classes/Register.class.php'); // Sets up scripts/css/pages
	$r = new AH\Register();
	$r->userResetPassword($user, $new_pass);
}

function enqueueData() {
	global $ALR;
	global $queuedCss;
	global $queuedScripts;
	global $masterEnqueueCount;
	global $queuedVariables;
	global $thisPage;
	global $masterVarCount;

	require_once(__DIR__.'/_classes/Logger.class.php'); $Logger = new AH\Logger(1, 'dequeue');
	$cssText = '';
	$scriptText = '';
	foreach($queuedCss as $css) {
		$localFile = $css->dir && $css->name ? __DIR__."/$css->dir/$css->name" : $css->fullPath;
		if (!$localFile) {
			wp_enqueue_style($css->nickname);
			$Logger->log("CSS name:$css->nickname just enqueue");
			continue;
		}
		$buffer = isset($css->buffer) && strlen($css->buffer) ? $css->buffer : file_get_contents($localFile);
		if (!empty($buffer))
			$cssText .= $buffer.PHP_EOL;
		$Logger->log("CSS name:$css->nickname, localFile:$localFile - buffer:".(empty($buffer) ? "N/A" : strlen($buffer)));
	}
	$queuedCssContent = $cssText;
	$queuedCss = [];

	foreach($queuedScripts as $script) {
		$localFile = $script->dir && $script->name ? __DIR__."/$script->dir/$script->name" : $script->fullPath;
		if (!$localFile) {
			wp_enqueue_script($script->nickname);
			$Logger->log("JS name:$script->nickname just enqueue");
			continue;
		}
		$buffer = isset($script->buffer) && strlen($script->buffer) ? $script->buffer : file_get_contents($localFile);
		if (!empty($buffer))
			$scriptText .= $buffer.PHP_EOL;
		$Logger->log("JS name:$script->nickname, localFile:$localFile - buffer:".(empty($buffer) ? "N/A" : strlen($buffer)));
	}
	$queuedScriptsContent = $scriptText;
	$queuedScripts = [];

	if (!empty($queuedCssContent)) {
		$slug = 'master'.$masterEnqueueCount.(isset($thisPage) && $thisPage ? "-$thisPage" : "-general");
		$tempFile = __DIR__."/_css/cssPack/$slug".'.css';
		$writeFile = !file_exists($tempFile) || filesize($tempFile) != strlen($queuedCssContent);
		$Logger->log("enqueueData - CSS writeFile:".($writeFile ? 'yes' : 'no').", file:$tempFile");
		if ($writeFile) {
			$lockfile = fopen(__DIR__."/_css/cssPack/$slug"."Css.lock", 'c');
			flock($lockfile, LOCK_EX);
			file_put_contents($tempFile, $queuedCssContent);
		}
		wp_register_style($slug, get_template_directory_uri()."/_css/cssPack/$slug.css", null, null);
		wp_enqueue_style($slug);
		$queuedCssContent = '';
		if ($writeFile) {
			flock($lockfile, LOCK_UN);
			fclose($lockfile);
		}
	}

	if (!empty($queuedScriptsContent)) {
		$slug = 'master'.$masterEnqueueCount.(isset($thisPage) && $thisPage ? "-$thisPage" : "-general");
		$tempFile = __DIR__."/_jsPack/$slug".'.js';
		$writeFile = !file_exists($tempFile) || filesize($tempFile) != strlen($queuedScriptsContent);
		$Logger->log("enqueueData - JS writeFile:".($writeFile ? 'yes' : 'no').", file:$tempFile");
		if ($writeFile) {
			$lockfile = fopen(__DIR__."/_jsPack/$slug"."Js.lock", 'c');
			flock($lockfile, LOCK_EX);
			file_put_contents($tempFile, $queuedScriptsContent);
		}
		wp_register_script($slug, get_template_directory_uri()."/_jsPack/$slug".'.js', null, null);
		if (!empty($queuedVariables)) {
			$varCount = 1;
			foreach($queuedVariables as $var) {
				$varName = "varHandle".$masterVarCount++;
				@wp_register_script($varName);
				wp_localize_script($varName, $var->name, $var->var);
				wp_enqueue_script($varName);
				$Logger->log("Variable handle:$varName, name:$var->nickname, var:$var->name");
			}
			$queuedVariables = [];
		}
		wp_enqueue_script($slug);
		$queuedScriptsContent = '';
		if ($writeFile) {
			flock($lockfile, LOCK_UN);
			fclose($lockfile);
		}
	}

	$masterEnqueueCount++;
}

add_action( 'widgets_init', 'my_register_sidebars' );

function my_register_sidebars() {

	/* Register the 'primary' sidebar. */
	register_sidebar(
		array(
			'id' => 'widgets',
			'name' => __( 'Widgets' ),
			'description' => __( 'Sidebar for widgets.' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-container">',
			'after_title' => '</h3>'
		)
	);

	register_sidebar(
		array(
			'id' => 'widgets_mobile',
			'name' => __( 'Widgets_mobile' ),
			'description' => __( 'Sidebar for mobile widgets.' ),
			'before_widget' => '<div id="%1$s" class="widget mobile %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-container">',
			'after_title' => '</h3>'
		)
	);

	/* Repeat register_sidebar() code for additional sidebars. */
}

// require_once (ABSPATH . WPINC . '/widgets.php');
// add_action( ‘_admin_menu’, ‘wp_widgets_add_my_menu’ );

// function wp_widgets_add_my_menu() {
// global $submenu;
// $submenu[‘themes.php’][7] = array( __( ‘Widgets’ ), ‘edit_themes’, ‘widgets.php’ );
// ksort($submenu[‘themes.php’], SORT_NUMERIC);
// }

// stop next post preloading and thus single.php calling twice
// remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');

// $ALR->log("functions.php exiting");

// add_filter('query_vars', 'add_my_var');

// function add_my_var($public_query_vars) {
// 	$public_query_vars[] = 'agent';
// 	return $public_query_vars;
// }
