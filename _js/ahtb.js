/*
*   Thickbox modal window
*/
var ahtb;
var old_remove = null;
var SMALLDESKTOP_WIDTH_SCALE = 1.00;
var SMALLDESKTOP_HEIGHT_SCALE = 1.00;
var LARGE_WIDTH_SCALE = 0.80;
var LARGE_HEIGHT_SCALE = 1.00;
var MEDIUM_WIDTH_SCALE = 0.75;
var MEDIUM_HEIGHT_SCALE = 0.95;
var SMALL_WIDTH_SCALE = 0.70;
var SMALL_HEIGHT_SCALE = 0.90;
var TINY_WIDTH_SCALE = 0.60;
var TINY_HEIGHT_SCALE = 0.90;

var TB_Variations = {
	'main-feedback': {
								smalldesktop: {width: 535,
												height: 450},
								large: {width: 480,
												height: 360},
								medium: {width: 375,
												height: 350},
								small: {width: 320,
												height: 340},
								tiny: {width: 310,
												height: 340}
								},
	'tb-find-a-home': {
								smalldesktop: {width: 535,
												height: 370},
								large: {width: 480,
												height: 360},
								medium: {width: 375,
												height: 350},
								small: {width: 320,
												height: 340},
								tiny: {width: 310,
												height: 325}
								},
	'intro-box': {
								smalldesktop: {width: 600,
												height: 350},
								large: {width: 480,
												height: 350},
								medium: {width: 375,
												height: 350},
								small: {width: 320,
												height: 325},
								tiny: {width: 310,
												height: 325}
								},
	'quiz-intro-box': {
								smalldesktop: {width: 600,
												height: 310},
								large: {width: 480,
												height: 350},
								medium: {width: 375,
												height: 350},
								small: {width: 320,
												height: 325},
								tiny: {width: 310,
												height: 325}
								},
	'registration': {
								smalldesktop: {width: 450,
												height: 625},
								large: {width: 450,
												height: 375},
								medium: {width: 375,
												height: 375},
								small: {width: 320,
												height: 375},
								tiny: {width: 310,
												height: 375}
								},
	'tag-window': {
								smalldesktop: {width: 770,
												height: 480}
								},
  'success-verify-agent': {
                medium: {width: 400,
                        height: 240},
                small: {width: 380,
                        height: 220},
                tiny: {width: 338,
                        height: 240}
                },
};

jQuery(document).ready(function($){
  ahtb = new function(){
    this.x_array = [];
    this.curX = null;
    this.opened = false;
    this.push = function(cb) {
      if (ahtb.curX) {
        // ahtb.close();
        ahtb.x_array.push(ahtb.curX);
        if (typeof cb != 'undefined')
          ahtb.close(cb);
      }
    }
    this.pop = function(cb) {
      if (ahtb.x_array.length) {
        // this.close();
        $('#TB_window').hide();
        $('#thick-click').click();
        window.setTimeout(function() {
          ahtb.open(ahtb.x_array.pop());
          if (typeof cb == 'function')
            cb();
        }, 250);
      }
      else
        this.close(cb);
    }
    this.getResponsiveWidthHeightForOptions = function(options) {
      if (ahtb.curX == null)
        return;

      var ele = $('#tb-submit');
			var div = $('#tb-submit div:nth-child(1)');
			var id = div.length ? div.attr('id') : '';
			
      var width = ahtb.curX.width;
      var height = ahtb.curX.height;

      var tb_window = ele.parent().parent();
      var tb_ajaxContent = ele.parent();

      // var largemobilepopup = $('.largemobilepopup').css('display') != 'none';
      // var mediummobilepopup = $('.mediummobilepopup').css('display') != 'none';
      // var smallmobilepopup = $('.smallmobilepopup').css('display') != 'none';
      // var mainpopup = $('.mainpopup').css('display') != 'none';
      var windowWidth = $(window).width();
      var mainpopup = windowWidth >= 1101;
			var smalldesktoppopup = windowWidth >= 668 && windowWidth < 1101;
      var largemobilepopup = windowWidth >= 501 && windowWidth < 668;
      var mediummobilepopup = windowWidth >= 376 && windowWidth < 501;
      var smallmobilepopup = windowWidth >= 321 && windowWidth < 376;
			var tinymobilepopup = $(window).width() < 321;

      var newWidth = width;
      var newHeight = height;
			var scale = 'normal';
			
			if (smalldesktoppopup) {
				newWidth = width*SMALLDESKTOP_WIDTH_SCALE;
        newHeight = height*SMALLDESKTOP_HEIGHT_SCALE;
				scale = 'smalldesktop';
      }
      else if (largemobilepopup) {
        newWidth = width*LARGE_WIDTH_SCALE;
        newHeight = height*LARGE_HEIGHT_SCALE;
				scale = 'large';
      }
      else if (mediummobilepopup) {
        newWidth = width*MEDIUM_WIDTH_SCALE;
        newHeight = height*MEDIUM_HEIGHT_SCALE < 315 && height > 315 ? 315 : (height > 315 ? height*MEDIUM_HEIGHT_SCALE : height);
				scale = 'medium';
      }
      else if (smallmobilepopup) {
        newWidth = width*SMALL_WIDTH_SCALE;
        newHeight = height*SMALL_HEIGHT_SCALE < 315 && height > 315 ? 315 : (height > 315 ? height*SMALL_HEIGHT_SCALE : height);
				scale = 'small';
      }
			else if (tinymobilepopup) {
        newWidth = 310; //width*SMALL_WIDTH_SCALE;
        // newHeight = height >= 315 && height*SMALL_HEIGHT_SCALE < 315 ? 315 : height*SMALL_HEIGHT_SCALE;
        newHeight = height < 315 ? height : 315;
				scale = 'tiny';
      }
			
			if (scale != 'normal' &&
          id &&
					id.length &&
					typeof TB_Variations[id] != 'undefined' &&
          typeof TB_Variations[id][scale] != 'undefined') {
				newWidth = TB_Variations[id][scale].width;
				newHeight = TB_Variations[id][scale].height;
			}
					

      var offsetWidth = (newWidth/2) * -1;
      var offsetHeight = (newHeight/2) * -1;

      options.cont.width = newWidth+'px';
      options.cont.height = newHeight+'px';
      options.wind.width = newWidth+'px';
      options.wind.height = newHeight+'px';
      options.wind.marginLeft = '-'+(newWidth/2)+'px';
      options.wind.marginTop = '-'+(newHeight/2)+'px'; 
    }
    this.responsiveAhtb = function() {
      if (ahtb.curX == null)
        return;

      var options = { cont: {}, wind: {} };
      this.getResponsiveWidthHeightForOptions( options );

      var ele = $('div#TB_ajaxContent #tb-submit');
      var tb_window = ele.parent().parent();
      var tb_ajaxContent = ele.parent();

      // ele.css('width', options.wind.width);
      // ele.css('height', options.wind.height);
      var divs = $('#tb-submit').children();
      if (divs.length == 1) {
        ele.css('height', options.wind.height);
        divs.css('height', '100%');
      }
      tb_window.css('width', options.wind.width);
      tb_window.css('height', options.wind.height);
      tb_window.css('margin-left', options.wind.marginLeft);
      tb_window.css('margin-top', options.wind.marginTop);
      tb_ajaxContent.css('width', options.wind.width);
      tb_ajaxContent.css('height', options.wind.height);
      tb_ajaxContent.css('margin-left', 0);
      tb_ajaxContent.css('margin-top', 0);
      // tb_ajaxContent.children().css({'width': '0px', 'height': '0px'});

      console.log("responsiveAhtb width: "+options.wind.width+", height:"+options.wind.height);
    }
    this.clear = function() {
      ahtb.x_array = [];
    }
    this.discard = function() {
      if (ahtb.x_array.length)
        ahtb.x_array.pop();
      else
        ahtb.close();
    }
  	this.open = function(x){
      if (this.opened) {
        this.edit(x);
      } else {
        var funcsToDo = [];
        if (x == null) x = {};

        var size = {
          width: window.innerWidth || document.body.clientWidth,
          height: window.innerHeight || document.body.clientHeight
        }
        var pos = -1;
        if ( x.width != null &&
            typeof x.width != 'number' &&
            (pos = (x.width.indexOf('%'))) != -1) {
          x.width = (size.width * parseInt(x.width.substring(0, pos)))/100;
        }
        if ( x.height != null &&
            typeof x.height != 'number' &&
            (pos = (x.height.indexOf('%'))) != -1) {
          x.height = (size.height * parseInt(x.height.substring(0, pos)))/100;
        }
        x.hideSubmit = x.hideSubmit == null ? false : x.hideSubmit;
        x.width = x.width == null ? 400 : (typeof x.width == 'number') ? x.width : parseInt(x.width);
        x.height = x.height == null ? (x.hideSubmit ? 75 : 110) : (typeof x.height == 'number') ? x.height : parseInt(x.height);

        if (x.maxWidth != null) {
          var maxWidth = typeof x.maxWidth != 'number' ? parseInt(x.maxWidth) : x.maxWidth;
          maxWidth = maxWidth == 0 ? 450 : maxWidth;
          if (x.width > maxWidth)
            x.width = maxWidth;
        } 

        if (x.maxHeight != null) {
          var maxHeight = typeof x.maxHeight != 'number' ? parseInt(x.maxHeight) : x.maxHeight;
          maxHeight = maxHeight == 0 ? 150 : maxHeight;
          if (x.height > maxHeight)
            x.height = maxHeight;
        }

        if (x.minHeight != null) {
          var minHeight = typeof x.minHeight != 'number' ? parseInt(x.minHeight) : x.minHeight;
          maxHeight = minHeight == 0 ? 150 : minHeight;
          if (x.height < maxHeight)
            x.height = maxHeight;
        }

        x.open = x.open == null ? function(){} : x.open;
        x.opened = x.opened == null ? function(){ $('#TB_window #TB_ajaxContent form#tb-submit').focus(); } : x.opened;
        x.close = x.close == null ? function(){} : x.close;
        x.closed = x.closed == null ? function(){} : x.closed;

        x.submit = x.submit == null ? (x.buttons == null ? function(){ahtb.close()} : function(){}) : x.submit;
        x.buttons = x.buttons == null ? 
          x.hideSubmit ? [] : [{text: (x.submitText ? x.submitText : 'OK'), action:function(){x.submit()}}] : 
          x.buttons;
        var but = x.buttons;
        x.buttons = '<div class="tb-buttons'+(x.buttonsClass ? ' '+x.buttonsClass : '')+'">';
        for (var i in but){
          if (i > 0)
            x.buttons += '<span>    </span>';
          x.buttons += '<button class="'+but[i].text.replace(/ /g,'-').toLowerCase()+'">'+but[i].text+'</button>';
          if (but[i].action) funcsToDo.push({ value: but[i], func: function(xxx){ $('#tb-submit button.'+xxx.text.replace(/ /g,'-').toLowerCase()).on('click',function(){xxx.action()}) } });
        }
        x.buttons += '</div>';

        $('body').append(
          '<div id="thickbox-window" style="display:none;"><form id="tb-submit">'+(x.html ? x.html : '')+x.buttons+'</form></div>'+
          '<a href="#TB_inline?width='+x.width+'&height='+x.height+'&inlineId=thickbox-window" id="thick-click" class="thickbox" style="display:none;"></a>'
        );

        x.buttons = but; // restore array of buttons

        if (old_remove == null &&
            typeof tb_remove != 'undefined')
          old_remove = tb_remove ;
        $('#thick-click').on('click',function(){ tb_remove = function(){
          $('#TB_window, #TB_overlay').finish();
          x = ahtb.curX ? ahtb.curX : x;
          x.close(); 
          if (old_remove) {
            var old = old_remove;
            old_remove = null;

            old();
            tb_remove = old;
          }
          
          $('#thickbox-window').remove(); $('#thick-click').remove();
          x.closed();
          ahtb.opened = false;
          return false;
        }}).click();

        $(document).off('keydown').on('keyup',function(e){
          if (e.which==13) {e.preventDefault(); $("#tb-submit").submit();}
          if (e.which==27 && x.hideClose == null) {e.preventDefault(); ahtb.close();}
        });

        if (x.hideTitle) $('#TB_title').hide();

        $('#TB_ajaxWindowTitle').html(x.title ? x.title : 'LifeStyled');
        $('#tb-submit').submit(function(e){ e.preventDefault(); x.submit(); });
        x.open();
        if (x.hideClose != null) this.hideClose();
        x.closeOnClickBG || typeof x.closeOnClickBG == 'undefined' ? this.closeOnClickBG(true) : this.closeOnClickBG(false);
        $('#TB_window, #TB_overlay').finish().hide().fadeIn({ duration: 500 });

        ahtb.curX = x;
        this.resize(x.width, x.height);
        x.opened();
        this.opened = true;

        if (funcsToDo.length > 0) for (var i in funcsToDo) funcsToDo[i].func(funcsToDo[i].value);
        
      }
    }
    this.close = function(theCallback){
      if (ahtb.opened){
        ahtb.curX = null;
        ahtb.opened = false;
        $('#TB_closeWindowButton').click(); 
        setTimeout(function(){
          $('#thickbox-window').finish().remove();
          $('#thick-click').finish().remove();
          if (theCallback != null) 
            setTimeout(function() {
              theCallback();
            }, 250);
        }, 150);
      }
      else if (theCallback != null) 
        setTimeout(function() {
          theCallback();
        }, 250);
    }
    this.alert = function(mess, options){ 
      this.open({
        html: '<p>'+(mess?mess:'Alert!')+'</p>',
        title: options && options.title ? options.title : null, 
        height: options && options.height ? options.height : 150, 
        width: options && options.width ? options.width : 350, 
        buttons: options && options.buttons ? options.buttons : [{text:'OK',action:function(){ahtb.close()}}],
        closeOnClickBG: true,
        opened: function(){ if ($('#tb-submit').height() == 5) ahtb.resize($('#tb-submit').width(), $('#tb-submit').height()+45) }
      }); 
    }
    this.confirm = function(x){
    	this.close();
    	if (x){
    		if (!x.html) x.html = '<p>Yes or No?</p>';
    		if (!x.title) x.title = 'Confirm';
    		if (!x.width) x.width = 450;
    		if (!x.onTrue) x.onTrue = function(){}; if (!x.trueText) x.trueText = 'True';
    		if (!x.onFalse) x.onFalse = function(){}; if (!x.falseText) x.falseText = 'False';
    		x.hideSubmit = 1;
    		x.opened = function(){
  				$('#tb-submit button').on('click',function(){
  					if ($(this).hasClass('true')) x.onTrue();
  					else if ($(this).hasClass('false')) x.onFalse();
  				});
  			}
    		x.html += '<button class="true">'+x.trueText+'</button> <button class="false">'+x.falseText+'</button>'
    		this.open(x);
    	}
    }
    this.loading = function(message, options){ // quick loading box
      var x = {};
      if (options) x = options;
      x.html = '<p>'+(message ? message : 'Loading...')+'</p>';
      x.title = options && options.title ? options.title : 'LifeStyled';
      x.closeOnClickBG = options && options.closeOnClickBG ? options.closeOnClickBG : false;
      x.buttons = options && options.buttons ? options.buttons : [];
      x.hideClose = options && options.hideClose ? options.hideClose : true;
      x.height = options && options.height ? options.height : 100;
      x.width = options && options.width ? options.width : 350;
      this.open(x);
    }
    this.hideClose = function(fade){
      $(document).on('keydown', function(e){ if (e.keyCode == 27) { e.preventDefault();} });
      fade ? $('#TB_closeAjaxWindow').fadeOut({duration:parseInt(fade), queue:false}) : $('#TB_closeAjaxWindow').hide()
    }
    this.showClose = function(fade){fade ? $('#TB_closeAjaxWindow').fadeIn({duration:parseInt(fade), queue:false}) : $('#TB_closeAjaxWindow').show()}
    this.resize = function(width, height, run_anim){
      if (typeof run_anim == 'undefined') run_anim = true;
      var options = { cont: {}, wind: {} };
      this.getResponsiveWidthHeightForOptions( options );
      // if (width){
      //   options.cont.width = width+'px';
      //   options.wind.marginLeft = '-'+parseInt((width/2), 10)+'px';
      //   options.wind.width = width+'px';
      // }
      // if (height){
      //   options.cont.height = height+'px';
      //   options.wind.height = height+'px';
      //   options.wind.marginTop = '-'+parseInt((height/2), 10)+'px'; 
      // }
      if (run_anim)
        $("#TB_ajaxContent").animate(options.cont,{duration: 250, queue: true,
          start: function(){
            ahtb.responsiveAhtb();
            // $("#TB_window").animate(options.wind,{duration: 250, queue: false, start:function(){ $("#TB_window").prop("width", width); $("#TB_window").prop("height", height); }, complete: function() { ahtb.responsiveAhtb(); } });
          }
        });
      else {
        ahtb.responsiveAhtb();
        // $("#TB_ajaxContent").css(options.cont);
        // $("#TB_window").css(options.wind);
        // $("#TB_window").prop("width", width);
        // $("#TB_window").prop("height", height);
      }
    }
    this.edit = function(x){
      $('#TB_window').fadeOut(125,function(){
        var funcsToDo = [];

        x.open = x.open == null ? function(){} : x.open;
        x.opened = x.opened == null ? function(){} : x.opened;
        x.close = x.close == null ? function(){} : x.close;
        x.closed = x.closed == null ? function(){} : x.closed;

        x.hideSubmit = x.hideSubmit == null ? false : x.hideSubmit;
        x.submit = x.submit == null ? (x.buttons == null ? function(){ahtb.close()} : function(){}) : x.submit;
        x.buttons = x.buttons == null ? 
          x.hideSubmit ? [] : [{text: (x.submitText ? x.submitText : 'OK'), action:function(){x.submit()}}] : 
          x.buttons;

        if (typeof x != 'object') x = [x];
        if (x.title) $('#TB_ajaxWindowTitle').html(x.title);
        if (x.hideTitle) $('#TB_title').hide();
        else $('#TB_title').show();
        var xx;
        var but = x.buttons;
        x.buttons = '<div class="tb-buttons'+(x.buttonsClass ? ' '+x.buttonsClass : '')+'">';
        for (var i in but){
          if (i > 0)
            x.buttons += '<span>    </span>';
          x.buttons += '<button class="'+but[i].text.replace(/ /g,'-').toLowerCase()+'">'+but[i].text+'</button>';
          if (but[i].action) funcsToDo.push({ value: but[i], func: function(xxx){ $('#tb-submit button.'+xxx.text.replace(/ /g,'-').toLowerCase()).on('click',function(){xxx.action()}) } });
        }
        x.buttons += '</div>';

        if (x.open) x.open();
        var tb = $('#tb-submit');
        if (tb)
          tb.html( (x.html ? x.html : '')+x.buttons );
        else 
          $('body').append(
            '<div id="thickbox-window" style="display:none;"><form id="tb-submit">'+(x.html ? x.html : '')+x.buttons+'</form></div>'+
            '<a href="#TB_inline?width='+x.width+'&height='+x.height+'&inlineId=thickbox-window" id="thick-click" class="thickbox" style="display:none;"></a>'
          );

        // get rid of previous ones..
        // $('body #thick-click').off();
        // $('body #thickbox-window').remove(); $('body #thick-click').remove();
        // $('body').append(
        //   '<div id="thickbox-window" style="display:none;"><form id="tb-submit">'+(x.html ? x.html : '')+x.buttons+'</form></div>'+
        //   '<a href="#TB_inline?width='+x.width+'&height='+x.height+'&inlineId=thickbox-window" id="thick-click" class="thickbox" style="display:none;"></a>'
        // );

        x.buttons = but; //restore

        // var old_remove = tb_remove;
        // $('#thick-click').off().on('click',function(){ tb_remove = function(){
        //   $('#TB_window, #TB_overlay').finish();
        //   x.close(); old_remove();
        //   $('#thickbox-window').remove(); $('#thick-click').remove();
        //   x.closed();
        //   ahtb.opened = false;
        //   return false;
        // }}).click();

        //x.closeOnClickBG || typeof x.closeOnClickBG == 'undefined' ? ahtb.closeOnClickBG(true) : ahtb.closeOnClickBG(false);


        ahtb.curX = x;
        if (x.width && x.height) ahtb.resize(x.width, x.height, false);
        else if (x.width) ahtb.resize(x.width, null, false);
        else if (x.height) ahtb.resize(null, x.height, false);
        if (funcsToDo.length > 0) for (var i in funcsToDo) funcsToDo[i].func(funcsToDo[i].value);

        if (typeof x.showClose !== 'undefined') x.showClose ? ahtb.showClose() : ahtb.hideClose();
        $(this).fadeIn(250, function(){ if (x.opened) x.opened(); });
      });
    }
    this.closeOnClickBG = function(b){ 
      // b ? $('#TB_overlay').one('click',function(){ahtb.close()}) : $('#TB_overlay').off('click') ; }
      if (b) 
        $('#TB_overlay').one('click',function(){
          ahtb.close();
      }) 
      else
        $('#TB_overlay').off('click') ; }
  }
});
