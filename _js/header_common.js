var updateProfile;
var bringUpSystem;
var makeAgent = null;
var states = '';
var hideAgent = false;
var profiles = null;
var portalRow = 0;
var gettingDirective = false;
var testPortal = '';
var testCityName = '';
var testCityId = 0;
var cityLimit = 0;
var feVersion = '1.1.69';
var trackVer = '..............................................................'; // NOTE: increase with each feVersion upping so that jsPack file has a different size
var tempVideoScrollTop = 0;
var controller = null;
var fbLogInData = {id: '',
				   first_name: '',
				   last_name: '',
				   email: '',
				   photo: '',
				   responseCount: 0,
				   fbInitCalled: 0};

// see testAgentMatchList below AgentOrderMode



var DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM = 999999999;

var SellerMetaFlags = {
	SELLER_IMPROVEMENT_TASKS: 1,
	SELLER_KEY: 2,
	SELLER_BRE: 4,
	SELLER_INVITE_CODE: 8,
	SELLER_VISITATIONS: 16,
	SELLER_PROFILE_DATA: 32,
	SELLER_AGENT_MATCH_DATA: 64,
	SELLER_MODIFIED_PROFILE_DATA: 128,
	SELLER_NICKNAME: 256,
	SELLER_NEW_ORDER: 512,
	SELLER_AGENT_ORDER: 1024,
	SELLER_AGENT_MATCH_STATUS: 2048,
	SELLER_NOTES_FROM_RESERVATION: 4096,
	SELLER_ALTERNATE_LISTHUB_DATA: 8192,
	SELLER_ZOHO_LEAD_ID: 16384,
	SELLER_PORTAL_MESSAGE: 32768,
	SELLER_COMPLETED_CARDS: 65536
}

var PortalUserMetaFlags = {
	QUIZS_TAKEN: 1,
	LISTINGS_VIEWED: 2,
	SITE_ENTRIES: 4
}

var ReservationMetaFlags = {
	RESERVATION_EXTRA_SELLER_DATA: (1 << 20)
}

var SellerFlags = {
	SELLER_AGREED_TAG_TERMS: 1,
	SELLER_UNUSED_1: 2,
	SELLER_UNUSED_2: 4,
	SELLER_IS_PREMIUM_LEVEL_1: 8,
	SELLER_IS_PREMIUM_LEVEL_2: 16,
	SELLER_IS_LIFETIME: 32,
	SELLER_INACTIVE_EXCLUSION_AREA: 64,
	SELLER_NEEDS_VERIFICATION_OF_BRE: 128,
	SELLER_LIFESTYLE_FROM_INVITE: 256,
	SELLER_NEEDS_BASIC_INFO: 512,
	SELLER_HAS_ALTERNATE_LISTHUB_DATA: 1024,
	SELLER_HAS_OPTED_IN: 2048
}

var AgentOrderMode = {
	ORDER_IDLE: 0,
	ORDER_BUYING: 1,
	ORDER_BOUGHT: 2,
	ORDER_PORTAL: 10,
	ORDER_AGENT_MATCH: 20,
	ORDER_SIGN_UP: 30
}

var ProductType = {
	MONTHLY_SUBSCRIPTION: 0,
	QUARTERLY_SUBSCRIPTION: 1
}

var PricingLevels = {
	GRADE_10: 0,
	GRADE_9: 1,
	GRADE_8: 2,
	GRADE_7: 3,
	GRADE_6: 4,
	GRADE_5: 5,
	GRADE_4: 6,
	GRADE_3: 7,
	GRADE_2: 8,
	GRADE_1: 9,
}


var AnalyticsType = {
	ANALYTICS_TYPE_CLICK: 1,
	ANALYTICS_TYPE_PAGE_ENTRY_INITIAL: 2,
	ANALYTICS_TYPE_PAGE_ENTRY: 3,
	ANALYTICS_TYPE_PAGE_EXIT: 4,
	ANALYTICS_TYPE_SLIDER: 5,
	ANALYTICS_TYPE_PROMO: 6,
	ANALYTICS_TYPE_EVENT: 7,
	ANALYTICS_TYPE_ERROR: 100,
	ANALYTICS_TYPE_ERROR_RECOVERY: 101
}

var PortalState = {
	PORTAL_NOT_EXIST: 0,
	PORTAL_VALID: 1,
	PORTAL_EXPIRING: 2,
	PORTAL_EXPIRED: 3,
	PORTAL_REMOVED: 4
}

var AgentType = {
	NON_AGENT: 0,
	AGENT_LISTHUB: 1,
	AGENT_INVITE: 2,
	AGENT_ORGANIC: 3
}


var testAgentMatchList = {};
testAgentMatchList.action = AgentOrderMode.ORDER_AGENT_MATCH;
testAgentMatchList.item = [];

var InviteCodeMode = {
	IC_USED: 1,
	IC_EXPIRE_IN_30: 2, // 30 days of use
	IC_EXPIRE_IN_180:4, //  180 days of use
	IC_EXPIRE_IN_365: 8, // good for a year
	IC_FEATURE_PORTAL: 16,
	IC_FEATURE_LIFESTYLE: 32,
	IC_FEATURE_LIFETIME: 64,
	// the following are not bit-wise
	IC_KEEP_EXPIRED_PORTAL_DAYS: 30,
	IC_ACCEPTED: 1000
}

var ListingMetaFlags = {
	LISTING_MODIFIED_DATA: 1,
	LISTING_UNUSED_META_1: 2,
	LISTING_UNUSED_META_2: 4,
	LISTING_UNUSED_META_3: 8,
	LISTING_VISITATIONS: 16
}

var ListingActiveCode = {
	AH_INACTIVE: 0,
	AH_ACTIVE: 1,
	AH_WAITING: 2,
	AH_REJECTED: 3,
	AH_TOOCHEAP: 4,
	AH_NEVER: 5,
	AH_AVERAGE: 6
}

var ListingErrorCode = {
	TOOCHEAP: 1,
	LOWTAGS:  2,
	NOIMAGE:  4,
	NOBEDBATH: 8,
	COMMERCIAL: 16,
	LACK_DESC: 32,
	RENTAL: 64
}

var ListingErrorCodeStr = {
	1: 'below median price cutoff',
	2:  'low tag count',
	4:  'low image count',
	8: 'lack bed/batch count',
	16: 'commercial',
	32: 'lack description',
	64: 'rental'
}

var RegistrationMode = {
	REGISTRATION_SPECIAL_NONE: 0,
	REGISTRATION_SPECIAL_ADMIN: 1,
	REGISTRATION_SPECIAL_CAMPAIGN_PORTAL: 2,
	REGISTRATION_SPECIAL_CAMPAIGN_LIFESTYLE: 3
}

var QuizType = {
	QUIZ_LOCALIZED: 0,
	QUIZ_NATION: 1,
	QUIZ_CITY: 2,
	QUIZ_STATE: 4,
	QUIZ_BY_ADDRESS: 10
}

var QuizFilter = {
	QUIZ_NO_LOWER_PRICE_LIMIT: 1,
	QUIZ_INCLUDE_COMMERCIAL: 2,
	QUIZ_INCLUDE_RENTAL: 4,
	QUIZ_ONLY_RENTAL: 8
}

var PortalLandingElementMode = {
  PORTAL_LANDING_NOT_USED: 0,
  PORTAL_LANDING_OPTIONAL: 1,
  PORTAL_LANDING_MUST_HAVE: 2,
  PORTAL_LANDING_USE_AS_PASSWORD: 3
};

var PortalUserFlags = {
	PORTAL_USER_NEED_SUMMARY_SENT: 1,
	PORTAL_USER_EMAIL_CAPTURED: 2,
	PORTAL_USER_HIDDEN: 4,
	PORTAL_USER_VIEWED: 8
}

var PortalUserCaptureMode = {
	CAPTURE_NONE: 0,
	CAPTURE_OPTIONAL: 1,
	CAPTURE_MUST: 2
}

function deleteCookie(what) {
	var name = what + "=";
	document.cookie = name+"; expires=Thu, 01 Jan 1970 00:00:01 GMT"+ "; path=/";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    deleteCookie(cname);
    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(what) {
    var user = getCookie(what);
    if (user != "") {
        return true;
    } else {
        return false;
    }
}

function Exception(what, where) {
	this.message = what;
	this.where = typeof where == 'undefine' ? 'Unknown' : where;
	this.toString = function() { return this.what+" at "+this.where; }
}

