var FeedbackMode = {
	MESSAGE_FEEDBACK: 1,
	MESSAGE_EMAIL_AGENT: 2,
	MESSAGE_EMAIL_LISTING_AGENT: 3,
	MESSAGE_REPLY_TO_USER: 4,
	MESSAGE_REPLY_TO_AGENT: 5,
	MESSAGE_EMAIL_AGENT_VIA_PORTAL: 6,
	MESSAGE_SELLER: 7,
	MESSAGE_CONTACT_US: 8,
	MESSAGE_EMAIL_FROM_LLC: 9,
	MESSAGE_EMAIL_CLIENT_AS_AGENT: 10,
	MESSAGE_EMAIL_PORTAL_AGENT: 11,
	MESSAGE_EMAIL_LISTING_OFFICE: 12
}

var EmailTrackerType = {
	ET_CONTENT: 1,
	ET_REPLY: 2
}

var FeedbackType = {
	FB_GENERAL: 0,
	FB_IMPROVEMENT: 1,
	FB_SUGGESTION: 2,
	FB_QUESTION: 3,
	FB_BUGREPORT: 4
}

var ahfeedback = null;

var fb_html = '<div id="main-feedback"><div class="feedback-bg"></div>'+
			    '<h2>Send a Message</h2>'  +
					'<div class="info">Enter Your Information</div>'  +
		     	'<div id="name" style="width:100%;height:31px">'  +
					'<label for="user_data">Name</label>'  +
				    '<input type="text" name="first-name" placeholder="First" />'  +
				    '<input type="text" name="last-name" placeholder="Last" />'  +
				'</div>'  +
				'<div id="email" style="width:100%;height:31px">'  +
					'<label for="user_data">Email</label>'  +
				    '<input type="email" name="email" placeholder="Name@Example.com" style="margin-left:3px;" />'  +
					'<label id="1"></label>' +
					'<select name="what" id="fb_type">'  +
						  	'<option value="0" selected="selected">General</option>'  +
						  	'<option value="1" >Improvements</option>'  +
						  	'<option value="2" >Suggestion</option>'  +
						  	'<option value="3" >Question</option>'  +
						  	'<option value="4" >Bug Report</option>'  +
					'</select>'  +
				'</div>' +
				'<div class="spacer" />' +
				'<div id="extra_feedback">'+
					'<span id="extra_message" style="display: none" />' +
					'<textarea id="xcomment" name="xcomment" rows="12" cols="50" placeholder="Please enter your comments here..."></textarea>' +
				'</div>' +
				'<div id="comment-div">'  +
					'<textarea id="comment" name="comment" rows="12" cols="50" placeholder="Please enter your comments here..."></textarea>' +
				'</div>'  +
		'</div>';

jQuery(document).ready(function($){
	ahfeedback = new function(){
		this.fbType = 0;
		this.first = '';
		this.last = '';
		this.email = '';
		this.comment = '';
		this.xcomment = '';
		this.user = null;
		this.agentId = 0;
		this.agentId2 = 0;
		this.listingAgent = 0;
		this.listingId = 0;
		this.userId = -1;
		this.etId = 0;
		this.listhubKey = '';
		this.bugReport = '';
		this.saveIntoFBInfo = false;
		this.callback = null;

		this.init = function() {
			$('#header-nav a.feedback').on('click',function(){ahfeedback.openModal()});
			$('#mobile-header-nav a.feedback').on('click',function(){ahfeedback.openModal()});
		}

		this.setCallback = function(cb) {
			ahfeedback.callback = cb;
		}

		this.clearData = function() {
			ahfeedback.fbType = 0;
			ahfeedback.first = '';
			ahfeedback.last = '';
			ahfeedback.email = '';
			ahfeedback.comment = '';
			ahfeedback.xcomment = '';
			ahfeedback.user = null;
			ahfeedback.agentId = 0;
			ahfeedback.agentId2 = 0;
			ahfeedback.listingId = 0;
			ahfeedback.userId = -1;
			ahfeedback.etId = 0;
			ahfeedback.listhubKey = '';
			ahfeedback.bugReport = '';
			ahfeedback.saveIntoFBInfo = false;
		}

		this.prep = function(mode) {
			console.log("ahfeedback prep called");

			ahfeedback.DB({
				query: 'get-user-data',
				data: {user_id: ahfeedback.userId},
				error: function(d) {
					console.log( (typeof d.data != 'undefined') ? d.data : "Failed to get user data");
					ahfeedback.userId = 0;
				},
				done: function(d) {
					console.log("Got user data");
					if (typeof d != 'undefined') {
						ahfeedback.first = d.first;
						ahfeedback.last = d.last;
						ahfeedback.email = d.email;
						ahfeedback.user = d;
					}
				},
				always: function() {
					$('input[name="last-name"]').val(ahfeedback.last);
					$('input[name="first-name"]').val(ahfeedback.first);
					$('input[name="email"]').val(ahfeedback.email);
					if (ahfeedback.email.length &&
						validateEmail(ahfeedback.email))
						$('input[name="email"]').prop('disabled', true);
					$('select#fb_type option[value="'+ahfeedback.fbType+'"]').prop('selected', true);
					$('textarea[name="comment"]').val(ahfeedback.xcomment);
					$('textarea[name="comment"]').val(ahfeedback.fbType != FeedbackType.FB_BUGREPORT ? ahfeedback.comment : ahfeedback.fixBugReport());
				}
			})
		}

		this.fixBugReport = function() {
			var d = new Date();
			var options = {
			    weekday: "long", year: "numeric", month: "short",
			    day: "numeric", hour: "2-digit", minute: "2-digit"
			};

			if (ahfeedback.bugReport.length == 0) {						
				val = 'Mark: '+d.toLocaleTimeString("en-us", options)+'\n' +
					  'Browser id: '+ah_local.candy+'\n' +
					  'Session id: '+ah_local.sessionID+'\n' +
					  'Reported by: \n' +
					  'Page that it happened on: \n' +
					  'Version (beta, alpha, mobile): \n' +
					  'File: \n' +
					  'Line#: \n' +
					  'Error: \n' +
					  'Steps to produce error: \n'+
					  'Is it repeatable (Y/N)? \n';
				ahfeedback.bugReport = val;
			}
			else {
				var pos = ahfeedback.bugReport.indexOf('Browser id');
				var sub = ahfeedback.bugReport.substring(pos);
				var dateStr = 'Mark: '+d.toLocaleTimeString("en-us", options)+'\n';
				ahfeedback.bugReport = dateStr.concat(sub);
			}
			return ahfeedback.bugReport;
		}

		this.openModal = function(mode, agentId, listingId, listhubKey, agentId2,
								  userId, first, email, content,
								  etId, last){	
			ahfeedback.clearData();
			if (typeof mode == 'undefined')
				mode = FeedbackMode.MESSAGE_FEEDBACK;
			if (typeof agentId != 'undefined')
				ahfeedback.agentId = agentId;
			if (typeof agentId2 != 'undefined')
				ahfeedback.agentId2 = agentId2;
			if (typeof listingId != 'undefined')
				ahfeedback.listingId = listingId;
			if (typeof listhubKey != 'undefined')
				ahfeedback.listhubKey = listhubKey;
			if (typeof userId != 'undefined')
				ahfeedback.userId = userId;
			if (typeof first != 'undefined' &&
				first != null)
				ahfeedback.first = first;
			if (typeof last != 'undefined' &&
				last != null)
				ahfeedback.last = last;
			if (typeof email != 'undefined' &&
				email != null)
				ahfeedback.email = email;
			if (typeof content != 'undefined' &&
				content.length) {
				if (mode != FeedbackMode.MESSAGE_EMAIL_CLIENT_AS_AGENT)
					ahfeedback.comment = "\n\n>> "+content;
				else
					ahfeedback.comment = content;
			}
			if (typeof etId != 'undefined')
				ahfeedback.etId = etId;

			var header = "Go see the sunset!"
			var title = 'What the heck!';
			var buttonTitle = 'Shoot me';
			var info = 'Enter Your Information';
			var extra_message = '';
			switch(mode) {
				case FeedbackMode.MESSAGE_FEEDBACK: 
					header = "Give us Feedback";
					title = "Feedback"; 
					buttonTitle = "Tell Us";
					break;
				case FeedbackMode.MESSAGE_EMAIL_AGENT: 
				case FeedbackMode.MESSAGE_EMAIL_LISTING_AGENT:
				case FeedbackMode.MESSAGE_EMAIL_LISTING_OFFICE:
				case FeedbackMode.MESSAGE_REPLY_TO_AGENT:
				case FeedbackMode.MESSAGE_EMAIL_AGENT_VIA_PORTAL:
				case FeedbackMode.MESSAGE_EMAIL_PORTAL_AGENT:
					header = "Send a Message";
					title = "Message"; 
					buttonTitle = "Send";
					if (mode == FeedbackMode.MESSAGE_EMAIL_AGENT_VIA_PORTAL &&
						listingId == -1) {// overloaded varialble, so if it's -1, then it came from the portal-landing page and we can save the user info
						ahfeedback.saveIntoFBInfo = true; // if they filled it out
						ahfeedback.listingId = 0;
					}
					break;
				case FeedbackMode.MESSAGE_EMAIL_CLIENT_AS_AGENT:
					header = "Send a Message";
					title = "Message"; 
					buttonTitle = "Send";
					info = "Enter client information";
					extra_message = "Note: a link to the listing will be added automatically, edit as needed."
					break;
				case FeedbackMode.MESSAGE_REPLY_TO_USER: 
				    header = "User Information";
					title = "Reply"; 
					buttonTitle = "Send";
					break;
				case FeedbackMode.MESSAGE_CONTACT_US:
					header = "Contact Us";
					title = "Message"; 
					buttonTitle = "Send";
					break;
			}

			var opened = function(){			
				$('#comment-div #1').html(title+' type:');
				$('#main-feedback h2').html(header);
				$('#main-feedback .info').html(info);
				$('#main-feedback #extra_message').html(extra_message);

				if (mode == FeedbackMode.MESSAGE_REPLY_TO_USER)
					$('div.info').hide();
				if (mode == FeedbackMode.MESSAGE_EMAIL_CLIENT_AS_AGENT) {
					$('#main-feedback').css('height', '480px');
					$('#main-feedback #extra_message').show();
				}

				ahfeedback.prep(mode);

				$('input[name="last-name"]').keyup(function(e) {
			    	e.preventDefault();
			    	ahfeedback.last = $(this).val();
			    })

			    $('input[name="first-name"]').keyup(function(e) {
			    	e.preventDefault();
			    	ahfeedback.first = $(this).val();
			    })

			    // $('input[name="email"]').keyup(function(e) {
			    // 	e.preventDefault();
			    // 	ahfeedback.email = $(this).val();
			    // })

			    $('textarea[name="comment"]').keyup(function(e) {
			    	e.preventDefault();
			    	ahfeedback.fbType != FeedbackType.FB_BUGREPORT ? ahfeedback.comment = $(this).val() : ahfeedback.bugReport = $(this).val();
			    })

			    $('textarea[name="xcomment"]').keyup(function(e) {
			    	e.preventDefault();
			    	ahfeedback.xcomment = $(this).val();
			    })

				$('select#fb_type').change(function(e) {
		    		e.preventDefault();
					var val = parseInt($(this).val());
					console.log("FB type selected: "+val);
					ahfeedback.fbType = val;
					if (val == FeedbackType.FB_BUGREPORT) // bug report
						$('textarea#comment').val(ahfeedback.fixBugReport());
					else
						$('textarea#comment').val(ahfeedback.comment);
				});
			} // end opened

			ahtb.open({
				html: fb_html,
				title: title,
				hideTitle: true,
				width: 600,
				height: (mode == FeedbackMode.MESSAGE_EMAIL_CLIENT_AS_AGENT) ? 480 : 450,
				buttons: [
					{text:buttonTitle, action: function() {
						if (ahfeedback.user == null)
							ahfeedback.user = {};

						if (ahfeedback.first == '') {
							ahtb.push();
							ahtb.alert("Please enter the first name, it is required.",
										{height: 160,
										 width: 450,
										 buttons: [
										 	{text:"OK", action: function() {
										 		ahtb.pop();
										 	}}],
										 closed: function() {
										 	ahtb.pop();
										 }});
							return;
						}

						if (ahfeedback.last == '') {
							ahtb.push();
							ahtb.alert("Please enter the last name, it is required.",
										{height: 160,
										 width: 450,
										 buttons: [
										 	{text:"OK", action: function() {
										 		ahtb.pop();
										 	}}],
										 closed: function() {
										 	ahtb.pop();
										 }});
							return;
						}

						// if (ahfeedback.email == '' &&
						// 	(mode == FeedbackMode.MESSAGE_EMAIL_AGENT ||
						// 	 mode == FeedbackMode.MESSAGE_EMAIL_LISTING_AGENT)) {
						ahfeedback.email = $('input[name="email"]').val();
						if (ahfeedback.email == '' ||
							!validateEmail(ahfeedback.email)) {
							ahtb.push();
							ahtb.alert("Please enter a valid email address.",
										{height: 160,
										 width: 500,
										 buttons: [
										 	{text:"OK", action: function() {
										 		ahtb.pop();
										 	}}],
										 closed: function() {
										 	ahtb.pop();
										 }});
							return;
						}


						if (typeof ahfeedback.user.first == 'undefined')
							ahfeedback.user.first= ahfeedback.first;
						if (typeof ahfeedback.user.last == 'undefined')
							ahfeedback.user.last = ahfeedback.last;
						if (typeof ahfeedback.user.user_email == 'undefined')
							ahfeedback.user.user_email = ahfeedback.email;
						if (typeof ahfeedback.user.user_id == 'undefined')
							ahfeedback.user.user_id = 0;

						ahfeedback.user.comment = ahfeedback.fbType != FeedbackType.FB_BUGREPORT ? ahfeedback.comment : ahfeedback.bugReport;
						ahfeedback.user.type = ahfeedback.fbType;

						if (ahfeedback.xcomment != '') {
							console.log("We have a comment bot!!");
							return;
						}

						if (ahfeedback.user.comment.length == 0) {
							ahtb.push();
							ahtb.alert("Please enter some comment or message.",
										{height: 160,
										 width: 500,
										 buttons: [
										 	{text:"OK", action: function() {
										 		ahtb.pop();
										 	}}],
										 closed: function() {
										 	ahtb.pop();
										 }});
							return;
						}

						setCookie('tp', ah_local.tp, 1);

						if (ahfeedback.saveIntoFBInfo) {
							fbLogInData.first_name = ahfeedback.first;
							fbLogInData.last_name = ahfeedback.last;
							fbLogInData.email = ahfeedback.email;
						}

						ahfeedback.DB({
							query: 'save-feedback',
							data: {feedback: ahfeedback.user,
									flags: mode,
									agent_id: ahfeedback.agentId,
									agent_id2: ahfeedback.agentId2,
									listing_id: ahfeedback.listingId,
									etId: ahfeedback.etId,
									listing_agent: typeof listingAgent != 'undefined' ? listingAgent.id : 0 },
							error: function(d) {
								console.log( (typeof d.data != 'undefined') ? d.data : "Failed to get user data");
								ahtb.alert(title+' was not sent!', {height: 150});
							},
							done: function(d) {
								console.log(d);
								ahtb.alert(title+' was sent!', {height: 150});
								// record for listhub
								if (mode == FeedbackMode.MESSAGE_EMAIL_LISTING_AGENT &&
									ah_local.ip != 1) {
									lh('submit', 'AGENT_EMAIL_SENT', {lkey: ahfeedback.listhubKey});
								}
								else if (mode == FeedbackMode.MESSAGE_EMAIL_LISTING_OFFICE &&
									ah_local.ip != 1) {
									lh('submit', 'OFFICE_EMAIL_SENT', {lkey: ahfeedback.listhubKey});
								}
								if (ahfeedback.callback) {
									ahfeedback.callback();
									ahfeedback.callback = null;
								}
							}
						})
					}},
					{text:"Cancel", action: function(){ ahtb.close(); }}],
				opened: opened
			})
		} // end openModal

		this.DB = function(xx){
			if (typeof xx.done == 'undefined') xx.done = function(d) { console.log(d); };
			if (typeof xx.error == 'undefined') xx.error = function(d) { console.log(d); };
			if (typeof xx.always == 'undefined') xx.always = function(d) { console.log("Always called."); };
			if (typeof xx.data == 'undefined') xx.data = {};
	  		// $.post(ah_local.tp+'/_pages/ajax-register.php',
	  		// 	{ query: xx.query,
	  		// 	  data: xx.data},
	  		// 	function(){}, 'JSON')
	  		//  .done(function(d){
	  		// 		if (d.status == 'OK') xx.done(d.data);
	  		// 		else xx.error(d);
	    //   	 })
	    //   	 .always(function(){
	    //   	 	xx.always();
	    //   	 });
			$('div#overlay-bg').show();
			$.ajax({
				type: "POST",
				dataType: "json",
				url:  ah_local.tp+"/_pages/ajax-register.php",
				data: { query: xx.query,
	  			     	data: xx.data},
	  			error: function($xhr, $status, $error){
	  				$('div#overlay-bg').hide();
	  				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
	  				xx.error(msg);
	  			},
	  			success: function(d){
	  				$('div#overlay-bg').hide();
	  				xx.done(d.data);
	  			},
	  			complete: function(){
	  				$('div#overlay-bg').hide();
		      	 	xx.always();
		      	}
			})
	  	}
	} // end ahfeedback

	ahfeedback.init();
})