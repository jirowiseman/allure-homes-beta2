  function contactController() {
    if (controller &&
      typeof controller.actionFBLoggedIn == 'function' &&
      fbLogInData.responseCount == (Object.keys(fbLogInData).length - 2) ) // subtract out fbInitCalled and responceCount
      controller.actionFBLoggedIn();
  }

   // gather data from user usng Graph API
  function connectAPI() {
    console.log('Welcome!  Fetching your information.... ');
    var fbData = null;
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name + ', detail: ' + JSON.stringify(response));
      fbLogInData.id = response.id;
      fbLogInData.responseCount++;
      // document.getElementById('status').innerHTML =
      //   'Thanks for logging in, ' + response.name + '!';
      FB.api('/me', {fields: ['first_name']}, function(response) {
        console.log(' page first_name: ' + JSON.stringify(response));
        fbLogInData.responseCount++;
        if (response && typeof response.first_name != 'undefined')
          fbLogInData.first_name = response.first_name;
        contactController();
      });      
      FB.api('/me', {fields: ['last_name']}, function(response) {
        console.log(' page last_name: ' + JSON.stringify(response));
        fbLogInData.responseCount++;
        if (response && typeof response.last_name != 'undefined')
          fbLogInData.last_name = response.last_name;
        contactController();
      });      
      FB.api('/me', {fields: ['email']}, function(response) {
        console.log(' page email: ' + JSON.stringify(response));
        fbLogInData.responseCount++;
        if (response && typeof response.email != 'undefined')
          fbLogInData.email = response.email;
        contactController();
      });
      // FB doesn't expose user phone number anymore, due to privacy concerns
      // FB.api('/me', {fields: ['phone']}, function(response) {
      //   console.log(' page phone: ' + JSON.stringify(response));
      //   fbLogInData.responseCount++;
      //   if (response && typeof response.phone != 'undefined')
      //     fbLogInData.phone = typeof response.phone == 'string' ? response.phone.replace(/[^0-9]/g, '') : response.phone;
      //   contactController();
      // });
      FB.api('/me', {fields: ['picture']}, function(response) {
        console.log(' page photo: ' + JSON.stringify(response));
        fbLogInData.responseCount++;
        if (response && typeof response.picture != 'undefined')
          fbLogInData.photo = response.picture.data.url;
        contactController();
      });
    });
  }

  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback - response:'+JSON.stringify(response));
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response && response.status === 'connected') {
      // Logged into your app and Facebook.
      fbLogInData.responseCount = 0; // reset
      fbLogInData.first_name = '';
      fbLogInData.last_name = '';
      fbLogInData.email = '';
      fbLogInData.photo = '';
      fbLogInData.id = '';
      connectAPI();
    } else if (response && response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('fb_status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('fb_status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
    if (controller &&
        typeof controller.actionFBInitialized == 'function')
      controller.actionFBInitialized();
    
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  // function fbLogin() {
  //   console.log("fb_login called");
  //   // Modify window settings here 
  //   var w = 660;
  //   var h = 470; 
  //   var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
  //   var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

  //   var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
  //   var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

  //   var left = ((width / 2) - (w / 2)) + dualScreenLeft;
  //   var top = ((height / 2) - (h / 2)) + dualScreenTop;

  //   // var left = parseInt((screen.availWidth/2) - (width/2));
  //   // var top = parseInt((screen.availHeight/2) - (height/2));
  //   var windowFeatures = "width=" + w + ",height=" + h + ",status=0,resizable=0,left=" + left + ",top=" + top; // + "screenX=" + left + ",screenY=" + top;


  //    //redirect_uri -where to redirect after the user approves your app 
  //    // scope - permissions separated by comma - like perms in the fb:login-button
  //    // APP_ID - your app id  

  //   login_url = 'https://graph.facebook.com/oauth/authorize?client_id=1181119758597729&scope=public_profile&redirect_uri='+window.location.href; 

  //   window.open(login_url, "_blank", windowFeatures);
  //   // FB.login(function(response) {
  //   //   statusChangeCallback(response);
  //   // },{scope: 'email,public_profile'});
  // }
 
  window.fbAsyncInit = function() {
    console.log("fbAsyncInit entered");
    FB.init({
      appId      : fbId,
      cookie     : true,
      xfbml      : true,
      version    : 'v2.7'
    });
 
    fbLogInData.fbInitCalled = 0;

    // FB.Event.subscribe('auth.login', function(response) {
    //   // do something with response
    //   console.log("FB auth.login called.");
    //   statusChangeCallback(response);
    // });


    // Now that we've initialized the JavaScript SDK, we call 
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.
    console.log("about to call FB.getLoginStatus");
    FB.getLoginStatus(function(response) {
      console.log("FB getLoginStatus called.");
      fbLogInData.fbInitCalled = 0;
      statusChangeCallback(response);
    }, true);

  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
