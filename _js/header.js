
var isOnIOS = navigator.userAgent.match(/iPad/i)|| navigator.userAgent.match(/iPhone/i);
var eventName = isOnIOS ? "pagehide" : "beforeunload";
var showSpinner = true;
var MIN_LENGTH_DESC = 300;
dbAnalyticCall = null;
dbAnalyticTimeout = 0;

ajaxRequests = [];

window.addEventListener(eventName, function (e) { 
	if (dbAnalyticTimeout)
		window.clearTimeout(dbAnalyticTimeout);

	if (dbAnalyticCall)
		dbAnalyticCall.abort();
	// if (typeof ahProfCall != 'undefined')
	if (ahProfTimeout)
		window.clearTimeout(ahProfTimeout);

	if (ahProfCall)
		ahProfCall.abort();
	return;

	if (SystemMaintenance &&
		thisPage == 'quiz')
		return;

	if (Browser.shortname == 'Firefox')
		return;

	if (thisPage == 'quiz')
		return;
	
	DBanalytics('record-analytic',
				{
					type: AnalyticsType.ANALYTICS_TYPE_PAGE_EXIT,
					session_id: ah_local.activeSessionID,
					origin: thisPage
				},
				function(query, status, msg){});
	
} );

var all_us_states = ['Alabama', 'AL', 'Alaska', 'AK', 'Arizona', 'AZ', 'Arkansas', 'AR', 'California', 'CA', 'Colorado', 'CO', 'Connecticut', 'CT', 'Delaware', 'DE', 'Florida', 'FL', 'Georgia', 'GA', 'Hawaii', 'HI', 'Idaho', 'ID', 'Illinois', 'IL', 'Indiana', 'IN', 'Iowa', 'IA', 'Kansas', 'KS', 'Kentucky', 'KY', 'Louisiana', 'LA', 'Maine', 'ME', 'Maryland', 'MD', 'Massachusetts', 'MA', 'Michigan', 'MI', 'Minnesota', 'MN', 'Mississippi', 'MS', 'Missouri', 'MO', 'Montana', 'MT', 'Nebraska', 'NE', 'Nevada', 'NV', 'New Hampshire', 'NH', 'New Jersey', 'NJ', 'New Mexico', 'NM', 'New York', 'NY', 'North Carolina', 'NC', 'North Dakota', 'ND', 'Ohio', 'OH', 'Oklahoma', 'OK', 'Oregon', 'OR', 'Pennsylvania', 'PA', 'Rhode Island', 'RI', 'South Carolina', 'SC', 'South Dakota', 'SD', 'Tennessee', 'TN', 'Texas', 'TX', 'Utah', 'UT', 'Vermont', 'VT', 'Virginia', 'VA', 'Washington', 'WA', 'West Virginia', 'WV', 'Wisconsin', 'WI', 'Wyoming', 'WY', 'American Samoa', 'AS', 'District of Columbia', 'DC', 'Federated States of Micronesia', 'FM', 'Guam', 'GU', 'Marshall Islands', 'MH', 'Northern Mariana Islands', 'MP', 'Palau', 'PW', 'Puerto Rico', 'PR', 'Virgin Islands', 'VI', 'Armed Forces Africa', 'AE', 'Armed Forces Americas', 'AA', 'Armed Forces Canada', 'AE', 'Armed Forces Europe', 'AE', 'Armed Forces Middle East', 'AE', 'Armed Forces Pacific', 'AP'];
var cities = [];

if (typeof ga != 'function')
	window.ga = function () {}



if (typeof cassie != 'undefined' &&
	cassie.length)
	setCookie('CassiopeiaDirective', cassie, 356);

// for Analytics that are non-page events (those are already taken cared of in this file)
// must have at least type (AnalyticsType) and what (i.e.: slide, next, prev, listing, get-results, etc)
function la(type, what, value_int, value_str, callback, errorCb) {
	if (typeof type == 'undefined') {
		ahtb.alert("la must have 'type' parameter", {width: 400, height: 150});
		return;
	}
	if (typeof what == 'undefined') {
		ahtb.alert("la must have 'what' parameter", {width: 400, height: 150});
		return;
	}
	
	what = typeof what != 'undefined' ? what : '';
	value_str = typeof value_str != 'undefined' && value_str != null ? value_str : '';
	value_int = typeof value_int != 'undefined' ? value_int : 0;
	DBanalytics('record-analytic',
				{
					type: type,
					session_id: ah_local.activeSessionID, // from directive
					origin: thisPage, // from php
					what: what,
					value_str: value_str,
					value_int: value_int
				},
				callback,
				errorCb);
}

function isObject(arg) { 
	return Object.prototype.toString.call(arg).indexOf('Object') !== -1; 
}

String.prototype.replaceAt=function(index, character) {
    return this.substr(0, index) + character + this.substr(index+character.length);
}

function getPrice(productType, priceLevel) {
	productType = parseInt(productType);
	priceLevel = parseInt(priceLevel);
	switch(productType) {
		case ProductType.MONTHLY_SUBSCRIPTION:
			return productPricing[priceLevel].monthly;
			break;
		case ProductType.QUARTERLY_SUBSCRIPTION:
			return productPricing[priceLevel].quarterly;
			break;
	}
}

function signInRegister() {
	ga('send', {
          'hitType': 'event',          // Required.
          'eventCategory': 'button',   // Required.
          'eventAction': 'click',      // Required.
          'eventLabel': 'SignIn Register'
        });
	la(AnalyticsType.ANALYTICS_TYPE_CLICK,
       'SignIn Register');
	window.location = ah_local.wp+'/register';
}

function prepOverlaySpinner(callback, args) {
	var whichPopup = 'div.spin-wrap';
	var parent = $('div#overlay-bg');
	var ele = $('div#overlay-bg '+whichPopup);
    var captureDivHeight = ele.innerHeight();
    var captureDivHeightHalf = captureDivHeight / 2;
    $('div#overlay-bg '+whichPopup).css('margin-top', '-'+captureDivHeightHalf+'px');
    $('#quiz-results .results-scroll').css('overflow-y', 'hidden');
    $('body').css('overflow', 'hidden');
    if (isMobile)
      	$('body').css('position','fixed');
    
	$('div#overlay-bg').fadeIn(500, function() {
		if (typeof callback == 'function')
			callback(args);
	});
}

// 1 = show all
// 2 = registered already but no options
// 3 = registered has LifeStyled, no Portal
// 4 = registered has Portal, not LifeStyled
// 5 = registered has both or Lifetime
function showAgentBenefits(mode) {
	if (ahtb.opened)
		ahtb.close();

	ga('send', {
          'hitType': 'event',          // Required.
          'eventCategory': 'button',   // Required.
          'eventAction': 'click',      // Required.
          'eventLabel': 'Agent Benefits'
        });
	la(AnalyticsType.ANALYTICS_TYPE_CLICK,
       'Agent Benefits');

	var coverAllBenefits = '<p style="margin: 0;padding: 1.5em .5em 0;font-size: .9em;"><span style="font-size: 1.55em; letter-spacing: 0;">As a registered agent on LifeStyled&#8482; Listings,</span> you can create listings, upload HD images, add tags to identity each listing&#39;s unique features, and give buyers a beautiful, detailed profile description that highlights your unique strengths and lifestyle, <i>all for free!</i></p></br></br>' +
					 		'<div style="padding:0 .5em"><span class="title">Become A LifeStyled™ Agent</span>' +
							'<span class="subtitle">Exclusively available to only 3 Agents per lifestyle.<br>Dominate your areas of expertise by being an exclusive agent in your area.   </span>' +
					 		'<p style="padding:0;margin:0">Lifestyled Agents have extensive knowledge about some lifestyle, such as aviation, boating, equestrian, golfing, ocean front homes, etc. You, as the agent will show up in all listings in your local area if a buyer choose a lifestyle that matches the one(s) you selected as your expertise. This is a fantastic way to build strong relationships with newbuyers.  Work with your dream clients and sell more homes.</p>  ' +
					 		'<span class="bottomsub">Very limited spots are available, <br>so act now to reserve your place in line.</span><button class="reserve" id="#">Reserve</button>  ' +
					 		'<span class="title">Get Your Own Portal.</span>' +
							'<span class="subtitle">We help you use your existing personal and social media networks to get paid!</span>' +
					 		'<p style="padding:0;margin:0">You can have <strong>your own pathway</strong> to our site: <strong>www.lifestyledlistings.com/&lt;your name&gt;</strong>.</br>We help you spread your link to your personal and social network.  When people enter through your link your picture and contact information is prominently displayed on every page. If a buyer comes through your link and contacts any listing agent or one of our Lifestyled Agents, you will be notified and at the same time an email will be sent to that listing agent or lifestyle agent notifying them that you have referred the buyer to them and if a home is sold as a result you are due a standard 25% referral fee.</p>' +
					 		'<span class="bottomsub">Get your custom link now while it’s still available!</span><button class="join" id="#">Buy It Now</button></br></br></div>';

	var h = {'0': {html: '<div id="agent-benefits" style="text-align: left!important; padding: 5px 20px 0 20px; background: #F9F9F9; color: #444; font-weight: 400; " >' +
							coverAllBenefits +
					 	  '</div>',
				   	height: 713},
			 '1': {html: '<div id="agent-benefits" style="text-align: left!important; padding: 5px 20px 0 20px; background: #F9F9F9; color: #444; font-weight: 400; " >' +
							coverAllBenefits +
						 		'Click <a href="javascript:register();" >here</a> to register as a real estate agent.' +
					 	  '</div>',
				   	height: 750},
			 '2': {html: '<div id="agent-benefits" style="text-align: left!important; padding: 5px 20px 0 20px; height: 780px; background: #F9F9F9; color: #444; font-weight: 400; " >' +
							'<p style="margin: 0;padding: 1.5em .5em 0;font-size: .9em;"><span style="font-size: 1.55em; letter-spacing: 0;">Thank you for becoming a registered agent on LifeStyled&#8482; Listings.</span>  We hope you created listings, uploaded HD images, added tags to identity each listing&#39;s unique features, ' +
					 		'and wrote a detailed profile description, highlighting your unique strengths and lifestyle for the world to see!</p></br></br>' +
					 		'We are a <strong>freemium service</strong> and offer additional features that gives you, the real estate agent, more tools and access to our network of buyers and ' +
					 		'agents to build up your contacts and referrals.  We do not want any of your referral fees, that is all yours to keep.  What we do offer are powerful ' +
					 		'tools for you to keep your clientele, know when someone contacts a listing agent, and to show up as an exclusive LifeStyled&#8482; agent.</br></br>' +
					 		'<span class="title">Get Your Own Portal.</span>' +
							'<span class="subtitle">We help you use your existing personal and social media networks to get paid!</span>' +
					 		'<p style="padding:0;margin:0">You can have <strong>your own pathway</strong> to our site: <strong>www.lifestyledlistings.com/&lt;your name&gt;</strong>.</br>We help you spread your link to your personal and social network.  When people enter through your link your picture and contact information is prominently displayed on every page. If a buyer comes through your link and contacts any listing agent or one of our Lifestyled Agents, you will be notified and at the same time an email will be sent to that listing agent or lifestyle agent notifying them that you have referred the buyer to them and if a home is sold as a result you are due a standard 25% referral fee.' +
					 		'The buyer&#39;s email address and the listing agent&#39;s contact information will be supplied to you also.</br></br></p>' +
					 		'<span class="title">Become A LifeStyled™ Agent</span><span class="subtitle">Exclusively available to only 3 Agents per lifestyle.<br></span>'+
					 		'This special agent will have extensive ' +
					 		'knowledge about some lifestyle, such as aviation, boating, equestrian, golfing, ocean front homes, etc.  This agent will show up in a listing, if the buyer ' +
					 		'had chosen a lifestyle that matches that of the LifeStyled&#8482; agent.  This is a fantastic way to build rapport and relationship with a buyer. ' +
					 		'If this LifeStyled&#8482; agent was found in your portal, the LifeStyled&#8482; agent has already agreed to communicate with you to create a mutual referral agreement.</br></br>' +
					 		'Click <a href="javascript:redirect('+"'"+ah_local.wp+'/sellers/#portal'+"'"+');" >here</a> to create your portal or <a href="javascript:redirect('+"'"+ah_local.wp+'/sellers/#agent_match'+"'"+');" >here</a> to define yourself as a LifeStyled&#8482; Agent.' +
					 	  '</div>',
					height: 780},
			 '3': {html: '<div id="agent-benefits" style="text-align: left!important; padding: 5px 20px 0 20px; height: 670px; background: #F9F9F9; color: #444; font-weight: 400; " >' +
							'<p style="margin: 0;padding: 1.5em .5em 0;font-size: .9em;"><span style="font-size: 1.55em; letter-spacing: 0;">Thank you for becoming an LifeStyled&#8482; agent on LifeStyled&#8482; Listings.</span>  We hope you created listings, uploaded HD images, added tags to identity each listing&#39;s unique features, ' +
					 		'and wrote a detailed profile description, highlighting your unique strengths and lifestyle for the world to see!</br></br></p>' +
					 		'As an exclusive <strong>LifeStyled&#8482; agent</strong>, please feel free to contact us and help you with anything, such as ghost writing your expert descriptions.  We know ' +
					 		'how hard it may be to find time to write your own.  We can have a phone conversation and talk about your lifestyle expertise and we can write a draft for your approval.  ' +
					 		'Once you are satisfied, we can enter that into your LifeStyled&#8482; agent attributes for those buyers who are looking for agents just like you.</br></br>' +
					 		'<span class="title">Get Your Own Portal.</span>' +
							'<span class="subtitle">We help you use your existing personal and social media networks to get paid!</span>' +
					 		'<p style="padding:0;margin:0">You can have <strong>your own pathway</strong> to our site: <strong>www.lifestyledlistings.com/&lt;your name&gt;</strong>.</br>We help you spread your link to your personal and social network.  When people enter through your link your picture and contact information is prominently displayed on every page. If a buyer comes through your link and contacts any listing agent or one of our Lifestyled Agents, you will be notified and at the same time an email will be sent to that listing agent or lifestyle agent notifying them that you have referred the buyer to them and if a home is sold as a result you are due a standard 25% referral fee.' +
					 		'The buyer&#39;s email address and the listing agent&#39;s contact information will be supplied to you also.</br></br></p>' +
					 		'Having both the <strong>LifeStyled&#8482; and Portal features</strong> will give you the most exposure and opportunities to generate referrals, which can lead to additiional income.  ' +
					 		'So please consider getting your own portal, before someone else claims the portal name that you may be thinking of.</br></br>' +
				 			'Click <a href="javascript:redirect('+"'"+ah_local.wp+'/sellers/#portal'+"'"+');" >here</a> to create your portal.' +
					 	  '</div>',
					 height: 670},
			 '4': {html: '<div id="agent-benefits" style="text-align: left!important; padding: 5px 20px 0 20px; height: 480px; background: #F9F9F9; color: #444; font-weight: 400; " >' +
							'<p style="margin: 0;padding: 1.5em .5em 0;font-size: .9em;"><span style="font-size: 1.55em; letter-spacing: 0;">Thank you for becoming a Portal agent on LifeStyled&#8482; Listings.</span>  We hope you created listings, uploaded HD images, added tags to identity each listing&#39;s unique features, ' +
					 		'and wrote a detailed profile description, highlighting your unique strengths and lifestyle for the world to see!</br></br></p>' +
					 		'As a <strong>Portal agent</strong>, please feel free to contact us for any assistance with spreading your portal within your social media accounts.  ' +
					 		'Our experts can work your social media sites to help you spread the portal link to your existing contacts!</br></br>' +
							'<span class="title">Become A LifeStyled™ Agent</span><span class="subtitle">Exclusively available to only 3 Agents per lifestyle.<br></span>'+
					 		'This special agent will have extensive ' +
					 		'knowledge about some lifestyle, such as aviation, boating, equestrian, golfing, ocean front homes, etc.  This agent will show up in a listing, if the buyer ' +
					 		'had chosen a lifestyle that matches that of the LifeStyled&#8482; agent.  This is a fantastic way to build rapport and relationship with a buyer. ' +
					 		'If this LifeStyled&#8482; agent was found in your portal, the LifeStyled&#8482; agent has already agreed to communicate with you to create a mutual referral agreement.</br></br>' +
					 		'Click <a href="javascript:redirect('+"'"+ah_local.wp+'/sellers/#agent_match'+"'"+');" >here</a> start becoming one of our exclusive LifeStyled&#8482; agents.' +
					 	  '</div>',
					 height: 480},
			 '5': {html: '<div id="agent-benefits" style="text-align: left!important; padding: 5px 20px 0 20px; height: 440px; background: #F9F9F9; color: #444; font-weight: 400; " >' +
							'<p style="margin: 0;padding: 1.5em .5em 0;font-size: .9em;"><span style="font-size: 1.55em; letter-spacing: 0;">Thank you for becoming both an LifeStyled&#8482; and Portal agent of LifeStyled&#8482; Listings.</span>  We hope you created listings, uploaded HD images, added tags to identity each listing&#39;s unique features, ' +
					 		'and wrote a detailed profile description, highlighting your unique strengths and lifestyle for the world to see!</br></br></p>' +
				 			'As an exclusive <strong>LifeStyled&#8482; agent</strong>, please feel free to contact us and help you with anything, such as ghost writing your expert descriptions.  We know ' +
					 		'how hard it may be to find time to write your own.  We can have a phone conversation and talk about your lifestyle expertise and we can write a draft for your approval.  ' +
					 		'Once you are satisfied, we can enter that into your LifeStyled&#8482; agent attributes for those buyers who are looking for agents just like you.</br></br>' +
						 	'As a <strong>Portal agent</strong>, please feel free to contact us for any assistance with spreading your portal within your social media accounts.  ' +
					 		'Our experts can work your social media sites to help you spread the portal link to your existing contacts!</br></br>' +
					 		'Click <a href="javascript:redirect('+"'"+ah_local.wp+'/sellers'+"'"+');" >here</a> to go to the seller&#39;s dashboard.' +
					 	  '</div>',
					height: 440},
			};
	console.log("showAgentBenefits - mode:"+mode);
	window.setTimeout(function() {
		if (mode == '0') {
			window.location = ah_local.wp+'/agent-benefits';
			return;
		}

		ahtb.open({html: h[mode].html,
				   width: 880,
						buttons: [],
				   height: h[mode].height,
				   opened: function() {
				   		if (mode == "0" ||
				   			mode == "1") {
					   		$('.reserve').on('click', function() {
					   			window.location = ah_local.wp+'/agent-reservation';
					   		})

					   		$('.join').on('click', function() {
					   			window.location = ah_local.wp+'/agent-purchase-portal';
					   		})
					   	}
				   }})
	}, 250);
	
}

function helpAgent() {
	this.state = -1;
	this.register = function(special) {
		if (hideAgent) {
			ahtb.alert("Agent feature not yet enabled, sorry!!",
					   {width: 450, height: 150});
			return;
		}
		var h = '<div id="upgrade-agent" > \
					<div id="bre_ahtb"><br/><div id="result"/>';
					if (typeof special == 'undefined')
						h += '<span>Please enter your Realtor License #</span>';
					else
						h += '<span>Please enter the Realtor License #</span>';
				    h += '<input type="text" id="breID" name="realtor-id" placeholder="Realtor ID - required for agent, plus state of registry"></input>';
				    h += '<div id="statesDiv">'
					    h += '<label id="stateLabel">State: </label> \
					    		<select name="States" id="states">';
						h += 	'<option value="-1" selected="selected">Select one</option>';
						h += 	states;
						h += 	'</select>';
					h+=	'</div>';
					h += '<span id="invite">  Invite code: </span><input type="text" id="invitation_code" placeHolder="(Optional)"/>';
			h += 	'</div> \
				 </div>';
		ahtb.open({
			    title: 'Upgrade to Agent',
			    height: 200,
			    width: 450,
			    html: h,
			    buttons:[
						{text:"OK", action:function(){ 
							// make ajax call
							// var ele = $('#bre input[name="realtor-id"]');
							var ele = $('#bre_ahtb #breID');
							var bre = ele.val();
							if (bre.length < 5) {
								ahtb.alert("Please enter a valid license #.", 
									       {height: 150});
								return;
							}
							if (makeAgent.state == -1) {
								ahtb.alert("Please select the state "+(typeof specail == 'undefined' ? 'your' : 'the')+" Realtor ID is registered.",
											{height: 150});
								return;
							}
							var input = $('#bre_ahtb input#invitation_code');
							var invitationCode = input.val();
							//jQuery('#result').html('Registering...').fadeIn();
							// will use current user login ID
							DBget('upgrade-register',
								  {
										realtor_id: bre,
										state: makeAgent.state,
										inviteCode: invitationCode,
										special: typeof special == 'undefined' ? 0 : 1
								  },
								  function(d) {
								  	if (d == null || d.data == null) {
										ahtb.alert("Return value from registration is null!",
											{height:150});
										return;
									}
									console.log(d.data);
									if (d.status == 'OK') {
										if (typeof special == 'undefined') {
											var msg = "Welcome to our site!  Please enjoy finding the listing that fits your lifestyle!";
											var extra = 0;
											if (bre != '') 
												if (invitationCode == '') {
													msg = "Welcome to our site!  Please browse our site.  Your agent page will be available when you verify your email address.";
													extra = 70;
												}
												else
													msg = "Welcome to our site!  Please use the seller's page and update your photo and description, then add your listings.";
											console.log(msg);
											ahtb.open({
											    title: 'Define Your Lifestyle',
											    height: 185 + extra,
											    width: 600,
											    html: d.data,
											    	buttons:[
														{text:"OK", action:function(){ 
															if (invitationCode != '' ) // then invited
						    									window.location = ah_local.wp+'/sellers';
						    								else
																ahtb.close();
														}},
													],
												opened: function() {
													console.log("Opened ack dialog");
												},
											    closed: function(){
											      ahtb.showClose(250);
											      ahtb.closeOnClickBG(1);
											    }
											});
										}
										else
											window.location = ah_local.wp+'/sellers/#agent-match-sales';
									}
									else {
										ahtb.alert(d.data, {height: 180});
									}
								  })
							// $.ajax({
							// 	type: "POST",
							// 	dataType: "json",
							// 	url:  ah_local.tp+"/_pages/ajax-register.php",
							// 	data: { 
							// 		query: 'upgrade-register',
							// 		data: {
							// 			realtor_id: bre,
							// 			state: makeAgent.state,
							// 			inviteCode: invitationCode,
							// 			special: typeof special == 'undefined' ? 0 : 1
							// 		}
							// 	},
							// 	error: function($xhr, $status, $error){
							// 		//jQuery('#result').html('Registering...').fadeOut();
							// 		var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
							// 		ahtb.open({ title: 'You Have Encountered an Error during Registration', 
							// 					height: 150, 
							// 					html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
							// 				});
							// 	},		
							// 	success: function(d){
							// 		//jQuery('#result').html('Registering...').fadeOut();
							// 		//if (d.status == 'OK') window.location = ah_local.wp+'/sellers';
							// 		if (d == null || d.data == null) {
							// 			ahtb.alert("Return value from registration is null!",
							// 				{height:150});
							// 			return;
							// 		}
							// 		console.log(d.data);
							// 		if (d.status == 'OK') {
							// 			if (typeof special == 'undefined') {
							// 				var msg = "Welcome to our site!  Please enjoy finding the listing that fits your lifestyle!";
							// 				var extra = 0;
							// 				if (bre != '') 
							// 					if (invitationCode == '') {
							// 						msg = "Welcome to our site!  Please browse our site.  Your agent page will be available when you verify your email address.";
							// 						extra = 70;
							// 					}
							// 					else
							// 						msg = "Welcome to our site!  Please use the seller's page and update your photo and description, then add your listings.";
							// 				console.log(msg);
							// 				ahtb.open({
							// 				    title: 'Define Your Lifestyle',
							// 				    height: 185 + extra,
							// 				    width: 600,
							// 				    html: d.data,
							// 				    	buttons:[
							// 							{text:"OK", action:function(){ 
							// 								if (invitationCode != '' ) // then invited
						 //    									window.location = ah_local.wp+'/sellers';
						 //    								else
							// 									ahtb.close();
							// 							}},
							// 						],
							// 					opened: function() {
							// 						console.log("Opened ack dialog");
							// 					},
							// 				    closed: function(){
							// 				      ahtb.showClose(250);
							// 				      ahtb.closeOnClickBG(1);
							// 				    }
							// 				});
							// 			}
							// 			else
							// 				window.location = ah_local.wp+'/sellers/#agent-match-sales';
							// 		}
							// 		else {
							// 			ahtb.alert(d.data, {height: 180});
							// 		}
							// 	}
							// });
							ahtb.open({
								    title: 'Registering',
								    height: 150,
								    width: 350,
								    html: '<p>Please wait a moment...</p>'});
						}},
						{text:"Cancel", action:function() {
							ahtb.close();
						}}
					],
				opened: function() {
					var state = jQuery('#bre_ahtb #states');
					state.change(function() {
						var val = jQuery("#bre_ahtb #states option:selected").val();
						makeAgent.state = val;
						console.log("State is now set to "+val);
					});
				},
			    closed: function(){
			      ahtb.showClose(250);
			      ahtb.closeOnClickBG(1);
			    }
			});
	}
}

function lifestyleAgentTermsAndConditions() {
	window.open( ah_local.wp+'/agent-terms');
	// ahtb.open({
	// 	html: '<div id="ecommerce-terms">'+
	// 		  '<p>LifeStyled&#8482; Agent is an exclusive feature available to agents whose lifestyle specialty has been vetted and approved by the LifeStyled Listings review panel.  '+
	// 		  'Every agent will receive a call for an interview about their selected lifestyle specialty and will need to prove to us that they are genuinely positioned to be part of our exclusive group of agents in a particular area</p>' +
	// 		  '<p>You may apply for any number of cities and specialties as you feel qualified to provide excellent service to unique clientele.  You will be charged for each city/specialty combination at $149.00/month each unless you have received a special promotion offer.  If the vetting process rules unfavorably, then the agent '+
	// 		  ' will have one appeal process to try to convince us of their strength.</p>' +
	// 		  '<p>REFUND POLICY<br/>' +
	// 		  'Refunds will be issued ONLY in the case that an agent has applied to be a Lifestyled&#8482; Agent, was charged, then it was found that he or she did not qualify for one or any of those lifestyles. He or she will be refunded in a reasonable amount of time for those lifestyles that have been paid for but were not found qualified for. No subscriptions fees paid shall be refunded should an agent choose to discontinue his or her subscription.  ' +
	// 		  'Sign up fees, if incurred, are non-refundable at any time.</p>'+
	// 		  '<p>Only three LifeStyled&#8482; Agent positions are open to any city/specialty combination.  Once those three positions are filled, no more agents are accepted until one of those agents relinquishes their position, or moves out of the area.  ' +
	// 		  'Any vetted agent will be placed on a waiting list (fees refunded), and will be notified when a position opens up.  Additional positions may open up if the number of clients visiting an area for a particular lifestyle begins to overload ' +
	// 		  'the approved agents capacity to accommodate them.</p>' +
	// 		  '<p>As a LifeStyled&#8482; participating agent, you also agree to abide by NAR guidelines for paying referral fees to the portal agent, if the buyer found you through the portal agent&#39;s link. The portal agent will be notified ' +
	// 		  'when a buyer contacts a LifeStyled&#8482; agent from within a listing, and will be supplied with the LifeStyled&#8482; agent&#39;s contact information.  LifeStyled&#8482; agents should expect a call from the portal agent to discuss referral agreements.</p>' +
	// 		  '<p>LifeStyled&#8482; Listings and it’s parent company Allure Technologies, LLC is not liable for any materialization of any referral fees expected by parties involved.  By agreeing to this Terms and Conditions, you are hereby agreeing to a &quot;gentleman&#39;s&quot; agreement, ' +
	// 		  'and it is wholly the responsibility of real estate agents to abide by their own agreements and ethics.</p>' +
	// 		  '<p>You hereby agree to the fees imposed, the vetting process and ethics standard.</p>' +
	// 		  '</div>',
	// 	width: 850,
	// 	height: 630,
	// 	title: "Terms And Conditions"
	// })
}

function basicFail(d) {
	if (d.status == 'fail') {
		ahtb.open({
				title: "Error",
				height: 180,
				width: 500,
				html: d.data,
				buttons: [ {text:"OK", action: function() { ahtb.close(); }}],
				closed: function(){
				      ahtb.showClose(250);
				      ahtb.closeOnClickBG(1);
				}
		});
	}
}

var BedsOptions = {
	0: {value: '0', text:'Any'},
	1: {value: '1', text:'1 '},
	2: {value: '2+', text:'2+ '},
	3: {value: '3+', text:'3+ '},
	4: {value: '4+', text:'4+ '},
};

var BathsOptions = {
	0: {value: '0', text:'Any'},
	1: {value: '1+', text:'1+'},
	2: {value: '2+', text:'2+'},
	3: {value: '3+', text:'3+'},
	4: {value: '4+', text:'4+'},
	5: {value: '5+', text:'5+'},
};


function DBget (query, data, callback, errorCb) {
	jQuery.ajax({
		type: "POST",
		dataType: "json",
		url:  ah_local.tp+"/_pages/ajax-register.php",
		data: { query: query,
				data: data },
		error: function(xhr, status, error){
			var msg = xhr.responseText ? xhr.responseText : (error ? error : "No data available");
			if (typeof errorCb == 'undefined')
				ahtb.open({ title: 'Error Fetching '+query, 
							height: 150, 
							html: '<p>Status: '+(status ? status : "No status.")+", Detail: "+msg+'</p>' 
						});
			else
				errorCb(query, status, msg);
		},		
		success: function(d){
			console.log(query+" returned "+d.status+", with: "+d.data);
			if (callback)
				callback(d);
		}
	})
}

function DBanalytics (query, data, callback, errorCb) {
	if (BypassAnalytics) {
		if (callback)
			callback({status: 'OK'});
		return;
	}

	// var doSync = Browser.shortname == 'Firefox' ||
	// 			 Browser.shortname == 'iPad' ||
	// 			 Browser.shortname == 'iPhone' ||
	// 			 Browser.shortname == 'Safari' ||
	// 			 Browser.shortname == 'IEMobile';

	var doSync = data.type == AnalyticsType.ANALYTICS_TYPE_PAGE_EXIT;
	console.log("DBanalytics called for type:"+data.type);

	dbAnalyticCall = jQuery.ajax({
		type: "POST",
		dataType: "json",
		url:  ah_local.tp+"/_admin/ajax_analytics.php",
		data: { query: query,
				data: data },
		async: !doSync,
		error: function(xhr, $status, error){
			dbAnalyticCall = null;
			if ( typeof $xhr.statusText != 'undefined' &&
                 $xhr.statusText == 'abort')
              return;
			var msg = xhr.responseText ? xhr.responseText : (error ? (typeof error == 'string' ? error : JSON.stringify(error) ): "No data available");
			msg += "data sent - ";
			msg += JSON.stringify(data);
			// var count = 0;
			// for(var i in data) {
			// 	msg += (count ? ", " : "")+i+":"+data[i];
			// 	count++;
			// }
			if (typeof errorCb == 'undefined') {
				if (data.type != AnalyticsType.ANALYTICS_TYPE_ERROR ) {
					var errData = { type: AnalyticsType.ANALYTICS_TYPE_ERROR,
									session_id: ah_local.activeSessionID,
									origin: thisPage,
									what: Browser.shortname+/*", user agent:"+Browser.userAgent+*/", version:"+Browser.version+", platform:"+Browser.platform+" failed:"+msg
								  };
					DBanalytics('record-analytic',
								errData,
								function(query, status, msg){},
								function(query, status, msg){});
				}
				else {
					console.log("Failed to record-analytic: ANALYTICS_TYPE_ERROR for "+thisPage);
				}
			}
			else
				errorCb(query, $status, msg);
		},		
		success: function(d){
			dbAnalyticCall = null;
			console.log(query+" for type:"+data.type+" returned "+d.status+(typeof d.data != 'undefined' && d.data ? ", with: "+d.data : "N/A"));
			if (typeof callback != 'undefined' &&
			 	callback)
				callback(d, typeof d.status != 'undefined' ? d.status : 'N/A',  typeof d.data != 'undefined' ? d.data : 'N/A');
		}
	});

	// if (data.type == AnalyticsType.ANALYTICS_TYPE_PAGE_ENTRY_INITIAL ||
	// 	data.type == AnalyticsType.ANALYTICS_TYPE_PAGE_ENTRY) {
	// 	// dbAnalyticCall.abort();
	// 	// dbAnalyticCall = null;
	// 	if (typeof callback != 'undefined' &&
	// 		 	callback)
	// 		callback('OK');
	// }

	// ajaxRequests.push( call );
}



function register(special) {
	makeAgent = new helpAgent();
	makeAgent.register(special);
}

function validateEmail(email){
	var ele = email.match(/(?:[A-z0-9!#$%&'*+/=?^_`{|}~-]*)+(?:\.[A-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9]*(?:[A-z0-9-]*[A-z0-9]*)?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|space)/); /* ' this just to make the regex exppression not screw up the text colorization */
	return ele == null ? false : true;
}

function validateURL(textval) {
    var urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
    return urlregex.test(textval);
}

function validatePhone(phone) {
	if (phone.length != 10)
		return false;
	if (phone == '1234567890')
		return false;
	if (phone == '0987654321')
		return false;
	var char = phone.charAt(0);
	// check if all same
	for(var i = 1; i < 10; i++) {
		var test = phone.charAt(i);
		if (!$.isNumeric(test))
			return false;
		if (test != char)
			return true;
	}

	return false;
}

// function getDirective() { // called if WAIT_PORTAL_ROW cookie was set and portalRow is available from home page
// 	console.log("getDirective - portalRow:"+portalRow);
// 	setCookie('WAIT_PORTAL_ROW', '', 1); // clear cookie
// 	if (showSpinner) $('.home-wrap #home-welcome .spinner').fadeIn(50, function(){});
// 	else  $('.home-wrap #home-welcome .spinner').hide();
// 	var cd = getCookie('CassiopeiaDirective');
// 	cd = cd.length == 0 ? "none" : cd;
// 	gettingDirective = true;
// 	console.log("getting directive at "+getTime()+", thisPage:"+thisPage);
// 	DBget(	'get-directive', 
// 			{target: cd,
// 			 portalRow: portalRow,
// 			 page: thisPage }, 
// 			 function(d) {
// 			 	console.log("got directive @1 at "+getTime());
// 			 	directive(d);
// 			 	console.log("processed directive @1 at "+getTime());
// 			 	//window.location.reload();
// 			 } );
// }

function directive(d) {
	bringUpSystem.directive(d.data);
}

function updateUserProfile() {
	updateProfile.prepare();
}

function populate(d) {
	if (d.status == 'fail') {
		basicFail(d);
		return;
	}
	updateProfile.populate(d);
}

function updated(d) {
	updateProfile.updated(d);
}

function redirect(d) {
	if (typeof d == 'object' &&
		d.status == 'fail') {
		basicFail(d);
		return;
	}

	if (typeof d == 'string') {
		if (window.location.href == d) {
			window.location.reload();
		}
		else
			window.location = d;
	}
	else if (window.location.href == d.data.redirect) {
		if (ahtb.opened)
			ahtb.close();
	}
	else
		window.location = d.data.redirect;
}

function agentProfile(d) {
	window.location = d;
}

function logout(d) {
	if (typeof IN != 'undefined' &&
		typeof IN.User != 'undefined' &&
		IN.User.isAuthorized())
		IN.User.logout(function(){ console.log("logged out of LinkedIn")});
	deleteCookie('ProfileData');
	DBget('logout', {redirect: d}, redirect);
}

function colorize($element, duration) {
	var interval = duration/10;
	setTimeout( function() {
		$element.css('-webkit-filter', 'grayscale(90%)');
		setTimeout( function() {
		$element.css('-webkit-filter', 'grayscale(80%)');
		setTimeout( function() {
		$element.css('-webkit-filter', 'grayscale(70%)');
		setTimeout( function() {
		$element.css('-webkit-filter', 'grayscale(60%)');
		setTimeout( function() {
		$element.css('-webkit-filter', 'grayscale(50%)');
		setTimeout( function() {
		$element.css('-webkit-filter', 'grayscale(40%)');
		setTimeout( function() {
		$element.css('-webkit-filter', 'grayscale(30%)');
		setTimeout( function() {
		$element.css('-webkit-filter', 'grayscale(20%)');
		setTimeout( function() {
		$element.css('-webkit-filter', 'grayscale(10%)');
		setTimeout( function() {
		$element.css('-webkit-filter', 'grayscale(0%)');
	}, interval);
	}, interval);
	}, interval);
	}, interval);
	}, interval);
	}, interval);
	}, interval);
	}, interval);
	}, interval);
	}, interval);
}

function obj2array(obj) {
	var ar = [];
	for(var i in obj)
		ar[i] = obj[i];
	return ar;
}

function arrayIsSame(ar1, ar2) {
	if (typeof ar1 == 'undefined') {
		if (typeof ar2 == 'undefined')
			return true;
		else if (ar2.length == 0)
			return true
		else
			return false;
	}
	if (typeof ar2 == 'undefined') {
		if (ar1.length == 0)
			return true;
		else
			return false;
	}

	if (this.length(ar1) != this.length(ar2)) // could be objects
		return false;

	var a1 = null;
	var a2 = null;
	a1 = Array.isArray(ar1) ? ar1.sort() : ar1; 
	a2 = Array.isArray(ar2) ? ar2.sort() : ar2; 			

	for(var i in a1)
		if (typeof a1[i] == 'object') {
			var temp1 = a1[i], temp2 = a2[i];
			if (!this.arrayIsSame(temp1, temp2))
				return false;
		}
		else if (a1[i] != a2[i])
			return false;

	return true;
}

function length(item) {
	if (typeof item == 'undefined' ||
		item == null ||
		item == false ||
		item == 0 ||
		item == 'false')
		return 0;

	if( Array.isArray(item) ) 
		return item.length;

	if (typeof item != 'object' || item == null)
		return item != null ? 1 : 0;

		// Detecting IE
    var oldIE;
    if ($('section.ie-identifier').is('.ie6, .ie7, .ie8')) {
        oldIE = true;
    }

    if (oldIE) {
        // Here's your JS for IE..
        var count = 0;
		for (var i in item) {
		    if (item.hasOwnProperty(i)) 
		        count++;
		    }
		return count;
    } else {
        // ..And here's the full-fat code for everyone else
        return Object.keys( item ).length;
    }			
}

function oldIE() {
	if ($('section.ie-identifier').is('.ie6, .ie7, .ie8 .ie9')) {
        return true;
    }
    return false;
}
function explainIconsPremier(premier) {
	var tags = {};
	for(var i in premier)
		for(var j in premier[i].tags)
			tags[j] = premier[i].tags[j];
	explainIcons(tags);
}

function explainIcons(tags) {
	var len = typeof tags != 'undefined' && tags ? length(tags) : 0;
	if (!len)
		return;
	var msg = "<p>These icons indicate lifestyle amenities that rated highly in the area.</p>";
	msg += "<div id='faq'>";
	for(var i in tags)
		msg += '<img src="'+ah_local.tp+'/_img/tag_icons/'+tags[i].icon+ '" style="width:40px;height:40px;float:left"/><span style="margin:10px 0 15px 15px;max-width:90%;display:inline-block"> - '+(typeof tags[i].desc != 'undefined' ? tags[i].desc : tags[i].description)+'</span><br/>';
	msg += '</div>';
	ahtb.open({	title: "Icon FAQ",
				html: msg,
				width: 830,
				 height: ((len)* 47) + 155,
				 maxHeight: 615,
				 opened: function(){
				 	$('#TB_ajaxContent #tb-submit #faq').css('text-align','left');
				 	$('#TB_ajaxContent #tb-submit #faq').css('margin-left','20px');
				 	$('#TB_ajaxContent #tb-submit #faq').css('margin-bottom','9px');
					$('#TB_ajaxContent #tb-submit #faq').css('height','465px');
					$('#TB_ajaxContent #tb-submit #faq').css('overflow-y','auto');
				 }});
}

function getTime(date) {
	var currentdate = typeof date == 'undefined' ? new Date() : date; 
	var datetime = (currentdate.getMonth()+1)  + "/" 
                + currentdate.getDate() + "/"
                + currentdate.getFullYear() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
    return datetime;
}

function goToAgency() {
	if (typeof ah_local.agency_url != 'undefined' &&
		ah_local.agency_url &&
		ah_local.agency_url.length) {
		var href = ah_local.agency_url.indexOf('http') == -1 ? 'http://'+ah_local.agency_url : ah_local.agency_url;
		window.open(href);
	}
}

jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + 
                                                $(window).scrollLeft()) + "px");
    return this;
}

function utf8_encode(argString) {
  //  discuss at: http://phpjs.org/functions/utf8_encode/
  // original by: Webtoolkit.info (http://www.webtoolkit.info/)
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: sowberry
  // improved by: Jack
  // improved by: Yves Sucaet
  // improved by: kirilloid
  // bugfixed by: Onno Marsman
  // bugfixed by: Onno Marsman
  // bugfixed by: Ulrich
  // bugfixed by: Rafal Kukawski
  // bugfixed by: kirilloid
  //   example 1: utf8_encode('Kevin van Zonneveld');
  //   returns 1: 'Kevin van Zonneveld'

  if (argString === null || typeof argString === 'undefined') {
    return '';
  }

  var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
  var utftext = '',
    start, end, stringl = 0;

  start = end = 0;
  stringl = string.length;
  for (var n = 0; n < stringl; n++) {
    var c1 = string.charCodeAt(n);
    var enc = null;

    if (c1 < 128) {
      end++;
    } else if (c1 > 127 && c1 < 2048) {
      enc = String.fromCharCode(
        (c1 >> 6) | 192, (c1 & 63) | 128
      );
    } else if ((c1 & 0xF800) != 0xD800) {
      enc = String.fromCharCode(
        (c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
      );
    } else { // surrogate pairs
      if ((c1 & 0xFC00) != 0xD800) {
        throw new RangeError('Unmatched trail surrogate at ' + n);
      }
      var c2 = string.charCodeAt(++n);
      if ((c2 & 0xFC00) != 0xDC00) {
        throw new RangeError('Unmatched lead surrogate at ' + (n - 1));
      }
      c1 = ((c1 & 0x3FF) << 10) + (c2 & 0x3FF) + 0x10000;
      enc = String.fromCharCode(
        (c1 >> 18) | 240, ((c1 >> 12) & 63) | 128, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
      );
    }
    if (enc !== null) {
      if (end > start) {
        utftext += string.slice(start, end);
      }
      utftext += enc;
      start = end = n + 1;
    }
  }

  if (end > start) {
    utftext += string.slice(start, stringl);
  }

  return utftext;
}

function md5(str) {
  //  discuss at: http://phpjs.org/functions/md5/
  // original by: Webtoolkit.info (http://www.webtoolkit.info/)
  // improved by: Michael White (http://getsprink.com)
  // improved by: Jack
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //    input by: Brett Zamir (http://brett-zamir.me)
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //  depends on: utf8_encode
  //   example 1: md5('Kevin van Zonneveld');
  //   returns 1: '6e658d4bfcb59cc13f96c14450ac40b9'

  var xl;

  var rotateLeft = function(lValue, iShiftBits) {
    return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
  };

  var addUnsigned = function(lX, lY) {
    var lX4, lY4, lX8, lY8, lResult;
    lX8 = (lX & 0x80000000);
    lY8 = (lY & 0x80000000);
    lX4 = (lX & 0x40000000);
    lY4 = (lY & 0x40000000);
    lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
    if (lX4 & lY4) {
      return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
    }
    if (lX4 | lY4) {
      if (lResult & 0x40000000) {
        return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
      } else {
        return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
      }
    } else {
      return (lResult ^ lX8 ^ lY8);
    }
  };

  var _F = function(x, y, z) {
    return (x & y) | ((~x) & z);
  };
  var _G = function(x, y, z) {
    return (x & z) | (y & (~z));
  };
  var _H = function(x, y, z) {
    return (x ^ y ^ z);
  };
  var _I = function(x, y, z) {
    return (y ^ (x | (~z)));
  };

  var _FF = function(a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(_F(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
  };

  var _GG = function(a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(_G(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
  };

  var _HH = function(a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(_H(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
  };

  var _II = function(a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(_I(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
  };

  var convertToWordArray = function(str) {
    var lWordCount;
    var lMessageLength = str.length;
    var lNumberOfWords_temp1 = lMessageLength + 8;
    var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
    var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
    var lWordArray = new Array(lNumberOfWords - 1);
    var lBytePosition = 0;
    var lByteCount = 0;
    while (lByteCount < lMessageLength) {
      lWordCount = (lByteCount - (lByteCount % 4)) / 4;
      lBytePosition = (lByteCount % 4) * 8;
      lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount) << lBytePosition));
      lByteCount++;
    }
    lWordCount = (lByteCount - (lByteCount % 4)) / 4;
    lBytePosition = (lByteCount % 4) * 8;
    lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
    lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
    lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
    return lWordArray;
  };

  var wordToHex = function(lValue) {
    var wordToHexValue = '',
      wordToHexValue_temp = '',
      lByte, lCount;
    for (lCount = 0; lCount <= 3; lCount++) {
      lByte = (lValue >>> (lCount * 8)) & 255;
      wordToHexValue_temp = '0' + lByte.toString(16);
      wordToHexValue = wordToHexValue + wordToHexValue_temp.substr(wordToHexValue_temp.length - 2, 2);
    }
    return wordToHexValue;
  };

  var x = [],
    k, AA, BB, CC, DD, a, b, c, d, S11 = 7,
    S12 = 12,
    S13 = 17,
    S14 = 22,
    S21 = 5,
    S22 = 9,
    S23 = 14,
    S24 = 20,
    S31 = 4,
    S32 = 11,
    S33 = 16,
    S34 = 23,
    S41 = 6,
    S42 = 10,
    S43 = 15,
    S44 = 21;

  str = utf8_encode(str);
  x = convertToWordArray(str);
  a = 0x67452301;
  b = 0xEFCDAB89;
  c = 0x98BADCFE;
  d = 0x10325476;

  xl = x.length;
  for (k = 0; k < xl; k += 16) {
    AA = a;
    BB = b;
    CC = c;
    DD = d;
    a = _FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
    d = _FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
    c = _FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
    b = _FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
    a = _FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
    d = _FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
    c = _FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
    b = _FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
    a = _FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
    d = _FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
    c = _FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
    b = _FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
    a = _FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
    d = _FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
    c = _FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
    b = _FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
    a = _GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
    d = _GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
    c = _GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
    b = _GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
    a = _GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
    d = _GG(d, a, b, c, x[k + 10], S22, 0x2441453);
    c = _GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
    b = _GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
    a = _GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
    d = _GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
    c = _GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
    b = _GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
    a = _GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
    d = _GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
    c = _GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
    b = _GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
    a = _HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
    d = _HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
    c = _HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
    b = _HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
    a = _HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
    d = _HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
    c = _HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
    b = _HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
    a = _HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
    d = _HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
    c = _HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
    b = _HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
    a = _HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
    d = _HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
    c = _HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
    b = _HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
    a = _II(a, b, c, d, x[k + 0], S41, 0xF4292244);
    d = _II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
    c = _II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
    b = _II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
    a = _II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
    d = _II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
    c = _II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
    b = _II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
    a = _II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
    d = _II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
    c = _II(c, d, a, b, x[k + 6], S43, 0xA3014314);
    b = _II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
    a = _II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
    d = _II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
    c = _II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
    b = _II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
    a = addUnsigned(a, AA);
    b = addUnsigned(b, BB);
    c = addUnsigned(c, CC);
    d = addUnsigned(d, DD);
  }

  var temp = wordToHex(a) + wordToHex(b) + wordToHex(c) + wordToHex(d);

  return temp.toLowerCase();
}

function shareSocialNetwork(which, where, url) {
	la(AnalyticsType.ANALYTICS_TYPE_CLICK,
	   'social'+which,
	   where,
	   url+", userId:"+wpId,
	   function(query, status, msg) {
	   	if (query.status != 'OK')
	   		console.log("shareSocialNetwork - "+where+" failed:"+query+", status:"+status+", msg:"+msg);
	   }
	);
	ga('send', 
		{
		  'hitType': 'event',          // Required.
		  'eventCategory': 'button',   // Required.
		  'eventAction': 'click',      // Required.
		  'eventLabel': 'social'+which+",where:"+where+",userId:"+wpId
		}
	);

	if ( which.indexOf("_Q") != -1) {
		var quiz_id = getCookie('QuizId');
		console.log("shareSocialNetwork - found quiz_id:"+quiz_id);
		if (quiz_id.length == 0) {
			ahtb.alert("Quiz id could not be found.  Unable to share to social media", {width: 450, height: 180});
			return;
		}
		url = url.replace("%quiz-id%", quiz_id);
		console.log("url is now:"+url);
	}
	
	switch(which) {
		case 'EM':
		case 'EM_Q':
			window.location.href = url;
			break;
		default:
			window.open(url);
			break;
	}
	
}

function loggedIntoLinkedIn(data) {
	console.log("entered loggedIntoLinkedIn - data:"+JSON.stringify(data));
	var goodToGo = IN.User.isAuthorized();
	console.log("shareLinkedIn, goodToGo:"+goodToGo);

	if (!goodToGo) {
		ahtb.aler("Failed to log into LinkedIn");
		return;
	}

	// if (seller.clipboardImageLinkedIn.length == 0)
	// 	seller.clipboardImageLinkedIn = ah_local.tp+"/_img/_banners/linkedin/Linked-in-1.jpg";
	if (typeof data.type != 'undefined' &&
		data.type == 'LI_Q') {
		var quiz_id = getCookie('QuizId');
		console.log("loggedIntoLinkedIn - found quiz_id:"+quiz_id);
		if (quiz_id.length == 0) {
			ahtb.alert("Quiz id could not be found.  Unable to share using LinkedIn", {width: 450, height: 180});
			return;
		}
		data.url = data.url.replace("%quiz-id%", quiz_id);
		console.log("url is now:"+data.url);
	}

	var clipboardData = {
			  "comment": data.comment,
			  "content": {
			    "title": data.title,
			    "description": data.description,
			    // "submitted-url": ah_local.wp+'/'+nickname,  
			    "submitted-url": data.url,  
			    "submitted-image-url": data.img
			  },
			  "visibility": {
			    "code": "anyone"
			  }  
			}

	console.log("Logged into LinkedIn");
	IN.API.Raw('/people/~/shares?format=json')
		.method("POST")
		.body( JSON.stringify(clipboardData) )
		.result(function(x) {
			var msg = typeof x == 'string' ? x :
					  typeof x == 'object' ? JSON.stringify(x) :
					  "Unknown reason";
			console.log(msg);
			var h = '<div id="gotoLinkedIn">Uploading shared link to LinkedIn was successful.  Press '+
					'<a href="'+x.updateUrl+'" target="_blank">here</a>'+
					' to be directed to LinkedIn to edit as you see fit.</div>';
			ahtb.alert(h,
					   {width: 500,
					    height: 210});
			la(AnalyticsType.ANALYTICS_TYPE_CLICK,
			   'social'+'LI',
			   data.where,
			   data.url+", userId:"+wpId,
			   function(query, status, msg) {
			   	if (query.status != 'OK')
			   		console.log("shareSocialNetwork - LI failed:analytics, status:"+status+", msg:"+msg);
			   }
			);
			ga('send', 
				{
				  'hitType': 'event',          // Required.
				  'eventCategory': 'button',   // Required.
				  'eventAction': 'click',      // Required.
				  'eventLabel': 'socialLI'+",where:"+data.where+",userId:"+wpId
				}
			);
		})
		.error(function(x) {
			var msg = typeof x == 'string' ? x :
					  typeof x == 'object' ? JSON.stringify(x) :
					  "Unknown reason";
			console.log(msg);
			msg = x.message == 'Do not post duplicate content' ? "You already posted to linkedin. To post again, please click on a different banner image from the images below and click on the linkedin share button again." : x.message;
			ahtb.alert(msg,
					   {width: 450,
					    height: 140+((msg.length/60)*30)});
		});
}

function shareLinkedIn(data) {
	var goodToGo = IN.User.isAuthorized();
	console.log("shareLinkedIn, goodToGo:"+goodToGo);
	var input = data;
	if (!goodToGo)
		IN.User.authorize( function() {
							loggedIntoLinkedIn(input);
						 });
	else
		loggedIntoLinkedIn(data);
	return;
}

// Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {
    	var goodToGo = IN.User.isAuthorized();
		console.log("shareLinkedIn, goodToGo:"+goodToGo);
		// if (goodToGo)
  //       	IN.Event.on(IN, "auth", getProfileData);
  //       else {
		// 	var retval = IN.User.authorize( function() {
		// 					getProfileData();
		// 				 });
		// 	console.log("onLinkedInLoad - authorize retval:"+retval);
		// }
    }

    // Handle the successful return from the API call
    function onSuccess(data) {
        console.log(data);
    }

    // Handle an error response from the API call
    function onError(error) {
        console.log(error);
        logout(ah_local.wp+'/'+thisPage);
    }

    // Use the API call wrapper to request the member's basic profile data
    function getProfileData() {
        // IN.API.Raw("/people/~").result(onSuccess).error(onError);
        IN.API.Profile("me").fields("first-name", "last-name", "email-address").result(function (data) {
                console.log(data);
            }).error(function (data) {
                console.log(data);
            });
    }

    function loadListHub() {
    	window.setTimeout(function() {
    		var isTest = ah_local.wp.indexOf('localhost') != -1 ||
    					 ah_local.wp.indexOf('alpha') != -1 ||
    					 ah_local.wp.indexOf('mobile') != -1;

    		(function(l,i,s,t,h,u,b){l['ListHubAnalyticsObject']=h;l[h]=l[h]||function(){
				  (l[h].q=l[h].q||[]).push(arguments)},l[h].d=1*new Date();u=i.createElement(s),
				  b=i.getElementsByTagName(s)[0];u.async=1;u.src=t;b.parentNode.insertBefore(u,b)
				})(window,document,'script','//tracking.listhub.net/la.min.js','lh');

			lh('init', {provider:'M-2536', test: isTest});

    	}, 50)
    }

jQuery(document).ready(function($){
	startApp();
});

function startApp() {
	if (typeof ahtb == 'undefined' ||
		!ahtb) {
		window.setTimeout(function() {
			startApp();
		}, 200);
		return;
	}

	$( window ).resize(function() {
	  	if (ahtb.opened)
	  		ahtb.responsiveAhtb();
			var ele = $('.top-panel');
			if (ele.length) {
				ele.css('min-height', '');
				ele.css('max-height', '');
				ele.removeClass('fullscreen-height');
				var height = ele.innerHeight();
				ele.css('min-height', height+'px');
				ele.css('max-height', height+'px');
				ele.addClass('fullscreen-height');
			}
	});

	console.log("Browser:"+Browser.shortname);

	if (agent != 0) {
		$('nav#header-nav .nav-container .logoDiv').hide();
		$('nav#header-nav .nav-container .agent-logoDiv').show();
		$('nav#mobile-header-nav .nav-container .dropmenu div.header-login .info-solo').css('float' , 'right');
		$('nav#mobile-header-nav .nav-container .dropmenu div.header-login .info-solo').css('margin-right' , '5px');
	}

	showSpinner = !(Browser.shortname == 'iPad' ||
					Browser.shortname == 'iPhone');

	function stopBrowserLoading() {
	    var fakeFrame = '<iframe style="height:0;width:0;display:none" src=""></iframe>';
	    if (Browser.shortname == 'Safari') {
	    	var elements = Browser.version.split('.');
	    	var version = parseInt(elements.join(''));
	    	if (version <= 519)
	    		return;
	    }
	    	
	    $('body').append(fakeFrame);
	    $(fakeFrame).remove();
	    console.log("stopBrowserLoading");
	}

	// if (typeof ah_local.candy != 'undefined' &&
	// 	ah_local.candy.length == 64)
	// 	setCookie('CassiopeiaDirective', ah_local.candy, 365);
	
	bringUpSystem = new function() {
		this.init = function() {
			// console.log("bringUpSystem sees myPortalRow:"+myPortalRow+", from ah_local:"+ah_local.portalRow);
			// if (getCookie('WAIT_PORTAL_ROW') != '') {
			// 	console.log("WAIT_PORTAL_ROW is "+getCookie('WAIT_PORTAL_ROW'));
			// 	return;
			// }
			// console.log("WAIT_PORTAL_ROW not set");
			// ahtb.alert("Initializing system..", {height: 150, hideSubmit: true});
			if (showSpinner) $('.home-wrap #home-welcome .spinner').fadeIn(50, function(){});
			else  $('.home-wrap #home-welcome .spinner').hide();
			setCookie('WAIT_PORTAL_ROW', '', 1); // clear cookie
			var cd = getCookie('CassiopeiaDirective');
			cd = cd.length == 0 ? (typeof cassie != 'undefined' && cassie.length ? cassie : "none") : cd;
			gettingDirective = true;
			portalRow = typeof myPortalRow != 'undefined' ? myPortalRow : 0;
			console.log("bringUpSystem calling get-directive at "+getTime()+", directive:"+cd+", portal:"+ah_local.portal+", thisPage:"+thisPage); //+", source:"+ah_local.directive);
			// if (typeof ah_local.directive != 'undefined' &&
			// 	ah_local.directive.length)
			// 	setCookie('CassiopeiaDirective', ah_local.directive, 30);
			DBget(	'get-directive', 
					{target: cd,
					portalRow: portalRow,
					portal: ah_local.portal,
			 		page: thisPage},
				    // target2: ah_local.directive}, 
					directive);
		}

		this.directive = function(d) {
			gettingDirective = false;
			if (showSpinner) $('.home-wrap #home-welcome .spinner').fadeOut(500, function(){});

			if (d.status == 'fail') {
				basicFail(d);
				return;
			}
			// ahtb.close();
			if (typeof d == 'string') {
				console.log(d);
				return;
			}

			console.log("directive: "+d.directive+", session: "+d.session);
			var cd = getCookie('CassiopeiaDirective');
			if (d.directive != cd) {
				console.log('current directive:'+cd+', got back:'+d.directive);
				ah_local.candy = d.directive;
			}
			setCookie('CassiopeiaDirective', d.directive, 365); // renew

			if (ah_local.sessionID != d.session &&
				typeof d.session == 'string' &&
				d.session != '') {
				console.log("current session:"+ah_local.sessionID+", got back:"+d.session);
				ah_local.sessionID = d.session;
				setCookie('PHPSESSID', d.session, 7);
			}

			if (typeof d.version != 'undefined') {
				var incomingVersion = d.version.split('.');
				var currentVersion = feVersion.split('.');
				for(var i in incomingVersion)
					if ( parseInt(incomingVersion[i]) > parseInt(currentVersion[i]) )
						window.location.reload();
			}
			//if (typeof ah_local.activeSessionID != 'undefined' &&
			//	ah_local.activeSessionID != d.activeSessionID)
			//	la( AnalyticsType.ANALYTICS_TYPE_ERROR,
			//		"ah_local.activeSessionID != d.activeSessionID, "+ah_local.activeSessionID+" vs "+d.activeSessionID+" for "+thisPage);
			ah_local.activeSessionID = d.activeSessionID;

			console.log("directive - nickname:"+d.nickname+", photo:"+d.photo+", activeSessionID:"+d.activeSessionID+", Referer:"+Referer+", ah_local.wp:"+ah_local.wp);
			dbAnalyticTimeout = window.setTimeout(function() {
				dbAnalyticTimeout = 0;
				var entryType = Referer.length == 0 ? AnalyticsType.ANALYTICS_TYPE_PAGE_ENTRY_INITIAL :
								Referer.indexOf(ah_local.wp) == -1 ? AnalyticsType.ANALYTICS_TYPE_PAGE_ENTRY_INITIAL : AnalyticsType.ANALYTICS_TYPE_PAGE_ENTRY;
				var what = '';
				var whatStr = '';
				switch(thisPage) {
					case 'listing':
					case 'listing-new':
						if (typeof listing_info != 'undefined') what = listing_info.id; 
						whatStr = ah_local.agentID;
						break;
					case 'explore-the-area':
					case 'explore-the-area-new':
						if (typeof explore_info != 'undefined' &&
							typeof explore_info.id != 'undefined') what = explore_info.id; break;
					case 'sellers':
					case 'new-listing':
					case 'agent-basic-lifestyle-info':
						what = wpId; break;
					case 'quiz-results':
					case 'quiz-results-new':
						what = incomingQuizId;
						whatStr = ah_local.agentID; 
						loadListHub();
						break;
					case 'portal-landing':
					case 'portal-landing-one':
					case 'quiz':
						what = ah_local.agentID; 
						whatStr = ah_local.agentID; break;
					default:
						break;

				}

				if ( entryType == AnalyticsType.ANALYTICS_TYPE_PAGE_ENTRY_INITIAL ) {
					setCookie("QuizResultsHistory",'', 1);
					deleteCookie("ShowTranslateButton");
				}

				// done in listing.js with setTimeout(5000), so listing loads just a tad faster
				if ( (entryType == AnalyticsType.ANALYTICS_TYPE_PAGE_ENTRY_INITIAL ||
					  entryType == AnalyticsType.ANALYTICS_TYPE_PAGE_ENTRY) &&
					 (thisPage == 'listing' ||
					  thisPage == 'listing-new') ) {
					return;
				}
				console.log("header - record-analytic for type:"+entryType+", page:"+thisPage);

				$('.ui-autocomplete').addClass('notranslate');

				DBanalytics('record-analytic',
							{
								type: entryType,
								session_id: ah_local.activeSessionID,
								origin: thisPage,
								referer: Referer,
								value_int: what,
								value_str: whatStr
							},
							function() {
								console.log("header - record-analytic returned for type:"+entryType+", page:"+thisPage);
								window.setTimeout( function() {
									stopBrowserLoading();
								}, 500);
							});
			}, 2000);


			if (typeof d.nickname == 'undefined' ||
				d.nickname.length == 0)
				return;

			var agentPage = ah_local.wp+'/agent/'+d.nickname;
			var agentPic = ah_local.tp+'/_img/_authors/55x55/'+(d.photo != null ? d.photo : '_blank.jpg');
			$('.agent-info a.agent-profile').attr('href', "javascript:agentProfile('"+agentPage+"')");
			$('.agent-info img').attr('src', agentPic);
			$('.agent-info a.name').attr('href', "javascript:agentProfile('"+agentPage+"')");
			$('.agent-info a.name').html(d.first_name+' '+d.last_name);
			$('.agent-info').show();

			var baseLogoHeight = 50;
			if (typeof ah_local.agency_logo != 'undefined' &&
				ah_local.agency_logo.length &&
				ah_local.agency_logo != 'blank.jpg' &&
				!isMobile) {
				var height = baseLogoHeight; // parseInt( $(".agent-info .agency-logo").css('width') );
				var width = ((parseFloat(ah_local.agency_logo_x_scale) * height) / 1);
				//$('.logoDiv').css('left', (225+width+20)+'px');
				$(".agent-info .agency-logo").css('background-image', 'url('+ah_local.tp+"/_img/_agency/"+ah_local.agency_logo+')');
				$(".agent-info .agency-logo").css('height', baseLogoHeight+"px");
				$(".agent-info .agency-logo").css('width', width+"px");
				$(".agent-info .tinymobile").html(ah_local.agency_shortname);
			}
			console.log("processed directive at "+getTime());
			// var h = '<a class="agent-profile" href="javascript:agentProfile('+"'"+agentPage+"'"+');" ><img src="'+ah_local.tp+'/_img/_authors/55x55/'+(d.photo != null ? d.photo : '_blank.jpg')+'" class="agent-picture" /></a>' +
			// 		'<span>Sponsored Agent: </span>' +
			// 		'<a class="name" href="javascript:agentProfile('+"'"+agentPage+"'"+');" >'+d.first_name+' '+d.last_name+'</a></span>';
			// $('.agent-info').html(h);
		}
	}
	bringUpSystem.init();
	console.log("System is coming up.");

	updateProfile = new function() {
		this.bre = '';
		this.init = function() {
			jQuery('div.header-login a.update-profile').on('click',function(){
				if (isSeller) {
					window.location = ah_local.wp+"/sellers";
					return;
				}
				// updateProfile.DB('get-user', {}, populate);
				DBget('get-user', {}, populate);
				ahtb.open({
				    title: 'Retrieving User Data',
				    height: 150,
				    width: 450,
				    html: '<p>Please wait a moment...</p>'});
			});
			console.log("updateProfile for user: "+wpId);
		}

		// this.DB = function(query, data, callback) {
		// 	jQuery.ajax({
		// 		type: "POST",
		// 		dataType: "json",
		// 		url:  ah_local.tp+"/_pages/ajax-register.php",
		// 		data: { query: query,
		// 				data: data },
		// 		error: function($xhr, $status, $error){
		// 			var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
		// 			ahtb.open({ title: 'Error Fetching '+query, 
		// 						height: 150, 
		// 						html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
		// 					});
		// 		},		
		// 		success: function(d){
		// 			console.log(query+" returned "+d.status+", with: "+d.data);
		// 			if (d.status == 'fail') {
		// 				ahtb.open({
		// 						title: "Error",
		// 						height: 180,
		// 						width: 500,
		// 						html: d.data,
		// 						buttons: [ {text:"OK", action: function() { ahtb.close(); }}],
		// 						closed: function(){
		// 						      ahtb.showClose(250);
		// 						      ahtb.closeOnClickBG(1);
		// 						}
		// 				});
		// 				return;
		// 			}
		// 			if (callback)
		// 				callback(d);
		// 		}
		// 	})
		// }

		this.populate = function(d) {
			var h = '<div id="nav-wrap" > \
						<div id="name"> \
						    <input type="text" id="first-name" placeholder="First Name" readonly /> \
						    <input type="text" id="last-name" placeholder="Last Name" readonly /> \
						</div> \
						<div id="login"> \
						    <input type="text" name="login-id" class="login-id" placeholder="Login (defaults to email)" title="Optional, defaults to your email"> \
						    <input type="text" name="email" placeholder="E-mail Address" /> \
						</div> \
						<div id="passwd"> \
					    	<input type="password" name="password" id="password" placeholder="New Password" /> \
					    	<input type="password" name="password" id="verify" placeholder="Verify Password" />\
					    </div> \
					</div> \
				    <span id="marker" class="entypo-attention"></span> ';

			ahtb.open({
				    title: 'Update Profile',
				    height: 185,
				    width: 450,
				    html: h,
				    buttons:[
							{text:"OK", action:function(){ 
								var email = '';
								if ( (email = $('input[name=email]').val()) == '') {
									ahtb.alert("Please enter your email adress.",
												{height: 150});
									return;
								}

								var login = '';
								if ( (login = $('input[name=login-id]').val()) == '') 
									login = email;

								if (!validateEmail(email)) {
									ahtb.alert("Please check your email adress, it doesn't appear to be valid.",
												{height: 150});
									return;
								}
								var passwd = '';
								if ( (passwd = $('input[name=password]').val()) != '' &&
									 passwd.length < 8) {
									ahtb.alert("Please enter a password of at least 8 characters.",
												{height: 150});
									return;
								}
								if ( passwd != $('#verify').val() ) {
									ahtb.alert("Passwords do not match.",
												{height: 150});
									return;
								}
								updateProfile.DB('update-user', 
												 {
													email: email, 
													login: login,
													password: passwd
												 },
												 updated );
								ahtb.open({
										    title: 'Registering',
										    height: 150,
										    width: 350,
										    html: '<p>Please wait a moment...</p>'});
							}},
							{text:"Cancel", action:function() {
									ahtb.close();
							}} ],
					opened: function() {
						$('#first-name').val(d.data.first_name);
						$('#last-name').val(d.data.last_name);
						$('input[name=login-id]').val(d.data.login);
						$('input[name=email]').val(d.data.email);
						updateProfile.bre = d.data.bre;

						$('#marker').hide()
					    $('#verify').keyup(function(e) {
					    	e.preventDefault();
					    	var len = $(this).val().length;
					    	len ? $('#marker').show() : $('#marker').hide();
					    	//$('#marker').attr('visibility', len ? 'visible' : 'hidden');
					    	var hasCancel = $('#marker').hasClass('entypo-attention');
					    	if ($(this).val() == $('#password').val()) {
					    		if (hasCancel) {
					    			$('#marker').removeClass('entypo-attention');
					    			$('#marker').addClass('entypo-check');
					    			$('#marker').css('color', 'lightgreen');
					    		}
					    	}
					    	else {
					    		if (!hasCancel) {
					    			$('#marker').removeClass('entypo-check');
					    			$('#marker').addClass('entypo-attention');
					     			$('#marker').css('color', 'red');
					   			}
					    	}
					    })

					    $('#password').keyup(function(e) {
					    	e.preventDefault();
					    	var len = $(this).val().length;
					    	var vLen = $('#verify').val().length;
					    	len && vLen ? $('#marker').show() : $('#marker').hide();
					    	//$('#marker').attr('visibility', len ? 'visible' : 'hidden');
					    	var hasCancel = $('#marker').hasClass('entypo-attention');
					    	if ($(this).val() == $('#verify').val()) {
					    		if (hasCancel) {
					    			$('#marker').removeClass('entypo-attention');
					    			$('#marker').addClass('entypo-check');
					    			$('#marker').css('color', 'lightgreen');
					    		}
					    	}
					    	else {
					    		if (!hasCancel) {
					    			$('#marker').removeClass('entypo-check');
					    			$('#marker').addClass('entypo-attention');
					     			$('#marker').css('color', 'red');
					   			}
					    	}
					    })

					},
					closed: function(){
				      ahtb.showClose(250);
				      ahtb.closeOnClickBG(1);
				    }
			});

		}

		this.updated = function(d) {
			var msg = "Your user profile has been updated";
			console.log(msg);
			ahtb.open({
			    title: 'Define Your Lifestyle',
			    height: 155,
			    width: 450,
			    html: d.data,
			  //   	buttons:[
					// 	{text:"OK", action:function(){ 
					// 		if (invitationCode != '' ) // then invited
					// 			window.location = ah_local.wp+'/sellers';
					// 		else
					// 			window.location.reload(); 
					// 	}},
					// ],
				opened: function() {
					console.log("Opened ack dialog");
				},
			    closed: function(){
			      ahtb.showClose(250);
			      ahtb.closeOnClickBG(1);
			    }
			});
		}
	}
	updateProfile.init();
	console.log("should've init updateProfile");

	videoControl = new function() {
		this.callback = null;

		this.init = function() {
			$('.video').on('click', function() {
				var id = $(this).attr('id');
				if ( typeof id != 'undefined' && typeof id != 'null' && id &&
					 $('.videoDiv'+'#'+id+' .video').html().length == 0 ) {
					var h = '<iframe src="//player.vimeo.com/video/'+videoList[id]+'" width="100%" height="100%" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
					$('.videoDiv'+'#'+id+' .video').html(h);
				}

				if ( $('div.videoDiv'+(typeof id != 'undefined' && typeof id != 'null' && id ? '#'+id : '')+' .video').html().length)
					videoControl.showVid(id);
				else
					ahtb.alert("Oops, video not available, sorry!");
			})
			$('.videoDiv .closeVid').hide();
			$('.videoDiv').hide();
			$('.videoDiv .closeVid').on('click', function() {
				videoControl.closeFrame($(this).parent());
				return false;
			});
			$( document ).on( 'keydown', function ( e ) {
					if ( e.keyCode === 27 ) { // Escape
							videoControl.closeFrame($('.videoDiv'));
					}
			});
		}

		this.showVid = function(id, callback) {
			tempVideoScrollTop = $(window).scrollTop();
			var vid = $('div.videoDiv'+(typeof id != 'undefined' && typeof id != 'null' && id ? '#'+id : ''));
			var frame = $('div.videoDiv'+(typeof id != 'undefined' && typeof id != 'null' && id ? '#'+id : '')+' iframe');
			vid.show();
			$('body').css('overflow','hidden');

			if (typeof callback != 'undefined')
				videoControl.callback = callback;

			var src = frame[0].src;
			var pos = 0;
			if ( (pos = src.indexOf('autoplay=')) == -1 )
				frame[0].src += "?autoplay=1";
			else
				frame[0].src = frame[0].src.replaceAt(pos+9,'1');;
			vid.animate({opacity: '1'},
						{queue: false, duration: 1000, done: function() {
							if (isMobile) {
								function delayfixed() {
									$('body').css('position','fixed');
								}
								timeoutID = window.setTimeout(delayfixed, 1000);
							}
						}} );
			$('.videoDiv'+(typeof id != 'undefined' && typeof id != 'null' && id ? '#'+id : '')+' .closeVid').show();
		}

		this.closeFrame = function(parent) {
			parent.animate({opacity: "0"},
							{queue: false, duration: 1, done: function() {
								var ele = parent.find('iframe');
								var src = ele[0].src;
								var pos = 0;
								if ( (pos = src.indexOf('autoplay=')) != -1 ) {
									src = src.replaceAt(pos+9,'0');
									ele.attr('src', src);
								}
								parent.hide();
								$('body').css('overflow','');
								if (isMobile)
									$('body').css('position','relative');
								$(window).scrollTop(tempVideoScrollTop);
							}});

			if (videoControl.callback &&
				typeof videoControl.callback == 'function') {
				videoControl.callback();
				videoControl.callback = null;
			}
		}
	}
	videoControl.init();

	function getBrowser(){
	    var userAgent = navigator.userAgent.toLowerCase();
	    $.browser.chrome = /chrome/.test(userAgent);
	    $.browser.safari= /webkit/.test(userAgent);
	    $.browser.opera=/opera/.test(userAgent);
	    $.browser.msie=/msie/.test( userAgent ) && !/opera/.test( userAgent );
	    $.browser.mozilla= /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent ) || /firefox/.test(userAgent);
	 
	    if($.browser.chrome) return "chrome";
	    if($.browser.mozilla) return "mozilla";
	    if($.browser.opera) return "opera";
	    if($.browser.safari) return "safari";
	    if($.browser.msie) return "ie";
	};

	if (oldIE())
	{
		if (getCookie("WarnOldIE") != 'true') {
			ahtb.alert("Your Internet Explorer needs to be updated to get full performance out of our site", {width: 500, height: 180});
			setCookie("WarnOldIE", 'true', 30);
		}
	}

	for (var j in all_us_states) 
		cities[j] = {label: all_us_states[j], value: all_us_states[j], id: j};

 	cityLimit = parseInt(j);
	j++;
	if (typeof ah_local != 'undefined' &&
		typeof ah_local.cities != 'undefined')
		for (var i in ah_local.cities) 
			cities[j++] = {label: ah_local.cities[i].label, value: ah_local.cities[i].label, id: ah_local.cities[i].id + cityLimit};

};
