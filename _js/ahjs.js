var ah = null;
var ahProf = null;
var gotInitialProfiies = false;
var waitedForAhFirstDirective = false;
var newWay = true;
ahProfCall = null;
ahProfTimeout = 0;
var isOnIOS = navigator.userAgent.match(/iPad/i)|| navigator.userAgent.match(/iPhone/i);
var eventName = isOnIOS ? "pagehide" : "beforeunload";

var QuizMode = {
	LOCALIZED: 0,
	NATION: 1,
	CITY: 2,
	STATE: 4
}

// window.addEventListener(eventName, function (e) { 
// 	if (ahProfCall)
// 		ahProfCall.abort();
// 	return;
// });

function cityPick(id, quizId, cityNames, doDistance, distance) {
	if (typeof doDistance == 'undefined')
		doDistance = ah.doDistance;
	if (typeof distance == 'undefined')
		distance = ah.distance;
    ah.quiz.validate(quizId, [id], doDistance, distance, cityNames);
}

var MIN_RADIUS = 0;

jQuery(document).ready(function($){
	// setCookie('NoShowProfileDelete','false',1);
	ahProf = new function() {

	this.doingQuery = false;
  	// this.profiles = null;
  	this.profileHtml = '';
  	this.attemptGetProfile = false;
  	this.callback = null;
  	this.deleteIndex = -1;

	this.defaults = {
		iconRowCount: 4, // # of icons to show per row 
		cellHeight: 43,
		actionButtonDiv: 60
	}

	this.buildProfileSection = function() {
	  	if (profiles == null)
	  		return '';
	  	var d = profiles;
	  	// var h = '<section class="profiles">'+
				// 	'<h2>existing profiles</h2>' +
		var h =		'<div style="diplay: block; height: auto; margin-top: 10px; overflow-y: auto" id="profile-table">';
		  	h += 		'<table style="width:100%" class="scrollTable">';
			// h += 		'<thead><p style="margin:0 0 1em;padding:0;font-size:.9em">Select a search profile from the list below.</p></thead>';
			// h += 			'<tbody class="scrollContent" style="width: 100%; overflow: auto; height: 250px">';
			h += 		'<thead><p style="margin:0 0 1em;padding:0;font-size:.9em">Select a search from the list below.</p></thead>';
			h += 			'<tbody class="scrollContent" style="width: 100%; overflow: auto">';

					for( var i = 0; i < d.length; i++) {
						h += '<tr';
						if ( (i % 2) == 1)
							h += ' class="alternate"';
							var isLastSearch = d[i].name.indexOf("View Last Search") === 0;
						h += '><td><button class="selectprofile" for="'+i+'">'+d[i].name+'</button>'+
							// (!isLastSearch ? '<a href="javascript:ahProf.deleteActivated('+i+');" class="deletion entypo-cancel" for="'+i+'"></a>' : '')+
							(!isLastSearch ? '<a class="deletion entypo-cancel" for="'+i+'"></button>' : '')+
							'</td>'+
							// '<td>'+(d[i].quizType != 1 && d[i].locations.length ? d[i].locations[0] : "WorldWide")+'</td>' +
							// '<td>'+(d[i].quizType != 1 ? d[i].distance : "&infin;")+'</td>' +
							'</tr>';
					}
			h += 			'</tbody></table></div>';
				// '</section>';
		self.profileHtml = h;
	  }

	  this.setNavBar = function() {
	  	if (thisPage == 'quiz-results')
	  		return;

	  	switch ( length(profiles) ) {
	  		case 0:
		  		$('.nav-container .find-a-home').off().on('click',function(){ window.location = ah_local.wp+'/quiz/#sq=0'; });
		  		$('.nav-container .find-a-home').html('Start Search');
		  		break;
		  	case 1:
		  		$('.nav-container .find-a-home').off().on('click',function(){ window.location = ah_local.wp+'/quiz-results/R-'+profiles[0].quizId; });
		  		$('.nav-container .find-a-home').html('My Results');
		  		break;
		  	default:
		  		$('.nav-container .find-a-home').off().on('click',function(){ah.openModal('my-profiles')});
		  		$('.nav-container .find-a-home').html('My Searches');
		  		console.log("My Search button is left alone, we have "+length(profiles)+" profiles");
	  	}
	  }

	  this.getProfiles = function() {
	  		self = ahProf;
	  		var profileJson = getCookie('ProfileData');
	  		if (profileJson != "" &&
	  			profileJson != '""') {
	  			profiles = JSON.parse(profileJson);
	  			self.buildProfileSection();
	  			self.setNavBar();
	  			gotInitialProfiies = true;	
	  			return;
	  		}

	  		if ( typeof ah_local.profiles == 'undefined' ||
	  			 typeof ah_local.profiles == 'string' ||
	  			 !ah_local.profiles.length)  {
		  		var data = {query: 'get-profiles',
							data: {sessionID: ah_local.activeSessionID,},
							error: function(d) {
							   		console.log("Failed to get any profiles");
							   		profiles = [];		
							   		setCookie('ProfileData', '', 1);	
							   		gotInitialProfiies = true;	
							   		ahProf.setNavBar();									
							},
							done: function(d) {
									console.log("getProfiles: "+JSON.stringify(d));
									// ah.profiles = d;
									profiles = d;
									ahProf.buildProfileSection();
									setCookie('ProfileData', JSON.stringify(profiles), 30);
									gotInitialProfiies = true;
									ahProf.setNavBar();
								}
							};
				if (ah_local.author_id != '0')
					ahProfTimeout = window.setTimeout(function() {
						ahProfTimeout = 0;
						console.log("start get-profiles at "+getTime());
						ahProf.DB(data);
					}, 3000);
			}
			else {
				console.log("getProfiles using from ah_local: "+JSON.stringify(ah_local.profiles));
				profiles = ah_local.profiles;
				ahProf.buildProfileSection();
				setCookie('ProfileData', JSON.stringify(profiles), 30);
				gotInitialProfiies = true;
				ahProf.setNavBar();
			}
	  }

	  this.reallyUseThisProfile = function(index) {
		  	if (profiles == null ||
		  		index == -1)
		  		return;

		  	// range check
		  	if (index >= profiles.length)
		  		index = profiles.length - 1;

	  		var quiz_id = profiles[index].quizId;
			// the session_id should be the same, as the user_id dictates session_id ownership
			// ah_local.sessionID = ah.profiles[index].session_id;
			// ah_local.sessionID = profiles[index].session_id;
			// ah_local.activeSessionID = profiles[index].activeSessionID;
			//$('#miles option[value="'+quizProfile.profiles[index].distance+'"]').prop('selected', true);
			var needToSave = profiles[index].name == "View Last Search" ? 1 : 0;
			setCookie("QuizCompleted", quiz_id, 1);
			setCookie("QuizListingPos", 0, 2);
			setCookie("QuizParsed", 1, 2);
			setCookie('QuizViewSelector', 'list', 1);
			setCookie("QuizSaveProfile", ah_local.author_id == "0" ? 0 : 1, 1);
			setCookie("QuizNeedSaveProfile", needToSave, 2);
			setCookie("LastViewedProfile", profiles[index].name, 7);
			setCookie("LastViewedProfileIndex", index, 2);
			// setCookie("PHPSESSID", ah_local.sessionID, 1);
			// setcookie('ACTIVE_SESSION_ID', ah_local.activeSessionID, 1);
			window.location = ah_local.wp + '/quiz-results/'+quiz_id+":"+ah_local.sessionID;

	  }

	  this.useThisProfile = function(index) {
			console.log('useThisProfile:'+profiles[index].name);
			ahProf.reallyUseThisProfile(index);
	  }

	  this.reallyDeleteProfile = function(val) {
			$('.scrollTable tr:nth-child('+val+')').remove();
			var discard = profiles.splice(val-1, 1);
			var data = {query: 'delete-profile',
						data: {sessionID: ah_local.activeSessionID,
							   info: discard[0]},
						error: function(d) {
						   		console.log("Failed to delete search");
						   		setCookie('ProfileData', '', 1);	
						   		ahtb.close();										
						},
						done: function(d) {
								console.log(JSON.stringify(d));
								// ah.profiles = d;
								profiles = d;
								setCookie('ProfileData', JSON.stringify(profiles), 30);
								if (d.length) {
									ahProf.selectMyProfile(ahProf.callback);
								}
								else
									ahtb.close();
							}
						};
			ahtb.loading("Deleting proflie..");
			ahProf.DB(data);	
		}

		this.showAskDelete = function() {
			self = ahProf;
			var index = self.deleteIndex;
            var h = '<p style="  margin-bottom:0;margin-top:.75em;font-size:1.05em;font-weight:400;padding:0">Are you sure you want to delete this search?</p><p style="margin:0 0 .5em 0;font-style:italic;font-size:.95em;color:#B5D2FF">'+profiles[index-1].name+'?</p>' +
            		'<p style="font-size:.85em;margin:0;padding:0"><input type="checkbox" id="noshow" style="height:12px;margin-right:7px" >'+"Don't show me this again</input></p>";
            
			var isMobile = $('.mobile-identifier').css('display') != 'none';
			if (!isMobile) {
				ahtb.push();
				ahtb.open({
					title: 'Delete Search',
					width: 450,
					height: 180,
					html: h,
					buttons: [
						{text:'OK', action: function() {
							ahtb.discard();
							self.reallyDeleteProfile(index);
						}},
						{text: "Cancel", action: function() {
							ahtb.pop();
							// ah.selectMyProfile();
						}}],
					opened: function() {
						$('#noshow').on('click', function() {
							setCookie('NoShowProfileDelete', $(this).prop('checked') ? 'true' : 'false', 30);
						})
					}
				})
			}
			else { // doing mobile
				h += '<button id="delete" >OK</button>@nbsp;<button id="cancel">Cancel</button>';
				$('.div .delete-profile').html(h);
				$('.delete-profile #delete').on('click', function() {
					self.reallyDeleteProfile(index);
					window.setTimeout(function() {
						$('.div .delete-profile').empty();
					}, 20);
				});
				$('.delete-profile #cancel').on('click', function() {
					window.setTimeout(function() {
						$('.div .delete-profile').empty();
					}, 20);
				});
			}
		}

		this.askReallyDelete = function(index) { // index is 1-based!!!
			self = ahProf;
			if (getCookie('NoShowProfileDelete') == 'true') {
				self.reallyDeleteProfile(index);
				return;
			}

			self.deleteIndex = index;
			// var h = '<p style="  margin-bottom:0;margin-top:.75em;font-size:1.05em;font-weight:400;padding:0">Are you sure you want to delete this profile?</p><p style="margin-top:0;font-style:italic;font-size:.95em;color:#B5D2FF">'+profiles[index-1].name+'?</p>' +
			ahtb.push();
			self.showAskDelete();
		}

	  this.deleteActivated = function(val) {
		// var val = parseInt($(this).attr('for'))+1;
		console.log("deleteActivated called with "+val);
		self.askReallyDelete(val);	
	  }

	  this.selectMyProfile = function(callback) {
	  		if (ahProf.doingQuery) {
		  		window.setTimeout(function() {
		  			ahProf.selectMyProfile(callback);
		  		}, 250);
		  		return;
		  	}
			console.log("Entered selectMyProfile");
			self = ahProf;
			if (profiles == null &&
				!self.attemptGetProfile) {
				self.attemptGetProfile = true;
				self.getProfiles();
				window.setTimeout(function() {
					self.selectMyProfile(callback);
				}, 0);
				return;
			}

			self.attemptGetProfile = false;
			if (profiles == null ||
				profiles.length == 0) {
				console.log("No profiles to select from");
				return false;
			}
			self.buildProfileSection();
			var h = self.profileHtml;
			var d = profiles;

			self.callback = callback;
			
		  	ahtb.open({title: "Select Your Search",
		  			   height: ((d.length+1) * self.defaults.cellHeight)+self.defaults.actionButtonDiv > 380+60 ? 380+self.defaults.actionButtonDiv : ((d.length+1) * self.defaults.cellHeight)+self.defaults.actionButtonDiv,
							 maxHeight: 440,
		  			   width: 450,
		  			   html: h,
		  			   hideSubmit: true,
		  			   buttons: [],
		  			   opened: function() {
		  			   		$('div#profile-table').css('height', ((d.length+2) * self.defaults.cellHeight) > 380 ? 380 : ((d.length+2) * self.defaults.cellHeight));
		  					var $table = $('.scrollTable'),
							    $bodyCells = $table.find('tbody tr:first').children(),
							    colWidth;

							// Get the tbody columns width array
							colWidth = $bodyCells.map(function() {
							    return $(this).width();
							}).get();

							// Set the width of thead columns
							$table.find('thead tr').children().each(function(i, v) {
							    $(v).width(colWidth[i]);
							});  

							$('.scrollTable td button.selectprofile').on('click', function() {
								var val = parseInt($(this).attr('for'));
								var needToSave = profiles[val].name == "View Last Search" ? 1 : 0;
								setCookie("QuizNeedSaveProfile", needToSave, 2);
								if (typeof self.callback != 'undefined' &&
									self.callback != null) {
									var cb = self.callback;
									self.callback = null;
									cb(val);
								}
								else
									self.useThisProfile(val);
								ahtb.close();
							});

							$('.scrollTable td a.deletion').on('click',function(e){
								e.preventDefault();
								var val = parseInt($(this).attr('for'))+1;
								self.askReallyDelete(val);		
								return false;														
							});
		  				}
		  			});
		  	return true;
	  }
	  this.DB = function(xx){
			if (xx != null){
				if (xx.data == null) xx.data = {};
				xx.data.sessionID = ah_local.activeSessionID;
				ahProf.doingQuery = true;
				ahProfCall = $.ajax({
					url: ah_local.tp+'/_pages/ajax-quiz.php',
					data: { query: xx.query,
					  		data: xx.data },
					dataType: 'json',
	                type: 'POST',
	                success: function(d){  	
	                	ahProfCall = null;
	                	if (d.status != 'OK')
				        	xx.error ? xx.error (d.data) : ahtb.alert('There was a problem.<br/><small>'+d.data+'</small>');
				        else if (xx.done != null)
				        	xx.done(d.data);
				        else console.log(d.data);
						ahProf.doingQuery = false;
	                },
	                error: function(d) {
	                	ahProfCall = null;
	                	ahProf.doingQuery = false;
	                	if ( typeof d.statusText != 'undefined' &&
			                 d.statusText == 'abort') {
	                	  console.log('aborted:'+xx.query);
			              return;
			      		}
	                	if (d && d.status && d.status != 'OK')
				        	xx.error ? xx.error (d.data) : ahtb.alert('There was a problem.<br/><small>'+d.data+'</small>');
				        else
				        	ahtb.alert("There was a problem, sorry!");
	                }	
	            });
			}
	  }
	}

	ah = new function(){
	  var ah_toDo = [{action: 'getProfiles',
			   	 	 arguments: null}];
	  this.doDistance = true;
	  this.distance = 8;
	  this.quizID = 0;
	  this.cityName = '';
	  this.doingQuery = false;
	  // this.profiles = null;
	  
	  	this.getReady = function() {
			if (!waitedForAhFirstDirective) {
				// ahtb.alert("Initalizing...", {width: 400, height: 150});
			    window.setTimeout(function() {
			      ah.waitForDirective();
			    }, 500);
			    waitedForAhFirstDirective = true;
			 }
		}

		this.waitForDirective = function() {
		  	if (gettingDirective) {
			    window.setTimeout(function() {
			      ah.waitForDirective();
			      // console.log("ah waiting for directive");
			    }, 1000);
			    return;
			}
			else {
				console.log("Got ah directive, sessionID:"+ah_local.sessionID+', activeSessionID:'+ah_local.activeSessionID);
				ah.init();
			}
		}
			
		this.init = function(){
			// $('.nav-container .find-a-home').on('click',function(){ah.openModal('my-profiles')});
			$('.nav-container .find-a-home').off().on('click',function(){
		  			window.location = ah_local.wp+'/quiz/#sq=0';
		  		});
			$('.nav-container .find-a-home').html('Start Search');
			//$('#mobile-header-nav a.find-a-home').on('click',function(){ah.openModal('my-profiles')});
			if (typeof ah_toDo != 'undefined'){
				for (var i in ah_toDo){
					ah[ah_toDo[i].action](ah_toDo[i].arguments);
				}
			}
		}

	  this.getProfiles = function() {
	  	ahProf.getProfiles();
	  }

	  this.openModal = function(preset){
	  	// if (ahProf.doingQuery) {
	  	// 	window.setTimeout(function() {
	  	// 		ah.openModal(preset);
	  	// 	}, 250);
	  	// 	return;
	  	// }
	  	
	  	if (preset) switch(preset){
	  		case 'my-profiles':
	  			if (ahProf.selectMyProfile() == false)
	  				!newWay ? ah.openModal('find-a-home') : window.location = ah_local.wp+'/quiz/#sq=0';
	  			break;
	  		case 'find-a-home':
	  			var h = '<div id="tb-find-a-home">'+
				    		'<section class="quiz-1">'+
				    			"<h2>Search Homes Across The U.S.</h2>"+
						        '<h3>By My Custom Lifestyle</h3>'+
						        '<button quiz="'+QuizMode.NATION+'">Start</button>'+
				    		'</section>'+
				    		'<section class="quiz-0">'+
					    		'<h2 style="color:#d7f4d4;">Search Homes In a Specific Area</h2>'+
								'<span class="desktop">'+
					    		"<table> \
					    			<tr> \
											<td><input type='checkbox' class='distance'></input></td> \
					    				<td><h3>Search Within</h3></td> \
							            <td><select name='distance' id='miles'> \
								                <option value='8' selected='selected'>8 Miles</option> \
								                <option value='15'>15 Miles</option> \
								                <option value='30'>30 Miles</option> \
								                <option value='50'>50 Miles</option> \
								                <option value='80'>80 Miles</option> \
								                <option value='120'>120 Miles</option> \
							            	</select></td> \
						          		<td><h3>Of A City</h3></td> \
						          	</tr> \
						          </table>" +
										'</span>'+
										'<span class="tinymobile">'+
					    		"<table> \
					    			<tr> \
					    				<td><h3>Search Within</h3></td> \
							            <td><select name='distance' id='miles'> \
								                <option value='8' selected='selected'>8 Miles</option> \
								                <option value='15'>15 Miles</option> \
								                <option value='30'>30 Miles</option> \
								                <option value='50'>50 Miles</option> \
								                <option value='80'>80 Miles</option> \
								                <option value='120'>120 Miles</option> \
							            	</select></td> \
						          	</tr> \
						          </table>" +
										'</span>'+
								'<div class="autocomplete">'+
									'<input type="text" class="inactive" placeholder="ENTER CITY OR STATE" autocomplete="off"/>'+
									'<button quiz="'+QuizMode.CITY+'" id="range">Search</button>'+
								'</div>'+
							'</section>';
				    h+= '</div>';
				

		    	var opened = function(){
		    		$("#tb-find-a-home .quiz-0 .distance").prop("checked", ah.doDistance);
		    		$("#tb-find-a-home .quiz-0 .distance").change(function() {
					    ah.doDistance = $("#tb-find-a-home .quiz-0 .distance").prop("checked");
					    console.log("distance is "+ah.doDistance);
					})

					$('#tb-find-a-home .quiz-0 #miles').change(function() {
					    ah.distance = parseInt($("#tb-find-a-home .quiz-0 #miles").val());
					    console.log("distance is "+ah.distance);
					});

					$('#tb-find-a-home #range').focus();

					// ah.getProfiles("#tb-find-a-home");

		    		$('#tb-find-a-home button').on('click',function(e){
		    			e.preventDefault();
		    			var quizID = $(this).attr('quiz');
		    			var valid = true;
		    			var cityNames = [];
		    			if (quizID != QuizMode.NATION) {
		    				var val = $('#tb-submit').attr('city_id');
		    				var quizLoc = val ? parseInt(val) : 0;
					        if (quizLoc == 0 || quizLoc <= cityLimit) { // even == city, odd == nationwide or statewide
						        var city = '';
						        if ( (city = $('#tb-find-a-home .autocomplete input').val()) != '' &&
              						  city != "ENTER CITY OR STATE") {
							      	$('<span class="validation-text" style="color:white">Validating entry...</span>').prependTo('#tb-find-a-home .autocomplete').hide().fadeIn({duration:250,queue:false});
						        	$.post(ah_local.tp+'/_pages/ajax-quiz.php',
						                { query:'find-city',
						                  data: { city: city,
						                  		  distance: ah.doDistance ? ah.distance : 0 }
						                },
						                function(){}, 'JSON').done(function(d){
											$('#tb-find-a-home .autocomplete span').fadeOut({duration:250,queue:false,complete:function(){$(this).remove()}});	                      
											if (d &&
						                        d.data &&
						                        d.data.city &&
						                        (typeof d.data.city == 'object' ||
						                        d.data.city.length) ){
							                    if (d.status == 'OK' &&
							                        (typeof d.data.city == 'object' || d.data.city.length) ) {							                        
							                        console.log(JSON.stringify(d.data));

							                    	if (d.data.allState == 'true' || d.data.allState == true || d.data.allState == 1) {// doing statewide
								                        ah.quiz.validate(QuizMode.STATE, [], 0, 0, [d.data.city[0]]);
								                        return;
								                    }

								                    var len = length(d.data.city); //(typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
							                    	// var len = (typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
													if (len > 1) {
														if (d.data.allState == 'false' || d.data.allState == false) {
									                        var h = '<p>Please pick one of the cities:</p>' +
									                                  '<ul class="cities">';
									                        for(var i in d.data.city) {
									                          h += '<li><a href="javascript:cityPick('+d.data.city[i].id+','+quizID+",['"+d.data.city[i].city+"']"+');>'+d.data.city[i].city+', '+d.data.city[i].state+'</a></li>';
									                        }
									                        h += '</ul>';
									                        ahtb.open(
									                          { title: 'Cities List',
									                            width: 380,
									                            height: (150 + (len * 25)) < 680 ? (150 + (len * 25)) : 680,
									                            html: h,
									                            buttons: [
									                                  {text: 'Cancel', action: function() {
									                                    ahtb.close();
									                                  }},
									                                  {text: 'Use All', action: function() {
									                                  	var cities = [];
									                                  	for(var i in d.data.city) {
									                                  		cities[i] = d.data.city[i].id;
									                                  		cityNames[cityNames.length] = d.data.city[i].city+', '+d.data.city[i].state;
                         												}
									                                  	ah.quiz.validate(quizID, cities, ah.doDistance, ah.distance, cityNames);
									                                  }}],
									                            opened: function() {
									                                // $('#TB_ajaxContent ul').css('height', (150 + (len * 25)) < 460 ? (150 + (len * 25)) : 460)
									                            }
									                          })
									                    }
									                    else {
								                          	var cities = [];
								                          	for(var i in d.data.city) {
								                            	cities[i] = d.data.city[i].id;
								                            	cityNames[cityNames.length] = d.data.city[i].city+', '+d.data.city[i].state;
                          									}
								                            // ah.distance = 30; // much faster than city/state search
							                          		ah.quiz.validate(quizID, cities, ah.doDistance, ah.distance, cityNames);
								                        }
								                    } // only one!
								                    else {
									                    cityNames[cityNames.length] = d.data.city[0].city+', '+d.data.city[0].state;
								                        ah.quiz.validate(quizID, [d.data.city[0].id], ah.doDistance, ah.distance, cityNames);
								                    }
							                    }
							                    else {
							                      $('#quiz .autocomplete input').addClass('invalid');
							                      ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
							                      			{title: 'City Match', 
							                      			 height:180, 
							                      			 width:600,
							                      			 buttons: [{text: 'OK', action: function() {
							                      			 	ah.openModal('find-a-home');
							                      			 }}]});
							                    }
							                }
							                else {
							                    $('#quiz .autocomplete input').addClass('invalid');
							                    ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
							                      			{title: 'City Match', 
							                      			 height:180, 
							                      			 width:600,
							                      			 buttons: [{text: 'OK', action: function() {
							                      			 	ah.openModal('find-a-home');
							                      			 }}]});
							                }
						                });
						          	return;
						        }
						        else
						        	valid = false;
						    }
						    else {
						    	quizLoc -= cityLimit;
						    	cityNames[cityNames.length] = ah.cityName;
						    }
					    }
					    if (valid) ah.quiz.validate(quizID, quizLoc, ah.doDistance, ah.distance, cityNames); else {
					      	$('#tb-find-a-home .autocomplete input').addClass('invalid');
					      	$('<span class="validation-text">Please enter a valid city or state.</span>').prependTo('#tb-find-a-home .autocomplete').hide().fadeIn({duration:250,queue:false});
					    }
		    		});
		    		ah.autocomplete({container: '#tb-find-a-home', input: '.autocomplete input'});
		    		$('#tb-find-a-home .autocomplete input').on('focus',function(){
		    			if ( $('#tb-find-a-home .autocomplete span').length > 0 ) $('#tb-find-a-home .autocomplete span').fadeOut({duration:250,queue:false,complete:function(){$(this).remove()}});
		    		});
		    	}
		    	// ah.getProfiles();
		    	if (window.location.href.indexOf('quiz-results') != -1 &&
	        		ah_local.author_id != '0' &&
	        		getCookie("QuizNeedSaveProfile") == '1') {
		    		!newWay ? ah.quiz.askUserToSave(ah.openModal, 'find-a-home') : ah.quiz.askUserToSave(redirect, ah_local.wp+'/quiz/#sq=0');  
	        	}
	        	else if (newWay)
	        		window.location = ah_local.wp+'/quiz/#sq=0';
	        	else {
	        		self = ah;
	        		var height = 370;
	     //    		if (self.profiles && self.profiles.length)
						// height += ((self.profiles.length+1) * self.defaults.cellHeight)+self.defaults.actionButtonDiv; // > 280+60 ? 280+self.defaults.actionButtonDiv : ((d.length+1) * self.defaults.cellHeight)+self.defaults.actionButtonDiv;
	     //    		if (profiles && profiles.length)
						// height += ((profiles.length+1) * self.defaults.cellHeight)+self.defaults.actionButtonDiv; // > 280+60 ? 280+self.defaults.actionButtonDiv : ((d.length+1) * self.defaults.cellHeight)+self.defaults.actionButtonDiv;
		      		ahtb.open({ height: height, 
			      					width: 535, 
			      					title: 'Start a New Search', 
			      					html: h, 
			      					opened: opened, 
			      					buttons: [] 
			      				});
		      	}
	  		break;
	  	}
	  }
	  this.autocomplete = function(x){
	  	if (x && x.container && x.input){
	  		$(x.container+' '+x.input).autocomplete({
	  			disabled: true,
	        source: cities,
	        position: { at: 'left bottom-15%' },
	        open: function (e,ui) {
	          if (!$(x.container+' ul.ui-autocomplete').hasClass('opened'))
	          	$(x.container+' ul.ui-autocomplete').finish().hide().slideDown(250).addClass('opened');
	          ah_local.cityWasSet = false;
	        },
	        close: function (e,ui) {
	        	$(x.container+' ul.ui-autocomplete').finish().show().slideUp(250).removeClass('opened');
	      	},
	        response: function(e, ui){
	          if (ui.content.length < 1) ui.content.push({label:'Not selected from list.',value:null});
	        },
	        select: function(e, ui){
	        	if (ui.item.value != null) {
	        		ah_local.cityWasSet = true;
	        		$('#tb-submit').attr('city_id', ui.item.id);
	        		ah.cityName = ui.item.label;
	        	}
	        }
		    }).on('focus', function(){
		      	if ($(this).val() == 'ENTER CITY OR STATE') $(this).val('');
		      	$(this).removeClass('invalid').removeClass('inactive').autocomplete('enable');
	      	}).on('focusout', function(){
		        if ($.trim($(this).val()).length < 1 ) {
		        	$(this).val('ENTER CITY OR STATE').addClass('inactive').autocomplete('close').autocomplete('disable');
		        }
		        else
		        	$('#tb-find-a-home #range').focus();
	      	}).on('keypress', function(e){
	      		var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
				if(keynum == 13) {//13 is the enter char code
					e.preventDefault();
					$('#tb-find-a-home #range').focus();
					$('#tb-find-a-home #range').click();
				}
	      	})
	      	;
	  	}
	  }

	  this.warnValidSearchName = function() {
	  	ahtb.alert("Please enter a valid search name.",
			{buttons: [
				{text: "OK", action: function() {
					ahtb.pop();
				}},
				{text:"Cancel", action: function() {
					ahtb.discard();
				}}]
			});
	  }

	  this.quiz = new function(){
	  	this.haveStartedQuiz = function(okCb, okArgs,
					  				 	cancelCb, cancelArgs) {
	  		var localized = ((typeof ah.quizID == 'string' ? parseInt(ah.quizID) : ah.quizID) % 2) == 0;
	  		ah.quiz.sendNew(ah.quizID, ah.quizLoc, localized ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0); 
	  		// ahtb.open({
	    //         title: 'Previous Quiz Exists',
	    //         height: 155,
	    //         width: 550,
	    //         html: '<p>You previously started a quiz</p>',
		   //      	buttons:[
	    //     			{text:"Start Over", action:function(){ 
	    //     				ah.quiz.sendNew(ah.quizID, ah.quizLoc, localized ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0); 
	    //     			}},
     //        		{text:"Continue", action:function(){ 
     //        			window.location = ah_local.wp+'/quiz#sq=1'; 
     //        		}}
     //    		],
	    //         closed: function(){
	    //           ahtb.showClose(250);
	    //           ahtb.closeOnClickBG(1);
	    //         }
	    //     });
	  	}
	  	this.havePrevQuiz = function(okCb, okArgs,
					  				 cancelCb, cancelArgs) {
	  		var localized = ((typeof ah.quizID == 'string' ? parseInt(ah.quizID) : ah.quizID) % 2) == 0;
	  	// 	if (ah.profiles == null ||
				// ah.profiles.length == 0) {
	  		if (profiles == null ||
				profiles.length == 0) {
				console.log("No profiles to select from");
				ah.quiz.sendNew(ah.quizID, ah.quizLoc, localized ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0); 
				return;
			}
	  		ahtb.open({
	            title: 'Completed Quiz Available',
	            height: 155,
	            width: 550,
	            html: '<p>Choose from existing searches?</p>',
	            buttons:[
	            	{text:"No Thanks", action:function(){ 
	            		ah.quiz.sendNew(ah.quizID, ah.quizLoc, localized ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0); 
	            	}},
        			{text:"View Searches", action:function(){ 
        				// setCookie("QuizParsed", 1, 2);
        				// window.location = ah_local.wp+'/quiz-results'; 
        				ah.selectMyProfile();
        			}}
        		],
	            closed: function(){
	              ahtb.showClose(250);
	              ahtb.closeOnClickBG(1);
	            }
	        });
	  	}
	  	this.askUserToSave = function(okCb, okArgs,
					  				  cancelCb, cancelArgs) {
	  		console.log('Ask user if they want to save it feature here to save quiz for registered user!!');
	    	// var h = "<div id='save-profile'><p>Name of profile:<input type='text' placeholder='Enter descriptive name' id='saveProfile'/></p></div>";
            var h = "<div id='save-profile'><p>Do you want to save your current search?<input type='text' placeholder='Name your search' id='saveProfile' style='width:65%;' /></p></div>";
   	    	var localized = ((typeof ah.quizID == 'string' ? parseInt(ah.quizID) : ah.quizID) % 2) == 0;
			ahtb.open({html: h,
					   title: "Save Your Search",
						width: 450,
						height: 180,
						buttons: [
						{text: "OK", action: function() {
							var val = $('input#saveProfile').val();
							if (val != '') {
								console.log("Saving profile as "+val);
								var data = {
									query: 'save-profile',
					  				data: { sessionID: ah_local.activeSessionID, 
					  						profile_name: val,
			  				  				quizId: getCookie("QuizId") 
						  			},
						  			done: function(d) {
						  				// ah.profiles = d;
						  				profiles = d;
						  				setCookie("QuizNeedSaveProfile", 0, 2);
				  						if (typeof okCb != 'undefined')
		  									okCb(okArgs);
		  								else
		  									ah.quiz.sendNew(ah.quizID, ah.quizLoc, localized ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0); 

						  			},
						  			error: function(d) {
						  				setCookie("QuizNeedSaveProfile", 1, 2);
						  				if (typeof okCb != 'undefined')
		  									okCb(okArgs);
		  								else
		  									ah.quiz.sendNew(ah.quizID, ah.quizLoc, localized ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0); 
						  			}
								}
								ah.DB(data);
							}
							else {
								ahtb.push();		
								ah.warnValidSearchName();						
							}
						}},			
						{text:"No Thanks", action: function() {
							setCookie("QuizNeedSaveProfile", 0, 2);
							if (typeof cancelCb != 'undefined')
								cancelCb(cancelArgs);
							else
								ah.openModal('find-a-home');
								// ah.quiz.sendNew(ah.quizID, ah.quizLoc, ah.quizID != 1 ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0); 
						}}]
					});		
	  	}

	  	this.getStatus = function(localized) {
	  		$.post(ah_local.tp+'/_pages/ajax-quiz.php', {
				query: 'get-status',
  				data: { sessionID: ah_local.activeSessionID, 
  				  				cassie:ah_local.candy,
	  				  		 	user_agent: ah_local.user_agent} 
	  			}, function(){},'JSON')
  				.done(function(d){
					if (d.status != 'OK') // should never be 'fail'
						ahtb.alert('Unable to find previous quizzes.');
					else {
						if (!isMobile) ahtb.showClose(1);
						if (d.data[0] == 'null') // no previous quiz
							ah.quiz.sendNew(ah.quizID, ah.quizLoc, localized ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0);
						else if (d.data[0] == 'started') // previous unfinished quiz
							ah.quiz.haveStartedQuiz();							  
				        else if (d.data[0] == 'done') {// previous finished quiz
				        	if (window.location.href.indexOf('quiz-results') != -1) {
				        		if (ah_local.author_id != '0' &&
				        			getCookie("QuizNeedSaveProfile") == '1') {
				        			ah.quiz.askUserToSave();	        		
				        		}
				        		else
									ah.quiz.sendNew(ah.quizID, ah.quizLoc, localized ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0); 
							}
							// else if (ah.quizID != 2) {
						 //        ah.quiz.havePrevQuiz();
						 //    }
						    else
						    	ah.quiz.sendNew(ah.quizID, ah.quizLoc, localized ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0); 
						}
				      	else if (d.data[0] == 'new') 
				      		ah.quiz.sendNew(ah.quizID, ah.quizLoc, localized ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0); 
				      	else
				      		ahtb.alert('Unexpected quiz status:'+(d.data[0]),
				      			{height: 180,
				      			 width: 450});
					}
				})
				.fail(function(d){ 
					// ahtb.alert('Failed to query for previous quizzes.',
		   //    			{height: 180,
		   //    			 width: 450});
					ah.quiz.sendNew(ah.quizID, ah.quizLoc, localized ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0);
				});
	  	}
	  	this.validate = function(quizID, quizLoc, quizDoDistance, quizDistance, cityNames){
	  		ah.quizID = quizID;
	  		ah.quizLoc = quizLoc;
	  		ah.doDistance = quizDoDistance;
	  		ah.distance = quizDistance;
	  		ah.cityNames = cityNames;
	  		var localized = ((typeof ah.quizID == 'string' ? parseInt(ah.quizID) : ah.quizID) % 2) == 0; // even = localized, odd = nationwide
	  		if (!isMobile)
		  		ahtb.loading('Loading...',{
		  			height: 95,
		  			opened: function(){
		  				ah.quiz.getStatus(localized);
		  			}
		  		});
		  	else
		  		ah.quiz.getStatus(localized);
	  	}
	  	this.sendNew = function(quizID, quizLoc, quizDistance){
	  		// even == city, odd == nationwide or statewide
	  		if (quizID == QuizMode.CITY) { //||// then it's from the city side ask user if quiz or all homes
	  			// quizID == QuizMode.STATE) { // then it's for the whole state
	  			if (typeof ah.cityNames != 'undefined' &&
	  				ah.cityNames.length) {
	  				var h = '<div id="chooseSearchMethod"><p>How do you want to search <span id="cityName">'+ah.cityNames[0]+'</span>?</p></div>';
	  				var processed = false;
	  				ahtb.open({
	  						html: h,
	  						title: 'Search Preference',
	  						width: 500,
	  						height: 180,
	  						closeOnClickBG: true,
	  						buttons: [
	  							{text: "By Your Lifestyle", action: function() {
	  								processed = true;
	  								ah.quizID = quizID == QuizMode.STATE ? QuizMode.STATE : 0; // even == localized, odd == nationwide or statewide
	  								ah.quiz.initiate(ah.quizID, ah.quizLoc, ah.quizID != 1 ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0, 0); 
	  							}},
	  							{text:"Skip to Homes", action: function() {
	  								processed = true;
	  								ah.quizID = quizID == QuizMode.STATE ? QuizMode.STATE : 2; // even == city, odd == nationwide or statewide
	  								// the last parameter to initiate(), skipQuiz bypasses Quiz and goes straight to results w/o any tags attached later
	  								ah.quiz.initiate(ah.quizID, ah.quizLoc, ah.quizID != 1 ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0, 1); 
	  							}}],
	  						closed: function() {
	  							if (!processed) {
		  							ah.quizID = quizID == QuizMode.STATE ? QuizMode.STATE : 0;
	  								ah.quiz.initiate(ah.quizID, ah.quizLoc, ah.quizID != 1 ? (ah.doDistance ? ah.distance : MIN_RADIUS) : 0, 0); 
	  							}
	  							ahtb.showClose(250);
	  						}
	  				});
	  				return;
	  			}
	  	 	}
	  	 	
	  	 	ah.quiz.initiate(quizID, quizLoc, quizDistance);
	  	}
	  	this.initiate = function(quizID, quizLoc, quizDistance, skipQuiz) {
	  		var data = {
	  			query: 'save-quiz-action',
	  			data: {
	  				// need this  QuizMode.STATE + 1 to send an odd number to save-quiz-axtion
	  				// so that it will grab '1' type questions
	  				quiz: quizID == QuizMode.STATE ? QuizMode.STATE + 1 : (quizID == QuizMode.CITY ? 0 : quizID), // dictates which quiz questions to load
	  				action: 'start',
	  				location: quizLoc,
	  				distance: quizDistance,
	  				sessionID:ah_local.activeSessionID,
	  				cassie:ah_local.candy,
	  				user_agent:ah_local.user_agent,
	  				state: quizID == QuizMode.STATE ? ah.cityNames[0] : ''
	  			},
	  			done: function(d){
  					if ( (typeof skipQuiz == 'undefined' ||
  						  skipQuiz == 0) )			
  						window.location = ah_local.wp+'/quiz#sq=1';
  					else {
  						console.log("initiate -start");
  						// this will force closure of quiz activity and begin Quiz with no tags.
  						setCookie("QuizCompleted", -1, 2);
						setCookie("QuizListingPos", 0, 2);
						setCookie("QuizParsed", 0, 2);
						setCookie('QuizViewSelector', 'list', 1);
						setCookie("QuizSaveProfile", ah_local.author_id == "0" ? 0 : 1, 1);
						setCookie("QuizNeedSaveProfile", 1, 2);
						ah.sendDone(quizID);
						// window.location = ah_local.wp+'/quiz-results/-1';
  					}
	      		},
	      		error: function(d) {
	      			ahtb.alert('Failed to get quiz questions. ' + (typeof d == 'string' ? d : ''));
	      		}
	  		};
	  		
	  		ah.DB(data);
	  	}
	  }

	  this.sendDone = function(quizID) {
	  	// this will force closure of quiz activity and begin Quiz with no tags.
	  	if (!isMobile)
	  		ahtb.loading();
	  	var data = {
	  			query: 'save-quiz-action',
	  			data: {
	  				quiz: quizID,
	  				action: 'done' }
	  	// 		done: function(d) {
				// 	data = {quiz: -1};
				// 	ah.DB({ query: 'run-quiz',
				// 					data: data });	
				// 	window.location = ah_local.wp+'/quiz-results/-1';
				// }
			}
		console.log("senDone is executing.");
		ah.DB(data);
		window.setTimeout(function() {
			window.location = ah_local.wp+'/quiz-results/-1';
		}, 1500);
	  }

	  this.DB = function(xx){
			if (xx != null){
				if (xx.data == null) xx.data = {};
				xx.data.sessionID = ah_local.activeSessionID;
				ah.doingQuery = true;
				$.post(ah_local.tp+'/_pages/ajax-quiz.php',
					{ query: xx.query,
					  data: xx.data },
					function(){}, 'JSON')
				.done(function(d){
					ah.doingQuery = false;
			        if (d.status != 'OK')
			        	xx.error ? xx.error (d.data) : ahtb.alert('There was a problem.<br/><small>'+d.data+'</small>');
			        else if (xx.done != null)
			        	xx.done(d.data);
			        else console.log(d.data);
			      })
				.fail(function(d){
					ah.doingQuery = false;
					if (d && d.status && d.status != 'OK')
			        	xx.error ? xx.error (d.data) : ahtb.alert('There was a problem.<br/><small>'+d.data+'</small>');
			        else
			        	ahtb.alert("There was a problem, sorry!");
				});
			}
	  }
	}
	ah.getReady();
});