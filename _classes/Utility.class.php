<?php
namespace AH;
global $Logger;
require_once(__DIR__.'/Logger.class.php'); $Logger = new Logger(1, 'utility');

// used by Controller class to hold the parsed data.
// since Controller is the parent class for many classes, subsequent calls to it from multiple
// classes will prevent the one class that wants the data packet to get an empty packet.  So
// whichever derived class that calls _Controller, and needs the data packet can get it.
global $query_packet;
$query_packet = null;
global $data_packet;
$data_packet = null;

define ("MAX_FILES_TO_ROTATE", 50);
// date_default_timezone_set('America/Los_Angeles');
class Out {
	public function __construct($status = 'fail', $data = null){
		$this->status = ( $status === 'OK' || $status === 1 || $status === true ? 'OK' : 'fail');
		empty($data) && $data !== '0' && $data !== 0 ? null : $this->data = $data;
	}
}
class Log {
	public $options = null;
	public function __construct($log_file = null){
		$this->options = (object)[
			'max_log_size' => 75000000, // 75 MB
			'buffer_length' => 65536,
			'file' => ( $log_file === null ? __DIR__ . '/_logs/log_'.date('Ymd-Hms') : $log_file ),
		];
		$this->logstring = '';
		if (!file_exists( $this->options->file )) $this->clear();
	}
	public function __destruct(){ $this->writeStringToFile(); }
	public function add($message, $hide_timestamp = false){
		global $timezone_adjust;
		$msg = ($hide_timestamp === true ? '' : '[' . date('Ymd.H:i:s', time() + ($timezone_adjust*3600)) . '] ') . ( !is_object($message) && !is_array($message) ? $message : removeslashes(json_encode($message)) ) . "\r\n";
		if ($this->options->buffer_length > 0 && (strlen($this->logstring) + strlen($msg)) > $this->options->buffer_length) $this->writeStringToFile();
		$this->logstring .= $msg;
		if ($this->options->buffer_length > 0 && strlen($this->logstring) > $this->options->buffer_length) $this->writeStringToFile();
	}
	public function getBackTraceString($execution_time = null){
		$x = new \Exception();
		$x = $x->getTrace();
		$out = [];
		foreach ($x as &$trace){
			if ($trace['function'] != 'getBackTraceString') {
				if (isset($trace['class'])) $trace['class'] = str_replace('//', '_', addslashes($trace['class']));
				if (isset($trace['type']) && $trace['type'] == '->') unset($trace['type']);
				if (isset($trace['file'])) $trace['file'] = substr($trace['file'], strpos($trace['file'], '/wp-'));
				if (isset($trace['args']) && empty($trace['args'])) unset($trace['args']);
				$out[] = $trace;
				unset($trace);
			}
		}
		if (isset($execution_time)) $out = [ 'query_time'=> $execution_time, 'stack' => $out ];
		return $out;
	}
	public function clear(){ if ($this->options && $this->options->file) file_put_contents($this->options->file, ''); }
	public function writeStringToFile(){
		if (strlen($this->logstring) > 0 &&
			$this->options && $this->options->file){
			error_log($this->logstring, 3, $this->options->file);
			$this->logstring = '';
		}
		if ($this->options && $this->options->file && filesize($this->options->file) > $this->options->max_log_size) {
			if (!$this->cleanUpLogFile( $this->options->file.'*'))
				rename($this->options->file, $this->options->file.'bak'.date("Y-m-d H:i:s", time() - (7*3600)));
			$this->clear();
		}
			// file_put_contents($this->options->file, '');
			// file_put_contents($this->options->file, substr(file_get_contents($this->options->file), (-1 * $this->options->max_log_size)) );
	}
	protected function cleanUpLogFile($logMask)
	{
		$files = array();
		foreach (glob($logMask) as $filename)
			$files[] = $filename;

		$keyFile = substr($logMask, 0, -1);

		$doClean = false;
		foreach($files as $filename)
			if ($filename == $keyFile)
			{
				$doClean = true;
				break;
			}

		if (!$doClean)
			return 0;

    	$count = count($files);
		if (!$count)
			return 0;
    		
    	rsort($files, SORT_STRING);
    	$max = $count > MAX_FILES_TO_ROTATE ? MAX_FILES_TO_ROTATE : $count;
    	foreach($files as $filename)
    	{
    		$file_info = pathinfo($filename);
    		if ($count > $max)
    			unlink($filename);
    		else
    		{
    			$base = preg_replace('/\d+/', '', basename($filename));
    			$newname = $file_info['dirname']."/".$base.$count;
    			rename($filename, $newname);
    		}
    		unset($file_info);
    		$count--;
    	}
    	return true;
	}
}

function parseException($e, $print = false, $debug = false){
	$Log = new Log(__DIR__ . '/_logs/exception_log');
	$Log->add( $Log->getBackTraceString() );
	if ($debug) $out = json_encode(new Out(0,
			array('trace'=>$e->getTrace(),'message'=>$e->getMessage(),'code'=>$e->getCode(),'file'=>$e->getFile(),'line'=>$e->getLine() )
		));
	else $out = json_encode(new Out(0, $e->getMessage()) );
	if ($print) echo $out;
	else return $out;
	unset($out, $e, $print, $debug);
	flush();
}

// function formatName(&$data) {
// 	if (!isset($data) || empty($data))
// 		return $data;

// 	$data = strtolower(trim($data));
// 	$data = str_replace('-',' ',$data);
// 	$data = str_replace(',',' ',$data);
// 	$data = str_replace('&',' ',$data);
// 	$data = str_replace('  ',' ',$data);
// 	$t = explode(' ', $data);
// 	$data = '';
// 	$doNotUpperFirst = ['the','for','by','on'];
// 	foreach ($t as $i=>$str){
// 		if ($i > 0) $data .= ' ';
// 		if ($i < 1 || !in_array($str, $doNotUpperFirst)) {
// 			$str = ucfirst($str);
// 			if (substr($str, 0,2) == "Mc" &&
// 				strlen($str) > 2)
// 				$str[2] = chr( ord($str[2]) - 32 ); // cap that 3rd letter
// 		}
// 		$data .= $str;
// 	} unset($i, $t, $str);
// 	return $data;
// }

function isDST() {
	global $Logger;
	$theTime = time(); # specific date/time we're checking, in epoch seconds. 

	$tz = new \DateTimeZone('America/Los_Angeles'); 
	$transition = $tz->getTransitions($theTime,$theTime); 

	# only one array should be returned into $transition. Now get the data: 
	$offset = intval($transition[0]['offset']); 
	$abbr = $transition[0]['abbr']; 

	// $Logger->log("isDST offset:$offset, abbr:$abbr");

	return $offset == -28800; // minus 8 hrs
}

// define("IS_DAYLIGHT_SAVINGS", 1);
global $timezone_adjust;
$timezone_adjust = -7;
if (isDst())
	$timezone_adjust -= 1;

global $agentAcronymMakeUppercase;
global $agentAcronymDoubleCheck;

$agentAcronymMakeUppercase = ['crs','mba','gri','cdpe','qsc','mrp','abr','ii','(dr)','(e)','pa','iii','p.a.','a.b.','ab','cpa','csr','dpp','sres','rli','ltg','sfr','cips','rebac','crb','ccim','epro','bpor','sf','csp','sfr.','pc','cai','pro','srs','cne','tahs','rs','cdrs','asr','chms','mre','reos','csn','cbr','ahwd','trc','afh.','gri.','trlp','sps','pllc'];
$agentAcronymDoubleCheck = ['reos','reo'];

function formatName(&$data, $isPerson = false, $isCity = false) {
	if (!isset($data) || empty($data))
		return $data;

	global $agentAcronymMakeUppercase;
	global $agentAcronymDoubleCheck;

	$data = html_entity_decode($data, ENT_QUOTES | ENT_HTML5);
	$data = strtolower(trim($data));
	$data = str_replace('-',' ',$data);
	$data = str_replace(',',' ',$data);
	if (!$isPerson)
		$data = str_replace('&',' ',$data);
	else
		$data = preg_replace('/[0-9]/', '', $data);
	$data = str_replace('  ',' ',$data);
	$data = str_replace("''","'",$data);
	$data = str_replace("`","'",$data);
	$t = explode(' ', $data);
	$data = '';
	$doNotUpperFirst = ['the','for','by','on'];
	if ( !isset($agentAcronymMakeUppercase) ||
		 count($agentAcronymMakeUppercase) == 0)
		$makeUpperCase = ['crs','mba','gri','cdpe','qsc','mrp','abr','ii','(dr)','(e)','pa','iii','p.a.','a.b.','ab','cpa','csr','dpp','sres','rli','ltg','sfr','cips','rebac','crb','ccim','epro','bpor','sf','csp','sfr.','pc','cai','pro','srs','cne','tahs','rs','cdrs','asr','chms','mre','reos','csn','cbr','ahwd','trc','afh.','gri.','trlp'];
	else
		$makeUpperCase = &$agentAcronymMakeUppercase;

	if ( !isset($agentAcronymDoubleCheck) ||
		  count($agentAcronymDoubleCheck) == 0)
		$doubleCheck = ['reos','reo'];
	else
		$doubleCheck = &$agentAcronymDoubleCheck;

	$x = [];
	foreach($t as $str)
		if (!empty($str))
			$x[] = $str;

	$t = $x;
	unset($x);

	foreach ($t as $i=>$str){
		if ($str == ' ' ||
			empty($str))
			continue;
		if ($i > 0) $data .= ' ';
		if ($i < 1 || !in_array($str, $doNotUpperFirst)) {
			if ($isPerson &&
				in_array($str, $makeUpperCase) ) {
				if (!in_array($str, $doubleCheck) ||
					(in_array($str, $doubleCheck) &&
					 count($t) > 1) )
					$str = strtoupper($str);
				else
					$str = ucfirst($str);
			}
			else {
				if ($isCity) {
					if ($i == 0) { // first word
						if ($str == 'st' ||
						 	$str == 'saint')
							$str = 'st.'; // fix St and Saint to be St.
						elseif ( strlen($str) > 3 &&
							 	 strpos($str, 'st.') === 0 ) {// then this must be a concatenated name like St.matthews
							$tmp = explode('.', $str);
							$str = '';
							foreach($tmp as $x) {
								if (strlen($str)) $str .= ' ';
								$str .= $x == 'st' ? 'st.' : $x;
								unset($x);
							}
							unset($tmp);
						}
					}
					else if ($str == 'st' || // after first word
							 $str == 'st.')
						$str = 'saint';

					if ($str == 'islan')
						$str = 'island';
				}
				elseif (!$isPerson && // not isCity and not isPerson, then must be street_address
						 $str == 'st.')
					$str = 'st'; // simplify St. to St
				$str = ucwords($str);
			}
			if ( (substr($str, 0,2) == "Mc" ||
				  substr($str, 0,2) == "O'"||
				  substr($str, 0,2) == "D'") &&
				strlen($str) > 2)
				$str[2] = chr( ord($str[2]) - 32 ); // cap that 3rd letter

			if ( $isPerson &&
				 ($str[0] == '(' ||
				  $str[0] == '"') &&
				  strlen($str) > 2) // then cap 2nd letter
				$str[1] = chr( ord($str[1]) - 32 );
		}
		$data .= trim($str);
		unset($i, $str);
	} unset($t);
	return $data;
}

function fixPhone($phone, $separator = '.') {
	if (!$phone || empty($phone))
		return '';

	$phone = gettype($phone) == 'string' ? $phone : number_format($phone, 0, '.', '');
	$len = strlen($phone);
	$number = $prefix = $area = $extra = '';
	if ($len >= 7) {
		$number = substr($phone, -4);
		$prefix = substr($phone, -7, 3);
		if ($len < 10 && $len > 7)
			$area = substr($phone, 0, $len - 7);
		else {
			$area = substr($phone, -10, 3);
			if ($len > 10)
				$extra = substr(0, $len - 10);
		}
	}
	elseif ($len > 4) {
		$number = substr($phone, -4);
		$prefix = substr($phone, 0, $len-4);
	}
	else
		return $phone;
	$phone = (empty($extra) ? '' : $extra."-").(empty($area) ? '' : $area.$separator).$prefix.$separator.$number;
	return $phone;
}

function nonConformingLocation($loc) {
	if (empty($loc) ||
		is_numeric($loc))
		return false;

	$loc = strtolower($loc);
	if ( ($pos = strpos($loc, "out of")) !== false ||
		 ($pos = strpos($loc, "outside")) !== false ||
		 ($pos = strpos($loc, "other")) !== false ||
		 ($pos = strpos($loc, "unincorporated")) !== false ||
		 ($pos = strpos($loc, "no match")) !== false ||
		 ($pos = strpos($loc, "foreign")) !== false ||
		 ($pos = strpos($loc, "koh chang")) !== false) {
		return true;
	}
	return false;
}
	
function fixCityStr($cityStr) {
	$cityStr = strtolower($cityStr);
	if (($pos = strpos($cityStr, "borough")) !== false)
		$cityStr = str_replace("borough", "", $cityStr);
	else if (($pos = strpos($cityStr, " boro")) !== false &&
		      $pos == strlen($cityStr) - 5)
		$cityStr = str_replace(" boro", "", $cityStr);
	else if (($pos = strpos($cityStr, " twp")) !== false &&
		      $pos == strlen($cityStr) - 4)
		$cityStr = str_replace(" twp", " township", $cityStr);
	else if (($pos = strpos($cityStr, " twp.")) !== false &&
		      $pos == strlen($cityStr) - 5)
		$cityStr = str_replace(" twp.", " township", $cityStr);
	else if (($pos = strpos($cityStr, " co.")) !== false &&
		      $pos == strlen($cityStr) - 4)
		$cityStr = str_replace(" co.", "", $cityStr);
	else if (($pos = strpos($cityStr, " by the sea")) !== false &&
		      $pos == strlen($cityStr) - 11)
		$cityStr = str_replace(" by the sea", "", $cityStr);
	else if (($pos = strpos($cityStr, " hts")) !== false &&
		      $pos == strlen($cityStr) - 4)
		$cityStr = str_replace(" hts", " heights", $cityStr);
	else if (($pos = strpos($cityStr, "the ")) !== false &&
		      $pos === 0)
		$cityStr = str_replace("the ", "", $cityStr);
	else if ( ($pos = strpos($cityStr, "(")) !== false)
		$cityStr = substr($cityStr, 0, $pos);
	else if ( strpos($cityStr, "Mc") === 0 ) // then it's a Mcxxxx city
		$cityStr = substr($cityStr, 0, 2).ucwords( substr($cityStr, 2));

	return $cityStr;
}

function fixCompass($location) {
	$pieces = explode(" ", $location);
	foreach($pieces as $i=>$word) {
		if (strlen($word)==1)
			switch($word) {
				case "E": $pieces[$i] = "East"; break;
				case "W": $pieces[$i] = "West"; break;
				case "N": $pieces[$i] = "North"; break;
				case "S": $pieces[$i] = "South"; break;
			}
	}
	return implode(' ', $pieces);
}

function fix2CharCity($city, $state)
{
	if (strlen($city) == 2) {
	 	switch ($state) {
	 		case 'CA':
				switch($city) {
					case 'SF': return "San Francisco"; break;
					case 'LA': return "Los Angeles"; break;
				}
				break;
			case 'NY':	
				switch($city) {
					case 'NY': return "New York"; break;
				}
				break;
		}
	}
	return $city;
}

function fixCity($city, $state) {
	$city = fix2CharCity($city, $state);
	$city = fixCompass($city);
	switch($state) {
		case 'WY':
			switch($city) {
				case 'Jackson Hole': 
					$city = "Jackson"; 
					break;
			}
			break;

		case 'NM':
			switch($city) {
				case 'Town Of Taos':
				case 'Taos Nm': 
					$city = "Taos";
					break;
				case 'Angl Fire':
					$city = "Angel Fire";
					break;
			}
			break;

		case 'NY':
			switch($city) {
				case 'Poughkeepsie Twp':
					$city = 'Poughkeepsie Township';
					break;
				case 'Poughkeepsie City':
					$city = 'Poughkeepsie';
					break;
				case 'New York Ny':
				case 'New York City':
					$city = 'New York';
					break;
			}
			break;

		case 'PA':
			switch($city) {
				case 'Downtown Pgh':
					$city = 'Pittsburgh';
					break;
			}
			break;

		case 'MT':
			switch($city) {
				case "Elliston/avon":
					$city = 'Avon';
					break;
				case 'Deer Lodge/garrison':
					$city = 'Deer Lodge';
					break;
			}
			break;

		case 'NJ':
			switch($city) {
				case 'Avalon Manor':
					$city = 'Avalon';
					break;
				case 'Hills Twp.':
				case 'Hills Twp':
				case 'Hills':
				case 'Hillsboro':
				case 'Hillsborough Twp':
				case 'Hillsborough Twp.':
					$city = 'Hillsborough';
					break;
				case 'Jc Heights':
				case 'Jc, Heights':
					$city = 'Jersey City, Heights';
					break;
				case 'Jc Greenville':
				case 'Jc, Greenville':
					$city = 'Jersey City, Greenville';
					break;
				case 'Jc Downtown':
				case 'Jc, Downtown':
					$city = 'Jersey City, Downtown';
					break;
				case 'Jc Journal Square':
				case 'Jc, Journal Square':
					$city = 'Jersey City, Journal Square';
					break;
				case 'Jc West Bergen':
				case 'Jc, West Bergen':
					$city = 'Jersey City, West Bergen';
					break;
			}
			break;

		case 'CA':
			switch($city) {
				case 'South Lake Tahoe Ca':
					$city = 'South Lake Tahoe';
					break;
			}
			break;

		case 'WA':
			switch($city) {
				case 'Seattl':
					$city = 'Seattle';
					break;
			}
			break;
	}
	return $city;
}

function fixNY($city, $state) {
	if (empty($state)) {
		if ($city == "NY" ||
			$city == "New York")
			return "NY";
	}
	return $state;
}

// should only be called from updateProfileFromReservation()
function updateReservationAndSeller($reservation, $seller, $needUpdate, $metas, $extraData, $modData, $caller = null) {
	if ($caller) $caller->log("updateReservationAndSeller - called for sellerId:$seller->id for reservation:$reservation->id, needUpdate:$needUpdate");

	$fields = [];
	if ($extraData) {
		// if ($caller) $caller->log("updateReservationAndSeller - entered code for extra data");
		global  $extraReservationData;
		$extraChanged = false;
		foreach($extraReservationData as $extra) {
			if (!empty($extraData->$extra) &&
				(!isset($seller->$extra) ||
				 ($seller->$extra != $extraData->$extra)) )
				$extraChanged = true;
		}

		if ($caller) $caller->log("updateReservationAndSeller - extra data is changed:".($extraChanged ? "yes" : "no"));
		if ($extraChanged) {
			if (empty($modData)) {
				$modData = new \stdClass();
				$modData->action = SELLER_MODIFIED_PROFILE_DATA;
				$modData->first_name = 0;
				$modData->last_name = 0;
				$modData->company = 0;
				$modData->website = 0;
				$modData->phone = 0;
				$modData->mobile = 0;
				$modData->email = 0;
				$needUpdate = true;
			}
			foreach($extraReservationData as $extra) {
				if (!empty($extraData->$extra) &&
					 (!isset($seller->$extra) ||
				 	  ($seller->$extra != $extraData->$extra)) ) {
					$fields[$extra] = $extraData->$extra;
					if (isset($modData->$extra)) {
						$modData->$extra = 1;
						$needUpdate = true;
					}
				}
			}
		}
	}

	if (isset($reservation->phone) &&
		((isset($seller->phone) &&
		 $reservation->phone != $seller->phone) ||
		 !isset($seller->phone)) ) {
		if (empty($modData)) {
			$modData = new \stdClass();
			$modData->action = SELLER_MODIFIED_PROFILE_DATA;
			$modData->first_name = 0;
			$modData->last_name = 0;
			$modData->company = 0;
			$modData->website = 0;
			$modData->phone = 0;
			$modData->mobile = 0;
			$modData->email = 0;
			$needUpdate = true;
		}
		$modData->phone = 1;
		$fields['phone'] = $reservation->phone;
		$needUpdate = true;
	}

	if ($needUpdate) {
		if (isset($modData))
			$metas[] = $modData;
		$fields['meta'] = $metas;
		require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new Sellers;
		$where = !empty($seller->author_id) ? ['author_id'=>$seller->author_id] : ['id'=>$seller->id];
		$Sellers->set([(object)['where'=>$where,
								'fields'=>$fields]]);
		if ($caller) $caller->log("updateReservationAndSeller - updated meta for seller:$seller->id, author_id:$seller->author_id, from reservation:$reservation->id"); //, where:".print_r($where, true).", metas:".print_r($metas, true));
		unset($Sellers);
	}
	else
		if ($caller) $caller->log("updateReservationAndSeller - no need to update seller:$seller->id, author_id:$seller->author_id, from reservation:$reservation->id");

	if ( empty($reservation->seller_id) ||
		 $reservation->seller_id != $seller->id ) {
		$zohoData = null;
		if ($seller->meta) foreach($seller->meta as $meta)
			if ($meta->action == SELLER_ZOHO_LEAD_ID) {
				$zohoData = $meta;
				break;
			}

		$fields = [];
		$fields['seller_id'] = $seller->id;
		if ($zohoData && $reservation->lead_id != $zohoData->lead_id) {
			$fields['lead_id'] = $zohoData->lead_id;
			$reservation->lead_id = $zohoData->lead_id;
		}
		require_once(__DIR__.'/../_classes/Reservations.class.php'); $Reservations = new Reservations;
		$Reservations->set([(object)['where'=>['id'=>$reservation->id],
									 'fields'=>$fields]]);
		unset($Reservations, $fields);
		$reservation->seller_id = $seller->id;
		if (!empty($reservation->lead_id)) {
			require_once(__DIR__.'/../_classes/Zoho.class.php'); $Zoho = new Zoho;
			$Zoho->processOneZohoCrm(false, $reservation, $seller);
			unset($Zoho);
		}
		else
			if ($caller) $caller->log("updateReservationAndSeller - cannot update sellerID for reservation:$reservation->id, LEADID is empty");
	}
}

// reservation data update to seller
function updateProfileFromReservation($reservation, $seller, $caller = null) {
	if (empty($reservation->meta)) {
		if ($caller) $caller->log("updateProfileFromReservation - meta is empty for reservation:$reservation->id");
		return false;
	}

	if ($reservation->seller_id != $seller->id) {
		if ($caller) $caller->log("updateProfileFromReservation - reservation seller_id:$reservation->seller_id not same as sellerId:$seller->id");
		return false;
	}

	// if (!empty($seller->author_id)) {
	// 	if ($caller) $caller->log("updateProfileFromReservation - sellerId:$seller->id is already a registered agent, not updating from reservation:$reservation->id");
	// 	return false;
	// }

	$lifestyles = null;
	$resvBre = null;
	$resvProfileData = null;
	$resvExtraSellerData = null;
	$amOrder = null;
	$amProfile = null;
	$bre = null;
	$profileData = null;
	$modData = null;
	$nicknameMeta = null;
	$needUpdate = false;

	foreach($reservation->meta as $meta) {
		$meta = (object)$meta;
		if ($lifestyles == null && // just the first one...
			$meta->action == ORDER_AGENT_MATCH) {
			$lifestyles = $meta->item;
		}
		elseif ($meta->action == SELLER_BRE)
			$resvBre = $meta;
		elseif ($meta->action == SELLER_PROFILE_DATA)
			$resvProfileData = $meta;
		elseif ($meta->action == RESERVATION_EXTRA_SELLER_DATA)
			$resvExtraSellerData = $meta;
	}

	// if ($caller) $caller->log("updateProfileFromReservation - reservation:$reservation->id with meta:".print_r($reservation->meta, true));

	// if (empty($lifestyles) &&
	// 	$caller) {
	// 	$caller->log("updateProfileFromReservation - no lifestyles for reservation:$reservation->id, portal:".(!empty($reservation->portal) ? $reservation->portal : "N/A"));
	// 	return false;
	// }

	if ($caller) $caller->log("updateProfileFromReservation - processing sellerId:$seller->id with reservation:$reservation->id");

	$metas = [];
	if (!empty($seller->meta)) foreach($seller->meta as $meta) {
		if ($meta->action == SELLER_AGENT_ORDER) {
			$amOrder = $meta;
		}
		elseif ($meta->action == SELLER_AGENT_MATCH_DATA) {
			$amProfile = $meta;
		}
		elseif ($meta->action == SELLER_BRE)
			$bre = $meta;
		elseif ($meta->action == SELLER_PROFILE_DATA)
			$profileData = $meta;
		elseif ($meta->action == SELLER_MODIFIED_PROFILE_DATA) {
			$modData = $meta;
		}
		elseif ($meta->action == SELLER_NICKNAME)
			$nicknameMeta = $meta;
		else
			$metas[] = $meta;
	}

	if (isset($reservation->portal) &&
		!empty($reservation->portal) &&
		($seller->flags & SELLER_IS_PREMIUM_LEVEL_1) == 0) { // then reservation has portal defined but the seller is not a portal agent yet
		if ($nicknameMeta) { // check if still the same
			if ($nicknameMeta->nickname != $reservation->portal) {
				if ($caller) $caller->log("updateProfileFromReservation - reservation portal:$reservation->portal has changed from currently recorded portal:$nicknameMeta->nickname");
				$nicknameMeta->nickname = $reservation->portal;
				$needUpdate = true;
			}
		}
		else { // make one
			$nicknameMeta = new \stdClass();
			$nicknameMeta->action = SELLER_NICKNAME;
			$nicknameMeta->nickname = $reservation->portal;
			$needUpdate = true;
		}
	}

	if ($seller->flags & SELLER_IS_PREMIUM_LEVEL_2) {
		if ($caller) $caller->log("updateProfileFromReservation - sellerId:$seller->id is already a Lifestyle Agent, not updating from reservation");
		if ($needUpdate) {
			if ($amProfile)
				$metas[] = $amProfile;
			if ($amOrder)
				$metas[] = $amOrder;
			if ($bre)
				$metas[] = $bre;
			if ($profileData)
				$metas[] = $profileData;
			// if ($modData)
			// 	$metas[] = $modData;
			if ($nicknameMeta)
				$metas[] = $nicknameMeta;
			if ($caller) $caller->log("updateProfileFromReservation - sellerId:$seller->id needs it's nickname updated to $reservation->portal");
			updateReservationAndSeller($reservation, $seller, $needUpdate, $metas, $resvExtraSellerData, $modData, $caller);
		}
		return false;
	}

	if (!empty($resvBre)) {
		if (!empty($bre)) {
			$updateBre = false;
			if ($bre->BRE != $resvBre->BRE ||
				$bre->state != $resvBre->state) {
				$updateBre = true;
			}
			if ($bre->userMode > $resvBre->userMode) { // then reservation has a higher order userMode, 3 = organic, 2 = invite, 1 = listhub
				$updateBre = true;
			}
			if ($bre->needVerify != $resvBre->needVerify &&
				$bre->needVerify) { // then reservation side says we don't need to verify anymore
				$updateBre = true;
			}
			if ($updateBre) {
				$bre = $resvBre;
				$needUpdate = true;
			}
			if ($caller) $caller->log("updateProfileFromReservation - updated BRE data from reservation");
		}
		else {
			$bre = $resvBre;
			$needUpdate = true;
			if ($caller) $caller->log("updateProfileFromReservation - assigning BRE data from reservation");
		}
	} 
	else
		if ($caller) $caller->log("updateProfileFromReservation - no BRE data from reservation");
	// else if empty, then $bre goes back as it is

	if (!empty($resvProfileData)) {
		if (!empty($profileData)) {
			if (empty($modData)) {
				$modData = new \stdClass();
				$modData->action = SELLER_MODIFIED_PROFILE_DATA;
				$modData->first_name = 0;
				$modData->last_name = 0;
				$modData->company = 0;
				$modData->website = 0;
				$modData->phone = 0;
				$modData->mobile = 0;
				$modData->email = 0;
				$needUpdate = true;
			}
			$needUpdatePD = false;
			if ($resvProfileData->contact_email != $profileData->contact_email) {
				$profileData->contact_email = $resvProfileData->contact_email;
				$needUpdate = $needUpdatePD = true;								
			}
			if (  empty($modData->first_name) &&
				  isset($resvProfileData->first_name) && isset($profileData->first_name) &&
				  $resvProfileData->first_name != $profileData->first_name &&
				  strlen($resvProfileData->first_name)) {
				$profileData->first_name = $resvProfileData->first_name;
				$modData->first_name = 1;
				$needUpdate = $needUpdatePD = true;
			}
			if (  empty($modData->last_name) &&
				  isset($resvProfileData->last_name) && isset($profileData->last_name) &&
				  $resvProfileData->last_name != $profileData->last_name &&
				  strlen($resvProfileData->last_name)) {
				$profileData->last_name = $resvProfileData->last_name;
				$modData->last_name = 1;
				$needUpdate = $needUpdatePD = true;
			}
			if ($resvProfileData->year != $profileData->year) {
				$profileData->year = $resvProfileData->year;
				$needUpdate = $needUpdatePD = true;								
			}
			if ($resvProfileData->inArea != $profileData->inArea) {
				$profileData->inArea = $resvProfileData->inArea;
				$needUpdate = $needUpdatePD = true;								
			}
			if ($resvProfileData->sold != $profileData->sold) {
				$profileData->sold = $resvProfileData->sold;
				$needUpdate = $needUpdatePD = true;								
			}
			if ($caller && $needUpdatePD) $caller->log("updateProfileFromReservation - updated ProfileData from reservation");
		}
		else {
			$profileData = $resvProfileData;
			$needUpdate = true;	
			if (empty($modData)) {
				$modData = new \stdClass();
				$modData->action = SELLER_MODIFIED_PROFILE_DATA;
				$modData->first_name = 0;
				$modData->last_name = 0;
				$modData->company = 0;
				$modData->website = 0;
				$modData->phone = 0;
				$modData->mobile = 0;
				$modData->email = 0;
				$needUpdate = true;
			}
			if (isset($resvProfileData->first_name) && 
				!empty($resvProfileData->first_name))
				$modData->first_name = 1;
			if (isset($resvProfileData->last_name) && 
				!empty($resvProfileData->last_name))
				$modData->last_name = 1;
			if ($caller) $caller->log("updateProfileFromReservation - assigning ProfileData from reservation");
		}
	}
	else
		if ($caller) $caller->log("updateProfileFromReservation - no ProfileData from reservation");

	// update AM Profile data with reservation data
	if ($lifestyles) {
		if (!$amProfile) { // make one!
			global  $agent_match_specialties;
			global  $agent_match_lifestyles;
			require_once(__DIR__.'/../_classes/Tags.class.php'); $Tags = new Tags();
			$specialty = $Tags->get((object)['what'=>['id','tag','type'], 'where'=>['id'=>$agent_match_specialties]]);
			$lifestyle = $Tags->get((object)['what'=>['id','tag','type'], 'where'=>['id'=>$agent_match_lifestyles]]);
			foreach($specialty as &$tag) {
				$tag->used = 0;
				$tag->desc = '';
			}
			foreach($lifestyle as &$tag) {
				$tag->used = 0;
				$tag->desc = '';
			}
			$amProfile = new \stdClass();
			$amProfile->action = SELLER_AGENT_MATCH_DATA;
			$amProfile->specialties = $specialty;
			$amProfile->lifestyles = $lifestyle;
			$needUpdate = true;
		}
		// reset tags->used to 0 so that they are not AgentMatch lifestyles anymore
		foreach($amProfile->specialties as &$tag)
			$tag->used = 0;
		foreach($amProfile->lifestyles as &$tag)
			$tag->used = 0;

		foreach($lifestyles as $item) {
			$item = (object)$item;				
			$found = false;
			foreach($amProfile->specialties as &$tag) {
				$tag = (object)$tag;
				if (intval($tag->id) == intval($item->specialty)) {
					// $profileDesc = ereg_replace('/\\/g',"", $tag->desc);
					// $orderDesc = ereg_replace('/\\/g',"", $item->desc);
					if (isset($item->desc)) {
						$profileDesc = removeslashes($tag->desc);
						$orderDesc = removeslashes($item->desc);
						if (strcmp($profileDesc, $orderDesc) != 0) {
							$tag->desc = ($orderDesc);
							$needUpdate = true;												
						}
					}
					if (intval($tag->used) != 1) {
						$tag->used = 1;
						$needUpdate = true;
					}
					$found = true;
				}
				unset($tag);
				if ($found) break;
			}
			if (!$found) foreach($amProfile->lifestyles as &$tag) {
				$tag = (object)$tag;
				if (intval($tag->id) == intval($item->specialty)) {
					// $profileDesc = ereg_replace('/\\/g',"", $tag->desc);
					// $orderDesc = ereg_replace('/\\/g',"", $item->desc);
					if (isset($item->desc)) {
						$profileDesc = removeslashes($tag->desc);
						$orderDesc = removeslashes($item->desc);
						if (strcmp($profileDesc, $orderDesc) != 0) {
							$tag->desc = ($orderDesc);
							$needUpdate = true;												
						}
					}
					if (intval($tag->used) != 1) {
						$tag->used = 1;
						$needUpdate = true;
					}
					$found = true;
				}
				unset($tag);
				if ($found) break;
			}
		}
	}
	else
		if ($caller) $caller->log("updateProfileFromReservation - not updating amProfile from reservation");

	// if incoming reservation is different from what's in database, replace it.
	if ($lifestyles) {
		if ($amOrder &&
			count($amOrder->item)) {
			foreach($amOrder->item as &$existing) {
				$existing = (object)$existing;
				if ($existing->type != ORDER_AGENT_MATCH) {
					unset($existing);
					continue;
				}
				$gotIt = false;
				foreach($lifestyles as $item) {
					$item = (object)$item;
					if ($existing->location == $item->location &&
						$existing->specialty == $item->specialty) {
						$gotIt = true;
						if (isset($item->desc) &&
							isset($existing->desc) &&
							strcmp(removeslashes($existing->desc), removeslashes($item->desc)) != 0) { 
							$existing->desc = removeslashes($item->desc);
							$needUpdate = true;
						}
					}
					unset($item);
					if ($gotIt) break;
				}
				unset($existing);
			}
		}
		else {
			if ($caller) $caller->log("updateProfileFromReservation - creating new SELLER_AGENT_ORDER for seller:$seller->id, author_id:$seller->author_id");
			$amOrder = new \stdClass();
			$amOrder->action = SELLER_AGENT_ORDER;
			$amOrder->order_id_last_purchased = 0;
			$amOrder->order_id_last_cancelled = 0;
			$amOrder->agreed_to_terms = 0;
			$amOrder->item = [];
			foreach($lifestyles as $item) {
				$item = (object)$item;
				$newItem = new \stdClass();
				$newItem->mode = ORDER_BUYING;
				$newItem->type = ORDER_AGENT_MATCH;
				$newItem->desc = isset($item->desc) && !empty($item->desc) ? removeslashes($item->desc) : '';
				$newItem->location = $item->location;
				$newItem->locationStr = removeslashes($item->locationStr);
				$newItem->specialty = $item->specialty;
				$newItem->specialtyStr = $item->specialtyStr;
				$newItem->cart_item_key = 0;
				$newItem->order_id = 0;
				$amOrder->item[] = $newItem;
				unset($item);
			}
			$needUpdate = true;
		}
	}

	if ($amProfile)
		$metas[] = $amProfile;
	if ($amOrder)
		$metas[] = $amOrder;
	if ($bre)
		$metas[] = $bre;
	if ($profileData)
		$metas[] = $profileData;
	// if ($modData)
	// 	$metas[] = $modData;
	if ($nicknameMeta)
		$metas[] = $nicknameMeta;

	if ($caller) $caller->log("updateProfileFromReservation - calling updateReservationAndSeller with extra:".($resvExtraSellerData ? "yes" : "N/A"));
	updateReservationAndSeller($reservation, $seller, $needUpdate, $metas, $resvExtraSellerData, $modData, $caller);

	return true;
}

function is_true($val, $return_null=false){
    $boolval = ( is_string($val) ? filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool) $val );
    return ( $boolval===null && !$return_null ? false : $boolval );
}

function urlExist($url)
{
	if ( substr($url, 0, 4 ) != 'http' )
		$url = "http://".$url;

    $handle   = curl_init($url);
    if (false === $handle)
    	return false;
    curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($handle, CURLOPT_HEADER, false);
    curl_setopt($handle, CURLOPT_FAILONERROR, true);  // this works
    curl_setopt($handle, CURLOPT_HTTPHEADER, Array("User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.15) Gecko/20080623 Firefox/2.0.0.15") ); // request as if Firefox
    curl_setopt($handle, CURLOPT_NOBODY, true);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, false);
    $connectable = curl_exec($handle);
    curl_close($handle);
  
    return $connectable;
}

function validYouTubeURL($url) {
	$videoId = ($pos = strpos($url, "v=")) !== false ? substr($url, $pos+2) : $url;
	$videoId = ($pos = strpos($videoId, "?")) !== false ? substr($videoId, 0, $pos) : $videoId;
	$ut = "https://www.googleapis.com/youtube/v3/videos?id=$videoId&part=id&key=AIzaSyCftrncjafmT1NCabvd0R-fHj-3Tqnk-V8";

	$handle   = curl_init($ut);
    if (false === $handle)
    	return false;
    // curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($handle, CURLOPT_HEADER, false);
    // curl_setopt($handle, CURLOPT_FAILONERROR, true);  // this works
    // curl_setopt($handle, CURLOPT_HTTPHEADER, Array("User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.15) Gecko/20080623 Firefox/2.0.0.15") ); // request as if Firefox
    // curl_setopt($handle, CURLOPT_NOBODY, true);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    $connectable = json_decode(curl_exec($handle));

    $haveIt = $connectable->pageInfo->totalResults;

    return $haveIt;
}

function ICErrorToStr($error) {
	switch($error) {
		case IC_WITHIN_30: return "WITHIN_30";
		case IC_WITHIN_14: return "WITHIN_14";
		case IC_WITHIN_7: return "WITHIN_7";
		case IC_WITHIN_1: return "WITHIN_1";
		case IC_EXPIRED: return "EXPIRED";
	}
}

function removeslashes($string)
{
	if (empty($string))
		return $string;
    $string=implode("",explode("\\",$string));
    return stripslashes(trim($string));
}

function getBrowser() 
{ 
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";
    $ub = 'Unknown';

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    
    // Next get the name of the useragent yes seperately and for good reason
    if( (preg_match('/MSIE/i',$u_agent) ||
    	 preg_match('/UCH-IE1/i',$u_agent) ||
    	 preg_match('/NP10/i',$u_agent) ||
    	 preg_match('/rv:11/i',$u_agent)) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
        if ( preg_match('/rv:11/i',$u_agent) )
        	$ub = "MSIE11";
    } 
    elseif(preg_match('/Slurp/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Slurp'; 
        $ub = "YahooBot"; 
    } 
    elseif(preg_match('/facebookexternalhit/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Facebook'; 
        $ub = "FacebookBot"; 
    } 
    elseif(preg_match('/Edge/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Microsoft Edge'; 
        $ub = "Edge"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent) && preg_match('/Mozilla/i',$u_agent) && preg_match('/FlipboardProxy/i',$u_agent)) 
    { 
        $bname = 'FlipboardProxy Firefox'; 
        $ub = "Flipboard"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent) && preg_match('/Mozilla/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Windows/i',$u_agent) && !preg_match('/Phone/i',$u_agent)) 
    { 
    	if(preg_match('/LCJB/i',$u_agent)) {
    		$bname = "Lenovo";
    		$ub = "MSIE IdeaPad";
    	}
    	elseif(preg_match('/MASAJS/i',$u_agent)) {
    		$bname = "Sony";
    		$ub = "MSIE Sony Windows";
    	}
    	elseif( preg_match('/MDDCJS/i',$u_agent) ||
    			preg_match('/MDDRJS/i',$u_agent) ||
    			preg_match('/MDDSJS/i',$u_agent)) {
    		$bname = "Dell";
    		$ub = "MSIE Dell Windows";
    	}
    	elseif( preg_match('/IEMobile/i',$u_agent) ||
    			preg_match('/WPDesktop/i',$u_agent) ||
    			preg_match('/XBLWP7/i',$u_agent)) {
    		$bname = "IEMobile";
    		$ub = "IEMobile";
    	}
    	elseif(preg_match('/Chrome/i',$u_agent)) 
	    { 
	        $bname = 'Google Chrome'; 
	        $ub = "Chrome"; 
	    } 
    	else {
	        $bname = 'Windows'; 
	        $ub = "MSIE Windows"; 
	    }
    } 
    elseif(preg_match('/Trident/i',$u_agent)) 
    { 
        $bname = 'Trident'; 
        $ub = "Trident"; 
    } 
    elseif(preg_match('/Android/i',$u_agent)) 
    { 
        $bname = 'Android'; 
        $ub = "Android"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/iPad/i',$u_agent)) 
    { 
        $bname = 'iPad'; 
        $ub = "iPad"; 
    } 
    elseif(preg_match('/iPhone/i',$u_agent)) 
    { 
        $bname = 'iPhone'; 
        $ub = "iPhone"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
    elseif(preg_match('/Googlebot/i',$u_agent)) 
    { 
        $bname = 'Googlebot'; 
        $ub = "Googlebot"; 
    }
    elseif(preg_match('/Twitterbot/i',$u_agent)) 
    { 
        $bname = 'Twitterbot'; 
        $ub = "Twitterbot"; 
    }
    elseif(preg_match('/LinkedInBot/i',$u_agent)) 
    { 
        $bname = 'LinkedInBot'; 
        $ub = "LinkedInBot"; 
    }
    elseif(preg_match('/Embedly/i',$u_agent)) 
    { 
        $bname = 'EmbedlyBot'; 
        $ub = "EmbedlyBot"; 
    }
    elseif(preg_match('/X11/i',$u_agent)) 
    { 
        $bname = 'Linux'; 
        $ub = "Linux"; 
    }
    
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= isset($matches['version'][0]) ? $matches['version'][0] : "N/A";
        }
        else {
            $version= isset($matches['version'][1]) ? $matches['version'][1] : "N/A";
        }
    }
    else {
        $version= $matches['version'][0];
    }
    
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'shortname' => $ub,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
} 

function stringArrayIsSame($ar1, $ar2) { // actaully ar1 and ar2 are like "1,2,3,4" and "2,3,4,5", comma separated numbers in a string form
	$list1 = explode(',', $ar1);
	$list2 = explode(',', $ar2);
	if (count($list1) != count($list2))
		return false;

	usort($list1, function($a, $b) {
		return intval($a) - intval($b);
	});

	usort($list2, function($a, $b) {
		return intval($a) - intval($b);
	});

	foreach($list1 as $x=>$v)
		if ($v != $list2[$x])
			return false;

	return true;
}

function readDashCardFile($xmlFile) {
	$dashboardCards = [];
	// require_once(__DIR__.'/Logger.class.php'); $Logger = new Logger(1, 'utility');
	global $Logger;
	$Logger->log("readDashCardFile - xmlFile:$xmlFile");
	$haveFile = file_exists($xmlFile);
	$handle = $haveFile ? fopen($xmlFile, "r") : null;
	if ($handle) {
	    while (($line = fgets($handle)) !== false) {
	    	// $s->log($line);
	        if (trim($line) == '<cards>')
	        	continue;
	        if (trim($line) == '<card>') {
	        	$buffer = '';
	        	while (($line = fgets($handle)) !== false) {
	        		if (trim($line) == '</card>')
	        			break;
	        		$buffer .= trim($line);
	        	}
	        	$buffer = str_replace('%card_title_icon_src%', get_template_directory_uri(), $buffer);
	        	$Logger->log($buffer);
	        	$dashboardCards[] = $buffer;
	        }
	    }
	    fclose($handle);
	} 
	else {
	    $Logger->log("cannot process xml file:$xmlFile, haveFile:".($haveFile ? 'yes' : 'no'));
	} 

	return $dashboardCards;
}

function outputDynamicHtml($htmlList) {
	foreach($htmlList as $id=>$packet) {
		if ($id == 'agentID' ||
			$id == 'repeating')
			continue;
		$packet = (object)$packet; // just to be sure
		if ( isset($packet->html) && !empty($packet->html))
			$packet->html = html_entity_decode($packet->html); // convert &quot; or other html special characters to ASCII representation
		else
			$packet->html = '';

		if ( isset($packet->style) && !empty($packet->style))
			$packet->style = html_entity_decode($packet->style); // convert &quot; or other html special characters to ASCII representation
		else
			$packet->style = '';
		switch($packet->type) {
			case 'span':
				echo '<'.$packet->type.' id="'.$packet->id.'" '.(!empty($packet->style) ? 'style="'.$packet->style.'"' : '').'>'.$packet->html.'</'.$packet->type.'>';
				break;
			case 'img':
				if (!isset($packet->src) || empty($packet->src))
					continue;
				$packet->src = html_entity_decode($packet->src);
				if ( strpos($packet->src, "%host%") !== false)
					$packet->src = str_replace("%host%", get_template_directory_uri(), $packet->src);
				echo '<'.$packet->type.' id="'.$packet->id.'" src="'.$packet->src.'" '.(!empty($packet->style) ? 'style="'.$packet->style.'"' : '').'>'.$packet->html.'</'.$packet->type.'>';
					break;

			case 'a':
				if ( isset($packet->href) && !empty($packet->href))
					$packet->href = html_entity_decode($packet->href);
				else
					$packet->href = 'javascript:;';
				echo '<'.$packet->type.' id="'.$packet->id.'" href="'.$packet->href.'" '.(!empty($packet->style) ? 'style="'.$packet->style.'"' : '').'>'.$packet->html.'</'.$packet->type.'>';
				break;
			case 'input':
				if ( isset($packet->placeholder) && !empty($packet->placeholder))
					$packet->placeholder = html_entity_decode($packet->placeholder);
				else
					$packet->placeholder = '';
				echo '<'.$packet->type.' id="'.$packet->id.'" placeholder="'.$packet->placeholder.'" '.(!empty($packet->style) ? 'style="'.$packet->style.'"' : '').'>'.$packet->html.'</'.$packet->type.'>';
				break;
			case 'button':
				if ( isset($packet->onclick) && !empty($packet->onclick))
					$packet->onclick = html_entity_decode($packet->onclick);
				else
					$packet->onclick = '';
				echo '<'.$packet->type.' id="'.$packet->id.'" '.(!empty($packet->onclick) ? 'onclick="'.$packet->onclick.'"' : '').' '.(!empty($packet->style) ? 'style="'.$packet->style.'"' : '').'>'.$packet->html.'</'.$packet->type.'>';
				break;
		}
	}
}

function isJson($string) {
 	json_decode($string);
 	return (json_last_error() == JSON_ERROR_NONE);
}

global  $agent_match_specialties;
global  $agent_match_lifestyles;
$agent_match_specialties = array( 8, 9, 14, 20, 23, 59, 65, 69, 70,  86, 144, 147, 168);
$agent_match_lifestyles = array( 6, 10, 18, 25, 40, 42, 44, 49, 66, 72, 123, 143, 145, 163, 164, 167, 169, 172, 196);

global  $tag_city;
global  $tag_listing;
// for finding lifestyled agents and getting agent profile setup (subset of complete city tags)
$tag_city = array(6 ,10, 18, 25, 40, 42, 49, 66, 72, 123,  143, 145, 163, 164, 167, 169, 172, 196);
$tag_listing = array(8, 9, 14, 20, 23, 44, 59, 65, 69, 76, 86, 144, 147, 168);

global $all_tag_city;
$all_tag_city = array(1, 4, 6 ,10, 16, 17, 18, 19, 24, 25, 27, 29, 31, 32, 33, 34, 40, 42, 49, 49, 66, 72, 79, 87, 104, 105, 114, 115, 116, 123, 142, 143, 145, 159, 163, 164, 165, 166, 167, 169, 172, 196);

global $tag_listing_for_sales_admin;
$tag_listing_for_sales_admin = array(8, 9, 14, 20, 23, 39, 45, 51, 62, 80, 81, 108, 109, 110, 111, 112, 113, 144, 147, 150, 155, 156, 157, 158, 161, 171);

global  $extraReservationData;
$extraReservationData = ['company', 'website', 'mobile', 'street_address', 'city', 'state', 'zip', 'country', 'about'];

global $siteMapKeys;
// for city, run rawurldecode() to convert to readable ascii
$siteMapKeys = [
		'wine tasting',
		'wine culture',
		'golf',
		'skiing',
		'ocean boating',
		'surfing',
		'beach',
		'equestrian',
		'casinos',
		'freshwater boating',
		'lake boating',
		'fishing',
		'hunting',
		'hiking',
		'biking',
		'metropolitan',
		'scuba',
		'nascar',
		'road_racing',
		'kayak',
		'inline skating',
		'ice skating',
		'mlb baseball',
		'fifa soccer',
		'nhl hockey',
		'nfl football',
		'nba basketball'
	];

define( 'SUPPORT_EMAIL', 'support@lifestyledlistings.com');
define( 'SALES_EMAIL', 'sales@lifestyledlistings.com');
// define( 'SUPPORT_EMAIL', 'tomt@lifestyledlistings.com');
define('HELP_CENTER', "http://lifestyledlistings-help.com");

define("QUANDL_API_KEY", "Zzo8mR4e1Y-Q1a3uKEiz");
define("MONTH_SECS", 3600*24*30);
define("DEFAULT_MEDIAN_HOME", 360000);
define("DEFAULT_MEDIAN_CONDO", 180000);
define("DEFAULT_MINIMUM_PRICE", 500000);
define("DEFAULT_MINIMUM_PRICE_FROM_LISTHUB", 70000);
define("DEFAULT_ROCK_BOTTOM_PRICE", 30000);
define("DEFAULT_MINIMUM_LIFESTYLE_AGENT_DESC", 300);
define("DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM", 999999999);
define("DEFAULT_INITIAL_CITY_GROUP", 6);
define("QUIZ_PAGE_PER", 200);
define("QUIZ_INIT_MAX_LISTINGS_PER_CITY", 5);
define("MAX_SESSION_OWN_QUIZ", 10);

define( 'EMAIL_BANNER_COUNT', 6);

define( 'NON_AGENT', 0);
define( 'AGENT_LISTHUB', 1);
define( 'AGENT_INVITE', 2);
define( 'AGENT_ORGANIC', 3);

define("TRIM_IMAGE", 0);
define("BLACKOUT_IMAGE", 1);
define("REGEN_IMAGE", 2);
define("CUSTOM_IMAGE_TASK", 3);
define("JPG_QUALITY_AT_95", 1);
define("JPG_QUALITY_AT_80", 8);
define('BASE_IMAGE_PROCESSED_VALUE', 8); // 8 w/jpg at 80%, down from 95% when in 5 (skipped 6 because when doing LR, image->processed was getting incremented)
define('BASE_CONSIDERED_PRICE_MULTIPLER', 0.80);
define('BASE_CONSIDERED_PRICE_MULTIPLER_CONDO', 0.90);
define('MINIMUM_TAG_COUNT', 3);

// error constants
define("BASIC_ERROR", 1);
define("TOOCHEAP", (1 << 0));	// 1
define("LOWTAGS", (1 << 1));	// 2
define("NOIMAGE", (1 << 2));	// 4
define("NOBEDBATH", (1 << 3));	// 8
define("COMMERCIAL", (1 << 4));	// 16
define("LACK_DESC", (1 << 5));	// 32
define("RENTAL", (1 << 6));		// 64

define('AH_INACTIVE', 0);
define('AH_ACTIVE', 1);
define('AH_WAITING', 2);
define('AH_REJECTED', 3);
define('AH_TOOCHEAP', 4);
define('AH_NEVER', 5);
define('AH_AVERAGE', 6);
define('AH_RENTAL', 7);

// meta data type constants
// NOTE: if any new meta id is added, see if _ajax.php's updateSellers():$metaIds needs to be updated accordingly!
define( 'SELLER_IMPROVEMENT_TASKS', (1 << 0)); // 1- their things to-do to improve listings
define( 'SELLER_KEY', (1 << 1)); // 2- their encrypted password
define( 'SELLER_BRE', (1 << 2)); // 4- their license number, state, userMode and whether their BRE needs to be verified
define( 'SELLER_INVITE_CODE', (1 << 3)); // 8- their assigned invitation code tied to their name and whether they own it or was given to them
define( 'SELLER_VISITATIONS', (1 << 4)); // 16- list of users that has them as their Premier Agent
define( 'SELLER_PROFILE_DATA', (1 << 5)); // 32- from their profile page, data in their Sellers row
define( 'SELLER_AGENT_MATCH_DATA', (1 << 6)); // 64- agent match data
define( 'SELLER_MODIFIED_PROFILE_DATA', (1 << 7)); // 128- list of items the seller had edited on their own and should not be overwritten by Listhub feed
define( 'SELLER_NICKNAME', (1 << 8)); // 256- save this portal name to use when payment is completed
// NOTE: if any new meta id is added, see if _ajax.php's updateSellers():$metaIds needs to be updated accordingly!
define( 'SELLER_NEW_ORDER', (1 << 9)); // 512- added something into the cart
define( 'SELLER_AGENT_ORDER', (1 << 10)); // 1024- order list
define( 'SELLER_AGENT_MATCH_STATUS', (1 << 11)); // 2048- stats kept for agent match, how many times agent matched with city and/or lifestyle
define( 'SELLER_NOTES_FROM_RESERVATION', (1 << 12)); // 4096
define( 'SELLER_ALTERNATE_LISTHUB_DATA', (1 << 13)); // 8192
define( 'SELLER_ZOHO_LEAD_ID', (1 << 14)); // 16384
define( 'SELLER_PORTAL_MESSAGE', (1 << 15)); // 32768
define( 'SELLER_COMPLETED_CARDS', (1 << 16)); // 65536
// NOTE: if any new meta id is added, see if _ajax.php's updateSellers():$metaIds needs to be updated accordingly!

// seller flags
define( 'SELLER_AGREED_TAG_TERMS', (1 << 0));//1
define( 'SELLER_AGENT_MATCH_WAITING', (1 << 1));//2
define( 'SELLER_UNUSED_2', (1 << 2));//4
define( 'SELLER_IS_PREMIUM_LEVEL_1',  (1 << 3)); // 8 Portal feature
define( 'SELLER_PURCHASED_PORTAL',    (1 << 3)); // 8 Portal feature
define( 'SELLER_IS_PREMIUM_LEVEL_2',  (1 << 4)); // 16 Lifestyle feature
define( 'SELLER_PURCHASED_LIFESTYLE', (1 << 4)); // 16 Lifestyle feature
define( 'SELLER_IS_LIFETIME', (1 << 5));        // 32 never expires (for a select few)
define( 'SELLER_INACTIVE_EXCLUSION_AREA', (1 << 6)); // 64 prevent local agent match agents to show up when via portal
define( 'SELLER_NEEDS_VERIFICATION_OF_BRE', (1 << 7)); // 128
define( 'SELLER_LIFESTYLE_FROM_INVITE', (1 << 8)); // 256
define( 'SELLER_NEEDS_BASIC_INFO', (1 << 9)); // 512
define( 'SELLER_HAS_ALTERNATE_LISTHUB_DATA', (1 << 10)); // 1024
define( 'SELLER_HAS_OPTED_IN', (1 << 11)); // 2048
define( 'SELLER_INFORM_PORTAL_USER_REENTRY', (1 << 12)); // 4096

// listing flags
define( 'LISTING_PERMIT_ADDRESS', (1 << 0)); // 1
define( 'LISTING_DID_TAG_SPACE_CHECK', (1 << 1)); // 2
define( 'LISTING_GATHERING_POI', (1 << 2)); // 4
// listing meta type constants
define( 'LISTING_MODIFIED_DATA', (1 << 0)); // list of items the seller had modified in their seller's page that shouldn't be overwritten by Listhub feed
define( 'LISTING_UNUSED_META_1', (1 << 1));
define( 'LISTING_UNUSED_META_2', (1 << 2));
define( 'LISTING_UNUSED_META_3', (1 << 3));
define( 'LISTING_VISITATIONS', (1 << 4));  // 16 - list of viewers, same construct as SELLER_VISITATIONS
define( 'LISTING_IS_CONDO', (1 << 5)); // 32
define( 'LISTING_ADDRESS_HAS_UNIT_NUMBER', (1 << 6)); // 64

// session meta tags
define('SESSION_PROFILE_NAME', 'SESSION_PROFILE_NAME');
define('SESSION_PREMIER_AGENT', 'SESSION_PREMIER_AGENT');
define('SESSION_PORTAL_USER', 'SESSION_PORTAL_USER');

//session types
define( 'SESSION_PORTAL_ENTRY', (1 << 0));
define( 'SESSION_RETIRED', (1 << 1)); // due to quiz activty getting too large 
define( 'SESSION_LOCKED', (1 << 2)); // prevent race condition when mutiple entry via portal agent into same IP

// Options table stuff
define( 'INVITATION_LIST', 'invitationList');

// feedback type
define( 'MESSAGE_FEEDBACK', 1 );
define( 'MESSAGE_EMAIL_AGENT', 2);
define( 'MESSAGE_EMAIL_LISTING_AGENT', 3);
define( 'MESSAGE_REPLY_TO_USER', 4);
define( 'MESSAGE_REPLY_TO_AGENT', 5);
define( 'MESSAGE_EMAIL_AGENT_VIA_PORTAL', 6);
define( 'MESSAGE_SELLER', 7); 
define( 'MESSAGE_CONTACT_US', 8);
define( 'MESSAGE_EMAIL_FROM_LLC', 9); // this is used for tagging stuff we send as promo, improve it, etc
define( 'MESSAGE_EMAIL_CLIENT_AS_AGENT', 10);
define( 'MESSAGE_EMAIL_PORTAL_AGENT', 11);
define( 'MESSAGE_EMAIL_LISTING_OFFICE', 12); // agent's office email

// EmailTracker meta action type
define( 'ET_CONTENT', (1 << 0));
define( 'ET_REPLY', (1 << 1));
define( 'ET_LISTING_VIEWED', (1 << 2));
define( 'ET_AGENT_RESPONSE', (1 << 3));


// invitation code flags, IC = InviationCode
define( 'IC_USED', (1 << 0));
define( 'IC_EXPIRE_IN_30', (1 << 1)); // 2 - 30 days of use
define( 'IC_EXPIRE_IN_180', (1 << 2)); //4 -  180 days of use
define( 'IC_EXPIRE_IN_365', (1 << 3)); // 8 - good for a year
define( 'IC_FEATURE_PORTAL', (1 << 4));// 16
define( 'IC_FEATURE_LIFESTYLE', (1 << 5));// 32
define( 'IC_FEATURE_LIFETIME', (1 << 6)); // 64
define( 'IC_EXPIRE_IN_2_YR', (1 << 7)); // 128 - good for a 2 years
define( 'IC_EXPIRE_IN_3_YR', (1 << 8)); // 256 - good for a 3 years
define( 'IC_EXTRA_1_MONTH', (1 << 9)); // 512 add month to total
define( 'IC_EXTRA_2_MONTH', (1 << 10)); // 1024 add month to total
define( 'IC_EXTRA_3_MONTH', (1 << 11)); // 2048 add month to total

define( 'IC_CORPORATE_WIDE', (1 << 20)); // 1024000
define( 'IC_CORPORATE_PER_USER', (1 << 21)); // 2048000
define( 'IC_CORPORATE_LEVEL_1', (1 << 22)); // 4096000
define( 'IC_CORPORATE_LEVEL_2', (1 << 23)); // 8192000



// warnings for invitation code expiration state
// used in seller's meta SELLER_NICKNAME and SELLER_AGENT_ORDER/ORDER_AGENT_MATCH
define( 'IC_WITHIN_30', 1 << 0); // 1
define( 'IC_WITHIN_14', 1 << 1); // 2
define( 'IC_WITHIN_7', 1 << 2);  // 4
define( 'IC_WITHIN_1', 1 << 3);  // 8
define( 'IC_EXPIRED', 1 << 10);	 // 1024
define( 'IC_REMOVED', 1 << 11);  // 2048
define( 'IC_IGNORE_ALL_WARNINGS', 1 << 12); //4096
define( 'IC_BOUGHT_PORTAL', 1 << 13); // 8192
define( 'IC_KEEP_EXPIRED_PORTAL_DAYS', 30); // number of days since IC_EXPIRED
define( 'IC_ACCEPTED', 1000);

// AgentMatch order related, used in reservations meta
define( 'ORDER_IDLE', 0);
define( 'ORDER_BUYING', 1);
define( 'ORDER_BOUGHT', 2);
define( 'ORDER_PORTAL', 10);
define( 'ORDER_PORTAL_EXCLUSION_1', 11); // specify radius of exclusion of agent matches
define( 'ORDER_AGENT_MATCH',20);
define( 'ORDER_SIGN_UP', 30);

// Pricing levels, these are indexes into the array defined in Options::AgentMatchProductPricing
define( 'GRADE_10', 0);
define( 'GRADE_9', 1);
define( 'GRADE_8', 2);
define( 'GRADE_7', 3);
define( 'GRADE_6', 4);
define( 'GRADE_5', 5);
define( 'GRADE_4', 6);
define( 'GRADE_3', 7);
define( 'GRADE_2', 8);
define( 'GRADE_1', 9);

// Product Type
define( 'MONTHLY_SUBSCRIPTION', 0);
define( 'QUARTERLY_SUBSCRIPTION', 1);


// feedback type - used in EmailTracker::type
define( 'FB_GENERAL', 0);
define( 'FB_IMPROVEMENT', 1);
define( 'FB_SUGGESTION', 2);
define( 'FB_QUESTION', 3);
define( 'FB_BUGREPORT', 4);
// these are emails send from us, not MailChimp
define( 'LLC_INTRO', 1);
define( 'LLC_FOLLOWUP', 2);
define( 'LLC_COMPLEX_FOLLOWUP', 3);
define( 'LLC_NO_TABLE_INTRO', 4);
// email type - used in EmailTracker::type, also used in SellerEmailDb to indicate what emails have been sent to the seller
// update $sendFlags in ajax_sellers_management.php::clear-all-flags if more email types are added
define( 'PROMO_FIRST', (1 << 3)); // 8
define( 'PROMO_SECOND', (1 << 4));  // 16
define( 'PROMO_THIRD', (1 << 5)); // 32
define( 'IMPROVE_LISTING', (1 << 6)); // 64
define( 'PROMO_FOURTH', (1 << 7)); // 128
define( 'PORTAL_PROMO_1', (1 << 8)); // 256
define( 'PORTAL_TOOLS_PKG', (1 << 9)); // 512

// SellerEmailDb flags
define( 'RESPONDED_TO_FIRST', (1 << 10));
define( 'RESPONDED_TO_SECOND', (1 << 11));
define( 'RESPONDED_TO_IMPROVE_LISITNG', (1 << 12));
define( 'RESPONDED_TO_VIEW_LISTING', (1 << 13));
define( 'RESERVATION_MADE', (1 << 14));
define( 'RESPONDED_TO_THIRD', (1 << 15));
define( 'RESPONDED_TO_FOURTH', (1 << 16));
define( 'RESPONDED_TO_PORTAL_PROMO', (1 << 17));
define( 'RESPONDED_TO_PORTAL_TOOLS_PKG', (1 << 18));

// update ajax_sellers_management.php if more listing types are added
define( 'SED_ALL_INACTIVE', (1 << 20));
define( 'SED_ACTIVE', (1 << 21));
define( 'SED_WAITING', (1 << 22));
define( 'SED_REJECTED', (1 << 23));
define( 'SED_TOOCHEAP', (1 << 24));
define( 'SED_NEVER', (1 << 25));
define( 'SED_AVERAGE', (1 << 26));
define( 'SED_NO_LISTING', (1 << 27));
define( 'EMAIL_UNSUBSCRIBED', (1 << 31));
define( 'RESPONDED_TO_EMAIL', (1 << 32)); // used only if SentTo flag has not been set yet
// continue above series but keep the campaign type, response/action enums together
// also, the EmailTracker::type/SellersEmabilDb::flag are now 64-bit, but use the last three
// bits as counter for any additional 64-bit that may be needed so that these fields can be used
// over many, many different email campaigns
define( 'PORTAL_PROMO_CONTENT_TEST', (1 << 33));
define( 'RESPONDED_TO_PORTAL_PROMO_CONTENT_TEST', (1 << 34));
define( 'PORTAL_PROMO_FOLLOW_UP', (1 << 35));
define( 'RESPONDED_TO_PORTAL_PROMO_FOLLOW_UP', (1 << 36));


// seller email db meta types
// Edit ajax_sellers_management.php::isChartData()/emailDbMetaActionType() to get any new meta types into chart data.
// Edit ajax_sellers_management.php::emailDbHasMeta() and it's support APIs when any of these changes!
define( 'SENT_FIRST_EMAIL', (1 << 0));      //1
define( 'RESPONSE_FIRST_EMAIL', (1 << 1));  //2
define( 'SENT_SECOND_EMAIL', (1 << 2));     //4
define( 'RESPONSE_SECOND_EMAIL', (1 << 3)); //8
define( 'IMPROVE_LISTING_SENT', (1 << 4));  //16
define( 'IMPROVE_LISTING_RESPONSE', (1 << 5));//32
define( 'VIEWED_LISTING', (1 << 6));		//64
define( 'RESERVED_PORTAL', (1 << 7));		//128
define( 'RESERVED_AGENT_MATCH', (1 << 8));	//256
define( 'RESERVED_SOMETHING', (1 << 9));	//512
define( 'SENT_THIRD_EMAIL', (1 << 10));		//1024
define( 'RESPONSE_THIRD_EMAIL', (1 << 11));	//2048
define( 'SENT_FOURTH_EMAIL', (1 << 12));	//4096
define( 'RESPONSE_FOURTH_EMAIL', (1 << 13));//8192
define( 'SENT_PORTAL_PROMO', (1 << 14));	//16384
define( 'RESPONSE_PORTAL_PROMO', (1 << 15));//32768
define( 'SENT_PORTAL_TOOLS_PKG', (1 << 16));	//16384
define( 'RESPONSE_PORTAL_TOOLS_PKG', (1 << 17));//32768
define( 'SENT_PORTAL_PROMO_CONTENT_TEST', (1 << 18)); // 65536
define( 'RESPONSE_PORTAL_PROMO_CONTENT_TEST', (1 << 19)); // 131072
define( 'SENT_PORTAL_PROMO_FOLLOW_UP', (1 << 20)); // 262144
define( 'RESPONSE_PORTAL_PROMO_FOLLOW_UP', (1 << 21)); // 424288
define( 'EMAIL_UNSUBSCRIBED_DATA', (1 << 31));//0xFFFF
define( 'RESPONSE_TO_EMAIL', (1 << 32)); // used only if SentTo flag has not been set yet
// SmtpAccessCount flags
define( 'EMAIL_STATS', (1 << 0));

define( 'MAX_QUIZ_ACTIVITY', 25);
define( 'MIN_AGENT_MATCH_TAG_DESC', 60); // min # of characters before giving tag to seller
define( 'MAX_EMAIL_PER_SMTP', 50); // per day
define( 'MAX_CHIMP_LIFESTYLES', 8); // how many lifestyle columns into the csv file

// reservation flag types
define( 'RESERVATION_CONVERTED_PORTAL', (1 << 0)); // 1, converted reservation
define( 'RESERVATION_CONVERTED_LIFESTYLE', (1 << 1)); // 2, converted reservation
define( 'RESERVATION_CONVERTED_PORTAL_CODE', (1 << 2)); // 4 converted reservation
define( 'RESERVATION_CONVERTED_LIFESTYLE_CODE', (1 << 3)); // 8, converted reservation
define( 'RESERVATION_LEADID_FROM_CONTACTS', (1 << 4)); // 16, last update from CONTACTS CRM

// reservation meta types - continuing past Seller meta types
define( 'RESERVATION_EXTRA_SELLER_DATA', (1 << 20)); //1,048,576

// Sales tool
define('TAGS_NEED_ALL', 0);
define('TAGS_HAVE_SOME', 1);
define('TAGS_HAVE_ALL', 2);
define('TAGS_ALL_LISTINGS', 3);

// Analytic
define('ANALYTICS_TYPE_CLICK', 1); 
define('ANALYTICS_TYPE_PAGE_ENTRY_INITIAL', 2); 
define('ANALYTICS_TYPE_PAGE_ENTRY', 3); 
define('ANALYTICS_TYPE_PAGE_EXIT', 4); 
define('ANALYTICS_TYPE_SLIDER', 5); 
define('ANALYTICS_TYPE_PROMO', 6); 
define('ANALYTICS_TYPE_EVENT', 7); 
define('ANALYTICS_TYPE_ERROR', 100); 
define('ANALYTICS_TYPE_ERROR_RECOVERY', 101); 

// quiz mode 
define('QUIZ_LOCALIZED', 0);
define('QUIZ_NATION', 1);
define('QUIZ_CITY', 2);
define('QUIZ_STATE', 4);
define('QUIZ_BY_ADDRESS', 10);

// product state
define ('PORTAL_NOT_EXIST', 0);
define ('PORTAL_VALID', 1);
define ('PORTAL_EXPIRING', 2);
define ('PORTAL_EXPIRED', 3);
define ('PORTAL_REMOVED', 4);
define ('PORTAL_RESERVED', 5); // found in a Reservation row with matching email with seller
define ('PORTAL_INVALID', 6);  // portal in Reservation is no longer valid

// Associations type
define('ASSOCIATION_BROKERAGE', 1);
define('ASSOCIATION_COMPANY', 2);
define('ASSOCIATION_NONPROFIT', 3);

// Special registration modes
define('REGISTRATION_SIMPLE_USER', -1);
define('REGISTRATION_SPECIAL_NONE', 0);
define('REGISTRATION_SPECIAL_ADMIN', 1);
define('REGISTRATION_SPECIAL_CAMPAIGN_PORTAL', 2);
define('REGISTRATION_SPECIAL_CAMPAIGN_LIFESTYLE', 3);

// flags for CitiesTags
define("CITY_TAG_NEED_REVIEW", (1 << 0)); 	// 1 - city tag added in processOrder() or processAMInvite() or during parse.
define("CITY_TAG_PURCHASED", (1 << 1));		// 2
define("CITY_TAG_INVITATION", (1 << 2));	// 4
define("CITY_TAG_TRIGGERED", (1 << 3));		// 8
define("CITY_TAG_ASSIGNED_BY_SALES_ADMIN", (1 << 4)); // 16, means it was added with assigned score
define("CITY_TAG_MODIFIED_BY_SALES_ADMIN", (1 << 5)); // 32, means it pre-existed, but score was changed
define("CITY_TAG_ADDED_USING_TAG_SPACE",   (1 << 6)); // 64, used TagSpaces table to extract this city tag

// flags for ListingsTags
define("LISTING_TAG_ASSIGNED_BY_AUTHOR", (1 << 0)); // 1
define("LISTING_TAG_AUTO_ASSIGNED", (1 << 1)); 		// 2
define("LISTING_TAG_FROM_PARSE", (1 << 2)); 		// 4
define("LISTING_TAG_ASSIGNED_BY_SALES_ADMIN", (1 << 4)); // 16, means it was added with assigned score, keep same as CITY_TAG, for convenience
define("LISTING_TAG_BLANK", (1 << 5)); // not used
define("LISTING_TAG_ADDED_USING_TAG_SPACE",   (1 << 6)); // 64, used TagSpaces table to extract this city tag

// flags used for image folder cleaning with ImageTracker table
define("IMAGE_TRACKER_KEEPER", (1 << 0)); // this would be a keeper
define("IMAGE_TRACKER_REMOVED", (1 << 1)); // this would be a keeper

// CitiesPoints meta types
define('CP_COORDS', (1 << 0)); // array of lng/lat

// flags to remote
define("OPTIMIZE_TABLE_AFTER_PARSE", (1 << 0));
define("MODIFY_CPU_LIMIT", (1 << 1));
define("MODIFY_CPU_AVAILABILITY", (1 << 2));

// meta type for PortalUsers table
define("QUIZS_TAKEN", (1 << 0)); // 1 finished quiz, entered quiz-results, array of ['quiz_id'=>number, 'date'=>timestamp]
define("LISTINGS_VIEWED", (1 << 1)); // 2 viewed listing, array of ['listing_id'=>number, 'date'=>timestamp]
define("SITE_ENTRIES", (1 << 2)); // 4 site entered, array of ['page'=>thisPage, 'date'=>timestamp]
define("DAILY_SUMMARY", (1 << 3)); // 8 summary of activity sorted by day
define("HIDDEN_AGENTS", (1 << 4)); // 16 list of agents that has hidden this user
define("AGENT_CAMPAIGNS", (1 << 5)); // 32 list of agent campaigns
define("AGENT_REENTRY_NOTIFICATION", (1 << 6)); // 64 timestamp of last time PORTAL_USER_ENTER_SITE was sent

// PortalUser flags
define("PORTAL_USER_NEED_SUMMARY_SENT", (1 << 0)); // 1
define("PORTAL_USER_EMAIL_CAPTURED", (1 << 1));    // 2, email was captured, but they have not signed up fully yet..
define("PORTAL_USER_HIDDEN", (1 << 2)); 		   // 4, hidden by agent
define("PORTAL_USER_VIEWED", (1 << 3));			   // 8, marked as viewed
define("PORTAL_USER_EMAIL_CAPTURE_NOTIFIED", (1 << 4)); // 16, mark as notified 

// City flags
define('CITY_HAVE_PORTAL_USERS', (1 << 0)); // 1 - quick check to see if we need to look into the meta data
define('CITY_DID_TAG_SPACE_CHECK', (1 << 1)); // 2

// City meta flags
define('CITY_PORTAL_USERS', (1 << 0)); // 1 - list of PortalUsers ID's

// TagSpace meta flags
define("TAG_SPACE_LIST", (1 << 0)); // 1 - tag list

// ListingsViewed meta flags
define("LISTINGS_VIEWER_LIST", (1 << 0)); // 1 - list of ip

// Quiz filter flags
define("QUIZ_NO_LOWER_PRICE_LIMIT", (1 << 0)); // 1
define("QUIZ_INCLUDE_COMMERCIAL", (1 << 1));   // 2
define("QUIZ_INCLUDE_RENTAL", (1 << 2));	   // 4
define("QUIZ_ONLY_RENTAL", (1 << 3));	   	   // 8

// Portal Landing Options
define("PORTAL_LANDING_NOT_USED", 0);
define("PORTAL_LANDING_OPTIONAL", 1);
define("PORTAL_LANDING_MUST_HAVE", 2);
define("PORTAL_LANDING_USE_AS_PASSWORD", 3);

// User capture options
define("CAPTURE_NONE", 0);
define("CAPTURE_OPTIONAL", 1);
define("CAPTURE_MUST", 2);

// PayWhirl meta action types
define("PW_INVOICES", (1 << 0)); // 1
define("PW_REFUNDS", (1 << 1));  // 2
define("PW_IDENTITY", (1 << 2)); // 4

// IMPORTANT!!!  IMPORTANT!!!  IMPORTANT!!!  READ NOTE!!!
// 1.1.2 - fix Quiz so that if there is no state nor location defined, the quiz type is forced to nationwide, or if statewide, but not state but location is defined, set to local search type.
// 1.1.3 - code to integrate portalUser during registration, so that this portal user doesn't get lost after registering.
// 1.1.4 - updated Loader to check Options::BufferJsAndCss and if true, it will save the js and css being queued into global
//         arrays and call functions.php::enqueueData() to write them out as a batch to reduce startup calls made to the server
//         to fetch javascripts and css files.
// 1.1.5 - updated Quizzing to allow for listings filtering.  If allowing commercial and/or all listings, treat discarded images as valid.
// 1.1.6 - reordering of packed js/css
// 1.1.7 - new Quiz::get-status response packet format, new mechanism to control portal-landing pages's input entry usage for email and phone
// 1.1.8 - expanding portal-user's phone availability to sellers.php/js and use the Options::PortalLandingAgentOptions to decide how to show
//         the agent stats
// 1.1.9 - enabled searching for rentals.  If set, then lower the minimum price to either 300 or what is in Options::MinRentalPrice value.
// 1.1.10 - incorporate listing filter into quiz-results page, if the Options::QuizOptions is defined for the user and showListingFilter is set to 1.
//        - Kendall: New videos for tools page. Also added new feature for videos sitewide that maintains your scroll point (prior to clicking the video)
//		    on the page on any device after you close the video to prevent the page from scrolling to the top every time.
// 1.1.11 - added rental only option
// 1.1.12 - updated portal user registration for allowing portal landing page to start search right away, and to capture the portal user in 
//          the quiz-results page.  Using Options::PortalLandingAgentOptions, QuizResultsPortalUserCaptureOptions and QuizResultsAgentOptions to control behavior
//          of the front end. 
// 1.1.13 - added queryMode to QuizResultsAgentOptions, so if set to 1, then will ask for email first, then the rest of info.
// 1.1.14 - implement Facebook login on portal-landing and quiz-results pages
//          still needs some work.  if the user uses the browser nav buttons, FB doesn't seem to return from FB.getLoginStatus().
// 1.1.15 - add code to capture email of portal user, even if they didn't fully signup
// 1.1.16 - updated 1.1.15 feature to capture name if available, and any other FB data, if available
// 1.1.17 - added attention and check icon to portal user capture input fields
// 1.1.18 - add agentID to portal-landing's ANALYTICS_TYPE_PAGE_ENTRY_INITIAL event
// 1.1.19 - adjusted portalUserRegistration.js to account for the markers put next to the input's for quiz-results::portalAgentCapture popup.
//          needed to find the div with the correct group attribute, then find the associated child input elements, instead of using the anchor
//          and looking for siblings(), which would fail now.  But this way should work for both portal-landing and quiz-results pages.
//        - For NEW_USER_FROM_LANDING event in PortalUsersMgr::sendEmail, if the portalUser has a QUIZ_TAKEN, then use the first one to get the
//          quiz detail and add that to the email sent out.
// 1.1.20 - fix Contact Me button on portal-landing.  updated ahfeedback to allow for usage from portal-landing and capture the user name and email
//          and re-use that in quiz-result's portalUserCapture popup.
//          fix portal-landing login url's
// 1.1.21 - added a popup in portal-landing if FB login was successful during initialization.  also cleaned up new portal user text message some more.
// 1.1.22 - added analytics to portal user capturing events.
// 1.1.23 - added new FB logger redirect popup on portal landing.
//        - return false from closeVid.on('click') event handler
// 1.1.24 - better persistence of CassiopeiaDirective for new site entry.
// 1.1.25 - added agentID to quiz page entry analytics
// 1.1.26 - agent portal users sorted by date (descending order), and usage next
// 1.1.27 - updated portal stats: show S#,Q#,L# in Location/Dates to show Site, Quiz and Listing activity counts, added Hide User and highlighing viewed
//          users.  When a user has new activities, then the PORTAL_USER_VIEWED flag is removed from that portal user.
// 1.1.28 - anchors don't like disabled attribute, so use class if it is, else if button, would work also on the a.start used in portal user capturing
// 1.1.29 - improved portal user hiding mechanism, instead of repainting the page, just hide the user row using fadeOut().
// 1.1.30 - added LeadIn option to QuizResultsPortalUserCaptureOptions option.  Very similiar in construct to the current options such as quizLoad, cityScroll
//          and such, but with the suffix, LeadIn to make quizLoadLeadIn, cityScrollLeadIn, etc.  The new LeadIn option may look like this:
/*
			"quizLoadLeadIn":
				{"captureMode":1,
				 "delay":5000,
				 "mobileDelay":3000,
				 "title":"Join the Revolution",
				 "message":"Find homes that suits your lifestyle, Call %agent% anytime or have %agent% call you when they find a listing that suits your needs."
				}

			The %agent% special marker can be used and it will be replaced the the portal agent's first name.

			If the LeadIn is not present or the captureMode = 0, then it will not appear for an event.  When it does appear, it will be a customized popup
			that will lead the user into the actual capture dialog.
*/
// 1.1.31 - added PortalLandingStrings option, so that the three line canned message in the portal landing page can be changed externally without
//		  - recoding.  It may look like this:
/*
			{"title":"Retiring<span class=&quot;dot&quot;>|</span>Relocating<span class=&quot;dot&quot;>|</span>Second home?",
			 "subtext":"Live somewhere with better lifestyle",
			 "subtitle":"Find Cities &amp; Homes that match your lifestyle perfectly"
			}

			If the option is not present, a default version with the above strings will be used.
			Any special HTML characters should use the appropriate &###; format as shown above.
*/
// 1.1.32 - fixed all_us_states[] to say 'District of Columbia' for 'DC' instead of 'Washington,DC'
// 1.1.33 - added mechanism to allow a portal user to be associated with multiple portal agents, and each agent can independently
//   		hide the user.
// 1.1.34 - added two bxslider units to portal-landing page for the slides
// 1.1.35 - fixed showPortalUserCapture to only check for 'typeof currentPage == "undefined"' to set page to -1
// 1.1.36 - installed overlay-bg to portal-landing, register, quiz-results pages to show a spinner during possible page transition or an
//			actual page transition (reload or redirect, after registration or listing view)
// 1.1.37 - added portal agent, portal user and wp user ID's into the QuizActivity, so that the Loader can auto-load portal agent
// 		    and go directly into the quiz-results.
// 1.1.38 - fixed portal user capture behavior on viewListing event.  If the captureMode is 1 (not hard capture), and the user closes
//			the dialog, allow the code to transition to the listing page.
// 1.1.39 - added PredefinedQuizListHome option to show prefined quized on home page if PredefinedQuizListHome::showPredefinedQuiz is 1.
//          This option is very similar to PredefinedQuizList, but with only one 'home' branch.
// 1.1.40 - updated the predefined quiz options and display to include the tag icon in the upper right corner
// 1.1.41 - updated PredefinedQuizList to have both sliderSize for Dialog and onPage.  So we now have slideSizeDialog, slideSizeOnPage and
//			widthDialog/heightDialog and widthOnPage/heightOnPage for custom sizes.
// 1.1.42 - added to sellers::portal::tools page, a portal capture setup mechanism.  If SellerLeadCaptureOption->showLeadCapture is set to 1,
//          a Setup button will appear on the tools page and the setup sequence will appear when that is clicked.
// 1.1.43 - changed options for QuizOptions, took out reference to quiz slide page #'s and added new ones: forcePopupPredefinedQuiz and
//			forcePopupPredefinedQuizWhenUsedAsLandingPage, to control the popup predefined dialog.  Also modified the PredefinedQuizList option
//			to house the activitySlide and predefinedQuizSlide page numbers, as they are global.
// 1.1.44 - added to QuizOptions, disablePopupPredefinedQuizWhenReferrerIsPortalLanding flag to override the predefined quiz dialog popup.
// 1.1.45 - added campaign feature to setting up the URL's for portal landing, quiz and quiz-results pages when used for a portal user capturing
//          mechanism.  The campaign name an agent uses is arbitrary, for instance:  www.lifestyledlistings.com/gtvracer?campaign=client_email_list,
//          or www.lifestyledlistings.com/gtvracer/?campaign=client_email_list or www.lifestyledlistings.com/quiz-results/R-645/P-8?campaign=client_email_list
// 1.1.46 - added portal-landing-one for A/B testing
// 1.1.47 - added mechanism to sent test to agent when email is captured, if QuizResultsAgentOptions::textMessageOnEmailCapture is true.
// 1.1.48 - added campaign and campaign_orig to PortalUser table, the portal user will also have that recorded in it's meta data.
//		  - the controller.actionPortalUserRegistered() for quiz-results will now prompt the user if they haven't been registered as a full
//          user, and ask them if they want to.  The header.php is also updated so the quiz-result's Log In/Register anchor now points to the
//          controller.login(), for quiz-results, that will open up the ahreg dialog instead of redirecting to the registration page.
// 1.1.49 - ahreg updated to pass along the campaign, agentID and isPhoneNumber values to basic-register.  PortalUsersMgr::addPortalUserFromWP()
//			will update the existing portal user's phone number and send out a text message with the subject line: Fully Qualified Portal User.
//        - improved quiz-results's recordQuizForPortalUser() to be called every time, so that sessionID and agentID are supplied to ecord-portal-user-quiz.
//          ajax-quiz::ecord-portal-user-quiz will now call PortalUsersMgr::updateSession() if the portalUserID and agentID are available and update the
//          quiz_id of the session also.
// 1.1.50 - added QuizResultsPortalUserCaptureOptions::byPassCount with counters for quizLoad and viewListing.  If non-zero, will allow that many events to
//			be bypassed before executing the captureMode.
// 1.1.51 - added QuizResultsPortalUserCaptureOptions::forceHardStopOnViewAllListingAtQuizLoad and enterListings/enterListingsLeadIn events.
//		  - also added ANALYTICS_TYPE_EVENT when user hits view all listings from quiz, change search area, change search parameters, change tag selections, 
//			save search or start new search in quiz-results.
// 1.1.52 - updated popup predefined quiz on quiz page, added 534x400 quiz size, updated Image class to check for questions.enabled = 2 for a special page
//			used for the predefined quiz images, add new Reservation in _ajax::get-portal-name if one does not exist for the seller.
// 1.1.53 - updated admin::quiz and quiz.js/ajax_quiz.php to use the overloaded QuizQuestions::enabled value.  1 = enabled, 2 = use as predefined, and these
//			may be OR'ed together.  So the queries and if statements will now use a '&' with '1' so it gets the enabled questions only for the quiz.  And the
//			Image class will produce images from slides from questions with enabled with 1, 2, (1 AND 2 = 3) values.
// 1.1.54 - added QuizResultsPortalUserCaptureOptions::informUserNotFullyRegisteredUserAfterSignUp to make the post sign up message to register user optional.
//			reenabled the popup in portalUserRegistration.js when phoneMode is PORTAL_LANDING_OPTIONAL and phone number is empty, with a new message
// 1.1.55 - incorporated Kendall's changes for optional phone number entry popup in portalUserRegistration.js.  Extended that code to be used for the optional
// 			email popup also.  Reworked backend to deal with an empty email when registering a new portal user, and to update that user's email properly if the
//			registers fully later.
// 1.1.56 - reduce timeout in quiz.js before window.location = quiz-results page down to 500.  Moved loading of listhub.js to header.js after jQuery is loaded and
//			directive is received.
// 1.1.57 - added Google Translate mechanism into the header.php.  Use SiteMasterOption::showGoogleTranslate to control whether to show it or not.
// 1.1.58 - added notranslate class to elements that should be left alone from Google Translator
// 1.1.59 - use ahreg from listing and explore-the-area-new pages.  fixed redirect from quiz-results to ahreg so that the new user properly inherits the current
//			quiz being taken.  re-enabled some logs in SessionsMgr::getDirective().  made checkSellerInDB part of default js loading for site pages.
// 			fixed styling for nav bar's info-buyer div so that the spans are inline-block.
// 1.1.60 - added setTimeout() in header.js and seller.js to wait for ahtb to be constructed.  Experienced a race condtion in sellers page, where it tried to 
//			access ahtb before it was created.
// 1.1.61 - fixed validYouTubeURL() to use an active Google API key so it works.
//          took out the videojs used in the listings page and made it conform to the videoDiv/iframe setup we are using for our in-house video.
//			moved the close button for the video's lower so it's not hidden by the nav bar and the new seller's nav bar (with Tools, etc..)
//    		fixed new-listings.js to account for the change in how youTube URL's are searched for the video it.  It may no longer have the "v=XXXXXX" signature.
// 1.1.62 - when page entry is ANALYTICS_TYPE_PAGE_ENTRY_INITIAL, then clear out cookie:QuizResultsHistory, as this causes strange behavior with quiz-results::onpopstate().
// 1.1.63 - better handling of returning portal users, so that if the user is presented again with the portal capture dialog and enters the email, the system will
//			will first check to see if it belongs to an existing registered user.  If so, it will log the user in and tell the FE to reload page or redirect to the listing page.
//			If the portal agent and portal user is the same person, don't sent the PORTAL_USER_ENTER_SITE message.
//			Send a TELL_PORTAL_USER_THEIR_LOGIN message to the portal user to use their user_login name when logging in.
//			In Loader, check for .css file in load('init'), and if it is a .css, just die.
// 1.1.64 - in sellers.php, look for SELLER_INVITE_CODE meta and set the inviteCode to be passed to js. And in sellers.js, see if that inviteCode.flags is appropriate for the product and use it instead of coupon or couponLA.
//			In Register::verifyInvite(), OR the IC_FEATURE_LIFETIME flag when checking,, as that supercedes other flags.
//			In SessionMgr::getPortalName(), access the $seller value earlier, so if the coupon is '0', a proper $seller value is returned anyway.
//			updated ahjs.js to use redirect() as ok callback.
//			In quiz.js, if the Referer is quiz-results, just start a new quiz
// 1.1.65 - moved $log and log(), clear(), flush() to _Controller, this involved changing many files derived from _Controller.
//			created ActivityHelper class to house common methods between Loader and ajax_quiz.php (mainly for getProfiles() functionality).
// 1.1.66 - fixed how quiz.js deals with the location.hash, so that it is preserved before pushState() is called.
//			changed the ActivityHelper::log() to take debugLevel and tabs parameters too, so it can be used in classes that had that pattern.
// 1.1.67 - added MESSAGE_EMAIL_PORTAL_AGENT to differentiate email types using the ahfeedback object.  Now the listing agent will get notified when a user contacts
//			a Lifestyled Agent (with or without a portal), and the Portal Agent (when applicable).
// 1.1.68 - added MESSAGE_EMAIL_LISTING_OFFICE to differentiate email types for listing agent's email and office email.  Updated ListHubData class to parse for OfficeEmail and store in seller's data
// 1.1.69 - added management of companies (Associations table) usage for the seller.  In the sellers::tools page, if the Options::AgentPageOption::showFindCompanyButton is 1, then
//			then a Find button will be displayed.  And depending on other settings in Options::AgentPageOption, various versions of the management dialogs will be displayed.
//			If allowAdvancedAgentCompanyEdits is 1, then the Options column will have the Edit button.
//			If showExtendedCompanyFields is one, when creating/editing a company, additional fields will be displayed for editing, i.e. address, contact, contact_email, url, etc.
//			If showCompanyMorePulldownTab is one, then there will be a Details column to show any addtional information on the company.
//			In Register::verifyInvite(), if the agent has IC_FEATURE_LIFETIME, then everything is unlimted.
// IMPORTANT!!!
// When this is changed, also remove all the files under _jsPack! Read NOTE below!
define("FE_VERSION", "1.1.69"); // NOTE: the feVersion in header.js must be changed also
							   //       when this is changed!!!!  Change this when there is a significant
							   // 		change to some front/backend code that must be synched for the user's browser
							   // 		to act as expected.  If the frontend's version is less than what is supplied
							   // 		with get-directive call to ajax-register.php, then header.js will auto reload	
// IMPORTANT!!!  IMPORTANT!!!  IMPORTANT!!!  READ NOTE!!!

