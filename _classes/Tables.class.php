<?php
namespace AH { 

require_once(__DIR__.'/Utility.class.php');

class Tables {
	public $tables = [
		'analytics' =>
			[ 'table' => 'analytics', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'session_id' => [ 'type' => 'int(12)', 'default' => 0 ], // from sessions table, get user_id, ip, etc..
				'added' => [ 'type' => 'timestamp' ],
				'type' => [ 'type' => 'smallint' ],
				'origin' => [ 'type' => 'tinytext' ],
				'what' => [ 'type' => 'tinytext' ],
				'value_str' => [ 'type' => 'tinytext', 'json' => 1],
				'value_int' => [ 'type' => 'bigint', 'default' => 0 ],
				'value_int2' => [ 'type' => 'bigint', 'default' => 0 ],
				'referer' => [ 'type' => 'tinytext' ],
				'ip' => [ 'type' => 'tinytext' ],
				'browser' => [ 'type' => 'tinytext',],
				'connection' => [ 'type' => 'text', 'json' => 1 ],
			]],
		'associations' =>
			[ 'table' => 'associations', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'company' => [ 'type' => 'tinytext' ], 
				'company_code' => [ 'type' => 'tinytext' ], 
				'shortname' => [ 'type' => 'tinytext' ], 
				'url'=>  [ 'type' => 'mediumtext' ],
				'street_address' => [ 'type' => 'text' ],
				'city' => [ 'type' => 'text' ],
				'state' => [ 'type' => 'text' ],
				'zip' => [ 'type' => 'text' ],
				'country' => [ 'type' => 'text' ],
				'about' => [ 'type' => 'mediumtext' ],
				'contact' => [ 'type' => 'tinytext' ], 
				'contact_email' => [ 'type' => 'tinytext' ], 
				'phone' => [ 'type' => 'bigint(12)' ],
				'phone2' => [ 'type' => 'bigint(12)' ],
				'type' => [ 'type' => 'bigint', 'default' => 0  ],			
				'meta' => [ 'type' => 'text', 'json' => 1],
				'flags' => [ 'type' => 'bigint', 'default' => 0  ],
				'logo' => [ 'type' => 'tinytext' ],
				'logo_x_scale' => [ 'type' => 'decimal(5,2)' ],
				'added' => [ 'type' => 'timestamp' ],
			]],
		'cities' =>
			[ 'table' => 'cities', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'city' => [ 'type' => 'text' ],
				'state' => [ 'type' => 'text' ],
				'country' => [ 'type' => 'text' ],
				'zip' => [ 'type' => 'text' ],
				'about' => [ 'type' => 'text' ],
				'image' => [ 'type' => 'text' ],
				'lat' => [ 'type' => 'decimal(10,7)' ],
				'lng' => [ 'type' => 'decimal(10,7)' ],
				'count' => [ 'type' => 'int(12)', 'default' => 0],
				'clicks' => [ 'type' => 'mediumint', 'default' => 0],
				'impressions' => [ 'type' => 'int(11)', 'default' => 0],
				'quandl' => [ 'type' => 'int(12)', 'default' => 0],
				'meta' => [ 'type' => 'longtext', 'json' => 1 ],
				'flags' => [ 'type' => 'int(11)', 'default' => 0  ],				
				'portal_user_count' => [ 'type' => 'int(11)', 'default' => 0  ]				
		]],
		'cities-counts' =>
			[ 'table' => 'cities_counts', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT']],
				'city_id' => [ 'type' => 'int(12)' ],
				'label' => [ 'type' => 'text' ],
				'value' => [ 'type' => 'text' ],
				'lat' => [ 'type' => 'decimal(10,7)' ],
				'lng' => [ 'type' => 'decimal(10,7)' ],
				'count' => [ 'type' => 'int(12)', 'default' => 0],
		]],
		'cities-tags' =>
			[ 'table' => 'cities_tags', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'tag_id' => [ 'type' => 'int(12)' ],
				'city_id' => [ 'type' => 'int(12)' ],
				'score' => [ 'type' => 'int(4)' ],
				'clicks' => [ 'type' => 'mediumint', 'default' => 0],
				'flags' => [ 'type' => 'int(11)', 'default' => 0  ]
			]],
		'cities-points' =>
			[ 'table' => 'cities_points', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'city_id' => [ 'type' => 'int(12)' ],
				'radius' => [ 'type' => 'int(12)' ],
				'poi_count' => [ 'type' => 'int(12)' ],
				'meta' => [ 'type' => 'text', 'json' => 1 ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
				'flags' => [ 'type' => 'int(11)', 'default' => 0  ]
			]],
		'custom-image-blocks' =>
			[ 'table' => 'custom_image_blocks', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'data' => [ 'type' => 'longtext'],
				'flag' => [ 'type' => 'tinyint', 'default' => 0 ]
			]],
		'custom-image-chunk-status' =>
			[ 'table' => 'custom_image_chunk_status', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'subdomain' => [ 'type' => 'int(12)' ],
				'startid' => [ 'type' => 'int(12)' ],
				'status' => [ 'type' => 'tinyint(4)' ],
				'pid' => [ 'type' => 'int(12)' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0],
			]],
		'email-tracker' =>
			[ 'table' => 'email_tracker', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'track_id' => [ 'type' => 'tinytext' ],
				'agent_id' => [ 'type' => 'mediumint', 'notnull' => true, 'default' => 0 ], // user id and not author_id since it can be from an unregistered agent
				'agent_email' => [ 'type' => 'tinytext'],
				'user_id' => [ 'type' => 'mediumint', 'notnull' => true, 'default' => 0 ],  // this is always froo wordpress user table, i.e. WP_User->ID
				'user_email' => [ 'type' => 'tinytext'],
				'first' => [ 'type' => 'tinytext'],
				'last' => [ 'type' => 'tinytext'],
				'listing_id' => [ 'type' => 'int', 'notnull' => true, 'default' => 0 ],
				'type' => [ 'type' => 'bigint', 'notnull' => true, 'default' => 0 ],
				'flags' => [ 'type' => 'int', 'notnull' => true, 'default' => 0 ],
				'meta' => [ 'type' => 'text', 'json' => 1 ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0 ]
			]],
		'feedback' =>
			[ 'table' => 'feedback', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'first' => [ 'type' => 'tinytext' ],
				'last' => [ 'type' => 'tinytext' ],
				'user_email' => [ 'type' => 'tinytext'],
				'comment' => [ 'type' => 'text' ],
				'user_id' => [ 'type' => 'smallint'],
				'type' => [ 'type' => 'tinyint' ],
				'flags' => [ 'type' => 'int'],
				'session_id' => [ 'type' => 'text' ],
				'agent_id' => [ 'type' => 'smallint', 'notnull' => true, 'default' => 0 ],
				'listing_id' => [ 'type' => 'smallint', 'notnull' => true, 'default' => 0 ],
				'added' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'],
				'etId' => [ 'type' => 'tinytext']
			]],
		'image-blocks' =>
			[ 'table' => 'image_blocks', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'data' => [ 'type' => 'longtext'],
				'flag' => [ 'type' => 'tinyint', 'default' => 0 ]
			]],
		'image-chunk-status' =>
			[ 'table' => 'image_chunk_status', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'subdomain' => [ 'type' => 'int(12)' ],
				'startid' => [ 'type' => 'int(12)' ],
				'status' => [ 'type' => 'tinyint(4)' ],
				'pid' => [ 'type' => 'int(12)' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0],
			]],
		'invitations' =>
			[ 'table' => 'invitations', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'code' => [ 'type' => 'tinytext' ],
				'comment' => [ 'type' => 'tinytext' ],
				'seller_id' => [ 'type' => 'text', 'json' => 1 ],
				'association_id' => [ 'type' => 'int', 'default' => 0 ],
				'good_for' => [ 'type' => 'smallint', 'default' => 30 ],
				'valid_until' => [ 'type' => 'smallint', 'default' => 60 ],
				'number_usage' => [ 'type' => 'smallint', 'default' => -1 ],
				'count_usage' => [ 'type' => 'smallint', 'default' => 0 ],
				'flags' => [ 'type' => 'bigint', 'default' => 18 ], // 18 = (IC_EXPIRE_IN_30 + IC_FEATURE_PORTAL)
				'added' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'],
				'meta' => [ 'type' => 'longtext', 'json' => 1 ],
			]],
		'ip-list' =>
			[ 'table' => 'ip_list', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'ip' => [ 'type' => 'tinytext' ],
				'city' => [ 'type' => 'tinytext' ],
				'state' => [ 'type' => 'tinytext' ],
				'country' => [ 'type' => 'tinytext' ]
			]],
		'listhub-blocks' =>
			[ 'table' => 'listhub_blocks', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'data' => [ 'type' => 'longtext'],
				'flag' => [ 'type' => 'tinyint', 'default' => 0 ]
			]],
		'listhub-image-tracker' =>
			[ 'table' => 'listhub_image_tracker', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'flags' => [ 'type' => 'int(11)', 'default' => 0  ],
				'data' => [ 'type' => 'tinytext'],
			]],
		'listhub-chunk-status' =>
			[ 'table' => 'listhub_chunk_status', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'subdomain' => [ 'type' => 'int(12)' ],
				'startid' => [ 'type' => 'int(12)' ],
				'status' => [ 'type' => 'tinyint(4)' ],
				'pid' => [ 'type' => 'int(12)' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0],
			]],
		'listhub-progress' =>
			[ 'table' => 'listhub_progress', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'type' => [ 'type' => 'int(8)' ],
				'data' => [ 'type' => 'text' ],
				'intData' => [ 'type' => 'int', 'default' => 0  ],
				'added' => [ 'type' => 'timestamp', 'default' => 0],
			]],
		'listings' =>
			[ 'table' => 'listings', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'title' => [ 'type' => 'text' ],
				'price' => [ 'type' => 'int(12)' ],
				'street_address' => [ 'type' => 'text' ],
				'city_id' => [ 'type' => 'int(12)', 'attr' => ['INDEX'] ],
				'city' => [ 'type' => 'text' ],
				'state' => [ 'type' => 'text' ],
				'zip' => [ 'type' => 'text' ],
				'country' => [ 'type' => 'text' ],
				'about' => [ 'type' => 'mediumtext' ],
				'author' => [ 'type' => 'int(12)' ],
				'url' => [ 'type' => 'tinytext'],
				'beds' => [ 'type' => 'tinyint(4)' ],
				'baths' => [ 'type' => 'decimal(6,3)' ],
				'mls' => [ 'type' => 'text' ],
				'lotsize' => [ 'type' => 'decimal(12,4)' ],
				'lotsize_std' => [ 'type' => 'varchar(6)', 'default' => 'ft' ],
				'interior' => [ 'type' => 'int(12)' ],
				'interior_std' => [ 'type' => 'varchar(6)', 'default' => 'ft' ],
				'images' => [ 'type' => 'mediumtext', 'json' => 1 ],
				'first_image' => [ 'type' => 'text', 'json' => 1 ],
				'list_image' => [ 'type' => 'text', 'json' => 1 ],
				'video' => [ 'type' => 'text', 'json' => 1 ],
				'active' => [ 'type' => 'tinyint(1)' ],
				'listing_status' => [ 'type' => 'tinytext' ],
				'tier' => [ 'type' => 'tinyint(1)' ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0 ],
				'listhub_key' => [ 'type' => 'varchar(48)', 'attr' => ['INDEX'] ],
				'author_has_account' 	=> [ 'type' => 'tinyint(1)', 'default' => 0 ],
				'meta' => [ 'type' => 'text', 'json' => 1],
				'error' => [ 'type' => 'tinyint(3)', 'default' => 0 ],
				'key_match' => [ 'type' => 'tinyint(1)', 'default' => 0 ],
				'flags' => [ 'type' => 'int(11)', 'default' => 0  ],
				'disclaimer' => [ 'type' => 'tinytext'],
				'clicks' => [ 'type' => 'smallint', 'default' => 0],
				'impressions' => [ 'type' => 'int(11)', 'default' => 0],
				'triggers' => [ 'type' => 'text', 'json' => 1]
			]],
		'listings-activity' =>
			[ 'table' => 'listings_activity', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'listing_id' => [ 'type' => 'int(12)', 'attr' => ['UNIQUE'] ],
				'author' => [ 'type' => 'int(12)' ],
				'data' => [ 'type' => 'longtext', 'json' => true ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0 ],
			]],
		'listings-geoinfo' =>
			[ 'table' => 'listings_geoinfo', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'listing_id' => [ 'type' => 'int(12)', 'attr' => ['UNIQUE'] ],
				'lat' => [ 'type' => 'decimal(10,7)' ],
				'lng' => [ 'type' => 'decimal(10,7)' ],
				'address' => [ 'type' => 'text'],
			]],
		'listings-tags' =>
			[ 'table' => 'listings_tags', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'tag_id' => [ 'type' => 'int(12)' ],
				'listing_id' => [ 'type' => 'int(12)' ],
				'clicks' => [ 'type' => 'smallint', 'default' => 0],
				'flags' => [ 'type' => 'int(11)', 'default' => 0  ]
			]],
		'listings-viewed' =>
			[ 'table' => 'listings_viewed', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'listing_id' => [ 'type' => 'int(12)', 'attr' => ['UNIQUE'] ],
				'clicks' => [ 'type' => 'smallint', 'default' => 0],
				'meta' => [ 'type' => 'longtext', 'json' => true ],
				'viewed' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
			]],
		'median-prices' =>
			[ 'table' => 'median_prices', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'zip' => [ 'type' => 'text' ],
				'city_id' => [ 'type' => 'int(12)', 'default' => 0 ],
				'median_home' => ['type' => 'decimal(11,2)' ],
				'median_condo' => ['type' => 'decimal(11,2)' ],
				'monthly_home' => ['type' => 'decimal(11,2)', 'default' => 0 ],
				'daily_home' => ['type' => 'decimal(11,2)', 'default' => 0 ],
				'home_count' => ['type' => 'int', 'default' => 0],
				'monthly_condo' => ['type' => 'decimal(11,2)', 'default' => 0 ],
				'daily_condo' => ['type' => 'decimal(11,2)', 'default' => 0 ],
				'condo_count' => ['type' => 'int', 'default' => 0],
				'month_started' => [ 'type' => 'timestamp', 'default' => 0],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
			]],
		'options' =>
			[ 'table' => 'options', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'opt' => [ 'type' => 'text' ],
				'value' => [ 'type' => 'mediumtext'],
			]],
		'paywhirl-plan' =>
			[ 'table' => 'paywhirl_plan', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'plan_id' => [ 'type' => 'int', 'default' => 0 ],
				'sku' => [ 'type' => 'tinytext' ],
				'flag' => [ 'type' => 'int(11)', 'default' => 0  ]
			]],
		'paywhirl-user' =>
			[ 'table' => 'paywhirl_user', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'wp_user_id' => [ 'type' => 'int(12)', 'attr' => ['UNIQUE'] ],
				'seller_id' => [ 'type' => 'int(12)', 'attr' => ['UNIQUE'] ],
				'customer_id' => [ 'type' => 'int(12)', 'attr' => ['UNIQUE'] ],
				'first_name' => [ 'type' => 'text' ],
				'last_name' => [ 'type' => 'text' ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0 ],
				'meta' => [ 'type' => 'longtext', 'json' => 1 ],
				'flags' => [ 'type' => 'int(11)', 'default' => 0  ]
			]],		
		'points' =>
			[ 'table' => 'points', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'name' => [ 'type' => 'text' ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0 ],
				'address' => [ 'type' => 'mediumtext' ],
				'lat' => [ 'type' => 'decimal(10,7)' ],
				'lng' => [ 'type' => 'decimal(10,7)' ],
				'meta' => [ 'type' => 'mediumtext', 'json' => 1],
			]],
		'points-categories' =>
			[ 'table' => 'points_categories', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'category' => [ 'type' => 'varchar(255)', 'attr' => ['UNIQUE'] ],
				'description' => [ 'type' => 'int(12)' ],
				'parent' => [ 'type' => 'int(12)', 'notnull' => true, 'default' => 0],
			]],
		'points-taxonomy' =>
			[ 'table' => 'points_taxonomy', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'category_id' => [ 'type' => 'int(12)' ],
				'point_id' => [ 'type' => 'int(12)'],
			]],
		'portal-users' =>
			[ 'table' => 'portal_users', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'first_name' => [ 'type' => 'text' ],
				'last_name' => [ 'type' => 'text' ],
				'email' => [ 'type' => 'text' ],
				'phone' => [ 'type' => 'bigint(12)' ],
				'photo' => [ 'type' => 'text' ],
				'flags' => [ 'type' => 'int(11)', 'default' => 0  ],
				'fb_id' => [ 'type' => 'tinytext' ],
				'ip' => [ 'type' => 'tinytext' ],
				'ip_orig' => [ 'type' => 'tinytext'],
				'campaign' => [ 'type' => 'tinytext'],
				'campaign_orig' => [ 'type' => 'tinytext'],
				'wp_user_id' => [ 'type' => 'int(12)', 'default' => 0 ], // this can be anyone that had registered to our site already
				'agent_id' => [ 'type' => 'int(12)', 'default' => 0 ],   // portal agent' ID
				'session_id' => [ 'type' => 'int(12)', 'default' => 0 ], // last session associated with this portal user
				'meta' => [ 'type' => 'longtext', 'json' => 1 ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0 ],
			]],
		'quiz-activity' =>
			[ 'table' => 'quiz_activity', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'session_id' => [ 'type' => 'text' ],
				'status' => [ 'type' => 'text' ],
				'data' => [ 'type' => 'longtext', 'json' => 1 ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0 ],
				'portal_agent_id' => [ 'type' => 'int(12)', 'default' => 0 ],
				'portal_user_id' => [ 'type' => 'int(12)', 'default' => 0 ],
				'wp_user_id' => [ 'type' => 'int(12)', 'default' => 0 ]
			]],
		'quiz-questions' =>
			[ 'table' => 'quiz_questions', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'enabled' => [ 'type' => 'tinyint(1)', 'default' => 0 ],
				'prompt' => [ 'type' => 'text' ],
				'description' => [ 'type' => 'text' ],
				'type' => [ 'type' => 'tinyint(2)', 'default'=>3 ],
				'multi' => [ 'type' => 'tinyint(1)' ],
				'multi_num' => [ 'type' => 'tinyint(1)' ],
				'flagged' => [ 'type' => 'tinyint(1)', 'default' => 0  ],
				'quiz' => [ 'type' => 'tinyint(1)', 'default' => 2 ],
				'weight' => [ 'type' => 'tinyint(2)', 'default' => 1 ],
				'section' => [ 'type' => 'tinyint(2)' ],
				'orderby' => [ 'type' => 'int(3)' ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0 ],
			]],
		'quiz-query' =>
			[ 'table' => 'quiz_query', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'session_id' => [ 'type' => 'text' ],
				'type' => [ 'type' => 'tinytext' ],
				'data' => [ 'type' => 'longtext', 'json' => 1 ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0 ],
			]],
		'quiz-slides' =>
			[ 'table' => 'quiz_slides', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'question_id' => [ 'type' => 'int(12)' ],
				'file' => [ 'type' => 'text' ],
				'title' => [ 'type' => 'text' ],
				'orderby' => [ 'type' => 'int(3)' ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0 ],
			]],
		'quiz-tags' =>
			[ 'table' => 'quiz_tags', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'slide_id' => [ 'type' => 'int(12)' ],
				'tag_id' => [ 'type' => 'int(12)' ],
			]],
		'reservations' =>
			[ 'table' => 'reservations', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'seller_id' => [ 'type' => 'int(12)' ],
				'first_name' => [ 'type' => 'text' ],
				'last_name' => [ 'type' => 'text' ],
				'email' => [ 'type' => 'text' ],
				'phone' => [ 'type' => 'bigint(12)' ],
				'type' => [ 'type' => 'int(12)' ],
				'portal' => [ 'type' => 'tinytext' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0],
				'meta' => [ 'type' => 'text', 'json' => 1],
				'flags' => [ 'type' => 'int', 'default' => 0 ],
				'lead_id'=> [ 'type' => 'bigint', 'default' => 0 ]
			]],
		'sellers' =>
			[ 'table' => 'sellers', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'author_id' => [ 'type' => 'int(12)', 'attr' => ['UNIQUE'] ],
				'first_name' => [ 'type' => 'text' ],
				'last_name' => [ 'type' => 'text' ],
				'website' => [ 'type' => 'text' ],
				'email' => [ 'type' => 'text' ],
				'office_email' => [ 'type' => 'tinytext' ],
				'company' => [ 'type' => 'text' ],
				'street_address' => [ 'type' => 'text' ],
				'city' => [ 'type' => 'text' ],
				'state' => [ 'type' => 'text' ],
				'zip' => [ 'type' => 'text' ],
				'country' => [ 'type' => 'text' ],
				'phone' => [ 'type' => 'bigint(12)' ],
				'mobile' => [ 'type' => 'bigint(12)' ],
				'photo' => [ 'type' => 'text' ],
				'about' => [ 'type' => 'mediumtext' ],
				'service_areas' => [ 'type' => 'text' ],
				'specialties' => [ 'type' => 'text' ],
				'associations' => [ 'type' => 'text', 'json' => 1 ],
				'accomplishments' => [ 'type' => 'mediumtext', 'json' => 1 ],
				'social' => [ 'type' => 'mediumtext', 'json' => 1 ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0 ],
				'meta' => [ 'type' => 'longtext', 'json' => 1 ],
				'bre' => [ 'type' => 'varchar(255)' ],
				'flags' => [ 'type' => 'int(11)', 'default' => 0  ],
				'association' => [ 'type' => 'bigint', 'default' => 0  ], // which agency is this agent associated with
				'portal_visits' => [ 'type' => 'int(11)', 'default' => 0],
				'participant_key' => [ 'type' => 'tinytext' ],
			]],
		'sellers-email-db' =>
			[ 'table' => 'sellers_email_db', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'seller_id' => [ 'type' => 'int(12)' ],
				'smtp_id' => [ 'type' => 'int(12)', 'default' => 0 ], // maps to smtp-server table
				'name' => [ 'type' => 'text' ], // the first part, i.e.: tomtong58
				'host' => [ 'type' => 'text'], // the email host part, i.e.: gmail.com
				'meta' => [ 'type' => 'text', 'json' => 1 ],
				'flags' => [ 'type' => 'bigint', 'default' => 0  ],
			]],
		'sellers-tags' =>
			[ 'table' => 'sellers_tags', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'tag_id' => [ 'type' => 'int(12)' ],
				'author_id' => [ 'type' => 'int(12)' ],
				'score' => [ 'type' => 'int(4)' ],
				'city_id' => [ 'type' => 'int(12)', 'default' => 0 ],
				'type' => [ 'type' => 'int(1)', 'default' => 0 ],
			]],
		'sessions' =>
			[ 'table' => 'sessions', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'session_id' => [ 'type' => 'text' ],
				'user_id' => [ 'type' => 'int(11)' ],
				'quiz_id' => [ 'type' => 'int(11)' ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0 ],
				'ip_address' => [ 'type' => 'decimal(39,0)' ],
				'ip_dot' => [ 'type' => 'tinytext' ],
				'user_agent' => [ 'type' => 'mediumtext' ],
				'type' => [ 'type' => 'smallint' ],
				'value' => [ 'type' => 'mediumtext', 'json' => 1],
				'browser_id' => [ 'type' => 'mediumtext' ],
				'data' => [ 'type' => 'longtext', 'json' => 1 ],
				'status' => [ 'type' => 'tinytext' ],
				'access' => [ 'type' => 'int', 'default' => 0 ]
			]],
		'sessions-back' =>
			[ 'table' => 'sessions_back', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'session_id' => [ 'type' => 'varchar(255)', 'attr' => ['UNIQUE'] ],
				'ip_address' => [ 'type' => 'decimal(39,0)' ],
				'value' => [ 'type' => 'longtext' ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
				'added' => [ 'type' => 'timestamp', 'default' => 0 ],
			]],
		'smtp-access-count' =>
			[ 'table' => 'smtp_access_count', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'smtp_id' => [ 'type' => 'int(12)' ], // maps to smtp-server table
				'count' => [ 'type' => 'int(12)'], 
				'date' => [ 'type' => 'tinytext'],
				'flags' => [ 'type' => 'int(11)', 'default' => 0  ],
				'last_sent' => [ 'type' => 'double', 'default' => 000000000.00 ],
			]],
		'smtp-server' =>
			[ 'table' => 'smtp_server', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'name0' => [ 'type' => 'tinytext' ], // name of email server
				'name1' => [ 'type' => 'tinytext' ], // name of email server
				'name2' => [ 'type' => 'tinytext' ], // name of email server
				'meta' => [ 'type' => 'text', 'json' => 1 ],
				'flags' => [ 'type' => 'int(11)', 'default' => 0  ],
				'updated' => [ 'type' => 'timestamp', 'default' => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ],
			]],
		'tags' =>
			[ 'table' => 'tags', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'tag' => [ 'type' => 'text', 'attr' => ['UNIQUE'] ],
				'type' => [ 'type' => 'int(1)', 'default' => 0 ],
				'description' => [ 'type' => 'mediumtext'],
				'icon' => [ 'type' => 'tinytext'],
				'clicks' => [ 'type' => 'int', 'default' => 0],
			]],
		'tags-categories' =>
			[ 'table' => 'tags_categories', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'category' => [ 'type' => 'varchar(255)', 'attr' => ['UNIQUE'] ],
				'description' => [ 'type' => 'mediumtext' ],
				'parent' => [ 'type' => 'int(12)', 'notnull' => true, 'default' => 0],
			]],
		'tags-space' =>
			[ 'table' => 'tags_space', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'north' => [ 'type' => 'decimal(10,7)' ], // lat
				'south' => [ 'type' => 'decimal(10,7)' ], // lat
				'west' => [ 'type' => 'decimal(10,7)' ],  // lng
				'east' => [ 'type' => 'decimal(10,7)' ],  // lng
				'type' => [ 'type' => 'int(1)', 'default' => 0 ],
				'meta' => [ 'type' => 'text', 'json' => 1 ],
			]],
		'tags-taxonomy' =>
			[ 'table' => 'tags_taxonomy', 'primary' => 'id', 'cols' => [
				'id' => [ 'type' => 'int(12)', 'notnull' => true, 'attr' => ['AUTO_INCREMENT'] ],
				'tag_id' => [ 'type' => 'int(12)' ],
				'category_id' => [ 'type' => 'int(12)'],
			]],
	];
	public function __construct(){
		$this->path = '/'.basename(__DIR__).'/'.basename(__FILE__);
		$this->log = null;
		require_once(__DIR__.'/Utility.class.php');
		if (!isset($this->wpdb)){
			if (!defined('SHORTINIT')) define( 'SHORTINIT', true ); // don't load all of Wordpress
			require_once(__DIR__.'/../../../../wp-load.php');
			global $wpdb;
			$this->wpdb = &$wpdb;
		}
		$this->log = new Log(__DIR__."/_logs/tables.log");
	}
	public function __destruct(){
		// if ($this->log !== null)
		// 	fclose($this->log);
	}
	public function log($message, $hide_timestamp = false) {
		if ($this->log !== null)
			$this->log->add($message, $hide_timestamp);
	}
	public function checkAllTables($type = 'all'){
		try {
			$out = array();
			foreach ($this->tables as $table_name=>&$table_info){
				if (!$this->exists($table_name)) {
					try {
						$out[$table_name] = (object)[ 'changes' => [ 'table doesnt exist, creating.'.$this->create($table_name) ] ];
						$this->log("checkAllTables - ".$out[$table_name]->changes[0]);
					}
					catch( \Exception $e) {
						$this->log("checkAllTables - exception: {$e->getMessage()}");
					}
				} else {
					$x = $this->checkTable($table_name, $type);
					if (!empty($x) && count($x)>0) $out[$table_name] = $x;
				}
			}

			$doTheChanges = true; // actually do it or just return what it will do

			if (!$doTheChanges) return $out;
			else {
				$do = $out;
				$out = $y = [];
				foreach ($do as $table=>&$actions) if (!empty($actions->do)) $out[] = $actions->do;
				$do = [];
				foreach ($out as $table_name=>$actions){
					// $toGet = array('add','change','modify');
					$toGet = array('add','change','modify','drop');
					$x = array();
					foreach ($toGet as $a) if (!empty($actions->$a)) foreach ($actions->$a as $b) {
						$r = $this->wpdb->query($b);
						// $r = 1;
						$x[] = array('sql'=>$b, 'result'=>$r);
						$y[] = $b;
					}
					if (count($x)>0) $do[$table_name] = $x;
				}
				return array($do, $y);
			}
		} catch (\Exception $e) { parseException($e); }
	}
	public function checkTable($table_name = null, $type = 'all'){
		if ($table_name === null) throw new Exception('No table name provided.');
		try {
			$out = new \stdClass();
			$out->found = $out->extra = $out->types = $out->foundFields = $out->missing = $out->changes = [];
			$out->do = new \stdClass();
			$table = $this->tables[$table_name];
			foreach ($this->get('table-info', $table_name) as $db_col){
				if (!empty($table['cols'][$db_col->Field])){
					// $out->changes[] = 'found: '.$db_col->Field;
					// column exists in database and $this->tables
					$default = $table['cols'][$db_col->Field];
					$out->found[$db_col->Field] = array($db_col, $default);
					$out->foundFields[] = $db_col->Field;
					foreach ($this->compareColumns($default, $db_col) as $compared_result){
						if (!isset($out->do->modify)) $out->modify = array();
						$out->do->modify[] = $sql = 'ALTER TABLE '.$this->get('table-name', $table_name).' '.$compared_result;
					}
				} else  { // column exists in database but not in $this->tables
					// check the renaming rules to see if it's just an old version of the database
					if ( !empty($this->renamingRules[$table_name]) && !empty($this->renamingRules[$table_name][$db_col->Field]) ){
						if (!isset($out->do->change)) $out->do->change = array();
						$out->do->change[] = $sql = 'ALTER TABLE '.$this->get('table-name', $table_name).' CHANGE '.$db_col->Field.' '.$this->renamingRules[$table_name][$db_col->Field].' '.$table['cols'][$this->renamingRules[$table_name][$db_col->Field]]['type'];
						// $out->changes[] = array('rename', $db_col->Field, $this->renamingRules[$table_name][$db_col->Field]);
						$default = $table['cols'][$this->renamingRules[$table_name][$db_col->Field]];
						$out->found[$db_col->Field] = array($db_col, $default);
						$out->foundFields[] = $this->renamingRules[$table_name][$db_col->Field];
					} else {
						// couldn't find any reason for column to exist
						$out->extra[] = $db_col->Field;
						if (!isset($out->do->drop)) $out->do->drop = array();
						$out->do->drop[] = 'ALTER TABLE '.$this->get('table-name', $table_name).' DROP `'.$db_col->Field.'`';
					}
				}
			}
			foreach ($table['cols'] as $field=>$required){
				if ( empty($out->found[$field]) && !in_array($field, $out->foundFields) ) {
					$out->missing[] = $field;
					// $out->changes[] = $this->get('field-format', $table_name, $required['type']);
					$sql = 'ALTER TABLE '.$this->get('table-name', $table_name) .
						' ADD `'.$field.'` '.$required['type'] .
						( !isset($required['default']) ? null : ' DEFAULT '.$this->prepare($this->get('field-format', $table_name, $field), $required['default'], ($required['default'] == 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ? "" : "'")) );
					if (!empty($required['notnull']) && $required['notnull']) $sql.= ' NOT NULL';
					if (!empty($required['attr']) && is_array($required['attr'])) foreach ($required['attr'] as $attr) $sql.= ' '.$attr;
					if (empty($out->do->add)) $out->do->add = null;
					$out->do->add[] = $sql;
				}
			}
			switch($type){
				case 'change': return $out->do->change; break;
				case 'modify': return $out->do->modify; break;
				case 'drop': return $out->do->drop; break;
				case 'add': return $out->do->add; break;
				case 'all': default: return $out; break;
			}
			// if (!empty($out)) return $out->changes;
			// else return false;
		} catch (\Exception $e) { parseException($e); }
	}
	public function compareColumns($default, $current){
		$checkOthers = false;
		$out = array();
		// check for correct data type
		if ( strcasecmp($current->Type, $default['type']) != 0 ) $out[] = 'MODIFY '.$current->Field.' '.$default['type'];
		// check if null values are allowed
		if ($checkOthers = true){
			$x = $current->Field.' '.$default['type'];
			if ( !empty($default['notnull']) ){
				$current->NotNull = $current->Null == 'YES' ? false : ($current->Null == 'NO' ? true : $current->Null);
				if ($current->NotNull != $default['notnull'])
					$out[] = 'MODIFY '.$x.' NOT NULL';
			}
			if ( !empty($default['default']) && $current->Default == null ) $out[] = 'MODIFY '.$x.' DEFAULT '.(is_numeric($default['default']) ? $default['default'] : "'".$default['default']."'");
			$flags = new \stdClass(); $flags->current = $flags->default = new \stdClass();
			$flags->current->auto_increment = $current->Extra == 'auto_increment' ? true : false;
			$flags->current->primary = $current->Key == 'PRI' ? true : false;
			$flags->current->unique = $current->Key == 'UNI' ? true : false;
			if (!empty($default['attr'])) foreach($default['attr'] as $attr){
				if ($attr == 'AUTO_INCREMENT') $flags->default->auto_increment = true;
				elseif ($attr == 'UNIQUE') $flags->default->unique = true;
			}
		}
		foreach ($flags->current as $key=>$value){
			if (empty($flags->default->$key)) $flags->default->$key = false;
			if ($flags->default->$key !== $value) $out[] = 'MODIFY '.$current->Field.' DEFAULT '.$default['type'].' '.$value;
		}
		return $out;
	}
	public function create($table_name = null){
		$this->log("create - ".($table_name ? $table_name : "N/A"));
		try {
			if (func_num_args() < 1) throw new \Exception('No table name provided.');
			$sql = 'CREATE TABLE '.$this->getTableName($table_name).' (';
				foreach ($this->tables[$table_name]['cols'] as $field=>$info){
					$sql .= $field.' '.$info['type'];
					if (!empty($info['notnull'])) $sql.= ' NOT NULL';
					elseif (isset($info['notnull']) && $info['notnull'] == false) $sql.= ' NULL';
					if (isset($info['attr']) && is_array($info['attr'])) foreach ($info['attr'] as $attr) $sql.= ' '.$attr;
					if (isset($info['default'])) $sql.= ' DEFAULT '.($info['default'] == 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' || is_numeric($info['default']) ? $info['default'] : "'".$info['default']."'" );
					$sql.= ', ';
				}
			if (!empty($this->tables[$table_name]['primary'])) $sql.= 'PRIMARY KEY ('.$this->tables[$table_name]['primary'].')';
			$sql .= ') ENGINE=InnoDB DEFAULT CHARSET=utf8;';
			$x = $this->wpdb->query($sql);
			if (!$x) throw new \Exception('Could not create table '.$table_name.', sql: '.$sql);
			else {
				$this->log("create - sql:$sql");
				return 'created table '.$table_name;
			}
		} catch (\Exception $e) { return parseException($e); }
	}
	public function exists($table_name = null){
		if (empty($table_name)) throw new \Exception('No table name provided.');
		$sql = $this->wpdb->prepare("SHOW TABLES LIKE %s", $this->get('table-name', $table_name));
		$out = $this->wpdb->query($sql);
		return empty($out) ? false : true;
	}
	public function isFieldUnique($table_name = null, $field = null){
		try {
			$table = $this->get('table', $table_name);
			$out = false;
			if (array_key_exists($field, $table['cols']))
				if (array_key_exists('attr',$table['cols'][$field]))
					return in_array('UNIQUE', $table['cols'][$field]['attr']);

			//foreach ($table['cols'] as $col) if ( !empty($col['attr']) && in_array('UNIQUE', $col['attr']) ) { $out = true; break; }
			return $out;
		}
		catch (\Exception $e) { parseException($e); }
	}
	public function isFieldJSON($table_name = null, $field = null){
		try {
			$table = $this->get('table', $table_name);
			$out = false;
			foreach ($table['cols'] as $col) if (!empty($col['json'])) { $out = true; break; }
			return $out;
		}
		catch (\Exception $e) { parseException($e); }
	}
	public function get(){
		try {
			$what = func_get_arg(0);
			if (empty($what)) throw new \Exception('get() did not recieve a valid query');
			$args = array();
			for ($i = 1; $i < func_num_args(); $i++) $args[] = func_get_arg($i);
			switch($what){
				case 'class':
					if (empty($args[0])) throw new \Exception('No class name provided.');
					$class = str_replace(' ', '', ucwords(str_replace('-', ' ', $args[0])));
					$file = __DIR__.'/'.$class.'.class.php';
					if (!file_exists($file)) throw new Exception('File does not exist: '.$file);
					require_once($file);
					$class = "AH\\".$class;
					if (!class_exists($class)) throw new Exception('Invalid class: '.$class);
					return new $class;
					break;
				case 'table-name':
					if (empty($args[0])) throw new \Exception('No table name provided.');
					if (empty($this->tables[$args[0]])) throw new \Exception('Table not registered: '.$args[0]);
					if (empty($this->tables[$args[0]]['table'])) throw new \Exception('Table registered in class, but table name not defined: '.$args[0]);
					return $this->wpdb->prefix.'alr_'.$this->tables[$args[0]]['table'];
					break;
				case 'table-key':
					if (func_num_args() < 2) throw new \Exception('no table name provided.');
					$table_out = null;
					foreach ($this->tables as $name=>$table)
						if ($args[0] == $this->wpdb->prefix.'alr_'.$table['table']){
							$table['name'] = $name;
							$table_out = $table;
							break;
						}
					return $table_out;
					break;
				case 'table':
					if (func_num_args() < 2) throw new \Exception('no table name provided.');
					if (empty( $this->tables[$args[0]] )) throw new \Exception('table not registered: '.$args[0]);
					return $this->tables[$args[0]];
					return $out;
					break;
				case 'table-info':
					if (func_num_args() < 2) throw new \Exception('get(table-info) needs more arguments');
					if ( !$this->exists($args[0]) ) throw new \Exception('table does not exist: '.$this->getTableName($args[0]));
					return $this->wpdb->get_results('SHOW COLUMNS FROM '.$this->getTableName($args[0]));
					break;
				case 'field-format':
					if (empty($args[0])) throw new \Exception('No table name provided.');
					if (empty($this->tables[$args[0]])) {
						$table = $this->get('table-key', $args[0]);
						if (!empty($table)) throw new Exception('Table not registered.');
						$this->get('field-format', $table, $args[1]);
					}
					$out = null;
					if (func_num_args() > 2){
						$types = wp_list_pluck($this->tables[$args[0]]['cols'], 'type');
						if (!array_key_exists($args[1], $types)) throw new \Exception('Field "'.$args[1].'" does not exist in '.$args[0]);
						$out = $types[$args[1]];
						if (empty($out)) throw new \Exception('Field does not exist in Tables class');
						if (strpos($out, 'int') !== false) $out = '%d';
						elseif (strpos($out, 'float') !== false || strpos($out, 'double') !== false || strpos($out, 'decimal') !== false) $out = '%f';
						else $out = '%s';
					} else {
						$out = wp_list_pluck($this->tables[$args[0]]['cols'], 'type');
						$types = array();
						foreach ($out as $type){
							if (strpos($type, 'int') !== false) $types[] = '%d';
							elseif (strpos($type, 'float') !== false || strpos($type, 'double') !== false || strpos($type, 'decimal') !== false) $types[] = '%f';
							else $types[] = '%s';
						}
						$out = $types;
					}
					return $out;
					break;
				default: throw new \Exception('get() invalid query: '.$what); break;
			}
		} catch (\Exception $e) { parseException($e); }
	}
	public function getTableName($tableName){ // shorthand for get('table-name', $tableName)
		try { return $this->get('table-name', $tableName); }
		catch (\Exception $e) { parseException($e); }
	}
	public function migrate($what = null){
		if ($what === null) $this->migrate('all');
		try {
			switch($what){
				case 'all':
					return $this->migrate('listings-tags');
					break;
				case 'quiz':
					// require_once(__DIR__.'/Tags.class.php');
					// require_once(__DIR__.'/QuizSlides.class.php');
					// require_once(__DIR__.'/QuizTags.class.php');
					// require_once(__DIR__.'/QuizQuestions.class.php');
					// $Tags = new Tags(); $QuizTags = new QuizTags(); $QuizSlides = new QuizSlides(); $QuizQuestions = new QuizQuestions();
					// $out = array();
					// $Tags = wp_list_pluck($Tags->get(), 'tag', 'id');
					// foreach ($QuizQuestions->get() as $question){
					// 	$x = new \stdClass(); $x->question_id = $question->id; $x->tags = array();
					// 	$s = $question->slides;
					// 	foreach ($s as $slide){
					// 		$addSlide = array('question_id'=>$question->id, 'file'=>$slide->file, 'title'=>$slide->title);
					// 		$slide_id = $QuizSlides->add($addSlide);
					// 		$out[] = $addSlide;
					// 		$out[] = 'added slide: '.$slide_id;
					// 		if (!empty($slide->tags)) foreach ($slide->tags as $tag){
					// 			$found = false;
					// 			foreach ($Tags as $id=>$db_tag) if ($db_tag == $tag){ $found = $id; break; }
					// 			if ($found !== false) $x->tags[] = $found;
					// 		}
					// 		if (!empty($x->tags)) foreach ($x->tags as $tag_id){
					// 			$tt = array('tag_id'=>$tag_id, 'slide_id'=>$slide_id);
					// 			$out[] = $tt;
					// 			$addTag = $QuizTags->add($tt);
					// 			$out[] = 'added tag: '.$addTag;
					// 		}
					// 	}
					// }
					// return $out;
					break;
				case 'cities':
					require_once(__DIR__.'/Cities.class.php');
					require_once(__DIR__.'/Listings.class.php');
					$Cities = new Cities();
					$Listings = new Listings();
					$out = array();
					$q = new \stdClass(); $q->what = array('id','city','state','zip','country');
					if ($Listings) foreach ($Listings->get($q) as $listing){
						if ($listing->city){
							$data = array();
							if (!empty($listing->city) && $listing->city != -1) $data['city'] = $listing->city;
							if (!empty($listing->state) && $listing->state != -1) $data['state'] = $listing->state;
							if (!empty($listing->zip) && $listing->zip != -1) $data['zip'] = $listing->zip;
							if (!empty($listing->country) && $listing->country != -1) $data['country'] = $listing->country;
							$out[] = $Cities->add($data);
						}
					}
					return $out;
					break;
				case 'points':
					require_once(__DIR__.'/Points.class.php');
					require_once(__DIR__.'/PointsCategories.class.php');
					require_once(__DIR__.'/PointsTaxonomy.class.php');
					$Points = new Points();
					$PointsCategories = new PointsCategories();
					$PointsTaxonomy = new PointsTaxonomy();
					$out = array();

					$Cats = wp_list_pluck($PointsCategories->get(), 'category', 'id');

					foreach ($Points->get() as $point){
						$out[] = $PointsTaxonomy->add( array('point_id'=>$point->id, 'category_id'=>array_search($point->category, $Cats)) );
					}

					// foreach ($Points->get() as $point){
					// 	$point->latlng = json_decode($point->latlng);
					// 	$p = new \stdClass();
					// 	$p->where = array('id'=>$point->id);
					// 	$p->fields = array(
					// 		'lat'=>$point->latlng[0],
					// 		'lng'=>$point->latlng[1]
					// 	);
					// 	if (!empty($point->place_id)) $p->fields['meta'] = array('google'=>array('place_id'=>$point->place_id, 'place_ref'=>$point->place_ref, 'icon'=>$point->icon));
					// 	$out[] = $p;
					// }
					return $out;
					// return $Points->set($out);
					break;
				case 'listings-tags':
					// return $this->migrate('listings-tags-categories');
					// return $this->migrate('listings-tags-copy-from-listings');
					// require_once(__DIR__.'/TagsTaxonomy.class.php');
					// $TagsTaxonomy = new TagsTaxonomy();
					break;
				case 'tags-cats-to-ids':
					require_once(__DIR__.'/TagsCategories.class.php');
					require_once(__DIR__.'/TagsTaxonomy.class.php');
					$TagsTaxonomy = new TagsTaxonomy();
					$TagsCategories = new TagsCategories();
					$cats = $TagsCategories->get();
					$tax = $TagsTaxonomy->get();
					$out = array();
					foreach ($tax as $t){
						$out[] = $t;
					}
					// foreach ($cats as $cat){
					// 	$out[] = $cat;
					// }
					return $tax;
					break;
				case 'listings-tags-copy-from-listings':
					require_once(__DIR__.'/Listings.class.php');
					require_once(__DIR__.'/Tags.class.php');
					require_once(__DIR__.'/ListingsTags.class.php');
					$Tags = new Tags();
					$Listings = new Listings();
					$ListingsTags = new ListingsTags();
					$listingTagsDB = wp_list_pluck($Listings->get(), 'tags', 'id');
					$tagsDB = $Tags->get();
					try {
						$out = array();
						foreach ($listingTagsDB as $listing_id=>$db_tags) if (!empty($db_tags)) {
							foreach ($db_tags as $listingTag){
								foreach ($tagsDB as $t) if ($t->tag == $listingTag){ $tag_id = $t->id; break; }
								if (!isset($tag_id)) $tag_id = $Tags->add(array('tag'=>$listingTag));
								$out[] = $ListingsTags->add(array('tag_id'=>$tag_id,'listing_id'=>$listing_id));
							}
						}
						return $out;
					} catch (\Exception $e){
						return $e;
					}
					break;
				case 'listings-tags-categories':
					require_once(__DIR__.'/TagsCategories.class.php');
					$TagsCategories = new TagsCategories();
					require_once(__DIR__.'/TagsTaxonomy.class.php');
					$TagsTaxonomy = new TagsTaxonomy();
					$out = array();
					try {
						foreach ($TagsTaxonomy->get() as $cat){
							try {
								if (!$TagsCategories->exists(array('category'=>$cat->category)))
									$out[] = $TagsCategories->add(array('category'=>$cat->category));
							}
							catch (\Exception $e) { print_r($e); exit(); }
						}
						return $out;
					} catch (\Exception $e) { print_r($e); exit(); }
					break;
				default: throw new \Exception('unable to find migrate pattern for '.$what); break;
			}
		} catch (\Exception $e){ parseException($e); }
	}
	public function prepare($field_format = null, $value = null, $replace_quotes_with = null, $encode_json = false){
		if (empty($value) && intval($value) !== 0) throw new \Exception('No value passed to prepare.');
		$found = false; foreach (array('%s','%d','%f','for-replace') as $f) if ($field_format == $f){ $found = true; break; }
		if (!$found) throw new \Exception('Invalid field format: '.$field_format); unset($found);
		$out = null;
		if ($encode_json && (is_array($value) || is_object($value)) ) $value = json_encode($value);
		$out = $this->wpdb->prepare($field_format, $value);
		if (empty($out) && $out !== '0') throw new \Exception('Prepare returned empty field');
		$out = empty($replace_quotes_with) && $replace_quotes_with !== '' ? $out : str_replace("'", $replace_quotes_with, $out);
		return empty($out) && $out !== '0' ? false : $out;
	}
}}
namespace {
	function getTableName($theName = null){
		$tables = new AH\Tables();
		return $tables->getTableName($theName);
	}
}