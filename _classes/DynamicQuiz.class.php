<?php
namespace AH;

require_once(__DIR__.'/../_classes/_Controller.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php');
require_once(__DIR__.'/../_classes/States.php');

class DynamicQuiz extends Controller {
	public function __construct($logIt = 0) {
		parent::__construct();
		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'NO_ASK_ALLOW_MIN_PRICE']]);
		$this->minPrice = !empty($opt) ? intval($opt[0]->value) : 50000;
		$this->debugLevel = 3;
		if ($logIt)
			$this->log = new Log(__DIR__."/_logs/dynamicQuiz.log");
	}
	
	public function parseQuizKey($optName, $key, $delimiter = '/') {
		$activity = rawurlencode( substr($optName, 5) );
		$parts = explode('-', $key);
		$index = 0;
		$base = '';
		$price = '';
		$distance = '';
		$beds = '';
		$city = [];
		$tagList = [];

		$Tags = $this->getClass('Tags')->get();
		$x = [];
		foreach($Tags as $tag) {
			$tag->tag = rawurlencode( strtolower($tag->tag) );
			$x[$tag->id] = $tag;
		}
		unset($Tags);
		$Tags = $x;
		$tagKeys = array_keys($Tags);

		foreach($parts as $sub) {
			if ($index == 0) 
				$base = $sub;
			elseif ( strpos($sub, 'beds') !== false)
				$beds = $sub;
			elseif ( $sub[0] == 'd' &&
					 is_numeric($sub[1])) 
				$distance = substr($sub, 1);
			elseif (is_numeric($sub) &&
					strpos($sub, ':') === false ) {
				$value = intval($sub);
				if ($value > $this->minPrice)
					$price = $value;
				else // single tag
					$tagList[] = $value;
			}
			elseif ( strpos($sub, ':') !== false ) {// must be taglist
				$parts = explode(':', $sub);
				foreach($parts as $tag) {
					if (!empty($tag))
						$tagList[] = $tag;
				}
			}
			else {
				$city[] = rawurlencode($sub);
			}

			$index++;
		}

		$output = $activity.($base == 'All' ? '' : $delimiter.$base);
		if (!empty($city)) foreach($city as $loc)
			$output .= $delimiter.$loc;
		if (!empty($price))
			$output .= $delimiter.$price;
		if (!empty($distance) &&
			!empty($city))
			$output .= $delimiter.$distance;
		if (!empty($tagList)) foreach($tagList as $tag)
			$output .= $delimiter.$Tags[$tag]->tag;
		if (!empty($beds))
			$output .= $delimiter.$beds;

		return $output;
	}

	protected function isState(&$state) {
		global $usStates;
		$state = strtoupper($state);
		if (!in_array($state, $usStates)) {
			$state = strtolower($state);
			if (array_key_exists ( ucwords($state), $usStates )) {
				$state = $usStates[ucwords($state)];
				return true;
			}
			return false;
		}
		else
			return true;
	}

	protected function parseQuizParameters($url, $indexStart, $param) {
		$Tags = $this->getClass('Tags')->get();
		$x = [];
		foreach($Tags as $tag) {
			$tag->tag = strtolower($tag->tag);
			$x[$tag->tag] = $tag;
		}
		unset($Tags);
		$Tags = $x;
		$tagKeys = array_keys($Tags);
		$tagList = [];

		for($i = $indexStart; $i < $param->len; $i++) {
			$value = $url[$i];
			if (empty($value))
				continue;
			if ( ($isState = $this->isState($value)) )
				$param->state = $value;
			elseif ( is_numeric($value) ) {
				$value = intval($value);
				if ($value > $param->minPrice) { 
					if (!isset($param->fields['price']))
						$param->fields['price'] = [$param->minPrice, $value];
				}
				elseif ($value <= 80)
					$param->fields['distance'] = $value;
				else
					$this->log("parseQuizParameters numeric: $value below minPrice:$param->minPrice for $param->optName", 2);
			}
			else {
				$value = strtolower( rawurldecode($value) ); // in case there are %20 nested in string
				$value = str_replace('_', ' ', $value);
				if ( in_array($value, $tagKeys)) {
					$tagList[$Tags[$value]->id] = $Tags[$value];
				}
				elseif ( ($pos = strpos($value, 'beds')) !== false &&
						  $pos !== 0 ) {
					$beds = substr($value, 0, $pos);
					$param->fields['homeOptions'] = ['beds'=>$beds, 'baths' => 0];
				}
				elseif (!empty($param->state)) { // check city
					$city = $this->getClass('Cities')->get((object)['where'=>['city'=>$value,
																			  'state'=>$param->state]]);
					if (!empty($city)) {
						$param->cityName .= (strlen($param->cityName) ? "-" : '').$city[0]->city;
						if (!isset($param->fields['location']))
							$param->fields['location'] = [];
						$param->fields['location'][] = $city[0]->id;
						if (!isset($param->fields['distance']))
							$param->fields['distance'] = 8;
						$this->log("parseQuizParameters added {$city[0]->city} to location array",2);
					}
					else
						$this->log("parseQuizParameters could not find $value in Cities table, using state:$param->state for $param->optName",2);		
				}
				else
					$this->log("parseQuizParameters irregular $value for $param->optName",2);
			}
		}

		if (!empty($tagList)) {
			usort($tagList, function($a, $b) { return $a->id - $b->id; });
			$tagString = '';
			$tagDescString = '';
			$param->fields['tags'] = [];
			foreach($tagList as $tag) {
				$tagString .= (strlen($tagString) ? ':' : '').$tag->id;
				$tagDescString .= (strlen($tagDescString) ? ', ' : '').$tag->tag;
				$param->fields['tags'][] = (object)['tag' => $tag->tag,
													'id' => $tag->id,
													'percent' => $tag->type == 1 ? 80 : 50,
													'flag'	=> 0];
			}
			$param->tagString = $tagString;
			$param->tagDescString = $tagDescString;
		}
	}

	protected function getKey($base, $city, $price, $tags, $homeOptions, $distance) {
		$key = $base;
		$key.= isset($city) && !empty($city) ? "-$city" : '';
		$key.= isset($price) && !empty($price) ? "-$price" : '';
		$key.= isset($tags) && !empty($tags) ? "-$tags" : '';
		$key.= isset($homeOptions) && !empty($homeOptions) ? "-$homeOptions" : '';
		if ( isset($city) && !empty($city) )
			$key.= isset($distance) && !empty($distance) ? "-d$distance" : '';

		return $key;
	}

	protected function getQuizId($quizSet, $base, $city = '', $price = '', $tags = '', $homeOptions = '', $distance = '') {
		$key = $this->getKey($base, $city, $price, $tags, $homeOptions, $distance);
		if (isset($quizSet[$key])) 
			return intval($quizSet[$key]);
		else 
			return 0;
	}


	public function generateNewQuiz($url, $indexStart, $Options, &$reuse) {
		$reuse = false;
		$optName = "Quiz_{$url[$indexStart]}";
		$opt = $Options->get((object)['where'=>['opt' => $optName]]);
		if ( empty($opt) ) {
			$this->log("generateNewQuiz did not find entry for $optName");
			return 0;
		}
		$quizSet = json_decode($opt[0]->value, true);
		$quiz_id = $this->getQuizId($quizSet, 'All'); // this is the base quiz to diverge from
		$len = count($url);
		if ( $len == ($indexStart+1)) {// then simple nationwide
			$this->log("generateNewQuiz found nationwide only quiz:$quiz_id for $optName");
			return $quiz_id;
		}

		$param = new \stdClass();
		$param->optName = $optName;
		$param->len = $len;
		$param->state = '';
		$param->cityName = '';
		$param->simpleStateQuiz = false;
		$param->nationwideQuiz = false;
		// $opt = $Options->get((object)['where'=>['opt'=>'NO_ASK_ALLOW_MIN_PRICE']]);
		$param->minPrice = $this->minPrice;
		$this->log("minPrice - ".$param->minPrice);
		$param->fields = [];
		$value = 0;

		// is next word a state?
		$indexStart++;
		$this->parseQuizParameters($url, $indexStart, $param);


		if ( empty($param->fields['location']) ) {
			if ( empty($param->state) )
			 	$param->fields['quiz'] = 1; // national
			else {
			 	$param->fields['quiz'] = 5; // state
			 	$param->fields['state'] = $param->state;
			 }
		}
		else
			$param->fields['quiz'] = 0; // city

		$pricekey = empty($param->fields['price']) ? '' : $param->fields['price'][1];
		$tagKey = empty($param->tagString) ? '' : $param->tagString;
		$homeOptions = empty($param->fields['homeOptions']) ? '' : $param->fields['homeOptions']['beds'].'beds';
		$base = empty($param->state) ? 'All' : $param->state;
		$cityName = empty($param->cityName) ? '' : $param->cityName;
		$distance = empty($param->fields['distance']) ? '' : $param->fields['distance'];
		$quiz_find = $this->getQuizId($quizSet, $base, $cityName, $pricekey, $tagKey, $homeOptions, $distance);

		if (!empty($quiz_find)) {
			$activity = $this->getClass('QuizActivity')->get((object)['where'=>['id'=>$quiz_find]]);
			if (empty($activity)) {
				$this->log("generateNewQuiz did not find QuizActivity from getQuizId()::id - $quiz_find for $optName");
			}
			else {
				$activity = array_pop($activity);
				$diff = time() - strtotime($activity->updated);
				if ( $diff < (3600 * 24 * 7)) // updated less than a week ago, then reuse results
					$reuse = true;
				$this->log("generateNewQuiz found existing for $base, city:".(empty($cityName) ? 'N/A' : $cityName)." price:".(empty($pricekey) ? 'N/A' : $pricekey).", tagKey:".(empty($tagKey) ? "N/A" : $tagKey).", homeOptions:".(empty($homeOptions) ? "N/A" : $homeOptions).", quiz:$quiz_find for $optName");
				return $quiz_find;
			}
		}

		// then need to generate new quiz_id
		// create new quiz out of $quiz_id
		$activity = $this->getClass('QuizActivity')->get((object)['where'=>['id'=>$quiz_id]]);
		if (empty($activity)) {
			$this->log("generateNewQuiz did not find QuizActivity at $quiz_id for $optName");
			return 0;
		}
		$activity = array_pop($activity);
		$quiz_id = $this->createNewQuiz($param->fields, 
										 $activity);

		$key = $this->getKey($base, $cityName, $pricekey, $tagKey, $homeOptions, $distance);
		$quizSet[$key] = $quiz_id;

		// rearrange so it's sorted by keys
		$keys = array_keys($quizSet);
		sort($keys);
		$x = [];
		foreach($keys as $key)
			$x[$key] = $quizSet[$key];
		unset($quizSet);
		$quizSet = $x;

		$Options->set([(object)['where'=>['opt' => $optName],
								'fields'=>['value'=>json_encode($quizSet)]]]);

		return $quiz_id;
	}

	protected function createNewQuiz($fields, $activity) {
		$size = count($activity->data);
		if (!$size) {
			$this->log("createNewQuiz - base activity:$activity->id has no data");
			return 0;
		}

		$this->log("createNewQuiz - fields:".print_r($fields, true));
			

		$last_quiz = $activity->data[ $size-1]; // pop the last quiz
		$last_results = array_pop($last_quiz->results); // pop the last result set

		$new_results = clone $last_results;
		$new_quiz = clone $last_quiz;
		$new_results->quiz = $last_quiz->quiz;

		foreach (['tags', 'location', 'price', 'distance', 'sessionID','homeOptions','state','quiz'] as $field)
			if ( isset($fields[$field]) ){
				if ($field == 'tags') {
					if ($fields[$field] == 'notags')
						$new_results->$field = $last_results->tags;
					else
						$new_results->$field = array_merge($fields[$field], $last_results->tags);
				}
				else
					$new_quiz->$field = $fields[$field];
			}

		if ( intval($new_quiz->quiz) == 1 ) { // nationwide
			if ( isset($new_quiz->state) ) unset($new_quiz->state);
			if ( isset($new_quiz->location) ) unset($new_quiz->location);
			if ( isset($new_quiz->distance) ) unset($new_quiz->distance);
		}
		elseif ( intval($new_quiz->quiz) == 0 ) { // city
			if ( isset($new_quiz->state) ) unset($new_quiz->state);
		}
		elseif ( intval($new_quiz->quiz) == 5 ) { // state
			if ( isset($new_quiz->location) ) unset($new_quiz->location);
			if ( isset($new_quiz->distance) ) unset($new_quiz->distance);
		}

		// account for a city having muliple entries in Cities table for one reason or another.
		// So if the distance is zero, we won't find all the duplicate cities involved, so go
		// query for duplicate cities and expande the location array accordingly
		$cityLoc = [];
		// $quiz = gettype($new_results->quiz) == 'string' ? intval($new_results->quiz) : $new_results->quiz;
		$quiz = gettype($new_results->quiz) == 'string' ? (int)$new_results->quiz : $new_results->quiz;
		if (($quiz % 2) == 0 &&
			isset($new_results->location) &&
			empty($new_results->distance) ) {
			$loc = is_array($new_results->location) ? $new_results->location : [$new_results->location];
			foreach($loc as $id) {
				$city = $this->getClass('Cities')->get((object)['where'=>['id'=>$id]]);
				$cities = $this->getClass('Cities')->get((object)['where'=>['city'=>$city[0]->city,
																			'state'=>$city[0]->state]]);
				foreach($cities as $city)
					$cityLoc[$city->id] = $city->city;
			}
			$new_quiz->location = array_keys($cityLoc);
		}

		if ( isset($new_results->city_sorted) ) unset($new_results->city_sorted);
		if ( isset($new_results->listings) ) unset($new_results->listings);
		if ( isset($new_results->cities) ) unset($new_results->cities);
		if ( isset($new_results->total_listings_found) ) unset($new_results->total_listings_found);
		if ( isset($new_results->admitted) ) unset($new_results->admitted);
		$new_results->quiz =$new_quiz->quiz;
		$new_quiz->results = [$new_results];
		$new_quiz->created = date("Y-m-d H:i:s");
		$new_quiz->updated = date("Y-m-d H:i:s");

		$id = 0;
		$SessionMgr = $this->getClass('SessionMgr');
		$session_id = $SessionMgr->getCurrentSession($id);
		if (!($row = $this->getClass('QuizActivity')->add((object) array(
																		'session_id' => $id,
																		'status' => 'done',
																		'data' => [$new_quiz]
																	))) ) {
			$this->log('createNewQuiz - unable to save quiz action.');
			return 0;
		}
		return $row;
	}
}
