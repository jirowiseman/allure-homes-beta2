<?php
namespace AH;
require_once(__DIR__.'/../_classes/Utility.class.php');

class Controller {
	protected $log = null;
	protected $debugLevel = 3;

	public function __construct(){
		global $timezone_adjust;
		$this->timezone_adjust = $timezone_adjust;

		global $query_packet;
		if ($query_packet !== null)
			return $query_packet;

		global $data_packet;

		try {
			// date_default_timezone_set('America/Los_Angeles');
			$query = $data = null;
			$call_type = 'runtime';
			if (func_num_args() > 0){
				$query = func_get_args()[0];
				$data = func_num_args() > 1 ? func_get_args()[1] : null;

				if (isset($_POST['query'])) unset($_POST['query']);
				if (isset($_POST['data'])) unset($_POST['data']);

			} elseif (isset($_POST['query'])) {
				$query = $_POST['query']; unset($_POST['query']);
				if (isset($_POST['data'])) { $data = $_POST['data']; unset($_POST['data']); }
				else $data = null;
				$call_type = 'post';
			} elseif (isset($_GET['query'])) {
				$query = $_GET['query']; unset($_GET['query']);
				if (isset($_GET['data'])) { $data = $_GET['data']; unset($_GET['data']); }
				else $data = null;
				$call_type = 'get';
			}

			if (isset($query)) {
				$query_packet = (object) array('query'=>$query, 'data'=>$data, 'call_type'=>$call_type);
			 	return $query_packet;
			}
			elseif (!empty($_POST) &&
					empty($data_packet)) {
				$data_packet = new \stdClass();
				$data_packet->data = [];
				$data_packet->data['type'] = "POST";
				foreach($_POST as $key=>$value)
					$data_packet->data[$key] = $value;
			}
			elseif (!empty($_GET) &&
					empty($data_packet)) {
				$data_packet = new \stdClass();
				$data_packet->data = [];
				$data_packet->data['type'] = "GET";
				foreach($_GET as $key=>$value)
					$data_packet->data[$key] = $value;
			}
		} catch (\Exception $e) { parseException($e); die(); }
	}

	public function log($message, $debugLevel = 3, $hide_timestamp = false, $tab = 0) {
		if ($this->log !== null &&
			$debugLevel >= $this->debugLevel) {
			if ($debugLevel < 3)
				$message .= ", debugLevel:$debugLevel";
			for($i = 0; $i < $tab; $i++)
				$message = "\t".$message;
			$this->log->add($message, $hide_timestamp);
		}
	}

	public function clear() {
		if ($this->log !== null)
			$this->log->clear();
	}

	public function flush() {
		if ($this->log != null)
			$this->log->writeStringToFile();
	}

	public function setDebugLevel($level) { $this->debugLevel = $level; }

	public function runQuery(){
		return call_user_func_array(array($this, '__construct'), func_get_args());
	}

	public function getClass($what = null, $option = null, $option2 = null){
		if (isset($this->$what)) return $this->$what;
		else {
			try {
				require_once(__DIR__.'/../_classes/'.$what.'.class.php');
			}
			catch(\Exception $e) {
				parseException($e);
				return null;
			}
			$x = 'AH\\'.$what;
			$this->$what = $option2 != null ? new $x($option, $option2) : ($option != null ? new $x($option) : new $x());
			return $this->$what;
		}
		unset($what);
	}

	public function userIP() {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		    $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = $_SERVER['REMOTE_ADDR'];
		}

		if ( ($pos = strpos($ip, ',')) !== false)
			$ip = substr($ip, 0, $pos);
		
		return $ip;
	}

	public function exitNow($msg) {
		ignore_user_abort(true); // just to be safe
		set_time_limit(0);
		ob_start();
		$out = array("status" => 'OK',
					"data"=>$msg);
		echo json_encode($out);
		$size = ob_get_length();
        header("Connection: close\r\n");
		header("Content-Length: $size\r\n");
		header("Content-Encoding: none\r\n" );//disable apache compressed

		ob_end_flush(); // Strange behaviour, will not work
		ob_flush();
		flush(); // Unless both are called !
	}
}