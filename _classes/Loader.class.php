<?php
namespace AH;

require_once(__DIR__.'/../_classes/ActivityHelper.class.php');
require_once(__DIR__.'/../../../../wp-includes/ms-functions.php');

global $doPortalLanding;
$doPortalLanding = true;
global $browser;
$browser = getBrowser();

class Loader extends ActivityHelper {
	// comes from Controller now
	//protected $timezone_adjust = -7;
	private $pages = array( // Stores all the pages in their heirarchy & required scripts
		'pages'=>array(
			'base_url'=>'_pages/page',
			'children'=>array(
				array('title'=>'Home', 'req'=>array('bxslider','jquery-mobile-slider')),
				array('title'=>'About'),
				array('title'=>'Contact'),
				array('title'=>'Terms'),
				array('title'=>'Agent Terms'),
				array('title'=>'Quiz', 'req'=>array('locationSelector','addressSelector')),
				array('title'=>'Quiz Results', 'req'=>array('jquery-ui-resizable','rating','locationSelector','google-maps-api-js','mask','fbInit','portalUserRegistration','controllerQuizResults')),
				array('title'=>'Quiz Results New', 'req'=>array('jquery-ui-resizable','rating','locationSelector','google-maps-api-js','mask')),
				array('title'=>'Listing', 'req'=>array('bxslider','sly','google-maps-api-js','jquery-mobile-slider')),
				array('title'=>'Listing New', 'req'=>array('bxslider','sly','video','google-maps-api-js','jquery-mobile-slider')),
				array('title'=>'Listings Viewer', 'req'=>array('bxslider','sly')),
				array('title'=>'Listing Gallery', 'req'=>array('bxslider')),
				array('title'=>'Explore the Area'),
				array('title'=>'Explore the Area New', 'req'=>array('google-maps-api-js')),
				array('title'=>'Register', 'req'=>array('mask')),
				array('title'=>'Seller Create'),
				array('title'=>'Tips and Hints'),
				array('title'=>'Special Register'),
				array('title'=>'FAQ'),
				array('title'=>'Agent'),
				array('title'=>'Cart'),
				array('title'=>'Product'),
				array('title'=>'Shop'),
				array('title'=>'My Account'),
				// array('title'=>'Checkout', 'req'=>array('checkout')),
				array('title'=>'Checkout', 'req'=>array('mask')),
				array('title'=>'Transaction Failed'),
				array('title'=>'Purchase Confirmation'),
				array('title'=>'Portal Landing', 'req'=>array('bxslider','jquery-mobile-slider','fbInit','portalUserRegistration')),
				array('title'=>'Portal Landing One', 'req'=>array('bxslider','jquery-mobile-slider','fbInit','portalUserRegistration')),
				array('title'=>'Portal Landing Two', 'req'=>array('bxslider','jquery-mobile-slider','fbInit','portalUserRegistration')),
				array('title'=>'Agent Benefits'),
				array('title'=>'Agent Reservation', 'req'=>array('mask')),
				array('title'=>'Agent Reserve Portal', 'req'=>array('mask')),
				array('title'=>'Agent Get Portal', 'req'=>array('mask')),
				array('title'=>'Agent Get Lifestyle', 'req'=>array('mask')),
				array('title'=>'Agent Basic Lifestyle Info', 'req'=>array('dropzone','mask','jcrop')),
				array('title'=>'Directory', 'req'=>array('mask','google-maps-api-js','allure-thickbox','Chart', 'dragselect', 'sales')),
			)
		),
		'sellers'=>array(
			'base_url'=>'_sellers/',
			'title'=>'Seller',
			'children'=>array(
				array('title'=>'Seller Admin', 'slug'=>'sellers', 'url'=>'index', 'req'=>array('dropzone','mask','jcrop','filesaver','clipboard','clipboardff','dashboard','controllerPortalCaptureSetup', 'controllerManageCompanies')),
					  	// 'children'=>array(
				array('title'=>'New Listing', 'slug'=>'new-listing', 'req'=>array('dropzone','mask','google-maps-api-js','jquery-ui-sortable'))
							// array('title'=>'Shop', 'slug'=>'shop', 'req'=>array('dropzone','mask','jcrop'))
						// )		
			)
		),
		'admin'=>array( // WordPress admin pages
			'base_url'=>'_admin/',
			'title'=>'Allure Admin',
			'children'=>array(
				array('title'=>'Explore The Area'),
				array('title'=>'Listings'),
				array('title'=>'Listings Viewer'),
				array('title'=>'Sellers Management', 'req'=>array('filesaver', 'Chart')),
				array('title'=>'Sales', 'req'=>array('mask','google-maps-api-js', 'Chart','dragselect')),
				array('title'=>'Analytics', 'req'=>array('mask','google-maps-api-js', 'Chart','date-picker','filesaver')),
				array('title'=>'Quiz', 'req'=>array('dropzone','jquery-ui-sortable')),
				array('title'=>'Tags', 'req'=>array('jquery-ui-sortable')),
				array('title'=>'Cities', 'req'=>array('dropzone')),
				array('title'=>'Options Editor'),
				// array('title'=>'EDD'),
				array('title'=>'Developer', 'req'=>array('filesaver', 'Chart')),
				array('title'=>"TomDev")
			)
		)
	);
	private $toRegister = array( // Stores list of scripts/styles/functions to be registered across the site
		'noPack' =>
			array('jquery', 'jquery-ui'),
		'always' => 					// Always load
			array('mask','perfect-scrollbar', 'jquery-ui-vader', 'jquery-ui-touch-punch', 'pages'),
		'page-load-head' => 	// Front end page load (header)
			// array('jquery-ui', 'modernizr', 'allure-js', 'allure-thickbox', 'google-fonts', 'perfect-scrollbar', 'entypo', 'allure-stylesheet','header'),
			array('allure-js', 'allure-thickbox', 'google-fonts', 'entypo','allure-stylesheet','header_common','header','checkSellerInDB'),
		'page-load-footer' => // Front end page load (footer)
			array('google-analytics'),
		'admin' => 						// Allure Specific WP Admin
			// array('jquery', 'jquery-ui', 'allure-thickbox','entypo'),
			array('allure-thickbox','entypo','header_common','header_admin'),
		'sellers' => 					// Sellers pages
			// array('jquery', 'jquery-ui','mask','allure-thickbox', 'google-fonts', 'perfect-scrollbar', 'entypo'),
			array(),
		'install'=> 					// Installation / Integrity checks
			array('permalink-structure', 'pages', 'seller-role'),
		'construction'=> 			// When not logged in and under construction
			// array('jquery', 'allure-stylesheet')
			array('allure-stylesheet')
	);

	private $mapPage = ['agent-purchase-lifestyle'=>'agent-get-lifestyle',
						'agent-purchase-portal'=>'agent-get-portal'];

	// for city, run rawurldecode() to convert to readable ascii
	// private $siteMapKeys = [
	// 	'listing', // catch this first, so we can go to a listing, instead of call DynamicQuiz
	// 	'wine tasting',
	// 	'golf',
	// 	'skiing',
	// 	'ocean boating',
	// 	'surfing',
	// 	'beach',
	// 	'equestrian',
	// 	'casinos',
	// 	'freshwater boating',
	// 	'lake boating',
	// 	'fishing',
	// 	'hunting',
	// 	'hiking',
	// 	'biking',
	// 	'scuba',
	// 	'nascar',
	// 	'road_racing',
	// 	'kayak',
	// 	'inline skating',
	// 	'ice skating',
	// 	'mlb baseball',
	// 	'fifa soccer',
	// 	'nhl hockey',
	// 	'nfl football',
	// 	'nba basketball'
	// ];

	public function __construct($input = null){
		parent::__construct();
		require_once(__DIR__.'/Utility.class.php');
		global $browser;

		// $browser = getBrowser();
		$this->logIt = $browser['userAgent'] != 'Site24x7' ? 1 : 0;
		try {
			$this->log_to_file = $this->logIt;
			$this->debugLevel = 3;
			$this->log = $this->log_to_file ? new Log(__DIR__.'/_logs/Loader.class.log') : null;
			$referer = isset($_SERVER["HTTP_REFERER"]) ? (!empty($_SERVER["HTTP_REFERER"]) ? strtolower($_SERVER["HTTP_REFERER"]) : "") : '';
			$uri = $_SERVER['REQUEST_URI'];
			if ( strpos($referer, 'drones.com') !== false ||
			     strpos(get_home_url(), 'drones.com') !== false ||
			     strpos(get_home_url(), '192.168.1.103') !== false ||
			     strpos($referer, 'colo-63') !== false ) {
				$msg = "*** intruder - home:".get_home_url()." attempt in constructor - referer:$referer, home:".get_home_url().", ip:".$this->userIP().", uri:$uri";
				$this->log($msg);
				$this->getClass('Email')->textMessage('8312344865', "Unauthorized site access", $msg);
				echo "Screw YOU TOO!";
				die;
			}
			
			$this->sendNotMinified = strpos(get_home_url(), 'local') !== false ||
								  	 strpos(get_home_url(), 'alpha') !== false ||
								  	 strpos(get_home_url(), 'mobile') !== false;

			$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'ForceNoMin']]);
			$this->sendNotMinified = empty($opt) ? $this->sendNotMinified : intval($opt[0]->value);
			// $this->log("Loader constructor entered, userAgent:{$browser['userAgent']}, $ip:{$this->userIP()}");
			// $this->getClass('Sellers', 1);
			// $_SERVER['HTTPS'] = 'off';
			if (!headers_sent())
				header('X-Frame-Options: GOFORIT');
			$Options = $this->getClass('Options');
			$opt = $Options->get((object)['where'=>['opt'=>'BufferJsAndCss']]);
			$this->bufferJs =  (!empty($opt) ? intval($opt[0]->value) : 0);
			if (is_admin())
				$this->bufferJs = false;

			$opt = $Options->get((object)['where'=>['opt'=>'SellersLogFromLoader']]);
			$this->logSellers =  (!empty($opt) ? intval($opt[0]->value) : 0);

			// PortalLandingBxSlider
			$opt = $Options->get((object)['where'=>['opt'=>'PortalLandingBxSlider']]);
			$this->portalLandingBxSlider = empty($opt) ? [] : json_decode($opt[0]->value);

			if (is_admin()) {
				add_action('admin_menu', array($this, 'load-init'));
				if ($this->is_allure_admin()) {
					add_action('admin_menu', array($this, 'load-admin'));
					add_action('admin_menu', array($this, 'load-page-'.$_GET['page']));
				}
			} else {
				force_ssl_content(false);
				$this->get('session');
				add_action('wp', array($this, 'load-init'));
			}

		} catch(\Exception $e) { parseException($e); die(); }
	}

	public function __destruct(){
		// $this->log("Loader __destruct entered");
	}

	public function __call($method, $args){
		if (substr($method, 0, 5) == 'load-') {
			if (substr($method, 5, 5) == 'page-') $this->register('allure-admin-'.substr($method, 10));
			else $this->load(substr($method, 5));
		}
	}
	public function is_allure_admin(){
		$found = false;
		if (is_admin()) foreach ($this->pages['admin']['children'] as $page)
				if ( !empty($_GET['page']) && $this->title2slug($page['title']) == $_GET['page'] ){ $found = true; break; }
		return $found;
	}

	protected function formatName(&$data) {
		$data = trim($data);
		$data = str_replace('-',' ',strtolower(trim($data)));
		$data = str_replace('  ',' ',strtolower(trim($data)));
		$t = explode(' ', $data);
		$data = '';
		$doNotUpperFirst = ['the','for','by','on'];
		foreach ($t as $i=>$str){
			if ($i > 0) $data .= ' ';
			if ($i < 1 || !in_array($str, $doNotUpperFirst)) $str = ucfirst($str);
			$data .= $str;
		} unset($i, $t, $str);
		return $data;
	}

	protected function checkPortalName($portal) {
		$portal = strtolower($portal);
		$blogusers = get_users( array( 'search' => $portal ) );
		$havePortal = false;
		if (!empty($blogusers)) foreach($blogusers as $user) {
			if (strtolower($user->user_nicename) == $portal &&
				strtolower($user->nickname) == $portal) {
				return [$user];
			}
		}
		return $havePortal;
	}

	public function get($what = null, $extra = null){
		global $browser;
		try {
			switch($what){
				case 'cities-and-counts':
					$Cities = $this->getClass('Cities');
					$CitiesDB = $Cities->get((object)[ 'what' => ['id','city','state','count','lat','lng'], 'orderby' => 'count' ]);
					// $CitiesDB = $Cities->get((object)[ 'what' => ['id','city','state','count','lat','lng'], 'where' => [ 'lat' => 'notnull' ], 'orderby' => 'count' ]);
					if (empty($CitiesDB)) throw new \Exception('No cities found.');
					$cities = [];
					foreach ($CitiesDB as &$row) {
						// if ($row->count < 1 || ($row->lat == -1 && $row->lng == -1)) unset($row);
						if (empty($row->lat) || ($row->lat == -1 && $row->lng == -1)) unset($row);
						else {
							$cities[$row->id] = (object)[ 'id' => $row->id,
														  'label'=>$row->city.', '.$row->state, 
														  'value'=>$row->city.', '.$row->state, 
														  'count'=>$row->count, 
														  'lng' => $row->lng,
														  'lat' => $row->lat ];
							unset($row);
						}
					} unset($CitiesDB);
					$cities = array_reverse($cities);
					return $cities;
					break;

				case 'page':
					$found = false;
					$slug = '';
					$out = new \stdClass();

					foreach ($this->pages as $page_type=>$pages)
						if ($page_type != 'admin') foreach ($pages['children'] as $page){
							$slug = empty($page['slug']) ? $this->title2slug($page['title']) : $page['slug'];
							if (is_page($slug)){
								$found = true;
								$out->type = $page_type;
								$out->slug = $slug; // == 'register' ? 'home' : $slug; // force to home if 'register'
								$out->base_url = $pages['base_url'];
								break;
							}
							elseif (!$found && !empty($page['children'])) foreach($page['children'] as $child){
								$slug = empty($child['slug']) ? $this->title2slug($child['title']) : $child['slug'];
								if (is_page($slug)){ $found = true; $out->type = $page_type; $out->slug = get_post()->post_name; $out->base_url = $pages['base_url']; break; }
							}
							if ($found) break;
						}

					// $browser = getBrowser();
					$referer = isset($_SERVER["HTTP_REFERER"]) ? (!empty($_SERVER["HTTP_REFERER"]) ? strtolower($_SERVER["HTTP_REFERER"]) : "") : '';
					if ( $browser['name'] == 'Twitterbot' ||
						(!empty($referer) &&
						  strpos($referer, "twitterbot") !== false) )
						die;

					if ($found && $out->type == 'pages') {
						if ($out->slug == 'portal-entry')
							$out->slug = 'home';

						if ($out->slug == 'products-table') {
							echo get_queried_object()->post_content;
							echo do_shortcode('[woocommerce_one_page_checkout template="product-list" product_ids="139,143,142,173,174, 175,176"]');
							return;
						}

						$this->log("get($what) - loading page:{$out->slug}");
						$slug = $out->slug;
						require_once( __DIR__.'/../'.$out->base_url.'-'.$out->slug.'.php' );
						$this->log("page - loaded $slug");
						if ( strpos($slug,'portal-landing') !== false ) // 'portal landing pages all use the same .js'
							$slug = 'portal-landing';

						if (file_exists(__DIR__.'/../_pages/_js/'.$slug.'.min.js')) $this->register('register-page-script', $slug.'.min');
						elseif (file_exists(__DIR__.'/../_pages/_js/'.$slug.'.js')) $this->register('register-page-script', $slug);
						else $this->log("Cannot find .js file: ".__DIR__.'/../_pages/_js/'.$slug.'.js');

						// if ( !is_user_logged_in() )
						// 	$this->register('allure-css-'.$out->slug);
					} elseif ($found && $out->type == 'sellers') {
						// if ($out->slug == 'shop')
						// 	$out->slug = 'sellers';
						// $this->register('allure-sellers-'.$out->slug);
						require_once( __DIR__.'/../'.$out->base_url.$out->slug.'.php' );
						$this->log("page - loaded $slug");
					} elseif (!$found) {
						echo '404: Page not found</br><pre>';
						print_r(get_post());
						echo '</pre>';
					}
					break;
				case 'seller':
					try {
						// require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new Sellers();
						$userId = wp_get_current_user()->ID;
						$user_info = get_userdata($userId);
						$Sellers = $this->getClass('Sellers', $this->logSellers);
						$this->log("get(seller) - extra:".($extra !== null ? (!$extra ? 'false' : 'true') : 'null'));
						// if ($extra !== null &&
						// 	$extra === false) {// just use author_id
							$this->log("get(seller) using author_id:$userId");
							$Sellers = $Sellers->get((object)[ 'what' => ['id','author_id','first_name','last_name','email','phone','mobile','state','service_areas','photo','flags','meta'], 
																'where' => ['author_id'=>$userId] ]);
							$this->log("get(seller) - ".(empty($Sellers) ? 'not found' : 'found id:'.$Sellers[0]->id));
						// }
						// else {// do a thorough search
						// 	$this->log("get(seller) using author_id or email, userId:$userId, email:".(!empty($user_info) ? $user_info->user_email : 'N/A'));
						// 	$Sellers = $Sellers->get((object)[ 'what' => ['id','author_id','first_name','last_name','email','phone','mobile','state','service_areas','photo','flags','meta'], 
						// 									   'or' => ['elements'=>[['author_id'=>$userId],
						// 									   						 ['email'=>$user_info->user_email] ]]]);
						// 	$this->log("get(seller) - ".(empty($Sellers) ? 'not find' : 'found id:'.$Sellers[0]->id));
						// }
						return $Sellers ? array_pop($Sellers) : null;
					} catch (\Exception $e){ parseException($e); die(); }
					break;
				case 'portal-agent':
					try {
						// require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new Sellers();
						// require_once(__DIR__.'/../_classes/SessionMgr.class.php'); $SessionMgr = new SessionMgr();
						$Sellers = $this->getClass('Sellers', $this->logSellers);
						$SessionMgr = $this->getClass('SessionMgr');
						$nickname = ''; $dummpy = 0;
						if ($this->logIt) // else Site24x7
							$this->log = $this->log == null ? new Log(__DIR__.'/_logs/Loader.class.log') : $this->log;
						$agent = $SessionMgr->getPortalAgent($nickname, $dummy);
						$this->log("get(portal-agent) - $agent");
						//$agent = isset($_COOKIE['PROFILE_AGENT_ID']) && intval($_COOKIE['PROFILE_AGENT_ID']) != 0 ? intval($_COOKIE['PROFILE_AGENT_ID']) : (isset($_SESSION['PROFILE_AGENT_ID']) && intval($_SESSION['PROFILE_AGENT_ID']) != 0 ? intval($_SESSION['PROFILE_AGENT_ID']) : 0);
						if ($agent == "0" ||
							$agent == 0)
							return "0";
						$agent = $Sellers->get((object)[ 'what' => ['id','author_id','first_name','last_name','photo','company','phone','mobile','email','city', 'state', 'service_areas', 'meta', 'flags'], 'where' => ['author_id'=>$agent] ]);
						$agent = $agent ? array_pop($agent) : "0";
						if ($agent != "0") {
							//$agent->nickname = $_COOKIE['PROFILE_AGENT_NAME'];
							$agent->phone = fixPhone($agent->phone);
							$agent->mobile = fixPhone($agent->mobile);
							$info = get_userdata($agent->author_id);
							$nickname = !empty($info->nickname) ? $info->nickname : $info->user_nicename;
							$agent->nickname = stripos($nickname, 'unknown') === false ? $nickname : '';
						}
						return $agent;
					} catch (\Exception $e){ parseException($e); die(); }
					break;
				case 'session':
					// add_action('init', function(){ 
					// 	if (!headers_sent()) 
					// 		setcookie('ProfileData', '', time() + (86400 * 1), "/"); 
					// 	$callGetDirective =  isset($_COOKIE['NEED_FORCED_SESSION_ID']) ? $_COOKIE['NEED_FORCED_SESSION_ID'] : 0;
					// 	$this->log("get(session) - callGetDirective:".($callGetDirective ? "yes" : "no").", directive:".(isset($_COOKIE['CassiopeiaDirective']) ? $_COOKIE['CassiopeiaDirective'] : "N/A"));
					// 	if ( $callGetDirective ) {
					// 		if (!headers_sent()) 
					// 			setcookie('NEED_FORCED_SESSION_ID', '', time() + (86400 * 1), "/"); // 86400 = 1 day
					// 		// require_once(__DIR__.'/SessionMgr.class.php'); 
					// 		// $reg = new SessionMgr();
					// 		$SessionMgr = $this->getClass('SessionMgr');
					// 		$SessionMgr->getDirective(isset($_COOKIE['CassiopeiaDirective']) ? $_COOKIE['CassiopeiaDirective'] : '', true);
					// 	}
					// 	else {
					// 		$directive = isset($_COOKIE['CassiopeiaDirective']) ? $_COOKIE['CassiopeiaDirective'] : '';
					// 		if (empty($directive)) {
					// 			// require_once(__DIR__.'/SessionMgr.class.php'); 
					// 			// $reg = new SessionMgr();
					// 			$SessionMgr = $this->getClass('SessionMgr');
					// 			$directive = $SessionMgr->getCassie();
					// 			if (!headers_sent()) 
					// 				setcookie('CassiopeiaDirective', $directive, time() + (86400 * 1), "/");
					// 			$this->log("action('init') - set direcive:$directive");
					// 		}
					// 		if(!headers_sent() &&
					// 			(!session_id() || session_status() != PHP_SESSION_ACTIVE)) session_start(); 
					// 	}
					// }, 1);
					add_action('wp_logout', function(){ 
						$agentID = $portalUserID = 0;
						$session_id = 0;
						$session = null;
						$this->getClass('SessionMgr')->getCurrentSession($session_id, $session);
						$this->getSessionData($session, $portalUserID, $agentID);
						$this->getClass('PortalUsersMgr')->updateSession(0, $agentID, $session_id);
						$this->log("logging out, set sessionID:$session_id to 0 from portalUser:$portalUserID, agentID:$agentID");
						// $this->getClass('SessionMgr')->releasePortalUser(); 
					});
					add_action('wp_login', function(){ if (!headers_sent()) setcookie('ProfileData', '', time() + (86400 * 1), "/"); if (session_status() == PHP_SESSION_ACTIVE) session_destroy();} );

					// require_once(__DIR__.'/Register.class.php'); 
					// add_action('init', function(){ 
					// 	$reg = new Register();
					// 	$reg->wp_init();
					// }, 1);
					// add_action('wp_logout', function(){
					// 	$reg = new Register();
					// 	$reg->wp_logout();
					// });
					// add_action('wp_login', function($user_login, $user){
					// 	$reg = new Register();			
					// 	$reg->wp_login($user_login, $user);
					// }, 10, 2);
					break;
				case 'title':
					if (empty($title)) wp_title( '|', true, 'right' ).' '.bloginfo('name');
					else $title.' '.bloginfo('name');
					break;
				default: return 'Unable to find: '.$what; break;
			}
		} catch(\Exception $e) { return $e->getMessage(); die(); }
	}
	public function load($what = null){
		global $browser;
		$referer = isset($_SERVER["HTTP_REFERER"]) ? (!empty($_SERVER["HTTP_REFERER"]) ? strtolower($_SERVER["HTTP_REFERER"]) : "") : '';
		if ( strpos($referer, 'drones.com') !== false ||
		     strpos(get_home_url(), 'drones.com') !== false ||
			 strpos(get_home_url(), '192.168.1.103') !== false ||
			 strpos($referer, 'colo-63') !== false  ) {
			$msg = "*** intruder - home:".get_home_url()." attempt in load($what) - referer:$referer, home:".get_home_url().", ip:".$this->userIP().", uri:{$_SERVER['REQUEST_URI']}";
			$this->log($msg);
			$this->getClass('Email')->textMessage('8312344865', "Unauthorized site access", $msg);
			echo "Screw YOU LOSER!";
			die;
		}

		try {
			switch ($what) {
				case 'init':
					//force_ssl_content(false);
					// register required scripts
					// $this->log("_SERVER data: ".print_r($_SERVER, true));
					global $thisPage;
					$queried_obj = get_queried_object();
					$thisPage = !empty($queried_obj) ? $queried_obj->post_name : '';
					$bufferJs = $this->bufferJs;
					$this->bufferJs = 0;

					foreach ($this->toRegister['noPack'] as &$req) {
						$this->register($req);
						unset($req);
					}

					$this->bufferJs = $bufferJs; // as before, one way or another
					foreach ($this->toRegister['always'] as &$req) {
						$this->register($req);
						unset($req);
					}
					
					$id = 0;
					// require_once(__DIR__.'/../_classes/SessionMgr.class.php'); $SessionMgr = new SessionMgr();
					$SessionMgr = $this->getClass('SessionMgr');
					$session = null;
					$session_id = $SessionMgr->getCurrentSession($id, $session);
					// search for the page
					$page = null;
					
					$portalLandingForAgent = false;
					if ($thisPage == 'portal-landing')
						$portalLandingForAgent = strpos($_SERVER['REQUEST_URI'], 'portal_entry') !== false;
					$input_var = get_query_var('id');
					$extra_var = get_query_var('extra');
					$isForImage = strpos($_SERVER['REQUEST_URI'], '.jpg') !== false || strpos($_SERVER['REQUEST_URI'], '.jpeg') !== false || strpos($_SERVER['REQUEST_URI'], '.png') !== false;
					$isForCss = strpos($_SERVER['REQUEST_URI'], '.css') !== false;
					if (!$isForImage &&
						!$isForCss) {
						$this->log("load($what) - queried_obj:".(empty($queried_obj) ? "N/A" : 'valid').', title: '.(empty($queried_obj) ? "N/A" : $queried_obj->post_title).', name: '.(empty($queried_obj) ? "N/A" : $queried_obj->post_name).", input_var:".(!empty($input_var) ? $input_var : "N/A").", extra_var:".(!empty($extra_var) ? $extra_var : "N/A").", uri:".$_SERVER['REQUEST_URI'].", siteUrl:".get_site_url().", ip:".$this->userIP().", sessionID:".(!empty($session) ? $session->id : 'N/A').", isForImage:".($isForImage ? 'yes' : 'no').", thisPage:$thisPage, portalLandingForAgent:".($portalLandingForAgent ? 'yes' : 'no').", bufferJs:".($this->bufferJs ? 'yes' : 'no'));
						$this->setupPortal($id, $session);


						foreach ($this->pages as $page_type => $page_type_info){
							if ($page !== null)
								break;
							foreach ($this->pages[$page_type]['children'] as &$page_info)
								if (!empty($page_info)) {
									if ($queried_obj && !is_admin() && $page_type != 'admin'){
										$title = $queried_obj->post_title;
										if ($page_info['title'] == $title){
											$page = (object) ['info'=>$page_info,'type'=>$page_type, 'title'=>$title];
											break;
										} elseif (!empty($page_info['children']))
											foreach($page_info['children'] as &$child_info){
												if ($child_info['title'] == $queried_obj->post_title){
													$page = (object) ['info'=>$child_info,'type'=>$page_type, 'title'=>$title];
													break;
												}
											}
									}
								}
						}

						unset($page_info, $page_type, $page_type_info);
						$cassie = isset($_COOKIE['CassiopeiaDirective']) ? $_COOKIE['CassiopeiaDirective'] : (isset($_SESSION['CassiopeiaDirective']) ? $_SESSION['CassiopeiaDirective'] : '');

						$agentCampaign = '';
						$portalLandingVersion = '';
						$extraInfo = '';
								
						// strip off any parameters
						$url = $_SERVER['REQUEST_URI'];
						if ( ($pos = strpos($url, "?")) !== false) {
							$args = substr($url, $pos);
							$url = substr($url, 0, $pos);
							$agentCampaign = isset($_GET['campaign']) ? $_GET['campaign'] : (isset($_POST['campaign']) ? $_POST['campaign'] : ''); 
							$portalLandingVersion = isset($_GET['plversion']) ? $_GET['plversion'] : (isset($_POST['plversion']) ? $_POST['plversion'] : ''); 
							if (!empty($agentCampaign) || !empty($portalLandingVersion))
								$this->log("got agentCampaign:$agentCampaign, portalLandingVersion:$portalLandingVersion");
							else
								$extraInfo = explode('=',$args)[1];
						}
					}
					elseif ($isForImage) {
							$url = explode('/', $_SERVER['REQUEST_URI']);
							$start = 0;
							foreach($url as $i=>$unit) 
								if ($unit == '_img')
									$start = $i;

							$trunc = array_slice($url, $start);
							$file = __DIR__."/../".trim(implode("/", $trunc),"/");
							$this->log("load('init') - isForImage:yes, ip:{$this->userIP()}, url:{$_SERVER['REQUEST_URI']}, file:$file, referer:$referer, home:".get_home_url());
							if (file_exists($file)) {
								header('Content-Type: image/jpeg');
								header('Cache-control: max-age='.(3600*24*7));
								header('ETag:"'.filemtime($file).'"');
								readfile($file);
							} 
							else {
								$this->log("load('init') - failed!! isForImage:yes, ip:{$this->userIP()}, url:{$_SERVER['REQUEST_URI']}, file:$file, referer:$referer");
								// $content = '<head></head><body><script>R=0; x1=.1; y1=.05; x2=.25; y2=.24; x3=1.6; y3=.24; x4=300; y4=200; x5=300; y5=200; DI=document.images; DIL=DI.length; function A(){for(i=0; i-DIL; i++){DIS=DI[ i ].style; DIS.position="absolute"; DIS.left=(Math.sin(R*x1+i*x2+x3)*x4+x5)+"px"; DIS.top=(Math.cos(R*y1+i*y2+y3)*y4+y5)+"px"}R++}setInterval("A()",5); void(0);</script>';
								$content = '<head></head><body>';
								$content.= 		'<div style="width:100%; height:100%; background: url('.get_home_url().'/_img/404.jpg) no-repeat; background-size: cover;">';
								$content.= 			'<img src="'.get_home_url().'/_img/lifestyled-listings-logo-main.png" style="width:80%; height: 120px; margin-left:10%" ></img>';
								$content.= 		'</div>';
								$content.= '</body>';

								header('Content-Type: text/html; charset=utf-8');
								echo $content;
							}
							exit;
					}
					else {
						$this->log("load($what) - queried_obj:".(empty($queried_obj) ? "N/A" : 'valid').', title: '.(empty($queried_obj) ? "N/A" : $queried_obj->post_title).', name: '.(empty($queried_obj) ? "N/A" : $queried_obj->post_name).", input_var:".(!empty($input_var) ? $input_var : "N/A").", extra_var:".(!empty($extra_var) ? $extra_var : "N/A").", uri:".$_SERVER['REQUEST_URI'].", siteUrl:".get_site_url().", sessionID:".(!empty($session) ? $session->id : 'N/A').", isForCss:".($isForCss ? 'yes' : 'no').", thisPage:$thisPage, portalLandingForAgent:".($portalLandingForAgent ? 'yes' : 'no').", bufferJs:".($this->bufferJs ? 'yes' : 'no'));
						die;
					}


					if ($page == null && !is_admin())  {
						// we are here because the url did not contain any known page identifications
						if ($this->logIt) // else Site24x7
							$this->log = $this->log == null ? new Log(__DIR__.'/_logs/Loader.class.log') : $this->log;
						if ($_SERVER['SERVER_NAME'] == 'localhost') $local = true; // So I can work locally without pulling remote scripts
						else $local = false;


						$url = explode('/', $url);
						foreach($url as $i=>$value)
							$url[$i] = strtolower($value);
						// $this->log("url:".$_SERVER['REQUEST_URI'].", exploded:".print_r($url, true));



						$nickname = '';
						$company_id = '';
						// while( count($url) )
						// 	if ( !empty($nickname = array_pop($url)) )
						// 		break;

						for($i = 0; $i < count($url); $i++) {
							if (empty($url[$i]))
								continue;

							if ($local &&
								$url[$i] == 'wordpress')
								continue;

							if ($url[$i] == 'home')
								continue;

							if (empty($nickname))
								$nickname = $url[$i];
							elseif (empty($company_id))
								$company_id = $url[$i];
						}

						// for($i = count($url) -1; $i >= 0; $i--) {
						// 	if (empty($nickname))
						// 		$nickname = $url[$i];
						// 	elseif (!empty($url[$i]) &&
						// 			empty($company_id) &&
						// 			$url[$i] != 'home' &&
						// 			(!$local ||
						// 			 ($local && $url[$i] != 'wordpress')))
						// 		$company_id = $url[$i];
						// }

						if (!$isForImage) $this->log("Loader - nickname: $nickname, company_id:$company_id, url:".$_SERVER['REQUEST_URI']);
						$id = get_query_var('id');
						$extra = get_query_var('extra');
						if (!$isForImage) $this->log("Loader id:".(!empty($id) ? $id : "N/A").", extra:".(!empty($extra) ? $extra : "N/A"));


						if ( $nickname == 'freebird')
						{
							$ip = $SessionMgr->userIP();
							$this->log("Loader - calling freebird - session:$session_id, ip:$ip");
							$SessionMgr->freebird();
							if (!headers_sent()) {
								setcookie('AGENCY', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
								setcookie('AGENCY_ID', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
								setcookie('AGENCY_LOGO', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
								setcookie('AGENCY_LOGO_X_SCALE', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
								setcookie('AGENCY_URL', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
								setcookie('AGENCY_SHORTNAME', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
								setcookie('ACTIVE_SESSION_ID', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
							}
							else {
								if (isset($_COOKIE['AGENCY'])) unset($_COOKIE['AGENCY']);
								if (isset($_COOKIE['AGENCY_ID'])) unset($_COOKIE['AGENCY_ID']);
								if (isset($_COOKIE['AGENCY_LOGO'])) unset($_COOKIE['AGENCY_LOGO']);
								if (isset($_COOKIE['AGENCY_LOGO_X_SCALE'])) unset($_COOKIE['AGENCY_LOGO_X_SCALE']);
								if (isset($_COOKIE['AGENCY_URL'])) unset($_COOKIE['AGENCY_URL']);
								if (isset($_COOKIE['AGENCY_SHORTNAME'])) unset($_COOKIE['AGENCY_SHORTNAME']);
								if (isset($_COOKIE['ACTIVE_SESSION_ID'])) unset($_COOKIE['ACTIVE_SESSION_ID']);
							}
							$_SESSION['AGENCY'] = '';
							$_SESSION['AGENCY_ID'] = 0;
							$_SESSION['AGENCY_LOGO'] = 'blank.jpg';
							$_SESSION['AGENCY_LOGO_X_SCALE'] = 1.00;
							$_SESSION['AGENCY_URL'] = '';
							$_SESSION['AGENCY_SHORTNAME'] = '';
							$_SESSION['ACTIVE_SESSION_ID'] = $id;
							// echo '<script type="text/javascript">window.location="'.get_home_url().'";</script>';
							header('Location: '.get_home_url());
							header("Connection: close");
							die;
						}

						// user a test string here, incase the 'nickname' is a campaign redirect
						$testNickname = preg_replace('/\W+/', '', $nickname);

						//$blogusers = get_users( 'blog_id='.$blog_id.'&orderby=login&role=contributor' );
						$blogusers = $this->checkPortalName($testNickname);
						if (!empty($blogusers)) {
							if (count($blogusers) == 1) {
								$roles = $blogusers[0]->roles;
								$agent = in_array('contributor', $roles) || in_array('administrator', $roles);
								if ($agent == false) { // double check if there's a seller attached to this id
									// require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new Sellers();
									$Sellers = $this->getClass('Sellers', $this->logSellers);
									$where = (object)['author_id'=>$blogusers[0]->ID];
									$agent = $Sellers->exists($where);
								}

								$nickname = preg_replace('/\W+/', '', $nickname);
								$this->log("load($what) - got agent is $agent, nickname: $nickname, directive:$cassie, agentCampaign:$agentCampaign");
								// require_once(__DIR__.'/../_classes/SessionMgr.class.php'); $SessionMgr = new SessionMgr();
								$SessionMgr = $this->getClass('SessionMgr');
								$agentID = ($agent ? $blogusers[0]->ID : '0');
								// $row = $SessionMgr->setPortalAgent($nickname, $agentID, true, true);
								// if ($row === true)
								// 	$row = '';
								// $this->log("load($what) - called setPortalAgent() - nickname:$nickname, agentID:$agentID, row:$row");

								// // setcookie('PHPSESSID', "0", time() + (86400 * 1), "/"); // 86400 = 1 day
								// setcookie('WAIT_PORTAL_ROW', $row, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
								// setcookie('PROFILE_AGENT_NAME', $nickname, time() + (86400 * 1), "/"); // 86400 = 1 day
								// setcookie('PROFILE_AGENT_ID', ($agent ? number_format($blogusers[0]->ID) : '0'), time() + (86400 * 1), "/"); // 86400 = 1 day
								// setcookie('TRACK_PREMIER_AGENT_USAGE', ($agent ? 'true' : 'false'), time() + (86400 * 1), "/"); // 86400 = 1 day
								// $_SESSION['PROFILE_AGENT_NAME'] = $nickname;
								// $_SESSION['PROFILE_AGENT_ID'] = $agent ? number_format($blogusers[0]->ID) : "0";
								// $_SESSION['TRACK_PREMIER_AGENT_USAGE'] = $agent ? 'true' : 'false';
								// $msg = "load($what) - PROFILE_AGENT_NAME:".$_COOKIE['PROFILE_AGENT_NAME'].", PROFILE_AGENT_ID:".$_COOKIE['PROFILE_AGENT_ID'].', TRACK_PREMIER_AGENT_USAGE:'.$_COOKIE['TRACK_PREMIER_AGENT_USAGE'];
								// $this->log($msg);
								// require_once(__DIR__.'/Register.class.php');
								// $r = new Register();
								// $r->updateSession($nickname, $blogusers->ID);
								// $h = '<!DOCTYPE html>';
								// $h.= '<html>';
								// $h.= 	'<head>';
								// $h.=		'<script type="text/javascript">';
								// $h.=				'function goHome() { setTimeout( window.location="'.get_home_url().'", 0 ); }';
								// $h.=		'</script>';
								// $h.=	'</head>';
								// $h.=	'<body>';
								// $h.=			'<script type="text/javascript">';
								// $h.=				'goHome();';
								// $h.=		'</script>';
								// $h.=	'</body>';
								// $h.= '</html>';

								global $doPortalLanding;
								if ($doPortalLanding) {
									$goToPage = 'portal-landing'.(!empty($portalLandingVersion) ? '-'.$portalLandingVersion : '');
									$redirect = get_site_url()."/$goToPage/?portal_entry=$agentID&nickname=$nickname".(!empty($company_id) ? "&agency=$company_id" : '')."&directive=$cassie".(!empty($agentCampaign) ? "&campaign=$agentCampaign" : '').(!empty($portalLandingVersion) ? "&plversion=$portalLandingVersion" : '');
								}
								else
									$redirect = get_site_url()."/?portal_entry=$agentID&nickname=$nickname".(!empty($company_id) ? "&agency=$company_id" : '')."&directive=$cassie".(!empty($agentCampaign) ? "&campaign=$agentCampaign" : '').(!empty($portalLandingVersion) ? "&plversion=$portalLandingVersion" : '');
								$this->log("load($what) - setting script to go to ".$redirect);

								// $browser = getBrowser();
								// if ($browser['shortname'] != 'iPad' &&
								// 	$browser['shortname'] != 'iPhone') {
								// 	if (headers_sent())
								// 		echo '<script type="text/javascript"> window.location = "'.get_site_url()."/portal-entry/$row".'; </script>';
								// 	else {
								// 		header('Location: '.get_site_url()."/portal-entry/$row");
								// 		header("Connection: close");
								// 	}
								// }
								// else {
									if (headers_sent())
										echo '<script type="text/javascript"> window.location = "'.$redirect.'; </script>';
									else {
										header('Location: '.$redirect);
										header("Connection: close");
									}
								// }
								die;

							}
							else {
								echo '<div><span>ERROR: more than one agent with nickname:'.$nickname.'</span></div>';
								die;
							}
						}
						else {
							$this->log("Loader - got nickname: $nickname, extraInfo: $extraInfo");
							$Options = $this->getClass('Options');
							$opt = $Options->get((object)['where'=>['opt'=>'CampaignRedirect']]);
							if (!empty($opt)) {
								$this->log("Loader - CampaignRedirect:".print_r($opt, true));
								$haveSeller = !empty($this->get('seller')) && wp_get_current_user()->ID != 8;
								$campaignRedirect = json_decode($opt[0]->value);
								$this->log("haveSeller:".($haveSeller ? 'yes' : 'no').", userId:".(wp_get_current_user()->ID != 8));
								foreach($campaignRedirect as $campaign=>$redirects)
									if ( strtolower($nickname) == strtolower($campaign) ) {
										$this->log("$campaign matched, incoming:$nickname");
										$url = get_site_url()."/".$redirects[0].'/'.$extraInfo;
										if ($haveSeller)
											$url = get_site_url()."/".$redirects[1];
										// require_once(__DIR__.'/../_classes/Analytics.class.php'); $Analytics = new Analytics(1);
										// require_once(__DIR__.'/../_classes/SessionMgr.class.php'); $SessionMgr = new SessionMgr();
										$Analytics = $this->getClass('Analytics', 1);
										$SessionMgr = $this->getClass('SessionMgr');
										// $id = 0;
										// $session_id = $SessionMgr->getCurrentSession($id);
										$ip = $SessionMgr->userIP();
										// $browser = getBrowser();
										$Analytics->add(['type'=>ANALYTICS_TYPE_PROMO,
															'origin'=>'external',
															'session_id'=>$id,
															'referer'=> !empty($referer) ? $referer : "not set",
															'what'=>'promoEntry, url:'.$url,
															'value_str'=>$campaign,
															'value_int'=>$haveSeller,
															'added'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)),
															'browser'=>$browser['shortname'],
															'connection'=>$browser,
															'ip'=>$ip]);
										header('Location: '.$url);
										header("Connection: close");
										die;
									}

								$this->log("did not match any campaign:$nickname");
							}

							// if it fell out to here, then it wasn't a portal, image file nor CampaignRedirect entry
							// so check to see if we can parse this as a legitimate sitemap entry
							// explode URI again anyway, should be the same as the original though...
							$url = explode('/', $_SERVER['REQUEST_URI']);
							global $siteMapKeys;
							$firstActivityMatch = -1;
							foreach($url as $i=>$value) {
								$url[$i] = rawurldecode( strtolower($value) );
								if ( $firstActivityMatch == -1 )
									foreach($siteMapKeys as $key) {
										if ( $key == $url[$i] ) {
											$firstActivityMatch = $i;
											break;
										}
									}
							}

							$siteMapKeys = array_merge(['listing'], $siteMapKeys);
							foreach($siteMapKeys as $key) {
								if ( in_array($key, $url) ) {
									foreach($url as $i=>$value) {
										if ( $value == $key ) {
											$this->log("siteMapKeys matched with $key");
											$redirect = '';
											$listing_id = 0;
											if ($key == 'listing') {
												if ( count($url) > ($i+1)) {
													$listing_id = $url[$i+1];
													$this->log("siteMapKeys next value is $listing_id");
													if ( is_numeric($listing_id)) {
														$redirect = get_home_url()."/listing/N-$listing_id/seo";
													}
												}
											}
											else {
												$reuse = false;
												$quiz_id = $this->getClass('DynamicQuiz', 1)->generateNewQuiz($url, $i, $Options, $reuse);
												if ($quiz_id) {
													$redirect = get_home_url()."/quiz-results/".($reuse ? 'L' : 'R')."-$quiz_id/seo";
													$this->log("DynamicQuiz generated $quiz_id redirect:$redirect, url:".print_r($url, true));
												}
												else
													$this->log('DynamicQuiz failed to generate quiz_id from '.print_r($url, true));
											}

											if (!empty($redirect)) {
												array_walk($url, function(&$ele, $i) { $ele = rawurlencode($ele); });
												$url = array_slice($url, $firstActivityMatch);
												$uri = '/'.implode('/', $url);
												setcookie('RedirectURL', $uri, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
												$_SESSION['RedirectURL'] = $uri;
												if (headers_sent())
													echo '<script type="text/javascript"> window.location = "'.$redirect.'; </script>';
												else {
													header('Location: '.$redirect);
													header("Connection: close");
												}
												die;
											}
										}
									}
								}
							}

							// fell out to here, so check if we nee to use mapPage
							$this->log("load -  uri:".$_SERVER['REQUEST_URI'].", mapPage:".print_r($this->mapPage, true));
							$url = explode('/', $_SERVER['REQUEST_URI']);
							$gotRemap = false;
							$redirect = get_site_url();
							$isLocal = strpos($redirect, 'localhost') !== false;
							foreach($url as $i=>$value) {
								if (empty($value))
									continue;
								$this->log("load - test remap: $value");
								if (array_key_exists($value, $this->mapPage)) {
									$redirect .= "/".$this->mapPage[$value];
									$this->log("found remap: {$this->mapPage[$value]} for $value, redirect:$redirect");
									$gotRemap = true;
								}
								elseif ($isLocal && $value == 'wordpress')
									continue;
								else
									$redirect .= "/$value";
							}

							if ($gotRemap) {
								$this->log("remap is redirecting to $redirect");
								if (headers_sent())
									echo '<script type="text/javascript"> window.location = "'.$redirect.'; </script>';
								else {
									header('Location: '.$redirect);
									header("Connection: close");
								}
								die;
							}
							
							$this->log("no remap found for ".$_SERVER['REQUEST_URI']);
						}

						if (strpos( $_SERVER['REQUEST_URI'], '/lookup/') !== 0)
							$this->getClass('Email')->sendMail( SUPPORT_EMAIL,
																"Suspicious loader activity",
																"siteUrl:".get_site_url().", uri:".$_SERVER['REQUEST_URI'].", from ip:".$this->userIP().", referer:$referer",
																"Intruder Alert",
																"Check logs and blacklist ip if necessary");
						$this->log("Suspicious activity - siteUrl:".get_site_url().", uri:".$_SERVER['REQUEST_URI'].", from ip:".$this->userIP().", referer:$referer");

						if (headers_sent())
							echo '<script type="text/javascript"> window.location = "'.get_site_url().'; </script>';
						else {
							header('Location: '.get_site_url());
							header("Connection: close");
						}
						die;
					
						// $h = '<div id="primary" class="content-area"> 
						// 		<main id="main" class="site-main" role="main">

						// 			<section class="error-404 not-found">
						// 				<header class="page-header">
						// 					<h1 class="page-title">Oops! That page can&rsquo;t be found.</h1>
						// 				</header><!-- .page-header -->

						// 				<div class="page-content">
						// 					<p>It looks like nothing was found at this location. Sorry from Lifestyled Listings.</p>

						// 				</div><!-- .page-content -->
						// 			</section><!-- .error-404 -->

						// 		</main><!-- .site-main -->
						// 	</div><!-- .content-area -->';
						// echo $h;
						// die;
						
					} // end if ($page == null && !is_admin())

					// setcookie('RedirectURL', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
					// $_SESSION['RedirectURL'] = '';

					if (!is_admin()){
						if ( ($page->info['title'] == 'Agent Reserve Portal' ||
							  $page->info['title'] == 'Agent Purchase Portal' ||
							  $page->info['title'] == 'Agent Purchase Lifestyle') &&
							wp_get_current_user()->ID != 8 &&
							!empty($this->get('seller')) ) {
							$url = get_site_url()."/sellers/#".($page->info['title'] == 'Agent Purchase Lifestyle' ? "agent_match_reserve" : "portal");
							header('Location: '.$url);
							header("Connection: close");
							die;
						}

						if ($page->info['title'] == 'New Listing' &&
							empty($this->get('seller'))) {
							$url = get_site_url();
							header('Location: '.$url);
							header("Connection: close");
							die;
						}

						if ($page->info['title'] == 'Agent Reservation') {
							$url = get_site_url()."/agent-purchase-lifestyle";
							header('Location: '.$url);
							header("Connection: close");
							die;
						}

						$portalUser = 0;
						$agentID = 0;
						$this->getSessionData($session, $portalUser, $agentID);
						$this->log("getSessionData returned portalUser:$portalUser, agentID:$agentID for page {$page->info['title']}");

						if ( strpos($page->info['title'],'Portal Landing') !== false ) {
							// if ( wp_get_current_user()->ID &&
							//  	 wp_get_current_user()->ID != 8 ) { // direct to home page
							// 	if ( empty($this->findPortalUser(wp_get_current_user()->ID)) )  {// not yet in system as PortalUser
							// 		$this->log("calling addPortalUserFromWP for WPUser:wp_get_current_user()->ID for session:$session->id, intended for portal-landing, cassie:$cassie");
							// 		$this->getClass('PortalUsersMgr')->addPortalUserFromWP($session->id, wp_get_current_user()->ID, 'portal-landing', $agentID, $agentCampaign);
							// 	}

							// 	$url = get_site_url();
							// 	header('Location: '.$url);
							// 	header("Connection: close");
							// 	die;
							// }
							// else {
								$this->log("PortalUser:$portalUser, agentID:$agentID, cassie:$cassie");
								if ($portalUser &&
									$agentID) {
									$url = get_site_url();
									header('Location: '.$url);
									header("Connection: close");
									die;
								}
								elseif (!empty($portalLandingVersion)) {
									$goToPage = 'portal-landing'.(!empty($portalLandingVersion) ? '-'.$portalLandingVersion : '');
									$redirect = get_site_url()."/$goToPage";
									$this->log("load($what) - setting script to go to since portalLandingVersion is set: ".$redirect);
									if (headers_sent())
										echo '<script type="text/javascript"> window.location = "'.$redirect.'; </script>';
									else {
										header('Location: '.$redirect);
										header("Connection: close");
									}
									die;
								}
							// }
						}

						$forcePage = false;
						$slug = $this->title2slug($page->info['title']);
						if ( $page->info['title'] == 'Quiz Results' ||
							 $page->info['title'] == 'Listing') {
						 	if ( !empty($extra_var) ) {
								$this->log("got $slug page with $extra_var");
							 	if ( strpos($extra_var, 'P-') === 0 ) {
									$portalAgentId = substr($extra_var, 2);
									$portalAgent = null;
									if (is_numeric($portalAgentId) )
										$portalAgent = get_userdata( intval($portalAgentId) );
									else {
										$blogusers = $this->checkPortalName($portalAgentId);
										if (!empty($blogusers)) {
											if (count($blogusers) == 1) {
												$portalAgent = $blogusers[0];
												$portalAgentId = $portalAgent->ID;
											}
										}
									}
									// $this->log("User data for $portalAgentId: ".print_r($portalAgent, true));
									if (!empty($portalAgent) &&
										!empty($portalAgent->nickname)) {
										$redirect = get_site_url()."/$slug/$input_var/F-$portalAgentId/?portal_entry=$portalAgentId&nickname=$portalAgent->nickname&directive=$cassie".(!empty($agentCampaign) ? "&campaign=$agentCampaign" : '').(!empty($portalLandingVersion) ? "&plversion=$portalLandingVersion" : '');
										$this->log("Got nickname:$portalAgent->nickname for $portalAgent->first_name $portalAgent->last_name, redirect:$redirect, page:$slug");

										if (headers_sent())
											echo '<script type="text/javascript"> window.location = "'.$redirect.'; </script>';
										else {
											header('Location: '.$redirect);
											header("Connection: close");
										}
										die;
									}
								}
								elseif ( strpos($extra_var, 'F-') === 0 ) {
									$forcePage = true;
									$this->log("$slug is forcing page");
								}
							}
							elseif ( strpos($input_var, 'L-') === 0 ||
									 strpos($input_var, 'R-') === 0) {
								$quizId = substr($input_var, 2);
								$quiz = $this->getClass('QuizActivity')->get((object)['where'=>['id'=>$quizId]]);
								$this->log("quiz results for quiz:$quizId, portalAgent:".(!empty($quiz) ? $quiz[0]->portal_agent_id : 'N/A'));
								if (!empty($quiz) &&
									isset($quiz[0]->portal_agent_id) &&
									!empty($quiz[0]->portal_agent_id)) {
									$portalAgent = get_userdata( intval($quiz[0]->portal_agent_id) );
									$curUser = wp_get_current_user();
									$userIsPortalAgent = false;
									if (!empty($curUser) &&
										$curUser->ID == $portalAgent->ID)
										$userIsPortalAgent = true;

									if (!$userIsPortalAgent &&
										!empty($portalAgent) &&
										!empty($portalAgent->nickname)) {
										$redirect = get_site_url()."/quiz-results/$input_var/F-$portalAgent->ID/?portal_entry=$portalAgent->ID&nickname=$portalAgent->nickname&directive=$cassie".(!empty($agentCampaign) ? "&campaign=$agentCampaign" : '').(!empty($portalLandingVersion) ? "&plversion=$portalLandingVersion" : '');
										$this->log("Got nickname:$portalAgent->nickname for $portalAgent->first_name $portalAgent->last_name, redirect:$redirect");

										if (headers_sent())
											echo '<script type="text/javascript"> window.location = "'.$redirect.'; </script>';
										else {
											header('Location: '.$redirect);
											header("Connection: close");
										}
										die;
									}
								}
							}
						}

						if ( $page->info['title'] == 'Quiz') {
						 	if ( !empty($input_var) ) {
								$this->log("got $slug page with $input_var");
							 	if ( strpos($input_var, 'P-') === 0 ) {
									$portalAgentId = substr($input_var, 2);
									$portalAgent = null;
									if (is_numeric($portalAgentId) )
										$portalAgent = get_userdata( intval($portalAgentId) );
									else {
										$blogusers = $this->checkPortalName($portalAgentId);
										if (!empty($blogusers)) {
											if (count($blogusers) == 1) {
												$portalAgent = $blogusers[0];
												$portalAgentId = $portalAgent->ID;
											}
										}
									}
									// $this->log("User data for $portalAgentId: ".print_r($portalAgent, true));
									if (!empty($portalAgent) &&
										!empty($portalAgent->nickname)) {
										$redirect = get_site_url()."/$slug/F-$portalAgentId/?portal_entry=$portalAgentId&nickname=$portalAgent->nickname&directive=$cassie".(!empty($agentCampaign) ? "&campaign=$agentCampaign" : '').(!empty($portalLandingVersion) ? "&plversion=$portalLandingVersion" : '');
										$this->log("Got nickname:$portalAgent->nickname for $portalAgent->first_name $portalAgent->last_name, redirect:$redirect, page:$slug");

										if (headers_sent())
											echo '<script type="text/javascript"> window.location = "'.$redirect.'; </script>';
										else {
											header('Location: '.$redirect);
											header("Connection: close");
										}
										die;
									}
								}
								elseif ( strpos($input_var, 'F-') === 0 ) {
									$forcePage = true;
									$this->log("$slug is forcing page");
								}
							}
						}

						$wpUserID = wp_get_current_user()->ID;
						$fromUs = strpos($referer, get_home_url()) !== false;
						$isInitialPage = empty($referer) || !$fromUs;
						$primary = !empty(get_query_var('id')) ? get_query_var('id') : null;
						$extra = !empty(get_query_var('extra')) ? get_query_var('extra') : null;
						$extra2 = !empty(get_query_var('extra2')) ? get_query_var('extra2') : null;

						$this->log("test condition - PortalUser:$portalUser, agentID:$agentID, wpUserID:$wpUserID, sessionID:".(!empty($session) ? $session->id : 'N/A').", cassie:$cassie, fromUs:$fromUs, isInitialPage:$isInitialPage");
							
						if ($agentID) {
							if ($portalUser) {
								if ($isInitialPage) {
									$this->log("calling recordPortalUserEntry - PortalUser:$portalUser, agentID:$agentID, wpUserID:$wpUserID, sessionID:".(!empty($session) ? $session->id : 'N/A').", cassie:$cassie, fromUs:$fromUs, isInitialPage:$isInitialPage");
									$this->getClass('PortalUsersMgr')->recordPortalUserEntry($portalUser, $agentID, $session->id, $thisPage, $agentCampaign);
								}
								else
									$this->getClass('PortalUsersMgr')->updateSession($portalUser, $agentID, $id);
							}
							elseif ($wpUserID) {
								if ($wpUserID != $agentID) {
									$this->log("calling addPortalUserFromWP with agentID:$agentID, wpUserID:$wpUserID, page:$thisPage, cassie:$cassie");
									$this->getClass('PortalUsersMgr')->addPortalUserFromWP($session->id, $wpUserID, $thisPage, $agentID, $agentCampaign);
								}
							}
							elseif (strpos($thisPage,'portal-landing') === false &&
									$isInitialPage &&
									!$forcePage) {
								// redirect to portal-page, with what page to go to when it's completed
								$this->log("Initial page entry - referer:$referer, homeUrl:".get_home_url().", PortalUser:$portalUser, agentID:$agentID, wpUserID:$wpUserID, sessionID:".(!empty($session) ? $session->id : 'N/A').", primary:".($primary ? $primary : "N/A").", extra:".($extra ? $extra : "N/A").", extra2:".($extra2 ? $extra2 : "N/A").", cassie:$cassie");
								$goToPage = 'portal-landing'.(!empty($portalLandingVersion) ? '-'.$portalLandingVersion : '');
								$url = get_site_url()."/$goToPage/$thisPage".($primary !== null ? "/$primary" : null).($extra !== null ? "/$extra" : null).($extra2 !== null ? "/$extra2" : null);
								header('Location: '.$url);
								header("Connection: close");
								die;
							}
						}
						
						$this->log("Loading page:$thisPage - referer:$referer, homeUrl:".get_home_url().", PortalUser:$portalUser, agentID:$agentID, wpUserID:$wpUserID".", primary:".($primary ? $primary : "N/A").", extra:".($extra ? $extra : "N/A").", extra2:".($extra2 ? $extra2 : "N/A"));

						add_action('wp_enqueue_scripts', array($this, 'load-page'));
						// add_action('wp_head', array($this, 'load-page'));
						// $this->load('page');
						add_action('wp_footer', array($this, 'load-footer'));	
						if ($page->type == 'sellers') {
							add_action('wp_enqueue_scripts', array($this, 'load-sellers'));
						}					
						// // register page required scripts
						// if ( !empty($page->info['req']) && (is_array($page->info['req']) || is_object($page->info['req'])) )
						// 	foreach($page->info['req'] as &$req) {
						// 		$this->register($req);
						// 	}
						if ( !empty($page->info['req']) && (is_array($page->info['req']) || is_object($page->info['req'])) )
							foreach($page->info['req'] as $req) {
								if ($req == 'bxslider')
									add_action('wp_footer', [ $this, 'get_bxslider' ]); // add this after the add_action('wp_footer','load-footer') so that it gets called
																						// after the bxslider.js is actually loaded first.
							}
					}

					break;
				case 'footer': 
					$this->log("load(footer) called");
					foreach ($this->toRegister['page-load-footer'] as $req) $this->register($req); 
					$this->callEnqueueData($what);
					break;
				case 'page':
					foreach ($this->toRegister['page-load-head'] as $req) $this->register($req);
					$this->callEnqueueData($what);
					break;
				case 'admin':
					foreach ($this->toRegister['admin'] as $req) $this->register($req);
					foreach ($this->pages['admin']['children'] as &$child) if (!empty($child['req'])) {
						if ($this->title2slug($child['title']) == $_GET['page']){
							foreach ($child['req'] as $req) $this->register($req);
							break;
						}
					}
					$this->callEnqueueData($what);
					break;
				default:
					foreach ($this->toRegister as $reg_type=>$scripts)
						if ($what == $reg_type) foreach($scripts as $req) $this->register($req);
					break;
			}
		} catch(\Exception $e) { parseException($e); die(); }
	}

	private function registerVariable($name, $varName, $var) {
		if ($this->bufferJs) {
			$newVar = (object)['nickname'=>$name,
							   'name'=>$varName,
							   'var'=>$var];
			global $queuedVariables;
			$queuedVariables[] = $newVar;
		}
		else
			wp_localize_script($name, $varName, $var); 
	}

	private function registerJs($name, $file = null, $depends = null, $version = '1.0', $dir = null, $file2 = null, $holdEnqueueScript = false) {
		$path = __DIR__."/../_jsPack";
		if ( !is_dir( $path ) && !mkdir( $path ) )
			$this->bufferJs = false;

		$localFile = $dir && $file2 ? __DIR__."/../$dir/$file2" : $file;
		$gotContents = false;

		if ($this->bufferJs) {
			$buffer = null;
			try {
				$buffer = $localFile ? @file_get_contents($localFile) : '';
			}
			catch( \Exception $e) {
				$this->log("registerJs caught exception for $localFile - ".$e->getMessage());
				$buffer = null;
			}
			$added = 0;
			$found = false;
			$index = 0;
			if (!empty($buffer) ||
				!$file) { // then just $name
				$gotContents = true;
				global $queuedScripts;
				$script = (object)['nickname'=>$name,
								   'fullPath'=>$file,
								   'name'=>$file2,
								   'dir'=>$dir,
								   'depends'=>$depends,
								   'buffer'=>$buffer];
				
				foreach($queuedScripts as $item) {
					if ($item->nickname == $name)
						$found = true;
					unset($item);
					if ($found) break;
				}
				if (!$found) {
					$haveDependsCount = 0;
					if (!empty($queuedScripts)) {
						foreach($queuedScripts as $item) {
							if (empty($script->depends)) {
								if (!empty($item->depends)) {
									if ($index == 0) {
										$queuedScripts = array_merge([$script], $queuedScripts);
										$added = true;
									}
									else {
										$left = array_slice($queuedScripts, 0, $index);
										$right = array_slice($queuedScripts, $index);
										$left[] = $script;
										$queuedScripts = array_merge($left, $right);
										$added = true;
									}
								}
								else if (($index+1) == count($queuedScripts)) { // end of the line
									$queuedScripts[] = $script;
									$added = true;
								}
							}
							else {
								if (in_array($item->nickname, $script->depends)) {
									$haveDependsCount++;
									if ($haveDependsCount == count($script->depends)) {
										$left = array_slice($queuedScripts, 0, $index+1);
										$right = array_slice($queuedScripts, $index+1);
										$left[] = $script;
										$queuedScripts = array_merge($left, $right);
										$added = true;
									}
								}
							}
							unset($item);
							if ($added) break;
							$index++;
						}
						if (!$added) {
							$queuedScripts[] = $script;
							$added = 1;
							$this->getClass('Logger', 1, 'enqueue')->log("registerJs - added to end, file:$file");
						}
					}
					else {
						$queuedScripts[] = $script;
						$added = 1;
					}
				}
			}
			$this->getClass('Logger', 1, 'enqueue')->log("registerJs - name:$name, file:$file, dir:".($dir ? $dir : 'N/A').", file2:".($file2 ? $file2 : 'N/A').", found:".($found ? 'yes' : 'no').", index:$index, added:$added, buffer:".(empty($buffer) ? "N/A" : strlen($buffer)));
			unset($buffer);
		}
		
		if (!$gotContents) {
			if ($file) {
				$this->getClass('Logger', 1, 'enqueue')->log("registerJs - wp_register_script for name:$name, file:".($file ? $file : "N/A"));
				wp_register_script($name, $file, $depends, $version);
			}
			if (!$holdEnqueueScript) {
				wp_enqueue_script($name);
				$this->getClass('Logger', 1, 'enqueue')->log("registerJs - wp_enqueue_script for name:$name, file:".($file ? $file : "N/A"));
			}
		}
	}

	private function callEnqueueData($from) {
		$this->getClass('Logger', 1, 'enqueue')->log("callEnqueueData - $from");
		enqueueData();
	}

	private function registerCss($name, $file = null, $dir = null, $file2 = null) {
		$path = __DIR__."/../_css/cssPack";
		if ( !is_dir( $path ) && !mkdir( $path ) )
			$this->bufferJs = false;

		$localFile = $dir && $file2 ? __DIR__."/../$dir/$file2" : $file;
		$gotContents = false;

		if ($this->bufferJs) {
			$buffer = null;
			try {
				$buffer = $localFile ? @file_get_contents($localFile) : '';
			}
			catch( \Exception $e) {
				$this->log("registerCss caught exception for $localFile - ".$e->getMessage());
				$buffer = null;
			}
			$found = false;
			if (!empty($buffer) ||
				!$file) {
				$gotContents = true;
				global $queuedCss;
				$css = (object)['nickname'=>$name,
								'fullPath'=>$file,
								'name'=>$file2,
								'dir'=>$dir,
								'buffer'=>$buffer];

				foreach($queuedCss as $item) {
					if ($item->nickname == $name)
						$found = true;
					unset($item);
					if ($found) break;
				}
				if (!$found) {
					$queuedCss[] = $css;
				}
			}
			$this->getClass('Logger', 1, 'enqueue')->log("registerCss - name:$name, file:$file, dir:".($dir ? $dir : 'N/A').", file2:".($file2 ? $file2 : 'N/A').", buffer:".(empty($buffer) ? "N/A" : strlen($buffer)));
			unset($buffer);
		}

		
		if (!$gotContents) {
			if ($file) {
				$this->getClass('Logger', 1, 'enqueue')->log("registerCss - wp_register_style for name:$name, file:".($file ? $file : "N/A"));
				wp_register_style($name, $file);
			}
			wp_enqueue_style($name);
		}
	}

	private function register($what = null, $script_slug = null){ // Registered everything required through Wordpress or PHP
		$out = new Out(0, 'nothing happened.');
		if ($_SERVER['SERVER_NAME'] == 'localhost') $local = true; // So I can work locally without pulling remote scripts
		else $local = false;
		global $browser;

		if (substr($what, 0, 13) == 'allure-admin-'){
			$session_id = null;
			$id = 0;
			$session = null;
			$SessionMgr = $this->getClass('SessionMgr');
			$session_id = $SessionMgr->getCurrentSession($id, $session);
			$ip = $SessionMgr->userIP();
			$this->log("register($what) - session:$session_id, ip:$ip");

			$prefix = 'allure-admin-';
			$name = substr($what, 13);
			$file = '/_admin/_js/'.$name.'.js';
			if (file_exists(__DIR__.'/..'.$file)){
				wp_register_script($what, get_template_directory_uri().$file, array('jquery'), null, true);
				wp_localize_script($what, 'ah_local', array('tp'=>get_template_directory_uri(),
																	'wp'=>get_home_url(),
																	'sessionID'=>$session_id,
																	'activeSessionID'=>$id,
																	'cities'=>$this->get('cities-and-counts'),
																	'candy'=>$SessionMgr->getCassie(),
																	'portal'=> isset($_GET['portal_entry']) ? $_GET['portal_entry'] : (isset($_POST['portal_entry']) ? $_POST['portal_entry'] : ''),
																	'version' => FE_VERSION
																	// 'agency'=>isset($_COOKIE['AGENCY']) && !empty($_COOKIE['AGENCY']) ? $_COOKIE['AGENCY'] :
																	// 		  isset($_SESSION) && isset($_SESSION['AGENCY']) && !empty($_SESSION['AGENCY']) ? $_SESSION['AGENCY'] : '',
																	// 'agency_id'=>isset($_COOKIE['AGENCY_ID']) && !empty($_COOKIE['AGENCY_ID']) ? $_COOKIE['AGENCY_ID'] :
																	// 		  isset($_SESSION) && isset($_SESSION['AGENCY_ID']) && !empty($_SESSION['AGENCY_ID']) ? $_SESSION['AGENCY_ID'] : 0,
																	// 'agency_logo'=>isset($_COOKIE['AGENCY_LOGO']) && !empty($_COOKIE['AGENCY_LOGO']) ? $_COOKIE['AGENCY_LOGO'] :
																	// 		  isset($_SESSION) && isset($_SESSION['AGENCY_LOGO']) && !empty($_SESSION['AGENCY_LOGO']) ? $_SESSION['AGENCY_LOGO'] : ''
																	// 'directive'=> isset($_GET['directive']) ? $_GET['directive'] : (isset($_POST['directive']) ? $_POST['directive'] : '')
																	)); // manufactured unique id for browser
																	
				wp_enqueue_script($what);
				// $file = '/_css/_compiled/admin-'.$name.'.css';
				$file = '/_css/_compiled/'.$name.'.css';
				if (file_exists( __DIR__.'/..'.$file )){
					wp_register_style($what, get_template_directory_uri().$file, null, '1.0');
					wp_enqueue_style($what);
				}
				$out = new Out('OK', 'Registered '.$what);
			} else { echo 'Unable to find file: '.$file.", for $what"; die(); }
		} elseif (substr($what, 0, 11) == 'allure-css-') {
			$prefix = 'allure-';
			$name = substr($what, 11);

			if ($name == 'products-table')
				return;

			$file = '/_css/_compiled/page-'.$name.'.css';
			if (file_exists( __DIR__.'/..'.$file )){
				$this->registerCss($prefix.$name, get_template_directory_uri().$file, '_css/_compiled', 'page-'.$name.'.css'); 
				$out = new Out('OK', 'Registered '.$what);
			} else { echo 'Unable to find file: '.__DIR__.'/..'.$file.", for $what"; die(); }
		} elseif (substr($what, 0, 19) == 'allure-sellers-css-') {
			$prefix = 'allure-';
			$name = substr($what, 19);
			$file = '/_css/_sellers/'.$name.'.css';
			if (file_exists(__DIR__.'/..'.$file)){
				$this->registerCss($prefix.$name, get_template_directory_uri().$file, '_css/_sellers', $name.'.css'); 
				$out = new Out('OK', 'Registered '.$what);
			} else { echo 'Unable to find file: '.$file.", for $what"; die(); }
		} elseif (substr($what, 0, 15) == 'allure-sellers-') {
			$prefix = 'allure-sellers-';
			$name = substr($what, 15);
			$file = '/_sellers/_js/'.$name.'.js';
			if (file_exists(__DIR__.'/..'.$file)){
				// $session_id = null;
				// $id = 0;
				// $SessionMgr = $this->getClass('SessionMgr');
				// $session = null;
				// $session_id = $SessionMgr->getCurrentSession($id, $session);

				// $portalUser = 0;
				// $agentID = 0;
				// $first = '';
				// $last = '';
				// $email = '';
				// $this->getSessionData($session, $portalUser, $agentID, $first, $last, $email);

				// $ip = $SessionMgr->userIP();
				// $this->log("register($what) - session:$session_id, ip:$ip, portalUser:$portalUser, agentID:$agentID");

				// $opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'MaxCities']]);
				// $maxCities = empty($opt) ? 50 : intval($opt[0]->value);
				// if (!empty($_COOKIE['PHPSESSID'])) $session_id = $_COOKIE['PHPSESSID'];
				// elseif (session_id()) { $session_id = session_id(); }
				$this->registerJs($prefix.$name, get_template_directory_uri().$file, array('jquery','jquery-ui','jquery-mask'), null, '_sellers/_js', $name.'.js');
				// wp_register_script($prefix.$name, get_template_directory_uri().$file, array('jquery'), null, true);
				$this->log("Loading: ".get_template_directory_uri().$file);
				$haveSession = isset($_SESSION) && !empty($_SESSION);
				// wp_localize_script($prefix.$name, 'ah_local', array('tp'=>get_template_directory_uri(),
				// $this->registerVariable($prefix.$name, 'ah_local', array('tp'=>get_template_directory_uri(),
				// 													'wp'=>get_home_url(),
				// 													'author_id'=>wp_get_current_user()->ID, 
				// 													'sessionID'=>$session_id, 
				// 													'activeSessionID'=>$id,
				// 													'cities'=>$this->get('cities-and-counts'),
				// 													'maxCities'=>$maxCities,
				// 													'portalUser'=>$portalUser,
				// 													'agentID'=>$agentID,
				// 													'portalUserFirstName' => $first,
				// 											  		'portalUserLastName' => $last,
				// 											  		'portalUserEmail' => $email,
				// 													'candy'=>$SessionMgr->getCassie(),
				// 													'portal'=> isset($_GET['portal_entry']) ? $_GET['portal_entry'] : (isset($_POST['portal_entry']) ? $_POST['portal_entry'] : ''),
				// 													 'agency'=>($haveSession && isset($_SESSION['AGENCY']) && !empty($_SESSION['AGENCY'])) ? $_SESSION['AGENCY'] : 
				// 													 			(isset($_COOKIE['AGENCY']) && !empty($_COOKIE['AGENCY']) ? $_COOKIE['AGENCY'] : ''),
																			  
				// 													 'agency_id'=>($haveSession && isset($_SESSION['AGENCY_ID']) && !empty($_SESSION['AGENCY_ID'])) ? $_SESSION['AGENCY_ID'] : 
				// 													 				(isset($_COOKIE['AGENCY_ID']) && !empty($_COOKIE['AGENCY_ID']) ? $_COOKIE['AGENCY_ID'] : 0),
																			  
				// 													 'agency_logo'=>($haveSession  && isset($_SESSION['AGENCY_LOGO']) && !empty($_SESSION['AGENCY_LOGO'])) ? $_SESSION['AGENCY_LOGO'] : 
				// 													 				(isset($_COOKIE['AGENCY_LOGO']) && !empty($_COOKIE['AGENCY_LOGO']) ? $_COOKIE['AGENCY_LOGO'] : ''),
																			  
				// 													 'agency_logo_x_scale'=>($haveSession && isset($_SESSION['AGENCY_LOGO_X_SCALE']) && !empty($_SESSION['AGENCY_LOGO_X_SCALE'])) ? $_SESSION['AGENCY_LOGO_X_SCALE'] : 
				// 													 				(isset($_COOKIE['AGENCY_LOGO_X_SCALE']) && !empty($_COOKIE['AGENCY_LOGO_X_SCALE']) ? $_COOKIE['AGENCY_LOGO_X_SCALE'] : 1.00),
																			  
				// 													 'agency_url'=>($haveSession && isset($_SESSION['AGENCY_URL']) && !empty($_SESSION['AGENCY_URL'])) ? $_SESSION['AGENCY_URL'] : 
				// 													 				(isset($_COOKIE['AGENCY_URL']) && !empty($_COOKIE['AGENCY_URL']) ? $_COOKIE['AGENCY_URL'] : ''),
																			  
				// 													 'agency_shortname'=>($haveSession && isset($_SESSION['AGENCY_SHORTNAME']) && !empty($_SESSION['AGENCY_SHORTNAME'])) ? $_SESSION['AGENCY_SHORTNAME'] : 
				// 													 				(isset($_COOKIE['AGENCY_SHORTNAME']) && !empty($_COOKIE['AGENCY_SHORTNAME']) ? $_COOKIE['AGENCY_SHORTNAME'] : ''),

				// 													 'profiles' =>$this->getProfiles($id)
																			  
				// 													// 'directive'=> isset($_GET['directive']) ? $_GET['directive'] : (isset($_POST['directive']) ? $_POST['directive'] : '')
				// 													));
				// if (!$this->bufferJs)
				// 	wp_enqueue_script($prefix.$name);
				// enqueueData();
				$out = new Out('OK', 'Registered '.$what);
			} else { echo 'Unable to find file: '.$file.", for $what"; die(); }
		} else switch($what){
			/*====== Scripts/Styles ======*/
				case 'allure-js': // mostly for the menu
					$this->log("allure-js start at ".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)) );
					$session_id = null;
					$id = 0;
					$SessionMgr = $this->getClass('SessionMgr');
					$session = null;
					$session_id = $SessionMgr->getCurrentSession($id, $session);
					$ip = $SessionMgr->userIP();
					$portalUser = 0;
					$agentID = 0;
					$seller = null;
					$first = '';
					$last = '';
					$email = '';
					$this->getSessionData($session, $portalUser, $agentID, $first, $last, $email);
					$this->log("register($what) - session:$session_id, ip:$ip, portalUser:$portalUser, agentID:$agentID");
					$ip = $SessionMgr->getUserIPDecimal();
					$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'MaxCities']]);
					$maxCities = empty($opt) ? 50 : intval($opt[0]->value);

					if (!empty($agentID)) {
						$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$agentID]]);
						if (!empty($seller))
							$seller = array_pop($seller);
					}

					$ahjs = $this->sendNotMinified ? 'ahjs.js' : 'ahjs.min.js';
					$this->registerJs('allure-js', get_stylesheet_directory_uri()."/_js/$ahjs", array('jquery','jquery-ui','jquery-mask','allure-thickbox'), null, '_js', $ahjs, $this->bufferJs);
					// wp_register_script('allure-js', get_stylesheet_directory_uri()."/_js/$ahjs", array('jquery'), null, true);
					$user_agent = isset($_SERVER['HTTP_USER_AGENT']) && !empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "";
					$haveSession = isset($_SESSION) && !empty($_SESSION);
					$redirect = isset($_COOKIE['RedirectURL']) && !empty($_COOKIE['RedirectURL']) ? $_COOKIE['RedirectURL'] : 
								(isset($_SESSION['RedirectURL']) && !empty($_SESSION['RedirectURL']) ? $_SESSION['RedirectURL'] : '');
					$agentCampaign = isset($_COOKIE['AGENT_CAMPAIGN']) && !empty($_COOKIE['AGENT_CAMPAIGN']) ? $_COOKIE['AGENT_CAMPAIGN'] : 
									 (isset($_SESSION['AGENT_CAMPAIGN']) && !empty($_SESSION['AGENT_CAMPAIGN']) ? $_SESSION['AGENT_CAMPAIGN'] : '');
					if (!headers_sent())	
						setcookie('RedirectURL', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
					$_SESSION['RedirectURL'] = '';
					global $thisPage;
					$ah_local = array('ip'=>$ip,
									  'sessionID'=>$session_id,
									  'activeSessionID'=>$id,
									  'user_agent'=>$user_agent,
									  'cities'=>$this->get('cities-and-counts'),
									  'maxCities'=>$maxCities,
									  'tp'=>get_template_directory_uri(),
									  'wp'=>get_home_url(),
									  'author_id'=>wp_get_current_user()->ID,
									  'portalUser'=>$portalUser,
									  'agentID'=>$agentID, // portal agent id
									  'agentCampaign'=>$agentCampaign,
									  'sellerID'=>!empty($seller) ? $seller->id : 0,
									  'portalUserFirstName' => $first,
									  'portalUserLastName' => $last,
									  'portalUserEmail' => $email,
									  'candy'=>$SessionMgr->getCassie(),
									  'portal'=> isset($_GET['portal_entry']) ? $_GET['portal_entry'] : (isset($_POST['portal_entry']) ? $_POST['portal_entry'] : ''),
									  'redirect' => $redirect,
									  'agency'=>($haveSession && isset($_SESSION['AGENCY']) && !empty($_SESSION['AGENCY'])) ? $_SESSION['AGENCY'] : 
																	 			(isset($_COOKIE['AGENCY']) && !empty($_COOKIE['AGENCY']) ? $_COOKIE['AGENCY'] : ''),
									  'agency_id'=>($haveSession && isset($_SESSION['AGENCY_ID']) && !empty($_SESSION['AGENCY_ID'])) ? $_SESSION['AGENCY_ID'] : 
									 				(isset($_COOKIE['AGENCY_ID']) && !empty($_COOKIE['AGENCY_ID']) ? $_COOKIE['AGENCY_ID'] : 0),
											  
									  'agency_logo'=>($haveSession  && isset($_SESSION['AGENCY_LOGO']) && !empty($_SESSION['AGENCY_LOGO'])) ? $_SESSION['AGENCY_LOGO'] : 
									 				(isset($_COOKIE['AGENCY_LOGO']) && !empty($_COOKIE['AGENCY_LOGO']) ? $_COOKIE['AGENCY_LOGO'] : ''),
											  
									  'agency_logo_x_scale'=>($haveSession && isset($_SESSION['AGENCY_LOGO_X_SCALE']) && !empty($_SESSION['AGENCY_LOGO_X_SCALE'])) ? $_SESSION['AGENCY_LOGO_X_SCALE'] : 
									 				(isset($_COOKIE['AGENCY_LOGO_X_SCALE']) && !empty($_COOKIE['AGENCY_LOGO_X_SCALE']) ? $_COOKIE['AGENCY_LOGO_X_SCALE'] : 1.00),
											  
									  'agency_url'=>($haveSession && isset($_SESSION['AGENCY_URL']) && !empty($_SESSION['AGENCY_URL'])) ? $_SESSION['AGENCY_URL'] : 
									 				(isset($_COOKIE['AGENCY_URL']) && !empty($_COOKIE['AGENCY_URL']) ? $_COOKIE['AGENCY_URL'] : ''),
											  
									  'agency_shortname'=>($haveSession && isset($_SESSION['AGENCY_SHORTNAME']) && !empty($_SESSION['AGENCY_SHORTNAME'])) ? $_SESSION['AGENCY_SHORTNAME'] : 
									 				(isset($_COOKIE['AGENCY_SHORTNAME']) && !empty($_COOKIE['AGENCY_SHORTNAME']) ? $_COOKIE['AGENCY_SHORTNAME'] : ''),

									  'profiles' =>$this->getProfiles($id)
									  // 'directive'=> isset($_GET['directive']) ? $_GET['directive'] : (isset($_POST['directive']) ? $_POST['directive'] : '')
									  );

					$this->registerVariable('allure-js', 'ah_local', $ah_local);  // manufactured unique id for browser

					if (!$this->bufferJs) 
						wp_enqueue_script('allure-js');

					$ahfeedback = $this->sendNotMinified ? 'ahfeedback.js' : 'ahfeedback.min.js';
					$this->registerJs('allure-feedback', get_stylesheet_directory_uri().'/_js/'.$ahfeedback, array('jquery','jquery-ui','allure-thickbox'), '1.1', '_js', $ahfeedback);
					$this->log("allure-js end at ".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)) );
					$out = new Out('OK', 'Registered allure-js and allure-feedback');
					break;
				case 'allure-thickbox': // for modal windows/popups
					$ahtb = $this->sendNotMinified ? 'ahtb.js' : 'ahtb.min.js';
					// add_thickbox();
					$this->registerJs('thickbox');
					$this->registerCss('thickbox');
					if ( is_network_admin() )
						add_action( 'admin_head', '_thickbox_path_admin_subfolder' );

					wp_deregister_script('allure-thickbox');
					$this->registerJs('allure-thickbox', get_stylesheet_directory_uri()."/_js/$ahtb", array('jquery','jquery-ui','thickbox'), '1.0', '_js', $ahtb);
				
					require_once(__DIR__.'/States.php');
					global $usStates;
					$i = 0; $states = '';
					foreach($usStates as $state) {
	  					$states .= '<option value="'.$state.'">'.$state.'</option>';
	  					$i++;
	  				}
	  				$form = wp_login_form( array( 	'echo' => false, 
 								'redirect' => site_url().(get_queried_object() ? '/'.get_queried_object()->post_name : ""),
 								'label_username' => __( 'Username:' ), 
 								'label_password' => __( 'Password:' ), 
 								'form_id' => 'login-form', 
 								'remember' => false) );
					$form = explode('</form>', $form);
					$form = str_replace("\n"," ", $form[0]);
					$form = str_replace("\t"," ", $form);
					// $lostPasswordUrl = wp_lostpassword_url();
					$lostPasswordUrl = get_home_url().'/wp-login.php?action=lostpassword';

					$ahreg = $this->sendNotMinified ? 'ahreg.js' : 'ahreg.min.js';
					wp_deregister_script('allure-registration-box');
					// wp_register_script('allure-registration-box', get_stylesheet_directory_uri()."/_js/$ahreg", array('allure-thickbox'), null, true);
					// wp_localize_script('allure-registration-box', 'ahreg_data', array('states'=>$states, 
					$this->registerJs('allure-registration-box', get_stylesheet_directory_uri()."/_js/$ahreg", array('allure-thickbox','jquery-mask'), null, '_js', $ahreg, $this->bufferJs);
					$this->registerVariable('allure-registration-box', 'ahreg_data', array('states'=>$states, 
																					  'login_form'=>$form, 
																					  'lost_password_url'=>$lostPasswordUrl,
																					  'redirect'=>site_url().(get_queried_object() ? '/'.get_queried_object()->post_name : "")) );
					if (!$this->bufferJs)
						wp_enqueue_script('allure-registration-box');
					$out = new Out('OK', 'Registered allure-thickbox and registration-box');
					break;
				case 'allure-stylesheet': // register this last to overwrite styles provided by other header scripts
					$this->registerCss('allure-stylesheet', get_template_directory_uri().'/_css/_compiled/style.css', '_css/_compiled', 'style.css');
					$out = new Out('OK', 'Registered allure-stylesheet');
					break;
				case 'checkout':
					$this->registerJs('checkout', get_stylesheet_directory_uri().'/_pages/_js/checkout.js', ['jquery'], '4.0.1', '_pages/_js', 'checkout.js');
					$out = new Out('OK', 'Registered checkout');
					break;
				case 'my_account':
					$this->registerJs('my_account', get_stylesheet_directory_uri().'/_pages/_js/my_account.js', ['jquery'], '4.0.1', '_pages/_js', 'my_account.js');
					$out = new Out('OK', 'Registered my_account');
					break;
				case 'bxslider':
					$this->registerJs('bxslider', get_stylesheet_directory_uri().'/_js/jquery.bxslider.min.js', [ 'jquery','jquery-ui','jquery-ui-touch-punch' ], '4.1.2', '_js', 'jquery.bxslider.min.js');
					$out = new Out('OK', 'Registered bxslider');
					break;
				case 'dropzone':
					$file = 'dropzone';
					if (!$this->sendNotMinified &&
						file_exists(__DIR__."/../_js/$file.min.js"))
						$file .= ".min.js";
					else
						$file .= ".js";
					$this->registerJs('dropzone', get_stylesheet_directory_uri().'/_js/'.$file, ['jquery'], '4.0.1', '_js', $file);
					$this->registerCss('dropzone', get_template_directory_uri().'/_css/_compiled/dropzone.css', '_css/_compiled', 'dropzone.css');
					$out = new Out('OK', 'Registered dropzone');
					break;
				case 'zeroClipboard':
					$this->registerJs('zeroClipboard', get_stylesheet_directory_uri().'/_js/ZeroClipboard.js', ['jquery','jquery-ui'], '1.0.1', '_js', 'ZeroClipboard.js');
					$out = new Out('OK', 'Registered zeroClipboard');
					break;
				case 'clipboardff':
					$this->registerJs('clipboardff', get_template_directory_uri().'/_js/clipboardff.min.js', ['jquery','jquery-ui'], '1.0.1', '_js', 'clipboardff.min.js');
					break;
				case 'clipboard':
					$this->registerJs('clipboard', get_stylesheet_directory_uri().'/_js/clipboard.min.js', ['jquery','jquery-ui'], '1.0.1', '_js', 'clipboard.min.js');

					$clipboard2 = $this->sendNotMinified ? 'clipboard2.js' : 'clipboard2.min.js';
					$this->registerCss('hljs', get_template_directory_uri().'/_css/_vendor/hljs.css', '_css/_vendor', 'hljs.css'); 
					$this->registerJs('hljs', get_stylesheet_directory_uri().'/_js/hljs.js', ['jquery','jquery-ui'], '1.0.1', '_js', 'hljs.js');
					$this->registerJs('clipboard2', get_stylesheet_directory_uri().'/_js/'.$clipboard2, ['jquery','jquery-ui'], '1.0.1', '_js', $clipboard2);
					$out = new Out('OK', 'Registered clipboard');
					break;
				case 'header_common':
					$headerJs = $this->sendNotMinified ? 'header_common.js' : 'header_common.min.js';
					$this->registerJs('header_common', get_stylesheet_directory_uri()."/_js/$headerJs", ['jquery','jquery-ui'], '1.0', '_js', $headerJs);
					// $this->registerCss('header_admin', get_template_directory_uri().'/_css/_compiled/nav-header.css', '_css/_compiled', 'nav-header.css');
					$out = new Out('OK', 'Registered header_common');
					break;
				case 'header':
					$headerJs = $this->sendNotMinified ? 'header.js' : 'header.min.js';
					$this->registerJs('header', get_stylesheet_directory_uri()."/_js/$headerJs", ['jquery','jquery-ui','header_common'], '3.1', '_js', $headerJs);
					$this->registerCss('header', get_template_directory_uri().'/_css/_compiled/nav-header.css', '_css/_compiled', 'nav-header.css');
					$out = new Out('OK', 'Registered header');
					break;
				case 'header_admin':
					$headerJs = $this->sendNotMinified ? 'header_admin.js' : 'header_admin.min.js';
					$this->registerJs('header_admin', get_stylesheet_directory_uri()."/_js/$headerJs", ['jquery','jquery-ui','header_common'], '3.1', '_js', $headerJs);
					// $this->registerCss('header_admin', get_template_directory_uri().'/_css/_compiled/nav-header.css', '_css/_compiled', 'nav-header.css');
					$out = new Out('OK', 'Registered header');
					break;
				case 'locationSelector':
					$this->registerJs('locationSelector', get_stylesheet_directory_uri().'/_js/locationSelector.js', ['header'], '1.0.1', '_js', 'locationSelector.js');
					$out = new Out('OK', 'Registered locationSelector');
					break;
				case 'addressSelector':
					$this->registerJs('addressSelector', get_stylesheet_directory_uri().'/_js/addressSelector.js', ['header'], '1.0.1', '_js', 'addressSelector.js');
					$out = new Out('OK', 'Registered addressSelector');
					break;
				case 'checkSellerInDB':
					wp_deregister_script('checkSellerInDB');
					$file = 'checkSellerInDB';
					if (!$this->sendNotMinified &&
						file_exists(__DIR__."/../_js/$file.min.js"))
						$file .= ".min.js";
					else
						$file .= ".js";
					$this->registerJs('checkSellerInDB', get_stylesheet_directory_uri().'/_js/'.$file, ['jquery','jquery-ui'], '1.0.1', '_js', $file);
					$out = new Out('OK', 'Registered checkSellerInDB');
					break;
				case 'portalUserRegistration':
					wp_deregister_script('portalUserRegistration');
					$file = 'portalUserRegistration';
					if (!$this->sendNotMinified &&
						file_exists(__DIR__."/../_js/$file.min.js"))
						$file .= ".min.js";
					else
						$file .= ".js";
					$this->registerJs('portalUserRegistration', get_stylesheet_directory_uri().'/_js/'.$file, ['jquery','jquery-ui'], '1.0.1', '_js', $file);
					$out = new Out('OK', 'Registered portalUserRegistration');
					break;
				case 'controllerQuizResults':
					$file = 'controllerQuizResults';
					if (!$this->sendNotMinified &&
						file_exists(__DIR__."/../_pages/_js/$file.min.js"))
						$file .= ".min.js";
					else
						$file .= ".js";
					$this->registerJs('controllerQuizResults', get_stylesheet_directory_uri().'/_pages/_js/'.$file, ['header'], '1.0.1', '_pages/_js', $file);
					$out = new Out('OK', 'Registered controllerQuizResults');
					break;
				case 'controllerPortalCaptureSetup':
					$file = 'controllerPortalCaptureSetup';
					if (!$this->sendNotMinified &&
						file_exists(__DIR__."/../_sellers/_js/$file.min.js"))
						$file .= ".min.js";
					else
						$file .= ".js";
					$this->registerJs('controllerPortalCaptureSetup', get_stylesheet_directory_uri().'/_sellers/_js/'.$file, ['header'], '1.0.1', '_sellers/_js', $file);
					$out = new Out('OK', 'Registered controllerPortalCaptureSetup');
					break;
				case 'controllerManageCompanies':
					$file = 'controllerManageCompanies';
					if (!$this->sendNotMinified &&
						file_exists(__DIR__."/../_sellers/_js/$file.min.js"))
						$file .= ".min.js";
					else
						$file .= ".js";
					$this->registerJs('controllerManageCompanies', get_stylesheet_directory_uri().'/_sellers/_js/'.$file, ['header'], '1.0.1', '_sellers/_js', $file);
					$out = new Out('OK', 'Registered controllerManageCompanies');
					break;
				case 'listhub':
					$this->registerJs('listhub', get_stylesheet_directory_uri().'/_js/listhub.js', null, '1.0', '_js', 'listhub.js');
					$out = new Out('OK', 'Registered listhub');
					break;
				case 'filesaver':
					// $browser = getBrowser();
					if ($browser['shortname'] == 'Safari' ||
						$browser['shortname'] == 'Firefox' ||
						$browser['shortname'] == 'Opera') {
						$version = explode('.', $browser['version']);
						$loadBlobJs = false;
						switch($browser['shortname']) {
							case 'Safari': $loadBlobJs = intval($version[0]) < 6 ? true : false; break;
							case 'Firefox': $loadBlobJs = intval($version[0]) < 20 ? true : false; break;
							case 'Opera': $loadBlobJs = intval($version[0]) < 15 ? true : false; break;
						}
						if ($loadBlobJs)
							wp_enqueue_script('blob', get_stylesheet_directory_uri().'/_js/Blob.js');
					}
					$this->registerJs('filesaver', get_stylesheet_directory_uri().'/_js/FileSaver.min.js', null, '1.0', '_js', 'FileSaver.min.js');
					$out = new Out('OK', 'Registered filesaver');
					break;
				case 'dragselect':
					$this->registerJs('dragselect', get_stylesheet_directory_uri().'/_js/keydragselect.js', ['jquery','jquery-ui'], '1.0.0', '_js', 'keydragselect.js');
					$out = new Out('OK', 'Registered dragselect');
					break;
				case 'sales':
					$this->registerCss('sales', get_template_directory_uri().'/_css/_compiled/sales.css', '_css/_compiled', 'sales.css');
					$this->registerJs('sales', get_stylesheet_directory_uri().'/_admin/_js/sales.js', [ 'jquery','jquery-ui','jquery-ui-touch-punch' ], '5.2.0', '_admin/_js', 'sales.js');
					$out = new Out('OK', 'Registered sales');
					break;

				case 'dashboard':
					$this->registerJs('dashboard', get_stylesheet_directory_uri().'/_sellers/_js/dashboard.js', [ 'jquery'], '1.0', '_sellers/_js', 'dashboard.js');
					$out = new Out('OK', 'Registered dashboard');
					break;

				case 'Chart':
					$this->registerJs('Chart', get_stylesheet_directory_uri().'/_js/Chart.min.js', ['jquery-ui'], '1.0', '_js', 'Chart.min.js');
					$out = new Out('OK', 'Registered Chart');
					break;
				case 'date-picker':
					$this->registerCss('date-picker', get_template_directory_uri().'/_css/_vendor/jquery.simple-dtpicker.css', '_css/_vendor', 'jquery.simple-dtpicker.css');
					$this->registerJs('date-picker', get_stylesheet_directory_uri().'/_js/jquery.simple-dtpicker.js', [ 'jquery','jquery-ui' ], '1.12.0', '_js', 'jquery.simple-dtpicker.js');
					$out = new Out('OK', 'Registered date-picker');
					break;
				case 'video':
					$this->registerCss('video', get_template_directory_uri().'/node_modules/video.js/dist/video-js/video-js.css', 'node_modules/video.js/dist/video-js', 'video-js.css');
					$this->registerJs('video', get_stylesheet_directory_uri().'/node_modules/video.js/dist/video-js/video.js', null, '1.0', 'node_modules/video.js/dist/video-js', 'video.js'); 
					$this->registerJs('video-youtube', get_stylesheet_directory_uri().'/node_modules/videojs-youtube/dist/vjs.youtube.js', null, '1.0', 'node_modules/videojs-youtube/dist', 'vjs.youtube.js'); 
					$out = new Out('OK', 'Registered video');
					break;
				case 'rating':
					$this->registerCss('rating', get_template_directory_uri().'/_css/_vendor/rateit.css', '_css/_vendor', 'rateit.css');
					$this->registerJs('rating', get_stylesheet_directory_uri().'/_js/jquery.rateit.min.js', [ 'jquery','jquery-ui' ], '1.0.22', '_js', 'jquery.rateit.min.js');
					break;
				case 'google-analytics':
					/*
					if (!$local) : ?>
						<script type="text/javascript">
						(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
						(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
						m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
						})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
						var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
						 var _gaq = _gaq || [];
						_gaq.push(['_require', 'inpage_linkid', pluginUrl]);
					  ga('create', 'UA-51003208-1', 'allure-homes.com');
					  ga('require', 'displayfeatures');
					  ga('send', 'pageview');
					  </script>
					<?php endif;
					*/
					$out = new Out('OK', 'Registered google-analytics');
					break;
				case 'google-maps-api-js':
					$oldBufferJs = $this->bufferJs;
					$this->bufferJs = false;
					$this->registerJs('google-maps-js-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA-X7VaeHvCIgXkiIHLao8_Yb1D5qmYtd8', ['jquery-ui']);
					$this->bufferJs = $oldBufferJs;
					$out = new Out('OK', 'Registered google-maps-js-api');
					break;
				case 'fbInit':
					// $file = strpos(get_home_url(), 'alpha') !== false ? 'fbInitAlpha' : 
					// 		(strpos(get_home_url(), 'local') !== false ? 'fbInitLocal' :
					// 		(strpos(get_home_url(), 'mobile') !== false ? 'fbInitMobile' : 'fbInit'));
					$file = 'fbInit';
					$testFile = __DIR__."/../_js/$file.min.js";
					if (!$this->sendNotMinified &&
						 file_exists($testFile) )
						$file .= ".min.js";
					else
						$file .= ".js";
					$this->registerJs('fbInit', get_stylesheet_directory_uri().'/_js/'.$file, ['header'], '2.7', '_js', $file);
					break;
				case 'jcrop':
					wp_deregister_script('jcrop');
					$this->registerJs('jcrop', get_template_directory_uri().'/_js/jquery.jcrop.min.js', array('jquery'), '1.0', '_js', 'jquery.jcrop.min.js' );
					$this->registerCss('ah-jcrop', get_template_directory_uri().'/_css/_req/jquery.Jcrop.min.css', '_css/_req', 'jquery.Jcrop.min.css');
					$out = new Out('OK', 'Registered dropzone');
					break;
				case 'jquery':
					if (!is_admin()){
						wp_deregister_script('jquery');
						if ($local) 
							$this->registerJs('jquery',  get_stylesheet_directory_uri().'/_js/jquery-1.11.0.min.js', null, null, '_js', 'jquery-1.11.0.min.js');
							// wp_register_script('jquery',  get_stylesheet_directory_uri().'/_js/jquery-1.11.0.min.js', null, null, true);
						else 
							$this->registerJs('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js", null, null);
							// wp_register_script('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js", null, null, true);
					}
					else
						wp_enqueue_script('jquery');
					$out = new Out('OK', 'Registered jquery');
					break;
				case 'jquery-ui':
					wp_deregister_script('jquery-ui');
					if ($local) {
						$this->registerJs('jquery-ui', get_stylesheet_directory_uri().'/_js/jquery-ui-1.10.4.min.js', array('jquery'), null, '_js', 'jquery-ui-1.10.4.min.js');
						$this->registerCss('jquery-mobile', get_template_directory_uri().'/_css/_vendor/jquery-ui.1.10.4.min.css', '_css/_vendor', 'jquery-ui.1.10.4.min.css');
						// wp_register_script('jquery-ui', get_stylesheet_directory_uri().'/_js/jquery-ui-1.10.4.min.js', array('jquery'), null, true);
						// wp_enqueue_style('jquery-mobile', get_template_directory_uri().'/_css/_vendor/jquery-ui.1.10.4.min.css', '', '1.10.4');
					}
					else 
						$this->registerJs('jquery-ui', "https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js", array('jquery'), null);
					$out = new Out('OK', 'Registered jquery-ui');
					break;
				case 'jquery-ui-vader':
					if ($local)
						break;
					$this->registerCss('jquery-ui-vader', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/vader/jquery-ui.min.css');
					$out = new Out('OK', 'Registered jquery-ui-vader');
					break;
				case 'jquery-ui-touch-punch':
					// if ($local)
					// 	break;
					$this->registerJs('jquery-ui-touch-punch', get_stylesheet_directory_uri().'/_js/jquery.ui.touch-punch.min.js', array('jquery','jquery-ui'), null, '_js', 'jquery.ui.touch-punch.min.js');
					
						// wp_register_script('jquery-ui', "https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js", array('jquery'), null, true);
						// wp_enqueue_style('jquery-ui-vader', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/vader/jquery-ui.min.css', '', null, true);
					// endif;
					// wp_enqueue_script('jquery-ui');
					$out = new Out('OK', 'Registered jquery-uitouch-punch');
					break;
				case 'jquery-ui-sortable':
					wp_enqueue_script('jquery-ui-sortable');
					$out = new Out('OK', 'Registered jquery-ui-sortable');
					break;
				case 'jquery-ui-resizable':
					wp_enqueue_script('jquery-ui-resizable');
					$out = new Out('OK', 'Registered jquery-ui-resizable');
					break;
				case 'jquery-mobile':
					$this->registerJs('jquery-mobile', get_template_directory_uri().'/_js/jquery.mobile.custom.min.js', array('jquery'), '1.4.2', '_js', 'jquery.mobile.custom.min.js');
					$this->registerCss('jquery-mobile', get_template_directory_uri().'/_css/_vendor/jquery.mobile.custom.theme.css', '_css/_vendor', 'jquery.mobile.custom.theme.css');
					$this->registerCss('jquery-mobile2', get_template_directory_uri().'/_css/_vendor/jquery.mobile-1.4.2.min.css', '_css/_vendor', 'jquery.mobile-1.4.2.min.css');
					add_action('wp_footer', array($this, 'jqueryMobileFooter'));
					break;
				case 'jquery-mobile-slider':
					$this->registerJs('jquery-mobile-slider', get_template_directory_uri().'/_js/mobile-slider.min.js', array('jquery'), null, '_js', 'mobile-slider.min.js');
					break;
				case 'mask':
					$this->registerJs('jquery-mask', get_template_directory_uri().'/_js/jquery.mask.min.js', array('jquery','jquery-ui'), '1.7.4', '_js', 'jquery.mask.min.js');
					$out = new Out('OK', 'Registered mask');
					break;
				case 'modernizr':
					$this->registerJs('modernizr', get_stylesheet_directory_uri().'/_js/modernizr-2.7.2.min.js', '', '2.7.2', '_js', 'modernizr-2.7.2.min.js');
					$out = new Out('OK', 'Registered modernizr');
					break;
				case 'perfect-scrollbar': // used on the site to make scrollbars pretty
					$this->registerJs('perfect-scrollbar', get_stylesheet_directory_uri().'/_js/perfect-scrollbar-0.4.10.with-mousewheel.min.js', array('jquery'), '0.4.10', '_js', 'perfect-scrollbar-0.4.10.with-mousewheel.min.js');
					$this->registerCss('perfect-scrollbar', get_stylesheet_directory_uri().'/_css/_vendor/perfect-scrollbar-0.4.10.min.css', '_css/_vendor', 'perfect-scrollbar-0.4.10.min.css');
					$out = new Out('OK', 'Registered perfect-scrollbar');
					break;
				case 'perfect-scrollbar-update': // updates scrollbar on resize
					?>
						<script type="text/javascript">
						$(document).ready(function(){
							if ($(document).height() > $(window).height()) $(".scrollbar").perfectScrollbar({suppressScrollX: true});
							$(window).resize(function(){ alr_resizeScrollbarUpdate() });

							function alr_resizeScrollbarUpdate(){
								if ($(document).height() > $(window).height()){
									if ( $('.scrollbar').hasClass('.ps-container') ) $(".scrollbar").perfectScrollbar('update');
									else $(".scrollbar").perfectScrollbar({suppressScrollX: true});
								}
							}
						});
						</script>
					<?php
					$out = new Out('OK', 'Registered perfect-scrollbar-update');
					break;
				case 'register-page-script':
					$this->registerJs($script_slug.'-js', get_stylesheet_directory_uri().'/_pages/_js/'.$script_slug.'.js', array('jquery','jquery-ui'), null, '_pages/_js', $script_slug.'.js');
					$out = new Out('OK', 'Registered '.$script_slug.'-js');
					break;
				case 'sly':
					$this->registerJs('sly', get_stylesheet_directory_uri().'/_js/sly.min.js', array('jquery'), '1.2.7', '_js', 'sly.min.js');
					$out = new Out('OK', 'Registered sly');
					break;
			/*====== Fonts ======*/
				case 'entypo': // icon font
					$this->registerCss('entypo', get_template_directory_uri().'/_css/_compiled/entypo.css', '_css/_compiled', 'entypo.css');
					$out = new Out('OK', 'Registered entypo');
					break;
				case 'google-fonts':
					if ($local) $out = new Out('OK','Skipped google-fonts, local mode.'); else {
						$this->registerCss('google-fonts', 'https://fonts.googleapis.com/css?family=Tinos:400,700|Open+Sans:300,400,600,700|Roboto:100,500');
						$out = new Out('OK', 'Registered google-fonts');
					}
					break;
			/*====== Pages/WP Settings ======*/
				case 'permalink-structure':
					update_option('permalink_structure', '/%postname%/');
					break;
				case 'pages': // creates the Wordpress posts (pages) to hold our structure
					foreach ($this->pages as $page_type=>&$pages){
						if ($page_type == 'admin' && is_admin()){
							require_once(__DIR__.'/../_classes/Admin.class.php');
							foreach($pages['children'] as &$child) $child['slug'] = $this->title2slug($child['title']);
							$admin = new Admin($pages['children']);
							add_menu_page('Allure Homes Admin', 'Allure', 'manage_options', 'allure-admin', array(&$admin, 'index'));
							foreach ($pages['children'] as &$page)
								add_submenu_page('allure-admin', $page['title'], $page['title'], 'manage_options', $page['slug'], array(&$admin, $page['slug']));
						} elseif (!is_admin() && $page_type != 'admin') foreach ($this->pages[$page_type]['children'] as $page){
							$found = false;
							$slug = empty($page['slug']) ? $this->title2slug($page['title']) : $page['slug'];
							if (!get_page_by_title($page['title'])) {// Page does not exist
								$post_id = wp_insert_post(array( // Create it
									'post_title'=>$page['title'],
									'post_name'=> $slug,
									'post_content'=>$page['title'].' page.',
									'post_status'=>'publish',
									'post_type'=>'page',
								), true);
								$this->log("register(pages) is creating new page:{$page['title']} with postId:$post_id");
							}
							if (is_page($slug)) {
								$found = true;
								if (!empty($page['req'])) foreach ($page['req'] as $req) {
									$this->register($req);
								}
								if ($slug == 'portal-entry')
									$slug = 'home';
								if ($page_type == 'pages') $this->register('allure-css-'.$slug);
								elseif ($page_type == 'sellers') {
									// $this->toRegister['page-load-head'][] = 'allure-sellers-'.$slug;
									// $this->toRegister['page-load-head'][] = 'allure-sellers-css-'.$slug;
									$this->register('allure-sellers-'.$slug);
									$this->register('allure-sellers-css-'.$slug);
								}
							}
							if (!empty($page['children'])) foreach ($page['children'] as $child) {// check for children of the page
								if (!get_page_by_title($child['title'])) // Child page does not exist
									wp_insert_post(array( // Create it
										'post_title'=>$child['title'],
										'post_name'=> empty($child['slug']) ? $this->title2slug($child['title']) : $child['slug'],
										'post_content'=>$child['title'].' page.',
										'post_status'=>'publish',
										'post_type'=>'page',
										'post_parent'=>$post_id
									));
								if (!$found && is_page( $slug = empty($child['slug'])?$this->title2slug($child['title']):$child['slug'] )){
									if ($page_type == 'pages') $this->register('allure-css-'.$slug);
									elseif ($page_type == 'sellers') {
										$this->register('allure-sellers-'.$slug);
										$this->register('allure-sellers-css-'.$slug);
									}
								}
							}
							if ($found)
								break;
						}
					}
					$out = new Out('OK', 'Registered pages');
					break;
			/*====== Sellers ======*/
				case 'seller-role':
					if (get_role('seller') == null) {
						$result = add_role('seller', __( 'Seller' ));
						$out = $result === null ? new Out(0, __LINE__.' Unable to create seller role.') : new Out('OK');
					}
					break;
			/*====== Default ======*/
				default: $out = new Out(0, __LINE__.' Unable to locate "'.$what.'"'); break;
		}

		if ($out->status !== 'OK') $out->data = 'Loader::register: '.$out->data;
		return $out;
	}
	public function get_bxslider(){
		global $thisPage;
		$slug = $this->bufferJs ? ('master1'.(isset($thisPage) && $thisPage ? "-$thisPage" : "-general")) : 'bxslider';
		$goodToGo = wp_script_is( $slug, 'done' );
		$this->getClass('Logger', 1, 'enqueue')->log("entered get_bxslider for $slug, goodToGo:".($goodToGo ? 'yes' : 'no'));
		// if ( wp_script_is( 'bxslider', 'done' ) ) {
		if ( $goodToGo ) {
		?>
			<script type="text/javascript">
				var slider;
				var portalLandingBxSlider = <?php echo json_encode($this->portalLandingBxSlider); ?>;
				var isMobile = $('section.ismobile-identifier').css('display') != 'none';
				jQuery(document).ready(function($){
					function getBrowser(){
					    var userAgent = navigator.userAgent.toLowerCase();
					    var pattern = {};
					 	pattern.chrome = /chrome/.test(userAgent);
					    pattern.safari= /webkit/.test(userAgent);
					    pattern.opera=/opera/.test(userAgent);
					    pattern.msie=/msie/.test( userAgent ) && !/opera/.test( userAgent );
					    pattern.mozilla= /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent ) || /firefox/.test(userAgent);

					    if(pattern.chrome) return "chrome";
					    if(pattern.mozilla) return "mozilla";
					    if(pattern.opera) return "opera";
					    if(pattern.safari) return "safari";
					    if(pattern.msie) return "ie";
					    return "ie";
					};
					<?php
						if (is_page('listing') || is_page('listing-viewer')) :
					?>
						if ($('ul.listing-gallery').children('li').length > 1)
							slider = $('ul.listing-gallery').bxSlider({
							  auto: true,
							  pause: 4000,
							  mode: 'fade',
							  responsive: 'false',
							  slideWidth: 1061
							});
					<?php
						elseif (is_page('listing-new')) :
					?>
						console.log("creating bxSlider for "+thisPage);
						if ($('ul.listing-gallery').children('li').length > 1)
							slider = $('ul.listing-gallery').bxSlider({
							  auto: true,
							  pause: 4000,
							  mode: 'fade',
							  // responsive: true,
							  slideWidth: 1061,
								touchEnabled: false,
							  // controls: true,
							  // pager: true,
							  nextText: '<span class="entypo-right-open-big right-control"></span>',
							  prevText: '<span class="entypo-left-open-big left-control"></span>',
							  nextSelector: '.bxslider-nav span.right',
							  prevSelector: '.bxslider-nav span.left',
							});
					<?php
						elseif (is_page('listing-gallery')) :
					?>
						if ($('ul.gallery-slider').children('li').length > 1)
							slider = $('ul.gallery-slider').bxSlider({
							  auto: false,
							  pause: 7000,
							  mode: 'fade',
							  responsive: 'false',
							  slideWidth: 900,
							  controls: false,
							  buildPager: function(theIndex){
							    if ( galleryImages[theIndex].file.substr(0, 4) != 'http' ) galleryImages[theIndex].file = '<?php bloginfo('stylesheet_directory'); ?>/_img/_listings/210x120/'+galleryImages[theIndex].file;
						  		return '<img src="'+galleryImages[theIndex].file+'" title="'+galleryImages[theIndex].title+'" />';
							  },
							  onSlideBefore: function(slideElement, oldIndex, newIndex){
							  	$('.meta').fadeOut(150, function(){
							  		theTitle = $.trim(galleryImages[newIndex].title).length > 0 && galleryImages[newIndex].title != 'N/A' ?
							  			galleryImages[newIndex].title : 'Image '+(newIndex+1)+' of '+galleryImages.length;
							  		theDescription = $.trim(galleryImages[newIndex].desc).length > 0 && galleryImages[newIndex].desc != 'blank' ?
							  			galleryImages[newIndex].desc : 'Image '+(newIndex+1)+' of '+galleryImages.length;
							  		$('.meta .subtitle').html(theTitle);
							  		$('.meta .desc').html();
							  		$('.meta').fadeIn(150);
							  	});
							  }
							});
					<?php
						elseif (is_page('portal-landing')) :
					?>
						var ele = $('ul.slide-gallery.desktop');
						if (ele.length) {
							if (ele.children('li').length > 1) {
								slider = ele.bxSlider({
								  auto:  typeof portalLandingBxSlider.desktop != 'undefined' &&
								  		 typeof portalLandingBxSlider.desktop.auto != 'undefined' ? portalLandingBxSlider.desktop.auto : true,
								  controls: typeof portalLandingBxSlider.desktop != 'undefined' &&
								  		 typeof portalLandingBxSlider.desktop.controls != 'undefined' ? portalLandingBxSlider.desktop.controls : false,
								  pager: typeof portalLandingBxSlider.desktop != 'undefined' &&
								  		 typeof portalLandingBxSlider.desktop.pager != 'undefined' ? portalLandingBxSlider.desktop.pager : false,
								  pause: typeof portalLandingBxSlider.desktop != 'undefined' &&
								  		 typeof portalLandingBxSlider.desktop.pause != 'undefined' ? portalLandingBxSlider.desktop.pause : 3000,
								  speed: typeof portalLandingBxSlider.desktop != 'undefined' &&
								  		 typeof portalLandingBxSlider.desktop.speed != 'undefined' ? portalLandingBxSlider.desktop.speed : 500,
								  mode:  typeof portalLandingBxSlider.desktop != 'undefined' &&
								  		 typeof portalLandingBxSlider.desktop.mode != 'undefined' ? portalLandingBxSlider.desktop.mode : 'fade',
								  responsive: 'false',
								  // slideWidth: typeof portalLandingBxSlider.desktop != 'undefined' &&
								  // 		 	  typeof portalLandingBxSlider.desktop.slideWidth != 'undefined' ? portalLandingBxSlider.desktop.slideWidth : 800
								});
								// ele.css("left", 
								// 		typeof portalLandingBxSlider.desktop != 'undefined' &&
								//   		typeof portalLandingBxSlider.desktop.left != 'undefined' ? portalLandingBxSlider.desktop.left : "20%");
								// ele.css("top", 
								// 		typeof portalLandingBxSlider.desktop != 'undefined' &&
								//   		typeof portalLandingBxSlider.desktop.top != 'undefined' ? portalLandingBxSlider.desktop.top : "80%");
								// if (!isMobile)
								// 	ele.fadeIn(500);
							}
						}

						var ele = $('ul.slide-gallery.mobile');
						if (ele.length) {
							if (ele.children('li').length > 1) {
								slider = ele.bxSlider({
								  auto:  typeof portalLandingBxSlider.desktop != 'undefined' &&
								  		 typeof portalLandingBxSlider.desktop.auto != 'undefined' ? portalLandingBxSlider.desktop.auto : true,
								  controls: typeof portalLandingBxSlider.desktop != 'undefined' &&
								  		 typeof portalLandingBxSlider.desktop.controls != 'undefined' ? portalLandingBxSlider.desktop.controls : false,
								  pager: typeof portalLandingBxSlider.desktop != 'undefined' &&
								  		 typeof portalLandingBxSlider.desktop.pager != 'undefined' ? portalLandingBxSlider.desktop.pager : false,
								  pause: typeof portalLandingBxSlider.mobile != 'undefined' &&
								  		 typeof portalLandingBxSlider.mobile.pause != 'undefined' ? portalLandingBxSlider.mobile.pause : 1500,
								  speed: typeof portalLandingBxSlider.desktop != 'undefined' &&
								  		 typeof portalLandingBxSlider.desktop.speed != 'undefined' ? portalLandingBxSlider.desktop.speed : 500,
								  mode:  typeof portalLandingBxSlider.desktop != 'undefined' &&
								  		 typeof portalLandingBxSlider.desktop.mode != 'undefined' ? portalLandingBxSlider.desktop.mode : 'fade',
								  responsive: 'false',
								  // slideWidth: typeof portalLandingBxSlider.mobile != 'undefined' &&
								  // 		 	  typeof portalLandingBxSlider.mobile.slideWidth != 'undefined' ? portalLandingBxSlider.mobile.slideWidth : 800
								});
								// ele.css("left", 
								// 		typeof portalLandingBxSlider.desktop != 'undefined' &&
								//   		typeof portalLandingBxSlider.desktop.left != 'undefined' ? portalLandingBxSlider.desktop.left : "5%");
								// ele.css("top", 
								// 		typeof portalLandingBxSlider.desktop != 'undefined' &&
								//   		typeof portalLandingBxSlider.desktop.top != 'undefined' ? portalLandingBxSlider.desktop.top : "80%");
								// if (isMobile)
								// 	ele.fadeIn(500);
							}
						}
					<?php
						elseif (is_page('home')) :
					?>
						var mode = getBrowser() == 'mozilla' ? 'fade' : 'fade';
						console.log("Browser: "+getBrowser());
						if ($('ul#listings').children('li').length > 1)
							slider = $('ul#listings').bxSlider({
							  auto: true,
							  pause: 6000,
							  easing: 'ease-in-out',
							  mode: 'fade',
							  slideZIndex: 0,
							  controls: false,
							  speed: 1200,
							  pager: false,	
							  buildPager: function(theIndex){
							    if ( homeImages[theIndex].file.substr(0, 4) != 'http' ) homeImages[theIndex].file = '<?php bloginfo('stylesheet_directory'); ?>/_img/_listings/158x64/'+homeImages[theIndex].file;
						  		return '<img src="'+homeImages[theIndex].file+'" title="'+homeImages[theIndex].title+'" alt="Home Image '+theIndex+'" />';
							  },
							  onSliderLoad: function(currentIndex) {
							  	// can't tell which li.img is up on screen from the currentSliderIndex() value
							  	// so color them all!
							 //  	if (getBrowser() != 'ie') 
								// 	$('.bx-viewport li').eq(currentIndex).each( function() { $('img', $('.bx-viewport li').eq(currentIndex)).removeClass('grayscale'); });
								// else
								// if (getBrowser() != 'mozilla') 
									$('.bx-viewport li').eq(currentIndex).each( function() { $('img', $('.bx-viewport li').eq(currentIndex)).removeClass('grayscale'); });
								// else {
								// 	//$('.bx-viewport li').eq(currentIndex).each( function() { $('div', $('.bx-viewport li').eq(currentIndex)).removeClass('grayscale'); });
								// 	$('.bx-viewport li').eq(currentIndex).each( function() { $('div', $('.bx-viewport li').eq(currentIndex)).toggleClass('grayscale-off'); });
								// }

								// }
								// else
								// 	$('.bx-viewport li').eq(currentIndex).each( function() { $('img', $('.bx-viewport li').eq(currentIndex)).removeClass('grayscale'); });

							  	//$('.bx-viewport li').each(function() { colorize($(this), 4000); });
							  },
							  onSlideBefore: function($element, oldIndex, newIndex) {
							  	// grey it out before display
							  	// $element.css('-webkit-filter', 'grayscale(1)');
							  	// keep comented code below for reference...
							  	// $element.each( function() { $('img', $element).toggleClass('gray'); } );  grayscale-fade
							 //  	if (getBrowser() != 'ie') 
								// 	$('.bx-viewport li').eq(oldIndex).each( function() { $('img', $('.bx-viewport li').eq(oldIndex)).addClass('grayscale'); });
								// else
								// if (getBrowser() != 'mozilla') 
									$('.bx-viewport li').eq(oldIndex).each( function() { $('img', $('.bx-viewport li').eq(oldIndex)).addClass('grayscale'); });
								// else {
								// 	// $('.bx-viewport li').eq(oldIndex).each( function() { $('div', $('.bx-viewport li').eq(oldIndex)).addClass('grayscale'); });
								// 	$('.bx-viewport li').eq(oldIndex).each( function() { $('div', $('.bx-viewport li').eq(oldIndex)).toggleClass('grayscale-off'); });
								// }
								// }
								// else
								// 	$('.bx-viewport li').eq(oldIndex).each( function() { $('img', $('.bx-viewport li').eq(oldIndex)).addClass('grayscale'); });
							  },
							  onSlideAfter: function($element, oldIndex, newIndex) {
							  	// keep comented code below for reference...
							  	// $element.css('-webkit-filter', 'grayscale(0)').delay(2000).fadeIn(5000);
							 //  	if (getBrowser() != 'ie') 
								// 	$element.each( function() { $('img', $element).removeClass('grayscale'); });
								// else
								// if (getBrowser() != 'mozilla') 
								  	$element.each( function() { $('img', $element).removeClass('grayscale'); });
								//   else {
								//   	// $element.each( function() { $('div', $element).removeClass('grayscale'); });
								//   	$element.each( function() { $('div', $element).toggleClass('grayscale-off'); });
								// 	//$('.bx-viewport li').eq(oldIndex).each( function() { $('div', $('.bx-viewport li').eq(oldIndex)).addClass('grayscale'); });
								// }
								  	// if (getBrowser() == 'ie') 
										// $('.bx-viewport li').eq(oldIndex).each( function() { $('img', $('.bx-viewport li').eq(oldIndex)).toggleClass('grayscale-off'); });
								// }
								// else {
								// 	colorize($element, 3000);
								// }
							  	// $element.toggleClass('grayScale');

							  	// now colorize it
								// colorize($element, 2000);
								// $('.bx-viewport li').eq(oldIndex).css('-webkit-filter', 'grayscale(1)');
							  }
							});
					<?php
						endif;
					?>
				});
			</script>
		<?php
		}
		else echo "<!-- failed to run get_bxslider -->";
	}
	private function title2slug($title = null){return str_replace(' ','-',strtolower($title)); }
	private function jqueryMobileFooter(){
		?><script type="text/javascript">jQuery(document).ready(function($){ $.mobile.loading().hide(); $.mobile.ajaxEnabled = false; });</script><?php
	}

	protected function setupPortal(&$id, &$session) {
		if ( (isset($_GET['portal_entry']) || isset($_POST['portal_entry'])) &&
			 (isset($_GET['nickname']) || isset($_POST['nickname'])) ) {
			$agentID = isset($_GET['portal_entry']) ? $_GET['portal_entry'] : (isset($_POST['portal_entry']) ? $_POST['portal_entry'] : 0); 
			$nickname = isset($_GET['nickname']) ? $_GET['nickname'] : (isset($_POST['nickname']) ? $_POST['nickname'] : ''); 
			$agency = isset($_GET['agency']) ? $_GET['agency'] : (isset($_POST['agency']) ? $_POST['agency'] : ''); 
			$cassie = isset($_COOKIE['CassiopeiaDirective']) ? $_COOKIE['CassiopeiaDirective'] : (isset($_SESSION['CassiopeiaDirective']) ? $_SESSION['CassiopeiaDirective'] : '');
			$campaign = isset($_GET['campaign']) ? $_GET['campaign'] : (isset($_POST['campaign']) ? $_POST['campaign'] : ''); 
			$this->log("setupPortal - agentID is $agentID, nickname: $nickname, agency: $agency, directive:$cassie, campaign:$campaign");
			// require_once(__DIR__.'/../_classes/SessionMgr.class.php'); $SessionMgr = new SessionMgr();
			$SessionMgr = $this->getClass('SessionMgr');
			$row = $SessionMgr->setPortalAgent($nickname, $agentID, true, true);
			if ($row === true)
				$row = '';
			$company = null;
			if (!empty($agency)) {
				// require_once(__DIR__.'/Associations.class.php'); $Associations = new Associations();
				$Associations = $this->getClass('Associations');
				$company = $Associations->get((object)['like'=>['company_code'=>$agency]]);
				unset($Associations);
			}
			else {
				// require_once(__DIR__.'/Sellers.class.php'); $Sellers = new Sellers();
				$Sellers = $this->getClass('Sellers', $this->logSellers);
				$seller = $Sellers->get((object)['where'=>['author_id'=>$agentID]]);
				if (!empty($seller)) {
					if (!empty($seller[0]->association)) {
						// require_once(__DIR__.'/Associations.class.php'); $Associations = new Associations();
						$Associations = $this->getClass('Associations');
						$company = $Associations->get((object)['where'=>['id'=>$seller[0]->association]]);
						if (!empty($company))
							$agency = $company[0]->company_code;
					}
				} 
				else {
					$this->log("setupPortal - cannot find agent:$agentID, negating portal request..");
					$ow = 0;
					$nickname = '';
					$agentID = 0;
				}
				unset($Sellers);
			}
			$this->log("setupPortal - called setPortalAgent() - nickname:$nickname, agentID:$agentID, campaign:$campaign, agency: $agency, company:".(!empty($company) ? print_r($company[0], true) : "N/A").", row:$row, headers_sent:".(headers_sent() ? "yes" : "no"));

			if (!empty($agency) &&
				empty($company)) {
				if (isset($_GET['agency'])) unset($_GET['agency']); elseif (isset($_POST['agency'])) unset($_POST['agency']);
			}

			$id = 0;
			$session = null;
			$session_id = $SessionMgr->getCurrentSession($id, $session);


			// setcookie('PHPSESSID', "0", time() + (86400 * 1), "/"); // 86400 = 1 day
			if (!headers_sent()) {
				setcookie('WAIT_PORTAL_ROW', $row, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
				setcookie('PROFILE_AGENT_NAME', $nickname, time() + (86400 * 1), "/"); // 86400 = 1 day
				setcookie('PROFILE_AGENT_ID', $agentID, time() + (86400 * 1), "/"); // 86400 = 1 day
				setcookie('AGENT_CAMPAIGN', $campaign, time() + (86400 * 1), "/"); // 86400 = 1 day
				setcookie('TRACK_PREMIER_AGENT_USAGE', ($agentID ? 'true' : 'false'), time() + (86400 * 1), "/"); // 86400 = 1 day
				if (!empty($company)) {
					setcookie('AGENCY', $agency, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
					setcookie('AGENCY_ID', $company[0]->id, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
					setcookie('AGENCY_LOGO', $company[0]->logo, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
					setcookie('AGENCY_LOGO_X_SCALE', $company[0]->logo_x_scale, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
					setcookie('AGENCY_URL', !empty($company[0]->url) ? $company[0]->url : '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
					setcookie('AGENCY_SHORTNAME', !empty($company[0]->shortname) ? $company[0]->shortname : '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
					setcookie('ACTIVE_SESSION_ID', $id, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
				}
				else {
					setcookie('AGENCY', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
					setcookie('AGENCY_ID', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
					setcookie('AGENCY_LOGO', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
					setcookie('AGENCY_LOGO_X_SCALE', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
					setcookie('AGENCY_URL', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
					setcookie('AGENCY_SHORTNAME', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
				}
			}
			$_SESSION['PROFILE_AGENT_NAME'] = $nickname;
			$_SESSION['PROFILE_AGENT_ID'] = $agentID;
			$_SESSION['TRACK_PREMIER_AGENT_USAGE'] = $agentID ? 'true' : 'false';
			$_SESSION['AGENCY'] = !empty($agency) ? $agency : '';
			$_SESSION['AGENCY_ID'] = !empty($company) ? $company[0]->id : 0;
			$_SESSION['AGENCY_LOGO'] = !empty($company) ? $company[0]->logo : 'blank.jpg';
			$_SESSION['AGENCY_LOGO_X_SCALE'] = !empty($company) ? $company[0]->logo_x_scale : 1.00;
			$_SESSION['AGENCY_URL'] = !empty($company) && !empty($company[0]->url) ? $company[0]->url : '';
			$_SESSION['AGENCY_SHORTNAME'] = !empty($company) && !empty($company[0]->shortname) ? $company[0]->shortname : '';
			$_SESSION['ACTIVE_SESSION_ID'] = $id;
			$_SESSION['AGENT_CAMPAIGN'] = $campaign;
		}
	}


	protected function findPortalUser($wpUserID) {
		$user = $this->getClass('PortalUsers')->get((object)['where'=>['wp_user_id'=>$wpUserID]]);
		return $user;
	}

	public function getSessionData($session, &$portalUser, &$agentID, &$first = '', &$last = '', &$email = '') {
		$portalUser = 0;
		$agentID = 0;
		if (empty($session))
			return;
		$this->log("getSessionData for $session->id");
		if ($session &&
			!empty($session->value)) {
			foreach($session->value as $meta) {
				$meta = (object)$meta;
				if ($meta->type == SESSION_PORTAL_USER) {
					$portalUser = intval($meta->portalUser);
				}
				elseif ($meta->type == SESSION_PREMIER_AGENT) {
					$agentID = $meta->agentID;
				}
				unset($meta);
			}
		}

		if ($portalUser) {
			$pUser = $this->getClass('PortalUsers')->get((object)['where'=>['id'=>$portalUser]]);
			if (!empty($pUser)){
				$first = $pUser[0]->first_name;
				$last = $pUser[0]->last_name;
				$email = $pUser[0]->email;
			}
			unset($pUser);
		}
	}

}
