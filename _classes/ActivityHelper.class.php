<?php
namespace AH;
require_once(__DIR__.'/../_classes/_Controller.class.php');

class ActivityHelper extends Controller {
	public function __construct(){
		$in = parent::__construct();
		return $in;
	}

	protected function getActivityBySession($session){
		try {
			if (!empty($session->quiz_id)) {
				return $this->getClass('QuizActivity')->get((object) array('where'=>array('id'=>$session->quiz_id)));
			}
			else
				return $this->getClass('QuizActivity')->get((object) array('where'=>array('session_id'=>$session->session_id)));
		} catch (Exception $e) { parseException($e); die(); }
	}

		// $session_id is now the 'id' column of Sessions table, and NOT session_id column
	// So get the Session row and use the quiz_id field to get the row from QuizActivity table
	protected function getActivityBySessionID($session_id, $quizId = 0, &$newSession = null, &$newSessionId = null){
		try {
			$ses = $this->getClass('Sessions')->get((object) array('where'=>array('id'=>$session_id)));
			if (!empty($ses &&
				!empty($ses[0]->quiz_id))) {
				$activity = $this->getClass('QuizActivity', 1)->get((object) array('where'=>array('id'=>$ses[0]->quiz_id)));
				if (!empty($activity) &&
					$quizId &&
					$quizId != -1 &&
					$activity[0]->id != $quizId) {
					$this->log("getActivityBySessionID - incoming quizId:$quizId not equal to session:{$session_id}'s quizId:{$ses[0]->quiz_id}, forcing it to incoming quizId");
					$activity = $this->getClass('QuizActivity', 1)->get((object) array('where'=>array('id'=>$quizId)));
				}
				return $activity;
			}
			elseif ($quizId &&
					$quizId != -1)
				return $this->getClass('QuizActivity', 1)->get((object) array('where'=>array('id'=>$quizId)));
			else {
				$activities = $this->getClass('QuizActivity', 1)->get((object) array('where'=>array('session_id'=>$session_id)));
				if (count($activities) > MAX_SESSION_OWN_QUIZ &&
					$newSessionId !== null &&
					$newSession !== null) {
					$session = '';
					$ip = $this->getClass('SessionMgr')->userIP();
					$newId = $this->getClass('SessionMgr')->setIPAssoc($ip, $session, true);
					if ($newId != $session_id) {
						$newSessionId = $newId;
						$newSession = $session;
						$lastActivity = array_pop($activities);
						$values = [];
						$fields['user_id'] = $ses[0]->user_id;
						$fields['quiz_id'] = $lastActivity->id;
						array_push($activities, $lastActivity); // put back;

						if (!empty($ses[0]->value)) foreach($ses[0]->value as $meta)
							if ($meta->type == SESSION_PREMIER_AGENT)
								$values[] = $meta;
							elseif ($meta->type == SESSION_PORTAL_USER)
								$values[] = $meta;

						if (count($values))
							$fields['value'] = $values;
						
						$this->getClass('Sessions')->set([(object)['where'=>['id'=>$newId],
																   'fields'=>$fields]]);
						$this->log("getActivityBySessionID - ASSIGNED new session:$newId for impacted session_id:$session_id, which owned ".count($activities)." activities.");
					}
					else
						$this->log("getActivityBySessionID - FAILED to get new session for impacted session_id:$session_id, it has ".count($activities)." activities.");
				}
				$this->log("getActivityBySessionID - Found for session_id:$session_id, ".count($activities)." activities.");
				return $activities; 
			}

		} catch (Exception $e) { parseException($e); die(); }
	}

	// session_id is the 'id' column of the Sessions table
	protected function getLastQuiz($session_id = null){
		try {
			if (empty($session_id)) throw new \Exception('No session id provided.');
			$activity = $this->getActivityBySessionID($session_id);
			if (empty($activity)) {
				$this->log("getLastQuiz - found no activity with $session_id"); 
				return false; 
			}
			else {
				$activity = array_pop($activity);
				$size = count($activity->data);

				$a = (array)$activity->data;
				$size = count($a);
				$index = count($a) > 0 ? count($a)-1 : 0;
				$b = &$a[$index];
				// this while loop should not trigger since $index should be 0
				while ($index >= 0 && !isset($b->results)){
					$index--;
					$b = &$a[$index];
				}
				if (empty($b->results)) {
					$this->log("getLastQuiz - found no result in QA:$activity->id"); 
					return false; //throw new \Exception('No results.');
				}
				$b->size = $size;
				// $b->index = $index;
				$b->index = $activity->id; // return row index of QuizActivity now..
				return $b;
			}
		} catch (\Exception $e) { 
			$this->log("Exception: ".$e->getMessage());
			parseException($e, true); 
			// throw $e;
		}
	}

	protected function getProfiles($sessionId) {
		$this->log("getProfiles - entered for session_id:$sessionId");
		if (empty($sessionId))
			return false;

		$profiles = [];
		$sessionForID = $this->getClass('Sessions')->get((object)array('where'=>array('id'=>$sessionId)));
		if (!empty($sessionForID))
			$sessionForID = array_pop($sessionForID);
		else
			throw new \Exception("Failed to find session with id: $sessionId");

		$s = $this->getClass('SessionMgr')->getSessions(); // all sessions based on userId
		$this->log("getProfiles - has ".count($s)." sessions to work with", 2);
		$user = $this->getClass('Register')->getUserData(-1); // valid regiestered user?
		$this->log("getProfiles - id:$sessionId, session_id:$sessionForID->session_id, user:".(!empty($user) && $user->status == 'OK' ? $user->data->user_id : '0'), 3);


		$lastQuiz = null;
		try {
			$lastQuiz = $this->getLastQuiz($sessionId);
			$this->log("getProfiles - getLastQuiz() returned ".(!empty($lastQuiz) ? $lastQuiz->index : "nothing"), 2);
			// NOTE: below check not needed anymore since each QuizActivity should only have one result/activity
			// if ($lastQuiz &&
			// 	$lastQuiz->index != ($lastQuiz->size-1)) // then this quiz was not the last one, but some previous result
			// 	$lastQuiz = null;
		}
		catch( \Execption $e) {
			// oops, no last quiz!
			// parseException($e); 
			$this->log("Exception from getLastQuiz: ".$e->getMessage());
		}

		// if we have lastQuiz set, make sure it didn't come from a user owned session if the userid == 0
		if (!empty($lastQuiz) &&
			!empty($s) &&
			$user->status != 'OK') {
			foreach($s as $ses) {
				// if ($ses->session_id == $sessionId &&
				if ($ses->id == $sessionId &&
					$ses->user_id != 0) {
					if ( $ses->ip_dot == $this->getClass('SessionMgr')->userIP() )
						break; // then same session on same ip, probably just logged out
					$lastQuiz = null;
					break;
				}
			}
		}
		
		$haveLastQuizProfiled = false;
		if (!empty($s) &&
			$user->status == 'OK') {
			// then grab all the sessions owned by this user
			// if (isset($s[0]->user_id) &&
			// 	$s[0]->user_id != 0) { 
			// 	$s = $this->getClass('Sessions')->get((object)array('where'=>array('user_id'=>$s[0]->user_id)));
			// }
			foreach($s as $j=>$ses) {
				$data = $ses->value;
				if (isset($data)) {
					$data = (array)$data;
					$msg = "session: $j has ".count($data)." elements";
					$this->log($msg, 2);
					foreach($data as $i=>$info) {
						if ($info->type == SESSION_PROFILE_NAME) {
							if (isset($info->owner) &&
								$info->owner != $user->data->user_id) {
								$this->log("Different owner, expecting:$info->owner, but current user is {$user->data->user_id}, session: $j index: $i, quizType: ".$info->quizType.", name:".$info->name.", id:".$info->quizId);
								continue;
							}
							$msg = "session: $j index: $i, quizType: ".$info->quizType.", name:".$info->name.", id:".$info->quizId;
							$this->log($msg, 2);
							if ( ($info->quizType % 2) != 1 &&
								!is_array($info->locations))
								$info->locations = [$info->locations];

							if ( ($info->quizType % 2) != 1 &&
								!empty($info->locations) &&
								count($info->locations)) // convert to city-name
								foreach($info->locations as $k=>$loc) {
									if (!empty($loc)) {
										$c = $this->getClass('Cities')->get((object)['where'=>['id'=>$loc]]);
										$info->locations[$k] = !empty($c) ? $c[0]->city.','.$c[0]->state : "N/A";
									}
									unset($k, $loc);
								}

							if ($lastQuiz != null &&
								// $ses->session_id == $sessionId &&
								$ses->id == $sessionId &&
								$info->quizId == $lastQuiz->index)
								$haveLastQuizProfiled = true;


							$info->session_id = $ses->session_id;
							$info->activeSessionID = $ses->id;
							if (!$this->profileInList($profiles, $info)) {
								$msg = "session: $j index: $i, added to profiles - quizType: ".$info->quizType.", name:".$info->name.", id:".$info->quizId;
								$this->log($msg, 2);
								$profiles[] = $info;
							}
							else {
								$msg = "session: $j index: $i, duplicate - not added - quizType: ".$info->quizType.", name:".$info->name.", id:".$info->quizId;
								$this->log($msg, 2);
							}
						}
						else {
							$msg = "session:$ses->session_id, session index: $j meta index: $i, type:$info->type";
							$this->log($msg, 2);
						}
						unset($info);
					}
				}
				unset($ses);
			}
		}
		
		$this->log("getProfiles - user status:$user->status, haveLastQuizProfiled:".($haveLastQuizProfiled ? "true" : "false").", lastQuiz:".(!empty($lastQuiz) ? $lastQuiz->index: "n/a"), 2);

		if (!$haveLastQuizProfiled &&
			!empty($lastQuiz &&
			$user->status == 'OK') ) {
			$info = new \stdClass();
			$info->type = SESSION_PROFILE_NAME;
			$info->name = "View Last Search";
			$info->quizId = $lastQuiz->index;
			$info->session_id = $sessionForID->session_id;
			$info->activeSessionID = $sessionForID->id;
			$info->quizType = $lastQuiz->quiz;
			$info->locations = isset($lastQuiz->location) ? $lastQuiz->location : [];
			$info->distance = isset($lastQuiz->distance) ? $lastQuiz->distance : 0;
			// $info->session_id = $sessionForID->session_id;
			// $info->id = $sessionId;
			// $last = [];
			// $last[] = $info;
			// $profiles = array_merge($last, $profiles);
			$profiles[] = $info;
		}
		return count($profiles) ? $profiles : false;
	}
}