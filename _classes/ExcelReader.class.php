<?php
namespace AH;
require_once(__DIR__.'/Utility.class.php');
set_include_path(__DIR__.'/_PHPExcel');
require_once('PHPExcel/IOFactory.php');
class ExcelReader {
	public function __construct($filename = null){
		if (empty($filename)) throw new \Exception('No filename provided.');
		if (!file_exists($filename)) throw new \Exception('File does not exist: '.$filename);

		$this->file = (object)[
			'name' => $filename,
			'type' => \PHPExcel_IOFactory::identify($filename),
		]; unset($filename);

		// initialize reader
		$this->reader = \PHPExcel_IOFactory::createReader($this->file->type);
		$this->reader->setReadDataOnly(true);
	}
	public function read($num_rows = null, $first_row = 1){
		if ($num_rows < 1 || empty($num_rows)){
			return $this->reader->load($this->file->name)->getActiveSheet()->toArray(null,true,true,true);
		} else {
			// setup read filter
			$readFilter = new ReadFilter($first_row, $num_rows);
			$this->reader->setReadFilter($readFilter);
			return $this->reader->load($this->file->name)->getActiveSheet()->toArray(null,true,true,true);
		}
	}
}

class ReadFilter implements \PHPExcel_Reader_IReadFilter {
	private $_startRow = 0;
	private $_endRow = 0;

	/**  We expect a list of the rows that we want to read to be passed into the constructor  */
	public function __construct($startRow = 1, $chunkSize = 20) {
		$this->setRows($startRow, $chunkSize);
	}

	public function setRows($startRow, $chunkSize) {
		$this->_startRow	= $startRow;
		$this->_endRow		= $startRow + $chunkSize;
	}

	public function readCell($column, $row, $worksheetName = '') {
		//  Only read the heading row, and the rows that were configured in the constructor
		if (($row == 1) || ($row >= $this->_startRow && $row < $this->_endRow)) {
			return true;
		}
		return false;
	}
}