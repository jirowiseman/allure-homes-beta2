<?php
namespace AH;
require_once(__DIR__.'/Utility.class.php');
require_once(__DIR__.'/_Base.class.php');
class Options extends Base {
	public function __construct($logIt = 0) {
		parent::__construct($logIt);
		// $this->log_to_file = $logIt;
		// $this->logFile = $this->log_to_file ? new Log(__DIR__.'/_logs/Options.class.log') : null;
	}

	public function __destruct() {
		if ($this->log !== null)
			$this->log->writeStringToFile();
	}

}