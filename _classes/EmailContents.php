<?php

global $html_body;
$html_body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html> <head> <meta http-equiv="Content-Type" content="text/html;UTF-8" /> </head> <body style="margin: 0px; background-color: #F4F3F4; font-family: Helvetica, Arial, sans-serif; font-size:12px;" text="#444444" bgcolor="#F4F3F4" link="#21759B" alink="#21759B" vlink="#21759B" marginheight="0" topmargin="0" marginwidth="0" leftmargin="0"> <table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F4F3F4"> <tbody> <tr> <td style="padding: 15px;"><center> <table width="100%" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff"> <tbody> <tr> <td> <img class="logoDiv" style="margin: 0 0 0 0; width: 100%; height: 60%;" src="http://lifestyledlistings.com/wp-content/themes/allure/_img/lifestyled-listings-logo-email.png" alt="LIFESTYLED LISTING" /></td> </tr> <tr> <td align="left"> <div style="border: solid 1px #d9d9d9;"> <table id="header" style="line-height: 1.6; font-size: 12px; font-family: Helvetica, Arial, sans-serif; border: solid 1px #FFFFFF; color: #444;" border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff"> <tbody> <tr> <td style="color: #ffffff;" colspan="2" valign="bottom" height="30px">.</td> </tr> <tr> <td style="line-height: 32px; padding-left: 30px;" valign="baseline"><span style="font-size: 32px;"><a style="text-decoration: none;" href="%blog_url%" target="_blank">%blog_name%</a></span></td> <td style="padding-right: 30px;" align="right" valign="baseline"><span style="font-size: 14px; color: #777777;">%blog_description%</span></td> </tr> </tbody> </table> <table id="content" style="margin-top: 15px; margin-right: 10px; margin-left: 10px; color: #444; line-height: 1.6; font-size: 12px; font-family: Arial, sans-serif;" border="0" width="98%" cellspacing="0" cellpadding="0" bgcolor="#ffffff"> <tbody> <tr> <td style="border-top: solid 1px #d9d9d9;" colspan="2"> <div style="padding: 0 0;">%content%</div> </td> </tr> </tbody> </table> <table id="footer" style="line-height: 1.5; font-size: 12px; font-family: Arial, sans-serif; margin-right: 30px; margin-left: 30px;" border="0" width="490" cellspacing="0" cellpadding="0" bgcolor="#ffffff"> <tbody> <tr style="font-size: 11px; color: #999999;"> <td style="border-top: solid 1px #d9d9d9;" colspan="2"> <div style="padding-top: 15px; padding-bottom: 1px;"><img style="vertical-align: middle;" src="http://beta.allure-homes.com/wp-admin/images/date-button.gif" alt="Date" width="13" height="13" /> Email sent %date% @ %time%</div> <div><img style="vertical-align: middle;" src="http://beta.allure-homes.com/wp-admin/images/comment-grey-bubble.png" alt="Contact" width="12" height="12" /> For any requests, please contact <a href="mailto:%admin_email%">%admin_email%</a></div> </td> </tr> <tr> <td style="color: #ffffff;" colspan="2" height="15">.</td> </tr> </tbody> </table> </div> </td> </tr> </tbody> </table> </center></td> </tr> </tbody> </table> </body></html>';

$emailContent = array(
	'Intro'=>array(
		'Active'=>array( 'fields'=>array('first_name','street_address'),
						 'content'=>'' ),
		'Waiting'=>array( 'fields'=>array('first_name','street_address'),
						 'content'=>'' ),
		'NoneAllowed'=>array( 'fields'=>array('first_name'),
						 'content'=>'' ),
		'NoListing'=>array( 'fields'=>array('first_name'),
						 'content'=>'' )),
	'Tags'=>array(
		'Great'=>array( 'fields'=>array('tag-count'),
						 'content'=>'' ),
		'Good'=>array( 'fields'=>array('tag-count'),
						 'content'=>'' ),
		'Poor'=>array( 'fields'=>array('tag-count'),
						 'content'=>'' )),
	'Description'=>array(
		'Great'=>array( 'fields'=>array('about'),
						 'content'=>'' ),
		'Good'=>array( 'fields'=>array('about'),
						 'content'=>'' ),
		'Poor'=>array( 'fields'=>array('about'),
						 'content'=>'' )),
	'Photos'=>array(
		'Great'=>array( 'fields'=>array('photo-count'),
						 'content'=>'' ),
		'Good'=>array( 'fields'=>array('photo-count'),
						 'content'=>'' ),
		'Poor'=>array( 'fields'=>array('photo-count'),
						 'content'=>'' )),
	'Bio'=>array(
		'Photo'=>array( 'fields'=>array('first_name'),
						 'content'=>'' ),
		'AreaBlob'=>array( 'fields'=>array('first_name'),
						 'content'=>'' ),
		'Tags'=>array( 'fields'=>array('first_name'),
						 'content'=>'' )),
);
