<?php
namespace AH;
require_once(__DIR__.'/Utility.class.php');
require_once(__DIR__.'/Logger.class.php');

class GoogleLocation extends Logger {
	private $APIkey = array(
		'0',
		//'AIzaSyA-X7VaeHvCIgXkiIHLao8_Yb1D5qmYtd8', // alluretechnologies@gmail
		// 'AIzaSyC_PKp6lCyFd0Yv4_NPG6G1e7I9WmhX49s', // Tom Shani's
		// 'AIzaSyDTLoaYGWCE1_EOwB5ty4LV5piq58WIo_A',// Jiro's
		// 'AIzaSyBbUep2qTJOj4eI6lF294Fu6Sv5WK-JEvA', // JD's
		// 'AIzaSyDSYHMelyurUAoz6DiyM6HEjFdjg2RbQyI', // Tom Tong's
		'AIzaSyCftrncjafmT1NCabvd0R-fHj-3Tqnk-V8', // Tom Tong's 2nd key
		// 'AIzaSyA7ioHUCvf56kfnyrdESQ5g0eu3ryftH6Q', // Tom Tong's 3rd key
		// 'AIzaSyDzCP3SPXOAs2-40Ezt_cWTwCjLwF8mAmY', // Tom Tong's 4th key
		// 'AIzaSyBWTfABVg56b_IfYMH0W8Xx7gkiMwNqYOY', // Omar (Jiro's friend)
		// 'AIzaSyByGEoVzj7i6hrIQFhe-CcsP_9Y0NwY2f0', // lifestyledlistings@gmail
		// 'AIzaSyCwksr2EPQr2UTd0UowCmiE0lPNLz5WRB0', // teamwfo - Bennett
		// 'AIzaSyCIRoNiD_fQ2LYzgx-NEXw4snFtRKvsMG8', // ditto
		// 'AIzaSyAkmMgHoQH20FUR929Zqtw_NHaCd6Oqmu0', // jiro@lifestyledlistings.com
		// 'AIzaSyBQKOOaKiUtVfKUV5-pY7KrPl7EF9Go58k' // ditto
		//'AIzaSyBEXXVpe8b0AvY3Z1kJYcZReVkp62PZ-8w', // Mel
		//'AIzaSyDYZSxI9y_9_6tt2IAof0Vswvp3tt15vt0', // Jiro's fake #2
		//'AIzaSyDDBzIZGoUPUl5o19ot8zkGO54YIdT9Hy0',  // Anne
		//'AIzaSyAxjItvLHBdRlMLEKXGGU1NoHTJyrAHkX4', // Tom Tong's 12/14/15
		//'AIzaSyBxjC_Hl2UpvyqPk7MOobUs3KCev7jSrYQ', // ditto
		//'AIzaSyDy58kSRHRsfqMxQwO8WvwVnmfrFNrXzuI', // ditto
		//'AIzaSyBDaXuQp8hv086gz_gaKoddbU44Ak1FAYw', // ditto
		//'AIzaSyBXvVUX-fXxddXfZK1YH3IqXrVbMCIv9xA', // Jiro's 12/15/15
		//'AIzaSyD8XSBMZ6Z_MP7loLOL_mtol0ISMVGQ6tk', // ditto
		//'AIzaSyBYbMtuEma8AzDhw5LjBvq5BS2aSii3k4I', // ditto
		//'AIzaSyBxlAlyEgeqzqMHFdvL5oh5BGYUjVRR6A0'  // ditto
		'AIzaSyBsFKxxkkugepI7DETjTA0BlZrJVeyRzME', // Tom Tong's
		'AIzaSyAdiX3IG6HthATU9zf1tfl--E--3d0E7E4', // Tom Tong's 3rd key
		'AIzaSyDTSLyYeovFCgNaIhBDcrd6qqxAfCVQecc', // Tom Tong's 4th key
		'AIzaSyAkgHhmp2qpoJsZAVLIVfANDfvTFTn-QCk',	// Tom Tong's 
		'AIzaSyBpJITf3EKisv9PT0cu_R16a8WcAmTR89A',	// Tom Tong's
		);

	public function __construct($logIt = 0) {
		parent::__construct( $logIt, 'googleLocation' );
		require_once(__DIR__.'/Options.class.php'); $this->Options = new Options();
		$this->apiIndex = 0;
		$this->timeToReset = 20; // in hours, this will guarantee at least one day cycle between feed parsing
		$this->startTime = microtime(true);
		$this->lockfile = null;

		$this->lockfile = fopen(__DIR__.'/_logs/geoCode.lock', 'c'); 

		$q = new \stdClass();
		$q->where = array('opt' => 'GeoCodeKeyIndex');
		$x = $this->Options->get($q);
		if (!empty($x)) {
			$data = json_decode($x[0]->value);
			$time = microtime(true);
			// time in seconds in float format.
			// 3600 secs/hr
			if ( ($time - floatval($data->time)) > (3600 * $this->timeToReset) )
				$this->apiIndex = 0;
			else
				$this->apiIndex = intval($data->index);
		}
		$this->recordIndex();
	}

	public function __destruct() {
		if ($this->lockfile);
			fclose($this->lockfile);
	}

	private function recordIndex() {
		//require_once(__DIR__.'/Options.class.php'); $options = new Options();
		$q = new \stdClass();
		$q->where = array('opt' => 'GeoCodeKeyIndex');
		$x = $this->Options->get($q);
		if (is_array($x)) // delete it
			$this->Options->delete($q->where);

		$data = array('index'=>(string)$this->apiIndex,
					  'time'=>microtime(true));
		$data = array('opt'=>'GeoCodeKeyIndex',
					  'value' => json_encode($data));
		$this->Options->add((object)$data);
	}

	protected function runGeocode($call, &$geocoded) {
		flock($this->lockfile, LOCK_EX);
		$lastTime = $this->Options->get((object)['where'=>['opt'=>'GeoCodeTimeStamp']]);
		$lastTime = json_decode($lastTime[0]->value);
		$curTime = microtime(true);
		$done = false;
		if ( ($curTime-$lastTime->time) < 1.0 ) {
			if ( $lastTime->count < 5 ) {
				$geocoded = json_decode(file_get_contents($call));
				$lastTime->count++;
				$this->Options->set([(object)['where'=>['opt'=>'GeoCodeTimeStamp'],
										'fields'=>['value'=>json_encode($lastTime)]]]);
				$done = true;
			}
			else
				usleep( ($curTime-$lastTime->time+0.1) * 1000000); // sleep off whatever time we need to since last fifth call
				// time_nanosleep(0, ($curTime-$lastTime->time+0.01)*1000000000 );
		}
		
		if (!$done) {
			$geocoded = json_decode(file_get_contents($call));
			$lastTime->count = 1;
			$lastTime->time = microtime(true);
			$this->Options->set([(object)['where'=>['opt'=>'GeoCodeTimeStamp'],
									'fields'=>['value'=>json_encode($lastTime)]]]);
		}

		flock($this->lockfile, LOCK_UN);
	}

	public function geocodeZip() {
		if (func_num_args() !== 2) return new Out(0, 'geocodeZip: invalid number of arguments'); else {
			$zip = func_get_arg(0);
			$nthTry = func_get_arg(1);
			$nthTry = is_string($nthTry) ? intval($nthTry) : $nthTry;
			if ($nthTry == count($this->APIkey))
				return new Out(0,  (object)['status'=>'OVER_QUERY_LIMIT'] );
			
			$call = "https://maps.googleapis.com/maps/api/geocode/json?address=$zip&sensor=false";
			if ($this->apiIndex)
				$call .= '&key='.$this->APIkey[$this->apiIndex];
			$geocoded = null;
			$this->runGeocode($call, $geocoded);
			if (property_exists($geocoded, 'status') &&
				($geocoded->status == 'OVER_QUERY_LIMIT' ||
				 $geocoded->status == 'REQUEST_DENIED')) {
				if ($this->apiIndex == (count($this->APIkey)-1) ) // out of keys
					$this->apiIndex = 0; // reset
				else 
					$this->apiIndex++;
				$this->recordIndex();
				return $this->geocodeZip(func_get_arg(0), $nthTry+1);
			}

			if ($geocoded->status == 'OK') return new Out('OK', $geocoded->results[0]);
			return new Out(0, $geocoded);
		}
	}

	/**
	 * Returns Geocoded result from Google Maps API
	 * @param  [string] func_get_arg(0) [address string to parse]
	 * @return [AH\Out] [status of Google response, result dataset]
	 */
	public function geocodeAddress(){ // requires address
		if (func_num_args() !== 2) return new Out(0, 'geocodeAddress: invalid number of arguments'); else {
			$address = str_replace(' ', '+', func_get_arg(0));
			$nthTry = func_get_arg(1);
			$nthTry = is_string($nthTry) ? intval($nthTry) : $nthTry;
			if ($nthTry == count($this->APIkey))
				return new Out(0,  (object)['status'=>'OVER_QUERY_LIMIT'] );
			// try the call without a key...
			$call = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&sensor=false';
			if ($this->apiIndex)
				$call .= '&key='.$this->APIkey[$this->apiIndex];
			$geocoded = null;
			$this->log("geocodeAddress - calling:$call");
			$this->runGeocode($call, $geocoded);
			$this->log("geocodeAddress - got back status:$geocoded->status");
			
			if (property_exists($geocoded, 'status') &&
				($geocoded->status == 'OVER_QUERY_LIMIT' ||
				 $geocoded->status == 'REQUEST_DENIED')) {
				if ($this->apiIndex == (count($this->APIkey)-1) ) // out of keys
					$this->apiIndex = 0; // reset
				else 
					$this->apiIndex++;
				$this->recordIndex();
				return $this->geocodeAddress(func_get_arg(0), $nthTry+1);
			}

			if ($geocoded->status == 'OK') return new Out('OK', $geocoded->results[0]);
			return new Out(0, $geocoded);
		}
	}

	public function geocodeLatLng(){ // requires lat lng
		if (func_num_args() !== 2) return new Out(0, 'geocodeAddress: invalid number of arguments'); else {
			$address = str_replace(' ', '', func_get_arg(0));
			$address = str_replace('+', ',', $address);
			$nthTry = func_get_arg(1);
			$nthTry = is_string($nthTry) ? intval($nthTry) : $nthTry;
			if ($nthTry == count($this->APIkey))
				return new Out(0,  (object)['status'=>'OVER_QUERY_LIMIT'] );
			$call = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$address.'&sensor=false';
			if ($this->apiIndex)
				'&key='.$this->APIkey[$this->apiIndex];
			$geocoded = null;
			$this->runGeocode($call, $geocoded);
			
			if ($geocoded->status == 'OVER_QUERY_LIMIT' ||
				$geocoded->status == 'REQUEST_DENIED') {
				if ($this->apiIndex == (count($this->APIkey)-1) ) // out of keys
					$this->apiIndex = 0;  // reset
				else 
					$this->apiIndex++;
				$this->recordIndex();
				return $this->geocodeLatLng(func_get_arg(0), $nthTry+1);
			}

			if ($geocoded->status == 'OK') return new Out('OK', $geocoded->results[0]);
			return new Out(0, $geocoded);
		}
	}
	/**
	 * Returns the distance between points as a float rounded to 2 decimal places
	 * @param  [float] $lat1 [latitude input 1]
	 * @param  [float] $lon1 [longitude input 1]
	 * @param  [float] $lat2 [latitude input 2]
	 * @param  [float] $lon2 [longitude input 2]
	 * @return [float]       [distance between points]
	 */
	public function getDistance($lat1, $lng1, $lat2, $lng2){
	  $theta = $lng1 - $lng2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    return round($dist * 60 * 1.1515, 2);
	}
	public function nearbySearch($lat, $lng, $query = null){
		$nearby = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='.$lat.','.$lng.'&rankby=distance&keyword='.$query.'&sensor=false&key='.$this->APIkey));
		if ($nearby->status == 'OK'){
			$results = [];
			foreach ($nearby->results as $point){
				$x = (object) [
					'address'=> $point->vicinity,
					'distance' => $this->getDistance(floatval($lat), floatval($lng), $point->geometry->location->lat, $point->geometry->location->lng),
					'name' => $point->name,
					'lat' => $point->geometry->location->lat,
					'lng' => $point->geometry->location->lng,
				];
				// if ( is_array($point->photos) && count($point->photos) > 0){
				// 	$x->photo = $point->photos[0]->photo_reference;
				// 	// $x->photo = $this->APIurl.'photo?maxheight=71&photoreference='.$photoref.'&sensor=false&key='.$this->APIkey;
				// 	// $x->photo = file_get_contents($this->APIurl.'photo?maxheight=71&photoreference='.$photoref.'&sensor=false&key='.$this->APIkey);
				// }
				// $x->icon = $results[$i]['nearby'][$j]->icon;
				// $x->place_id = $results[$i]['nearby'][$j]->id;
				// $x->place_ref = $results[$i]['nearby'][$j]->reference;
				$results[] = $x;
			}
			return $results;
		} else return $nearby;
	}
}
