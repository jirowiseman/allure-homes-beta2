<?php
namespace AH;

// Load WP components, no themes
if ( ! defined( 'WP_USE_THEMES' ) )
	define('WP_USE_THEMES', false);
// define('WP_ADMIN', true);2

require_once(__DIR__.'/IpList.class.php');
require_once(__DIR__.'/_Controller.class.php');
require_once(__DIR__.'/Utility.class.php');
require_once(__DIR__.'/IPv6.php');
require_once(__DIR__.'/States.php');



// require_once(__DIR__.'/../../../../wp-load.php');
// require_once(__DIR__.'/../../../../wp-includes/pluggable.php');
// require_once(__DIR__.'/../../../../wp-admin/includes/admin.php' );
	
/** Load Ajax Handlers for WordPress Core */
require_once( __DIR__.'/../../../../wp-admin/includes/ajax-actions.php' );

global $wpdb;
global $usStates;
global $llCrew;
$llCrew = ['live'=>[2,3,4,5,8],
		   'alpha'=>[2,3,5,8,80],
		   'mobile'=>[2,3,4,5,8],
		   'local'=>[8]];

define( 'NOTIFY_PORTAL_AGENT_NEW', '<div style="text-align: left;"><p>Hi %first_name%,<br/>
&nbsp;&nbsp;This is a courtesy email to let you know that someone %what% fully signed in with their %capturedData%!  Be sure to follow up with your lead and find out why they are searching for property!  For more info click here: %home%/sellers/#stats<br/>
<strong>Name:</strong>%contact_name%
<strong>Email:</strong>%contact_email%
<strong>Phone:</strong>%contact_phone%
%campaign%
%extra%
Time:%timeStamp%<br/>
Good luck!<br/>
<span style="font-style: italic; color:lightgray;">The Lifestyled Team</span>
www.lifestyledlistings.com
831.508.8821</p></div>' 
);

define( 'PORTAL_USER_ENTER_SITE', '<div style="text-align: left;"><p>Hi %first_name%,<br/>
&nbsp;&nbsp;This is a courtesy email to let you know that someone has re-entered into your portal, directly into %page% page.   Now is a great time to follow up and see what has them so interested in looking for properties! To see the price range, search details, and recent activity, click here: %home%/sellers/#stats<br/>
<strong>Name:</strong>%contact_name%
<strong>Email:</strong>%contact_email%
<strong>Phone:</strong>%contact_phone%
%campaign%
Time:%timeStamp%<br/>
Good luck!<br/>
<span style="font-style: italic; color:lightgray;">The Lifestyled Team</span>
www.lifestyledlistings.com
831.508.8821<br/>
If you did not receive a text message also, please verify your mobile number and also contact us and tell us who your carrier is.</p></div>'
);

define( 'NOTIFY_EMAIL_CAPTURED', '<div style="text-align: left;"><p>Hi %first_name%,<br/>
&nbsp;&nbsp;This is a courtesy email to let you know that someone has entered their email in you portal. They may fill in their phone number and name as well, so stay tuned. Below is their contact info and the details of their search so you can follow up and find out what&quot;s motivating them to look for property! For more info click here: %home%/sellers/#stats<br/>
<strong>Name:</strong>%contact_name%
<strong>Email:</strong>%contact_email%
%campaign%
%extra%
Time:%timeStamp%<br/>
Good luck!<br/>
<span style="font-style: italic; color:lightgray;">The Lifestyled Team</span>
www.lifestyledlistings.com
831.508.8821<br/>
If you did not receive a text message also, please verify your mobile number and also contact us and tell us who your carrier is.</p></div>'
);

define( 'TELL_PORTAL_USER_THEIR_LOGIN', '<div style="text-align: left;"><p>Hi %first_name%,<br/>
&nbsp;&nbsp;This is a courtesy email to let you know that the email used %email% belongs to an existing user with login name: %login%.  Please use that login name and the password associated with it when you return to sign into our system.<br/>
&nbsp;&nbsp;If this is incorrect, please contact us immediately.<br/>
Thank you!<br/>
<span style="font-style: italic; color:lightgray;">The Lifestyled Team</span>
www.lifestyledlistings.com
831.508.8821</p></div>'
);


class PortalUsersMgr extends Controller {
	// comes from Controller now
	// protected $timezone_adjust = -7;

	const NEW_USER_FROM_LANDING = 1;
	const NEW_WP_USER        	= 2;
	const PORTAL_USER_ENTER_SITE= 3;
	const EMAIL_CAPTURED 		= 4;
	const NEW_WP_USER_WITH_PHONE = 5;
	const TELL_PORTAL_USER_THEIR_LOGIN = 6;
	const NOTIFICATION_INTERVAL = 3600;

	public function __construct(){
		parent::__construct();
		require_once(__DIR__.'/Options.class.php');
		$o = $this->getClass('Options');
		$x = $o->get((object)array('where'=>array('opt'=>'PortalUsersMgrClassDebugLevel')));
		$this->log_to_file = empty($x) ? 0 : intval($x[0]->value);
		$this->log = $this->log_to_file ? new Log(__DIR__.'/_logs/portalUsersMgr.log') : null;
		$this->debugLevel = !empty($x) ?  intval($x[0]->value) : 3;

		$this->agentAcronymMakeUppercase = ['crs','mba','gri','cdpe','qsc','mrp','abr','ii','(dr)','(e)','pa','iii','p.a.','a.b.','ab','cpa','csr','dpp','sres','rli','ltg','sfr','cips','rebac','crb','ccim','epro','bpor','sf','csp','sfr.','pc','cai','pro','srs','cne','tahs','rs','cdrs','asr','chms','mre','reos','csn','cbr','ahwd','trc','afh.','gri.','trlp','sps'];
		$this->agentAcronymDoubleCheck = ['reos','reo'];
	}

	public function __destruct() {
		if ($this->log !== null)
			$this->log->writeStringToFile();
	}

	protected function getTagString($tags) {
		if (empty($tags))
			return "none chosen";

		$tagStr = '';
		foreach($tags as $tag)
			$tagStr .= (empty($tagStr) ? '' : ', ').$tag->tag;

		return $tagStr;
	}

	protected function checkAgentRentryNotification($portalUser, $currentTime) {
		if (empty($portalUser->meta))
			return 1;

		foreach($portalUser->meta as $meta) {
			if ($meta->action == AGENT_REENTRY_NOTIFICATION) {
				if ( (strtotime($currentTime) - strtotime($meta->timeStamp)) < self::NOTIFICATION_INTERVAL) {
					$this->log("checkAgentRentryNotification - portalUser:$portalUser->id at $currentTime, last notification:$meta->timeStamp");
					return 0;
				}
				break;
			}
			unset($meta);
		}

		$this->log("checkAgentRentryNotification - portalUser:$portalUser->id ok to send notification");
		return 1;
	}

	protected function updateAgentRentryNotification($portalUser, $currentTime) {
		$metas = [];
		if (!empty($portalUser->meta)) foreach($portalUser->meta as $meta) {
			if ($meta->action != AGENT_REENTRY_NOTIFICATION) 
				$metas[] = $meta;
			unset($meta);
		}

		$meta = new \stdClass();
		$meta->action = AGENT_REENTRY_NOTIFICATION;
		$meta->timeStamp = $currentTime;
		$metas[] = $meta;

		$this->getClass('PortalUsers')->set([(object)['where'=>['id'=>$portalUser->id],
													  'fields'=>['meta'=>$metas]]]);
		$this->log("updateAgentRentryNotification - updated portalUser:$portalUser->id with $currentTime");
		unset($metas);
	}

	protected function getQuizData($portalUser, &$textExtra, &$extra) {
		if (!empty($portalUser->meta)) {
			foreach($portalUser->meta as $meta) {
				if ($meta->action == QUIZS_TAKEN &&
					!empty($meta->list)) {
					$meta->list = (array)$meta->list;
					$keys = array_keys($meta->list); // should only have one...
					$quiz_id = $keys[0];
					$quiz = $this->getClass('QuizActivity')->getDetails($quiz_id);
					if ($quiz->status == 'OK') {
						$location = 'unknown';
						if ( empty($quiz->data['location']) && empty($quiz->data['state']) )
							$location = "nationwide";
						elseif ( !empty($quiz->data['state']) ) 
							$location = "statewide:{$quiz->data['state']}";
						else 
							$location = 'localized:'.$quiz->data['location'];
									 
						$price = "from $".$quiz->data['price'][0].' to '.($quiz->data['price'][1] == -1 ? "MAX" : '$'.$quiz->data['price'][1]);
						$tags = $this->getTagString($quiz->data['tags']);
						$extra .= "A $location quiz with price $price was taken".($tags != 'none chosen' ? " with tags:$tags" : " to show all homes");
						$textExtra .= "A $location quiz taken";
					}
				}
				unset($meta);
			}
		}
	}

	public function sendEmail($portalUserID, $agentID, $ip, $thisPage, $why, $campaign = '', $sendTextMessage = true) {
		$portalUser = $this->getClass('PortalUsers')->get((object)['where'=>['id'=>$portalUserID]]);
		$timeStamp = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$location = $this->getCity($ip);
		$this->log("sendEmail entered with $why");
		if (!empty($portalUser)) {
			$portalUser = array_pop($portalUser);
			$portalUser->last_name = !empty($portalUser->last_name) ? $portalUser->last_name : '';
			
			$subject = "New Portal User";
			$blogname = "Full Lead Captured!";
			$blogdescription = "You can find more about this users activity from here: ".get_home_url()."/sellers/#stats";
			$textMessage = (!empty($portalUser->last_name) ? "$portalUser->first_name " : '').(!empty($portalUser->last_name) ? ' '.$portalUser->last_name.' ' : '')."with email:".(!empty($portalUser->email) ? $portalUser->email : 'N/A');
			$textMessage .= !empty($portalUser->phone) ? ", phone:".fixPhone($portalUser->phone) : '';
			$textMessage .= !empty($campaign) ? ", campaign:$campaign" : '';
			$extra = '';
			$textExtra = '';
			$capturedData = '';
			if (!empty($portalUser->email))
				$capturedData = "email";
			if (!empty($portalUser->phone))
				$capturedData .= (!empty($capturedData) ? ' and ' : '')."phone number";
			switch($why) {
				case self::NEW_USER_FROM_LANDING:
					$msg = NOTIFY_PORTAL_AGENT_NEW;
					$msg = str_replace('%what%', "new has", $msg);
					$msg = str_replace('%home%', get_home_url(), $msg);
					$msg = str_replace('%capturedData%', $capturedData, $msg);
					$textMessage = $textMessage." added to your list";
					$this->getQuizData($portalUser, $textExtra, $extra);
					$msg = str_replace('%extra%', $extra, $msg);
					$textMessage .= !empty($textExtra) ? ".  $textExtra" : ".";
					break;

				case self::NEW_WP_USER: 
				case self::NEW_WP_USER_WITH_PHONE:
					$msg = NOTIFY_PORTAL_AGENT_NEW;
					$msg = str_replace('%what%', "has signed", $msg);
					$msg = str_replace('%home%', get_home_url(), $msg);
					$msg = str_replace('%capturedData%', $capturedData, $msg);
					$textMessage = "New user ".$textMessage." added to your list";
					$blogdescription = "You now have full contact information for someone who is searching for properties, follow up!";
					if ($why == self::NEW_WP_USER_WITH_PHONE) {
						$subject = "Fully Qualified Portal User";
						$blogname = "Phone number acquired";
					}
					break;

				case self::EMAIL_CAPTURED: 
					$msg = NOTIFY_EMAIL_CAPTURED;
					$msg = str_replace('%what%', "has", $msg);
					$textMessage = "New user ".$textMessage." added to your list";
					$this->getQuizData($portalUser, $textExtra, $extra);
					$msg = str_replace('%extra%', $extra, $msg);
					$msg = str_replace('%home%', get_home_url(), $msg);
					$textMessage .= !empty($textExtra) ? ".  $textExtra" : ".";
					$subject = "Portal User Email Captured";
					$blogname = "New Email Captured!";
					$blogdescription = "You can find out more about this users activity here: ".get_home_url().'/sellers/#stats';
					break;


				case self::PORTAL_USER_ENTER_SITE:
					if (!$this->checkAgentRentryNotification($portalUser, $timeStamp)) {
						$this->log("sendEmail - $portalUser->first_name $portalUser->last_name has been here within the hour, not sending notifications to agent:$agentID");
						return;
					}

					$msg = PORTAL_USER_ENTER_SITE;
					$msg = str_replace('%page%', $thisPage, $msg);
					$msg = str_replace('%home%', get_home_url(), $msg);
					$subject = "Returning Portal User";
					$blogname = "Returning portal user";
					$blogdescription = "Time to follow up!";
					$textMessage = "Existing ".$textMessage." revisting your portal";
					break;

				case self::TELL_PORTAL_USER_THEIR_LOGIN:
					$msg = TELL_PORTAL_USER_THEIR_LOGIN;
					$msg = str_replace('%first_name%', $portalUser->first_name, $msg);
					$msg = str_replace('%email%', $portalUser->email, $msg);
					$msg = str_replace('%login%', $ip, $msg); // $ip is overloaded here..
					$subject = "Login name notification";
					$blogname = "Existing site user found with same email";
					break;
			}

			$contact_name = !empty($portalUser->first_name) || !empty($portalUser->last_name) ? "$portalUser->first_name $portalUser->last_name" : "N/A";
			$msg = str_replace('%contact_name%', $contact_name, $msg);
			$msg = str_replace('%contact_email%', !empty($portalUser->email) ? $portalUser->email : 'N/A', $msg);
			$msg = str_replace('%contact_phone%', !empty($portalUser->phone) ? fixPhone($portalUser->phone) : 'N/A', $msg);
			$msg = str_replace('%timeStamp%', $timeStamp."(PST)", $msg);
			$msg = str_replace('%campaign%', (empty($campaign) ? '' : '<strong>Campaign:</strong>'.$campaign), $msg);			

			$agent = $agentID ? $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$agentID]]) : null;
			if (!empty($agent)) {
				$agent = array_pop($agent);
				$agentMobile = $agent->mobile;
				$email = $agent->email;
				if (!empty($agent->meta)) foreach($agent->meta as $meta) {
					if ($meta->action == SELLER_PROFILE_DATA &&
						!empty($meta->contact_email) ) {
						$email = $meta->contact_email;
						$this->log("sendEmail is using contact_email:$email for agent:$agentID - $agent->first_name $agent->last_name");
					}
				}

				$msg = str_replace('%first_name%', $agent->first_name, $msg);

				$banner_orig = get_option('email_banner_img');
				$newBanner = get_template_directory_uri()."/_img/_banners/_email/portal_agent.png";
				// $newBanner = get_template_directory_uri()."/_img/_banners/_email/donkey.png";
				$this->log("sendEmail - replacing $banner_orig with $newBanner");

				// ob_start(function($buf) {
				// 	$this->log("sendMail from wpbe.php - $buf");
				// });

				$retval = update_option('email_banner_img', $newBanner);
				$this->log("sendEmail - update_option() was ".($retval ? 'success' : 'failure'));

				$retval = $this->getClass('Email')->sendLeadEmail("$portalUser->first_name $portalUser->last_name",
																 !empty($portalUser->email) ? $portalUser->email : 'N/A',
																 !empty($portalUser->phone) ? fixPhone($portalUser->phone,'-') : '000-000-0000',
																$email, $subject, $msg,
															 	$blogname, $blogdescription,
															 	$extra);
				$this->log("sendEmail - to $email was ".($retval ? 'success' : 'failure'));
				$retval = $this->getClass('Email')->sendLeadEmail("$portalUser->first_name $portalUser->last_name",
																 !empty($portalUser->email) ? $portalUser->email : 'N/A',
																 !empty($portalUser->phone) ? fixPhone($portalUser->phone,'-') : '000-000-0000',
																SUPPORT_EMAIL, $subject, $msg,
																"For agent:$agentID - $agent->first_name $agent->last_name - ".$blogname, 
																"Portal user:$portalUser->first_name $portalUser->last_name, with email:$portalUser->email",
																$extra);
				update_option('email_banner_img', $banner_orig);
				// ob_end_flush();
				// flush();

				$this->log("sendEmail - to ".SUPPORT_EMAIL." was ".($retval ? 'success' : 'failure'));

				$this->log("sendEmail - For agent:$agentID - $agent->first_name $agent->last_name at $email, mobile:$agentMobile, $msg");

				// if ($why == self::PORTAL_USER_ENTER_SITE)
				$this->updateAgentRentryNotification($portalUser, $timeStamp);

				if ($sendTextMessage &&
					!empty($agentMobile) &&
					($why != self::PORTAL_USER_ENTER_SITE ||
					 ($why == self::PORTAL_USER_ENTER_SITE &&
					  $agent->flags & SELLER_INFORM_PORTAL_USER_REENTRY))) {
					$this->log("sendMail - sending text message to $agentMobile, msg:$textMessage");
					$retval = $this->getClass('Email')->textMessage($agentMobile, $subject, $textMessage);
					$this->log("textMessage returned ".($retval ? "success" : "failure"));
				}
				elseif (empty($agentMobile))
					$this->log("sendEmail has empty mobile number for agent:$agentID");
			}
			elseif ($why == self::TELL_PORTAL_USER_THEIR_LOGIN) {
				$this->log("sendEmail - For portalUser:$portalUserID - $portalUser->first_name $portalUser->last_name at $portalUser->email, $msg");
				$this->getClass('Email')->sendMail($portalUser->email, "Thank you for registering", $msg,
													$blogname,
													$blogdescription);
				$this->getClass('Email')->sendMail(SUPPORT_EMAIL, "Existing WP user found", $msg,
													$blogname,
													$blogdescription);
			}
			else
				$this->log("sendEmail - failed to find seller with author_id:$agentID");
			
		}
		else
			$this->log("sendEmail could not find portalUserID:$portalUserID");
	}

	public function recordPortalUserListing($portalUserID, $listing_id) {
		$this->log("recordPortalUserListing - entered for portalUserID:$portalUserID, listing_id:$listing_id");
		$msg = '';
		$ip = $this->getClass("SessionMgr")->userIP();
		$updated = false;
		$portalUser = $this->getClass('PortalUsers')->get((object)['where'=>['id'=>$portalUserID]]);
		if (!empty($portalUser)) {
			$portalUser = array_pop($portalUser);
			$listingViewed = null;
			$metas = [];
			if (!empty($portalUser->meta)) foreach($portalUser->meta as $meta) {
				if ($meta->action == LISTINGS_VIEWED)
					$listingViewed = $meta;
				else
					$metas[] = $meta;
				unset($meta);
			}
			if (!$listingViewed) {
				$listingViewed = new \stdClass();
				$listingViewed->action = LISTINGS_VIEWED;
				$listingViewed->list = [];
			}

			$checkSummaryFlag = $this->updateVisitations($metas, $listingViewed, $listing_id, $ip, 'Listing');
			$metas[] = $listingViewed;
			$fields = ['meta'=>$metas];
			if (empty($portalUser->ip_orig))
				$fields['ip_orig'] = $portalUser->ip;
			if ($portalUser->ip != $ip)
				$fields['ip'] = $ip; 
			if ($checkSummaryFlag &&
				!($portalUser->flags & PORTAL_USER_NEED_SUMMARY_SENT))
				$fields['flags'] = $portalUser->flags | PORTAL_USER_NEED_SUMMARY_SENT;

			$updated = $this->getClass('PortalUsers')->set([(object)['where'=>['id'=>$portalUserID],
																  	 'fields'=>$fields]]);
			unset($fields, $metas, $listingViewed);
			$msg = "Updated portal user with id:$portalUserID with listing_id:$listing_id";
			
			$this->getCity($ip);
		}
		else
			return new Out('fail', "Could not find portal user with id:$portalUserID");
		
		$this->log($msg);
		return new Out('OK', $msg);
	}

	protected function updateSummary(&$metas, $summary, $date) {
		if (!$summary)
			return false;

		$dailySummary = null;
		foreach($metas as &$meta) {
			if ($meta->action == DAILY_SUMMARY) {
				$dailySummary = $meta;
				$dayData = null;
				foreach($meta->list as &$day) {
					if ($day->date == $date) {
						$dayData = $day;
						$day->list[] = $summary;
					}
					unset($day);
					if ($dayData)
						break;
				}
				if (!$dayData) {
					$day = (object)['date'=>$date,
									'list'=>[]];
					$day->list[] = $summary;
					$meta->list[] = $day;
				}
			}
			unset($meta);
			if ($dailySummary)
				break;
		}

		if (!$dailySummary) {
			$dailySummary = new \stdClass();
			$dailySummary->action = DAILY_SUMMARY;
			$dailySummary->list = [];
			$day = (object)['date'=>$date,
							'list'=>[]];
			$day->list[] = $summary;
			$dailySummary->list[] = $day;
			$metas[] = $dailySummary;
		}
		return true;
	}

	protected function updateVisitations(&$meta, &$visits, $visitId, $ip, $which, $fixedStamp = null) {
		$gotOne = false;
		// $needUpdate = false;
		$summary = null;
		$stamp = empty($fixedStamp) ? date("Y-m-d", time() + ($this->timezone_adjust*3600)) : $fixedStamp;
		$exactTime = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		// $visit == array of user IDs.  Each of those are arrays of IPs, Each IP is an array of dates
		$this->log("updateVisitations for $which - visit:$visitId, ip:$ip, stamp:$stamp");
		if (!empty($visits->list)) 
			foreach($visits->list as $id=>&$ips) {
				if (gettype($visitId) != 'string')
					$id = intval($id);
				if ($id === $visitId) {
					$this->log("updateVisitations for $which - found match:$visitId, id:$id", 2);
					foreach($ips as $addr=>&$theDates) {
						if ($addr == $ip) {		
							$this->log("updateVisitations for $which - found match:$ip", 2);				
							foreach($theDates as $date)
								if ($date->date == $stamp) {
									$this->log("updateVisitations for $which - found match:$stamp", 2);			
									$gotOne = true;
									$date->count++;
									// $needUpdate = true;
									$this->log("updateVisitations for $which - visit:$visitId, already have for $ip, date:$stamp, count is now $date->count");
									break;
								}
							if (!$gotOne) {
								$theDates[] = (object)[ 'date'=>$exactTime,
											  			'count'=>1 ];
								$this->log("updateVisitations for $which - visit:$visitId, adding for $ip, date:$stamp");
								$gotOne = true;
								// $needUpdate = true;
								$summary = (object)['action'=>$which,
													'what'=>$visitId,
													'date'=>$exactTime,
													'ip'=>$ip];
							}
						}
						unset($theDates);
					}
					if (!$gotOne) { // then same user, but different IP
						$ips = (array)$ips;
						$ips[$ip] = [(object)['date'=>$stamp,
											  'count'=>1]];
						$ips = (object)$ips;
						$this->log("updateVisitations for $which - visit:$visitId, adding new $ip, date:$stamp");
						$gotOne = true;
						// $needUpdate = true;
						$summary = (object)['action'=>$which,
											'what'=>$visitId,
											'date'=>$exactTime,
											'ip'=>$ip];
					}
					break;
				}
				unset($ips);
			}

		if (!$gotOne) { // fresh visit
			$users = (array)$visits->list;
			$users[$visitId] = (object)[$ip=>[(object)['date'=>$stamp,
											  		   'count'=>1]]];
			$visits->list = (object)$users;
			$this->log("updateVisitations for $which - new visit:$visitId, adding new $ip, date:$stamp");
			// $needUpdate = true;
			$summary = (object)['action'=>$which,
								'what'=>$visitId,
								'date'=>$exactTime,
								'ip'=>$ip];
		}
		if (isset($ips)) unset($ips);
		return $this->updateSummary($meta, $summary, $stamp);
	}

	public function recordPortalUserQuiz($portalUserID, $quiz_id) {
		$this->log("recordPortalUserQuiz - entered for portalUserID:$portalUserID, quiz_id:$quiz_id");
		$msg = '';
		$ip = $this->getClass("SessionMgr")->userIP();
		$portalUser = $this->getClass('PortalUsers')->get((object)['where'=>['id'=>$portalUserID]]);
		if (!empty($portalUser)) {
			$portalUser = array_pop($portalUser);
			$quizTaken = null;
			$metas = [];
			if (!empty($portalUser->meta)) foreach($portalUser->meta as $meta) {
				if ($meta->action == QUIZS_TAKEN)
					$quizTaken = $meta;
				else
					$metas[] = $meta;
				unset($meta);
			}
			if (!$quizTaken) {
				$quizTaken = new \stdClass();
				$quizTaken->action = QUIZS_TAKEN;
				$quizTaken->list = [];
			}

			$checkSummaryFlag = $this->updateVisitations($metas, $quizTaken, $quiz_id, $ip, 'Quiz');
			$metas[] = $quizTaken;
			$fields = ['meta'=>$metas];
			if (empty($portalUser->ip_orig))
				$fields['ip_orig'] = $portalUser->ip;
			if ($portalUser->ip != $ip)
				$fields['ip'] = $ip; 
			if ($checkSummaryFlag) {
				if (!($portalUser->flags & PORTAL_USER_NEED_SUMMARY_SENT))
					$portalUser->flags = $fields['flags'] = $portalUser->flags | PORTAL_USER_NEED_SUMMARY_SENT;
				if ($portalUser->flags & PORTAL_USER_VIEWED)
					$portalUser->flags = $fields['flags'] = $portalUser->flags & ~PORTAL_USER_VIEWED;
				if ($portalUser->flags & PORTAL_USER_HIDDEN)
					$portalUser->flags = $fields['flags'] = $portalUser->flags & ~PORTAL_USER_HIDDEN;
			}

			$updated = $this->getClass('PortalUsers')->set([(object)['where'=>['id'=>$portalUserID],
																  	 'fields'=>$fields]]);
			$msg = "Updated portal user with id:$portalUserID with quiz_id:$quiz_id";
			unset($fields, $metas, $quizTaken);
			
			$this->getCity($ip); // make sure this ip gets into the IpList table
		}
		else
			return new Out('fail', "Could not find portal user with id:$portalUserID");
		
		$this->log($msg);
		return new Out('OK', $msg);
	}

	protected function getCity($ip) {
		return $this->getClass('IpList')->getCity($ip);
		// $city = $this->getClass('IpList')->get((object)['where'=>['ip'=>$ip]]);
		// if (!empty($city)) {
		// 	$retval = ['city'=>$city[0]->city,
		// 			   'state'=>$city[0]->state];
		// 	unset($city);
		// 	return $retval;
		// }

		// $this->log("getCity got ip:$ip");
		// $call = "http://ipinfo.io/".$ip;
		// $result = @json_decode(file_get_contents($call));
		// $this->log("getCity got back:".(!empty($result) ? print_r($result, true) : "N/A"));
		// if( !empty($result) &&
		// 	isset($result->city) &&
		// 	isset($result->region) ) {
		// 	if ( isset($result->country) &&
		// 		 $result->country == 'US') {
		// 		global $usStates;
		// 		$state = '';
		// 		if ( array_key_exists($result->region, $usStates) )
		// 			$state = $usStates[$result->region];
		// 		elseif ( in_array($result->region, $usStates) )
		// 			$state = $result->region;

		// 		$city = $this->formatName($result->city, false, true);

		// 		$newIp = ['ip'=>$ip,
		// 				  'city'=>$city,
		// 				  'state'=>$state,
		// 				  'country'=>$result->country];
		// 		$this->getClass('IpList')->add($newIp);

		// 		return ['city'=>$city,
		// 				'state'=>$state];
		// 	}
		// }
		// return null;
	}

	protected function updateCity($ip, $portalUserID) {
		$this->log("updateCity got ip:$ip for portalUserID:$portalUserID");
		$cityResult = $this->getCity($ip);

		if (!empty($cityResult)) {
			$city = $cityResult['city'];
			$state = $cityResult['state'];

			if (!empty($state)) {
				$city = $this->getClass('Cities')->get((object)['where'=>['city'=>$city,
																		  'state'=>$state]]);
				$cityPortalUsers = null;
				$gotUser = false;
				$needUpdate = false;
				$metas = [];
				if (!empty($city)) {
					$city = array_pop($city);
					if ( !empty($city->meta) ) foreach($city->meta as $meta) {
						if ($meta->action == CITY_PORTAL_USERS) {
							$cityPortalUsers = $meta;
							foreach($meta->list as $user) {
								if ($user == $portalUserID) {
									$gotUser = true;
								}
								unset($user);
								if ($gotUser) break;
							}
						}
						else
							$metas[] = $meta;
						unset($meta);
					}
					if ( !$cityPortalUsers) {
						$cityPortalUsers = new \stdClass();
						$cityPortalUsers->action = CITY_PORTAL_USERS;
						$cityPortalUsers->list = [];
						$needUpdate = true;
					}
					if ( !$gotUser) {
						$cityPortalUsers->list[] = $portalUserID;
						$needUpdate = true;
					}
					$metas[] = $cityPortalUsers;
					if ($needUpdate) {
						$fields = [];
						$fields['meta'] = $metas;
						$fields['portal_user_count'] = count($cityPortalUsers->list);
						if ( ($city->flags & CITY_HAVE_PORTAL_USERS) == 0)
							$fields['flags'] = $city->flags |= CITY_HAVE_PORTAL_USERS;
						$this->getClass('Cities')->set([(object)['where'=>['id'=>$city->id],
																'fields'=>$fields]]);
						$this->log("updateCity for ip:$ip, updated $city->city, $city->state with portalUser:$portalUserID, now have a total portal users of: ".$fields['portal_user_count']);
						unset($fields);
					}
					unset($city, $metas);
				}
				else
					$this->log("updateCity for ip:$ip, failed to find matching city in db: $city, $state");
			}
			else
				$this->log("updateCity for ip:$ip, failed to find state for city:$city");
		}
	}

	public function addPortalUserFromLanding($name, $email, $phone, $agentID, $sessionID, 
											 $registeringUser = false, 
											 $thisPage = 'portal-landing', 
											 $fbId = '',
											 $photo = '',
											 $campaign = '') {
		$completingSignUpOfEmailOnlyCapturedUser = false;
		$this->log("addPortalUserFromLanding - entered for name:$name, email:$email, agentID:$agentID, sessionID:$sessionID");
		$ip = $this->getClass("SessionMgr")->userIP();
		$stamp = date("Y-m-d", time() + ($this->timezone_adjust*3600));
		$name = explode(' ', $name);
		$x = [];
		foreach($name as $part)
			if (!empty($part))
				$x[] = trim( ucfirst( strtolower($part) ));

		$first_name = '';
		$last_name = '';
		$emailOnlyCapturedUser = false;
		foreach($x as $part)
			if (empty($first_name)) $first_name = $part;
			else $last_name .= empty($last_name) ? $part : ' '.$part;

		$q = new \stdClass();
		$genericSearch = false;
		if (!empty($email))
			$q->where = ['email'=>$email];
		elseif (!empty($phone))
			$q->where = ['phone'=>$phone];
		elseif (!empty($fbId))
			$q->where = ['fb_id'=>$fbId];
		else {
			$q->where = ['ip'=>$ip];
			if (!empty($last_name))
				$q->or = ['elements'=>[['first_name'=>$first_name],
									   ['last_name'=>$last_name]]];
			else
				$q->where['first_name'] = $first_name;
			$genericSearch = true;
		}

		$activeSession = $this->getClass('Sessions')->get((object)['where'=>['id'=>$sessionID]]);
		$quiz_id = 0;
		if (!empty($activeSession))
			$quiz_id = $activeSession[0]->quiz_id;

		$user = $this->getClass('PortalUsers')->get($q);
		$currentPortalAgentAssociatedWithUser = 0;
		if (!empty($user) &&
			$genericSearch &&
			!empty($last_name)) {
			$found = false;
			foreach($user as $person)
				if ($person->last_name == $last_name)
					$found = $person;

			if ($found !== false)
				$user = [$found];
			else
				$user = [];
			$this->log("addPortalUserFromLanding - generic search with $name only.  User was ".(empty($user) ? "not found." : "found with id:$found->id" ));
		}
		$wpUser = null;
		if (empty($user)) {
			unset($q);
			$q = new \stdClass();
			$q->first_name = $first_name;
			if (!empty($last_name)) $q->last_name = $last_name;
			// if (!empty($email) || 
			// 	(isset($email) && $email == '') ) 
			// 	$q->email = $email;

			$q->email = !empty($email) ? $email : "none supplied";

			if (!empty($phone)) $q->phone = intval($phone);
			if (!empty($photo)) $q->photo = $photo;
			if (!empty($fbId)) $q->fb_id = $fbId;
			$q->ip = $ip;
			$q->ip_orig = $ip;
			$q->agent_id = $agentID;
			$q->session_id = $sessionID;
			if (!empty($campaign)) {
				$q->campaign_orig = $campaign;
				$q->campaign = $campaign;
			}

			$user = null;
			if ( !empty($email) &&
				($wpUser = get_user_by( 'email', $email )) )
				$q->wp_user_id = $wpUser->ID;

			$x = $this->getClass('PortalUsers')->add($q);
			if (!empty($x)) 
				$this->log( "Added new portal user: $first_name $last_name with $email at $x");
			else
				$this->log( "Failed to add portal user: $first_name $last_name with $email");
			
			$out = new Out(empty($x) ? 'fail' : 'OK', empty($x) ? ['msg'=>"Failed to add user with $email, probably already taken"] : 
																  ['msg'=>"Added new portal user at $x", 
																   'id'=>$x,
																   'first_name'=>$first_name,
																   'last_name'=>$last_name,
																   'email'=>$email,
																   'wp_user_id'=>!empty($wpUser) ? $wpUser->ID : 0,
																   'login'=>!empty($wpUser) ? $wpUser->user_login : '']);
		}
		else {
			$this->log("Portal user: $first_name $last_name with email:$email, phone:$phone exists with flags:".$user[0]->flags.", agent_id:".(!empty($user[0]->agent_id) ? $user[0]->agent_id : "N/A"), ", has PORTAL_USER_EMAIL_CAPTURED flag:".($user[0]->flags & PORTAL_USER_EMAIL_CAPTURED ? 'yes' : 'no'));
			$currentPortalAgentAssociatedWithUser = $user[0]->agent_id;
			if ( !empty($user[0]->email) )
				$wpUser = get_user_by( 'email', $user[0]->email );

			if ($user[0]->flags & PORTAL_USER_EMAIL_CAPTURED) { // then set the rest of missing data
				$completingSignUpOfEmailOnlyCapturedUser = true;
				$fields = [];
				if (empty($user[0]->first_name) ||
					$user[0]->first_name != $first_name) 
					$fields['first_name'] = $first_name;

				if (!empty($last_name) &&
					(empty($user[0]->last_name) ||
					 $user[0]->last_name != $last_name)) 
					$fields['last_name'] = $last_name;

				if (!empty($phone)) 
					$fields['phone'] = intval($phone);

				if (!empty($photo) &&
					(empty($user[0]->photo) ||
					 $user[0]->photo != $photo)) 
					$fields['photo'] = $photo;

				if (!empty($fbId) &&
					$user[0]->fb_id != $fbId) 
					$fields['fb_id'] = $fbId;

				if ($user[0]->agent_id != $agentID) 
					$fields['agent_id'] = $agentID;

				if ($user[0]->session_id != $sessionID) 
					$fields['session_id'] = $sessionID;

				if (!empty($wpUser)) 
					$fields['wp_user_id'] = $wpUser->ID;

				if (!empty($campaign)) {
					$fields['campaign'] = $campaign;
					if (empty($portalUser->campaign_orig))
						$fileds['campaign_orig'] = $campaign;
				}

				$emailOnlyCapturedUser = $user[0]->flags & PORTAL_USER_EMAIL_CAPTURED;
				$fields['flags'] = $user[0]->flags & ~PORTAL_USER_EMAIL_CAPTURED;

				$this->getClass('PortalUsers')->set([(object)['where'=>['id'=>$user[0]->id],
															  'fields'=>$fields]]);
				$user = $this->getClass('PortalUsers')->get($q); // refresh data
				$this->log("addPortalUserFromLanding - updated portalUser:".$user[0]->id." with fields:".print_r($fields, true).", now has flags:".$user[0]->flags);
				unset($fields);
			}
			else
				$this->log("addPortalUserFromLanding - updated portalUser:".$user[0]->id." does not have PORTAL_USER_EMAIL_CAPTURED flag");

			$out = new Out('OK', ['msg'=>($completingSignUpOfEmailOnlyCapturedUser ? "Completed sign up email captured user" : "Portal user already exists"), 
								  'id'=>$user[0]->id,
								  'first_name'=>$user[0]->first_name,
								  'last_name'=>$user[0]->last_name,
								  'email'=>$user[0]->email,
								  'wp_user_id'=>!empty($wpUser) ? $wpUser->ID : 0,
								  'login'=>!empty($wpUser) ? $wpUser->user_login : '']);
			$x = $user[0]->id;
		}

		// $x is now the portalUser id value
		if (!empty($x)) {
			if ($quiz_id)
				$this->recordPortalUserQuiz($x, $quiz_id);

			$this->updatePageVisitations($x, $thisPage, $ip, $stamp);
			$this->updateCampaignVisitations($x, $campaign);
		}
			
		if (!empty($wpUser) ) {
			$this->log("addPortalUserFromLanding - has found existing wpUser:$wpUser->ID");
			$this->loginAsWpUser($wpUser, $user[0], $agentID, $sessionID, $phone, $quiz_id, $out);
		}
		if ($registeringUser)
			$registeringUser = empty($wpUser); // if expecting to register user, but be have $user, then this person's email is already in the system

		if (!empty($x)) {
			if (!$registeringUser) {// otherwise, Register::basicRegistry() will call it when a new session is assigned to this portal user
				$this->log("addPortalUserFromLanding calling updateSession() for portalUserID:$x, agentID:$agentID, id:$sessionID, thisPage:$thisPage");
				$this->updateSession($x, $agentID, $sessionID);
			}

			$loggedInUser = wp_get_current_user();
			if (!$loggedInUser->ID ||
				 $loggedInUser->ID != $agentID)
				$this->sendEmail($x, $agentID, $ip, $thisPage, empty($user) || $emailOnlyCapturedUser ? self::NEW_USER_FROM_LANDING : self::PORTAL_USER_ENTER_SITE, $campaign);
			else
				$this->log("addPortalUserFromLanding - not calling sendEmail(), logged in user is:$loggedInUser->ID, agentID,$agentID");
			$this->updateCity($ip, $x);
			if (!$completingSignUpOfEmailOnlyCapturedUser)
				$this->updateSeller($agentID, $x);
		}
		return $out;
	}

	public function loginAsWpUser($wpUser, $portalUser, $agentID, $sessionID, $phone, $quiz_id, &$out, $fromEmailCaptureEvent = false) {
		$this->log("loginAsWpUser - wpUser:$wpUser->ID, portalUser:$portalUser->id, agentID:$agentID, sessionID:$sessionID");
		$magic = get_user_meta($wpUser->ID, 'user_key', true);
		$testPasswd = '';
		$i = 0;
		$loggedIn = false;
		if (!empty($magic)) {
			foreach($magic as $value) {
				$ch = chr( $value ^ ($i++ + ord('.')));
				$this->log("ch:$ch ");
				$testPasswd .= $ch;
			}
			$this->log("loginAsWpUser - logging in PortalUser:".$portalUser->id.", login:$wpUser->user_login");
			$retval = $this->getClass('Register')->loginUser($wpUser->user_login, $testPasswd);
			$session = null;
			$dummy = 0;
			$prevSessionID = $sessionID;
			$this->getClass('SessionMgr')->getCurrentSession($sessionID, $session);
			$this->log("loginAsWpUser - after logging:".($retval === true ? 'success' : 'failed')." PortalUser:".$portalUser->first_name.", email:".$portalUser->email.", pre-sessionID:$prevSessionID, post-sessionID:$sessionID");
			if ($retval === true) { // logged in
				if ($quiz_id)
					$out->data['quiz_id'] = $quiz_id;
				if ($prevSessionID != $sessionID &&
					$session &&
					$quiz_id != $session->quiz_id) {
					// reset to whatever quiz_id the incoming current active session was
					$this->log("loginAsWpUser - incoming active sessionID:$prevSessionID with quiz_id:$quiz_id, after logging in, sessionID is now:$sessionID with quiz_id:$session->quiz_id, so that will be updated to $quiz_id");
					$this->getClass('Sessions')->set([(object)['where'=>['id'=>$sessionID],
															   'fields'=>['quiz_id'=>$quiz_id]]]); 
				}
				if (!empty($phone) &&
					$testPasswd != $phone &&
					!$fromEmailCaptureEvent) {
					// remind portal user they need to use this testPasswd and login to get back into the system
					$this->sendEmail($portalUser->id, 0, $wpUser->user_login, '', self::TELL_PORTAL_USER_THEIR_LOGIN);
				}

				$loggedIn = true;
			}
		}
		else {
			$this->log("loginAsWpUser - failed to find user_key, setting requireLogin to 1");
			$out->data['requireLogin'] = 1;
			$this->updateSession($portalUser->id, $agentID, $sessionID);
		}
		return $loggedIn;
	}

	public function updateSeller($agentID, $portalUserID) {
		$portalUser = $this->getClass('PortalUsers')->get((object)['where'=>['id'=>$portalUserID]]);
		$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$agentID]]);
		$ip = $this->getClass("SessionMgr")->userIP();

		if (!empty($seller)) {
			$portalUser = array_pop($portalUser);
			// update the seller meta data with userId and IP and date
			$meta = array();
			$visits = null;
			if (!empty($seller[0]->meta)) foreach($seller[0]->meta as $data) {
				if ($data->action != SELLER_VISITATIONS)
					$meta[] = $data;
				else {
					$visits = $data;
					// $visits->users = json_decode($visits->users, true);
				}
			}
			$newVisit = false;
			$needUpdate = false;

			if (!$visits) {
				$visits = new \stdClass();
				$visits->action = SELLER_VISITATIONS;
				$visits->users = array();
				$visits->portalUsers = array();
				$newVisit = true;
			}
			elseif (!isset($visits->portalUsers))
				$visits->portalUsers = array();

			if (!empty($portalUser->wp_user_id))
				$needUpdate = $this->getClass('SessionMgr')->portVisitations($meta, $visits, $portalUser->wp_user_id, $portalUserID, $agentID);

			$needUpdate = $this->getClass('SessionMgr')->updatePortalUserVisitations($meta, $visits, $portalUserID, $ip) | $needUpdate;
			if ($needUpdate) {
				// $visits->users = json_encode($visits->users);
				$meta[] = $visits;
				$updated = $this->getClass('Sellers')->set([(object)['fields'=>['meta'=>$meta,
																	  			'portal_visits'=>($seller[0]->portal_visits+1)],
																	  'where'=>['author_id'=>$agentID]]]);
				$this->log("updateSeller - updating SELLER_VISITATIONS for agent:$agentID, on portal-landing, for $portalUserID was ".(empty($updated) ? "failure" : "success"));
			}
		}
	}

	public function updateExistingPortalUserCampaign(&$portalUser, $campaign, &$fields) {
		if (!empty($campaign)) {
			$gotOne = false;
			$gotCampaign = false;
			if (!isset($portalUser->meta) ||
				$portalUser->meta == null)
				$portalUser->meta = []; // setup blank array

			foreach($portalUser->meta as &$meta) {
				if ($meta->action == AGENT_CAMPAIGNS) {
					$gotOne = true;
					foreach($meta->list as &$item) {
						if ($item->campaign == $campaign) {
							$gotCampaign = true;
							$item->count++;
						}
						unset($item);
					}
					if (!$gotCampaign) 
						$meta->list[] = (object)['campaign'=>$campaign,
										   		 'count'=>1];
				}
				unset($meta);
			}
			if (!$gotOne) {
				$agentCampaigns = new \stdClass();
				$agentCampaigns->action = AGENT_CAMPAIGNS;
				$agentCampaigns->list = [];
				$agentCampaigns->list[] = (object)['campaign'=>$campaign,
												   'count'=>1];
				$portalUser->meta[] = $agentCampaigns;
			}
			if (!$gotOne ||
				!$gotCampaign)
				$fields['meta'] = $portalUser->meta;
		}
	}

	protected function byPassLLCrew($wpUserID) {
		global $llCrew;
		$wpUserID = intval($wpUserID);
		$homeURL = get_home_url();
		// $this->log("byPassLLCrew for $wpUserID, home:$homeURL, in_array:".in_array($wpUserID, $llCrew['local']).", array:".print_r($llCrew, true));
		if (strpos($homeURL, 'alpha') !== false && in_array($wpUserID, $llCrew['alpha']))
			return 1;
		if (strpos($homeURL, 'mobile') !== false && in_array($wpUserID, $llCrew['mobile']))
			return 1;
		if (strpos($homeURL, 'localhost') !== false && in_array($wpUserID, $llCrew['local']))
			return 1;
		if (in_array($wpUserID, $llCrew['live']))
			return 1;

		return 0;
	}

	public function addPortalUserFromWP($sessionID, $wpUserID, $thisPage, $agentID = 0, $campaign = '', $isPhoneNumber = false, $phone = '') {
		$this->log("addPortalUserFromWP - entered for wpUserID:$wpUserID, agentID:$agentID, sessionID:$sessionID, thisPage:$thisPage, campaign:$campaign");
		if ($this->byPassLLCrew($wpUserID)) {
			$this->log("addPortalUserFromWP is skipping LL Crew member: $wpUserID");
			return 0;
		}
		$thisPage = empty($thisPage) ? 'home' : $thisPage;
		$first_name = '';
		$last_name = '';
		$email = '';
		if (empty($agentID)) // not supplied
			$agentID = isset($_COOKIE['PROFILE_AGENT_ID']) && !empty($_COOKIE['PROFILE_AGENT_ID']) ? $_COOKIE['PROFILE_AGENT_ID'] :
					   (isset($_SESSION['PROFILE_AGENT_ID']) && !empty($_SESSION['PROFILE_AGENT_ID']) ? $_SESSION['PROFILE_AGENT_ID'] : 0);	

		if (empty($agentID)) {
			$this->log("addPortalUserFromWP - failed to find agentID from COOKIE nor SESSION for $wpUserID");
			return 0;
		}	
		$ip = $this->getClass('SessionMgr')->userIP();
		$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$wpUserID]]);
		if (!empty($seller)) {
			$first_name = $seller[0]->first_name;
			$last_name = $seller[0]->last_name;
			$email = $seller[0]->email;
			if (!empty($seller[0]->meta)) foreach($seller[0]->meta as $meta) {
				if ($meta->action == SELLER_PROFILE_DATA &&
					!empty($meta->contact_email) ) {
					$email = $meta->contact_email;
					$this->log("addPortalUserFromWP is using contact_email:$email for WPUser:$wpUserID - $first_name $last_name");
					break;
				}
			}
		}
		else {
			$user_info = get_userdata($wpUserID);
			$first_name = $user_info->first_name;
			$last_name = $user_info->last_name;
			$email = $user_info->user_email;
		}

		$stamp = date("Y-m-d", time() + ($this->timezone_adjust*3600));
		$fields = ['first_name'=>$first_name,
				   'last_name'=>$last_name,
				   'email'=>$email,
				   'ip'=>$ip,
				   'ip_orig'=>$ip,
				   'campaign'=>$campaign,
				   'campaign_orig'=>$campaign,
				   'session_id'=>$sessionID,
				   'wp_user_id'=>$wpUserID,
				   'agent_id'=>$agentID
				  ];

		if ($isPhoneNumber &&
			!empty($phone))
			$fields['phone'] = intval($phone);

		// sanity check, make sure we don't have already
		$portalUser = $this->getClass('PortalUsers')->get((object)['where'=>['email'=>$email]]);
		if (empty($portalUser)) {
			// then see if this new register is from a portal user that had no email supplied
			$q = new \stdClass();
			$q->where = ['ip'=>$ip];
			$q->or = ['elements'=>[['first_name'=>$first_name],
								   ['last_name'=>$last_name]]];
			$user = $this->getClass('PortalUsers')->get($q);
			if (!empty($user)) {
				$found = false;
				foreach($user as $person)
					if ($person->last_name == $last_name)
						$found = $person;

				if ($found !== false)
					$portalUser = [$found];

				$this->log("addPortalUserFromWP - generic search with $first_name $last_name only.  User was ".(empty($portalUser) ? "not found." : "found with id:$found->id" ));
			}
			unset($q);
		}

		if (empty($portalUser)) {
			$x = $this->getClass('PortalUsers')->add($fields);
			$this->log("Added new portal user: $first_name $last_name with $email at $x from existing WP user:$wpUserID");
			$this->updateSession($x, $agentID, $sessionID);
			$this->sendEmail($x, $agentID, $ip, $thisPage, self::NEW_WP_USER, $campaign);
			$this->updateCity($ip, $x);
			$this->updatePageVisitations($x, $thisPage, $ip, $stamp);
			$this->updateCampaignVisitations($x, $campaign);
			return $x;
		}
		else {
			$portalUser = array_pop($portalUser);
			$this->log("addPortalUserFromWP found existing portalUser:$portalUser->id:$first_name $last_name with $email");
			$fields = [];
			if ($portalUser->first_name != $first_name)
				$fields['first_name'] = $first_name;
			if ($portalUser->last_name != $last_name)
				$fields['last_name'] = $last_name;
			if ($portalUser->email != $email)
				$fields['email'] = $email;
			if (!empty($campaign)) {
				$fields['campaign'] = $campaign;
				if (empty($portalUser->campaign_orig))
					$fileds['campaign_orig'] = $campaign;
			}
			if ($isPhoneNumber &&
				!empty($phone) &&
				$portalUser->phone != intval($phone))
				$fields['phone'] = intval($phone);
			if (!$portalUser->wp_user_id) {
				$fields['wp_user_id'] = $wpUserID;
				$this->log("addPortalUserFromWP - setting empty wp_user_id of portalUser:$portalUser->id to $wpUserID");
			}

			// $this->updateExistingPortalUserCampaign($portalUser, $campaign, $fields);

			if (count($fields)) {
				$this->getClass('PortalUsers')->set([(object)['where'=>['id'=>$portalUser->id],
															  'fields'=>$fields]]);
				$this->log("addPortalUserFromWP updated portalUser:$portalUser->id with fields:".print_r($fields, true));
			}
			$this->updatePageVisitations($portalUser->id, $thisPage, $ip, $stamp);
			$this->updateCampaignVisitations($portalUser->id, $campaign);
			$this->updateSession($portalUser->id, $agentID, $sessionID);
			$this->updateCity($ip, $portalUser->id);
			if ($portalUser->agent_id != $agentID)
				$this->sendEmail($portalUser->id, $agentID, $ip, $thisPage, ($isPhoneNumber && !empty($phone) ? self::NEW_WP_USER_WITH_PHONE : self::NEW_WP_USER), $campaign);
			elseif ($portalUser->wp_user_id != $agentID) {
				$this->sendEmail($portalUser->id, $agentID, $ip, $thisPage, self::PORTAL_USER_ENTER_SITE, $campaign);
				$this->log("addPortalUserFromWP - portalUser:$portalUser->id already has $agentID as agent_id");
			}
			else
				$this->log("addPortalUserFromWP - portalUser:$portalUser->id is the same person as the agent:$agentID");
			return $portalUser->id;
		}
	}

	public function updateSession($portalUserID, $agentID, $sessionID) {
		$this->log("updateSession - entered for portalUserID:$portalUserID, agentID:$agentID, sessionID:$sessionID");
		$updateSession = false;
		$updated = false;
		$oldPortalUser = 0;
		$oldAgentID = 0;
		$session = $this->getClass('Sessions')->get((object)['where'=>['id'=>$sessionID]]);

		// update session 
		if (!empty($session)) {
			$data = isset($session[0]->value) && $session[0]->value ? (array)$session[0]->value : [];
			$haveMeta = false;
			$haveAgentMeta = false;
			if (!empty($data)) 
				foreach($data as $i=>&$info) {
					if ($info->type == SESSION_PORTAL_USER) {
						if (intval($info->portalUser) != $portalUserID) {
							$oldPortalUser = $info->portalUser;
							$info->portalUser = $portalUserID;
							$updateSession = true;
						}
						$haveMeta = true;
					}
					elseif ($info->type == SESSION_PREMIER_AGENT) {
						if (intval($info->agentID != $agentID)) {
							$oldAgentID = $info->agentID;
							$info->agentID = $agentID;
							$userData = get_userdata($agentID);
							if (!empty($userData))
								$info->nickname =  $userData->nickname;
							else
							 	throw new \Exception("Failed to find nickname for agentID:$agentID");
							$updateSession = true;
						}
						$haveAgentMeta = true;
					}
					unset($info);
				}

			if (!$haveMeta) {
				$meta = new \stdClass();
				$meta->type = SESSION_PORTAL_USER;
				$meta->portalUser = $portalUserID;
				$data[] = $meta;
				$updateSession = true;
				$this->log("updateSession - adding SESSION_PORTAL_USER to sessionID:$sessionID, with portalUserID:$portalUserID");
			}
			if (!$haveAgentMeta) {
				$meta = new \stdClass();
				$meta->type = SESSION_PREMIER_AGENT;
				$meta->agentID = $agentID;
				$userData = get_userdata($agentID);
				if (!empty($userData))
					$meta->nickname = $userData->nickname;
				else
					$meta->nickname = '';
				$data[] = $meta;
				$updateSession = true;
				$this->log("updateSession - adding SESSION_PORTAL_USER to sessionID:$sessionID, with agentID:$agentID");
			}

			if ($updateSession) {
				$updated = $this->getClass('Sessions')->set([(object)['where'=>['id'=>$sessionID],
														   			  'fields'=>['value'=>$data]]]);
				$this->log("updateSession - updating SESSION_PORTAL_USER for sessionID:$sessionID, from oldPortalUser:$oldPortalUser to portalUserID:$portalUserID, oldAgentID:$oldAgentID to agentID:$agentID was ".(empty($updated) ? "failure" : "success"));
			}
			// elseif ($haveMeta)
			// 	$this->log("updateSession - no need to update SESSION_PORTAL_USER for sessionID:$sessionID");
			// else {
			// 	$meta = new \stdClass();
			// 	$meta->type = SESSION_PORTAL_USER;
			// 	$meta->portalUser = $portalUserID;
			// 	$data[] = $meta;
			// 	$updated = $this->getClass('Sessions')->set([(object)['where'=>['id'=>$sessionID],
			// 											   			  'fields'=>['value'=>$data]]]);
			// 	$this->log("updateSession - adding SESSION_PORTAL_USER to sessionID:$sessionID, with $portalUserID was ".(empty($updated) ? "failure" : "success"));
			// }
		}
		else
			$this->log("updateSession - could not locate session with ID:$sessionID");

		return $updated;
	}


	public function recordPortalUserEntry($portalUserID, $agentID, $sessionID, $page, $campaign = '') {
		$this->log("recordPortalUserEntry - entered for portalUserID:$portalUserID, agentID:$agentID, sessionID:$sessionID, page:$page");
		$msg = '';
		$updateSession = false;
		$haveMeta = false;
		$updated = $this->updateSession($portalUserID, $agentID, $sessionID);
		
		// now do the seller's side
		$ip = $this->getClass("SessionMgr")->userIP();
		$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$agentID]]);
		if (!empty($seller)) {
			// update the seller meta data with userId and IP and date
			$meta = array();
			$visits = null;
			if (!empty($seller[0]->meta)) foreach($seller[0]->meta as $data) {
				if ($data->action != SELLER_VISITATIONS)
					$meta[] = $data;
				else {
					$visits = $data;
					// $visits->users = json_decode($visits->users, true);
				}
			}
			$newVisit = false;
			if (!$visits) {
				$visits = new \stdClass();
				$visits->action = SELLER_VISITATIONS;
				$visits->users = array();
				$visits->portalUsers = array();
				$newVisit = true;
			}
			elseif (!isset($visits->portalUsers))
				$visits->portalUsers = array();

			$needUpdate = $this->getClass('SessionMgr')->updatePortalUserVisitations($meta, $visits, $portalUserID, $ip);
			if ($needUpdate) {
				// $visits->users = json_encode($visits->users);
				$meta[] = $visits;
				$updated = $this->getClass('Sellers')->set([(object)['fields'=>['meta'=>$meta,
																	  			'portal_visits'=>($seller[0]->portal_visits+1)],
																	  'where'=>['author_id'=>$agentID]]]);
				$this->log("recordPortalUserEntry - updating SELLER_VISITATIONS for agent:$agentID, on page:$page, for $portalUserID was ".(empty($updated) ? "failure" : "success"));
			}

			$portalUser = $this->getClass('PortalUsers')->get((object)['where'=>['id'=>$portalUserID]]);
			if (empty($portalUser))
				$this->log("recordPortalUserEntry - failed to find PortalUser:$portalUserID!!!");
			else {
				$portalUser = array_pop($portalUser);
				if ($portalUser->ip != $ip) {
					$fields = [];
					if (empty($portalUser->ip_orig))
						$fields['ip_orig'] = $portalUser->ip;
					$fields = ['ip'=>$ip];

					$this->updateExistingPortalUserCampaign($portalUser, $campaign, $fields);

					$this->log("recordPortalUserEntry - updating PortalUser:$portalUserID ip from:$portalUser->ip to $ip");
					$this->getClass('PortalUsers')->set([(object)['where'=>['id'=>$portalUserID],
																  'fields'=>$fields]]);
					unset($fields);
				}
			}
		}	
		else
			$this->log("recordPortalUserEntry - could not locate agent with ID:$agentID");	


		// now update the PortalUser
		if (!$this->updatePageVisitations($portalUserID, $page, $ip))
			return new Out('fail', "recordPortalUserEntry - Could not find portal user with id:$portalUserID");

		$this->sendEmail($portalUserID, $agentID, $ip, $page, self::PORTAL_USER_ENTER_SITE, $campaign);

		$msg = $updated ? "Successfully updated for portalUser:$portalUserID, agentID:$agentID" : "Nothing to update for portalUser:$portalUserID, agentID:$agentID";
		$this->log("recordPortalUserEntry - $msg");
		$this->updateCity($ip, $portalUserID);

		$out = new Out('OK',$msg);
		return $out;
	}

	public function updatePageVisitations($portalUserID, $page, $ip, $date = null) {
		$this->log("updatePageVisitations - entered for portalUserID:$portalUserID, page:$page, ip:$ip, date:".($date ? $date : "N/A"));
		// now update the PortalUser
		$updated = false;
		$portalUser = $this->getClass('PortalUsers')->get((object)['where'=>['id'=>$portalUserID]]);
		if (!empty($portalUser)) {
			$portalUser = array_pop($portalUser);
			$siteEntries = null;
			$metas = [];
			if (!empty($portalUser->meta)) foreach($portalUser->meta as $meta) {
				if ($meta->action == SITE_ENTRIES)
					$siteEntries = $meta;
				else
					$metas[] = $meta;
			}
			if (!$siteEntries) {
				$siteEntries = new \stdClass();
				$siteEntries->action = SITE_ENTRIES;
				$siteEntries->list = [];
			}

			$checkSummaryFlag = $this->updateVisitations($metas, $siteEntries, $page, $ip, 'Page', $date);
			$metas[] = $siteEntries;
			$fields = ['meta'=>$metas];
			if (empty($portalUser->ip_orig))
				$fields['ip_orig'] = $portalUser->ip;
			if ($portalUser->ip != $ip)
				$fields['ip'] = $ip; 
			if ($checkSummaryFlag &&
				!($portalUser->flags & PORTAL_USER_NEED_SUMMARY_SENT))
				$fields['flags'] = $portalUser->flags | PORTAL_USER_NEED_SUMMARY_SENT;

			$updated = $this->getClass('PortalUsers')->set([(object)['where'=>['id'=>$portalUserID],
																  	 'fields'=>$fields]]);
			unset($fields, $metas, $siteEntries);

			// NOTE: not calling getCity($ip) since updateCity() is called from updatePageVisitations()'s caller
		}
		return $updated;
	}

	public function updateCampaignVisitations($portalUserID, $campaign) {
		$this->log("updateCampaignVisitations - entered for portalUserID:$portalUserID, campaign:$campaign");
		// now update the PortalUser
		if (empty($campaign))
			return false;

		$updated = false;
		$portalUser = $this->getClass('PortalUsers')->get((object)['where'=>['id'=>$portalUserID]]);
		if (!empty($portalUser)) {
			$portalUser = array_pop($portalUser);
			$agentCampaigns = null;
			$metas = [];
			if (!empty($portalUser->meta)) foreach($portalUser->meta as $meta) {
				if ($meta->action == AGENT_CAMPAIGNS)
					$agentCampaigns = $meta;
				else
					$metas[] = $meta;
			}
			if (!$agentCampaigns) {
				$agentCampaigns = new \stdClass();
				$agentCampaigns->action = AGENT_CAMPAIGNS;
				$agentCampaigns->list = [];
			}

			$found = false;
			foreach($agentCampaigns->list as &$item) {
				if ($item->campaign == $campaign) {
					$found = true;
					$item->count++;
				}
				unset($item);
			}
			if (!$found)
				$agentCampaigns->list[] = (object)['campaign'=>$campaign,
												   'count'=>1];

			$metas[] = $agentCampaigns;
			$fields = ['meta'=>$metas];
			
			$updated = $this->getClass('PortalUsers')->set([(object)['where'=>['id'=>$portalUserID],
																  	 'fields'=>$fields]]);
			unset($fields, $metas, $siteEntries);
		}
		return $updated;
	}
}
