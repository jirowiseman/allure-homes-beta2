<?php
namespace AH;

// Load WP components, no themes
if ( ! defined( 'WP_USE_THEMES' ) )
	define('WP_USE_THEMES', false);
// define('WP_ADMIN', true);2

require_once(__DIR__.'/_Controller.class.php');
require_once(__DIR__.'/Utility.class.php');
require_once(__DIR__.'/IPv6.php');



require_once(__DIR__.'/../../../../wp-load.php');
require_once(__DIR__.'/../../../../wp-includes/pluggable.php');

require_once(__DIR__.'/../../../../wp-admin/includes/admin.php' );
	
/** Load Ajax Handlers for WordPress Core */
require_once( __DIR__.'/../../../../wp-admin/includes/ajax-actions.php' );
require_once( __DIR__.'/../src/Mailchimp.php');


define( 'chimpAPI', '2498e90280dac0d887608426112d5676-us10');
define( 'CHIMP_LIMIT', 25);

function set_html_content_type() {

	return 'text/html';
}



class Chimp extends Controller {
	// comes from Controller now
	// protected $timezone_adjust = 7;
	protected $chimpCampaigns = [
		["key"=>"7-7-M","cid"=>"67e0c47b82","name"=>"Batch 1 (7/7)","type"=>8],
		["key"=>"7-8-M","cid"=>"6679381340","name"=>"New Mass Test","type"=>8],
		["key"=>"7-8-A","cid"=>"37dfe403dc","name"=>"Evening Send 7/8","type"=>8],
		["key"=>"7-8-E","cid"=>"301bc70bf3","name"=>"Evening Send 7/8 (Copy 01)","type"=>8],
		["key"=>"7-9-A","cid"=>"7573b76f66","name"=>"7-9 12=>36PM","type"=>8],
		["key"=>"7-10-M","cid"=>"3868d0bc86","name"=>"7/10 6AM","type"=>8],
		["key"=>"7-13-M","cid"=>"dcd55cbdf2","name"=>"Sentout 7/13AM","type"=>8],
		["key"=>"7-13-M-2-1","cid"=>"812081bb14","name"=>"Followup to batch 7/7","type"=>16],
		["key"=>"7-13-M-2-2","cid"=>"babeef62aa","name"=>"Followup to batch 7/7 (Copy 01)","type"=>16],
		["key"=>"7-14-M","cid"=>"1d43f87bd8","name"=>"Followup to New Mass Test", "type"=>16],
		["key"=>"7-14-A","cid"=>"cf512c1d5b","name"=>"Phase 2 Batch 1", "type"=>16],
		["key"=>"7-15-M","cid"=>"ade0645540","name"=>"FollowupA to Evening Send 7/8", "type"=>16],
		["key"=>"7-15-M-2","cid"=>"88db0b6d40","name"=>"FollowupB to Evening Send 7/9", "type"=>16]
	];

	public function __construct($logIt = 0){
		parent::__construct();
		set_time_limit(0);
		require_once(__DIR__.'/Options.class.php');
		$o = $this->getClass('Options');
		$x = $o->get((object)array('where'=>array('opt'=>'ChimpClassDebugLevel')));
		$this->log_to_file = empty($logIt) ? (empty($x) ? 0 : intval($x[0]->value)) : $logIt;
		$this->log = $this->log_to_file ? new Log(__DIR__.'/_logs/chimp.log') : null;
	
		$this->chimp = new \MailChimp(chimpAPI);

		$x = $o->get((object)array('where'=>array('opt'=>'ChimpCampaigns')));
		if (!empty($x))
			$this->chimpCampaigns = json_decode($x[0]->value);
	}

	public function __destruct() {
		if ($this->log !== null)
			$this->log->writeStringToFile();
	}
	
	public function sendMail($email, $subject, $msg, 
								$blogname = 'Welcome!', 
								$blogdescription = "You're now part of the best lifestyled listings revolution!") {
		$user = wp_get_current_user();

		update_option( 'blogurl', get_home_url());
		update_option( 'blogname', $blogname);
		update_option( 'blogdescription', $blogdescription);
		update_option( 'admin_email', SUPPORT_EMAIL); 

		$headers = ['From: Lifestyled Listings <'.SUPPORT_EMAIL.'>' . "\r\n"];
	  	$success = wp_mail( $email, $subject, $msg, $headers); 

	  	update_option( 'blogname', 'Welcome!');
	  	update_option( 'blogdescription', 'The best lifestyled listing site!');
	  	return $success;
	}


	protected function getSellerEmailDb($seller) {
		$EmailDb = $this->getClass("SellersEmailDb");
		if ( !$EmailDb->exists(['seller_id'=>$seller->id]) &&
			 !empty($seller->email) ) {
			$email = explode('@', $seller->email);
			if (count($email) == 2) {
				$db = (object)['seller_id'=>$seller->id,
								'name'=>$email[0],
								'host'=>strtolower($email[1])];
				$msg = "getSellerEmailDb - added sellerId: {$seller->id}, email:{$seller->email}";
				$this->log($msg);
				$id = $EmailDb->add($db);
				unset($db);
				return $EmailDb->get((object)['where'=>['id'=>$id]])[0];
			}
			else
				return false;
		}
		else if (!empty($seller->email))
			return $EmailDb->get((object)['where'=>['seller_id'=>$seller->id]])[0];
		else
			return false;
	}

	protected function checkSellerEmailDb($seller) {
		$EmailDb = $this->getClass("SellersEmailDb");
		if ( !$EmailDb->exists(['seller_id'=>$seller->id]) &&
			 !empty($seller->email) ) {
			$email = explode('@', $seller->email);
			if (count($email) == 2) {
				$db = (object)['seller_id'=>$seller->id,
								'name'=>$email[0],
								'host'=>strtolower($email[1])];
				$msg = "checkSellerEmailDb - added sellerId: {$seller->id}, email:{$seller->email}";
				$this->log($msg);
				$EmailDb->add($db);
				unset($db);
			}
		}
	}

	protected function setEmailDb($emailDb, &$flag, &$action, $today, $trackerId, $restrict = false, $type = 'LLC Internal', $name = '', $checkFlag = true) {
		$needMeta = true;
		if ($flag != EMAIL_UNSUBSCRIBED &&
			$flag != RESPONDED_TO_IMPROVE_LISITNG &&
			$flag == RESPONDED_TO_FIRST &&
			$checkFlag) { // else leave it alone!!!
			// now try to adjust to correct flag, since same email tracker could have been used by different campaigns
			$checkForFlags = [PROMO_FOURTH, PROMO_THIRD, PROMO_SECOND, PROMO_FIRST, PORTAL_PROMO_1, PORTAL_TOOLS_PKG, PORTAL_PROMO_CONTENT_TEST, PORTAL_PROMO_FOLLOW_UP];
			foreach($checkForFlags as $sentFlag) {
				$gotTestSend = $emailDb->flags & $sentFlag; // which send out was it?
				if (!$gotTestSend)
					continue;
				break;
			}

			$sentFlag = $gotTestSend; // could be 0
			switch ($sentFlag) {
				case 0: // if database hasn't been updated with the recent chimp campaigns, this will be 0, or it's an IMPROVE_LISTING type email
					if ( ($emailDb->flags & IMPROVE_LISTING) == 0) { // then really a chimp campaign!
						$action = RESPONSE_TO_EMAIL;
						$flag = RESPONDED_TO_EMAIL;
					}
					else {
						$action = IMPROVE_LISTING_RESPONSE; 
						$flag = RESPONDED_TO_IMPROVE_LISITNG;
					}
					break;
				case PROMO_FIRST: 
					$action = RESPONSE_FIRST_EMAIL; 
					$flag = RESPONDED_TO_FIRST;
					break;
				case PROMO_SECOND:
					$action = RESPONSE_SECOND_EMAIL; 
					$flag = RESPONDED_TO_SECOND;
					break;
				case PROMO_THIRD: 
					$action = RESPONSE_THIRD_EMAIL; 
					$flag = RESPONDED_TO_THIRD;
					break;
				case PROMO_FOURTH: 
					$action = RESPONSE_FOURTH_EMAIL; 
					$flag = RESPONDED_TO_FOURTH;
					break;
				case PORTAL_PROMO_1: 
					$action = RESPONSE_PORTAL_PROMO; 
					$flag = RESPONDED_TO_PORTAL_PROMO;
					break;
				case PORTAL_TOOLS_PKG: 
					$action = RESPONSE_PORTAL_TOOLS_PKG; 
					$flag = RESPONDED_TO_PORTAL_TOOLS_PKG;
					break;
				case PORTAL_PROMO_CONTENT_TEST:
					$action = RESPONSE_PORTAL_PROMO_CONTENT_TEST; 
					$flag = RESPONDED_TO_PORTAL_PROMO_CONTENT_TEST;
					break;
				case PORTAL_PROMO_FOLLOW_UP:
					$action = RESPONSE_PORTAL_PROMO_FOLLOW_UP; 
					$flag = RESPONDED_TO_PORTAL_PROMO_FOLLOW_UP;
					break;
			}
			// make sure we don't have one already!
			if (!empty($emailDb->meta)) foreach($emailDb->meta as $meta) {
				if ($meta->action == $action) {
					$needMeta = false;
					break;
				}
			}
		}
		$emailDb->flags |= $flag;
		$fields = [];
		$fields['flags'] = $emailDb->flags;

		if ($needMeta) {
			$metas = [];
			$respondMeta = null;

			if (!empty($emailDb->meta)) foreach($emailDb->meta as $meta) {
				if ($meta->action == $restrict) {
					$respondMeta = $meta;
				}
				else
					$metas[] = $meta;
			}

			$respondMeta = new \stdClass();
			$respondMeta->action = $action;
			$respondMeta->date = $today;
			$respondMeta->emailTrackerId = $trackerId;
			$respondMeta->type = $type;
			if (!empty($name))
				$respondMeta->name = $name;
			$metas[] = $respondMeta;
			$fields['meta'] = $metas;
			$this->getClass('SellersEmailDb')->set([(object)['where'=>['id'=>$emailDb->id],
															 'fields'=>$fields]]);
			unset($metas, $respondMeta, $fields);
		}
	}

	protected function updateEmailDb($sellerId, $trackerId, $flag, $action, $track, $sendMailAlert = true, $emailDb = null) {
		$this->log("updateEmailDb - entered sellerId:$sellerId, trackerId:$trackerId, flag:$flag,  action:$action");

		$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$sellerId]]);
		if (empty($seller)) {
			$this->log("updateEmailDb - failed to find sellerId:$sellerId");
			return;
		}

		$seller = array_pop($seller);
		if (empty($emailDb))
			$emailDb = $this->getSellerEmailDb($seller);
		$today = date('Y-m-d H:i:s', time() + ($this->timezone_adjust*3600));
		$this->setEmailDb($emailDb, $flag, $action, $today, $trackerId);
		unset($emailDb);

		$msg = "Seller - id:$seller->id, $seller->first_name $seller->last_name, ";
		$trackerMeta = new \stdClass();
		$trackerMeta->action = ET_AGENT_RESPONSE;
		$trackerMeta->date = date('Y-m-d H:i:s', time() + ($this->timezone_adjust*3600));
		switch(intval($flag)) {
			case RESPONDED_TO_EMAIL:
				$msg .= "responded to PROMO EMAIL";
				$trackerMeta->to = "response to PROMO EMAIL";
				break;
			case RESPONDED_TO_FIRST:
				$msg .= "responded to INITIAL INTRO EMAIL";
				$trackerMeta->to = "response to INITIAL INTRO EMAIL";
				break;
			case RESPONDED_TO_SECOND:
				$msg .= "responded to SECOND EMAIL";
				$trackerMeta->to = "response to SECOND EMAIL";
				break;
			case RESPONDED_TO_THIRD:
				$msg .= "responded to THIRD EMAIL";
				$trackerMeta->to = "response to THRD EMAIL";
				break;
			case RESPONDED_TO_FOURTH:
				$msg .= "responded to FOURTH EMAIL";
				$trackerMeta->to = "response to FOURTH EMAIL";
				break;
			case RESPONDED_TO_PORTAL_PROMO:
				$msg .= "responded to PORTAL EMAIL";
				$trackerMeta->to = "response to PORTAL EMAIL";
				break;
			case RESPONDED_TO_PORTAL_TOOLS_PKG:
				$msg .= "responded to PORTAL TOOLS_PKG EMAIL";
				$trackerMeta->to = "response to PORTAL TOOLS_PKG EMAIL";
				break;
			case RESPONDED_TO_IMPROVE_LISITNG:
				$msg .= "responded to IMPROVE IT EMAIL";
				$trackerMeta->to = "response to IMPROVE IT EMAIL";
				break;
			case EMAIL_UNSUBSCRIBED:
				$msg .= "initiated UNSUBSCRIBE ORDER from ";
				$trackerMeta->to = "UNSUBSCRIBE ORDER from ";
				switch(intval($track->flags)) {
					case MESSAGE_EMAIL_FROM_LLC:
						switch(intval($track->type)) {
							case PROMO_FIRST: 
								$msg .= "INITIAL INTRO EMAIL";
								$trackerMeta->to .= "INITIAL INTRO EMAIL";
								break;
							case PROMO_SECOND:
								$msg .= "SECOND EMAIL";
								$trackerMeta->to .= "SECOND EMAIL";
								break;
							case PROMO_THIRD: 
								$msg .= "THIRD EMAIL";
								$trackerMeta->to .= "THIRD EMAIL";
								break;
							case PROMO_FOURTH: 
								$msg .= "FOURTH EMAIL";
								$trackerMeta->to .= "FOURTH EMAIL";
								break;
							case PORTAL_PROMO_1: 
								$msg .= "PROTAL EMAIL";
								$trackerMeta->to .= "PORTAL EMAIL";
								break;
							case PORTAL_TOOLS_PKG: 
								$msg .= "PROTAL TOOL PKG EMAIL";
								$trackerMeta->to .= "PROTAL TOOL PKG EMAIL";
								break;
							case PORTAL_PROMO_CONTENT_TEST:
								$msg .= "PORTAL PROMO CONTENT TEST";
								$trackerMeta->to .= "PORTAL PROMO CONTENT TEST";
								break;
							case PORTAL_PROMO_FOLLOW_UP:
								$msg .= "PORTAL PROMO FOLLOW_UP";
								$trackerMeta->to .= "PORTAL PROMO FOLLOW_UP";
								break;
							case IMPROVE_LISTING: 
								$msg .= "IMPROVE IT EMAIL";
								$trackerMeta->to .= "IMPROVE IT EMAIL";
								break;
						}
				}
				break;
		}

		if (!empty($track)) {
			if (empty($track->meta))
				$track->meta = [$trackerMeta];
			else
				$track->meta[] = $trackerMeta;
			$this->getClass("EmailTracker")->set([(object)['where'=>['id'=>$track->id],
								   						  'fields'=>['meta'=>$track->meta]]]);
		}

		if ($sendMailAlert) {
			$sent = $this->sendMail( SUPPORT_EMAIL, "Email Response", $msg,
									"Agent Response",
									"Note action and source.");

			$this->log("updateEmailDb - $msg, support email sent:$sent");
		}

		unset($seller, $trackerMeta);
	}

	public function getChimpComplaints($index) {
		$campaign = null;
		foreach($this->chimpCampaigns as $camp) {
			$camp = (object)$camp;
			if ($camp->key == $index) {
				$campaign = $camp;
				break;
			}
		}

		if (!$campaign) {
			$this->log("getChimpComplaints failed to find campaign for $index");
			return ['total'=>0];
		}

		$count = 0;
		$page = 0;
		$limit = CHIMP_LIMIT;
		$this->log("getChimpComplaints - begin for $index with cid:$campaign->cid");
		while(true) {
			$list = $this->chimp->reports->abuse($campaign->cid, ['start'=>$page, 'limit'=>$limit]);
			$this->log("getChimpComplaints on page:$page got ".$list['total']);
			$page++;
			if (empty($list) ||
				$list['total'] == 0 ||
				empty($list['data']))
				break;
			$count += $list['total'];
			unset($list);
		}
		$this->log("getChimpComplaints got $count");
		return ['total'=>$count];
	}

	public function getChimpUnsubscribes($index) {
		$campaign = null;
		$this->log("getChimpUnsubscribes entered for $index");
		foreach($this->chimpCampaigns as $camp) {
			$camp = (object)$camp;
			if ($camp->key == $index) {
				$campaign = $camp;
				break;
			}
		}
		if (!$campaign) {
			$this->log("getChimpUnsubscribes failed to find campaign for $index");
			return ['total'=>0];
		}
		$count = 0;
		$page = 0;
		$limit = CHIMP_LIMIT;
		$this->log("getChimpUnsubscribes - begin for $index with cid:$campaign->cid");
		while(true) {
			$list = $this->chimp->reports->unsubscribes($campaign->cid, ['start'=>$page, 'limit'=>$limit]);
			$this->log("getChimpUnsubscribes on page:$page got ".$list['total']);
			$page++;
			if (empty($list) ||
				$list['total'] == 0 ||
				empty($list['data']))
				break;
			$count += $list['total'];
			unset($list);
		}
		$this->log("getChimpUnsubscribes got $count");
		return ['total'=>$count];
	}

	public function getChimpOpen($index) {
		$campaign = null;
		$this->log("getChimpOpen entered for $index");
		foreach($this->chimpCampaigns as $camp) {
			$camp = (object)$camp;
			if ($camp->key == $index) {
				$campaign = $camp;
				break;
			}
		}
		if (!$campaign) {
			$this->log("getChimpOpen failed to find campaign for $index");
			return ['total'=>0];
		}
		$count = 0;
		$page = 0;
		$limit = CHIMP_LIMIT;
		$this->log("getChimpOpen - begin for $index with cid:$campaign->cid");
		$list = $this->chimp->reports->opened($campaign->cid, ['start'=>$page, 'limit'=>$limit]);
		$this->log("getChimpOpen got {$list['total']}");
		return ['total'=>$list['total']];

		// while(true) {
		// 	$list = $this->chimp->reports->opened($campaign->cid, ['start'=>$page, 'limit'=>$limit]);
		// 	$this->log("getChimpOpen on page:$page got ".$list['total']);
		// 	$page++;
		// 	if (empty($list) ||
		// 		$list['total'] == 0 ||
		// 		empty($list['data']))
		// 		break;
		// 	$count += $list['total'];
		// 	unset($list);
		// }
		// $this->log("getChimpOpen got $count");
		// return ['total'=>$count];
	}

	protected function getEmailDbAction($flag) {
		$action = 0;
		switch(intval($flag)) {
			case RESPONDED_TO_FIRST: 
				$action = RESPONSE_FIRST_EMAIL; 
				break;
			case RESPONDED_TO_SECOND:
				$action = RESPONSE_SECOND_EMAIL; 
				break;
			case RESPONDED_TO_THIRD:
				$action = RESPONSE_THIRD_EMAIL; 
				break;
			case RESPONDED_TO_FOURTH:
				$action = RESPONSE_FOURTH_EMAIL; 
				break;
			case RESPONDED_TO_IMPROVE_LISITNG: 
				$action = IMPROVE_LISTING_RESPONSE; 
				break;
			case EMAIL_UNSUBSCRIBED:
				$action = EMAIL_UNSUBSCRIBED_DATA;
				break;
			case PROMO_FIRST: 
				$action = SENT_FIRST_EMAIL; 
				break;
			case PROMO_SECOND:
				$action = SENT_SECOND_EMAIL; 
				break;
			case PROMO_THIRD: 
				$action = SENT_THIRD_EMAIL; 
				break;
			case PROMO_FOURTH: 
				$action = SENT_FOURTH_EMAIL; 
				break;
			case PORTAL_PROMO_1: 
				$action = SENT_PORTAL_PROMO; 
				break;
			case PORTAL_TOOLS_PKG: 
				$action = SENT_PORTAL_TOOLS_PKG; 
				break;
			case PORTAL_PROMO_CONTENT_TEST:
				$action = SENT_PORTAL_PROMO_CONTENT_TEST; 
				break;
			case PORTAL_PROMO_FOLLOW_UP:
				$action = SENT_PORTAL_PROMO_FOLLOW_UP; 
				break;
			case IMPROVE_LISTING: 
				$action = IMPROVE_LISTING_SENT; 
				break;
			case RESERVATION_MADE:
				$action = RESERVED_SOMETHING;
				break;
		}
		return $action;
	}


	protected function processUnsubscribeList($list, $campaign, &$chimpTrack, $type) {
		if ( empty($list) ||
			 $list['total'] == 0) {
			return 0;
		}

		$key = $campaign->key;
		$cid = $campaign->cid;
		$name = $campaign->name;
		$type = $campaign->type;

		if (!empty($chimpTrack)) {
			if ( array_key_exists($key, $chimpTrack) ) {
				if ( $chimpTrack[$key]->count == $list['total'] )
					return 0;
			}
			else
				$chimpTrack[$key] = (object)['count'=>0,
											 'cid'=>$cid,
											 'emails'=>[]];	
		}
		else {
			if (!isset($chimpTrack) || gettype($chimpTrack) != 'array')
				$chimpTrack = [];
			$chimpTrack[$key] = (object)['count'=>0,
										 'cid'=>$cid,
										 'emails'=>[]];
		}
		$count = 0;
		foreach($list['data'] as $chump) {
			if ( in_array($chump['member']['email'], $chimpTrack[$key]->emails) )
				continue;

			$seller = $this->getClass('Sellers')->get((object)['where'=>['email'=>$chump['member']['email']]]);
			if ( empty($seller) ) {
				// $this->log("processUnsubscribeList - failed to find seller with email: {$chump['member']['email']} for $key:$cid");
				continue;
			}
			foreach($seller as $agent) {
				$emailDb = $this->getClass('SellersEmailDb')->get((object)['where'=>['seller_id'=>$agent->id]]);
				if ( empty($emailDb) ) {
					continue;
				}
				$tracker = $this->getClass('EmailTracker')->get((object)['where'=>['agent_id'=>$agent->id,
																						'type'=>$type]]);
				$trackerId = empty($tracker) ? -1 : $tracker[0]->id;
				$action = EMAIL_UNSUBSCRIBED_DATA;
				$flag = EMAIL_UNSUBSCRIBED;
																							// this parameter to force overwrite
				$this->setEmailDb($emailDb[0], $flag, $action, $chump['member']['timestamp'], $trackerId, EMAIL_UNSUBSCRIBED_DATA, $type, $name);
				unset($agent, $tracker, $emailDb);
			}
			$chimpTrack[$key]->emails[] = $chump['member']['email'];
			$chimpTrack[$key]->count++;
			$count++;
			$msg = "Seller - id:{$seller[0]->id}, {$seller[0]->first_name} {$seller[0]->last_name}, initiated UNSUBSCRIBE ORDER for $name:$type from $key:$cid";
			$sent = $this->sendMail( SUPPORT_EMAIL, "Chimp unsubsribe", $msg,
									"Chimp Unsubsribe",
									"Note action and source.");

			$this->log("processUnsubscribeList - $msg, support email sent:$sent");
			unset($seller);
		}
		return $count;
	}

	public function updateChimpComplaints() {
		$this->log("updateChimpComplaints - entered for ".count($this->chimpCampaigns)." campaigns");
		$count = 0;
		$chimpTrack = $this->getClass('Options')->get((object)['where'=>['opt'=>'ChimpUnsubscribeTracker']]);
		if (!empty($chimpTrack))
			$chimpTrack = json_decode($chimpTrack[0]->value);
		$chimpTrack = (array)$chimpTrack;
		$index = 0;
		foreach($this->chimpCampaigns as $campaign) {
			if (empty($campaign) ||
				$index == count($this->chimpCampaigns))
				break;
			$campaign = (object)$campaign;
			$index++;
			$count = 0;
			$page = 0;
			$limit = CHIMP_LIMIT;
			$working = true;
			while( $working ) {
				$list = $this->chimp->reports->abuse($campaign->cid, ['start'=>$page, 'limit'=>$limit]);
				if (empty($list) ||
					$list['total'] == 0 ||
					empty($list['data']))
					break;
				$this->log("updateChimpComplaints - $campaign->key:$campaign->name page:$page for abuse, got:".count($list['data']));
				$count += $this-> processUnsubscribeList($list, $campaign, $chimpTrack, 'Chimp Complaints');
				$page++;
				if ( count($list['data']) < $limit)
					$working = false;
				unset($list);
			}
			
			$page = 0;
			$working = true;
			while( $working ) {
				$list = $this->chimp->reports->unsubscribes($campaign->cid, ['start'=>$page, 'limit'=>$limit]);
				if (empty($list) ||
					$list['total'] == 0 ||
					empty($list['data']))
					break;
				$this->log("updateChimpComplaints - $campaign->key:$campaign->name page:$page for unsubscribes, got:".count($list['data']));
				$count += $this-> processUnsubscribeList($list, $campaign, $chimpTrack, 'Chimp Unsubscribes');
				$page++;
				if ( count($list['data']) < $limit)
					$working = false;
				unset($list);
			}
			$this->log("updateChimpComplaints - $campaign->key:$campaign->name done");
			unset($campaign);
		}
		$this->log("updateChimpComplaints - done with campaigns");
		if ($count) // something got updated
			$this->getClass('Options')->set([(object)['where'=>['opt'=>'ChimpUnsubscribeTracker'],
													  'fields'=>['value'=>json_encode($chimpTrack)]]]);

		$this->log("updateChimpComplaints - exiting with $count");
		return new Out('OK', $count);
	}

	public function updateChimpComplaintsPerCampaign($index) {
		$campaign = null;
		$this->log("updateChimpComplaintsPerCampaign entered for $index");
		foreach($this->chimpCampaigns as $camp) {
			$camp = (object)$camp;
			if ($camp->key == $index) {
				$campaign = $camp;
				break;
			}
		}
		if (!$campaign) {
			$this->log("updateChimpComplaintsPerCampaign failed to find campaign for $index");
			return ['total'=>0];
		}

		$chimpTrack = $this->getClass('Options')->get((object)['where'=>['opt'=>'ChimpUnsubscribeTracker']]);
		if (!empty($chimpTrack)) {
			$chimpTrack = json_decode($chimpTrack[0]->value);
			$chimpTrack = (array)$chimpTrack;
		}
		else
			$chimpTrack = [];

		$count = 0;
		$page = 0;
		$limit = CHIMP_LIMIT;
		$working = true;
		while( $working ) {
			$list = $this->chimp->reports->abuse($campaign->cid, ['start'=>$page, 'limit'=>$limit]);
			if (empty($list) ||
				$list['total'] == 0 ||
				empty($list['data']))
				break;
			$this->log("updateChimpComplaintsPerCampaign - $campaign->key:$campaign->name page:$page for abuse, got:".count($list['data']));
			$count += $this-> processUnsubscribeList($list, $campaign, $chimpTrack, 'Chimp Complaints');
			$page++;
			if ( count($list['data']) < $limit)
				$working = false;
			unset($list);
		}
		
		$page = 0;
		$working = true;
		while( $working ) {
			$list = $this->chimp->reports->unsubscribes($campaign->cid, ['start'=>$page, 'limit'=>$limit]);
			if (empty($list) ||
				$list['total'] == 0 ||
				empty($list['data']))
				break;
			$this->log("updateChimpComplaintsPerCampaign - $campaign->key:$campaign->name page:$page for unsubscribes, got:".count($list['data']));
			$count += $this-> processUnsubscribeList($list, $campaign, $chimpTrack, 'Chimp Unsubscribes');
			$page++;
			if ( count($list['data']) < $limit)
				$working = false;
			unset($list);
		}
		$this->log("updateChimpComplaintsPerCampaign - $campaign->key:$campaign->name done");
		unset($campaign);
		
		$this->log("updateChimpComplaintsPerCampaign - done with campaigns");
		if ($count) // something got updated
			$this->getClass('Options')->set([(object)['where'=>['opt'=>'ChimpUnsubscribeTracker'],
													  'fields'=>['value'=>json_encode($chimpTrack)]]]);

		$this->log("updateChimpComplaintsPerCampaign - exiting with $count");
		return ['total'=>$count];
	}

	public function updateCampaignSentTo($index) {
		$campaign = null;
		$this->log("updateCampaignSentTo entered for $index");
		foreach($this->chimpCampaigns as $camp) {
			$camp = (object)$camp;
			if ($camp->key == $index) {
				$campaign = $camp;
				break;
			}
		}
		if (!$campaign) {
			$this->log("updateCampaignSentTo failed to find campaign for $index");
			return ['total'=>0];
		}

		$chimpTrack = $this->getClass('Options')->get((object)['where'=>['opt'=>'ChimpSentToTracker']]);
		if (!empty($chimpTrack)) {
			$chimpTrack = json_decode($chimpTrack[0]->value);
			$chimpTrack = (array)$chimpTrack;
		}

		$page = 0;
		if (!empty($chimpTrack)) {
			if ( array_key_exists($campaign->key, $chimpTrack) ) {
				$page = $chimpTrack[$campaign->key]->page;
			}
			else
				$chimpTrack[$campaign->key] = (object)['page'=>0];	
		}
		else {
			if (!isset($chimpTrack) || gettype($chimpTrack) != 'array')
				$chimpTrack = [];
			$chimpTrack[$campaign->key] = (object)['page'=>0];
		}
		$count = 0;
		
		$limit = CHIMP_LIMIT;
		$key = $campaign->key;
		$cid = $campaign->cid;
		$name = $campaign->name;
		$type = $campaign->type;
		$working = true;
		$summary = $this->chimp->reports->summary($campaign->cid);
		$timestamp = date('Y-m-d H:i:s', strtotime($summary['timeseries'][0]['timestamp']) + ($this->timezone_adjust*3600));
		while( $working ) {
			$list = $this->chimp->reports->sentTo($campaign->cid, ['start'=>$page, 'limit'=>$limit]);
			
			if (empty($list) ||
				empty($list['data']))
				break;
			$page++;
			$this->log("updateCampaignSentTo - page:$page $name:$type ".count($list['data']));
			$campaignCount = 0;
			foreach($list['data'] as $chump) {
				time_nanosleep(0, 100000000);
				$seller = $this->getClass('Sellers')->get((object)['where'=>['email'=>$chump['member']['email']]]);
				if ( empty($seller) ) {
					$this->log("updateCampaignSentTo - empty seller for ".$chump['member']['email']);
					continue;
				}
				foreach($seller as $agent) {
					$emailDb = $this->getClass('SellersEmailDb')->get((object)['where'=>['seller_id'=>$agent->id]]);
					if ( empty($emailDb) ) {
						$this->log("updateCampaignSentTo - empty emailDb for agent:$agent->id");
						// $this->log("processUnsubscribeList - failed to find sellerEmailDb for seller:{$seller[0]->id} with email: {$chump['member']['email']} for $key:$cid");
						continue;
					}
					$emailDb = array_pop($emailDb);
					// $tracker = $this->getClass('EmailTracker')->get((object)['where'=>['agent_id'=>$agent->id,
					// 																	'type'=>$type]]);
					// $trackerId = empty($tracker) ? -1 : $tracker[0]->id;
					$trackerId = -1;
					$action = $this->getEmailDbAction($type);
					if (!$action) {
						$this->log("updateCampaignSentTo - failed to get action for $type");
						continue;
					}
					$flag = $type; // type of campaign == flag that would need to be set
					// if this campaign hasn't been tagged as sent, then do it											
					if ( ($emailDb->flags & $flag) == 0) {
						if ( ($emailDb->flags & RESPONDED_TO_EMAIL) != 0 ) { // then agent had responded before the SentTo was called
							$response = 0;
							$responseAction = 0;
							switch($flag) {
								case PROMO_FIRST: $responseAction = RESPONSE_FIRST_EMAIL; $response = RESPONDED_TO_FIRST; break;
								case PROMO_SECOND: $responseAction = RESPONSE_SECOND_EMAIL; $response = RESPONDED_TO_SECOND; break;
								case PROMO_THIRD: $responseAction = RESPONSE_THIRD_EMAIL; $response = RESPONDED_TO_THIRD; break;
								case PROMO_FOURTH: $responseAction = RESPONSE_FOURTH_EMAIL; $response = RESPONDED_TO_FOURTH; break;
								case PORTAL_PROMO_1: $responseAction = RESPONSE_PORTAL_PROMO; $response = RESPONDED_TO_EMAIL; break;
								case PORTAL_TOOLS_PKG: $responseAction = RESPONSE_PORTAL_TOOLS_PKG; $response = RESPONDED_TO_EMAIL; break;
								case PORTAL_PROMO_CONTENT_TEST: $responseAction = RESPONSE_PORTAL_PROMO_CONTENT_TEST; $response = RESPONDED_TO_EMAIL; break;
								case PORTAL_PROMO_FOLLOW_UP: $responseAction = RESPONSE_PORTAL_PROMO_FOLLOW_UP; $response = RESPONDED_TO_EMAIL; break;
							}
							if (!empty($emailDb->meta)) {
								foreach($emailDb->meta as &$meta)
									if ($meta->action == RESPONSE_TO_EMAIL) {
										$meta->action = $responseAction; // correct the meta type
										break; // out of foreach()
									}
							}
							// clear generic flag and put in specific one
							$emailDb->flags = ($emailDb->flags & ~RESPONDED_TO_EMAIL) | $response;
						}

						$this->setEmailDb($emailDb, $flag, $action, $timestamp, $trackerId, $action, "ChimpSentTo", $name, false);
						$count++;
						$campaignCount++;
					}
					// else
					// 	$this->log("updateCampaignSentTo -  agent:$agent->id has flag:$flag set already");
					unset($agent, $emailDb, $tracker);
				}
				if ( count($list['data']) < $limit )
					$working = false;
			}
			$this->log("updateCampaignSentTo - $name:$type counted $campaignCount updates");
			unset($list);
		}
		unset($summary);
		if (($page > 1 ? $page-1 : $page) != $chimpTrack[$campaign->key]->page) {
			$chimpTrack[$campaign->key]->page = $page > 1 ? $page-1 : $page;
			$this->getClass('Options')->set([(object)['where'=>['opt'=>'ChimpSentToTracker'],
													  'fields'=>['value'=>json_encode($chimpTrack)]]]);
		}
		$this->log("updateCampaignSentTo got $count");
		return ['total'=>$count];
	}

	public function updateCampaignSentToOnePage($index) {
		$campaign = null;
		$this->log("updateCampaignSentTo entered for $index");
		foreach($this->chimpCampaigns as $camp) {
			$camp = (object)$camp;
			if ($camp->key == $index) {
				$campaign = $camp;
				break;
			}
		}
		if (!$campaign) {
			$this->log("updateCampaignSentToOnePage failed to find campaign for $index");
			return ['total'=>0];
		}

		$chimpTrack = $this->getClass('Options')->get((object)['where'=>['opt'=>'ChimpSentToTracker']]);
		if (!empty($chimpTrack)) {
			$chimpTrack = json_decode($chimpTrack[0]->value);
			$chimpTrack = (array)$chimpTrack;
		}

		$page = 0;
		if (!empty($chimpTrack)) {
			if ( array_key_exists($campaign->key, $chimpTrack) ) {
				$page = $chimpTrack[$campaign->key]->page;
			}
			else
				$chimpTrack[$campaign->key] = (object)['page'=>0];	
		}
		else {
			if (!isset($chimpTrack) || gettype($chimpTrack) != 'array')
				$chimpTrack = [];
			$chimpTrack[$campaign->key] = (object)['page'=>0];
		}
		$count = 0;
		
		$limit = CHIMP_LIMIT;
		$key = $campaign->key;
		$cid = $campaign->cid;
		$name = $campaign->name;
		$type = $campaign->type;
		$working = true;
		$summary = $this->chimp->reports->summary($campaign->cid);
		$timestamp = date('Y-m-d H:i:s', strtotime($summary['timeseries'][0]['timestamp']) + ($this->timezone_adjust*3600));
		//while( $working ) {
		$list = $this->chimp->reports->sentTo($campaign->cid, ['start'=>$page, 'limit'=>$limit]);
		
		if (empty($list) ||
			empty($list['data'])) {
			$working = false;
		}
		if ($working) {
			$page++;
			$this->log("updateCampaignSentToOnePage - page:$page $name:$type ".count($list['data']));
			$campaignCount = 0;
			foreach($list['data'] as $chump) {
				$seller = $this->getClass('Sellers')->get((object)['where'=>['email'=>$chump['member']['email']]]);
				if ( empty($seller) ) {
					$this->log("updateCampaignSentToOnePage - empty seller for ".$chump['member']['email']);
					continue;
				}
				foreach($seller as $agent) {
					$emailDb = $this->getClass('SellersEmailDb')->get((object)['where'=>['seller_id'=>$agent->id]]);
					if ( empty($emailDb) ) {
						$this->log("updateCampaignSentToOnePage - empty emailDb for agent:$agent->id");
						// $this->log("processUnsubscribeList - failed to find sellerEmailDb for seller:{$seller[0]->id} with email: {$chump['member']['email']} for $key:$cid");
						continue;
					}
					$emailDb = array_pop($emailDb);
					// $tracker = $this->getClass('EmailTracker')->get((object)['where'=>['agent_id'=>$agent->id,
					// 																	'type'=>$type]]);
					// $trackerId = empty($tracker) ? -1 : $tracker[0]->id;
					$trackerId = -1;
					$action = $this->getEmailDbAction($type);
					if (!$action) {
						$this->log("updateCampaignSentToOnePage - failed to get action for $type");
						continue;
					}
					$flag = $type; // type of campaign == flag that would need to be set
					// if this campaign hasn't been tagged as sent, then do it											
					if ( ($emailDb->flags & $flag) == 0) {
						if ( ($emailDb->flags & RESPONDED_TO_EMAIL) != 0 ) { // then agent had responded before the SentTo was called
							$response = 0;
							$responseAction = 0;
							switch($flag) {
								case PROMO_FIRST: $responseAction = RESPONSE_FIRST_EMAIL; $response = RESPONDED_TO_FIRST; break;
								case PROMO_SECOND: $responseAction = RESPONSE_SECOND_EMAIL; $response = RESPONDED_TO_SECOND; break;
								case PROMO_THIRD: $responseAction = RESPONSE_THIRD_EMAIL; $response = RESPONDED_TO_THIRD; break;
								case PROMO_FOURTH: $responseAction = RESPONSE_FOURTH_EMAIL; $response = RESPONDED_TO_FOURTH; break;
								case PORTAL_PROMO_1: $responseAction = RESPONSE_PORTAL_PROMO; $response = RESPONDED_TO_EMAIL; break;
								case PORTAL_TOOLS_PKG: $responseAction = RESPONSE_PORTAL_TOOLS_PKG; $response = RESPONDED_TO_EMAIL; break;
								case PORTAL_PROMO_CONTENT_TEST: $responseAction = RESPONSE_PORTAL_PROMO_CONTENT_TEST; $response = RESPONDED_TO_EMAIL; break;
								case PORTAL_PROMO_FOLLOW_UP: $responseAction = RESPONSE_PORTAL_PROMO_FOLLOW_UP; $response = RESPONDED_TO_EMAIL; break;
							}
							if (!empty($emailDb->meta)) {
								foreach($emailDb->meta as &$meta)
									if ($meta->action == RESPONSE_TO_EMAIL) {
										$meta->action = $responseAction; // correct the meta type
										break; // out of foreach()
									}
							}
							// clear generic flag and put in specific one
							$emailDb->flags = ($emailDb->flags & ~RESPONDED_TO_EMAIL) | $response;
						}

						$this->setEmailDb($emailDb, $flag, $action, $timestamp, $trackerId, $action, "ChimpSentTo", $name, false);
						$count++;
						$campaignCount++;
					}
					// else
					// 	$this->log("updateCampaignSentTo -  agent:$agent->id has flag:$flag set already");
					unset($agent, $emailDb, $tracker);
				}
			}
			if ( count($list['data']) < $limit ) {
				$working = false;
				$page--; // reuse this page to start next time
			}
			$this->log("updateCampaignSentToOnePage - $name:$type counted $campaignCount updates");
			unset($list);
		}
		unset($summary);
		if ($page != $chimpTrack[$campaign->key]->page) {
			$chimpTrack[$campaign->key]->page = $page;
			$this->getClass('Options')->set([(object)['where'=>['opt'=>'ChimpSentToTracker'],
													  'fields'=>['value'=>json_encode($chimpTrack)]]]);
		}
		$this->log("updateCampaignSentToOnePage got $count");
		return ['total'=>$count, 'working'=>$working];
	}

	public function updateCampaignOpen($index) {
		$campaign = null;
		$this->log("updateCampaignOpen entered for $index");
		foreach($this->chimpCampaigns as $camp) {
			$camp = (object)$camp;
			if ($camp->key == $index) {
				$campaign = $camp;
				break;
			}
		}
		if (!$campaign) {
			$this->log("updateCampaignOpen failed to find campaign for $index");
			return ['total'=>0];
		}

		$chimpTrack = $this->getClass('Options')->get((object)['where'=>['opt'=>'ChimpOpenTracker']]);
		if (!empty($chimpTrack)) {
			$chimpTrack = json_decode($chimpTrack[0]->value);
			$chimpTrack = (array)$chimpTrack;
		}

		$page = 0;
		if (!empty($chimpTrack)) {
			if ( array_key_exists($campaign->key, $chimpTrack) ) {
				$page = $chimpTrack[$campaign->key]->page;
			}
			else
				$chimpTrack[$campaign->key] = (object)['page'=>0];	
		}
		else {
			if (!isset($chimpTrack) || gettype($chimpTrack) != 'array')
				$chimpTrack = [];
			$chimpTrack[$campaign->key] = (object)['page'=>0];
		}
		$count = 0;
		
		$limit = CHIMP_LIMIT;
		$key = $campaign->key;
		$cid = $campaign->cid;
		$name = $campaign->name;
		$type = $campaign->type;
		$working = true;
		while( $working ) {
			$list = $this->chimp->reports->clicks($campaign->cid, ['start'=>$page, 'limit'=>$limit]);
			
			if (empty($list) ||
				empty($list['data']))
				break;
			$page++;
			$this->log("updateCampaignOpen - $name:$type ".count($list['data']));
			$campaignCount = 0;
			foreach($list['data'] as $chump) {
				$seller = $this->getClass('Sellers')->get((object)['where'=>['email'=>$chump['member']['email']]]);
				if ( empty($seller) ) {
					$this->log("updateCampaignOpen - empty seller for ".$chump['member']['email']);
					continue;
				}
				foreach($seller as $agent) {
					$emailDb = $this->getClass('SellersEmailDb')->get((object)['where'=>['seller_id'=>$agent->id]]);
					if ( empty($emailDb) ) {
						$this->log("updateCampaignOpen - empty emailDb for agent:$agent->id");
						// $this->log("processUnsubscribeList - failed to find sellerEmailDb for seller:{$seller[0]->id} with email: {$chump['member']['email']} for $key:$cid");
						continue;
					}
					$emailDb = array_pop($emailDb);
					// $tracker = $this->getClass('EmailTracker')->get((object)['where'=>['agent_id'=>$agent->id,
					// 																	'type'=>$type]]);
					// $trackerId = empty($tracker) ? -1 : $tracker[0]->id;
					$trackerId = -1;
					$action = $this->getEmailDbAction($type);
					if (!$action) {
						$this->log("updateCampaignOpen - failed to get action for $type");
						continue;
					}
					$flag = $type; // type of campaign == flag that would need to be set
					// if this campaign hasn't been tagged as sent, then do it											
					if ( ($emailDb->flags & $flag) == 0) {
						if ( ($emailDb->flags & RESPONDED_TO_EMAIL) != 0 ) { // then agent had responded before the SentTo was called
							$response = 0;
							$responseAction = 0;
							switch($flag) {
								case PROMO_FIRST: $responseAction = RESPONSE_FIRST_EMAIL; $response = RESPONDED_TO_FIRST; break;
								case PROMO_SECOND: $responseAction = RESPONSE_SECOND_EMAIL; $response = RESPONDED_TO_SECOND; break;
								case PROMO_THIRD: $responseAction = RESPONSE_THIRD_EMAIL; $response = RESPONDED_TO_THIRD; break;
								case PROMO_FOURTH: $responseAction = RESPONSE_FOURTH_EMAIL; $response = RESPONDED_TO_FOURTH; break;
								case PORTAL_PROMO_1: $responseAction = RESPONSE_PORTAL_PROMO; $response = RESPONDED_TO_EMAIL; break;
								case PORTAL_TOOLS_PKG: $responseAction = RESPONSE_PORTAL_TOOLS_PKG; $response = RESPONDED_TO_EMAIL; break;
								case PORTAL_PROMO_CONTENT_TEST: $responseAction = RESPONSE_PORTAL_PROMO_CONTENT_TEST; $response = RESPONDED_TO_EMAIL; break;
								case PORTAL_PROMO_FOLLOW_UP: $responseAction = RESPONSE_PORTAL_PROMO_FOLLOW_UP; $response = RESPONDED_TO_EMAIL; break;
							}
							if (!empty($emailDb->meta)) {
								foreach($emailDb->meta as &$meta)
									if ($meta->action == RESPONSE_TO_EMAIL) {
										$meta->action = $responseAction; // correct the meta type
										break; // out of foreach()
									}
							}
							// clear generic flag and put in specific one
							$emailDb->flags = ($emailDb->flags & ~RESPONDED_TO_EMAIL) | $response;
						}

						$this->setEmailDb($emailDb, $flag, $action, $chump['member']['timestamp'], $trackerId, $action, "ChimpSentTo", $name);
						$count++;
						$campaignCount++;
					}
					// else
					// 	$this->log("updateCampaignSentTo -  agent:$agent->id has flag:$flag set already");
					unset($agent, $emailDb, $tracker);
				}
				if ( count($list['data']) < $limit )
					$working = false;
			}
			$this->log("updateCampaignOpen - $name:$type counted $campaignCount updates");
			unset($list);
		}
		if (($page > 1 ? $page-1 : $page) != $chimpTrack[$campaign->key]->page) {
			$chimpTrack[$campaign->key]->page = $page > 1 ? $page-1 : $page;
			$this->getClass('Options')->set([(object)['where'=>['opt'=>'ChimpOpenTracker'],
													  'fields'=>['value'=>json_encode($chimpTrack)]]]);
		}
		$this->log("updateCampaignOpen got $count");
		return ['total'=>$count];
	}

	public function getSummary($campaign) {
		return $this->chimp->reports->summary($campaign->cid);
	}

	public function updateChimpSentTo() {
		$count = 0;
		$chimpTrack = $this->getClass('Options')->get((object)['where'=>['opt'=>'ChimpSentToTracker']]);
		if (!empty($chimpTrack)) {
			$chimpTrack = json_decode($chimpTrack[0]->value);
			$chimpTrack = (array)$chimpTrack;
		}

		foreach($this->chimpCampaigns as $campaign) {
			$campaign = (object)$campaign;
			$page = 0;
			$limit = CHIMP_LIMIT;

			if (!empty($chimpTrack)) {
				if ( array_key_exists($campaign->key, $chimpTrack) ) {
					$page = $chimpTrack[$campaign->key]->page;
				}
				else
					$chimpTrack[$campaign->key] = (object)['page'=>0];	
			}
			else {
				if (!isset($chimpTrack) || gettype($chimpTrack) != 'array')
					$chimpTrack = [];
				$chimpTrack[$campaign->key] = (object)['page'=>0];
			}

			$working = true;
			while( $working ) {
				$list = $this->chimp->reports->sentTo($campaign->cid, ['start'=>$page, 'limit'=>$limit]);
				if (empty($list) ||
					$list['total'] == 0 ||
					empty($list['data']))
					break;
				$page++;
				$key = $campaign->key;
				$cid = $campaign->cid;
				$name = $campaign->name;
				$type = $campaign->type;
				$this->log("updateChimpSentTo - page:$page, $name:$type ".count($list['data']));
				$campaignCount = 0;
				foreach($list['data'] as $chump) {
					$seller = $this->getClass('Sellers')->get((object)['where'=>['email'=>$chump['member']['email']]]);
					if ( empty($seller) ) {
						$this->log("updateChimpSentTo - empty seller for ".$chump['member']['email']);
						continue;
					}
					foreach($seller as $agent) {
						$emailDb = $this->getClass('SellersEmailDb')->get((object)['where'=>['seller_id'=>$agent->id]]);
						if ( empty($emailDb) ) {
							$this->log("updateChimpSentTo - empty emailDb for agent:$agent->id");
							// $this->log("processUnsubscribeList - failed to find sellerEmailDb for seller:{$seller[0]->id} with email: {$chump['member']['email']} for $key:$cid");
							continue;
						}
						// $tracker = $this->getClass('EmailTracker')->get((object)['where'=>['agent_id'=>$agent->id,
						// 																	'type'=>$type]]);
						// $trackerId = empty($tracker) ? -1 : $tracker[0]->id;
						$trackerId = -1;
						$action = $this->getEmailDbAction($type);
						if (!$action) {
							$this->log("updateChimpSentTo - failed to get action for $type");
							continue;
						}
						$flag = $type;
																									
						if ( ($emailDb->flags & $flag) == 0) {
							$this->setEmailDb($emailDb[0], $flag, $action, $chump['member']['timestamp'], $trackerId, $action, $type, $name);
							$count++;
							$campaignCount++;
						}
						else
							$this->log("updateChimpSentTo -  agent:$agent->id has flag:$flag set already");
						unset($agent, $emailDb, $tracker);
					}
				}
				$this->log("updateChimpSentTo - $name:$type counted $campaignCount updates");
				if ( count($list['data']) < $limit)
					$working = false;
				unset($list);
			}

			if (($page > 1 ? $page-1 : $page) != $chimpTrack[$campaign->key]->page) {
				$chimpTrack[$campaign->key]->page = $page > 1 ? $page-1 : $page;
				$this->getClass('Options')->set([(object)['where'=>['opt'=>'ChimpSentToTracker'],
														  'fields'=>['value'=>json_encode($chimpTrack)]]]);
			}
			unset($campaign);
		}
		$this->log("updateChimpSentTo - done for $count agents");
		return new Out('OK', $count);
	}

	// called for agent response to one of our mail outs
	public function agentUnsubscribe($responseKey) {
		$keys = explode("__", $responseKey);
		$userId = intval($keys[1]); // this is the seller id, not author_id
		$tracker = $keys[0];

		$et = $this->getClass("EmailTracker");
		$track = $et->get((object)['where'=>['track_id'=>$tracker]]);
		if (empty($track)) {
			$h = '<!DOCTYPE html>';
			$h .= '<html><head>Failed to find email tracking</head>';
			$h .= '<body><h1>Invalid key: '.$keys[0].'.  Please contact administrator</h1></body>';
			$h .= '</html>';
			echo $h;
			return;
		}

		$track = array_pop($track);
		if ($track->agent_id != $userId) {
			$h = '<!DOCTYPE html>';
			$h .= '<html><head>Failed to matching user</head>';
			$h .= '<body><h1>Invalid userID: '.$keys[1].' with key:'.$keys[0].'.  Please contact administrator</h1></body>';
			$h .= '</html>';
			echo $h;
			return;
		}

		$action = EMAIL_UNSUBSCRIBED_DATA;
		$flag = EMAIL_UNSUBSCRIBED;

		$this->updateEmailDb($userId, $tracker, $flag, $action, $track);
		if (headers_sent())
			echo '<script type="text/javascript"> window.location = "'.get_home_url().'/agent-benefits/unsubscribe'.'"; </script>';
		else {
			header('Location: '.get_site_url()."/agent-benefits/unsubscribe");
			header("Connection: close");
		}
		die;
	}

	// called for agent response to one of our mail outs
	public function agentRespond($responseKey) {
		$keys = explode("__", $responseKey);
		$userId = intval($keys[1]); // this is the seller id, not author_id
		$tracker = $keys[0];

		$et = $this->getClass("EmailTracker");
		$track = $et->get((object)['where'=>['track_id'=>$tracker]]);
		if (empty($track)) {
			$h = '<!DOCTYPE html>';
			$h .= '<html><head>Failed to find email tracking</head>';
			$h .= '<body><h1>Invalid key: '.$keys[0].'.  Please contact administrator</h1></body>';
			$h .= '</html>';
			echo $h;
			return;
		}

		$track = array_pop($track);
		if ($track->agent_id != $userId) {
			$h = '<!DOCTYPE html>';
			$h .= '<html><head>Failed to matching user</head>';
			$h .= '<body><h1>Invalid userID: '.$keys[1].' with key:'.$keys[0].'.  Please contact administrator</h1></body>';
			$h .= '</html>';
			echo $h;
			return;
		}

		switch(intval($track->flags)) {
			case MESSAGE_EMAIL_FROM_LLC:
				$action = 0;
				$flag = 0;
				
				switch(intval($track->type)) {
					case PROMO_FIRST: 
						$action = RESPONSE_FIRST_EMAIL; 
						$flag = RESPONDED_TO_FIRST;
						break;
					case PROMO_SECOND:
						$action = RESPONSE_SECOND_EMAIL; 
						$flag = RESPONDED_TO_SECOND;
						break;
					case PROMO_THIRD: 
						$action = RESPONSE_THIRD_EMAIL; 
						$flag = RESPONDED_TO_THIRD;
						break;
					case PROMO_FOURTH: 
						$action = RESPONSE_FOURTH_EMAIL; 
						$flag = RESPONDED_TO_FOURTH;
						break;
					case IMPROVE_LISTING: 
						$action = IMPROVE_LISTING_RESPONSE; 
						$flag = RESPONDED_TO_IMPROVE_LISITNG;
						break;
					case PORTAL_PROMO_1: 
						$action = RESPONSE_PORTAL_PROMO; 
						$flag = RESPONDED_TO_PORTAL_PROMO;
						break;
					case PORTAL_TOOLS_PKG: 
						$action = RESPONSE_PORTAL_TOOLS_PKG; 
						$flag = RESPONDED_TO_PORTAL_TOOLS_PKG;
						break;
					case PORTAL_PROMO_CONTENT_TEST: 
						$action = RESPONSE_PORTAL_PROMO_CONTENT_TEST; 
						$flag = RESPONDED_TO_PORTAL_PROMO_CONTENT_TEST;
						break;
					case PORTAL_PROMO_FOLLOW_UP: 
						$action = RESPONSE_PORTAL_PROMO_FOLLOW_UP; 
						$flag = RESPONDED_TO_PORTAL_PROMO_FOLLOW_UP;
						break;
				}
				switch(intval($track->type)) {
					case PROMO_FIRST:
					case PROMO_SECOND:
					case PROMO_THIRD:
					case PROMO_FOURTH:
						$this->updateEmailDb($userId, $tracker, $flag, $action, $track);
						if (headers_sent())
							echo '<script type="text/javascript"> window.location = "'.get_home_url().'/agent-reservation/'.$userId.'"; </script>';
						else {
							header('Location: '.get_site_url()."/agent-reservation/$userId");
							header("Connection: close");
						}
						break;
					case PORTAL_PROMO_1:
						$this->updateEmailDb($userId, $tracker, $flag, $action, $track);
						$compound = "T$track->id-$userId";
						if (headers_sent())
							echo '<script type="text/javascript"> window.location = "'.get_home_url().'/agent-purchase-portal/'.$compound.'"; </script>';
						else {
							header('Location: '.get_site_url()."/agent-purchase-portal/$compound");
							header("Connection: close");
						}
						break;

					case PORTAL_TOOLS_PKG:
					case PORTAL_PROMO_CONTENT_TEST:
					case PORTAL_PROMO_FOLLOW_UP:
						$h = '<!DOCTYPE html>';
						$h .= '<html><head><h2>No API designated for PORTAL TOOLS PKG</h2></head>';
						$h .= '<body><h1>Please head over to our site, lifestyledlistings.com<br/>Have a great day!</h1></body>';
						$h .= '</html>';
						echo $h;
						die;
						break;

					case IMPROVE_LISTING:
						$page = "/register/$userId";
						if ($track->user_id) { // this is the user id from wordpress, must be able to get to seller's page
							$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$userId]]); // seller id
							if (!empty($seller) &&
								!empty($seller[0]->author_id) &&
								$seller[0]->author_id == $track->user_id) {
								$seller = array_pop($seller);
								$password = '';
								foreach($seller->meta as $meta) {
									if ($meta->action == SELLER_KEY) {
										$key = json_decode($meta->key);
										$len = count($key); // strlen($meta->key);
										for($i = 0; $i < $len; $i++)
											$password .= chr( $key[$i] ^ ($i + ord('.')) );
									}
								}

								$user_info = get_userdata($track->user_id);
								if ( ($h = $this->loginUser($user_info->user_login, $password)) !== true) {
									$this->log("agentRespond - agentRespond could not login seller:$userId, author_id:$seller->author_id");
								}
								else
									$page = '/sellers';

								$this->updateEmailDb($userId, $tracker, $flag, $action, $track);
							}
						}
						else {
							$this->updateEmailDb($userId, $tracker, $flag, $action, $track);
						}
						if (headers_sent())
							echo '<script type="text/javascript"> window.location = "'.get_home_url().$page.'"; </script>';
						else {
							header('Location: '.get_site_url().$page);
							header("Connection: close");
						}
						break;
				}
				break;			
			die;
		}

	}
	
}

// new Register();
