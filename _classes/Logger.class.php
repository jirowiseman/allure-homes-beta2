<?php
namespace AH;
require_once(__DIR__.'/../_classes/Utility.class.php');

/**
 * Almost all the DB interaction classes extend this class
 * @package AllureHome
 * @since 1.0
 */
class Logger {
	protected $log = null;
	protected $debugLevel = 3;

	public function __construct($logIt = 0, $name = "logger"){
		$this->log_to_file = $logIt;
		global $timezone_adjust;
	 	$this->timezone_adjust = $timezone_adjust;
		$this->name = strtolower(preg_replace( '/(?<!^)([A-Z])/', '-\\1', $name ));
		try {
			if ($this->log_to_file) {
				$this->log = new Log(__DIR__."/_logs/$this->name.log");
				// $this->log->clear();
				// $this->logFile->add( get_class($this).'::_construct()' );
				$this->log->options->buffer_length = -1;
			}
		} catch (\Exception $e) { parseException($e); }
	}
	public function __destruct(){
		if ($this->log !== null)
			$this->log->writeStringToFile();
	}

	public function forceEnableLog() {
		if (!$this->log) {
			$this->log = new Log(__DIR__."/_logs/".$this->name.".log");
			//$this->logFile->add( get_class($this).':forceEnableLog()' );
			$this->saved_log_to_file = $this->log_to_file;
			$this->log_to_file = 1;
			// $this->log->options->buffer_length = -1;
			return true;
		}
		return false;
	}

	public function forceDisableLog() {
		if ($this->log != null) {
			//$this->logFile->add( get_class($this).':forceDisableLog()' . "\r\n" );
			//fclose($this->lockfile);
			$this->flush();
			if (!$this->saved_log_to_file)
				$this->log = null;
		}
	}

	public function log($message, $debugLevel = 3, $hide_timestamp = false, $tab = 0) {
		if ($this->log !== null &&
			$debugLevel >= $this->debugLevel) {
			if ($debugLevel < 3)
				$message .= ", debugLevel:$debugLevel";
			for($i = 0; $i < $tab; $i++)
				$message = "\t".$message;
			$this->log->add($message, $hide_timestamp);
		}
	}

	public function clear() {
		if ($this->log !== null)
			$this->log->clear();
	}

	public function flush() {
		if ($this->log != null)
			$this->log->writeStringToFile();
	}
}

