<?php
namespace AH;
require_once(__DIR__.'/_Base.class.php');
class SmtpAccessCount extends Base {
	public function __construct($logIt = 0){
		parent::__construct();
		require_once(__DIR__.'/Options.class.php');
		$o = new Options;
		$x = $o->get((object)array('where'=>array('opt'=>'SmtpAccessCountClassDebugLevel')));
		$this->log_to_file = empty($logIt) ? (empty($x) ? 0 : intval($x[0]->value)) : $logIt;
		$this->log = $this->log_to_file ? new Log(__DIR__.'/_logs/SmtpAccessCount.log') : null;		
	}
}