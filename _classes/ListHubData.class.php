<?php
namespace AH;
require_once(__DIR__.'/_Controller.class.php');
require_once(__DIR__.'/ListHubMatchTags.class.php');
require_once(__DIR__.'/Utility.class.php');

require_once(__DIR__.'/../../../../wp-load.php');
require_once(__DIR__.'/../../../../wp-includes/pluggable.php');
require_once(__DIR__.'/../../../../wp-admin/includes/admin.php' );
	
/** Load Ajax Handlers for WordPress Core */
require_once( __DIR__.'/../../../../wp-admin/includes/ajax-actions.php' );

// 4.0.2 - a good base to start versioning
// 4.0.3 - added check to make sure if bed/baths value is higher in pre-existing, don't overwrite data from xml
//         most likely it was updated by admin or seller
//       - check to see if pre-existing has more tags than extracted from xml.  If so, don't overwrite it.
//       - if pre-existing was tagged as inactive, but shows up again in the feed, tag as either TOOCHEAP or WAITING
// 4.0.4 - changed the baths value is calculated.  It will now be #.###, where the digit represents whole baths, and
//         fractional value repreents # of partial (non-full) baths.
// 4.0.5 - Updated createListing() so that it checks to see that the incoming beds/baths are zero and pre-existing one's
//         value is not zero, then leave it alone.
// 4.0.6 - Force baths to 3 decimal places.
// 4.0.7 - Added feature to gzip the lisbhub csv log and put it into get_home_url()."/listhub
// 4.0.8 - Updated Photos processing to account for 'url' field in the pre-existing listing.  The presence of 'url'
//         means that this image had either trimmed or blackout treatment done on it and the 'file' now points to
//         the actual jpeg images created.
//         Also modified concludePP() so that it only retrieves id, listhub_key and active fields.  Not sure why the
//         listings are not marked as AH_INACTIVE if the 'tier' is still 0.
// 4.0.9 - Adjust order of checks made for Photo so that the 'url' field is given priority importance over 'file' and 'discard'.
//         Added image border trimming code.
// 4.0.10 - Added width and height values to be saved in each image data, displayed in the Listings Viewer.
// 4.0.11 - Modified fixImageBorders() to short-circuit if the 'file' is not an http link, since it's a static image
//          The command from page-listing will still work, as that does not check for the existance of static image.
// 4.0.12 - Modified fixImageBorders() to store the width/height values of the new image and for any static image that doesn't.
// 4.0.13 - Fixed listhubCSVTimestamp() to use the string 'listhubCSVTimestamp' for opt, instead of a non-existant $_POST[] value.
// 4.0.14 - Updated bailOutListings() to account for all discarded images.
// 4.0.15 - Add listing::id to debug log when adddresses dont' jive up.
// 4.0.16 - Fixed 4.0.14's change to bailOutListing()'s image count tracking.
// 4.0.17 - Fixed createListing() to assign AH_WAITING if it was AH_INACTIVE and it is now present.
// 4.0.18 - Use intval() when comparing ['price'] values to get TOOCHEAP error to take effect.
//          If no error and 'price' > 2500000, set it to AH_ACTIVE.
//          Fixed AN_AVERAGE to AH_AVERAGE.
//          Took out code to check for 'mls' field to find pre-existing listing.
// 4.0.19 - If listing has TOOCHEAP flag and NOBEDBATH, condemn to NEVER.  If 'price' is < 600000 and has NOIMAGE also, then NEVER also.
// 4.0.20 - Modified fixImageBorders() to do process 1000 images at a time.  Asking for too many listings seems to break the php memory limit.
// 4.0.21 - Modified createListing() to check Activity table to see if the preExisting listing had any forced 'active' category change.
//          If it was changed, then keep it there.
// 4.0.22 - added 'processed' flag to 'images' to prevent going through border checks unnecessarily.
// 4.0.23 - added IMAGE_BORDER_GROUP to park new page of listings to process in fixImageBorders().
// 4.0.24 - added 'start'/'end' to IMAGE_BORDER_GROUP
//          added COMMERCIAL error type, which will force the listing to AH_NEVER.
// 4.0.25 - added logging when listing is tagged as COMMERCIAL.  need to track down why some listings are getting it.
// 4.0.26 - added code in updateTags() to prevent any possible chance of sending duplicate tag ids.
// 4.0.27 - fixed parseRule for 'ListingTitle' to use ' land ' instead of 'land', since 'land' will get a hit for any word that had 'land' in it.
//          if priced less than 600K but is COMMERCIAL, tag as NEVER.
// 4.0.28 - updated bailOutListing() to use ':' to separate error strings, instead of ','.
// 4.0.29 - updated fixImageBorders() to correctly assign 'discard' field.
// 4.0.30 - added call to ListHubMatchTags::unTagList() when doing 'ListingDescription' to remove conflicts.
// 4.0.31 - added applyKeywords() and cleanTags().  ListHubMatchTags class was also updated with new tagging sets.
// 4.0.32 - fixed bug in ListHubMatchTags::searchSet() that prevented tags from being recorded. (typo of untagIt)
// 4.0.33 - fixed call to ListHubMatchTags::unTagList() with misspelled $object.  added 'override' to 'open floor plan' for untagging.
// 4.0.34 - added address data to seller table.
//          updated parser to merge cities tags with tags found in the listing, then merge that list with any existing tags for the listing.
//          modified minimum value a no-error listing can go active to 1.25M, from 2.25M.
// 4.0.35 - modifed such that $hasForcedActivity trumps whatever 'active' mode is in the database in createListing().
// 4.0.36 - added code to formatName() on seller's address.
//          updated parseListing() to gather all data except for Photos and DetailedCharacteristics if pre-existing is AH_NEVER.  This
//          allows for updating other data if needed, i.e. seller data.
// 4.0.37 - force seller's 'state' equal listing's if it's not there, so at least seller can be found by state.
// 4.0.38 - updated to accommodate Amazon's AWS by returning $_SERVER['HTTP_HOST'] so the front end can keep track of the load values.
// 4.0.39 - added missing ' ' into getCitiesTags(), just after b.score = 7
// 4.0.40 - added pcntl_fork() calls to processBlocks()
// 4.0.41 - added function handler for 'FullBathrooms' to prevent double counting with 'Bathrooms' field.
// 4.0.42 - added MarketingInformation::PermitAddressOnInternet processing to 'flags' col in the listings table bitwised with LISTING_PERMIT_ADDRESS
//          Modified parseListing() to do the check for $isNever and !isset($this->parseRules[$name]) before setting $pre value when 'primary'
//          is defined.  This caused the current code to lose all values to $listing.  Strangely, this code has been here since the beginning.
//          Re-activated the geo coding if this is a new listing that is AH_ACTIVE.
// 4.0.43 - added stripCity() to get rid of '(city)' in the city field.
// 4.0.44 - updated ListHubMatchTags with new set of tags to match (from Jiro).
//          renamged cities array with cityTags.
//          moved previous use of LOWTAGS to LACK_DESC.
//          do mergeCityTags() before calling createListings() to set LOWTAGS error code if necessary
//          call matchCity() to set 'city_id' in the listing, and if ncessary create a new entry in the Cities table.
// 4.0.45 - moved stripCity() and formatName() out of matchCities(), and made calls in stripCity() case in-sensitive.
// 4.0.46 - added batch saving of xml data to db.  call setBatchSizeXML() to get value from Options table if it exists.  If the value is
//          is 0, then resort to earlier single listing saves.
// 4.0.47 - update seller's information as needed in findSeller().
// 4.0.48 - updated handling of Office::PhoneNumber to be a func that only updates the 'phone' value if it's not set.
//          look at PropertyType and PropertySubType and tag 'house' and 'condo' accordingly.
// 5.0.0 -  changed 'equestrian' to 'horse property' in the description matching.
// 5.0.1 -  revamped the Google GeoCoding mechanism to use to separate APIs for address and lng/lat for creating the listing geocode values.
// 5.0.2 -  added more debug logs when doing geocoding.
// 5.0.3 -  added more terms to catch 'COMMERCIAL' listings in the ListingTitle and PropertyType elements.
//          also put existing listings through assignCategory(), even if in AH_ACTIVE if it wasn't forced into AH_ACTIVE.  This should
//          force existing listings that now have 'COMMERCIAL' to go to AH_NEVER.
//          check for 0,0 values for lng/lat.  Also any 'lat' not between 0/90 and lat not between 0/180.  If they fail, use address to get correct lng/lat
//          fixed Listings::geocodeListing() to look out for state/country values with 'out of','outside','other',unincorporated','foreign' and deny those.
// 5.0.4 -  force the 'state' to be in 2-char abbreviated form in createListings().
// 5.0.5 -  added code to trigger logging geocode activity in Listings class. Looks for 'listingGeoCodeDebugLevel' in Options to set the flag.
// 5.0.6 -  added code to getCitiesTags() and mergeCityTags() so that they both are checking for the correct key sequence/pattern of City-ST, and not
//          some other pattern like CITY-St, or City-st, etc..
// 5.0.7 -  updated matchCity() to reject any non-conforming city/state/country fields.
// 5.0.8 -  added code to fixImageBorders() so that the 'file' is restored from 'url' if its not set at the end.
// 5.0.9 -  updated geoCoding, such that if using the address to find lat/lng, the code is now able to use just the 'street_address' and 'zip' to find the
//          correct address.  And when that succeeds, it will update the Listings table with the correct 'city' and/or 'state' if those fields were non-conforming.
// 5.0.10 - reworked the listing's geoCoding, so that if it needed updating after getting lng/lat from address, set the city_id if avaliable, if not try
//          to create a new city geoCode entry.
// 5.0.11 - updated concludePP() to use glob() to find the next $index to append to the csv file.
// 5.0.12 - accomodate ListingURL tag, added 'url' col to Listings.
//          changed 5.0.11 to simply append the date field to the listhub csv file.
//          made comparing 'street_address' better by taking into account the possibility of '#' in the string.  So if there is,
//          only compare the forward section of the string.  Also spare the latter part from getting called into formatName() to preserve
//          the '-' and capitalization.
// 5.0.13 - added parsing for 'Disclaimer'
// 5.0.14 - lowered threshold to allow entry into AH_ACTIVE w/o errors to 800000.
// 5.0.15 - additiional error logging for image border fixing code.
// 5.0.16 - Updated getCitiesTags() to include tags with -1 score also.
// 5.0.17 - Updated geocoding to capture abbreviated cities like SF, LA and NY, missing MY state, hyphenated street-address.
// 5.0.18 - Updated geocoding to replace single letter compass direction with full word, i.e. East, West, North, Sourth
// 5.0.19 - Updated Address:UnitNumber so that it will set the 'condo' tag onto the listing, and prevent 'PropertyType' from setting the
//          'house' tag if 'condo' is alreay set.
// 5.0.20 - Updated Address:UnitNumber function to check to existing '#' in the $data value.  If so, don't add another one.  Also
//          check to see if $data is simply ".", and if so, disregard.
// 5.0.21 - Updated updateTags() to strip out 'house' tag from existing list if the new list has 'condo' in it.
// 5.0.22 - Updated Address:UnitNumber rule to disregard single char '0' also, besides '.' or is 'n/a'.
// 5.0.23 - Updated updateTags() to strip out 'house' tag from new list if the list also had 'condo' in it.
// 5.0.24 - When parsing PropertyType and PropertySubType, use the $ele arguemnt and try to extract the 'otherDescription' attribute.
//          If that attribute is Condominium, set the 'condo' tag.
// 5.0.25 - Beefed up all set() calls with try/catch.  Updated updateTags() to catch same count tags (existing/incoming), but are different.
// 5.0.26 - Updated geocodeLatLng() to take nthTry parameter so it doesn't go into an infinte recursion.
// 5.0.27 - Updated PropertySubType to look for apartment keyword to tag as condo.
// 5.0.28 - Updated fixImageBorders() to take $fromLast argument so if set, then it will look for 'LastImageBorderId' option and start the
//          processing from that listing.
// 5.0.29 - Check SELLER_MODIFIED_PROFILE_DATA meta data in findSeller() to decide whether to update the incoming listhub data or not.  If the
//          seller had edited their profile, prevent overwrite by the parser.
// 5.0.30 - Updated concludePP() to remove all traces of INACTIVE listings that have not been updated for over 30 days.
// 5.0.31 - improved 5.0.29's field screener to limit it to images, beds and baths for now.
// 5.0.32 - use addslashes() and stripslashes() to allow 'ListingDescription' to be saved as blocks properly when there are nested "'" (quote) 
//          in the description.  Screws up MySQL.
// 5.0.33 - added logging for when author_has_account gets changed from the existing value.
// 5.0.34 - updated concludePP() to defrag and optimze tables after removing old listings.
// 5.0.35 - added logging to defrag and optimze tables after removing old listings.
// 5.0.36 - withdrew defrag and optimization calls, hangs up..
// 5.0.37 - enable just optimization calls
// 5.0.38 - added checkSellerEmailDb() to maintain the seller's email db.
// 5.0.39 - In findSeller(), use 'like' for last email checking.
// 5.0.40 - checkSellerEmailDb() check for valid email address first, and set host to all lower case.
// 5.0.41 - added SellersEmailDb to optimizeTable()
// 5.0.42 - updated createListing() so that if the geoInfo[] was there with lng/lat values, then just use it if it's in range of +/- 90/180.
// 5.0.43 - updated checkSellerEmailDb() to set the flags value based on what the value of listing->active is.
// 5.0.44 - updated parseXml() and processBlock() to call new methods: addslashes() and stripslashes() for ListingDescription, Offices and Brokerage
// 5.0.45 - call out Sellers::updatePageEmailDb() in concludePP().
// 5.1.00 - first update since official email campaign.
//          added ProviderName to addslashes() and stripslashes() calls.
// 5.1.01 - added func to deal with Address::FullStreetAddress so that it will strip out any stupid www.xxx.xxx out of the address
// 5.1.02 - addslashes() and stripslashes() to images array
// 5.1.03 - added code to import images from listings to local storage
// 5.1.04 - changed up checkSellerEmailDb to get() instead of exists().  One seller fell through the crack and caused a warning.
// 5.1.05 - made locking of sql controlled by flag in init();
// 5.1.06 - use realpath() in fixImageBorders().  The "..", could be preventing file_exists() from returning true.
// 5.1.07 - added code to parallelize image border fixing.
// 5.1.08 - updated fixOneImageBorder() to consider a 'file' with http, but processsed images exists.
// 5.1.09 - added code for specialOperation(4), so that image border can be done in parallel right after a listhub parse.
//          added code to log activity from Seller::updatePageEmailDb() with it is called from concludePP.
//          added code to recordParseResult() to be called from FE when the parse is completed.
// 5.1.10 - added IMAGE_BORDER_UPDATE_LAST_ID to indicate what happened at update.
// 5.1.11 - added more debug when getting image count for border fixes.
// 5.1.12 - more debug logs.  code not updating Sellers email db.
// 5.1.13 - updated to use defines instead of hard-coded values for special ops.
//        - updated FE to allow cron'ed image border processing
// 5.1.14 - changed fixOneImageBorder() to check for hasWhiteBorder() even if http and processed == 1.  processed set to 3 or 4 now to differentiate from before
// 5.1.15 - fix bad calls to updateToDb() in fixOneImageBorder().
// 5.1.16 - added processed value to fixOneImageBorder() logs.
// 5.1.17 - reworked fixOneImageBorder() to always set the processed flag to $baseProcessValue so the image is not revisited.
// 5.1.18 - if image_jpeg() fails, set processed to -1
// 5.1.19 - added more debug in removeOneInactiveListing().
// 5.1.20 - updated cleanInactiveListings() to tag listings as AH_INACTIVE.
// 5.1.21 - always optimizeTable() in concludePP().
// 5.1.22 - added 'mobile' to list of user editable fields.
// 5.1.23 - undid 5.1.22
//          account for "Mc" in name, so created a new formatName(), and renamed previous to formatNameOldWay().
//          so if the seller couldn't be found with formatName(), use the old way and see.  The name stored with have
//          the "McXxxx", formatting for the name
// 5.1.24 - corrected formatName()'s treatment of "Mc" names to capitalize the 2-index letter instead of 3-index since it is 0-based.
//        - also cleaned up findSeller() by creating like2 and like3 in Base::formatWhereClause() to make the search cap-proof.
// 5.1.25 - added more logging to find/createSeller().
// 5.1.26 - formatName() in findSeller() before using first and last names.
// 5.1.27 - using BASE_IMAGE_PROCESSED_VALUE to change the image->processed value.
//          updated fixOneImageBorder() so that even if there is a local file, if the processed value is < BASE_IMAGE_PROCESSED_VALUE, i
//          it will be redone.
// 5.1.28 - more logging when doing parallel image border processing.
// 5.1.29 - fix fixOneImageBorder()'s less-than 'check for less than BASE_IMAGE_PROCESSED_VALUE' code if a local file exists.  If the image->url
//          is not there, don't unset($image->file), and set $alreadyDone to true.
// 5.2.00 - User special flag: USER_ABORTED_PP (99) to indicate user abort for parallel processing and only clear tables if the mode
//          to concludePP() is SPECIAL_OP_NONE (0)
// 5.2.01 - when setting inactive listings to AH_INACTIVE, do it for AH_NEVER ones also.
//          use time() instead of microtime(true) for current time in secs, microtime returns a decimal value with microsecs.  May be
//          why the time difference isn't triggering now?
//          use BASE_CONSIDERED_PRICE instead of 600000, can be changed later.
// 5.2.02 - User the $minPrice parameter to processBlocks() in assignCategory().
//          Set $this->baseConsideredPrice from BASE_CONSIDERED_PRICE, which is set to 500,000 right now.
// 5.2.03 - call assignCategory() for existing listings marked TOOCHEAP also.
//          updated assignCategory() to mark to 'Rejected' if TOOCHEAP, but no desc, image, tags, bed/bath
// 5.2.04 - updated parseXml() to record the listhub block count to Options::ListHubFeedSize.
// 5.2.05 - city is normalized now before calling createListings() and mergeCityTags(), without doing this, the key constructed
//          from city/state will not match the keys in $this->cityTags[].
// 5.2.06 - return Out from perform() instead of closiing connection.
//          if preExisting has a null or -1 city_id, try to get it.  The Cities::geocodeCity() now works better for non-US cities, as it
//          deems it a good find if the locality/neightborhood and country matches.
//          updated getCityGeoCode() to return null where appropriate.
// 5.2.07 - If getCityGeoCode() returns a null when retrying for a city_id == NULL for a preExisting, set it to -1, so it won't try again.
// 5.2.08 - wrapped set() with try/catch when setting the city_id at the end of parseOneListing()
// 5.2.09 - check for pre-existing city id before calling getCityGeoCode() at the end of parseOneListing().
// 5.2.10 - reverted some of the changes in perform().  Dunno if that had anything to do with the parser front end acting strange.
// 5.2.11 - added fixNY(), fixCity(), and fixCityStr() in matchCity().
// 5.2.12 - updated all header() call sequence to include header("Content-Encoding: none\r\n" ) and '\r\n' patterns so php will allow
//          the browser to have a clean disconnect and move on, preserving the asynchronous behavior.
// 5.2.13 - replaced stripslashes() with removeslashes().
// 5.2.14 - added geocodeZip() to Cities and GoogleLocation to find city names if it's empty but we have the zip code.
// 5.2.15 - updated matchCity() to account for preExisting to have an empty city and the city_id pointing to a Cities entry with blank city name also.
//        - then call geocodeZip() before fixing up the city/state names and trying to find a matching Cities entry for that.
// 5.2.16 - added check in matchCity() for nonConformingLocation($listing['city'])
// 5.2.17 - updated image border fixing code to generate base file names using up to two subdirectory names a prefiex to the base filename, this
//          should ensure that the image filename will be unique.
// 5.2.18 - updated matchCity() to use the lng/lat from geocodeZip() if it's good and try to find a city for it, if cannot find one, make one.
// 5.3.00 - added code to use MedianPrices to adjust each ZIP code's min price that's accepted.
// 5.3.01 - short-circuit parsing if the price < minimumAcceptedPrice
// 5.3.02 - modified short-circuiting so it gets all data except Photos, and then call getMedianPrice() so that the zip code's median prices are true.
// 5.3.03 - added MedianPrices to optimizeTable().
// 5.3.04 - record belowMinimumAcceptedPrice into RESULTS_PARALLEL
// 5.3.05 = updated usage of median prices so MedianPrices now has daily_home and daily_condo values that will hold that day's parsing information.
//          then in concludePP(), MedianPrices::consolidateDailyMedianPrices() gets called to update the monthly_home/condo and median_home/condo values
//          using the daily_home/condo values to have a rolling average for them (instead of monthly updates).
//          then in writeLogHeaderForParallelFile(), we call MedianPrices::resetDaily(). to reset the daily_home/condo and home/condo_count values to 0
// 5.3.06 - if the data value is < 1000, then force the $unit to 'acres' for LotSize, when it is in 'ft', assume it's an error
//        - add try/catch to add() in checkSellerEmailDb().
//        - fixed image border processing to not call getBaseImageName() again if $dest is already set.  This causes the filename to get messed up.
// 5.3.07 - updated matchCity() to test zip codes if Cities return more than one match for city/state combo.
//        - updated getCityGeoCode() to take a zip code and Cities::geocodeCities() and Cities::geocodeCity() to use the zip (if available)
// 5.3.08 - updated writeLogHeaderForParallelFile() to use 'WHERE 1' to set 'tier' to 0, instead of `tier` != 0 OR `tier` IS NULL"
// 5.3.09 - only allow fixOneImageBorder() for listhub listings, not user created ones.
// 5.3.10 - updated getActiveListingCount() to get groups of 1000 id's instead of in one big gulp.
// 5.3.11 - fixed repairListingImageData() to only process active listings from the feed, not ones created organically.  Also commented out the sections
//          related to using $mistakenListingIdLimit (past oops situation)
// 5.3.12 - Updated fixOneImageBorder() to only unset($image->file) if isset($image->url).  Also flag as IMAGE_BORDER_ERROR_FATAL if $image->file implies a processed
//          file, but it doesn't exist and $image->url is empty, basically lost the information :(
//          If that is the case, for listhub listings, those image instances will be removed from the array.
//          For organic listings, email will be sent to SUPPORT so we can address the problem.
// 5.3.13 - updated getActiveListingCount() to use $q for query options, was missing the ['what'=>['id']] call in the while() loop
// 5.3.14 - fixOneImageBorder() now uses readImage() and try to limit file accessing time to 5 secs.  If it times out, retry up to 5 times before giving up
// 5.3.15 - strangely, unlink() leaves an empty file with no extension?  trying popen("rm /tmp/alr??????", 'r'); to see if it helps
// 5.3.16 - call popen('rm') after calling processBlocks() for IMAGE_BORDER_PARALLEL.
// 5.3 17 - added IMAGE_BORDER_PARALLEL_BATCH to process images with less sql interaction
// 5.3.18 - added processOneImage() so we can test a single call to fixOneImageBorder().
// 5.3.19 - added SPECIAL_PARSE_BATCH to gather all xml blocks at one shot, may use it further if we can make the main parse more efficient
// 5.3.20 - set first_image for new listings and also during fixOneImageBorder() if it's not set yet or is different than what is present.
// 5.3.21 - added first_image to processBlocks() getting batch data for IMAGE_BORDER_PARALLEL_BATCH operation.
// 5.3.22 - updated parseXml() and processBlock() to call new methods: addslashes() and stripslashes() for Directions
// 5.3.23 - call setMiscOptions() to set forceRedoGeoCodeListing if Options::ForceRedoGeoCodeListing it exists, else default to false.
//          if only forceRedoGeoCodeListing is true, will a listing with a previously failed geocode be retried.
// 5.3.24 - redid findSeller() to look for sellers in the following manner:
//			1st: first_name, last_name and email
//			2nd: only first_name and email
//			3rd: only last_name and email
//			4th: first_name, last_name and phone or mobile
//			5th: first_name, last_name, and city
// 5.3.25 - if geocodeListing() fails, write back a geocode for listing (-1,-1) anyway to mark it as not locatable.
// 5.3.26 - added try/catch around processBlock() calls
// 5.3.27 - added code to recover bad chunks (initiated by front end)
//          fixed createSeller() to use $info returned from findSeller() as an object and not an array.
// 5.3.28 - added $isPerson parameter to formatName() to allow '&' character and also check for empty $str or if $str == ' '
// 5.3.29 - formatName() now checks for "O'" combination and uppercases the third character.
// 5.3.30 - formatName() now check for double single quotes and makes it one single quote.
// 5.3.31 - updated createSeller() to use the $x value returned from add() and use that as the $id, and not call finddSeller() again.
// 5.3.32 - reverted 5.3.31, since it may result in duplicate seller entries if two listings from the same seller was being processed and
//          each listing was tring to findSeller().  Essentially a race condition.  Now, createSeller() will delete the seller that has a
//			different id and row_id ($x) values, since that would indicate a redundant entry.
// 5.3.33 - updated formatName() with makeUpperCase[] array, to make some patterns uppercase (usually designations)
// 5.3.34 - added formatEmail() to strip out first email if it's a set of emails.
//          added more acronyms to capitalize in formatName().
// 5.3.35 - added more acronyms to capitalize in formatName(), and also to handle "(", and '"' starting $str to cap 2nd letter
//          record in findSellers() when email is updated.
// 5.3.36 - updated concludePP() to set Options::ListingsMostViewed array.
// 5.3.37 - use Options::AgentAcronymMakeUppercase to get the array of acronyms to make upper case.  If not there, will use some default values.
// 5.3.38 - added Options::AgentAcronymDoubleCheck to make sure that acronym isn't someone's last name.
// 5.3.39 - added code to convert single quotes to double quotes for AgentAcronymMakeUppercase and AgentAcronymDoubleCheck, so that json_decode() works.
//          call setMiscOptions() in perform().
// 5.3.40 - added calls to new class Process, to call restartSQL from writeLogHeaderForParallelFile() before doing a regular parse.
//          the frontend will now call do-back on ajax-developer.php and start doing back up after the parse and image processing is completed, if
//          the Options::BACKUP_AFTER_FULL_PARSE is set to 1.
// 5.3.41 - added more precise logging for formatName(). also fixed llke3 to like3.
//          fixed Base::formatWhereClause to have the 'or' in it's $clause_type.
// 5.3.42 - use LISTING_ADDRESS_HAS_UNIT_NUMBER to flag listings with unit number, but don't flag it as a condo just yet.  Let the
//          ListingTitle, PropertyType, PropertySubType and ListHubMatchTag class deal with it.  After parsing one listing, check the
//          LISTING_ADDRESS_HAS_UNIT_NUMBER and if house is not set yet, if 'gated community' is set, set 'house' and unset 'condo' if it's set.
//          if 'gated community' is not set, and lotsize is 0, then set 'condo' if it's not already
// 5.3.43 - Fixed usage for Base::formatWhereClause() use of 'or' and construction of 'or' in findSeller().
// 5.3.44 - added to seller info, ParticipantKey to be saved as participant_key and used in findSeller if exists.
//          changed how NOIMAGE is applied in bailOutListing() to use a foreach() statement instead of the while() that was being used.
// 5.3.45 - added group www-data to sudoers file for running service.  now calling restartSQL should work.
// 5.3.46 - use Seller meta data - SELLER_ALTERNATE_LISTHUB_DATA, to record variations in seller info that uses the same participant_key.
//          seller's flag will be set for SELLER_HAS_ALTERNATE_LISTHUB_DATA if this meta data is added to the seller for easier identification.
// 5.3.47 - fixed code in findSeller() and updateAlternateListhubData() so that data is properly handled and saved to database.
// 5.3.48 - added improved facility to clear out image folders from orphan images, freeing up disk space
// 5.3.49 - changed deleteImagesNotFlaggedAsKeeper() to always use $page = 0, since every iteration will see a new table, so keep taking it off
//          the front until there are no more.
// 5.3.50 - replace CLEAN_IMAGE_UPDATE with CLEAN_FOLDER_UPDATE in removeOneListing().
// 5.3.51 - fix usage of 'tier'.  By setting all listings to tier 1 in writeLogHeaderForParallelFile(), it essentially updates the row and the calculation of 
//          the listing expiration should be wrong every time.  There are over 143K tier 0 listings. Should only be about 2x daily intake...
//          so in createListing(), we set the tier to 1 for new listings, and update to 1 if pre-existing listing has none or is set to 0.
// 5.3.52 - after thinking about the 5.3.51 logic, we still needed to reset the tier 1's to 0's, but only selectively, using WHERE tier = 1, instead 
//          of WHERE 1.  So those with tier 0 already won't get their 'updated' column affected.
// 5.3.53 - using Options::RunPPAsBatch to have external control over how parallel jobs are run.
// 5.3.54 - added ability to detect huge image files and downsize them into usable sizes and keep a 0-size file in the uploaded folder so that the
//          file referencing code works as expected.  Set Options::ProcessHugeImageFile to 1, to process the huge files.  If set to 0, will do nothing
//          with them.
// 5.3.55 - added code in writeLogHeaderForParallelFile() to stall restaring SQL if there are any Quiz pending or processing for up to 120 secs..
// 5.3.56 - fixed 5.3.55 to separate the one count() call usng the 'or' clause, as we cannot have two array elements using the same index ('status'),
//          into two count() calls using a single 'where' clause of 'status'=>'processing' and 'pending'.
// 5.3.57 - changed up how the minimumAcceptedPrice is used initially in parseOneListing(), so that it is adjusted for home/condo.  If home, then use
//          the value, but if condo, multiply by 0.60.  Then, multiply that with medianPriceOffset to get the actual allowable minimum price to consider
//          outright rejection of the listing.
// 5.3.58 - added ListingCategory to consider, and if it is "Rent", then mark listing as RENTAL, and reject it, regardless of price.
// 5.3.59 - use RENTAL to filter out listings in MedianPrices::getMedianPrices(), so it doesn't update the stats unncessarily.
// 5.3.60 - Listing::geocodeListing() wasn't stripping off everthing after '#' in the street_address if there any space after it.  Changed it so the
//          rest gets stripped of regardless.
// 5.3.61 - Added to Cities::geocodeCity() a non-conformace check before calling google geocoder.
// 5.3.62 - added more code for assigning city geocode to listing if listing cannot be geocoded properly.
// 5.3.63 - updated Listing::geocodeListing() to account for 'street_number' and 'route' instead of 'street_address'.
//          updated createGeoCode() to update existing row's address if needed also.
// 5.3.64 - updated Listing::geocodeListing() to use BingLocation to try to see if it can geocode the address.
// 5.3.65 - updated Listing::geocodeListing() to allow more broad hit locations (locality, natural_feature, postal_code, PopulatedPlace)
// 5.3.66 - separate minimumPriceOffset, medianPriceOffset, medianPriceOffsetCondo values
// 5.3.67 - created ImageBlocks and ImageChunkStatus tables so that image processing indices are now separate from listing ids and we can do
//          both at the same time.
// 5.3.68 - flag ImageBlocks as they complete, so if rerun, they can be skipped.
// 5.3.69 - changed readImage() to be able to use curl or readfile(), by now use file_get_contents() by default.
// 5.3.70 - pass context to file_get_contents() - 'header'=>'Connection: close\r\n'
// 5.3.71 - findSeller() will use likeonlycase clause for email searches.  The Base::formatWhereClause() handles all 'like' types with
//          the LIKE = '%<something>%', so anything will match if it has matching <something>.  We don't want that.  The 'likeonlycase' will use LIKE = 'xxxx', so  only the case won't matter, 
//          but won't do the % thing.
// 5.3.72 - use IMAGE_BORDER_PROCESSING_STAT to record how IP is going along.  FE will look at that and if perImage rate is > ACCEPTABLE_PER_IMAGE_RETRIEVAL_RATE (3.5 secs),
//          then it will stop sending chunks over until that rate speeds up.
// 5.3.73 - corrected usage of IMAGE_BORDER_PROCESSING_STAT so that the proper perImage value is sent.  Also added $force to updateToDb() so that even if doing a batch job,
//          the IMAGE_BORDER_PROCESSING_STAT is recorded right away.
// 5.3.74 - pass the $force value to updateProgress() from updateToDb().
// 5.4.00 - introduced doOneCustomImageTask() to deal with special operation type: IMAGE_BORDER_PARALLEL_CUSTOM_TASK.
//          the idea here is to have a doOneCustomImageTask() that will be called from processOneImageTask() instead of fixOneImageBorder() to do any custom image
//          task we need.  The first one is to revist every processed image and regen jpegs at 80% quality, reset first_image and also to create a new list_image
//          entry that has limited sizes for every first image we want to show in the quiz-results page (for both main list view and sublisting view in a city view)
//          also introduce a new RESTART_PARALLEL_IP for recording the parameters of the parallel processing to differentiate it from RESTART_PARALLEL, the regular parse.
//          so pausePP, resumePP haltPP, findStartOfLastPPRecord, writeLogHeaderForParallelFile, getLastStartEntry will all trigger off parsing type.
//          the front-end has been updated to deal with the new scheme.
// 5.4.01 - updated getListHubData() to check Options::LastListHubDataFeedRead, and if it is less than 4 hours since last read, reuse data
// 5.4.02 - fixes for generating list_image
// 5.4.03 - Image class now incorporates Imagick class if imagecreatefromjpeg() fails to deal with the input.
//          add LastName to xml entry that gets special slashes treatment.
// 5.4.04 - changed findSellers() to use likeonlycase for city names.
//          in fixOneImageBorder(), stall for a maximum of 5 secs if the retrieval time per image is greater than DELAY_NEXT_IMAGE_RETIEVAL_IF_PER_IMAGE_IS_OVER_THIS.
// 5.4.05 - fixed usage of LastListHubDataFeedRead in getListHubData().
// 5.4.06 - fixed typo in fixOneImageBorder() for $needUpdate when resizing was being done.
//          also use require_once() for States.php
// 5.4.07 - added code to record ListingKey during feed parse.
// 5.4.08 - added sysdown and sysup process calls to optimizeTable() made internally
// 5.4.09 - use imagedestroy() instead of unset() for $img in doOneImageBorder();
// 5.4.10 - use MINIMUM_IMAGE_WIDTH set to 580 to decide worthiness of image.  was 600, but by shaving 20, get more listings.
// 5.4.11 - updated array_isDifferent() to check for !isset($ar2[$k])
// 5.4.12 - updated code to handle array_isDifferent() properly and set the $diff[] array.
// 5.4.13 - reduced MINIMUM_IMAGE_WIDTH to 256
// 5.4.14 - fix processOneChunk() to work properly.
// 5.4.15 - clean up image processing code, account for empty($image->file).
// 5.4.16 - added code to have a separate chunk and status tables for custom image processing.
// 5.4.17 - fixed how makeListImage() and fixOneImageBorder() handles the list_image.  makeListImage() did not make sure that
//          the uploaded folder has a valid entry.  fixOneImageBorder() now does a sanity check to make sure there are actual files
//          for the list_image, and recreate if needed or touch uploaded folder with the filename so an entry exists.
// 5.4.18 - added runScriptOnWeb01() to account for the fact that this can be running on a remote server (engr1).
//          if running on engr1, copy the listhub csv.gz file from  remote to web01s' /var/www/html/lifestyledlistings/listhub folder
// 5.4.19 - fixed openDebugFile() account for value 5, listhubImageCustomPP.  added code to doOneCustomImageTask() to retire images if no file nor url exists.
// 5.4.20 - stop optimizing QuizActivity and QuizQuery tables.
// 5.4.21 - added code to processBlocks() to record to ListhubBlocks, the flag of 1 when that block had been processed, much like ImageBlocks and CustomImageBlocks
//          so that restarting the PP won't reprocess that block again.
// 5.4.22 - Arggghhh!!! had _Base handling of 'or' all wrong!  Fixed it... again..  findSellerHelper() now uses the correct syntax and also switches the phone/mobile 
//          to see if phone #'s match complementarily
// 5.4.23 - update findSeller() to make sure the seller's state is the abbreviated form.
// 5.4.24 - updated writeLogHeaderForParallelFile() to use resetMedianAndTiers to determine whether to reset median and tier tables.  If false, then
//          leave alone, so that during cleanup, listings that were already processed doesn't get demoted to INACTIVE.
// 5.4.25 - added sendFlagToRemote() to send data to remote, and updated concludePP($mode, $ppRunId, $optimizeTable = true) so FE can control table optimization manually if it chose to.
// 5.4.26 - in getListHubData(), check to see that the $tempFile exists after reading into the gz file.
//          in saveToDb() try to open the xml file twice before giving up
// 5.4.27 - updated popen(rm) calls for image processing to explicity say /tmp/alr*.img and /tmp/alr?????? instead of alr*, so it doesn't accidentally erase the listhub feed file
//          if the feed was being read in.
// 5.4.28 - updated formatName() to take $isCity parameter and use that to fix "St, St., and Saint" cities to all be "St.", and also
//          account for city mispelling of 'Island' as 'Islan', and concatenation of 'St.', as in 'St.matthews' and such.
// 5.4.29 - updated formatName() to account for 'Saint' not being the first word of the city.  If not, leave it as is, i.e.: Port Saint Lucie.
//          Also, if the St or St. appears in not the first word, change it to Saint.
// 5.4.30 - added code to check for CURL getting little to no response for 30 secs.  If so, abort and retry, but to three times.
// 5.4.31 - took out usage for getCitiesTags() and mergeCityTags().  page-listing.php will now get the city tags and merge them with the
//          listing tags.  Also added triggerCities[] to prepare for doing the trigger tags latger.
// 5.4.32 - prevent recursed call of getListHubData() from emptying the ListHubProgress table.
//          added code for Options::SaveClickedListings, so if it's 1, then we save all clicked-on listings, so the portal analytics remains true.
// 5.4.33 - updated addslashes() to take $find and $replace arguments.  If present, they will be used in str_replace(), i.e.: The '&' symbol in ListingDescription element
//          need to be changed to 'and'.
//          updated ListHubMatchTags class to set trigger flags for cities, and added code here to update the city tags as needed in processTriggers().
//          writeLogHeaderForParallelFile() will now use the $resetMedianAndTiers to decide whether to rotate debug/log files and create new ones.
// 5.4.34 - changed getListHubData() to exitNow() immediately.
// 5.4.35 - added okToEmptyProgressTableBeforeIP flag for debugging to preserve the progress logs, defaults to true, set Options::OkToEmptyProgressTableBeforeIP to 0 to keep logs.
// 5.4.36 - added calls to fix2CharCity(), fixCity(), and fixNY() before calls to matchCity() and in findSellers().
//          fixCity() now address Taos, NM
// 5.4.37 - record FAILED_PARSE_TO_DB upon failure to read the listhub feed, so FE can retry
//          added Poughkeepsie, NY to fixCity() list
// 5.4.38 - update parseXml() to use addslashes() for '&amp;' also and replace with 'and'
// 5.4.39 - fixCity() for 'New York Ny' and 'New York City'
//        - added ListingsViewed entry to be removed in removeOneListing()
// 5.4.40 - added to checking LISTING_MODIFIED_DATA fields: about, lotsize, lotsize_std, interior, interior_std, street_address
// 5.4.41 - updated updateTags() to only add/remove tags as needed.
//          updated createListings() to check preExisting listing, if there, to see about merging exiting tags with ones found, and possibly undoing the LOWTAGS
//          and LACK_DESC errors.
// 5.4.42 - in updateTags(), call strtolower() when organizing existing tags.
// 5.4.43 - updated Listings::geocodeListing() to take a Cities object, and it will use it to see if the listing's city exists and it has a valid lng/lat.
//          and if it doesn't exist or have a valid lng/lat, it will try the geocoding with just the state and ZIP.
// 5.4.44 - updated bailOutListing() so if there is at least one tag and 'about' is more than 80 characters, don't set the LACK_DESC error.
// 5.4.45 - updated doOneChunk() to set ListhubBlocks Table's flag value to 1 when done.
// 5.4.46 - fully process commercial and rental properties
// 5.4.47 - fixed parseOneListing() so that it wasn't throwing any more exceptions when it was trying to create an ArrayObject using non-existant $listing['tags']
// 5.4.48 - use $this->recordListHubKey to control whether to dump out ListHubKey's during parse only run
// 5.4.49 - added listing_status as tinytext to the listing information saved
// 5.4.50 - added try/catch in processBlocks() when adding to ChunkStatus table, and record the event
// 5.4.51 - updated processBlocks() to consider a situation where a chunk status is now owned by a different subdomain, and one entry with the same startid 
//          already exists in the DB.  So in that case, set the subdomain value to the incoming SubDomain's id, so we don't get a DB error for INSERT INTO with
//          a pre-existing startid error.  Also, in 'check-chunk' in ajax-developer.php, if a chunk can't be found with subdomain and startid, just use startid to
//          find it if was moved to another SubDomain.
// 5.4.52 - Fixed ListHubMatchTags class's constructor so that it is properly setting the tags to lower case.  Because it wasn't, ski front homes weren't tagged.
// 5.4.53 - changed geocoding limiter so that if $listing->active > AH_INACTIVE && <= AH_REJECTED, it will geoCoode.  The AH_REJECTED will be increased to eventually
//			include all listings.
// 5.4.54 - save OfficeEmail in seller::office_email column.
// 5.4.55 - updated findSeller() to update office_email for seller.
// 5.4.56 - if doExtraGeoCoding but extraGeoCodingMask == 0, then set doGeoCodingAnyway to true, to do all geocoding.
// 5.4.57 - oops, wrong ideal with 5.4.56.  Use Options::GeoCodeAllMaskType set to 1, if we want to do all listings that match the mask in a block, instead of just one.
// 5.4.58 - added LegalDescription to addslashes()/stripslashes() group of tags to sanitize, created $xmlTagsToClean[] to group them together.
//        - fixed findSeller(), when updating the seller, create a new $q, so that it's not loaded with baggage that won't work with Base::set().
// 5.4.59 - fixed how Cities::geocodeCity() works.  It compares lowercased strings, uses 'sublocality' type, and also levenshtein() < 3 to catch a two letter
//          difference in city names.  Utility::fixCityStr() now accounts for city starting with Mcxxxx so it becomes McXxxxxx.
//			added triggers field to the Listings table, as this was causing Base::add() failures since it was not an expected field.  triggers got passed
//          through in part because matchCity() failed for the above reasons for geocoding.
// 5.5.00 - added mechanism to update city and listing tags for new cities and listings from the TagSpace table's area boundary based tags.
//        - also from the admin->developers page, you can choose to run the TagSpace tags over all existing cities and listings also.
// 5.5.01 - when creating $existingTagList in updateListingFromTagSpace() and updateCityFromTagSpace(), use the tag_id value instead of id since
//          these values are from ListingsTags and CitiesTags, and not Tags.
//
define("PARSER_VERSION", "5.5.01");

define('SPECIAL_OP_NONE', 0);
define('IMPORT_ACTIVE_LISTINGS_IMAGE', 1);
define('CREATE_IMAGES_FROM_IMPORTED', 2);
define('IMAGE_BORDER_PARALLEL', 3);
define('IMAGE_BORDER_PARALLEL_AFTER_PARSE', 4);
define('IMAGE_BORDER_PARALLEL_REMOTELY', 5);
define('IMAGE_BORDER_PARALLEL_CRON_ALL', 6);
define('IMAGE_BORDER_PARALLEL_CRON_FROM_LAST', 7);
define('IMAGE_BORDER_PARALLEL_CUSTOM_TASK', 8); // use this to traffic IP tasks that are possibly one-of's
define('IMAGE_BORDER_PARALLEL_BATCH', 10);
define('SPECIAL_PARSE_BATCH', 20);
define('USER_ABORTED_PP', 99);

define('POST_FROM_FE', 300);

define('CHUNK_START', 1);
define('CHUNK_PROCESSED', 2);
define('CHUNK_ERROR', 3);
define('CHUNK_FATAL', 4); // completely give up on it... could not recover within chunk size * 1 min

define('MINIMUM_IMAGE_WIDTH', 256);

// this is for the list_image column of Listings, to produce a selected set of sizes for viewing in the quiz-results page
global $sizes;
$sizes = array(
			array('dir'=>'845x350','width'=>845,'height'=>350, 'active' => 1), // quiz results + setup wizard bg images
			array('dir'=>'210x120','width'=>210,'height'=>120, 'active' => 1) // listing gallery thumbs
		 );

global $xmlTagsToClean;
$xmlTagsToClean = [['tag'=>'LastName',
					'find'=>null,
					'replace'=>null],
				   ['tag'=>'ProviderName',
					'find'=>null,
					'replace'=>null],
				   ['tag'=>'ListingDescription',
					'find'=>"andamp;",
					'replace'=>'and'],
				   ['tag'=>'ListingDescription',
					'find'=>"&amp;",
					'replace'=>'and'],
				   ['tag'=>'ListingDescription',
					'find'=>"&",
					'replace'=>'and'],
				   ['tag'=>'Offices',
					'find'=>null,
					'replace'=>null],
				   ['tag'=>'Brokerage',
					'find'=>null,
					'replace'=>null],
				   ['tag'=>'Directions',
					'find'=>null,
					'replace'=>null],
				   ['tag'=>'LegalDescription',
					'find'=>null,
					'replace'=>null],

];

class ListHubData extends Controller {
	private $parseRules = array(
		'ListPrice' => array('listing', 'price'),
		'Bedrooms'=> array('listing', 'beds'),
		'Bathrooms'=> array('listing', 'baths', 1),
		//'FullBathrooms'=> array('listing', 'baths', 1),
		'FullBathrooms'=> array('listing', 'func'=>0), // add func in constructor
		'ThreeQuarterBathrooms'=> array('listing', 'baths', 0.001),
		'HalfBathrooms'=> array('listing', 'baths', 0.001),
		'OneQuarterBathrooms'=> array('listing', 'baths', 0.001),
		'PartialBathrooms'=> array('listing', 'baths', 0.0),
		'LivingArea' => array('listing', 'interior'),
		'LotSize' => array('listing', 'lotsize', 'prefix' => 'commons', 'func'=>0), // add func in constructor
		//'LotSize' => array('listing', 'lotsize'),
		'ListingKey' => array('listing', 'func'=>0), // add func in constructor
		'ListingStatus'=> array('listing', 'active'),
		'ListingDescription' => array('listing', 'func'=>0), // add func in constructor
		'ListingTitle' => array('listing', 'func'=>0), // add func in constructor // 'title'),
		'ListingURL' => array('listing', 'url'),
		'YearBuilt'=> array('listing', 'func'=>0), // add func in constructor
		'MlsNumber' => array('listing', 'func'=>0), // add func in constructor
		'Disclaimer' => array('listing', 'disclaimer'),
		//'MlsNumber' => array('listing', 'mls'),
		'Offices' => array('primary' => 'seller',
							'Office' => 1),
		'Office' => array('primary' => 'seller',
						'OfficeKey' => 0,
						'OfficeId' => 0,
						'Level' => 0,
						'OfficeCode' => 0,
						'OfficeEmail' => array( 'seller', 'office_email'),
						'Name' => array('seller','company'),
						'CorporateName' => 0,
						//'CorporateName' => array('seller','company', ', parent company: '),
						'BrokerId' => 0,
						'PhoneNumber' => array('seller','func'=>0),// add func in constructor	
						'Address' => array( 'primary' => 'seller',
											'prefix' => 'commons',
											'preference-order' => 0,
											'address-preference-order' => 0,
											'FullStreetAddress' => array('seller', 'street_address'),
											'UnitNumber' => array('seller','street_address',', #'),
											'City' => array('seller', 'city'),
											'StateOrProvince' => array('seller', 'state'),
											'PostalCode' => array('seller', 'zip'),
											'Country' => array('seller', 'country') ),
						'Website' => 0),

		'Photos' => array('primary' => 'listing',
						'retainIndex',
						'Photo' => 1),
		'Photo' => array('MediaModificationTimestamp' => 0,
						'ModificationTimestamp' => 0,
						'MediaURL' => array('listing','func'=>0), // add func in constructor
						'MediaCaption' => array('listing','func'=>0), // add func in constructor
						'MediaDescription' => array('listing','func'=>0)), // add func in constructor
		'ListingCategory'=> array('listing', 'func'=>0), // add func in constructor
		'PropertyType'=> array('listing', 'func'=>0), // add func in constructor
		'PropertySubType'=> array('listing', 'func'=>0), // add func in constructor
		'Videos' => array('primary' => 'listing',
						'retainIndex',
						'Video' => 1),
		'Video' => array('MediaModificationTimestamp' => 0,
						'ModificationTimestamp' => 0,
						'MediaURL' => array('listing','func'=>0), // add func in constructor
						'MediaCaption' => array('listing','func'=>0), // add func in constructor
						'MediaDescription' => array('listing','func'=>0)), // add func in constructor			
						
		'Brokerage' => array('primary' => 'seller',
						'Name' => 0,
						'Phone' => array('seller','func'=>0),// add func in constructor	
						'Email' => array('seller','func'=>0),// add func in constructor	
						'WebsiteURL' => array('seller','func'=>0),// add func in constructor	
						'LogoURL' => 0,
						'Address' => 0),

		'ListingParticipants' => array('primary' => 'seller',
									'Participant' => 1),

		'Participant' => array('ParticipantKey' => array('seller','participant_key'),
								'ParticipantId' => 0,
								'FirstName' => array('override' => 1),
								'LastName' => array('override' => 1),
								'Role' => 0,
								'PrimaryContactPhone' => array('seller','mobile'),
								'OfficePhone' => array('override' => 1),
								'Email' => array('override'=>1),
								'Fax'=> 0,
								'WebsiteURL' => array('override'=>1)),


		'Phone' => array('override'=> array('any','phone')), // this so that override works
		'FirstName'=> array('override'=> array('any','first_name')), // this so that override works
		'LastName'=> array('override'=> array('any','last_name')), // this so that override works
		'OfficePhone' => array('override'=> array('any','phone')), // this so that override works
		'Email' => array('override'=> array('any','email')), // this so that override works
		'WebsiteURL' => array('override'=> array('any','website')), // this so that override works


		'Address' => array( 'primary' => 'listing',
							'prefix' => 'commons',
							'preference-order' => 0,
							'address-preference-order' => 0,
							// 'FullStreetAddress' => array('listing', 'street_address'),
							'FullStreetAddress' => array('listing', 'func'=>0),// add func in constructor	
							// 'UnitNumber' => array('listing','street_address',', #'),
							'UnitNumber' => array('listing','func'=>0),// add func in constructor	
							'City' => array('listing', 'city'),
							'StateOrProvince' => array('listing', 'state'),
							'PostalCode' => array('listing', 'zip'),
							'Country' => array('listing', 'country') ),

		'MarketingInformation' => array(
							'primary' => 'listing',
							'prefix' => 'commons',
							'PermitAddressOnInternet'=> array('listing','func'=>0),// add func in constructor	
							'VOWAddressDisplay'=> 0,
							'VOWAutomatedValuationDisplay' => 0,
							'VOWConsumerComment' => 0),

		//'AlternatePrices' => array('AlternatePrice'),
		//'AlternatePrice' => array('AlternateListPrice','AlternateListPriceLow'),
		//'MarketingInformation' => array('prefix' => 'commons','commons:PermitAddressOnInternet','commons:VOWAddressDisplay','commons:VOWAutomatedValuationDisplay','commons:VOWConsumerComment'),
		//'Photos' => array('Photo');
		//'VirtualTour' => array('MediaModificationTimestamp','MediaURL','MediaCaption','MediaDescription'),
		//'Offices' => array('Office'),
		//'OfficeCode' => array('OfficeCodeId'),
		//'Franchise' => array('Name'),
		//'Builder' => array('Name','Phone','Fax','Email','WebsiteURL','Address'),
		'Location' => array('primary' => 'geoInfo',
							'Latitude' => array('geoInfo','lat'),
							'Longitude' => array('geoInfo','lng'),
							'Elevation' => 0,
							'Directions' => 0,
							'GeocodeOptions' => 0,
							'County' => 0,
							'ParcelId' => 0,
							'Community' => 0,
							'Neighborhoods' => 0),
		//'Community' => array('commons:Subdivision','commons:Schools'),
		//'commons:Schools' => array('common:School'),
		//'commons:School' => array('commons:Name','commons:SchoolCategory','commons:District','commons:Description'),
		//'Neighborhoods' => array('Neighborhood'),
		//'Neighborhood' => array('Name','Description'),
		//'OpenHouses' => array('OpenHouse');
		//'OpenHouse' => array('Date','StartTime','EndTime','Description'),
		//'Taxes' => array('Tax'),
		//'Tax' => array('Year','Amount','TaxDescription'),
		//'Expenses' => array('Expense'),
		//'Expense' => array('commons:ExpenseCategory','commons:ExpenseValue'),
		'ExteriorTypes' => array('primary'=>'listing',
								'ExteriorType' => array('listing',
														'func' => 0)),// add func in constructor	
		'Rooms' 		=> array('primary'=>'listing',
								'Room' => array('listing',
												'func' => 0)),// add func in constructor	
		'ViewTypes' 	=> array('primary'=>'listing',
								'ViewType' => array('listing',
												'func' => 0)),// add func in constructor	

		'DetailedCharacteristics' => array(
							'primary' => 'listing',
							'func' 				=> 0,// add func in constructor	
							'Appliances'		=> 0,
							'ArchitectureStyle' => 2, // not just process, use parent func
							'HasAttic' 			=> 0,
							'HasBarbecueArea' 	=> 0,
							'HasBasement' 		=> 0,
							'BuildingUnitCount' => 0,
							'IsCableReady' 		=> 0,
							'HasCeilingFan' 	=> 0,
							'CondoFloorNum' 	=> 0,
							'CoolingSystems' 	=> 0,
							'HasDeck' 			=> 0,
							'HasDoorman' 		=> 2,
							'HasDoublePaneWindows' => 2,
							'HasElevator' 		=> 0,
							'ExteriorTypes' 	=> 1,
							'HasFireplace' 		=> 2,
							'FloorCoverings' 	=> 0,
							'HasGarden' 		=> 0,
							'HasGatedEntry' 	=> 2,
							'HasGreenhouse' 	=> 2,
							'HeatingFuels' 		=> 0,
							'HeatingSystems' 	=> 0,
							'HasHotTubSpa' 		=> 2,
							'Intercom' 			=> 0,
							'HasJettedBathTub' 	=> 2,
							'HasLawn' 			=> 0,
							'LegalDescription' 	=> 0,
							'HasMotherInLaw' 	=> 2,
							'IsNewConstruction' => 2,
							'NumFloors' 		=> 0,
							'NumParkingSpaces' 	=> 2,
							'HasPatio' 			=> 0,
							'HasPond' 			=> 0,
							'HasPool' 			=> 2,
							'HasPorch' 			=> 0,
							'RoofTypes' 		=> 0,
							'RoomCount' 		=> 0,
							'Rooms' 			=> 1,
							'HasRVParking' 		=> 0,
							'HasSauna' 			=> 2,
							'HasSecuritySystem' => 0,
							'HasSkylight' 		=> 0,
							'HasSportsCourt' 	=> 2,
							'HasSprinklerSystem'=> 0,
							'HasVaultedCeiling' => 2,
							'ViewTypes' 		=> array('primary' => 'listing',
														'ViewType' => 1),
							'IsWaterfront' 		=> 2,
							'HasWetBar' 		=> 2,
							'IsWired' 			=> 2,
							'YearUpdated' 		=> 2,
							'HasDock'         	=> 2,
							'HasDisabledAccess'	=> 0)
	);
	private $ignoreList = array(
		//'MarketingInformation',
		'DiscloseAddress',
		'ListPriceLow',
		'AlternatePrices',
		'ForeclosureStatus',
		//'ListingURL',
		'ProviderName',
		'ProviderURL',
		'ProviderCategory' ,
		'LeadRoutingEmail',
		'Appliances',
		'Builder',
		//'PropertyType','PropertySubType',
		// 'ListingCategory',
		'ListingDate',
		'MlsId',
		'MlsName',	
		'Franchise','OpenHouses',
		'VirtualTours',		
		//'Disclaimer',	
		'Taxes','Expenses',
		'ModificationTimestamp',
		'Licenses',
		'MainOfficeId',
		'Fax',
		// 'OfficeEmail',
		'ParkingTypes',
		'Zoning'
	);

	const UPDATE = 1;
	const RESULT = 2;
	const ERROR  = 3;
	const FATAL  = 4;
	const RESULT_PARALLEL = 5;
	const RESTART_PARALLEL = 6;
	const BEGIN_PARALLEL = 7;
	const DENY_PARALLEL = 8;
	const REPEATING_PARALLEL = 9;
	const ALIEN_RUNID = 10;
	const END_PARALLEL = 11;
	const PAUSE_PARALLEL = 12;
	const RESUME_PARALLEL = 13;
	const HALT_PARALLEL = 14;
	const REMOTE_PARALLEL = 15;
	const REMOTE_P2DB     = 16;
	const RELOAD_PAGE     = 17;
	const WRITE_CSV_GZ    = 18;
	const RECORD_LISTHUB_KEY = 19;
	const LOCK_SQL = 20;
	const UNLOCK_SQL = 21;
	const UNUSED_22 = 22;
	const CLEAN_FOLDER_UPDATE = 23;
	const CLEAN_SQL_TABLE = 24;
	const LISTHUB_PARSE_RESULT = 25;
	const BEGIN_UPDATE_EMAIL_DB = 26;
	const PAGE_START_UPDATE_EMAIL_DB = 27;
	const PAGE_END_UPDATE_EMAIL_DB = 28;
	const END_UPDATE_EMAIL_DB = 29;

	const IMAGE_BORDER_START = 30;
	const IMAGE_BORDER_LISTING_START = 31;
	const IMAGE_BORDER_LISTING_DONE = 32;
	const IMAGE_BORDER_ERROR = 33;
	const IMAGE_BORDER_END = 34;
	const IMAGE_BORDER_CMD_RECEIVED = 35;
	const IMAGE_BORDER_GROUP = 36;
	const IMAGE_BORDER_UPDATE = 37;
	const IMAGE_BORDER_UPDATE_LAST_ID = 38;
	const IMAGE_BORDER_HTTP_HAD_LOCAL_IMAGES = 39;
	const IMAGE_BORDER_ERROR_FATAL = 40;
	const IMAGE_BORDER_LISTING_UPDATED = 41;
	const IMAGE_BORDER_HUGE_FILE = 42;
	const IMAGE_BORDER_PROCESSING_STAT = 43;
	const IMAGE_BORDER_NO_FILE = 44;
	const IMAGE_BORDER_BAD_DEST_MAKING_LIST_IMAGE = 45;
	const IMAGE_BORDER_MADE_LIST_IMAGE = 46;
	const IMAGE_BORDER_ASSOC_LIST_IMAGE = 47;
	const IMAGE_BORDER_FAILED_TO_CREATE_BASENAME = 48;
	const IMAGE_BORDER_REGEN_OK = 49;
	const IMAGE_BORDER_CREATE_LIST_IMAGE_UPLOADED_FILE = 50;
	const IMAGE_BORDER_TOUCH_LIST_IMAGE_UPLOADED_FILE= 51;
	const IMAGE_BORDER_MODIFIED_12= 52;
	const IMAGE_BORDER_MODIFIED_13= 53;
	const IMAGE_BORDER_MODIFIED_14= 54;
	const IMAGE_BORDER_MODIFIED_15= 55;
	const IMAGE_BORDER_GETBASENAME_ERROR = 56;
	const IMAGE_BORDER_MODIFIED_17= 57;
	const IMAGE_BORDER_FAILED_GET_COuNT = 58;
	const IMAGE_BORDER_FAILED_CREATE_IMAGE_JPEG = 59;

	const KEYWORD_START = 60;
	const KEYWORD_LISTING_START = 61;
	const KEYWORD_LISTING_DONE = 62;
	const KEYWORD_ERROR = 63;
	const KEYWORD_END = 64;
	const KEYWORD_CMD_RECEIVED = 65;
	const KEYWORD_GROUP = 66;
	const KEYWORD_MATCHED = 67;
	const KEYWORD_RESET_COUNT = 68;

	const UNTAG_START = 80;
	const UNTAG_LISTING_START = 81;
	const UNTAG_LISTING_DONE = 82;
	const UNTAG_ERROR = 83;
	const UNTAG_END = 84;
	const UNTAG_CMD_RECEIVED = 85;
	const UNTAG_GROUP = 86;
	const UNTAG_MATCHED = 87;

	const EMAIL_DB_ADDED = 100;
	const EMAIL_DB_UPDATED = 101;

	const IMAGE_IMPORT_ERROR = 120;
	const IMAGE_IMPORT_LISTING_DONE = 121;

	const IMAGE_GEN_IMAGE_ERROR = 130;
	const IMAGE_GEN_FILE_URL_BUT_HAVE_LOCAL = 131;
	const IMAGE_GEN_IMAGE_FETCHED_URL = 132;
	const IMAGE_GEN_IMAGE_LOCAL_EXISTS = 133;
	const IMAGE_GEN_IMAGE_LISTING_DONE = 134;

	const REMOTE_IMAGE_PROCESSING = 140;
	const CLEAN_IMAGE_INACTIVE = 141;

	const CONCLUDE_PP_MEDIAN_VALUES_CONSOLIDATE_DAILY = 150;
	const CONCLUDE_PP_OPTIMIZE_TABLES = 151;
	const CONCLUDE_PP_TAG_SPACE_CHECK_LISTING = 152;
	const CONCLUDE_PP_TAG_SPACE_CHECK_CITY = 153;

	const WRITE_LOG_HEADER_START = 160;
	const WRITE_LOG_HEADER_RESET_DAILY_MEDIAN_VALUES = 161;
	const WRITE_LOG_HEADER_UPDATE_TIERS = 162;
	const WRITE_LOG_HEADER_DONE = 163;
	const WRITE_LOG_HEADER_PRESTART_SQL = 164;
	const WRITE_LOG_HEADER_EMPTY_TABLE = 165;
	const WRITE_LOG_HEADER_UPDATE_BLOCK_FLAGS = 166;

	const PROCESS_EXEC_START = 170;
	const PROCESS_EXEC_END = 171;
	const PROCESS_EXEC_OUTPUT = 172;
	const PROCESS_EXEC_SCRIPT_START = 173;
	const PROCESS_EXEC_SCRIPT_END = 174;
	const PROCESS_EXEC_SCRIPT_OUTPUT = 175;

	const CLEAN_IMAGE_START = 180;
	const CLEAN_IMAGE_UPDATE = 181;
	const CLEAN_IMAGE_END = 182;
	const CLEAN_IMAGE_INIT_COUNT = 183;
	const CLEAN_IMAGE_REMOVE_COUNT = 184;
	const CLEAN_IMAGE_ERROR = 185;
	const CLEAN_IMAGE_MARK_IT = 186;
	const CLEAN_IMAGE_REMOVE_IT = 187;
	const CLEAN_IMAGE_RESET_IMAGES = 188;

	const RESTART_PARALLEL_IP = 190;
	const RESTART_PARALLEL_CUSTOM_IP = 191;

	const PING_REMOTE = 200;
	const STATS_REMOTE = 201;
	const FLAGS_TO_REMOTE = 202;

	const POST_FROM_FE = 300;
	const FAILED_PARSE_TO_DB = 301;
	const INCONSISTENT_SUBDOMAIN_ID = 302;

	// this is set to the image size in quiz-results cities and listing views
	const MAX_IMAGE_WIDTH = 845;
	const MAX_IMAGE_HEIGHT = 350;

	const DELAY_NEXT_IMAGE_RETIEVAL_IF_PER_IMAGE_IS_OVER_THIS = 3.5;

	public function __construct() {
		parent::__construct();

		require_once(__DIR__.'/States.php');
		//require_once(__DIR__.'/../_classes/Tables.class.php');
		//$this->Tables = new AH\Tables();
		if (!defined('SHORTINIT')) define( 'SHORTINIT', true ); // don't load all of Wordpress
			require_once(__DIR__.'/../../../../wp-load.php');
		global $usStates;
		
		$this->debugLevel = 3;
		$this->haveOutputFile = true;
		$this->saveLogToDb = false;
		$this->index = -1;
		// get this timezone from Controller now
		// global $timezone_adjust;
		// $this->timezone_adjust = $timezone_adjust;
		$this->listingLimit = '*'; /* use '*' for all */
		$this->decompressIt = false;
		$this->saveDecompressedFile = true;
		$this->justSavePhotUrl = true;
		$this->urlFile = null;
		$this->minimumPrice = 800000;
		$this->baseConsideredPrice = DEFAULT_MINIMUM_PRICE * BASE_CONSIDERED_PRICE_MULTIPLER;
		$this->minimumTagCount = 4;
		$this->saveListingToDb = false;
		$this->listingsPerEntry = 200;
		$this->listingIdRange = null;
		$this->doingParallel = false;
		$this->maxPhotosPerListing = 40;
		$this->listingProcessed = 0;
		$this->listHubRecord = array();
		$this->triggerCities = [];
		//$this->xhr = false;
		$this->saveXmlFiles = false;
		$this->out = null;
		$this->listHubLog = null;
		$this->saveParallelResultToDb = true;
		$this->addedToDb = 0;
		$this->duplicates = 0;
		$this->rejected = 0;
		$this->belowMinimumAcceptedPrice = 0;
		$this->saveRejected = false;
		$this->preExisting = null;
		$this->cityTags = null; // grab only score of > 7 or -1
		$this->allCityTags = null; // grab all tags for every city
		$this->batchSizeXML = 50;
		$this->debugListingGeoCoding = false;
		$this->forceRedoGeoCodeListing = false;
		$this->usStates = $usStates;
		$this->imageBorderMode = AH_ACTIVE;
		$this->imageBorderOp =TRIM_IMAGE;
		$this->processOnlyNewImages = true;
		$this->imageBorderFromLast = false;

		$this->minimumPriceOffset = 1.20;
		$this->medianPriceOffset = 1.20;
		$this->medianPriceOffsetCondo = 1.20;
		$this->medianId = 0;
		$this->medianCityId = 0;
		$this->useMedianPrices = 1;
		$this->baseConsideredPriceMultiplier = BASE_CONSIDERED_PRICE_MULTIPLER;
		$this->baseConsideredPriceMultiplierCondo = BASE_CONSIDERED_PRICE_MULTIPLER_CONDO;
		$this->minimumAcceptedPrice = DEFAULT_MINIMUM_PRICE_FROM_LISTHUB;

		$this->specialOpType = SPECIAL_OP_NONE;
		$this->updateToDbList = [];

		$this->log_to_file = true;
		$this->log = $this->log_to_file ? new Log(__DIR__.'/_logs/.parser.log') : null;
		$this->lock_sql = true;
		$this->lockfile = $this->lock_sql ? fopen(__DIR__.'/_logs/lockSql.lock', 'c') : null; 

		$this->allowSystemBusyWindow = false;
		$this->agentAcronymMakeUppercase = ['crs','mba','gri','cdpe','qsc','mrp','abr','ii','(dr)','(e)','pa','iii','p.a.','a.b.','ab','cpa','csr','dpp','sres','rli','ltg','sfr','cips','rebac','crb','ccim','epro','bpor','sf','csp','sfr.','pc','cai','pro','srs','cne','tahs','rs','cdrs','asr','chms','mre','reos','csn','cbr','ahwd','trc','afh.','gri.','trlp','sps'];
		$this->agentAcronymDoubleCheck = ['reos','reo'];

		$this->imageCleanFolderOperation = self::CLEAN_IMAGE_MARK_IT;
		$this->okToEmptyProgressTableBeforeIP = true;
		$this->runPPAsBatch = 1;
		$this->processHugeImageFile = 0;
		$this->forceRegenerateHugeImageFile = 1;
		$this->uselocksql = 0;

		$this->retryCurlCount = 0;
		$this->saveClickedListings = false;
		$this->recordListHubKey = false;
		$this->extraGeoCodingSet = 0;
		$this->doExtraGeoCoding = false;
		$this->extraGeoCodingMask = (4 + 8 + 16 + 32 + 64 + 128);
		$this->geoCodeAllMaskType = 0;

		$debug = get_cfg_var("allure.xdebugLevel");
		if ($debug)
			$this->debugLevel = intval($debug);
		else
			$this->setDebugLevel();
		$this->setBatchSizeXML();
		$this->setAllowSystemBusy();
		$testPrice = get_cfg_var('allure.mininumPrice');
		if ($testPrice)
			$this->minimumPrice = intval($testPrice);
		$testTagCount = get_cfg_var('allure.minimumTagCount');
		if ($testTagCount)
			$this->minimumTagCount = intval($testTagCount);
		$testMaxPhotosPerListing = get_cfg_var('allure.maxPhotosPerListing');
		if ($testMaxPhotosPerListing)
			$this->maxPhotosPerListing = intval($testMaxPhotosPerListing);

		$this->getClass('Listings', $this->debugListingGeoCoding);

		$this->setMiscOptions();

		try{
			set_time_limit(0);
			error_reporting(0);
			ignore_user_abort(true);
/*
			if (!$this->haveOutputFile && $this->xhr)
			{
				$this->setXhr();
			}
*/
			// this will override whatever was in the 'Bathrooms' field, so it's not double counted.
			$this->parseRules['FullBathrooms']['func'] = 
						function(&$object, $data){ 
							$object['baths'] = floatval($data); 
						};
			$this->parseRules['LotSize']['func'] = 
						function(&$object, $field, $data, $attr){ 
								$unit = 'ft';
								foreach($attr as $a => $b) {
									$b = $b->__toString();
									if ($a == 'areaUnits') {	
										if (strtolower($b) == 'squarefoot')
											$b = 'ft';								
										$unit = $b;
										break;
									}
								}

								$data = floatval($data);

								if ($unit == 'acre')
									$unit = 'acres';

								if ($unit != 'acres' &&
									$unit != 'ft' &&
									$unit != 'meters') {
									if ($data < 3000) //let's just assume it's in acres at this point
										$unit = 'acres';
									else
										$unit = 'ft';
								}

								if ($unit == 'ft' &&
									$data < 1000) // must be a mistake, force it into acres
									$unit = 'acres';

								if (floatval($data) < 2.0) {
									$data = round(floatval($data) * 43560); // turns to integer
									$unit = 'ft';
								}
								elseif ( floatval($data) > 43560 ) {  // convert to acre
									$data = number_format(floatval($data)/43560, 4);
									$units = 'acres';
								}
								$object['lotsize'] = $data;
								$object['lotsize_std'] = $unit;
						};
			$this->parseRules['ListingKey']['func'] = 
						function(&$object, $data){ 
							if (!isset($object['mls'])) // guarantee unique # if MlsNumber is not present
								$object['mls'] = $data;
							$object['listhub_key'] = $data;
						};
			$this->parseRules['MarketingInformation']['PermitAddressOnInternet']['func'] = 
						function(&$object, $data){
							if ($data == 'true' || $data == 'yes' || 'data' == 1)
								$object['flags'] |= LISTING_PERMIT_ADDRESS;
							else
								$object['flags'] &= ~LISTING_PERMIT_ADDRESS; // clear it out
						};
			$this->parseRules['MlsNumber']['func'] = 
						function(&$object, $data){ 
							if (isset($object['mls']) &&
								preg_match_all('/[0-9]/', $data) <= 3) // ignore if set already and has 3 or less #'s'
								return;
							$object['mls'] = $data;
						};
			$this->parseRules['Address']['FullStreetAddress']['func'] = 
						function(&$object, $data){ 
							if ( ($pos = strpos(strtolower($data), "www.")) !== false)
								$object['street_address'] = trim(substr($data, 0, $pos));
							else
								$object['street_address'] = $data;
					
							if ($pos !== false)
								$this->record("FullStreetAddress: stripped out www, ended up with {$object['street_address']}, \n", 3);
						};
			$this->parseRules['Address']['UnitNumber']['func'] = 
						function(&$object, $data){ 
							if (empty($data) ||
								(strlen($data) == 1 &&
								 ($data == '.' ||
								  $data == '0')) ||
								 $data == 'n/a')
								return;

							if (strpos($data, "#") !== false)
								$object['street_address'] .= ', '.$data;
							else
								$object['street_address'] .= ', #'.$data;

							// $lm = $this->getClass("ListHubMatchTags");
							// $object['tags']['condo'] = $lm->getTagId('condo');
							// $this->record("UnitNumber: $data, forcing condo tag\n", 3);
							$object['flags'] |= LISTING_ADDRESS_HAS_UNIT_NUMBER;
						};
			$this->parseRules['ListingStatus']['func'] = 
						function(&$object, $field, $data){ 
							$object[$field] = $data == 'Active' ?  1 :  0; 
							$object['listing_status'] = $data;
						};
			$this->parseRules['Photo']['MediaURL']['func'] = 
						function(&$object, $data){
							if (!isset($object['images']))
								$object['images'] = array();
							$url = $base = $data;

							$isDup = false;
							$discard = false;
							$isUrl = false;
							$image = null;
							$width = $height = 0;
							if ($this->preExisting != null) {
								foreach($this->preExisting->images as $image) {
									if (property_exists($image, 'url') && removeslashes($image->url) == $url) { // this will be there if image had border trimmmed
										$isDup = true;
										$isUrl = true;
									}
									elseif (property_exists($image, 'discard') && removeslashes($image->discard) == $url) { // if rejected previously
										$isDup = true;
										$discard = true;
									}
									elseif ( property_exists($image, 'file') && removeslashes($image->file) == $url )
										$isDup = true;
										
									if ($isDup) {
										$this->record( "Image with $url already exists in pre-existing listing with MLS: ".$this->preExisting->listhub_key."\n", 1);
										break;
									}
								}
							}
							else
								$this->record( "Image with $url is new to listing:".$object['listhub_key']."\n", 2);

							if (!$isDup &&
								(!property_exists($image, 'width') ||
								 !property_exists($image, 'height')) ) {
								list($width, $height, $type, $attr) = getimagesize($url);
								if ((int)$width < (int)$height ||
									(int)$width < MINIMUM_IMAGE_WIDTH)
								{
									$this->record( "Image with width:$width, height:$height, discarded - file:$base\n", 2); 
									$discard = true; 
								}
							}

							if (!$isDup &&
								!$this->justSavePhotUrl) {
								// $file_info = pathinfo($url);
								// $base = $file_info['filename'];

								// $base = preg_replace('/[^a-zA-Z0-9]/', '_', $base);

								$base = $this->getBaseImageName($url);
								// if (strpos($base, '.') == false)
								switch($type){
									case IMG_JPG: $base .= '.jpg'; break;
									case IMG_JPEG: $base .= '.jpeg'; break;
									case IMG_GIF: $base .= '.gif'; break;
									case IMG_PNG: $base .= '.png'; break;
									case IMG_WBMP: $base .= '.wbmp'; break;
									case IMG_XPM: $base .= '.xpm'; break;
								}
									
								$dest = __DIR__.'/../_img/_listings/uploaded/'.$base;
								if (!copy($url, $dest)){
									$this->record( "Failed to copy image: $url, to $dest\n", 4 ); 
									return; 
								}
							}	
							elseif ($this->urlFile) {
								fwrite($this->urlFile, $url."\n");
								fflush($this->urlFile);
							}

							$count = count($object['images']);
							$pos = 'pos'.$count;			

							if (!$isDup)
							{
								$this->record( "Saved image with width:$width, height:$height for index: $this->index, array[$pos]: ".($object['images'][$count] == null ? "empty" : "full").", count: $count, file:$base\n", 2); 								
							}
							/*
							$memUsedWhole = memory_get_usage(true);
							$memUsedeMalloc = memory_get_usage(false);
							$this->record("memory usage before allocation array[$pos]: SystemUsed: $memUsedWhole, eMallocUsed: $memUsedeMalloc\n", 2);
							*/
							if ( $object['images'][$count] == null )
							{
								$this->record( "About to allocate array[$pos]\n", 2);
								$object['images'][$count] = array();
								$this->record( "Allocated array[$pos]\n", 2);
							}
							else
								$this->record( "No allocation for array[$pos]\n", 2);

							if ($isUrl) {
								$gotApos = false;
								if (strpos(strtolower($image->url),"'") !== false ) {
									$this->record("need addslashes() for ".(isset($object['listhub_key']) ? $object['listhub_key'] : "unknown")." - image url:$image->url\n", 3);
									$gotApos = true;
								}
								$object['images'][$count]['url'] = addslashes($image->url);
								$object['images'][$count]['file'] = addslashes($image->file); // this would be the border-processed image
								if ($gotApos)
									$this->record("after addslashes() - {$object['images'][$count]['url']}\n, 3");
							}
							elseif (!$discard) {
								if ($isDup &&
									$image->width < MINIMUM_IMAGE_WIDTH) {// revised number
									$object['images'][$count]['discard'] = addslashes($base);
									$this->record("Photo for {$object['listhub_key']} was a file, but is now set to discard: $base");
								}
								else
									$object['images'][$count]['file'] = addslashes($base);
							}
							else {
								if ($isDup &&
									$image->width >= MINIMUM_IMAGE_WIDTH) {// revised number, restore if needed
									$object['images'][$count]['file'] = addslashes($base);
									$this->record("Photo for {$object['listhub_key']} was a discard, but is now set to file: $base");
								}
								else
									$object['images'][$count]['discard'] = addslashes($url);
							}

							
							$object['images'][$count]['width'] = !property_exists($image, 'width') ? $width : $image->width;								
							$object['images'][$count]['height'] = !property_exists($image, 'height') ? $height : $image->height;								

							if (property_exists($image, 'processed') )
								$object['images'][$count]['processed'] = $image->processed;

							$this->record( "Assigned image to index: $pos\n", 2); 								
						};
			$this->parseRules['Photo']['MediaCaption']['func'] = 
						function(&$object, $data){
							if (!isset($object['images']))
								$object['images'] = array();
							if (!isset($object['images'][$this->index]))
								$object['images'][$this->index] = array();
							$object['images'][$this->index]['title'] = $data;
						};
			$this->parseRules['Photo']['MediaDescription']['func'] = 
						function(&$object, $data){
							if (!isset($object['images']))
								$object['images'] = array();
							if (!isset($object['images'][$this->index]))
								$object['images'][$this->index] = array();
							$object['images'][$this->index]['desc'] = $data;
						};
			$this->parseRules['Video']['MediaURL']['func'] = 
						function(&$object, $data){
							if (!isset($object['video']))
								$object['video'] = array();
							$url = $data;
							$base = basename($url);
							$dest = __DIR__.'/../_img/_listings/uploaded/'.$base;

							if (!copy($url, $dest)){
								$this->record( "Failed to copy video: $url, to $dest\n" ); return; }
							if (!isset($object['video'][$this->index]))
								$object['video'][$this->index] = array();
							$object['video'][$this->index]['file'] = $base;
						};
			$this->parseRules['Video']['MediaCaption']['func'] = 
						function(&$object, $data){
							if (!isset($object['video']))
								$object['video'] = array();
							if (!isset($object['video'][$this->index]))
								$object['video'][$this->index] = array();
							$object['video'][$this->index]['title'] = $data;
						};
			$this->parseRules['Video']['MediaDescription']['func'] = 
						function(&$object, $data){
							if (!isset($object['video']))
								$object['video'] = array();
							if (!isset($object['video'][$this->index]))
								$object['video'][$this->index] = array();
							$object['video'][$this->index]['desc'] = $data;
						};
			$this->parseRules['Office']['PhoneNumber']['func'] =
						function(&$object, $data){
							if (!isset($object['phone']))
								$object['phone'] = $data;
						};
						/*
			$this->parseRules['Brokerage']['Name']['func'] = 
						function(&$object, $data){
							$split = explode(' ', $data);
							$object['first_name'] = $split[0];
							$object['last_name'] = $split[count($split)-1];
						};
						*/
			$this->parseRules['Brokerage']['Phone']['func'] =
						function(&$object, $data){
							if (!isset($object['phone']))
								$object['phone'] = $data;
						};
			$this->parseRules['Brokerage']['Email']['func'] =
						function(&$object, $data){
							if (!isset($object['email']))
								$object['email'] = $data;
						};
			$this->parseRules['Brokerage']['WebsiteURL']['func'] =
						function(&$object, $data){
							if (!isset($object['website']))
								$object['website'] = $data;
						};
			$this->parseRules['YearBuilt']['func'] =
						function(&$object, $data){
							$this->getClass("ListHubMatchTags")->getTagList($object, 'YearBuilt', $data);
						};
			$this->parseRules['ListingDescription']['func'] =
						function(&$object, $data){
							$object['about'] = $data;
							$this->getClass("ListHubMatchTags")->getTagList($object, 'ListingDescription', $data);
							$this->getClass("ListHubMatchTags")->unTagList($object, $data);
						};
			$this->parseRules['ListingTitle']['func'] =
						function(&$object, $data){
							$commercial = array('commercial','industrial','warehouse','land\/lot',' land ','residential inc','retail','multi-family',
												'apartment complex','mobile home park','units','lots & acreage','multi-unit',
												'income','fourplex','restaurant','zoned residential','unimproved','res & com',
												'residential development', 'vacant land', 'land/acres', 'vacant lot', 'lots and acreage',
												'lots/acreage','cld ','apartment building','lots and land','apt complex', 'rld ',
												'land lot','hotel','motel','office buiding','unit lot','real estate only',
												'investment','business','farm land');
							$tmp = strtolower($data);
							$gotOne = false;
							foreach($commercial as $key) {
								if ( ($pos = strpos($tmp, $key)) !== false) {
									$object['error'] |= COMMERCIAL;
									$this->record("Listing MLS:".$object['listhub_key']. " is set with COMMERCIAL for title: $data\n", 3);
									$gotOne = true;
									break;
								}
							}

							if (!$gotOne) { // test for condo
								if ( strpos($tmp, 'condo') !== false ||
									 strpos($tmp, 'townhouse') !== false ||
									 strpos($tmp, 'townhome') !== false ||
									 strpos($tmp, 'apartment') !== false ||
									 strpos($tmp, 'cooperative') !== false ) {
									$lm = $this->getClass("ListHubMatchTags");
									$object['tags']['condo'] = $lm->getTagId('condo');
									$object['flags'] |= LISTING_IS_CONDO;
									if (isset($object['tags']['house']))
										unset($object['tags']['house']);
									$this->record("ListingTitle: $data is triggering condo tag");
								}
							}
							$object['title'] = $data;
						};
			$this->parseRules['DetailedCharacteristics']['func'] =
						function(&$object, $field, $data){
							// $prep = is_array($data) ? array_pop($data) : $data;
							// $i = 0;
							// if (gettype($prep) == 'object') {
							// 	$key = '0';
							// 	$prep = $prep->$key;
							// 	$i++;
							// 	if (gettype($prep) == 'object') {
							// 		$prep = $prep->__toString();
							// 		$i = 3;
							// 	}
							// }
							// else {
							// 	$prep = $prep.__toString();
							// 	$i = 2;
							// }
							$this->getClass("ListHubMatchTags")->getTagList($object, $field, $data);
						};
			$this->parseRules['Rooms']['Room']['func'] =
						function(&$object, $data){
							$this->getClass("ListHubMatchTags")->getTagList($object, 'Room', $data);
						};
			$this->parseRules['ExteriorTypes']['ExteriorType']['func'] =
						function(&$object, $data){
							$this->getClass("ListHubMatchTags")->getTagList($object, 'ExteriorType', $data);
						};
			$this->parseRules['ViewTypes']['ViewType']['func'] =
						function(&$object, $data){
							$this->getClass("ListHubMatchTags")->getTagList($object, 'ViewType', $data);
						};
			$this->parseRules['ListingCategory']['func'] =
						function(&$object, $data, $ele){
							if ( strpos($data, "Rent") !== false )
								$object['error'] |= RENTAL;
							elseif ( strpos($data, "Purchase") === false )
								$this->record("ListingCategory for {$object['listhub_key']} is $data", 3);
						};
			$this->parseRules['PropertyType']['func'] =
						function(&$object, $data, $ele){
							if ($data == "Commercial" ||
								$data == "Lots And Land") {
								$object['error'] |= COMMERCIAL;
								if (isset($object['tags']['condo'])) {
									unset($object['tags']['condo']);
									$object['flags'] &= ~LISTING_IS_CONDO;
								}
								if (isset($object['tags']['house']))
									unset($object['tags']['house']);
								$this->record("Listing MLS:".$object['listhub_key']. " is set with COMMERCIAL for PropertyType\n", 3);
							}
							elseif ( stripos($data, "Residential") !== false &&
									 stripos($data, "Land") === false ) {
								$lm = $this->getClass("ListHubMatchTags");
								// $attr = isset($ele['otherDescription']) ? ['otherDescription'=>(string)$ele['otherDescription']] : [];
								$attr = array();
								$attr['otherDescription'] = $ele->attributes()->otherDescription;
								if (!isset($object['tags']['condo'])) {
									if (isset($attr['otherDescription']) && 
										(stripos($attr['otherDescription'], 'condo') !== false ||
										 stripos($attr['otherDescription'], 'apartment') !== false ||
										 stripos($attr['otherDescription'], 'townhouse') !== false ||
										 stripos($attr['otherDescription'], 'townhome') !== false ||
										 stripos($attr['otherDescription'], 'cooperative') !== false) ) {
										$object['tags']['condo'] = $lm->getTagId('condo');
										$object['flags'] |= LISTING_IS_CONDO;
										$this->record("PropertyType attribute is ".$attr['otherDescription'].", so setting condo tag\n", 3);
										if (isset($object['tags']['house']))
											unset($object['tags']['house']);
									}	
									else
										$object['tags']['house'] = $lm->getTagId('house');
								}
								else
									$this->record("Condo is already set, though PropertyType says it's $data\n", 3);
								unset($attr);
							}

						};
			$this->parseRules['PropertySubType']['func'] =
						function(&$object, $data, $ele){
							// $attr = isset($ele['otherDescription']) ? ['otherDescription'=>(string)$ele['otherDescription']] : [];
							$attr = array();
							$attr['otherDescription'] = $ele->attributes()->otherDescription;
							if ( stripos($data, "condo") !== false ||
								 stripos($data, "apartment") !== false ||
								 (isset($attr['otherDescription']) && stripos($attr['otherDescription'], 'condo') !== false) ||
								 (isset($attr['otherDescription']) && stripos($attr['otherDescription'], 'apartment') !== false) ||
								 (isset($attr['otherDescription']) && stripos($attr['otherDescription'], 'townhouse') !== false) ||
								 (isset($attr['otherDescription']) && stripos($attr['otherDescription'], 'townhome') !== false) ||
								 (isset($attr['otherDescription']) && stripos($attr['otherDescription'], 'cooperative') !== false) ) {
								$this->record("PropertySubType attribute is ".$attr['otherDescription']."\n", 3);
								$lm = $this->getClass("ListHubMatchTags");
								$object['tags']['condo'] = $lm->getTagId('condo');
								$object['flags'] |= LISTING_IS_CONDO;
								if (isset($object['tags']['house']))
									unset($object['tags']['house']);
							}
							unset($attr);
						};

		}
		catch(\Exception $e)
		{
			$this->record( $this->parseException($e) ); die(); 
		}
	}

	public function __destruct() {
		if ($this->lockfile);
			fclose($this->lockfile);
	}

	public function version() {
		return PARSER_VERSION;
	}

	public function lock() {
		if ($this->lockfile !== null &&
			$this->uselocksql)
			flock($this->lockfile, LOCK_EX);
	}
	public function unlock() {
		if ($this->lockfile !== null &&
			$this->uselocksql)
			flock($this->lockfile, LOCK_UN);
	}


	public function setDebugLevel() {
		$o = $this->getClass('Options');
		$q = new \stdClass();
		$q->where = array('opt'=>'parserDebugLevel');
		$x = $o->get($q);
		if (!empty($x))
			$this->debugLevel = intval($x[0]->value);

		$q->where = array('opt'=>'listingGeoCodeDebugLevel');
		$x = $o->get($q);
		if (!empty($x))
			$this->debugListingGeoCoding = intval($x[0]->value);
	}

	protected function setBatchSizeXML() {
		$o = $this->getClass('Options');
		$q = new \stdClass();
		$q->where = array('opt'=>'parserBatchSizeXML');
		$x = $o->get($q);
		if (!empty($x))
			$this->batchSizeXML = intval($x[0]->value);
	}

	protected function setAllowSystemBusy() {
		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'AllowSystemBusyWindow']]);
		if (!empty($opt))
			$this->allowSystemBusyWindow = intval($opt[0]->value);
	}

	protected function setPricingOptions() {
		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'MinimumPriceOffset']]);
		if (!empty($opt))
			$this->minimumPriceOffset = floatval($opt[0]->value);

		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'MedianPriceOffset']]);
		if (!empty($opt))
			$this->medianPriceOffset = floatval($opt[0]->value);

		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'MedianPriceOffsetCondo']]);
		if (!empty($opt))
			$this->medianPriceOffsetCondo = floatval($opt[0]->value);

		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'UseMedianPrices']]);
		if (!empty($opt))
			$this->useMedianPrices = intval($opt[0]->value);

		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'BaseConsideredPriceMultiplier']]);
		if (!empty($opt))
			$this->baseConsideredPriceMultiplier = floatval($opt[0]->value);

		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'BaseConsideredPriceMultiplierCondo']]);
		if (!empty($opt))
			$this->baseConsideredPriceMultiplierCondo = floatval($opt[0]->value);

		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'MinimumAcceptedPriceFromListhub']]);
		if (!empty($opt))
			$this->minimumAcceptedPrice = intval($opt[0]->value);
	}

	protected function setMiscOptions() {
		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'ForceRedoGeoCodeListing']]);
		if (!empty($opt))
			$this->forceRedoGeoCodeListing = intval($opt[0]->value);

		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'AgentAcronymMakeUppercase']]);
		if (!empty($opt)) {
			$opt[0]->value = str_replace("'", '"', $opt[0]->value);
			$this->agentAcronymMakeUppercase = json_decode($opt[0]->value);
		}

		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'AgentAcronymDoubleCheck']]);
		if (!empty($opt)) {
			$opt[0]->value = str_replace("'", '"', $opt[0]->value);
			$this->agentAcronymDoubleCheck = json_decode($opt[0]->value);
		}

		
		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'ImageCleaningJustMarkIt']]);
		if (!empty($opt)) {
			$this->imageCleanFolderOperation = intval($opt[0]->value) == 0 ? self::CLEAN_IMAGE_REMOVE_IT : self::CLEAN_IMAGE_MARK_IT;
		}

		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'RunPPAsBatch']]);
		if (!empty($opt))
			$this->runPPAsBatch = intval($opt[0]->value);

		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'ProcessHugeImageFile']]);
		if (!empty($opt))
			$this->processHugeImageFile = intval($opt[0]->value);

		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'ForceRegenerateHugeImageFile']]);
		if (!empty($opt))
			$this->forceRegenerateHugeImageFile = intval($opt[0]->value);

		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'SaveClickedListings']]);
		if (!empty($opt))
			$this->saveClickedListings = intval($opt[0]->value);

		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'OkToEmptyProgressTableBeforeIP']]);
		if (!empty($opt))
			$this->okToEmptyProgressTableBeforeIP = intval($opt[0]->value);

		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'RecordListHubKey']]);
		if (!empty($opt))
			$this->recordListHubKey = intval($opt[0]->value);
		// DoExtraGeoCoding
		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'DoExtraGeoCoding']]);
		if (!empty($opt))
			$this->doExtraGeoCoding = intval($opt[0]->value);
		// extraGeoCodingMask
		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'ExtraGeoCodingMask']]);
		if (!empty($opt))
			$this->extraGeoCodingMask = intval($opt[0]->value);
		// GeoCodeAllMaskType
		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'GeoCodeAllMaskType']]);
		if (!empty($opt))
			$this->geoCodeAllMaskType = intval($opt[0]->value);
	}

	protected function activeToStr($active) {
		switch($active) {
			case AH_INACTIVE: return "Inactive";
			case AH_ACTIVE: return "Active";
			case AH_WAITING: return "Waiting";
			case AH_REJECTED: return "Rejected";
			case AH_NEVER: return "Never";
			case AH_TOOCHEAP: return "TooCheap";
			case AH_AVERAGE: return "Average";
			case AH_RENTAL: return "Rental";
			default:
				return "Unknown";
		}
	}

	// public function findStartOfLastPPRecord($doingImagePP = 0) {
	// 	$progress = $this->getClass("ListhubProgress");
	// 	$q = new \stdClass();
	// 	$q->where = array('type'=> $doingImagePP ? self::RESTART_PARALLEL_IP : self::RESTART_PARALLEL);
	// 	$x = $progress->get($q);
	// 	if (!empty($x))
	// 		return $x[count($x)-1]->id;
	// 	else
	// 		return 0;
	// }

	public function getPPRecordRange($start, $end) {
		$progress = $this->getClass("ListhubProgress");
		$tableName = $progress->gettablename($progress->table);
		$sql = "SELECT * FROM $tableName WHERE `id` >= $start AND `id` <= ".($end-1);
		if (!$sql_results = $progress->wpdb->get_results($sql)) throw new \Exception('Unable to add row and data to database using: '.$sql);

		if (empty($sql_results) || count($sql_results) < 1) $sql_results = false;
			return $sql_results;
	}

	private function process($cmd, $wait = true) {
		$this->updateToDb("process - $cmd, starting", self::PROCESS_EXEC_START);
		// $Process = $this->getClass('Process',$cmd);
		// if ($wait)
		// 	while( $Process->status() )
		// 		sleep(1);
		$out = [];
		exec($cmd, $out);
		foreach($out as $log)
			$this->updateToDb("process - $log", self::PROCESS_EXEC_OUTPUT);

		$this->updateToDb("process - $cmd, exiting", self::PROCESS_EXEC_END);
	}

	private function runScriptOnWeb01($cmd, $wait = true) {
		$this->updateToDb("process - $cmd, starting", self::PROCESS_EXEC_SCRIPT_START);
		// $Process = $this->getClass('Process',$cmd);
		// if ($wait)
		// 	while( $Process->status() )
		// 		sleep(1);
		$out = [];
		if ( strpos(get_home_url(), 'engr') !== false)
			$cmd = "ssh www-data@192.168.1.3 ".$cmd;

		exec($cmd, $out);
		foreach($out as $log)
			$this->updateToDb("process - $log", self::PROCESS_EXEC_SCRIPT_OUTPUT);

		$this->updateToDb("process - $cmd, exiting", self::PROCESS_EXEC_SCRIPT_END);
	}

	public function writeLogHeaderForParallelFile(	$subdomains, 
													$chunks, 
													$unitsperchunk, 
													$saverejected, 
													$cpulimit,
													$minTags, 
													$minPrice,
													$totalBlocks,
													$ppRunId,
													$specialOperation = SPECIAL_OP_NONE,
													$restartSQL = true,
													$resetMedianAndTiers = true)
	{
		$doingImagePP = !($specialOperation >= IMAGE_BORDER_PARALLEL && $specialOperation <= IMAGE_BORDER_PARALLEL_BATCH) ? 0 :
						 ($specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 1 : 2);
		$blockTable = !$doingImagePP ? 'ListhubBlocks' : ($doingImagePP == 1 ? 'ImageBlocks' : 'CustomImageBlocks');
		$this->updateToDb("writeLogHeaderForParallelFile entered for specialOperation:$specialOperation, doingImagePP:".($doingImagePP ? $doingImagePP : 'no').", resetMedianAndTiers:".($resetMedianAndTiers ? 'yes' : 'no').", blockTable:$blockTable", self::WRITE_LOG_HEADER_START);

		if ($specialOperation != IMAGE_BORDER_PARALLEL_AFTER_PARSE) {// then reinitialize log files
			if ($resetMedianAndTiers) {
				$this->cleanUpLogFiles($doingImagePP);  // cycle log files
				$this->createDebugFile(!$doingImagePP ? 1 : ($doingImage == 1 ? 3 : 4)); // otherwise create new listhubParallel.txt
			}
			else
				$this->openDebugFile(!$doingImagePP ? 1 : ($doingImage == 1 ? 4 : 5));

			if ( !$doingImagePP &&
				 strpos( strtolower(php_uname()), 'linux') !== false) {
				// $q1 = ['where'=>['status'=>'processing']];
				// $q2 = ['where'=>['status'=>'pending']];
				// $ticks = 0;
				// // stall for any Quiz pending or processing
				// while( $this->getClass('QuizActivity',1)->count($q1) ||
				// 	   $this->getClass('QuizActivity',1)->count($q2) ) {
				// 	$ticks++;
				// 	sleep(1);
				// 	if ($ticks > 120)
				// 		break;
				// }
				// $this->updateToDb("writeLogHeaderForParallelFile - waited $ticks secs to restartSQL", self::WRITE_LOG_HEADER_PRESTART_SQL);
				if ($restartSQL) {
					$this->runScriptOnWeb01("restartSQL");
					$this->optimizeTable(true); // call sysdown/sysup
				}
			}

			if ( $doingImagePP )
				@popen("rm /tmp/alr*.img /tmp/alr??????", 'r');

			$this->beginOutputBuffering();

			if ($resetMedianAndTiers)
				$this->logHeader();
			$this->record("ppRunId:$ppRunId, doingImagePP:$doingImagePP, specialOperation:$specialOperation, restartSQL:$restartSQL, resetMedianAndTiers:$resetMedianAndTiers", 3);
			$this->stopOutputBuffering();
		}

		if ($specialOperation == SPECIAL_OP_NONE ||
			$specialOperation == SPECIAL_PARSE_BATCH) { // then doing a regular parse
			if ($resetMedianAndTiers) {
				$this->createDebugFile(2);  // create just listHubLog.txt
				$header = "ListingKey,Status,URL,Message,Timestamp\n";
				fwrite($this->listHubLog, $header);
				flush();
				fclose($this->listHubLog);
			}
			if ($this->allowSystemBusyWindow) {
				$systemBusy = $this->getClass('Options')->get((object)['where'=>['opt'=>'SystemMaintenance'],
																		'what'=>['value']]);
				$systemBusy = empty($systemBusy) ? -1 : intval($systemBusy[0]->value);
				if ($systemBusy == 0)
					$this->getClass('Options')->set([(object)['where'=>['opt'=>'SystemMaintenance'],
															  'fields'=>['value'=>1]]]);
				elseif ($systemBusy == -1) // create one
					$this->getClass('Options')->add(['opt'=>'SystemMaintenance',
													 'value'=>1]);
			}

			if ($resetMedianAndTiers) {
				$this->updateToDb("writeLogHeaderForParallelFile calling resetDaily() median values", self::WRITE_LOG_HEADER_RESET_DAILY_MEDIAN_VALUES);
				$this->getClass('MedianPrices')->resetDaily();
			}
		}
		else // clean up old tmp images
			@popen("rm /tmp/alr*.img /tmp/alr??????", 'r');

		// $doingImagePP = $specialOperation >= IMAGE_BORDER_PARALLEL && $specialOperation <= IMAGE_BORDER_PARALLEL_BATCH;
		// mark start of PP
		$reason = !$doingImagePP ? self::RESTART_PARALLEL : ($doingImagePP == 1 ? self::RESTART_PARALLEL_IP : self::RESTART_PARALLEL_CUSTOM_IP);
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;	
		$data = new \stdClass();
		$data->subdomains = $subdomains;
		$data->chunks = $chunks;
		$data->unitsperchunk = $unitsperchunk;
		$data->saverejected = $saverejected;
		$data->cpulimit = $cpulimit;
		$data->minTags = $minTags;
		$data->minPrice = $minPrice;
		$data->totalBlocks = $totalBlocks;
		$data->starttime = microtime(); // date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$data->ppRunId = $ppRunId;
		$data->specialOperation = $specialOperation;
		$this->updateProgress(json_encode($data), $reason); // to LHP
		$this->updateStartParallelData(json_encode($data), $reason); // to Options
 		$this->saveLogToDb = $oldFlag;

		$chunkTable = !$doingImagePP ? 'ListhubChunkStatus' : ($doingImagePP == 1 ? 'ImageChunkStatus' : 'CustomImageChunkStatus');
		$lcs = $this->getClass($chunkTable);
		$lcs->emptyTable(true); // clear out chunk status
		$this->updateToDb("writeLogHeaderForParallelFile cleared table:$chunkTable", self::WRITE_LOG_HEADER_EMPTY_TABLE);

		if (!$specialOperation) {
			// reset tier to all zero
			$l = $this->getClass('Listings');
			if ($resetMedianAndTiers) {
				$this->updateToDb("writeLogHeaderForParallelFile about to update tiers", self::WRITE_LOG_HEADER_UPDATE_TIERS);
				// $this->record("writeLogHeaderForParallelFile about to update tiers", 3);
				// $sql = "UPDATE ".getTableName($l->table)." SET `tier` = 0 WHERE 1"; // `tier` != 0 OR `tier` IS NULL";
				// $x = $l->rawQuery($sql);
				$l->set([(object)['where'=>['tier'=>1],
								  'fields'=>['tier'=>0]]]); // so this will update only those listings which are currently active to 'test' mode
															// and will not affect the 'updated' field of those which are already at 0 state.
															// if it is visited in this parse, the tier will go to 1, else stay at 0.
				$this->updateToDb("writeLogHeaderForParallelFile about to update block flags", self::WRITE_LOG_HEADER_UPDATE_BLOCK_FLAGS);
				$this->getClass($blockTable)->set([(object)['where'=>['flag' => 1],
														    'fields'=>['flag'=> 0]]]);
			}

			if ($this->debugListingGeoCoding)
				$l->clear();
		}
		$this->updateToDb("writeLogHeaderForParallelFile exiting", self::WRITE_LOG_HEADER_DONE);
		// $this->record("writeLogHeaderForParallelFile exiting", 3);
	}

	public function prepareImageFolderCleaning() {
		// send ack back now so it won't time out on front end
		$this->exitNow("Begin cleaning out img folder.");

		$startChars = ['int'=>['start'=>48,
								'end'=>57],
					   'big'=>['start'=>65,
								'end'=>90],
					   'small'=>['start'=>97,
								'end'=>122]];

		$ImageTracker = $this->getClass('ListhubImageTracker');
		$ImageTracker->emptyTable(true);

		$msg = (object)['msg'=>"Sorting through listings image folder to delete stray images"];
		$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_START);
		$total = 0;

		$path = __DIR__.'/../_img/_listings/uploaded/';
		$contents = opendir($path);
		// $list = glob(__DIR__.'/../_img/_listings/uploaded/'.chr($start)."*");
		$q = new \stdClass();
		$q->flags = 0;
		// foreach($list as $i=>$name) {
		while( !empty($name = readdir($contents)) ) {
			// $q->data = basename($name);
			if ($name[0] == '.')
				continue;
			$q->data = $name;
			$ImageTracker->add($q);
			// unset($name);
			$total++;

			if ( ($total % 1000) == 0) {
				$msg = (object)['msg'=>"Added ".$total." files so far"];
				$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_UPDATE);
			}
		}

		// populate database with all existing images in uploaded folder
		// foreach($startChars as $what=>$range) {
		// 	$start = $range['start'];
		// 	$end = $range['end'];
			
		// 	while ($start <= $end) {
		// 		$path = __DIR__.'/../_img/_listings/uploaded/'.chr($start)."*";
		// 		$contents = opendir($path);
		// 		// $list = glob(__DIR__.'/../_img/_listings/uploaded/'.chr($start)."*");
		// 		$q = new \stdClass();
		// 		$q->flags = 0;
		// 		$count = 0;
		// 		// foreach($list as $i=>$name) {
		// 		while( !empty($name = readdir($contents)) ) {
		// 			// $q->data = basename($name);
		// 			$q->data = $name;
		// 			$ImageTracker->add($q);
		// 			// unset($name);
		// 			$count++;
		// 		}

		// 		$total += $count;
		// 		$msg = (object)['msg'=>"Added ".$count." files for the $what:".chr($start)];
		// 		$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_UPDATE);
		// 		unset($list);
		// 		$start++;
		// 	}
		// }

		$Listings = $this->getClass('Listings');
		$sql = "SELECT COUNT(*) FROM {$Listings->getTableName()} WHERE ";
		$sql.= "active <= 4 OR author_has_account = 1 OR author = 3 OR author = 4";
		global $wpdb;
		$x = intval($wpdb->get_var($sql));

		$msg = (object)['msg'=>"Total $total image files to process",
						'countImages'=>$total,
						'countListings'=>$x];
		$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_INIT_COUNT);

		die;
	}

	protected function cleanOneImage($listing, $id, &$image, $ImageTracker, &$removed, &$error, &$marked, &$needReset, $isListImage = false) {
		if (isset($image->file) && substr($image->file, 0, 4 ) != 'http') { // then processed image
			if ( !empty($track = $ImageTracker->get((object)['where'=>['data'=>$image->file]])) ) {
				if ($this->imageCleanFolderOperation == self::CLEAN_IMAGE_REMOVE_IT) {
					if ( ($x = $ImageTracker->delete(['data'=>$image->file])) )
						$removed++;
					else {
						$this->updateToDb(json_encode(['msg'=>"Failed to find image:$id, $image->file to delete from ImageTracker for listing:$listing->id, active:".$this->activeToStr($listing->active)]), self::CLEAN_IMAGE_ERROR);
						$error++;
					}
				}
				else {
					if ($track[0]->flags == 0) {
						if ( !empty($x = $ImageTracker->set([(object)['where'=>['data'=>$image->file],
																	  'fields'=>['flags'=>IMAGE_TRACKER_KEEPER]]])) )
							$marked++;
						else {
							$this->updateToDb(json_encode(['msg'=>"Failed to set image:$id, $image->file to IMAGE_TRACKER_KEEPER from ImageTracker for listing:$listing->id, active:".$this->activeToStr($listing->active)]), self::CLEAN_IMAGE_ERROR);
							$error++;
						}
					}
					else if (!$isListImage) {
						$this->updateToDb(json_encode(['msg'=>"Already processed $image->file from ImageTracker as row:{$track[0]->id} - suspect listing:$listing->id, image:$id, active:".$this->activeToStr($listing->active)]), self::CLEAN_IMAGE_ERROR);
						$error++;
					}
				}
			}
			else if (!$isListImage) { // then $image->file was not recorded into the ImageTracker table, meaning it didn't exist on disk anymore
				if ( ($listing->active == 1 ||
					  $listing->active == 2) &&
					 isset($image->url) ) {  // so try to revert it back
					$this->updateToDb(json_encode(['msg'=>"Failed to find image:$id, $image->file to process from ImageTracker for listing:$listing->id, resetting from url - active:".$this->activeToStr($listing->active)]), self::CLEAN_IMAGE_ERROR);
					$error++;
					$needReset = true;
					$image->file = $image->url;
					unset($image->url);
					if (isset($image->processed)) unset($image->processed);
				}
			}
		}
	}

	public function cleanImageFolder($page) {
		$perPage = 1000;
		$removed = 0;
		$marked = 0;
		$error = 0;
		$Listings = $this->getClass('Listings');
		$ImageTracker = $this->getClass('ListhubImageTracker');
		// check only active or waiting listings
		$sql = "SELECT id, images, active FROM {$Listings->getTableName()} WHERE ";
		$sql.= "active <= 4 OR author_has_account = 1 OR author = 3 OR author = 4 ";
		$sql.= "LIMIT ".$page*$perPage.', '.$perPage;

		if ( !empty($listings = $Listings->rawQuery($sql)) ) {
			foreach($listings as $listing) {
				if (!empty($listing->images)) {
					$listing->images = json_decode($listing->images);
					$needReset = false;
					foreach($listing->images as $id=>&$image) {
						$this->cleanOneImage($listing, $id, $image, $ImageTracker, $removed, $error, $marked, $needReset);
						unset($image);
					}
					$fields = [];
					if ($needReset)
						$fields['images'] = $listing->images;

					// account for list_image, independent from first_image and images
					if (isset($listing->list_image) &&
						!empty($listing->list_image)) {
						$this->cleanOneImage($listing, 888, $listing->list_image, $ImageTracker, $removed, $error, $marked, $needReset, true); // signal it's list_image
					}

					if (count($fields)) {
						$x = $Listings->set([(object)['where'=>['id'=>$listing->id],
													  'fields'=>$fields]]);
						if (!empty($x))
							$this->updateToDb(json_encode(['msg'=>"Reset images for listing:$listing->id - active:".$this->activeToStr($listing->active)]), self::CLEAN_IMAGE_RESET_IMAGES);
						else {
							$this->updateToDb(json_encode(['msg'=>"Failed to reset images for listing:$listing->id - active:".$this->activeToStr($listing->active)]), self::CLEAN_IMAGE_ERROR);
							$error++;
						}
					}
					unset($fields);
				}
				unset($listing);
			}
			$out = new Out('OK', ['msg'=>($this->imageCleanFolderOperation == self::CLEAN_IMAGE_REMOVE_IT ? "Removed $removed images from ImageTracker"
																										  : "Marked $marked images in ImageTracker"),
								  'status'=>'green',
								  'removed'=>$removed,
								  'marked'=>$marked,
								  'errored'=>$error,
								  'listings'=>count($listings),
								  'operation'=>$this->imageCleanFolderOperation]);
		}
		else { // no more listing to do, now get rid of any remaining images in ImageTracker database
			if ($this->imageCleanFolderOperation == self::CLEAN_IMAGE_REMOVE_IT)
				$totalLeft = $ImageTracker->count();
			else
				$totalLeft = $ImageTracker->count(['where'=>['flags'=>IMAGE_TRACKER_KEEPER]]);
			$out = new Out('OK', ['msg'=>"Completed sorting through all listings that mattered",
								  'status'=>'completed',
								  'totalLeft'=>$totalLeft,
								  'operation'=>$this->imageCleanFolderOperation]);
		}
		return $out;
	}

	public function deleteImagesNotFlaggedAsKeeper($page) {
		$perPage = 100;
		$removed = 0;
		$error = 0;
		$ImageTracker = $this->getClass('ListhubImageTracker');
		$Image = $this->getClass('Image');

		$msg = (object)['msg'=>"deleteImagesNotFlaggedAsKeeper - entered for $page"];
		$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_UPDATE);
		$page = 0; // always set to 0, since each interation, the table will be different.

		if ( !empty($images = $ImageTracker->get((object)['where'=>['flags'=>0],
														  'page'=>$page,
														  'limit'=>$perPage])) ) {
			$msg = (object)['msg'=>"deleteImagesNotFlaggedAsKeeper - $page got back ".count($images)." rows from ImageTracker"];
			$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_UPDATE);
			$q = new \stdClass();
			foreach($images as $img) {
				$q->file = $img->data;
				$Image->unlinkListingImage($q, true); // log it
				$removed++;
				if (!$ImageTracker->set([(object)['where'=>['id'=>$img->id],
												  'fields'=>['flags'=>IMAGE_TRACKER_REMOVED]]])) {
					$this->updateToDb(json_encode(['msg'=>"Failed to update image:$img->id, $img->data as removed from ImageTracker"]), self::CLEAN_IMAGE_ERROR);
					$error++;
				}
			}
			$out = new Out('OK', ['msg'=>"Removed $removed images from Image Folder",
								  'status'=>'green',
								  'removed'=>$removed,
								  'errored'=>$error] );
		}
		else
			$out = new Out('OK', ['msg'=>"Completed removing all images that mattered",
								  'status'=>'completed']);

		return $out;
	}

	public function cleanListingsImgFolder() {
		// send ack back now so it won't time out on front end
		$this->exitNow("Begin cleaning out img folder.");

		$startChars = ['int'=>['start'=>48,
								'end'=>57],
					   'big'=>['start'=>65,
								'end'=>90],
					   'small'=>['start'=>97,
								'end'=>122]];
		$Listings = $this->getClass('Listings');
		$Image = $this->getClass('Image');
		$count = $Listings->count();

		$this->doingParallel = true; // so it appends, not overwrite
		$this->openDebugFile(1);
		$this->beginOutputBuffering();

		$msg = (object)['msg'=>"Sorting through $count listings to delete stray images"];
		$this->record($msg->msg."\n", 3);
		$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_START);

		$perPage = 1000;
		$pages = $count/$perPage;
		$pages += ($count % $perPage) ? 1 : 0;
		$imgCount = 0;

		foreach($startChars as $what=>$range) {
			$start = $range['start'];
			$end = $range['end'];
			
			
			while ($start <= $end) {
				$list = glob(__DIR__.'/../_img/_listings/uploaded/'.chr($start)."*");
				foreach($list as $i=>$name)
					$list[$i] = basename($name);

				$msg = (object)['msg'=>"Checking ".count($list)." files for the letter:".chr($start)];
				$this->record($msg->msg."\n", 3);
				$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_UPDATE);

				$page = 0;
				$chr = chr($start);
				while(count($list) &&
					  $page < $pages) {
					$listings = $Listings->get((object)['limit'=>$perPage,
														'page'=>$page]);
					foreach($listings as $listing) {
						if (!empty($listing->images)) foreach($listing->images as $image) {
							if (isset($image->file) && substr($image->file, 0, 4 ) != 'http') {
								if ( $image->file[0] == $chr &&
									($pos = array_search($image->file, $list)) !== false) {
									array_splice($list, $pos, 1); // remove from list
									$msg = (object)['msg'=>"Removing file: {$image->file} from list of files to remove, has ".count($list)." left in it"];
									$this->record($msg->msg."\n", 3);
									$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_UPDATE);
								}
							}
							unset($image);
							if (count($list) == 0)
								break;
						}
						unset($listing);
						if (count($list) == 0)
							break;
					}
					$page++;
					unset($listings);
				}

				if (count($list)) {
					$imgs = [];
					$msg = (object)['msg'=>"Removing ".count($list)." files starting with the letter:".chr($start)];
					$this->record($msg->msg."\n", 3);
					$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_REMOVE_COUNT);
					foreach($list as $file) {
						$imgs[] = (object)['file'=>$file];
						$msg = (object)['msg'=>"Removing $file"];
						$this->record($msg->msg."\n", 3);
						$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_UPDATE);
					}
					
					$imgCount += count($imgs);

					$Image->unlinkListingImages($imgs); // left over from filtering out existing ones
				}
				else {
					$msg = (object)['msg'=>"Nothing to remove for the letter:".chr($start)];
					$this->record($msg->msg."\n", 3);
					$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_UPDATE);
				}

				unset($list);
				$start++;
			}
		}

		$msg = (object)['msg'=>"Removed $imgCount image files from img folder"];
		$this->record($msg->msg."\n", 3);
		$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_END);

		$this->stopOutputBuffering();

		die();
	}

	public function repairListingImageData() {
		$this->updateToDb("entered repairListingImageData @1", self::UPDATE);
		// send ack back now so it won't time out on front end
		$this->exitNow("Begin repairing listing image data.");

		// $mistakenListingIdLimit = 0;
		// $opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'MistakenListingIdLimit']]);
		// if (!empty($opt))
		// 	$mistakenListingIdLimit = intval($opt[0]->value);

		// $msg = "repairListingImageData - mistakenListingIdLimit:$mistakenListingIdLimit";
		$this->updateToDb("entered repairListingImageData @2", self::UPDATE);

		$perPage = 1000;
		$page = 0;
		$path = realpath(__DIR__."/../_img/_listings/uploaded");
		$count = 0;
		$total = 0;
		while ( !empty($listings = $this->getClass('Listings')->get((object)['where'=>['active'=>1,
																					   'listhub_key'=>'notnull'], // then not user created organically
																			 'limit'=>$perPage,
																			 'page'=>$page])) ) {
			$page++;
			foreach($listings as $listing) {
				$total++;
				$needUpdate = false;
				if (!empty($listing->images)) foreach($listing->images as &$image) {
					if (isset($image->file)) {
						if (substr($image->file, 0, 4 ) != 'http') {
							$name = $path.'/'.$image->file;
							if (!file_exists($name) &&
								 isset($image->url) ) {
								unset($image->file);
								$image->file = $image->url;
								$image->processed = 0;
								unset($image->url);
								$msg = "restored listing:$listing->id, url: $image->file";
								$this->updateToDb($msg, self::UPDATE);
								$needUpdate = true;
							}
						}
						// elseif ($mistakenListingIdLimit &&
						// 		$listing->id <= $mistakenListingIdLimit) {
						// 	unset($image->processed);
						// 	$needUpdate = true;
						// 	$msg = "restored listing:$listing->id, unset processed";
						// 	$this->updateToDb($msg, self::UPDATE);
						// }
					}
				}
				if ($needUpdate) {
					$count++;
					$x = $this->getClass('Listings')->set([(object)['where'=>['id'=>$listing->id],
																	'fields'=>['images'=>$listing->images]]]);
				}
			}
		}

		$msg = "Completed repairing listing image data for $count listings out of total:$total";
		$this->updateToDb($msg, self::UPDATE);
	}

	public function cleanInactiveListings() {
		$l = $this->getClass('Listings');
		$q = new \stdClass();
		$q->what = array('id', 'listhub_key', 'active', 'updated');
		$q->where = array('tier'=>0,
						  'listhub_key'=>'notnull');
		$x = $l->get($q);
		unset($q->what);

		$maxInactiveTime = $this->getClass('Options')->get((object)['where'=>['opt'=>'MaxInactiveDurationSecs'],
																	'what'=>['value']]);
		$maxInactiveTime = empty($maxInactiveTime) ? (3600*24*30) : intval($maxInactiveTime[0]->value);

		$now = time();
		$deactivated = 0;
		$removed = 0;
		$Image = $this->getClass('Image');

		$this->doingParallel = true; // so it appends, not overwrite
		$this->openDebugFile(1);
		$this->beginOutputBuffering();

		$msg = (object)['msg'=>"Deleting ".count($x)." inactive listings, maxInactiveTime:$maxInactiveTime"];
		$this->record($msg->msg."\n", 3);
		$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_START);

		foreach($x as $data) {
			//$i++;
			if ($this->removeOneInactiveListing($data, $maxInactiveTime, $now, $Image, $l, true))
				$removed++;
			else if ($data->active != AH_INACTIVE &&
					// $data->active != AH_NEVER &&
					property_exists($data, 'listhub_key') &&
					!empty($data->listhub_key) ) { // then set it to inactive, wasn't in the stream anymore
				$q->fields = array('active'=>AH_INACTIVE);
				$q->where = array('id'=>$data->id);
				$a = array();
				$a[] = $q;
				try {
					$l->set($a);
				}
				catch(\Exception $e) {
					parseException($e);
				}
				$msg = (object)['msg'=>'deactivated id:'.$data->id, 'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600))];
				$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_INACTIVE);
				unset($a, $q->fields, $q->where);
				$deactivated++;
			}
			unset($data);
		}

		$this->optimizeTable(true);

		$data = new \stdClass();
		$data->endtime = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$data->msg = "Removed $removed listings, deactivated: $deactivated";
		$this->record($data->msg."\n", 3);
		$this->updateToDb(json_encode($data), self::CLEAN_IMAGE_END);

		$this->stopOutputBuffering();

		return new Out('OK', $removed);

	}

	protected function removeOneListing($data, $Image, $l) {
		$listing = $l->get((object)['where'=>['id'=>$data->id],
									'what'=>['images']]);
		$msg = (object)['msg'=> "Listing:".$data->id.", key:".$data->listhub_key." was last updated:".$data->updated." - being cleaned out from database"];
		$this->record($msg->msg."\n", 3);
		$this->updateToDb(json_encode($msg), self::CLEAN_FOLDER_UPDATE);
		if (!empty($listing) &&
			isset($listing[0]->images) && 
			!empty($listing[0]->images) ) {
			$msg = (object)['msg'=> "Removing ".count($listing[0]->images)." images for listing:".$data->id];
			$this->record($msg->msg."\n", 3);
			$this->updateToDb(json_encode($msg), self::CLEAN_FOLDER_UPDATE);
			$Image->unlinkListingImages($listing[0]->images);
			unset($listing);
		}
		// remove listings tags
		$sql = 'DELETE a.* FROM `'.$l->getTableName('listings-tags').'` AS a ';
		$sql.= 'INNER JOIN '.$l->getTableName().' AS b ON b.id = a.listing_id WHERE b.id = '.$data->id;
		$l->rawQuery($sql);
		// remove listings activity
		$sql = 'DELETE a.* FROM `'.$l->getTableName('listings-activity').'` AS a ';
		$sql.= 'INNER JOIN '.$l->getTableName().' AS b ON b.id = a.listing_id WHERE b.id = '.$data->id;
		$l->rawQuery($sql);
		// remove geoinfo
		$sql = 'DELETE a.* FROM `'.$l->getTableName('listings-geoinfo').'` AS a ';
		$sql.= 'INNER JOIN '.$l->getTableName().' AS b ON b.id = a.listing_id WHERE b.id = '.$data->id;
		$l->rawQuery($sql);
		// remove listings viewed
		$sql = 'DELETE a.* FROM `'.$l->getTableName('listings-viewed').'` AS a ';
		$sql.= 'INNER JOIN '.$l->getTableName().' AS b ON b.id = a.listing_id WHERE b.id = '.$data->id;
		$l->rawQuery($sql);
		// remove the listing itself
		$sql = 'DELETE FROM `'.$l->getTableName().'` WHERE `id` = '.$data->id;
		$l->rawQuery($sql);
		$msg = (object)['msg'=>"Removed from all tables, listing:".$data->id];
		$this->updateToDb(json_encode($msg), self::CLEAN_FOLDER_UPDATE);
		$this->record($msg->msg."\n", 3);
		ob_flush();
		flush();
		return true;
	}

	protected function removeOneInactiveListing(&$data, $maxInactiveTime, $now, &$Image, &$l, $dumpLog = false) {
		$diff = ($now - strtotime($data->updated));
		$overdue =  $diff > $maxInactiveTime;
		if ( (!$this->saveClickedListings ||
			  ($this->saveClickedListings && empty($data->clicks))) &&
			$overdue &&
			$data->active != AH_ACTIVE) {
			$this->record("removeOneInactiveListing - removing Listing:$data->id, updated:$data->updated, diff:$diff, maxInactiveTime:$maxInactiveTime\n", 3);
			return $this->removeOneListing($data, $Image, $l);
		}
		elseif ($dumpLog) {
			$this->record("removeOneInactiveListing - not removed Listing:$data->id, updated:$data->updated, diff:$diff, maxInactiveTime:$maxInactiveTime, clicks:$data->clicks, saveClickedListings: $this-saveClickedListings", 3);
		}
		return false;
	}

	public function concludePP($mode, $ppRunId, $optimizeTable = true) { // 0 = normal, 1 = user stopped
		// send ack back now so it won't time out on front end
		$this->exitNow("End of Parallel Processing marked.");
		
		if ($this->allowSystemBusyWindow) {
			$systemBusy = $this->getClass('Options')->get((object)['where'=>['opt'=>'SystemMaintenance'],
																		'what'=>['value']]);
			$systemBusy = empty($systemBusy) ? 0 : intval($systemBusy[0]->value);
			if ($systemBusy)
				$this->getClass('Options')->set([(object)['where'=>['opt'=>'SystemMaintenance'],
														  'fields'=>['value'=>0]]]);
		}

		$deactivated = 0;
		$removed = 0;
		if ($mode === SPECIAL_OP_NONE) // then it was not user aborted
		{
			//$index = count(glob(__DIR__."/../../../../listhub/listing_status_*"));
			$gvName = "listing_status_".date("Y-m-d", time() + ($this->timezone_adjust*3600)).".csv.gz";
			$gvFile = __DIR__."/../../../../listhub/$gvName";
			if (file_exists(($gvFile))) // rotate it
				$this->cleanUpLogFile($gvFile.'*');

			$src = __DIR__."/../listHubLog.txt";
			$reason = self::WRITE_CSV_GZ;
			$msg = "Wrote out listhub file: $gvFile";
			if ( $this->gzCompressFile($src, $gvFile) == false) {
				$reason = self::ERROR;
				$msg = "Failed to gzip $src to $gvFile";
			}
			elseif ( strpos(get_home_url(), 'engr') !== false) {
				$this->process("scp $gvFile www-data@192.168.1.3:/var/www/html/lifestyledlistings/listhub/$gvName");
			}

			$oldFlag = $this->saveLogToDb;
			$this->saveLogToDb = true;	
			$data = new \stdClass();
			$data->time = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
			$data->ppRunId = $ppRunId;
			$data->msg = $msg;
			$this->updateProgress(json_encode($data), $reason);
			$this->saveLogToDb = $oldFlag;
		
			//$count = $this->getBlockCount();
			$l = $this->getClass('Listings');
			$q = new \stdClass();
			$q->what = array('id', 'listhub_key', 'active', 'updated','clicks');
			$q->where = array('tier'=>0,
						      'listhub_key'=>'notnull');
			$listings = $l->get($q);
			unset($q->what);

			$maxInactiveTime = $this->getClass('Options')->get((object)['where'=>['opt'=>'MaxInactiveDurationSecs'],
																		'what'=>['value']]);
			$maxInactiveTime = empty($maxInactiveTime) ? (3600*24*30) : intval($maxInactiveTime[0]->value);
			$now = time();
			$Image = $this->getClass('Image');

			$this->doingParallel = true; // so it appends, not overwrite
			$this->openDebugFile(1);
			$this->beginOutputBuffering();

			if (!empty($msg))
				$this->record($msg, 3);

			$msg = (object)['msg'=>"Testing ".count($listings)." inactive listings, maxInactiveTime:$maxInactiveTime"];
			$this->record($msg->msg."\n", 3);
			$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_START);

			foreach($listings as $data) {
				//$i++;
				if ($this->removeOneInactiveListing($data, $maxInactiveTime, $now, $Image, $l))
					$removed++;
				else if ($data->active != AH_INACTIVE &&
					// $data->active != AH_NEVER &&
					property_exists($data, 'listhub_key') &&
					!empty($data->listhub_key) ) { // then set it to inactive, wasn't in the stream anymore
					$q->fields = array('active'=>AH_INACTIVE);
					$q->where = array('id'=>$data->id);
					$a = array();
					$a[] = $q;
					try {
						$l->set($a);
					}
					catch(\Exception $e) {
						parseException($e);
					}
					$msg = (object)['deactivated id:'=>$data->id, 'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600))];
					$this->updateToDb(json_encode($msg), self::CLEAN_IMAGE_INACTIVE);
					unset($a, $q->fields, $q->where);
					$deactivated++;
				}
				unset($data);
			}	
			unset($listings, $q, $l);

			$page = 0; $pagePer = 1000;
			$msg = "About to call Seller::updatePageEmailDb";
			$this->updateToDb($msg, self::BEGIN_UPDATE_EMAIL_DB);
			$this->record($msg."\n", 3);
			while(true) {
				$msg = "Seller::updatePageEmailDb for page: $page";
				$this->updateToDb($msg, self::PAGE_START_UPDATE_EMAIL_DB);
				$this->record($msg."\n", 3);
				$out = $this->getClass('Sellers', 1)->updatePageEmailDb($page, $pagePer); //, true);
				if ($out->status == 'fail')
					break;
				$page++;
				$msg = "Seller::updatePageEmailDb next page: $page";
				$this->updateToDb($msg, self::PAGE_END_UPDATE_EMAIL_DB);
				$this->record($msg."\n", 3);
			}

			$msg = "Completed call to Seller::updatePageEmailDb, removed $removed listings";
			$this->updateToDb($msg, self::END_UPDATE_EMAIL_DB);
			$this->record($msg."\n", 3);

			$ids = $this->getClass('Listings')->get((object)['what'=>['id'],
															 'orderby'=>'clicks',
															 'order'=>'desc',
															 'limit'=>100]);
			$updates = [];
			foreach($ids as $id)
				$updates[] = $id->id;
			try {
				$this->getClass('Options')->set([(object)['where'=>['opt'=>'ListingsMostViewed'],
														  'fields'=>['value'=>json_encode($updates)]]]);
			}
			catch( \Exception $e) {
				$this->record("Exception while setting ListingsMostViewed\n", 3);
			}
			unset($ids, $updates);
		}

		$msg = "Starting median value consolidation";
		$this->updateToDb($msg, self::CONCLUDE_PP_MEDIAN_VALUES_CONSOLIDATE_DAILY);
		$this->getClass('MedianPrices', 1)->consolidateDailyMedianPrices();

		// check tag spaces
		// $msg = "Start Tag Space check listings";
		// $this->updateToDb($msg, self::CONCLUDE_PP_TAG_SPACE_CHECK_LISTING);
		// $this->checkListingTagSpaces();

		// $msg = "Start Tag Space check cities";
		// $this->updateToDb($msg, self::CONCLUDE_PP_TAG_SPACE_CHECK_CITY);
		// $this->checkCityTagSpaces();

		if ($optimizeTable) {
			$msg = "Starting table optimizations";
			$this->updateToDb($msg, self::CONCLUDE_PP_OPTIMIZE_TABLES);
			$this->optimizeTable(true);
		}
		// mark end of PP
		$data = new \stdClass();
		$data->endtime = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$data->ppRunId = $ppRunId;
		$data->mode = $mode;
		$data->deactivated = $deactivated;
		$data->msg = "Removed $removed listings, deactivated: $deactivated, mode: $mode";
		$this->record($data->msg."\n", 3);
		$this->updateToDb(json_encode($data), self::END_PARALLEL);

		$this->stopOutputBuffering();
	}

	public function checkListingTagSpaces($doAll = false) {
		$sql = 'SELECT a.id, a.flags, b.lat, b.lng FROM '.$this->getClass('Listings')->getTableName().' AS a ';
		$sql.= 'INNER JOIN '.$this->getClass('ListingsGeoinfo')->getTableName().' AS b ';
		$sql.= 'ON a.id = b.listing_id ';
		$sql.= 'WHERE b.lat != -1 AND a.active != 0 ';
		if (!$doAll)
			$sql.= 'AND !(a.flags & '.LISTING_DID_TAG_SPACE_CHECK.') ';

		$page = 0;
		$pagePer = 1000;
		$done = false;
		$total = 0;
		while ( !$done ) {
			$result = $this->getClass('Listings')->rawQuery( $sql.' LIMIT '.($page * $pagePer).','.$pagePer);
			if (!empty($result)) {
				foreach($result as $loc) {
					if ($this->updateListingFromTagSpace($loc))
						$total++;
				}
				unset($result);
			}
			else $done = true;
			$page++;
			$this->log("checkListingTagSpaces - finished ".($page*$pagePer)." listings so far", 3);
		}
		return new Out('OK', 'Total listings processed is '.$total);
	}

	public function updateListingFromTagSpace($loc) {
		if ($loc->lat == -1 ||
			$loc->lng == -1 ||
			!$loc->lat ||
			!$loc->lng)
			return false;

		$listingTag = [	'listing_id' => $loc->id,
						'flags'   => LISTING_TAG_ADDED_USING_TAG_SPACE,
						'clicks'  => 0 ]; // reuse in loop
		$tagSpaces = $this->getClass('TagsSpace', 1)->get((object)[	'greaterthanequal'=>['north'=>$loc->lat],
															    	'lessthanequal'=>['south'=>$loc->lat],
															    	'lessthanequal2'=>['west'=>$loc->lng],
															    	'greaterthanequal2'=>['east'=>$loc->lng],
															     	'where'=>['type'=> 0 ] ]); // listing type
		if (empty($tagSpaces))
			return false;

		$existing = $this->getClass('ListingsTags')->get((object)['where'=>['listing_id'=>$loc->id]]);
		$existingTagList = [];
		if (!empty($existing)) {
			foreach($existing as $tag) $existingTagList[] = intval($tag->tag_id);
			unset($existing);
		}

		$count = 0;
		foreach($tagSpaces as $space) {
			if (!empty($space->meta)) foreach($space->meta as $meta) {
				if ($meta->action == TAG_SPACE_LIST &&
					!empty($meta->tags)) foreach($meta->tags as $tag=>$data) {
					$tag = intval($tag);
					if (in_array($tag, $existingTagList))
						continue;

					$listingTag['tag_id'] = $tag;
					$existingTagList[] = $tag;
					$this->getClass('ListingsTags')->add((object)$listingTag);
					$this->record("Added to listing_id:$loc->id, tagId:$tag from TagSpace:$space->id", 3);
					$count++;
				}
				unset($meta);
			}
			unset($space);
		}
		
		if ($count) {
			$this->record("Added to listing_id:$loc->id, count:$count tags, flag before: $loc->flags", 3);
			if ( !($loc->flags & LISTING_DID_TAG_SPACE_CHECK) ) {
				$x = $this->getClass('Listings')->set([(object)['where'=>['id'=>$loc->id],
														   	    'fields'=>['flags'=>($loc->flags | LISTING_DID_TAG_SPACE_CHECK)]]]);
				$this->record("updateListingFromTagSpace - updating listing:$loc->id with LISTING_DID_TAG_SPACE_CHECK was ".(!empty($x) ? "successful" : "failure"), 3);
			}
		}
		unset($existingTagList, $tagSpaces, $listingTag);
		return $count;
	}

	public function checkCityTagSpaces($doAll = false) {
		$sql = 'SELECT a.id, a.flags, a.lat, a.lng FROM '.$this->getClass('Cities')->getTableName().' AS a ';
		$sql.= 'WHERE a.lat != -1 ';
		if (!$doAll)
			$sql.= 'AND !(a.flags & '.CITY_DID_TAG_SPACE_CHECK.') ';

		$page = 0;
		$pagePer = 1000;
		$done = false;
		$total = 0;
		while ( !$done ) {
			$result = $this->getClass('Cities')->rawQuery( $sql.' LIMIT '.($page * $pagePer).','.$pagePer);
			if (!empty($result)) {
				foreach($result as $loc) {
					if ($this->updateCityFromTagSpace($loc->id, $loc))
						$total++;
				}
				unset($result);
			}
			else $done = true;
			$page++;
			$this->log("checkCityTagSpaces - finished ".($page*$pagePer)." cities so far", 3);
		}
		return new Out('OK', 'Total cities processed is '.$total);
	}

	public function updateCityFromTagSpace($cityId, $loc) {
		$this->record("updateCityFromTagSpace entered for $cityId", 3);
		if (empty($cityId))
			return false;

		if ($loc->lat == -1 ||
			$loc->lng == -1 ||
			!$loc->lat ||
			!$loc->lng)
			return false;

		$this->record("updateCityFromTagSpace entered for city:$cityId", 3);
		$tagSpaces = $this->getClass('TagsSpace', 1)->get((object)['greaterthanequal'=>['north'=>$loc->lat],
														    	'lessthanequal'=>['south'=>$loc->lat],
														    	'lessthanequal2'=>['west'=>$loc->lng],
														    	'greaterthanequal2'=>['east'=>$loc->lng],
														    	'where'=>['type'=> 1 ]]); // city type
		if (empty($tagSpaces)) {
			$this->record("updateCityFromTagSpace no TagSpaces found for city:$cityId", 3);
			return false;
		}

		$cityTag = ['city_id' => $cityId,
					'flags'   => CITY_TAG_ADDED_USING_TAG_SPACE,
					'clicks'  => 0]; // reuse in loop
		$count = 0;
		$existing = $this->getClass('CitiesTags')->get((object)['where'=>['city_id'=>$cityId]]);
		$existingTagList = [];
		if (!empty($existing)) {
			foreach($existing as $tag) $existingTagList[] = intval($tag->tag_id);
			unset($existing);
		}

		foreach($tagSpaces as $space) {
			if (!empty($space->meta)) foreach($space->meta as $meta) {
				if ($meta->action == TAG_SPACE_LIST &&
					!empty($meta->tags)) foreach($meta->tags as $tag=>$data) {
					if (gettype($data) == 'string') // very old style, mirrors $tag data only
						$score = 8; // just to make it show up
					else
						$score = isset($data->score) ? $data->score : 8;
					$tag = intval($tag);
					if (in_array($tag, $existingTagList))
						continue;

					$cityTag['tag_id'] = $tag;
					$cityTag['score']  = $score;
					$existingTagList[] = $tag;
					$this->getClass('CitiesTags')->add((object)$cityTag);
					$this->record("Added to city_id:$cityId, tagId:$tag, score:$score from TagSpace:$space->id", 3);
					$count++;
				}
				unset($meta);
			}
			unset($space);
		}
		
		if ($count) {
			$this->record("Added to city_id:$cityId, count:$count tags", 3);
			if ( !($loc->flags & CITY_DID_TAG_SPACE_CHECK) ) {
				$x = $this->getClass('Cities')->set([(object)['where'=>['id'=>$loc->id],
														   	  'fields'=>['flags'=>($loc->flags | CITY_DID_TAG_SPACE_CHECK)]]]);
				$this->record("updateCityFromTagSpace - updating city:$loc->id with CITY_DID_TAG_SPACE_CHECK was ".(!empty($x) ? "successful" : "failure"), 3);
			}
		}
		unset($existingTagList, $tagSpaces, $cityTag);
		return $count;
	}

	public function optimizeTable($internalCall = false) {
		if ($internalCall)
			$this->runScriptOnWeb01('sysdown');

		$Listings = $this->getClass('Listings');
		$tbl_name = $Listings->getTableName();
		//defrag
		// $sql = "ALTER TABLE $tbl_name ENGINE=INNODB";
		// $this->updateToDb("Defrag $tbl_name", self::CLEAN_SQL_TABLE);
		// $$Listings->rawQuery($sql);
		// optimize
		$sql = "OPTIMIZE TABLE $tbl_name";
		$this->updateToDb("Optimize $tbl_name", self::CLEAN_SQL_TABLE);
		// $this->record($sql."\n", 3);
		$Listings->rawQuery($sql);

		$ListingsTags = $this->getClass('ListingsTags');
		$tbl_name = $ListingsTags->getTableName();
		//defrag
		// $sql = "ALTER TABLE $tbl_name ENGINE=INNODB";
		// $this->updateToDb("Defrag $tbl_name", self::CLEAN_SQL_TABLE);
		// $$ListingsTags->rawQuery($sql);
		// optimize
		$sql = "OPTIMIZE TABLE $tbl_name";
		$this->updateToDb("Optimize $tbl_name", self::CLEAN_SQL_TABLE);
		// $this->record($sql."\n", 3);
		$Listings->rawQuery($sql);

		$ListingsGeoinfo = $this->getClass('ListingsGeoinfo');
		$tbl_name = $ListingsGeoinfo->getTableName();
		//defrag
		// $sql = "ALTER TABLE $tbl_name ENGINE=INNODB";
		// $this->updateToDb("Defrag $tbl_name", self::CLEAN_SQL_TABLE);
		// $$ListingsGeoinfo->rawQuery($sql);
		// optimize
		$sql = "OPTIMIZE TABLE $tbl_name";
		$this->updateToDb("Optimize $tbl_name", self::CLEAN_SQL_TABLE);
		// $this->record($sql."\n", 3);
		$ListingsGeoinfo->rawQuery($sql);

		$ListingsActivity = $this->getClass('ListingsActivity');
		$tbl_name = $ListingsActivity->getTableName();
		//defrag
		// $sql = "ALTER TABLE $tbl_name ENGINE=INNODB";
		// $this->updateToDb("Defrag $tbl_name", self::CLEAN_SQL_TABLE);
		// $$ListingsActivity->rawQuery($sql);
		// optimize
		$sql = "OPTIMIZE TABLE $tbl_name";
		$this->updateToDb("Optimize $tbl_name", self::CLEAN_SQL_TABLE);
		// $this->record($sql."\n", 3);
		$ListingsActivity->rawQuery($sql);

		$nextTable = $this->getClass('Cities');
		$tbl_name = $nextTable->getTableName();
		// optimize
		$sql = "OPTIMIZE TABLE $tbl_name";
		$this->updateToDb("Optimize $tbl_name", self::CLEAN_SQL_TABLE);
		// $this->record($sql."\n", 3);
		$nextTable->rawQuery($sql);

		$nextTable = $this->getClass('CitiesTags');
		$tbl_name = $nextTable->getTableName();
		// optimize
		$sql = "OPTIMIZE TABLE $tbl_name";
		$this->updateToDb("Optimize $tbl_name", self::CLEAN_SQL_TABLE);
		// $this->record($sql."\n", 3);
		$nextTable->rawQuery($sql);

		// $nextTable = $this->getClass('QuizActivity');
		// $tbl_name = $nextTable->getTableName();
		// // optimize
		// $sql = "OPTIMIZE TABLE $tbl_name";
		// $this->updateToDb("Optimize $tbl_name", self::CLEAN_SQL_TABLE);
		// // $this->record($sql."\n", 3);
		// $nextTable->rawQuery($sql);

		// $nextTable = $this->getClass('QuizQuery');
		// $tbl_name = $nextTable->getTableName();
		// // optimize
		// $sql = "OPTIMIZE TABLE $tbl_name";
		// $this->updateToDb("Optimize $tbl_name", self::CLEAN_SQL_TABLE);
		// // $this->record($sql."\n", 3);
		// $nextTable->rawQuery($sql);

		$nextTable = $this->getClass('Sellers');
		$tbl_name = $nextTable->getTableName();
		// optimize
		$sql = "OPTIMIZE TABLE $tbl_name";
		$this->updateToDb("Optimize $tbl_name", self::CLEAN_SQL_TABLE);
		// $this->record($sql."\n", 3);
		$nextTable->rawQuery($sql);

		$nextTable = $this->getClass('Sessions');
		$tbl_name = $nextTable->getTableName();
		// optimize
		$sql = "OPTIMIZE TABLE $tbl_name";
		$this->updateToDb("Optimize $tbl_name", self::CLEAN_SQL_TABLE);
		// $this->record($sql."\n", 3);
		$nextTable->rawQuery($sql);

		if ($internalCall)
			$this->runScriptOnWeb01('sysup');

		$nextTable = $this->getClass('SellersEmailDb');
		$tbl_name = $nextTable->getTableName();
		// optimize
		$sql = "OPTIMIZE TABLE $tbl_name";
		$this->updateToDb("Optimize $tbl_name", self::CLEAN_SQL_TABLE);
		// $this->record($sql."\n", 3);
		$nextTable->rawQuery($sql);


		$nextTable = $this->getClass('MedianPrices');
		$tbl_name = $nextTable->getTableName();
		// optimize
		$sql = "OPTIMIZE TABLE $tbl_name";
		$this->updateToDb("Optimize $tbl_name", self::CLEAN_SQL_TABLE);
		// $this->record($sql."\n", 3);
		$nextTable->rawQuery($sql);
	}

	public function pausePP($ppRunId, $doingImagePP = 0) {
		$y = $this->getLastStartEntry($doingImagePP);
		$startUpData = json_decode($y->data);
		if ($ppRunId != $startUpData->ppRunId) {
			$msg = "ppRunId: $ppRunId does not match the current ppRunId:".$startUpData->ppRunId;
			$out = new Out('fail', $msg);
			return $out;
		}

		// mark end of pause PP
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;	
		$data = new \stdClass();
		$data->pausetime = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$data->ppRunId = $ppRunId;
		$this->updateProgress(json_encode($data), self::PAUSE_PARALLEL);
		$this->saveLogToDb = $oldFlag;
		$out = new Out('OK', "Pause of Parallel Processing marked.");
		return $out;
	}

	public function resumePP($ppRunId, $doingImagePP = 0) {
		$y = $this->getLastStartEntry($doingImagePP);
		$startUpData = json_decode($y->data);
		if ($ppRunId != $startUpData->ppRunId) {
			$msg = "ppRunId: $ppRunId does not match the current ppRunId:".$startUpData->ppRunId;
			$out = new Out('fail', $msg);
			return $out;
		}

		// mark end of resume PP
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;	
		$data = new \stdClass();
		$data->resumetime = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$data->ppRunId = $ppRunId;
		$this->updateProgress(json_encode($data), self::RESUME_PARALLEL);
		$this->saveLogToDb = $oldFlag;
		$out = new Out('OK', "Resume of Parallel Processing marked.");
		return $out;
	}

	public function haltPP($ppRunId, $doingImagePP = 0) {
		$y = $this->getLastStartEntry($doingImagePP);
		$startUpData = json_decode($y->data);
		if ($ppRunId != $startUpData->ppRunId) {
			$msg = "ppRunId: $ppRunId does not match the current ppRunId:".$startUpData->ppRunId;
			$out = new Out('fail', $msg);
			return $out;
		}

		// mark end of halt PP
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;	
		$data = new \stdClass();
		$data->halttime = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$data->ppRunId = $ppRunId;
		$this->updateProgress(json_encode($data), self::HALT_PARALLEL);
		$this->saveLogToDb = $oldFlag;
		$out = new Out('OK', "Halt of Parallel Processing marked.");
		return $out;
	}

	public function startRemotePP($remote) {
		// mark remote PP request
		$data = new \stdClass();
		$data->starttime = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$data->remoteRequester = $remote;
		$this->updateToDb(json_encode($data), self::REMOTE_PARALLEL);
		$out = new Out('OK', "Remote start of Parallel Processing marked.");
		return $out;
	}

	public function startRemoteP2Db($remote) {
		// mark remote PP request
		$data = new \stdClass();
		$data->starttime = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$data->remoteRequester = $remote;
		$this->updateToDb(json_encode($data), self::REMOTE_P2DB);
		$out = new Out('OK', "Remote start of Parsing to DB marked.");
		return $out;
	}

	public function startRemoteImageProcessing($remote,
												$mode,
											  	$op,
											  	$onlyNew,
											  	$fromLast,
											  	$numSubDomains,
											  	$unitsPerChunk,
											  	$numChunkPerSubDomain,
											  	$cpuLimit) {
		// mark remote PP request
		$data = new \stdClass();
		$data->starttime = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$data->remoteRequester = $remote;
		$data->mode = $mode;
		$data->op = $op;
		$data->onlyNew = $onlyNew;
		$data->fromLast = $fromLast;
		$data->numSubDomains = $numSubDomains;
		$data->unitsPerChunk = $unitsPerChunk;
		$data->numChunkPerSubDomain = $numChunkPerSubDomain;
		$data->cpuLimit = $cpuLimit;
		$this->updateToDb(json_encode($data), self::REMOTE_IMAGE_PROCESSING);
		$out = new Out('OK', "Remote start of Parsing to DB marked.");
		return $out;
	}

	public function reloadPage($remote) {
		// mark remote PP request
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;	
		$data = new \stdClass();
		$data->starttime = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$data->remoteRequester = $remote;
		$this->updateProgress(json_encode($data), self::RELOAD_PAGE);
		$this->saveLogToDb = $oldFlag;
		$out = new Out('OK', "Request reload of page marked.");
		return $out;
	}

	public function lockSql($lockIt) {
		// mark remote PP request
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;	
		$data = new \stdClass();
		$data->starttime = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$data->command = "Issuing ".($lockIt ? 'LOCK_SQL' : 'UNLOCK_SQL');
		$this->updateProgress(json_encode($data), $lockIt ? self::LOCK_SQL : self::UNLOCK_SQL);
		$this->saveLogToDb = $oldFlag;
		$out = new Out('OK', "Request ".($lockIt ? 'lock' : 'unlock')." SQL of remote marked.");
		return $out;
	}


	public function logHeader()
	{
		$ini = php_ini_loaded_file();
		$ver = phpversion();
		$this->record("PHP version: $ver, ini file used is: $ini\n", 3);
		$this->record("Parser version: ".$this->version()."\n", 3);
		$this->record("This task was started ".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600))."\n", 3);
		$limit = ini_get('memory_limit');
		$memUsedWhole = memory_get_usage(true);
		$memUsedeMalloc = memory_get_usage(false);
		$this->record("memory limit: $limit, SystemUsed: $memUsedWhole, eMallocUsed: $memUsedeMalloc\n", 2);
		$this->record("mininumPrice: $this->minimumPrice, minimumTagCount: $this->minimumTagCount, debugLevel: $this->debugLevel\n", 3);
	}

	protected function listhubCSVTimestamp($set = true){
		$t = $this->getClass("Options");
		$q = new \stdClass();
		$q->where = array('opt'=>'listhubCSVTimestamp');
		$x = $t->get($q);
		if ($set) {// then we are making a new one
			if (!empty($x)) // update with new
				$t->delete($q->where); // get rid of the old
		}
		else {
			if (!empty($x))
				return $x[0]->value;
		}
		// add new one
		$timestamp = str_replace(" ","_",date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)));
		$data = array('opt'=>'listhubCSVTimestamp',
					'value'=>$timestamp);
		$x = $t->add((object)$data);
		return $timestamp;
	}

	protected function openDebugFile($which = 0)
	{
		switch ($which) {
			case 0: $name = __DIR__."/../listhubData.txt"; break;
			case 1: $name = __DIR__."/../listhubParallel.txt"; break;
            case 2: $name = __DIR__."/../listHubLog.txt"; break;
            case 3: $name = __DIR__."/../listhubChunkRecovery.txt"; break;
            case 4: $name = __DIR__."/../listhubImagePP.txt"; break;
            case 5: $name = __DIR__."/../listhubImageCustomPP.txt"; break;
            //case 2: $name = __DIR__."/../listing_status_".$this->listhubCSVTimestamp(false)."csv"; break;
		}			

		if ($this->haveOutputFile)
		{
			$attr = $this->doingParallel || $which == 3 ? "a" : "w";
			if ($which == 0 ||
				$which == 1 ||
				$which == 3 ||
				$which == 4 ||
				$which == 5) {
				if ( ($this->out = fopen($name, $attr)) == false)
				{
					$this->record("Failed to open debug file: $name\n", 5);
					$this->haveOutputFile = false;
				}
			}
			else if ($which == 2) {
				if ( ($this->listHubLog = fopen($name, $attr)) == false)
				{
					$this->record("Failed to open debug file: $name\n", 5);
					$this->haveOutputFile = false;
				}
			}
		}
	}

	protected function createDebugFiles()
	{
		$this->updateToDb("about to create listhubData.txt");
		$this->createDebugFile();
		$out = $this->out;
		$this->updateToDb("about to create listhubParallel.txt");
		$this->createDebugFile(1);
		fclose($this->out); // save the parallel stuff for later
		$this->out = $out;  // set it to listhubdata.txt
		$this->updateToDb("exiting createDebugFiles");
	}

	public function createDebugFile($which = 0)
	{
		switch ($which) {
			case 0: $name = __DIR__."/../listhubData.txt"; break;
			case 1: $name = __DIR__."/../listhubParallel.txt"; break;
			case 3: $name = __DIR__."/../listhubImagePP.txt"; break;
			case 4: $name = __DIR__."/../listhubImageCustomPP.txt"; break;
            case 2: $name = 
            	__DIR__."/../listHubLog.txt"; 
            	$this->listhubCSVTimestamp(); // create a row in Options to record the timestamp that'll be used for the gz file.
            	break;
            //case 2: $name = __DIR__."/../listing_status_".$this->listhubCSVTimestamp()."csv"; break;
		}

		if ($this->haveOutputFile)
		{
			$attr = "w";
			if ($which == 0 ||
				$which == 1 ||
				$which == 3 ||
				$which == 4) {
				if ( ($this->out = fopen($name, $attr)) == false)
				{
					$this->record("Failed to create debug file: $name\n", 5);
					$this->haveOutputFile = false;
				}
			}
			else if ($which == 2) {
				if ( ($this->listHubLog = fopen($name, $attr)) == false)
				{
					$this->record("Failed to open debug file: $name\n", 5);
					$this->haveOutputFile = false;
				}
			}

		}
	}
	public function dump($buffer)
	{
		flock($this->out, LOCK_EX);
		fwrite($this->out, $buffer);
		fflush($this->out);
		flock($this->out, LOCK_UN);
		return "";
	}

	public function beginOutputBuffering() {
		if ($this->haveOutputFile)
			ob_start(function($buf) {
				$this->dump($buf);
			});
	}

	public function stopOutputBuffering() {
		if ($this->haveOutputFile)
		{
			ob_end_flush();
			if ($this->out)
				fclose($this->out);
		}
	}

	protected function record($buffer, $level = 1, $reason = self::UPDATE, $force = 0)
	{
		if ($level >= $this->debugLevel)
		{
			$this->updateProgress($buffer, $reason, $force);
			if ($this->haveOutputFile)
				echo $buffer.(strpos($buffer, "\n") === false ? "\n" : '');
		}
		
		if (!$this->doingParallel || $force) // when doing parallel block processing, prefer to flush the ob at end of each block so the data doesn't interlieave between processes
		{
			ob_flush();
			flush();
		}
	}

	protected function setXhr()
	{
		$this->debugLevel = 3; // force this so we don't output too much
		header("Content-Type: application/octet-stream\r\n");
		header("Cache-Control: no-cache\r\n"); // recommended to prevent caching of event data.
		header("Content-Encoding: none\r\n");//disable apache compressed

		// Turn off output buffering
		ini_set('output_buffering', 'off');
		// Turn off PHP output compression
		ini_set('zlib.output_compression', false);
		// Implicitly flush the buffer(s)
		ini_set('implicit_flush', true);
		ob_implicit_flush(true);
		// Clear, and turn off output buffering
		while (ob_get_level() > 0) {
		    // Get the curent level
		    $level = ob_get_level();
		    // End the buffering
		    ob_end_clean();
		    // If the current level has not changed, abort
		    if (ob_get_level() == $level) break;
		}   

		ob_end_flush();
		ob_start();
	}

	public function parseException($e, $obj = null){
		if ($this->haveOutputFile) $out = array( 'trace'=>$e->getTrace(),'message'=>$e->getMessage(),'code'=>$e->getCode(),'file'=>$e->getFile(),'line'=>$e->getLine() );
		else $out = array('message'=>$e->getMessage());
		if ($obj != null)
			$out['obj'] = print_r($obj, true);
		$this->updateToDb("Exception caught: ".print_r($out, true), self::FATAL);
		return $out;
	}

	// start/end time are values from microtime();
	protected function diffTime($starttime, $endtime)
	{
		//time before
		list($usec, $sec) = explode(' ',$starttime);
		$querytime_before = ((float)$usec + (float)$sec);
		/* your code */

		//time after
		list($usec, $sec) = explode(' ',$endtime);
		$querytime_after = ((float)$usec + (float)$sec);
		$diff = (float)($querytime_after - $querytime_before);
		return number_format($diff, 4, '.', '');
	}

	function remote_filesize($url, $user = "", $pw = ""){
		ob_start();
		try {
			$this->updateToDb("remote_filesize before curl_init for url:$url");
		    $ch = curl_init($url);
		    if (!$ch) {
		    	$this->updateToDb("Failed to curl_init");
		    	throw new Exception("Failed to curl_init");
		    }
		    curl_setopt($ch, CURLOPT_HEADER, 1);
		    curl_setopt($ch, CURLOPT_NOBODY, 1);
		 
		    if(!empty($user) && !empty($pw))
		    {
		        $headers = array('Authorization: Basic ' .  base64_encode("$user:$pw"));
		        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    }
		 
			$this->updateToDb("remote_filesize before curl_exec");
		    $ok = curl_exec($ch);
		    curl_close($ch);
		    $head = ob_get_contents();
		    ob_end_clean();
		 
		    //$regex = '/Content-Length:\s([0-9].+?)\s/';
		    $regex = '/Content-length:\s([0-9]+)/';
		    $count = preg_match($regex, $head, $matches);
		 
		 	$result = isset($matches[1]) ? $matches[1] : "unknown";
			$this->updateToDb("remote_filesize returning with $result");
		    return $result;
		}
		catch(\Exception $e) {
			ob_end_clean();
			parseException($e);
			$this->updateToDb("caught exception in remote_filesize");
		}
	}

	 /**
	 * GZIPs a file on disk (appending .gz to the name)
	 *
	 * From http://stackoverflow.com/questions/6073397/how-do-you-create-a-gz-file-using-php
	 * Based on function by Kioob at:
	 * http://www.php.net/manual/en/function.gzwrite.php#34955
	 * 
	 * @param string $source Path to file that should be compressed
	 * @param integer $level GZIP compression level (default: 9)
	 * @return string New filename (with .gz appended) if success, or false if operation fails
	 */
	protected function gzCompressFile($source, $dest, $level = 9){ 
	    $mode = 'wb' . $level; 
	    $error = false; 
	    if ($fp_out = gzopen($dest, $mode)) { 
	        if ($fp_in = fopen($source,'rb')) { 
	            while (!feof($fp_in)) 
	                gzwrite($fp_out, fread($fp_in, 1024 * 512)); 
	            fclose($fp_in); 
	        } else {
	            $error = true; 
	        }
	        gzclose($fp_out); 
	    } else {
	        $error = true; 
	    }
	    if ($error)
	        return false; 
	    else
	        return $dest; 
	} 


	protected function uncompress($file_name)
	{
		// Raising this value may increase performance
		try {
			$buffer_size = 4096; // read 4kb at a time
			$out_file_name = str_replace('.gz', '', $file_name).'.xml'; 
			// Open our files (in binary mode)
			$file = gzopen($file_name, 'rb');
			$out_file = fopen($out_file_name, 'wb'); 
			// Keep repeating until the end of the input file
			while(!gzeof($file)) {
			// Read buffer-size bytes
			// Both fwrite and gzread and binary-safe
			  fwrite($out_file, gzread($file, $buffer_size));
			}  
			// Files are done, close files
			fclose($out_file);
			gzclose($file);
			$this->record("Decompressed from $file_name to $out_file_name\n");
			return $out_file_name;
		}
		catch(\Exception $e)
		{
			$this->record( "Caught an exception uncompressing file: $file_name, error:".$e->getMessage()."\n", 5);
			return false;
		}
	}

	protected function getDirectorySize($path){
	    $bytestotal = 0;
	    //$path = realpath($path);
	    if($path!==false){
	    	$directory = new \RecursiveDirectoryIterator($path, \FilesystemIterator::FOLLOW_SYMLINKS);
			$filter = new \RecursiveCallbackFilterIterator($directory, function ($current, $key, $iterator) {
							  // Skip hidden files and directories.
							  if ($current->getFilename()[0] === '.') {
							    return FALSE;
							  }
							  if ($current->isDir()) {
							    // Only recurse into intended subdirectories.
							    return $current->getFilename() === 'wanted_dirname';
							  }
							  else {
							    // Only consume files of interest.
							    return strpos($current->getFilename(), 'wanted_filename') === 0;
							  }
							});
			$iterator = new \RecursiveIteratorIterator($filter);
			foreach ($iterator as $object) {
	        //foreach(new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path, \FilesystemIterator::SKIP_DOTS)) as $object){
	        	try {
	        		$size = isset($object) ? $object->getSize() : 0;
	        		if ($size)
	            		$bytestotal += $size;
	            }
	            catch(\TypeError $e) {
	            	$this->record("TypeError exception: ".$e->getMessage()."\n");
	            }
	            catch(\Exception $e) {
	            	$this->record("Failed to getSize, exception: ".$e->getMessage()."\n");
	            }
	        }
	    }
	    return $bytestotal;
	}

	protected function updateStartParallelData($value, $type) {
		$opt = $type == self::RESTART_PARALLEL_IP  ? 'RESTART_PARALLEL_IP' : ($type ==  self::RESTART_PARALLEL_CUSTOM_IP ? 'RESTART_PARALLEL_CUSTOM_IP' : 'RESTART_PARALLEL');
		$cur = $this->getClass('Options')->get((object)['where'=>['opt'=>$opt]]);
		if (empty($cur))
			$this->getClass('Options')->add(['opt'=>$opt,
										     'value'=>$value]);
		else
			$this->getClass('Options')->set([(object)['where'=>['opt'=>$opt],
													  'fields'=>['value'=>$value]]]);
	}


	protected function updateProgress($msg, $type, $force = 0, $extra = 0)
	{
		if (!$this->saveLogToDb) return;
		if (!$force &&
			($this->specialOpType == IMAGE_BORDER_PARALLEL_BATCH ||
			 $this->specialOpType == IMAGE_BORDER_PARALLEL_CUSTOM_TASK ||
			 $this->specialOpType == SPECIAL_PARSE_BATCH) ) {
			$this->updateToDbList[] = (object)['data'=>$msg,
											   'type'=>$type,
											   'intData'=>$extra];
			return;
		}
		$progress = $this->getClass("ListhubProgress");
		$q = new \stdClass();
		$q->type = $type;
		$q->data = $msg;
		$q->intData = $extra;
		$this->lock();
		$this->getClass("ListhubProgress")->add($q);
		$this->unlock();
		unset($q);
	}

	protected function getProgressType($type)
	{
		$progress = $this->getClass("ListhubProgress");
		$q = new \stdClass(); 
		$q->where = array('type'=>$type);
		try { 
			$x = $progress->get($q); 
			if(!empty($x))
				return $x[0];
			else
				return false;
		}
		catch (\Exception $e){ return array('error'=>$this->parseException($e)); }
	}

	public function getProgress()
	{
		if ( ($x = $this->getProgressType(self::FATAL)))
			return $x;

		if ( ($x = $this->getProgressType(self::RESULT)))
			return $x;

		$progress = $this->getClass("ListhubProgress");
		if ( ($x = $progress->getLast('id')) ) {
			return $x[0];
		}
	}

	public function getThisProgress($id)
	{
		$progress = $this->getClass("ListhubProgress");
		$q = new \stdClass(); 
		$q->where = array('id'=>$id);
		try { 
			$x = $progress->get($q); 
			if(!empty($x))
				return $x[0];
			else
				return false;
		}
		catch (\Exception $e){ return array('error'=>$this->parseException($e)); }
	}

	public function getBlockCount($specialOperation = SPECIAL_OP_NONE)
	{
		$doingImagePP = !($specialOperation >= IMAGE_BORDER_PARALLEL && $specialOperation <= IMAGE_BORDER_PARALLEL_BATCH) ? 0 :
						 ($specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 1 : 2);
		$t = $this->getClass("ListHubData");
		$blockTable = !$doingImagePP ? 'ListhubBlocks' : ($doingImagePP == 1 ? 'ImageBlocks' : 'CustomImageBlocks');
		try {
			$x = $this->getClass($blockTable)->getLast('id');
			if ($x)
				return $x[0]->id;
			else
				return "0";
		}
		catch(\Exception $e) { return array('error'=>$this->parseException($e)); }
	}

	public function getLastProgressCount()
	{
		try {
			$x = $this->getClass("ListhubProgress")->getLast('id');
			if ($x)
				return $x[0]->id;
			else
				return "0";
		}
		catch(\Exception $e) { return array('error'=>$this->parseException($e)); }
	}

	protected function getStartParallelData($doingImagePP) {
		$opt = !$doingImagePP ? 'RESTART_PARALLEL' : ($doingImagePP == 1 ? 'RESTART_PARALLEL_IP' : 'RESTART_PARALLEL_CUSTOM_IP');
		$cur = $this->getClass('Options')->get((object)['where'=>['opt'=>$opt]]);
		if (empty($cur))
			return false;
		else
			return (object)['data'=>$cur[0]->value];
	}

	public function getLastStartEntry($doingImagePP = 0)
	{
		return $this->getStartParallelData($doingImagePP);


		// $progress = $this->getClass("ListhubProgress");
		// $q = new \stdClass(); 
		// $q->where = array('type'=>$doingImagePP ? self::RESTART_PARALLEL_IP : self::RESTART_PARALLEL);
		// try { 
		// 	$x = $progress->get($q); 
		// 	if(!empty($x))
		// 		return $x[count($x)-1]; // last one..
		// 	else
		// 		return false;
		// }
		// catch (\Exception $e){ return array('error'=>$this->parseException($e)); }
	}

	public function getLastStartEntryFromLHP($doingImagePP = 0)
	{
		$progress = $this->getClass("ListhubProgress", 1);
		$q = new \stdClass(); 
		$q->where = array('type'=> !$doingImagePP ? self::RESTART_PARALLEL : ($doingImagePP == 1 ? self::RESTART_PARALLEL_IP : self::RESTART_PARALLEL_CUSTOM_IP));
		try { 
			$x = $progress->get($q); 
			if(!empty($x))
				return $x[count($x)-1]; // last one..
			else
				return false;
		}
		catch (\Exception $e){ return array('error'=>$this->parseException($e)); }
	}

	public function ping()
	{
		$load = sys_getloadavg();
		$result = array('load'=>$load[0],
						'host'=>$_SERVER['HTTP_HOST']);
		return $result;
	}

	public function pingRemote() {
		// mark end of pause PP
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;	
		$data = new \stdClass();
		$data->pingTime = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$this->updateProgress(json_encode($data), self::PING_REMOTE);
		$this->saveLogToDb = $oldFlag;
		$out = new Out('OK', "Ping remote marked.");
		return $out;
	}

	public function statsRemote() {
		// mark end of pause PP
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;	
		$data = new \stdClass();
		$data->statsTime = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$this->updateProgress(json_encode($data), self::STATS_REMOTE);
		$this->saveLogToDb = $oldFlag;
		$out = new Out('OK', "Stats remote marked.");
		return $out;
	}

	public function sendFlagToRemote($flag, $value) {
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;	
		$data = new \stdClass();
		$data->flag = $flag;
		$data->value = $value;
		$data->flagToRemoteTime = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$this->updateProgress(json_encode($data), self::FLAGS_TO_REMOTE);
		$this->saveLogToDb = $oldFlag;
		$out = new Out('OK', "sendFlagToRemote remote marked for flag:$flag, value:".($value ? $value : 0));
		return $out;
	}

	public function cleanUpLogFiles($doingImagePP = false)
	{
		if ($doingImagePP) {
			$time = " ".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
			$logMask = $doingImagePP == 1 ? __DIR__."/../listhubImagePP.txt*" : __DIR__."/../listhubImageCustomPP.txt*";
			$this->cleanUpLogFile($logMask);
			return;
		}

		$time = " ".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$logMask = __DIR__."/../listhubParallel.txt*";
		$this->cleanUpLogFile($logMask);
		//$this->updateToDb($this->cleanUpLogFile($logMask).$time);
		$logMask = __DIR__."/../listhubData.txt*";
		$this->cleanUpLogFile($logMask);
		// $this->updateToDb($this->cleanUpLogFile($logMask).$time);
		$logMask = __DIR__."/../listHubLog.txt*"; 
		$this->cleanUpLogFile($logMask);
		// $this->updateToDb($this->cleanUpLogFile($logMask).$time);
		$logMask = __DIR__."/../listhubChunkRecovery.txt*"; 
		$this->cleanUpLogFile($logMask);
		// $this->updateToDb($this->cleanUpLogFile($logMask).$time);
	}

	protected function cleanUpLogFile($logMask)
	{
		$files = array();
		foreach (glob($logMask) as $filename)
			$files[] = $filename;

		$keyFile = substr($logMask, 0, -1);

		$doClean = false;
		foreach($files as $filename)
			if ($filename == $keyFile)
			{
				$doClean = true;
				break;
			}

		if (!$doClean)
			return "No file to clean, $keyFile does not exist.";

    	$count = count($files);
		if (!$count)
			return "No log $keyFile files to clean.";
    		
    	rsort($files, SORT_STRING);
    	$max = $count > 10 ? 10 : $count;
    	foreach($files as $filename)
    	{
    		if ($count > $max)
    			unlink($filename);
    		else
    		{
    			$base = preg_replace('/\d+/', '', basename($filename));
    			$newname = __DIR__."/../".$base.$count;
    			rename($filename, $newname);
    		}
    		$count--;
    	}
    	return "Cleaned $count log $keyFile files.";
	}

	public function getListHubData($saveToDb = false, $onlyParseFeed = false, $recursed = false, $retryCount = 0) {
		$this->saveListingToDb = $saveToDb;
		$this->saveLogToDb = true;
		$this->doingParallel = false;

		if (!$recursed)
			$this->exitNow("Start list feed reading.");

		if (!$recursed) {
			$this->cleanUpLogFiles();
			$this->createDebugFile(); // create just listhubdata.txt

			$this->beginOutputBuffering();
		
			$this->logHeader();
		}

		$lastListHubDataFeedRead = $this->getClass('Options')->get((object)['where'=>['opt'=>'LastListHubDataFeedRead']]);
		if (!empty($lastListHubDataFeedRead)) {
			$now = time();
			$lastRead = strtotime($lastListHubDataFeedRead[0]->value);
			if ( ($now - $lastRead) < (3600*4) ) {// read less than 4 hours ago, so let's reuse it
				$this->record("getListHubData - is reusing data from ".number_format((($now - $lastRead)/3600.0), 2)." hrs ago", 3);
				$this->exitNow("DONE");
				sleep(2); // let front end catch up
				$count = $this->getClass('ListhubBlocks')->count();
				if ($count) {
					$this->updateProgress(strval($count), self::RESULT);
					return;
				}
			}
			else
				$this->record("getListHubData - last data was read ".number_format((($now - $lastRead)/3600.0), 2)." hrs ago", 3);
		}
		else
			$this->record("getListHubData - failed to find LastListHubDataFeedRead", 3);

		$username = 'allure';
		$password = 'YmK3k5b';
 		//$url = "http://www.listhub.net/syndication-docs/allure.xml.gz";
 		$url = "https://feeds.listhub.com/pickup/allure/allure.xml.gz";
 		//$url = "http://s3.amazonaws.com/pickup.us-east-1.listhub.net/allure/allure.xml.gz?AWSAccessKeyId=AKIAIRD2723CINUUOZWQ&Expires=1413609642&Signature=s9NfmnnBgIOJSVqy1ycDWCTXs%2Fs%3D&user=allure&channelId=allure&etag=80dab5ed2c135a6a51f6b04e8224d921";
 		/*
		$context = stream_context_create(
			array('http' => array('method' => "GET",
								  'header'  => "Authorization: Basic " . base64_encode("$username:$password"))));
		$data = file_get_contents($url, false, $context);
		*/
		$this->updateToDb("before remote_filesize");
		$this->filesize = (int)$this->remote_filesize($url, $username, $password);
		$timeout = 14400; // in secs
		//$tempFile = tempnam(__DIR__.'/../uploads/', 'alr').'gz';
		$tempFile = tempnam("", 'alr').'.gz';

		$opt = $this->getClass('Options')->get((object)['like'=>['opt'=>'activeIP']]);
		$doingImagePP = !empty($opt);

		if (!$doingImagePP) {// we need to keep the ppRunId value in RESTART_PARALLEL_IP
			if (!$recursed &&
				!$retryCount) {
				$this->record("getListHubData is emptying ListhubProgress, no LIKE 'activeIP' found", 3);
				$this->getClass("ListhubProgress")->emptyTable(true);
			}
		}
		else
			$this->record("getListHubData is preserving ListhubProgress, activeIP found", 3);

		$dir = dirname($tempFile);
		@popen('rm '.$dir.'/alr*'." $dir/alr??????", "r");
		
		$this->record("Removed all files in $dir/alr*, size of $dir is now ".$this->getDirectorySize($dir)."\n");
		$this->record("This run will process $this->listingLimit files.\n", 3);

		$this->updateToDb("invoking CURL");
		$curl = curl_version();
		$curl = $curl['version'];
		$this->record("Curl version: $curl\n", 6);
		$curl = explode('.', $curl);
		$this->record( "Writing curl file to: $tempFile, with size of $this->filesize bytes.\n", 5);
		$tmpFile = fopen($tempFile, "wb");
		$sizeUp = (int)0;
		$soFarUp = (int)0;
		set_time_limit($timeout); // set time out max value
		$startTime = microtime();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); //timeout in seconds
		curl_setopt($ch, CURLOPT_FILE, $tmpFile);
		// the download speed must be at least 1 byte per second
		curl_setopt($ch, CURLOPT_LOW_SPEED_LIMIT, 1);
		// if the download speed is below 1 byte per second for
		// more than 30 seconds curl will give up
		curl_setopt($ch, CURLOPT_LOW_SPEED_TIME, 30);
		
		//if ($this->haveOutputFile)
		if ($this->saveLogToDb)
		{
			curl_setopt($ch, CURLOPT_NOPROGRESS, false);
			if (intval($curl[1]) <  32)
				curl_setopt($ch, CURLOPT_PROGRESSFUNCTION, function($in, $size, $sofar, $sizeUp, $soFarUp){
					static $count = 0;
					if ( ($count % 400) == 0)
						$this->record("Downloaded $sofar out of $this->filesize bytes.\n", (!$this->saveLogToDb) ? 1 : 6);
					$count++;
					return 0;
				});
			else
				curl_setopt($ch, CURLOPT_XFERINFOFUNCTION, function($in, $size, $sofar, $sizeUp, $soFarUp){
					static $count = 0;
					if ( ($count % 400) == 0)
						$this->record("Downloaded $sofar out of $this->filesize bytes.\n", (!$this->saveLogToDb) ? 1 : 6);
					$count++;
					return 0;
				});
		}
		
		$output = curl_exec($ch);
		$endTime = microtime();
		if ($output === false) {
			if ($this->retryCurlCount < 3) {
				$this->record("Curl returned false after ".$this->diffTime($startTime, $endTime)." secs, retrying", 3);
				$this->retryCurlCount++;
				curl_close($ch);
				fclose($tmpFile);
				@popen("rm $tmpFile", 'r');
				$this->getListHubData($saveToDb, $onlyParseFeed, true);
				return;
			}
		}

		$info = curl_getinfo($ch);
		curl_close($ch);
		fclose($tmpFile);
		set_time_limit(0);

		$this->record("Time to read list hub data: ".$this->diffTime($startTime, $endTime)." secs\n", 3);

		if ($this->debugLevel < 3)
			print_r($info);	

		if (!$output)
		{
			$this->record("Failed to retrieve listHub data file.\n", 6, self::FAILED_PARSE_TO_DB, 1);
			return "Failed";
		}

		$this->stopOutputBuffering();

		$terminal_msg = "Finished retrieving data file, saveToDb flag is set to: $this->saveListingToDb";
		
		$this->beginOutputBuffering();

		$this->record( $terminal_msg."\n", 5);
		$this->listingProcessed = 0;

		if (!file_exists($tempFile)) {
			$this->record( "The feed file:$tempFile, does not exist!");
			$this->updateProgress(strval($this->listingProcessed), self::RESULT);
			return false;
		}

		// process it now...
		if ($this->decompressIt)
			$xmlfile = $this->uncompress($tempFile);
		else
			$xmlfile = 'compress.zlib://'.$tempFile;
		$files = array();
		$files[] = $xmlfile;

		//$this->record( "Before getting memory stats\n", 3);
		//$memUsedWhole = memory_get_usage(true);
		//$memUsedeMalloc = memory_get_usage(false);
		//$this->record("memory usage before parseXmlFiles: SystemUsed: $memUsedWhole, eMallocUsed: $memUsedeMalloc\n", 2);

		if (!$this->saveListingToDb) // if not saving to db, then brute force.  If xml is big, may take many hours, even days.
		{
			$this->createDebugFile(1); // create just listhubParallel.txt
			$output = !$this->parseXmlFiles($files);
		}
		else
			$output = $this->saveToDb($files, $onlyParseFeed);

		if ($this->decompressIt &&
			!$this->saveDecompressedFile)
			unlink ($xmlfile);

		unlink($tempFile);
		if ($this->urlFile)
			fclose($this->urlFile);

		$this->stopOutputBuffering();

		if (!empty($lastListHubDataFeedRead)) 
			$this->getClass('Options')->set([(object)['where'=>['opt'=>'LastListHubDataFeedRead'],
													  'fields'=>['value'=>date("Y-m-d H:i:s", time())]]]);
		else
			$this->getClass('Options')->add((object)['opt'=>'LastListHubDataFeedRead',
													 'value'=>date("Y-m-d H:i:s", time())]);
/*
		if ($this->xhr)
		{
			if ($this->saveListingToDb)
				echo "TOTAL $this->listingProcessed\n";	
			echo "DONE";
		}
*/
		if ($output)
			return $output;
		else
		{
			$this->saveLogToDb = true;
			$this->updateProgress("Failed to process listhub data", self::FATAL);
			return false;
		}
	}

	protected function recordForListhub()
	{
		//ListingKey,Status,URL,Message,Timestamp
		if ($this->listHubLog &&
			count($this->listHubRecord))
		{
			flock($this->listHubLog, LOCK_EX);
			foreach($this->listHubRecord as $value)
			{
				$why = trim($value['reason']);
				$buf = $value['listhub_key'].",".$value['status'].",".($value['id'] != 0 ? "http://lifestyledlistings.com/listing/".$value['id'] : " ").",".$why.",".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600))."\n";
				fwrite($this->listHubLog, $buf);
			}
			fflush($this->listHubLog);
			flock($this->listHubLog, LOCK_UN);
		}
	}

	public function updateToDb($msg, $reason = self::UPDATE, $level = 3, $force = 0, $extra = 0) {
		if ($level < $this->debugLevel)
			return;
		if (!$force && 
			($this->specialOpType == IMAGE_BORDER_PARALLEL_BATCH ||
			 $this->specialOpType == IMAGE_BORDER_PARALLEL_CUSTOM_TASK ||
			 $this->specialOpType == SPECIAL_PARSE_BATCH) ) {
			$this->updateToDbList[] = (object)['data'=>$msg,
											   'type'=>$reason,
											   'intData'=>$extra];
			return;
		}
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;
		$this->updateProgress($msg, $reason, $force, $extra);
		$this->saveLogToDb = $oldFlag;
	}

	public function recordParseResult($msg) {
		$this->updateToDb($msg, self::LISTHUB_PARSE_RESULT);
	}

	public function processBlocks($list, $cpuLimit, $id, $saveRejected, $minTags, $minPrice, $ppRunId, $specialOperation = SPECIAL_OP_NONE, $locksql = 0)
	{
		//$this->updateToDb("Entered processBlocks for host:".$_SERVER['HTTP_HOST'], self::UPDATE);
		$load = sys_getloadavg();
		$exitNow = false;

		//$this->updateToDb("Load: $load for host:".$_SERVER['HTTP_HOST'], self::UPDATE);

		$this->saveRejected = $saveRejected == 'true';
		$this->minimumTagCount = intval($minTags);
		$this->minimumPrice = intval($minPrice);
		$this->uselocksql = $locksql;

		$doingImagePP = !($specialOperation >= IMAGE_BORDER_PARALLEL && $specialOperation <= IMAGE_BORDER_PARALLEL_BATCH) ? 0 :
						 ($specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 1 : 2);
		$chunkTable = !$doingImagePP ? 'ListhubChunkStatus' : ($doingImagePP == 1 ? 'ImageChunkStatus' : 'CustomImageChunkStatus');

		$y = $this->getLastStartEntry($doingImagePP);
		$startUpData = json_decode($y->data);

		$lcs = $this->getClass($chunkTable);
		$q = new \stdClass();
		$q->where = array(//'subdomain'=>$id,
							'startid'=>$list[0]);
		$this->lock();
		$x = $lcs->get($q);
		if (empty($x) &&
			$load[0] <= floatval($cpuLimit) &&
			$ppRunId == $startUpData->ppRunId) {
			$chunkStatus = array('subdomain'=>$id,
								 'startid'=>$list[0],
								 'status'=>CHUNK_START,
								 'pid'=>getmypid());
			try {
				$z = $lcs->add((object)$chunkStatus); // add this subdomain/startid to db before another request comes in.
			}
			catch(\Exception $e) {
				$this->record("SubDomain: ".$id." startId:".$list[0]." - caught exception adding CHUNK_START to $chunkTable",3, self::ERROR, true);
			}
			//$this->record("ProcessBlocks added to $chunkTable at row ".$z.", for SubDomain:".$id." with startId:".$list[0].", specialOperation:$specialOperation, time:".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)), 3);
		}
		if (!empty($x) &&
			$x[0]->subdomain != $id) {
			$this->record("Incoming SubDomain: ".$id." startId:".$list[0].", but from DB got SubDomain:{$x[0]->subdomain}, resetting subdomain to incoming value.", 3, self::INCONSISTENT_SUBDOMAIN_ID, true);
			$q->fields = ['subdomain'=>$id];
			$lcs->set([$q]);
		}
		$this->unlock();
		unset($q);
		
		if ($ppRunId != $startUpData->ppRunId) {
			$terminal_msg = "SubDomain: ".$id." startId:".$list[0]." - denied, the ppRunId:$ppRunId does not match current session:".$startUpData->ppRunId;
			$reason = self::ALIEN_RUNID;
			$exitNow = true;
		}
		elseif (!empty($x)) {// this chunk as already started, most like recent due to network error
			$terminal_msg = "SubDomain: ".$id." startId:".$list[0]." -  already started at ".$x[0]->added.", current status:".$x[0]->status.", load:".$load[0].", ppRunId:$ppRunId, chunkTable:$chunkTable";
			$reason = self::REPEATING_PARALLEL;
			$exitNow = true;
		}
		elseif ($load[0] > floatval($cpuLimit)) {
			$terminal_msg = "SubDomain: ".$id." startId:".$list[0]." - CPU load:".$load[0].", request denied, limit is ".$cpuLimit.", ppRunId:$ppRunId";
			$reason = self::DENY_PARALLEL;
			$exitNow = true;
		}
		else {
			$terminal_msg = "SubDomain: ".$id." startId:".$list[0]." - Processing now, saveRejected:".$saveRejected.", minTags:".$this->minimumTagCount.", minPrice: ".$this->minimumPrice.", cpu limit is ".$cpuLimit.", load:".$load[0].", specialOperation:$specialOperation, doingImagePP:$doingImagePP, locksql:".($locksql ? 'yes' : 'no').", pid: ".getmypid().", ppRunId:$ppRunId".", time:".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
			$reason = self::BEGIN_PARALLEL;
		}

		//$this->updateToDb("Before header exit for host:".$_SERVER['HTTP_HOST'], self::UPDATE);
		$this->exitNow($terminal_msg);

		$this->doingParallel = true; // so it appends, not overwrite
		$this->openDebugFile(!$doingImagePP ? 1 : ($doingImage == 1 ? 4 : 5));
		//$this->updateToDb("After openDebugFile(1) for host:".$_SERVER['HTTP_HOST'], self::UPDATE);

		// so now, all output to file, with result going to progress db
		$this->beginOutputBuffering();

		$this->specialOpType = $specialOperation;
		$this->record($terminal_msg, 3, self::UPDATE, true);

		//$this->updateToDb("After header exit for host:".$_SERVER['HTTP_HOST'], self::UPDATE);
		// $oldFlag = $this->saveLogToDb;
		// $this->saveLogToDb = true;
		if ($reason == self::ALIEN_RUNID) 
			$result = array('id'=>$id,
							'startid'=>$list[0],
							'load'=>$load[0],
							'ppRunId'=>$ppRunId,
							'host'=>$_SERVER['HTTP_HOST'],
							'ppRunId'=>$ppRunId,
							'specialOperation'=>$specialOperation);
		elseif ($reason != self::REPEATING_PARALLEL)
			$result = array('id'=>$id,
							'startid'=>$list[0],
							'load'=>$load[0],
							'host'=>$_SERVER['HTTP_HOST'],
							'ppRunId'=>$ppRunId,
							'specialOperation'=>$specialOperation);
		else
			$result = array('id'=>$id,
							'startid'=>$list[0],
							'status'=> !empty($x) ? $x[0]->status : 0,
							'load'=>$load[0],
							'host'=>$_SERVER['HTTP_HOST'],
							'ppRunId'=>$ppRunId,
							'doingImagePP'=>$doingImagePP,
							'specialOperation'=>$specialOperation);
		$this->updateToDb(json_encode($result), $reason, 3, true, !empty($x) ? $x[0]->status : 0);
		// $this->saveLogToDb = $oldFlag;

		

		//$this->updateToDb("Before exit:$exitNow for host:".$_SERVER['HTTP_HOST'], self::UPDATE);
		if ($exitNow) {
			$this->stopOutputBuffering();
			return false;
		}
		else
			$this->record("processBlocks recorded start to DB for SubDomain: ".$id." started, beginning at block:".$list[0], 3, self::UPDATE, true);

		// $chunkStatus = array('subdomain'=>$id,
		// 					 'startid'=>$list[0],
		// 					 'status'=>CHUNK_START);

		$this->setPricingOptions();
		// $this->setMiscOptions();

		// $this->lock();
		// $x = $lcs->add((object)$chunkStatus); // add this subdomain/startid to db before another request comes in.
		// $this->unlock();
		// $this->record("ProcessBlocks added to $chunkTable at row ".$x.", for SubDomain:".$id." with startId:".$list[0].", specialOperation:$specialOperation, time:".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)), 3);

		$this->saveListingToDb = false; // just to be sure
		if ($specialOperation == SPECIAL_OP_NONE ||
			$specialOperation == SPECIAL_PARSE_BATCH)
			$this->openDebugFile(2);
		//$this->updateToDb("After openDebugFile(2) for host:".$_SERVER['HTTP_HOST'], self::UPDATE);
		
		$startTime = microtime();

		// gather city tags
		if ($specialOperation == SPECIAL_OP_NONE ||
			$specialOperation == SPECIAL_PARSE_BATCH) 
			$this->getCitiesTags();
		// do stuff
		$good = array();
		$bad = array();
		$forked = 0;
		$status = 0;
		$haveFork = true;
		if (! function_exists('pcntl_fork')) {
			$haveFork = false;
			$this->record("Fork is not available!\n", 3);
		}

		$listingId = 0; $newLastId = 0;
		$origList = array_merge($list);
		$blockTable = !$doingImagePP ? 'ListhubBlocks' : ($doingImagePP == 1 ? 'ImageBlocks' : 'CustomImageBlocks');
		if ($specialOperation == IMAGE_BORDER_PARALLEL_BATCH ||
			$specialOperation == IMAGE_BORDER_PARALLEL_CUSTOM_TASK) {
			$startTimeGrab = microtime();
			$blocks = $this->getClass($blockTable)->get((object)['where'=>['id'=>$list]]);
			$process = [];
			$flagged = [];
			$ids = [];
			foreach($blocks as $block)
				if ( empty($block->flag) )
					$process[] = $block;
				else
					$flagged[] = $block;

				
			if (!empty($process)) {
				$ids = array_map( function($ele){ return $ele->data; }, $process );
				$this->lock();
				$list = $this->getClass('Listings')->get((object)['where'=>['id'=>$ids],
																  'what'=>['id','images','first_image','list_image','listhub_key']]);
				$this->unlock();
				unset($ids);
			}

			$this->record("Image blocks - total:".count($origList).", process:".count($process).", flagged:".count($flagged), 3);
			if (!empty($flagged))
				$good = array_map( function($ele){ return $ele->id; }, $flagged );

			if (empty($process)) {
				$list = []; // empty;
			}

			$endTime = microtime();
			$this->record("Time to grab all image data for ".count($list)." listings is ".$this->diffTime($startTimeGrab, $endTime)." secs", 3);
			$index = 0;
			if (!empty($list)) foreach($list as &$l)
				$l->blockId = $origList[$index++];
			$this->record("Assigned blockId's to all listings, starting with ".$origList[0], 3);
		}
		elseif ($specialOperation == SPECIAL_PARSE_BATCH) {
			$startTimeGrab = microtime();
			$this->lock();
			$list = $this->getClass($blockTable)->get((object)['where'=>['id'=>$list]]);	
			$this->unlock();	

			$process = [];
			$flagged = [];
			$ids = [];
			foreach($list as $block)
				if ( empty($block->flag) )
					$process[] = $block;
				else
					$flagged[] = $block;

			$this->record("ListHub blocks - total:".count($origList).", process:".count($process).", flagged:".count($flagged), 3);
			if (!empty($flagged))
				$good = array_map( function($ele){ return $ele->id; }, $flagged );

			$list = $process; // whatever we have that's not flagged

			$endTime = microtime();
			$this->record("Time to grab all xml block data for ".count($origList)." listings is ".$this->diffTime($startTimeGrab, $endTime)." secs", 3);
			$index = 0;
			if (!empty($list)) foreach($list as $l)
				$l->blockId = $origList[$index++];
			$this->record("Assigned blockId's to all xml blocks, starting with ".$origList[0], 3);
		}

		$startTimeListProcessing = microtime();
		$elements = []; // for batch purpose
		if (!empty($list)) foreach($list as &$l)
		{
			if ($haveFork) {
				$load = sys_getloadavg();
				if ($load[0] < floatval($cpuLimit)) {
					$pid = pcntl_fork();
					switch($pid) {
						case 0:
							//$this->updateToDb("Before processBlock for $l", self::UPDATE);
							$this->record("Child for listing chunk: $l\n", 3);
							$retval = true;				
							switch ($specialOperation) {
								case IMAGE_BORDER_PARALLEL:
								case IMAGE_BORDER_PARALLEL_BATCH:
								case IMAGE_BORDER_PARALLEL_AFTER_PARSE: 
								case IMAGE_BORDER_PARALLEL_REMOTELY:
								case IMAGE_BORDER_PARALLEL_CRON_ALL:
								case IMAGE_BORDER_PARALLEL_CRON_FROM_LAST:
								case IMAGE_BORDER_PARALLEL_CUSTOM_TASK:
									$retval = $this->processOneImageTask($l, $listingId); 
									$newLastId = $listingId > $newLastId ? $listingId : $newLastId;
									break;

								case CREATE_IMAGES_FROM_IMPORTED: $retval = $this->processGenerateImageBlock($l); break;
								case IMPORT_ACTIVE_LISTINGS_IMAGE: $retval = $this->processImageImportBlock($l); break;
								case SPECIAL_OP_NONE: 
								case SPECIAL_PARSE_BATCH:
									try {
										$listing = [];
										if ( ($retval = $this->processBlock($l, $listing)) )
											$elements[] = $listing;
										elseif (is_object($listing))
											unset($listing);
									}
									catch( \Exception $e) {
										$this->record("Caught exception from processBlock(): ".$e->getMessage()."\n");
			        					$this->parseException($e);
			        					$this->updateToDb("Exception caught: subdomain:$id, startId:".$origList[0].", id:".($this->specialOpType == SPECIAL_OP_NONE ? $l : $l->id), self::FATAL);
									}
									break;
							}
							if ($retval)
								$good[] = $specialOperation != IMAGE_BORDER_PARALLEL_BATCH &&
										  $specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK &&
							  			  $specialOperation != SPECIAL_PARSE_BATCH ? $l : $l->blockId;
							else
								$bad[] = $specialOperation != IMAGE_BORDER_PARALLEL_BATCH &&
										 $specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK &&
							  			 $specialOperation != SPECIAL_PARSE_BATCH ? $l : $l->blockId;
							exit($specialOperation == IMAGE_BORDER_PARALLEL_BATCH || $specialOperation == IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? $l : $l->blockId);
						case -1:
							$this->record("Failed to fork for listing chunk: $l\n", 3);
							break;
						default:
							$this->record("Forked for listing chunk: $l with pid: $pid\n", 3);
							$forked++;
							break;
					}
				}

				if ($forked &&
					($pid = pcntl_waitpid(0, $status, WNOHANG)) != 0) {
					$forked--;
					$this->record("Pid: $pid retuned with status: $status\n", 3);
				}
			}
			else {
				$retval = true;
				switch ($specialOperation) {
					case IMAGE_BORDER_PARALLEL:
					case IMAGE_BORDER_PARALLEL_BATCH:
					case IMAGE_BORDER_PARALLEL_AFTER_PARSE: 
					case IMAGE_BORDER_PARALLEL_REMOTELY:
					case IMAGE_BORDER_PARALLEL_CRON_ALL:
					case IMAGE_BORDER_PARALLEL_CRON_FROM_LAST:
					case IMAGE_BORDER_PARALLEL_CUSTOM_TASK:
						$retval = $this->processOneImageTask($l, $listingId); 
						$newLastId = $listingId > $newLastId ? $listingId : $newLastId;
						break;
					case CREATE_IMAGES_FROM_IMPORTED: $retval = $this->processGenerateImageBlock($l); break;
					case IMPORT_ACTIVE_LISTINGS_IMAGE: $retval = $this->processImageImportBlock($l); break;
					case SPECIAL_OP_NONE: 
					case SPECIAL_PARSE_BATCH:
						try {
							$listing = [];
							if ( ($retval = $this->processBlock($l, $listing)) )
								$elements[] = $listing;
							elseif (is_object($listing)) // could be false
								unset($listing);
						}
						catch( \Exception $e) {
							$this->record("Caught exception from processBlock(): ".$e->getMessage()."\n");
        					$this->parseException($e); //subdomain'=>$id,
        					$this->updateToDb("Exception caught: subdomain:$id, startId:".$origList[0].", id:".($this->specialOpType == SPECIAL_OP_NONE ? $l : $l->id), self::FATAL);
						}
						break;
				}
				if ($retval)
					$good[] = $specialOperation != IMAGE_BORDER_PARALLEL_BATCH &&
							  $specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK &&
							  $specialOperation != SPECIAL_PARSE_BATCH ? $l : $l->blockId;
				else
					$bad[] = $specialOperation != IMAGE_BORDER_PARALLEL_BATCH &&
							 $specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK &&
							 $specialOperation != SPECIAL_PARSE_BATCH ? $l : $l->blockId;
			}
		}
		$endTime = microtime();
		$this->record("Time to process list for ".count($list)." listings is ".$this->diffTime($startTimeListProcessing, $endTime)." secs", 3);

		while($haveFork && $forked) {
			$this->record("inside while waiting for forks");
			if ( ($pid = pcntl_waitpid(0, $status, WNOHANG)) != 0 ) {
				$forked--;
				$this->record("Pid: $pid retuned with status: $status\n", 3);
			}
			sleep(1);
		}

		if ($specialOperation == IMAGE_BORDER_PARALLEL_BATCH ||
			$specialOperation == IMAGE_BORDER_PARALLEL_CUSTOM_TASK ||
			$specialOperation == IMAGE_BORDER_PARALLEL_CUSTOM_TASK) {
			if (!empty($list)) {
				// $this->record("subDomain:$id, startId:{$origList[0]} - about to create ids from list", 3);
				$ids = array_map( function($ele){ return $ele->blockId; }, $list );
				// $this->record("subDomain:$id, startId:{$origList[0]} - about to call set for list for ids:".print_r($ids, true), 3);
				$this->lock();
				$this->getClass($blockTable)->set([(object)['where'=>['id'=>$ids],
															'not'=>['flag' => 1],
															'fields'=>['flag'=>1]]]);
				$this->unlock();
				// $this->record("subDomain:$id, startId:{$origList[0]} - after to call set for list", 3);
				$fieldTypes = ['images','first_image','list_image']; // can be an array of non-homegenous updates, some row can have one or more fields
									  // in this example, just one field
				$keyType = 'id'; // what column to trigger on
				$startTimeSaveBatch = microtime();
				foreach($list as &$l) {
					if ( isset($l->needUpdate) &&
						 $l->needUpdate == true) {
						$elements[] = (object)['key'=>$l->id,
										   	  'fields'=>$l->fields];
					}
				}
				if (count($elements)) {
					$this->record("About to setMultiple for $keyType for ".count($elements)." elements", 3);
					// $this->getClass('Listings')->forceEnableLog();
					$this->lock();
					$x = $this->getClass('Listings')->setMultiple($keyType, $fieldTypes, $elements);
					$this->unlock();
					// $this->getClass('Listings')->forceDisableLog();
				}
				unset($elements, $fieldTypes);
			}

			if (count($this->updateToDbList)) {
				$this->record("About to addMultiple for ".count($this->updateToDbList)." records", 3);
				// $this->getClass("ListhubProgress")->forceEnableLog();
				$this->lock();
				$this->getClass("ListhubProgress")->addMultiple( $this->updateToDbList );
				$this->unlock();
				// $this->getClass("ListhubProgress")->forceDisableLog();
			}
			$endTime = microtime();
			$this->record("Time to save batch image data for ".count($list)." listings is ".$this->diffTime($startTimeSaveBatch, $endTime)." secs", 3);
			$this->specialOpType = SPECIAL_OP_NONE; //do this so updateProgress won't stick into the updateToDbList array.
		}
		elseif ($specialOperation == SPECIAL_PARSE_BATCH ) {
			$startTimeSaveBatch = microtime();
			if (!empty($list)) {
				// $this->record("subDomain:$id, startId:{$origList[0]} - about to create ids from list", 3);
				$ids = array_map( function($ele){ return $ele->blockId; }, $list );
				// $this->record("subDomain:$id, startId:{$origList[0]} - about to call set for list for ids:".print_r($ids, true), 3);
				$this->lock();
				$this->getClass($blockTable)->set([(object)['where'=>['id'=>$ids],
															'not'=>['flag' => 1],
															'fields'=>['flag'=>1]]]);
				$this->unlock();
			}
			if (count($this->updateToDbList)) {
				$this->record("About to addMultiple for ".count($this->updateToDbList)." records", 3);
				// $this->getClass("ListhubProgress")->forceEnableLog();
				$this->getClass("ListhubProgress")->addMultiple( $this->updateToDbList );
				// $this->getClass("ListhubProgress")->forceDisableLog();
			}
			$endTime = microtime();
			$this->record("Time to save batch image data for ".count($list)." listings is ".$this->diffTime($startTimeSaveBatch, $endTime)." secs", 3);
			$this->specialOpType = SPECIAL_OP_NONE; //do this so updateProgress won't stick into the updateToDbList array.
		}

		// update db status to 2
		try {
			$q = new \stdClass();
			$q->fields = array('status'=>CHUNK_PROCESSED);
			$q->where = array('subdomain'=>$id,
						 	  'startid'=>$origList[0]);
			$a = array();
			$a[] = $q;
			$this->lock();
			try {
				$lcs->set($a);
			}
			catch(\Exception $e) {
				parseException($e);
			}
			$this->unlock();
			unset($q, $a);

			$endTime = microtime();
			$numerator = floatval($this->diffTime($startTime, $endTime));
			$denominator = floatval(count($origList));
			$timeEach = number_format( $numerator/$denominator, 4 );
			$msg = "Time to process ".count($list)." listing for subDomain:$id, starting with ".$origList[0]." was ".$this->diffTime($startTime, $endTime)." secs, for ".count($origList)." listings, or ".$timeEach." per listing - timestamp:".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
			$this->record($msg."\n", 3);
		}
		catch(\Exception $e) {
        	$this->record("Failed to set chunk status, exception: ".$e->getMessage()."\n");
        	$this->parseException($e);
        }
		/* too much data...
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;
		$this->updateProgress($msg, self::UPDATE);
		$this->saveLogToDb = $oldFlag;
		*/

		if ($specialOperation == SPECIAL_OP_NONE ||
			$specialOperation == SPECIAL_PARSE_BATCH) {
			$this->recordForListhub();
			$this->processTriggers();
		}
		else {
			// ignore IMAGE_BORDER_PARALLEL_CUSTOM_TASK
			switch($specialOperation) {
				case IMAGE_BORDER_PARALLEL:
				case IMAGE_BORDER_PARALLEL_BATCH:
				case IMAGE_BORDER_PARALLEL_AFTER_PARSE: 
				case IMAGE_BORDER_PARALLEL_REMOTELY:
				case IMAGE_BORDER_PARALLEL_CRON_ALL:
				case IMAGE_BORDER_PARALLEL_CRON_FROM_LAST:
					$lastId = 0; 
					$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'LastImageBorderId']]);
					if (!empty($opt)) {
						$lastId = intval($opt[0]->value);

						if ($newLastId > $lastId) {
							$this->lock();
							$x = $this->getClass('Options')->set([(object)[//'where'=>['opt'=>'LastImageBorderId'],
																		  'where'=>['id'=>$opt[0]->id],
																		  'fields'=>['value'=>strval($newLastId)]]]);
							$this->unlock();
							$msg = (empty($x) ? "Failed to update" : "Updated")." index:{$opt[0]->id}, lastId from $lastId to $newLastId for SubDomain: $id, beginning at block:".$origList[0];
							$this->updateToDb($msg, self::IMAGE_BORDER_UPDATE_LAST_ID);
						}
						else
							$this->record("processBlocks - updated new last id: $newLastId", 3);
					}
					break;
			}
		}

  		$load = sys_getloadavg();
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;
		$result = array('good'=>$good,
						'bad'=>$bad,
						'startid'=>$origList[0],
						'id'=>$id,
						'load'=>$load[0],
						'added'=>$this->addedToDb,
						'duplicate'=>$this->duplicates,
						'rejected'=>$this->rejected,
						'belowMinimumAcceptedPrice'=>$this->belowMinimumAcceptedPrice,
						'host'=>$_SERVER['HTTP_HOST'],
						'ppRunId'=>$ppRunId,
						'endtime'=>microtime() );
		$this->updateProgress(json_encode($result), self::RESULT_PARALLEL, $id);
		$this->saveLogToDb = $oldFlag;
		$this->record("SubDomain: ".$id." startId:".$origList[0]." - exiting", 3);

		$this->stopOutputBuffering();


/*
		$result = array('good'=>$good,
						'bad'=>$bad,
						'startId'=>$list[0]);

		return $result;
*/
		unset($origList, $list, $elements);
		return true;
	}

	protected function processTriggers() {
		if (empty($this->triggerCities))
			return;

		$cityIds = array_keys($this->triggerCities);
		$Cities = $this->getClass('Cities');
		$sql = "SELECT a.id, a.city, a.state, GROUP_CONCAT( CONCAT("."'".'{"tag_id":'."'".',b.tag_id,'."'".',"score":'."'".',b.score,'."',";
		$sql .= '"tag":'.'"'."',c.tag,'".'",';
		$sql .= '"type"'.":',c.type,".'"}"'.") SEPARATOR ',') as tags FROM ".$Cities->getTableName()." AS a ";
		$sql .= " INNER JOIN ".$Cities->getTableName('cities-tags')." AS b ON a.id = b.city_id";
		$sql .= " INNER JOIN ".$Cities->getTableName('tags')." AS c on c.id = b.tag_id";
		$sql .= " WHERE a.id IN (".implode(",",$cityIds).")";
		$sql .= " GROUP BY a.id";

		$this->record("processTriggers - sql:$sql", 3);
		$cityTags = $Cities->rawQuery($sql);

		$addList = [];
		if (empty($cityTags)) {
			$this->record("Failed to find any city tags using $sql", 3);
			// then we should add all trigger tags into each of these cities
		}
		
		if (!empty($cityTags)) {
			foreach($cityTags as $city) {
				$cityTagList = [];
				if (!empty($city->tags)) {
					$city->tags = '['.$city->tags.']';
					$city->tags = json_decode($city->tags);
					foreach($city->tags as $tag) {
						$cityTagList[strtolower($tag->tag)] = intval($tag->tag_id);
						unset($tag);
					}
				}
				$missingTags = [];
				$checkTags = $this->triggerCities[$city->id];
				if (count($checkTags) > count($cityTagList))
					$missingTags = array_diff($checkTags, $cityTagList);

				if (!empty($missingTags)) {
					$this->record("processTriggers - appending tags to city:$city->id:".print_r($missingTags, true), 3);
					foreach($missingTags as $tag) {
						$addList[] = (object)['tag_id'=>$tag,
											  'city_id'=>$city->id,
											  'score'=>1,
											  'clicks'=>0,
											  'flags'=>CITY_TAG_NEED_REVIEW | CITY_TAG_TRIGGERED];
					}
				}
				unset($this->triggerCities[$city->id]); // get rid of this one
				unset($city, $cityTagList, $missingTags);
			}
			unset($cityTags);
		}

		// check if we had cities without any tags..
		if (!empty($this->triggerCities)) {
			foreach($this->triggerCities as $city_id=>$tagList) {
				$this->record("processTriggers - adding new tags to city:$city->id:".print_r($tagList, true), 3);
				foreach($tagList as $tag) {
					$addList[] = (object)['tag_id'=>$tag,
											  'city_id'=>$city_id,
											  'score'=>1,
											  'clicks'=>0,
											  'flags'=>CITY_TAG_NEED_REVIEW | CITY_TAG_TRIGGERED];
					unset($tag);
				}
				unset($tagList);
			}
		}

		if (!empty($addList)) {
			$this->record("processTriggers - adding ".count($addList)." to CitiesTags table:".print_r($addList, true), 3);
			$this->getClass('CitiesTags')->addMultiple($addList);
		}
		unset($addList);
	}

	protected function stripslashes(&$x, $xmlOpenTag, $xmlCloseTag) {
		$startPos = strpos($x->data, $xmlOpenTag);
		$endPos = strpos($x->data, $xmlCloseTag, $startPos);
		$partOne = substr($x->data, 0, $startPos);
		$partTwo = removeslashes(substr($x->data, $startPos, ($endPos-$startPos)));
		$partThree = substr($x->data, $endPos);
		$x->data = $partOne.$partTwo.$partThree;
	}

	protected function processOneImageTask(&$id, &$lastId) {
		$this->record("processOneImageTask - entered for listing chunk:".($this->specialOpType != IMAGE_BORDER_PARALLEL_BATCH && $this->specialOpType != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? $id : $id->id), 3);
		// error_reporting(E_ERROR | E_WARNING | E_PARSE);
		$retval = true;
		$imageClass = $this->getClass("Image");
		$startTime = microtime();
		$path = realpath(__DIR__."/../_img/_listings/uploaded");
		$listing = null;
		$unsetListing = false;

		if ($this->specialOpType != IMAGE_BORDER_PARALLEL_BATCH && 
			$this->specialOpType != IMAGE_BORDER_PARALLEL_CUSTOM_TASK) {
			$this->lock();
			$x = $this->getClass("ImageBlocks")->get((object)['where'=>['id'=>$id]]);
			$this->unlock();
			if(!empty($x))
			{
				$x = array_pop($x);
				if (!empty($x->data))
				{
					if ( intval($x->flag) == 1) {
						$this->record("processOneImageTask - image block, id:$x->id for $x->data is already flagged", 3);
						return true;
					}
					$this->lock();
					$listing = $this->getClass('Listings')->get((object)['where'=>['id'=>$x->data]]);
					$this->unlock();
					// $this->record("processOneImageTask - begin for listing chunk: $id, listingId:".(empty($x) ? "N/A" : $x[0]->data)."\n", 3);
					if (!empty($listing)) {
						$this->record("processOneImageTask - begin for listing chunk: $id, listingId:$x->data, image list with ".count($listing[0]->images)." images\n", 3);
						$listing = array_pop($listing);
						$unsetListing = true;
					}
					else {
						$this->record("Failed to retrieve listing - id:$x->data\n", 3);
						$this->rejected++;
						$retval = false;
					}
				}
				else {
					$this->record("Retrieved data block for $id is empty!\n", 3);
					$this->rejected++;
					$retval = false;
				}
			}
			else
			{
				$this->record("Failed to retrieve Image Block - id:$id\n", 3);
				$this->rejected++;
				$retval = false;
			}
		}
		else 
			$listing = &$id;
						
		if ($retval &&
			!empty($listing->listhub_key)) {
			$this->record("processOneImageTask - begin for listingId:$listing->id, image list with ".count($listing->images)." images\n", 3);
			try {
				if ($this->specialOpType == IMAGE_BORDER_PARALLEL_CUSTOM_TASK)
					$this->doOneCustomImageTask($listing, $this->imageBorderOp, $path, $this->processOnlyNewImages, $lastId);	
				else
					$this->fixOneImageBorder($listing, $this->imageBorderOp, $path, $this->processOnlyNewImages, $lastId);	
			}
			catch( \Exception $e) {
				parseException($e);
				$this->record("processOneImageTask exception for  listingId:$listing->id: ".$e->getMessage(), 3);
				return false;
			}	
			if ($this->specialOpType != IMAGE_BORDER_PARALLEL_BATCH &&
				$this->specialOpType != IMAGE_BORDER_PARALLEL_CUSTOM_TASK )
				$this->getClass("ImageBlocks")->set([(object)['where'=>['id'=>$x->id],
														  	  'fields'=>['flag'=>1]]]);
		}

		$endTime = microtime();
		$this->record("Time to process listingId:$listing->id, was ".$this->diffTime($startTime, $endTime)." secs\n", 3);

		if ($unsetListing)
			unset($listing);

		return $retval;
	}

	public function processOneImage($id) {
		$this->saveLogToDb = true;
		$this->haveOutputFile = false;

		$this->record("processOneImage - entered for $id", 3);
		// error_reporting(E_ERROR | E_WARNING | E_PARSE);
		$retval = true;
		$imageClass = $this->getClass("Image");
		$startTime = microtime();
		$path = realpath(__DIR__."/../_img/_listings/uploaded");
		$listing = null;
		$unsetListing = false;
		
		$this->lock();
		$listing = $this->getClass('Listings')->get((object)['where'=>['id'=>$id]]);
		$this->unlock();
		// $this->record("processOneImageTask - begin for listing chunk: $id, listingId:".(empty($x) ? "N/A" : $x[0]->data)."\n", 3);
		if (!empty($listing)) {
			$this->record("processOneImage - begin for listing chunk: $id, listingId:$id, image list with ".count($listing[0]->images)." images\n", 3);
			$listing = array_pop($listing);
			$unsetListing = true;
		}
		else {
			$this->record("Failed to retrieve listing - id:$id\n", 3);
			$this->rejected++;
			$retval = false;
		}
					
		$lastId = 0;		
		if ($retval &&
			!empty($listing->listhub_key)) {
			$this->record("processOneImage - begin for listingId:$listing->id, image list with ".count($listing->images)." images\n", 3);
			try {
				$this->fixOneImageBorder($listing, TRIM_IMAGE, $path, true, $lastId);	
			}
			catch( \Exception $e) {
				parseException($e);
				$this->record("processOneImage exception for  listingId:$listing->id: ".$e->getMessage(), 3);
				return false;
			}	
			if ($this->getClass("ImageBlocks")->exists(['data'=>$id]))
				$this->getClass("ImageBlocks")->set([(object)['where'=>['data'=>$id],
															  'fields'=>['flag'=>1]]]);
		}

		$endTime = microtime();
		$this->record("Time to process listingId:$listing->id, was ".$this->diffTime($startTime, $endTime)." secs\n", 3);

		if ($unsetListing)
			unset($listing);

		return $retval;
	}

	protected function processGenerateImageBlock($id) {
		$this->record("processGenerateImageBlock - entered for listing chunk: $id\n", 3);
		// error_reporting(E_ERROR | E_WARNING | E_PARSE);
		$retval = true;
		$q = new \stdClass();
		$q->where = array('id' => $id);
		$startTime = microtime();
		$this->lock();
		$x = $this->getClass("ImageBlocks")->get($q);
		$this->unlock();
		$imageClass = $this->getClass("Image");
		$this->record("processGenerateImageBlock - begin for listing chunk: $id, listingId:".(empty($x) ? "N/A" : $x[0]->data)."\n", 3);
		if(!empty($x))
		{
			$x = array_pop($x);
			if (!empty($x->data))
			{
				$this->lock();
				$listing = $this->getClass('Listings')->get((object)['where'=>['id'=>$x->data]]);
				$this->unlock();
				if (!empty($listing)) {
					$this->record("processGenerateImageBlock - begin image list with ".count($listing[0]->images)." images\n", 3);
					$needUpdate = false;
					$imgCount = 0;
					$imported = 0;
					$generatedImage = 0;
					$startTime = microtime();
					$listing = array_pop($listing);
					if (!empty($listing->images)) foreach($listing->images as &$image) {
						$imgCount++;
						$img = null;
						$name = '';
						$width = 0; $height = 0;
						$haveAlready = false;
						$alreadyDone = false;
						$nonExistantFile = false;

						if (!isset($image->file) &&
							isset($image->url)) { // uh oh...
							$image->file = removeslashes($image->url);
							$needUpdate = true;
						}

						if (isset($image->file) ) {
							if (substr($image->file, 0, 4 ) != 'http') {  // then a static image exists for this 
								$path = realpath(__DIR__."/../_img/_listings/uploaded");
								$name = $path.'/'.removeslashes($image->file);
								// if (file_exists(($name)))
								if (file_exists($name))
									$haveAlready = true;
								else {
									$msg = "Failed to locate file: $name, url: $image->url";
									$this->updateToDb($msg, self::IMAGE_GEN_IMAGE_ERROR);
									unset($image->file);
									$nonExistantFile = true;
								}
							}
							else {
								$name = removeslashes($image->file);
								$file_info = pathinfo($name);
								// $base = $file_info['filename'];
								// $base = preg_replace('/[^a-zA-Z0-9]/', '_', $base).'.'.$file_info['extension'];
								// $name = __DIR__."/../_img/_listings/uploaded/".$base;
								$name = __DIR__."/../_img/_listings/uploaded/".$this->getBaseImageName($name).'.'.$file_info['extension'];
								if (file_exists($name)) {
									// $alreadyDone = true;
									$haveAlready = true;
									$image->url = removeslashes($image->file);
									$image->file = $base;
									$needUpdate = true;
									$msg = "listing:$listing->id has local, but file is url:$base";
									$this->updateToDb($msg, self::IMAGE_GEN_FILE_URL_BUT_HAVE_LOCAL);
									// $this->duplicates++;
								}
								else if (empty($file_info['extension']) &&
										 file_exists($name.".jpeg")) {
									$haveAlready = true;
									$image->url = removeslashes($image->file);
									$image->file = $base.".jpeg";
									$needUpdate = true;
									$msg = "listing:$listing->id has local, but file is url:$base.jpeg";
									$this->updateToDb($msg, self::IMAGE_GEN_FILE_URL_BUT_HAVE_LOCAL);
								}
							}
						}

						
						$bypass = false;
						$created = false;
						if (!$haveAlready) {
							if ( ((isset($image->file) &&
								   substr($image->file, 0, 4 ) == 'http') ||
								   isset($image->url)) ) { // only one of them can can be 'http' address
								$name = (isset($image->file) && substr($image->file, 0, 4 ) == 'http') ? removeslashes($image->file) : removeslashes($image->url);
								$startImgGet = microtime();
								list($width, $height, $type, $attr) = getimagesize($name);
								// $msg = "getimagesize - $type, $width, $height";
								// $this->updateToDb($msg, self::UPDATE);

								
							  	switch($type){
							  		case IMG_JPEG:
									case IMG_JPG: $img = imagecreatefromjpeg($name); break;
									case IMG_GIF: $img = imagecreatefromgif($name); break;
									case IMG_PNG: $img = imagecreatefrompng($name); break;
									case IMG_WBMP: $img = imagecreatefromwbmp($name); break;
									case IMG_XPM: $img = imagecreatefromxpm($name); break;
									default:
										$msg = "Failed to getimagesize($name), bypassing.";
										$this->updateToDb($msg, self::IMAGE_GEN_IMAGE_ERROR);
										$bypass = true;
								}

								if ($bypass)
									continue;

								// $file_info = pathinfo($name);
								// $base = $file_info['filename'];
								// $base = preg_replace('/[^a-zA-Z0-9]/', '_', $base);
								// $path = realpath(__DIR__."/../_img/_listings/uploaded");
								// $dest = "$path/$base.jpeg";
								$dest = $path."/".$this->getBaseImageName($name).".jpeg";
								$created = imagejpeg($img, $dest, 95);
								imagedestroy($img);
								$endImgGet = microtime();
								if (!$created) {
									$msg = "Failed to create jpeg from buffer to $dest";
									$this->updateToDb($msg, self::IMAGE_GEN_IMAGE_ERROR);
									continue;
								}
								$needUpdate = true;
								$msg = "listing:$listing->id fetched image, status:".($created ? "success" : "failed")." for $dest";
								$this->updateToDb($msg, self::IMAGE_GEN_IMAGE_FETCHED_URL);
							}
							else {
								$msg = "Don't already have and don't have http url either for listingId:$listing->id, bypassing.";
								$this->updateToDb($msg, self::IMAGE_GEN_IMAGE_ERROR);
								continue;
							}
						}
						else {
							$name = removeslashes($image->file);
							$file_info = pathinfo($name);
							// $base = $file_info['filename'];
							// $base = preg_replace('/[^a-zA-Z0-9]/', '_', $base);
							// $name = $base.'.'.$file_info['extension'];
							$name = $this->getBaseImageName($name).'.'.$file_info['extension'];
							$path = realpath(__DIR__."/../_img/_listings/uploaded");
							$dest = "$path/$name";
							$msg = "listing:$listing->id has local $name";
							$this->updateToDb($msg, self::IMAGE_GEN_IMAGE_LOCAL_EXISTS);
						}


						$startImgGen = microtime();
						$out = $imageClass->generateSizes('listings', $dest, $created);							
						$endImgGen = microtime();
					
						if ($out->status != 'OK') {
							$generated = 0;
							foreach($out->data as $stats)
								if ($stats->status == 'OK')
									$generated++;
								// else {
								// 	$msg = "Failed listingId:$listing->id to generate image: $stats->data";
								// 	$this->updateToDb($msg, self::IMAGE_GEN_IMAGE_ERROR);
								// }

							$generatedImage = $generated ? 1 : 0;
							$msg = "Got back ".count($out->data)." results, ".($generated ? "Generated $generated images" : "All present, none generated")." for listing:$listing->id, took:".$this->diffTime($startImgGen, $endImgGen)." secs , source: $name";
							$this->updateToDb($msg, self::UPDATE);
						}
						else {
							$msg = "generateSizes all images for listing: $listing->id, took:".$this->diffTime($startImgGet, $endImgGet)." secs, generate:".$this->diffTime($startImgGen, $endImgGen)." secs, $dest";
							$this->updateToDb($msg, self::UPDATE);
							$generatedImage++;
						}
					
						if ($created) {
							if (!isset($image->url))
								$image->url = addslashes($image->file);
							unset($image->file);
							$image->file = addslashes(basename($dest));
							$image->width = $width;
							$image->height = $height;
							$needUpdate = true;
							
							$imported++;
						}
						
						if ( (empty($image->width) ||
							   empty($image->height)) &&
							   $width && $height) {
							$image->width = $width;
							$image->height = $height;
							$needUpdate = true;
						}
					} // end foreach($listing->images as $image) 

					$endTime = microtime();
					$reason = self::IMAGE_GEN_IMAGE_LISTING_DONE;
					if ($needUpdate){
						$this->lock();
						try {
							$x = $this->getClass('Listings')->set([(object)['where'=>['id'=>$listing->id],
																			'fields'=>['images'=>$listing->images]]]);
						}
						catch(\Exception $e) {
							parseException($e);
						}
						$this->unlock();
						if ($x == false) {
							$msg = "Imported $imported images, generated:$generatedImage, total:$imgCount but could not update listing:$listing->id images data, time: ".$this->diffTime($startTime, $endTime)." secs";
							$reason = self::IMAGE_GEN_IMAGE_ERROR;
						}
						else {
							$msg = "Imported $imported images, generated:$generatedImage, total:$imgCount and updated listing:$listing->id images data, time: ".$this->diffTime($startTime, $endTime)." secs";
							$this->addedToDb++;
						}
					}
					else if (!$generatedImage) {
						$msg = "No images generated, had $imgCount images for listing:$listing->id time: ".$this->diffTime($startTime, $endTime)." secs";
					}
					else {
						$msg = "Generated $$generatedImage images, total:$imgCount but not updating listing:$listing->id images data, time: ".$this->diffTime($startTime, $endTime)." secs";
						$this->duplicates++;
					}
			
					$result = array('id'=>$listing->id,
									'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)),
									'msg'=>$msg);
					$this->updateToDb(json_encode($result), $reason);
					$this->record($msg."\n", 3);
				}
				else {
					$this->record("Failed to retrieve listing - id:$x->data\n", 3);
					$this->rejected++;
					$retval = false;
				}
			}
		}
		else
		{
			$this->record("Failed to retrieve data Block - id:$id\n", 3);
			$this->rejected++;
			$retval = false;
		}

		$endTime = microtime();
		$this->record("Time to process block: $id, was ".$this->diffTime($startTime, $endTime)." secs\n", 3);

		return $retval;
	}

	protected function processImageImportBlock($id) {
		$this->record("processImageImportBlock - begin for listing chunk: $id\n", 3);

		$retval = true;
		$q = new \stdClass();
		$q->where = array('id' => $id);
		$startTime = microtime();
		$this->lock();
		$x = $this->getClass("ImageBlocks")->get($q);
		$this->unlock();
		$imageClass = $this->getClass("Image");
		if(!empty($x))
		{
			$x = array_pop($x);
			if (!empty($x->data))
			{
				$this->lock();
				$listing = $this->getClass('Listings')->get((object)['where'=>['id'=>$x->data]]);
				$this->unlock();
				if (!empty($listing)) {
					$needUpdate = false;
					$imgCount = 0;
					$imported = 0;
					$startTime = microtime();
					$listing = array_pop($listing);
					if (!empty($listing->images)) foreach($listing->images as &$image) {
						$imgCount++;
						$img = null;
						$name = '';
						$width = 0; $height = 0;
						$alreadyDone = false;
						$nonExistantFile = false;

						if (!isset($image->file) &&
							isset($image->url)) { // uh oh...
							$image->file = removeslashes($image->url);
							$needUpdate = true;
						}

						if (isset($image->file) ) {
							if (substr($image->file, 0, 4 ) != 'http') {  // then a static image exists for this 
								$name = __DIR__."/../_img/_listings/uploaded/".removeslashes($image->file);
								if (file_exists(($name)))
									$alreadyDone = true;
								else {
									$msg = "Failed to locate file: $name, url: $image->url";
									$this->updateToDb($msg, self::IMAGE_IMPORT_ERROR);
									unset($image->file);
									$nonExistantFile = true;
								}
							}
							else {
								$name = removeslashes($image->file);
								$file_info = pathinfo($name);
								// $base = $file_info['filename'];
								// $base = preg_replace('/[^a-zA-Z0-9]/', '_', $base).'.'.$file_info['extension'];
								// $name = __DIR__."/../_img/_listings/uploaded/".$base;
								$name = __DIR__."/../_img/_listings/uploaded/".$this->getBaseImageName($name).'.'.$file_info['extension'];
								if (file_exists($name)) {
									$alreadyDone = true;
									$image->url = removeslashes($image->file);
									$image->file = $base;
									$needUpdate = true;
									$this->duplicates++;
								}
								else if (empty($file_info['extension']) &&
										 file_exists($name.".jpeg")) {
									$alreadyDone = true;
									$image->url = removeslashes($image->file);
									$image->file = $base.".jpeg";
									$needUpdate = true;
									$this->duplicates++;
								}
							}
						}

						if ( !$alreadyDone &&
							 ((isset($image->file) &&
							   substr($image->file, 0, 4 ) == 'http') ||
							   isset($image->url)) ) { // only one of them can can be 'http' address
							$name = (isset($image->file) && substr($image->file, 0, 4 ) == 'http') ? removeslashes($image->file) : removeslashes($image->url);

							$startImgGet = microtime();
							list($width, $height, $type, $attr) = getimagesize($name);
							// $msg = "getimagesize - $type, $width, $height";
							// $this->updateToDb($msg, self::UPDATE);

							$bypass = false;
						  	switch($type){
						  		case IMG_JPEG:
								case IMG_JPG: $img = imagecreatefromjpeg($name); break;
								case IMG_GIF: $img = imagecreatefromgif($name); break;
								case IMG_PNG: $img = imagecreatefrompng($name); break;
								case IMG_WBMP: $img = imagecreatefromwbmp($name); break;
								case IMG_XPM: $img = imagecreatefromxpm($name); break;
								default:
									$msg = "Failed to getimagesize($name), bypassing.";
									$this->updateToDb($msg, self::IMAGE_IMPORT_ERROR);
									$bypass = true;
							}

							if ($bypass)
								contnue;

							// $file_info = pathinfo($name);
							// $base = preg_replace('/[^a-zA-Z0-9]/', '_', $file_info['filename']);
							// $dest = __DIR__."/../_img/_listings/uploaded/$base.jpeg";
							$dest = __DIR__."/../_img/_listings/uploaded/".$this->getBaseImageName($name).".jpeg";
							$created = imagejpeg($img, $dest, 95);
							imagedestroy($img);
							$endImgGet = microtime();

							$startImgGen = microtime();
							if ($created)
								$out = $imageClass->generateSizes('listings', $dest, true);
							else {
								$msg = "Failed to imagejpeg to $dest";
								$this->updateToDb($msg, self::IMAGE_IMPORT_ERROR);
								$bypass = true;
							}
							$endImgGen = microtime();

							if ($bypass)
								contnue;

							if ($out->status != 'OK') {
									$msg = "Error generateSizes(1): ".(gettype($out->data) == 'string' ? $out->data : print_r($out->data, true)).", source: $name";
								$this->updateToDb($msg, self::IMAGE_IMPORT_ERROR);
								$bypass = true;
							}
							else {
								$msg = "generateSizes for nonExistantFile:".($nonExistantFile ? 'true' : 'false').", listing: $listing->id, get:".$this->diffTime($startImgGet, $endImgGet)." secs, generate:".$this->diffTime($startImgGen, $endImgGen)." secs, $dest";
								$this->updateToDb($msg, self::UPDATE);
							}

							if ($bypass)
								contnue;

							$madeImages = true;
							if (!isset($image->url)) 
								$image->url = addslashes($image->file);
							$image->file = addslashes(basename($dest));
							$image->width = $width;
							$image->height = $height;
							$needUpdate = true;
							
							$imported++;
							//$image->processed = 1;
						}
						if ( (empty($image->width) ||
							   empty($image->height)) &&
							   $width && $height) {
							$image->width = $width;
							$image->height = $height;
							$needUpdate = true;
						}
					} // end foreach($listing->images as $image) 

					$endTime = microtime();
					$reason = self::IMAGE_IMPORT_LISTING_DONE;
					if ($needUpdate){
						$this->lock();
						try {
							$x = $this->getClass('Listings')->set([(object)['where'=>['id'=>$listing->id],
																			'fields'=>['images'=>$listing->images]]]);
						}
						catch(\Exception $e) {
							parseException($e);
						}
						$this->unlock();
						if ($x == false) {
							$msg = "Imported $imported images, total:$imgCount but could not update listing:$listing->id images data, time: ".$this->diffTime($startTime, $endTime)." secs";
							$reason = self::IMAGE_IMPORT_ERROR;
						}
						else {
							$msg = "Imported $imported images, total:$imgCount and updated listing:$listing->id images data, time: ".$this->diffTime($startTime, $endTime)." secs";
							$this->addedToDb++;
						}
					}
					else if (!$imported) {
						$msg = "No images to import, had $imgCount images for listing:$listing->id time: ".$this->diffTime($startTime, $endTime)." secs";
						$this->duplicates++;
					}
					else {
						$msg = "Imported $imported images, total:$imgCount but not updating listing:$listing->id images data, time: ".$this->diffTime($startTime, $endTime)." secs";
						$this->addedToDb++;
					}

					$oldFlag = $this->saveLogToDb;
					$this->saveLogToDb = true;
					$result = array('id'=>$listing->id,
									'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)),
									'msg'=>$msg);
					$this->updateProgress(json_encode($result), $reason);
					$this->saveLogToDb = $oldFlag;
					$this->record($msg."\n", 3);
				}
				else {
					$this->record("Failed to retrieve listing - id:$x->data\n", 3);
					$this->rejected++;
					$retval = false;
				}
			}
		}
		else
		{
			$this->record("Failed to retrieve data Block - id:$id\n", 3);
			$this->rejected++;
			$retval = false;
		}

		$endTime = microtime();
		$this->record("Time to process block: $id, was ".$this->diffTime($startTime, $endTime)." secs\n", 3);

		return $retval;
	}

	protected function processBlock($id, &$listing)
	{
		$this->record("processBlock - begin for listing chunk: ".($this->specialOpType == SPECIAL_OP_NONE ? $id : $id->id)."\n", 3);

		$retval = true;
		$unsetData = false;
		$startTime = microtime();
		if ($this->specialOpType == SPECIAL_OP_NONE) {
			$x = $this->getClass("ListhubBlocks")->get((object)['where'=>['id'=>$id]]);
			if(!empty($x))
			{
				$x = array_pop($x);
				if (!empty($x->data))
					$unsetData = true;
				else {
					$this->record("Retrieved empty ListHub Block - id:$id\n", 3);
					$retval = false;
				}
			}
			else
			{
				$this->record("Failed to retrieve ListHub Block - id:$id\n", 3);
				$retval = false;
			}
		}
		else
			$x = $id;

		try {
			if ($retval &&
				empty($x->flag)) { // still good :)
				$doc = new \DOMDocument("1.0", "UTF-8");
				// $listing = array();
				$seller  = array();
				$geoInfo = array();

				// default values so they won't be NULL
				$listing['lotsize'] = 0;
				$listing['lotsize_std'] = 'ft';
				$listing['interior'] = 0;
				$listing['interior_std'] = 'ft';

				try {
					// $this->stripslashes($x, "<LastName>", "</LastName>");
					// $this->stripslashes($x, "<ProviderName>", "</ProviderName>");
					// $this->stripslashes($x, "<ListingDescription>", "</ListingDescription>");
					// $this->stripslashes($x, "<Offices>", "</Offices>");
					// $this->stripslashes($x, "<Brokerage>", "</Brokerage>");
					// $this->stripslashes($x, "<Directions>", "</Directions>");
					// $this->stripslashes($x, "<LegalDescription>", "</LegalDescription>");
					global $xmlTagsToClean;
					$alreadyDone = [];
					foreach($xmlTagsToClean as $cleaner) {
						if (in_array($cleaner['tag'], $alreadyDone))
							continue;
						$this->stripslashes($x, '<'.$cleaner['tag'].'>', '</'.$cleaner['tag'].'>');
						$alreadyDone[] = $cleaner['tag'];
					}
					unset($alreadyDone);
				}
				catch( \Exception $e) {
					$this->updateToDb("processBlock stripslashes() - listing chunk: ".($this->specialOpType == SPECIAL_OP_NONE ? $id : $id->id).", Exception caught: ".$e->getMessage().", data:".print_r($x, true), self::FATAL);
					$retval = false;
					if ($unsetData)
						unset($x);

					return $retval;
				}
				// $this->record("incoming: ".$x->data."\n", 3);

				try {
					$doc->loadXML($x->data);
				}
				catch( \Exception $e) {
					$this->updateToDb("processBlock loadXML() - listing chunk: ".($this->specialOpType == SPECIAL_OP_NONE ? $id : $id->id).", Exception caught: ".$e->getMessage().", data:".print_r($x, true), self::FATAL);
					$retval = false;
					if ($unsetData)
						unset($x);

					return $retval;
				}

				$startTimeOneListing = microtime();
				try {
	            	$newListing = simplexml_import_dom($doc);
	            }
				catch( \Exception $e) {
					$this->updateToDb("processBlock simplexml_import_dom() - listing chunk: ".($this->specialOpType == SPECIAL_OP_NONE ? $id : $id->id).", Exception caught: ".$e->getMessage().", data:".print_r($x, true), self::FATAL);
					$retval = false;
					if ($unsetData)
						unset($x);

					return $retval;
				}

	            try {
					if (!$this->parseOneListing($newListing, $listing, $seller, $geoInfo, ($this->specialOpType == SPECIAL_OP_NONE ? $id : $id->id))) {
						$this->record("Failed to parse ListHub Block - id:".($this->specialOpType == SPECIAL_OP_NONE ? $id : $id->id)."\n", 3);
						$retval = false;
					}
					else {
						$endTimeOneListing = microtime();
						$this->record("Time to process one listing: ".($this->specialOpType == SPECIAL_OP_NONE ? $id : $id->id).", MLS: ".$listing->listhub_key.", was ".$this->diffTime($startTimeOneListing, $endTimeOneListing)." secs\n", 3);
					}
				}
				catch( \Exception $e) {
					$this->updateToDb("processBlock parseOneListing() - listing chunk: ".($this->specialOpType == SPECIAL_OP_NONE ? $id : $id->id).", Exception caught: ".$e->getMessage().", data:".print_r($newListing, true), self::FATAL);
					$retval = false;
					if ($unsetData)
						unset($x);

					return $retval;
				}
				unset($dom);
				unset($newListing);
				if ($this->specialOpType == SPECIAL_OP_NONE)
					ob_flush();

				// unset($listing);
				unset($seller);
				unset($geoInfo);
			}
		}
		catch(\Exception $e) {
			$this->updateToDb("processBlock - Exception caught: ".$e->getMessage(), self::FATAL);
			$retval = false;
		}
		
		$endTime = microtime();
		$this->record("Time to process block: ".($this->specialOpType == SPECIAL_OP_NONE ? $id : $id->id).", was ".$this->diffTime($startTime, $endTime)." secs\n", 3);

		if ($unsetData)
			unset($x);

		return $retval;
	}

	protected function getCitiesTags() {
		return;

		$c = $this->getClass('Cities');
		$c = getTableName($c->table);
		$ct = $this->getClass('CitiesTags');
		$ct = getTableName($ct->table);
		$t = $this->getClass('Tags');
		$t = getTableName($t->table);
		$sql = 'SELECT a.city, a.state, b.tag_id, c.tag, b.score FROM `'.$c.'` AS a ';
		$sql .= 'INNER JOIN '.$ct.' AS b ON a.id = b.city_id AND (b.score >= 7 OR b.score = -1)';
		$sql .= 'INNER JOIN '.$t.' AS c ON c.id = b.tag_id ';
		$sql .= 'ORDER BY a.city ASC';
		$this->updateToDb("getCitiesTags - sql: $sql", 3);

		$cityTags = $this->getClass('Cities')->rawQuery($sql);

		//$this->updateToDb("getCitiesTags after rawQuery - sql: $sql", self::UPDATE);
		$this->cityTags = array();
		foreach($cityTags as $tag) {
			$key = ucwords(strtoupper($tag->city)).'-'.strtoupper($tag->state);
			if (!array_key_exists($key, $this->cityTags))
				$this->cityTags[$key] = array();
			if ($tag->score == -1)
				$tag->score = 10;
			$this->cityTags[$key][$tag->tag] = (object)['id'=>$tag->tag_id,
														'score'=>$tag->score];
		}
		//$this->updateToDb("getCitiesTags exit - sql: $sql", self::UPDATE);

		//$this->getAllCityTags();
	}

	protected function getAllCityTags() {
		$c = $this->getClass('Cities');
		$cities = getTableName($c->table);
		$ct = $this->getClass('CitiesTags');
		$citiesTags = getTableName($ct->table);
		$t = $this->getClass('Tags');
		$tags = getTableName($t->table);
		$sql = "SELECT a.city, a.state, GROUP_CONCAT( CONCAT("."'".'{"tag_id":'."'".',b.tag_id,'."'".',"score":'."'".',c.score,'."',";
		$sql .= '"icon":'.'"'."',c.icon,'".'",';
		$sql .= '"tag":'.'"'."',c.tag,'".'",';
		$sql .= '"desc":'.'"'."',c.description,'".'",';
		$sql .= '"type"'.":',c.type,".'"}"'.") SEPARATOR ',') as tags FROM ".$cities." AS a ";
		$sql .= " INNER JOIN $citiesTags AS b ON a.id = b.city_id";
		$sql .= " INNER JOIN $tags AS c on c.id = b.tag_id";
		$sql .= " GROUP BY a.id ASC";

		$this->updateToDb("getAllCityTags - sql: $sql", 3);
	}

	public function processChunkRecover($chunkSize, $specialOperation = SPECIAL_OP_NONE) {
		$this->doingParallel = true;
		$doingImagePP = !($specialOperation >= IMAGE_BORDER_PARALLEL && $specialOperation <= IMAGE_BORDER_PARALLEL_BATCH) ? 0 :
						 ($specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 1 : 2);
		$this->openDebugFile(!$doingImagePP ? 1 : ($doingImage == 1 ? 4 : 5));
		$this->beginOutputBuffering();

		$exitMsg = '';
		$this->record("processChunkRecover entered with chunkSize:$chunkSize, specialOperation:$specialOperation", 3);
		$doingImagePP = !($specialOperation >= IMAGE_BORDER_PARALLEL && $specialOperation <= IMAGE_BORDER_PARALLEL_BATCH) ? 0 :
						 ($specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 1 : 2);
		if (!$doingImagePP) {
			try {
				$this->getCitiesTags();
				$this->record("processChunkRecover - got city tags", 3);
			}
			catch(\Exception $e) {
				$this->record("processChunkRecover - ".$e->getMessage(), 3);
				$exitMsg = $e->getMessage()."  ";
			}
		}

		$chunkTable = !$doingImagePP ? 'ListhubChunkStatus' : ($doingImagePP == 1 ? 'ImageChunkStatus' : 'CustomImageChunkStatus');

		$chunk = $this->getClass($chunkTable)->getFirst('id',['where'=>['status'=>CHUNK_ERROR]]);
		$this->record("processChunkRecover - got for ERROR:".print_r($chunk, true), 3);
		if (empty($chunk) ||
		    (gettype($chunk) == 'array' && count($chunk) == 0)) { // CHUNK_START
			$this->record("processChunkRecover - try START:", 3);
			$chunk = $this->getClass($chunkTable)->getFirst('id',['where'=>['status'=>CHUNK_START]]);
			$this->record("processChunkRecover - got for START:".print_r($chunk, true), 3);
			if (empty($chunk)  ||
                    	   (gettype($chunk) == 'array' && count($chunk) == 0)) {
				$this->record("processChunkRecover looks all done", 3);
				$exitMsg .= 'Looks all done';
			}
			else
				$exitMsg .= 'No bad chunks found';
		}

		if (!empty($exitMsg)) {
			$this->stopOutputBuffering();
			return new Out('fail', $exitMsg);
		}

		$chunk = array_pop($chunk);
		$startId = $chunk->startid;

		if (!$doingImagePP)
			$this->openDebugFile(3);

		// $this->setPricingOptions();
		// $this->setMiscOptions();
		// $specialOperation = SPECIAL_OP_NONE;

		$list = [];
		for($i = 0; $i < $chunkSize; $i++)
			$list[] = $startId+$i;

		$this->record("processChunkRecover - start for subDomain:$chunk->subdomain, startId:$chunk->startid, size:$chunkSize\n", 3);

		$status = CHUNK_PROCESSED;
		try {
			$out = $this->doOneChunk($chunk->subdomain, $list);
			$this->record("processChunkRecover - completed for subDomain:$chunk->subdomain, startId:$chunk->startid, size:$chunkSize\n", 3);
		}
		catch(\Exception $e) {
			$this->record("processChunkRecover - exception for subDomain:$chunk->subdomain, startId:$chunk->startid: ".$e->getMessage()."\n", 3);
			$status = CHUNK_FATAL;
		}
		
		$this->getClass($chunkTable)->set([(object)['where'=>['id'=>$chunk->id],
															 'fields'=>['status'=>$status]]]);
		unset($list);

		$this->stopOutputBuffering();

		return $out;
	}

	public function performChunk($list) {
		$this->doingParallel = false;
		// gather city tags
		$this->getCitiesTags();


		$this->beginOutputBuffering();

		$this->cleanUpLogFiles();
		$this->openDebugFile();
		$this->logHeader();

		$this->record("performChunk - startid:".$list[0].", size:".count($list)."\n", 3);

		$out = $this->doOneChunk(0, $list);

		$this->stopOutputBuffering();

		return $out;
	}

	// prep was done...
	protected function doOneChunk($id, $list) {
		$this->setPricingOptions();
		// $this->setMiscOptions();
		$specialOperation = SPECIAL_OP_NONE;

		// do stuff
		$good = array();
		$bad = array();
		$retval = true;
		$startTime = microtime();
		$this->log("doOneChunk starting for ".count($list)." blocks\n", 3);
		foreach($list as &$l)
		{
			//$this->log("doOneChunk - id:$l\n", 3);
			try {
				$listing = [];
				if ( ($retval = $this->processBlock($l, $listing)) )
					$elements[] = $listing;
				elseif (is_object($listing)) // could be false
					unset($listing);
			}
			catch( \Exception $e) {
				$this->record("Caught exception from processBlock(): ".$e->getMessage()."\n");
				$this->parseException($e); //subdomain'=>$id,
				$this->updateToDb("Exception caught: subdomain:$id, startId:".$list[0].", id:".($this->specialOpType == SPECIAL_OP_NONE ? $l : $l->id), self::FATAL);
			}

			if ($retval)
				$good[] = $specialOperation != IMAGE_BORDER_PARALLEL_BATCH &&
						  $specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK &&
						  $specialOperation != SPECIAL_PARSE_BATCH ? $l : $l->blockId;
			else
				$bad[] = $specialOperation != IMAGE_BORDER_PARALLEL_BATCH &&
			  			 $specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK &&
						 $specialOperation != SPECIAL_PARSE_BATCH ? $l : $l->blockId;
		}

		$endTime = microtime();
		$numerator = floatval($this->diffTime($startTime, $endTime));
		$denominator = floatval(count($origList));
		$timeEach = number_format( $numerator/$denominator, 4 );
		$msg = "Time to process ".count($list)." listings starting with ".$list[0]." was ".$this->diffTime($startTime, $endTime)." secs, or ".$timeEach." per listing - timestamp:".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$this->record($msg."\n", 3);
		$out = new Out('OK', ['good'=>$good,
							  'bad'=>$bad,
							  'startId'=>$list[0],
							  'subDomain'=>$id]);

		$this->lock();
		$this->getClass('ListhubBlocks')->set([(object)['where'=>['id'=>$list],
														'not'=>['flag' => 1],
														'fields'=>['flag'=>1]]]);
		$this->unlock();

		return $out;
	}
	
	public function perform($in) {
		if (empty($in))
			return array('success' => 'Form was submitted', 'formData' => $_POST);

		$this->doingParallel = false;
		// gather city tags
		$this->getCitiesTags();


		$this->beginOutputBuffering();

		$this->cleanUpLogFiles();
		$this->openDebugFile();
		$this->logHeader();

		$error = false;
		$files = array();
		//$iniValue = ini_get("allow_url_fopen");
		//ini_set("allow_url_fopen", true);

		$this->setPricingOptions();
		// $this->setMiscOptions();
 
		$uploaddir = __DIR__.'/../uploads/';
		foreach($_FILES as $file)
		{
			if ($this->saveXmlFiles)
			{
				move_uploaded_file($file['tmp_name'], $uploaddir.'listhub.xml');
				$files[] = $uploaddir.'listhub.xml';
			}
			else
			 	$files[] = $file['tmp_name'];
		}

		if (!$error) {
			$this->record("perform - begin call to parseXmlFiles", 3);
			$result = !$this->parseXmlFiles($files);
		}

		$this->record("perform - after call to parseXmlFiles", 3);
		if (!$this->saveXmlFiles)
			foreach($files as $file)
				unlink($file);
		else 
			foreach($_FILES as $file) // get rid of original tmp files
				unlink($file);

		$this->processTriggers();

		$this->stopOutputBuffering();
		$this->exitNow("DONE");
		
		return null;

		//ini_set("allow_url_fopen", (boolean)$iniValue);
		// return new Out('OK', 'Done processing file');
	}

	protected function saveToDb($files, $onlyParseFeed = false)
	{
		$retval = true;
		$startTime = microtime();
		$this->record("entered saveToDb\n", 3);
		$this->record("about to create ListhubBlocks in saveToDb\n", 3);
		try {
			$lhb = $this->getClass('ListhubBlocks');
		}
		catch( \Exception $e)
		{
			$this->record("Failed to create ListhubBlocks\n", 3);
			return false;
		}
		try {
			$this->record("About to empty out listhub block data\n", 3);
			$lhb->emptyTable(true);
		}
		catch( \Exception $e)
		{
			$this->record("Failed to truncate ListhubBlocks\n", 3);
			return false;
		}
		$endTime = microtime();
		$this->record("All data blocks removed from table:listhub_blocks, took ".$this->diffTime($startTime, $endTime)." secs\n", 3);
		$this->listingIdRange = array();

		$startTime = microtime();
		foreach($files as $file)
		{
			$xml = new \XMLReader();
			if (($result = $xml->open($file, null, LIBXML_PARSEHUGE)) == false)
			{
				$this->record("Failed to open xml file: $file, retrying\n", 6);
				sleep(1);
				if (($result = $xml->open($file, null, LIBXML_PARSEHUGE)) == false) {
					$this->record("Failed to open xml file: $file, second time, giving up\n", 6);
					continue;
				}
			}

			//$xml = simplexml_load_string($buffer);
			$this->parseXml($xml, $onlyParseFeed);
			$xml->close();
		}
		$endTime = microtime();
		$time = $this->diffTime($startTime, $endTime);
		$secs = is_object($time) ? $time->__toString() : $time;
		$timeEach = number_format($this->listingProcessed / floatval($secs), 5);
		$this->record("Time to process ".$this->listingProcessed." listings to db was ".$time." secs, or $timeEach per listing.\n", 3);

		$count = count($this->listingIdRange);
		$firstRowId = 1; //$this->listingIdRange[0];
		$lastRowId = $this->listingIdRange[$count-1];
		$this->record("Saved to db - total:$count, firstRow:$firstRowId, lastRow:$lastRowId\n", 3);
		$retval = array('first'=>$firstRowId,
						'last'=>$lastRowId);
		

		$this->updateProgress(strval($this->listingProcessed), self::RESULT);

		return $retval;

	}

	protected function parseXmlFiles($files)
	{
		$retval = true;
		foreach($files as $file)
		{
			$xml = new \XMLReader();
			if (($result = $xml->open($file)) == false)
			{
				$this->record("Failed to open xml file: $file\n", 6);
				continue;
			}

			//$xml = simplexml_load_string($buffer);
			$retval = $this->parseXml($xml);
			$xml->close();
		}
		return $retval;
	}

	protected function addslashes(&$buffer, $xmlOpenTag, $xmlCloseTag, $find = null, $replace = null) {
		$startPos = strpos($buffer, $xmlOpenTag);
		$endPos = strpos($buffer, $xmlCloseTag, $startPos);
		$partOne = substr($buffer, 0, $startPos);
		$partTwo = addslashes(substr($buffer, $startPos, ($endPos-$startPos)));
		if ($find && $replace)
			$partTwo = str_replace($find, $replace, $partTwo);
		$partThree = substr($buffer, $endPos);
		$buffer = $partOne.$partTwo.$partThree;
	}

	protected function parseXml(&$xml, $onlyParseFeed = false)
	{
		//if ($xml->getName() != 'Listings')
		if ($xml->read() && 
			$xml->name != 'Listings' &&
			$xml->name != 'Listing')
		{
			$name = $xml->name;
			$this->record( "The first element in this XML is not Listings, it is $name\n", 5 );
			return false;
		}
		$index = (int)0;
		$header = '<?xml version="1.0" encoding="UTF-8"?>';
		$batch = array();

		$this->record("parseXml - entered, onlyParseFeed:".($onlyParseFeed ? 'yes' : 'no'), 3);

		while( $xml->name == 'Listing' ? true : $xml->read() )
		{
			if ( ($index % 2000) == 0 )
				$this->setDebugLevel();

			$xmlName = $xml->name;
			if ($xml->nodeType == \XMLReader::ELEMENT && $xml->name == "Listing") {
				if (!$this->saveListingToDb)
					$this->record( "****** Begin parse listing #$index\n", 4);
				elseif ( ($index % 5000) == 0)
					$this->record( "Processed $index listings to the database\n", 3);
					
				$startTime = microtime();
				
                $doc = new \DOMDocument("1.0", "UTF-8");
                $newListing = simplexml_import_dom($doc->importNode($xml->expand(),true));

                $listing = array();
				$seller  = array();
				$geoInfo = array();

				// default values so they won't be NULL
				$listing['lotsize'] = 0;
				$listing['lotsize_std'] = 'ft';
				$listing['interior'] = 0;
				$listing['interior_std'] = 'ft';
				$listing['error'] = 0;
				$listing['flags'] = 0;

				//$listing['author_has_account'] = 0;

				$endTime = microtime();
				$this->record("Prep time for $index, was ".$this->diffTime($startTime, $endTime)." secs\n", 2);

				$startTimeOneListing = microtime();
				if ($this->saveListingToDb)
				{
					$startTime = microtime();
					//$info = array();
					$buffer = $newListing->asXml();
					$endTime = microtime();
					$this->record("Time to get data as xml: ".$this->diffTime($startTime, $endTime)." secs\n", 2);
					$startTime = microtime();
					if ($buffer != false)
					{
						// $this->addslashes($buffer, "<LastName>", "</LastName>");
						// $this->addslashes($buffer, "<ProviderName>", "</ProviderName>");
						// $this->addslashes($buffer, "<ListingDescription>", "</ListingDescription>", "andamp;", "and");
						// $this->addslashes($buffer, "<ListingDescription>", "</ListingDescription>", "&amp;", "and");
						// $this->addslashes($buffer, "<ListingDescription>", "</ListingDescription>", "&", "and");
						// $this->addslashes($buffer, "<Offices>", "</Offices>");
						// $this->addslashes($buffer, "<Brokerage>", "</Brokerage>");
						// $this->addslashes($buffer, "<Directions>", "</Directions>");
						// $this->addslashes($buffer, "<LegalDescription>", "</LegalDescription>");
						global $xmlTagsToClean;
						foreach($xmlTagsToClean as $cleaner)
							$this->addslashes($buffer, '<'.$cleaner['tag'].'>', '</'.$cleaner['tag'].'>', $cleaner['find'], $cleaner['replace']);

						if ($onlyParseFeed &&
							$this->recordListHubKey) {
							$pos = strpos($buffer, '<ListingKey>');
							$posEnd = strpos($buffer, '</ListingKey>');
							$listingKey = substr($buffer, $pos, $posEnd-$pos+12);
							$this->record($index.":$listingKey", 3, self::RECORD_LISTHUB_KEY);
						}

						// $this->record("outgoing - startPos: $startPos, endPos:$endPos\n", 3);
						// $this->record("outgoing - partOne:$partOne\n", 3);
						// $this->record("outgoing - partTwo:$partTwo\n", 3);
						// $this->record("outgoing - partThree:$partThree\n", 3);
						// $this->record("outgoing - $buffer"."\n", 3);

						if ($this->batchSizeXML == 0) {
							$info['data'] = $header.$buffer;
							$rowId = $this->getClass("ListhubBlocks")->add((object)$info);
							$this->listingIdRange[] = $rowId;
							$this->record( "Parsed and stored listing index: $index to row: $rowId\n", 2 );
							$endTime = microtime();
							$this->record("Time to save one listing to db: $index, was ".$this->diffTime($startTime, $endTime)." secs\n", 2);
						}
						else {
							$batch[] = $header.$buffer;
							if (count($batch) == $this->batchSizeXML) {
								$lhb = $this->getClass("ListhubBlocks");
								$sql = "INSERT INTO ".$lhb->getTableName($lhb->table)." (`data`) VALUES ";
								$i = 0;
								foreach($batch as $data) {
									if ($i)
										$sql .= ",";
									$out = $lhb->wpdb->prepare("%s", $data);
									$sql .= "(".$out.")";
									$i++;
								}
								$this->listingIdRange[] = $rowId = $lhb->wpdb->query($sql);
								$endTime = microtime();
								$this->record("Time to save batch of ".count($batch)." listings to db: $index, was ".$this->diffTime($startTime, $endTime)." secs\n", 2);

								unset($batch);
								$batch = array();
							}
						}
						unset($buffer, $partOne, $partTwo, $partThree);
					}
					else
						$this->record( "Failed to parse and store listing index: $index\n", 3 );
					unset($info);
				}
				else {
					if (!$this->parseOneListing($newListing, $listing, $seller, $geoInfo, $index))
						$this->record( "Failed to parse Listing for index: $index, continuing to next if it exists\n", 3 );
					else {
						$endTimeOneListing = microtime();
						$this->record("Time to process one listing: $index, MLS: ".$listing->listhub_key.", was ".$this->diffTime($startTimeOneListing, $endTimeOneListing)." secs\n", 3);
					}
				}

				$startTime = microtime();
				$xml->next();
				$this->record("After next it is now: ".$xml->name."\n");
				unset($dom);
				unset($newListing);
				ob_flush();

				unset($listing);
				unset($seller);
				unset($geoInfo);

				$endTime = microtime();
				$this->record("Clean up time for $index, was ".$this->diffTime($startTime, $endTime)." secs\n", 2);

				$index++;
				if ($this->listingLimit != '*' &&
					$index >= $this->listingLimit)
					break;
			}
			else
			{
				//$name = $newListing->getName();
				$name = $xml->name;
				$this->record( "Ignoring major element: $name\n" );
			}
		}

		if ($this->getClass('Options')->exists(['opt'=>'ListHubFeedSize']))
			$this->getClass('Options')->set([(object)['where'=>['opt'=>'ListHubFeedSize'],
													  'fields'=>['value'=>$index]]]);
		else
			$this->getClass('Options')->add(['opt'=>'ListHubFeedSize',
											 'value'=>$index]);

		if (count($batch) ) {
			$startTime = microtime();
			$lhb = $this->getClass("ListhubBlocks");
			$sql = "INSERT INTO ".$lhb->getTableName($lhb->table)." (`data`) VALUES ";
			$i = 0;
			foreach($batch as $data) {
				if ($i)
					$sql .= ",";
				$sql .= "('".$data."')";
				$i++;
			}
			$this->listingIdRange[] = $rowId = $lhb->rawQuery($sql);
			$endTime = microtime();
			$this->record("Time to save batch of ".count($batch)." listings to db: $index, was ".$this->diffTime($startTime, $endTime)." secs\n", 2);

			unset($batch);
		}
		$this->listingProcessed = $index;
		$this->record( "done parsing xml: $xml->name\n", 4 );

		//if (!$this->haveOutputFile) {
		//	$this->record("TERMINATE progress\n", 3);	
		//}
		return true;
	}

	protected function parseOneListing(&$newListing, &$listing, &$seller, &$geoInfo, $index)
	{
		$startTime = microtime();
		$checked = 0;
		$this->preExisting = null;
		$rejected = false;
		$this->minimumPrice = DEFAULT_MINIMUM_PRICE;
		$this->baseConsideredPrice = DEFAULT_MINIMUM_PRICE * $this->baseConsideredPriceMultiplier;
		$this->medianId = 0;
		$this->medianCityId = 0;

		$retval = false;

		$this->record("parseOneListing entered for block: $index", 3);

		try {
			$retval = $this->parseListing($newListing, $listing, $seller, $geoInfo, $checked);
		}
		catch(\Exception $e) {
			$this->updateToDb("parseOneListing - parseListing() Exception caught: ".$e->getMessage(), self::FATAL);
			$retval = false;
		}

		$this->record("parseOneListing completed parseListing for block: $index", 3);

		if ($retval)
		{
			if ($this->specialOpType == SPECIAL_OP_NONE)
				ob_flush();

			$endTime = microtime();
			$this->record("Time to parse one listing: $index, was ".$this->diffTime($startTime, $endTime)." secs\n", 3);

			// if ($this->preExisting)
			// 	$this->record("Pre-existing:".print_r($this->preExisting, true), 3);
			// $this->record("Read-in:".print_r($listing, true), 3);

			$mls = $listing['listhub_key'];
			if (($count = count($listing)) < 5)
			{
				$this->record("Listing with MLS:$mls has only $count fields, it is ignored.\n", 3);
				$this->rejected++;
				return false;
			}

			$isCommercial = ($listing['error'] & COMMERCIAL) != 0;
			$isRental = ($listing['error'] & RENTAL) != 0;
			$isHome = !isset($listing['tags']) ? 1 /* assume so */ : (!isset($listing['tags']['condo']) ? 1 : 0);

			try {
				if ( $listing['flags'] & LISTING_ADDRESS_HAS_UNIT_NUMBER) {
					if ( isset($listing['tags']) &&
						 !isset($listing['tags']['house']) ) {
						$lm = $this->getClass("ListHubMatchTags");
						if ( isset($listing['tags']['gated community']) ) {
							$hadCondo = isset($listing['tags']['condo']);
							if (isset($listing['tags']['condo'])) unset($listing['tags']['condo']);
							$listing['flags'] &= ~LISTING_IS_CONDO;
							$listing['tags']['house'] = $lm->getTagId('house');
							$this->record("Listing with MLS:$mls ".($hadCondo ? 'had' : "didn't have")." condo attribute in {$listing['city']}, {$listing['state']}, but is a gated community, set to house", 3);
						}
						elseif ( !isset($listing['tags']['condo']) ) {
							if ( !isset($listing['lotsize']) ||
								 empty($listing['lotsize']) ) {
								$listing['tags']['condo'] = $lm->getTagId('condo');
								$listing['flags'] |= LISTING_IS_CONDO; 	
								$this->record("Listing with MLS:$mls given condo attribute in {$listing['city']}, {$listing['state']}, has LISTING_ADDRESS_HAS_UNIT_NUMBER and 0 lotsize", 3);
							}
						}
					}
				}
				$isHome = !isset($listing['tags']) ? 1 /* assume so */ : (!isset($listing['tags']['condo']) ? 1 : 0);
				$baseMinumum = $this->minimumAcceptedPrice * $this->minimumPriceOffset;
				$baseMinumum = $isHome || $isCommercial ? $baseMinumum : $baseMinumum * 0.60;
				$this->record("Listing:{$listing['listhub_key']}, isHome:".($isHome ? 'yes' : 'no').", priced ({$listing['price']}), minimumAcceptedPrice:$this->minimumAcceptedPrice, baseMinumum:$baseMinumum", 3);

				if ( $isRental ||
					 intval($listing['price']) < $baseMinumum ) {
					$this->record("Listing with MLS:$mls is priced ({$listing['price']}) below threshold of baseMinumum:$baseMinumum or is a rental:".($isRental ? "yes" : "no"), 3);
					//$this->rejected++;
					$this->belowMinimumAcceptedPrice++;
					// call out getMedianPrice() so that even though it is below our minimumAcceptedPrice, we should
					// update our median prices for this zip code, so that it is representing 'true' data
					if ( ($listing['error'] & COMMERCIAL) == 0 &&
						 $this->useMedianPrices ) {
						$price = intval($listing['price']);
						$median = $this->getClass('MedianPrices', 1)->getMedianPrice($listing['zip'], $price, $isHome, $isRental);
						$median_home = $median['median_home'];
						$median_condo = $median['median_condo'];
						$this->medianId = $median['id'];
						$this->medianCityId = $median['city_id'];
						$this->minimumPrice = ($median_price = ($isHome ? $median['median_home'] * $this->medianPriceOffset : ($median['median_condo'] * $this->medianPriceOffsetCondo)) >= 500000 ? 500000 : $median_price);
						$this->record("Rental listing:{$listing['listhub_key']}, isHome:".($isHome ? 'yes' : 'no').", rental:".($isRental ? "yes" : "no").", price:({$listing['price']}), minimumPrice:$this->minimumPrice, median_home:$median_home, median_condo:$median_condo, medianPriceOffset:$this->medianPriceOffset, medianPriceOffsetCondo:$this->medianPriceOffsetCondo, medianId:$this->medianId, medianCityId:$this->medianCityId", 3);
					}
					// return false;
				}
			}
			catch(\Exception $e) {
				$this->updateToDb("parseOneListing - below threshold prices - Exception caught: ".$e->getMessage(), self::FATAL);
				return false;
			}

			$why = '';
			$bail = 0;
			if ( ($bail = $this->bailOutListing($listing, $why)) != 0 )
			{
				// $why = "Listing with MLS:".$listing['listhub_key']." - ".$why."\n";
				// $this->record($why, 3);
				$this->rejected++;
				$rejected = true;
			}
			$listing['error'] |= $bail;

			try {
				$startTime = microtime();
				$author_has_account = false;
				if (empty($seller['state'])) // then at least give it state from listing
					$seller['state'] = $listing['state'];
				if (($author_id = $this->findSeller($seller, $author_has_account, $listing['listhub_key'])) === false)
					$author_id = $this->createSeller($seller, $author_has_account, $listing['listhub_key']);
				$endTime = microtime();
				$this->record("Time to record seller with author_id: $author_id, author_has_account: ".(!$author_has_account ? "no" : "yes").", for listing: $index, was ".$this->diffTime($startTime, $endTime)." secs\n", 3);
			}
			catch(\Exception $e) {
				$this->updateToDb("parseOneListing - sellers - Exception caught: ".$e->getMessage(), self::FATAL);
				return false;
			}

			if ($author_id === false)
			{
				$this->record( "Failed to create author for index: $index, continuing to next if it exists\n", 3 );
				if (!$rejected) // don't double count
					$this->rejected++;
				$this->record($why, 3);
				return false;
			}

			if ($this->preExisting &&
				$this->preExisting->active == AH_NEVER) {
				$this->record( "This listing, id:$mls, is in NEVER category.  It is marked as rejected.\n", 3 );
				if (!$rejected) // don't double count
					$this->rejected++;
				$this->record($why, 3);
				// return false;
			}

			$listing['author'] = $author_id;
			//$listing['is_listhub'] = 1;
			//$listing['author_has_account'] = 0;

			try {
				$median_home = DEFAULT_MEDIAN_HOME;
				$median_condo = DEFAULT_MEDIAN_CONDO;
				if ( !$isRental &&
					 !$isCommercial &&
					 $this->useMedianPrices ) {
					$price = intval($listing['price']);
					$median = $this->getClass('MedianPrices', 1)->getMedianPrice($listing['zip'], $price, $isHome);
					$median_home = $median['median_home'];
					$median_condo = $median['median_condo'];
					$this->minimumPrice = ($median_price = ($isHome ? $median['median_home'] * $this->medianPriceOffset : $median['median_condo'] * $this->medianPriceOffsetCondo)) >= 500000 ? 500000 : $median_price;
					$this->medianId = $median['id'];
					$this->medianCityId = $median['city_id'];
					if ($price >= $this->minimumPrice &&
						$bail & TOOCHEAP) {// unset TOOCHEAP
						if ( $bail == TOOCHEAP ) { // this was the only reason for bailing out
							$this->rejected--;
							$rejected = false;
						}
						$listing['error'] &= ~TOOCHEAP;
						$bail &= ~TOOCHEAP;
					}
				}
				$this->baseConsideredPrice = $this->minimumPrice * ($isHome ? $this->baseConsideredPriceMultiplier : $this->baseConsideredPriceMultiplierCondo);
				$this->record("Listing:{$listing['listhub_key']}, isHome:".($isHome ? 'yes' : 'no').", minimumPrice:$this->minimumPrice, baseConsideredPrice:$this->baseConsideredPrice, useMedianPrices:".($this->useMedianPrices ? 'yes' : 'no').", minimumAcceptedPrice:$this->minimumAcceptedPrice, median_home:$median_home, median_condo:$median_condo, medianPriceOffset:$this->medianPriceOffset, medianPriceOffsetCondo:$this->medianPriceOffsetCondo, medianId:$this->medianId, medianCityId:$this->medianCityId", 3);
			}
			catch(\Exception $e) {
				$this->updateToDb("parseOneListing - check median prices - Exception caught: ".$e->getMessage(), self::FATAL);
				return false;
			}

			if ($bail & TOOCHEAP) {
				if (strlen($why))
					$why .= ": ";
				$why .= "Priced ({$listing['price']}) below the threshold of $this->minimumPrice";
			}
			// record an errors
			if ($bail) {
				$why = "Listing with MLS:".$listing['listhub_key']." - ".$why."\n";
				$this->record($why, 3);
			}

			// $this->record("For block: $index, bail:$bail", 3);

			try {
				// save keys 
				if (isset($listing['tags'])) {
					$tags = new \ArrayObject($listing['tags']);
					$tags = $tags->getArrayCopy();
					if (isset($listing['tags']['condo']))
						$listing['flags'] |= LISTING_IS_CONDO;
					unset($listing['tags']);

					$foundTagCount = count($tags);
				}
				else
					$foundTagCount = 0;
			}
			catch( \Exception $e) {
				$this->record("ArrayObject exception: ".$e->getMessage(), 3);
				if (isset($listing['tags'])) unset($listing['tags']);
				$foundTagCount = 0;
			}

			$startTime = microtime();
			$isDup = false;
			$listing['author_has_account'] = $author_has_account ? 1 : 0;
			if ($this->preExisting != null) {
				if ($this->preExisting->author_has_account != $listing['author_has_account']) 
					$this->log("author_has_account has been changed to ".$listing['author_has_account']." for listing id: {$this->preExisting->id}, with listhub_key: {$this->preExisting->listhub_key}, with sellerId: ".$seller->id.", author_id: ".($seller->author_id ? $seller->author_id : "N/A")." from ".$seller->city.", ".$seller->state);
			}
			else 
				$this->log("author_has_account for new listing is set to ".($listing['author_has_account'] ? "1" : "0")." for listhub_key: ".$listing['listhub_key']." with sellerId: ".$seller->id.", author_id: ".($seller->author_id ? $seller->author_id : "N/A")." from ".$seller->city.", ".$seller->state);
			
			if ($checked == 0) {
				if (!isset($listing['listhub_key'])) {
					$this->record("This listing failed to get checked for pre-existence, must not have the listhub_key value");
					$listing = false;
				}
				else {
					$this->record("This listing failed to get checked for pre-existence, but it has a listhub_key value: ".$listing['listhub_key']);
				}
			}
			
			// $this->record("For block: $index, before fixing city name", 3);
			// normalize city name first
			$listing['city'] = $this->stripCity($listing['city']);
			$listing['city'] = fix2CharCity($listing['city'], $listing['state']);
			$listing['city'] = fixCity($listing['city'], $listing['state']);
			$listing['state'] = fixNY($listing['city'], $listing['state']);
			$listing['city'] = $this->formatName($listing['city'], false, true);

			if ($this->preExisting) { // then see if we already have any listing tags that are not city ones
				$sql =  'SELECT b.id, b.type, b.tag FROM '.$this->getClass('ListingsTags')->getTableName().' AS a ';
				$sql .= 'INNER JOIN '.$this->getClass('Tags')->getTableName().' AS b ON a.tag_id = b.id ';
				$sql .= 'WHERE a.listing_id = '. $this->preExisting->id .' AND b.type = 0';
				$x = $this->getClass('ListingsTags')->rawQuery($sql);
				// let's merge the existing tags with ones extracting during the parse
				if ( !empty($x) ) {
					$this->record("Pre-existing listing has ".count($x)." tags", 3);
					foreach($x as $tag) {
						if (!in_array( intval($tag->id), $tags))
							$tags[$tag->tag] = intval($tag->id);
					}
					$this->record("after merging with Pre-existing listing has ".count($tags)." tags", 3);
					unset($x);
				}		
			}
			
			if (count($tags) <  $this->minimumTagCount) {
				$listing['error'] |= LOWTAGS;
			}
			else { // got enough tags, so lets see if we need to undeo the LACK_DESC
				$listing['error'] = $listing['error'] & ~LOWTAGS;
				if ($listing['error'] & LACK_DESC ||
					($this->preExisting &&
					 $this->preExisting->error & LACK_DESC)) {
					$useExistingAbout = false;
					if ( $this->preExisting &&
						 isset($this->preExisting->about) ) {
						if (!empty($this->preExisting->meta)) foreach($this->preExisting->meta as $meta) {
							if ($meta->action == LISTING_MODIFIED_DATA) {
								$useExistingAbout = isset($meta->about) ? 1 : 0;
							}
							unset($meta);
						}
					}
					$about = $useExistingAbout ? removeslashes($this->preExisting->about) : $listing['about'];
					if ( strlen( $about ) >= 80 ) {
						$this->record("Undoing LACK_DESC, length of about: ".strlen($about), 3);
						$listing['error'] = $listing['error'] & ~LACK_DESC;
					}
				}
			}

			// $this->record("For block: $index, before matchCity", 3);

			$this->mergeCityTags($tags, $listing);
			$this->matchCity($listing); // city_id should be set or is null or not set
			$this->record("matchCity returned for city:".(!empty($listing['city_id']) ? $listing['city_id'] : 'N/A'), 3);
			// check $listing['trigger'] and $listing['city_id']
			// if we have both, then save it and try to update the city_tags after all the listings are processed in this block
			if (isset($listing['city_id']) && $listing['city_id'] && $listing['city_id'] != -1 &&
				isset($listing['triggers']) && !empty($listing['triggers'])) {
				if (!array_key_exists($listing['city_id'], $this->triggerCities))
					$this->triggerCities[$listing['city_id']] = $listing['triggers']; // this is an array of ['ocean'=>19, etc..]
				else {
					// add any new ones to the city's tag list
					$keyList = array_keys($this->triggerCities[$listing['city_id']]);
					foreach($listing['triggers'] as $key=>$tag) {
						if (!array_key_exists($key, $keyList))
							$this->triggerCities[$listing['city_id']][$key] = $tag;
						unset($tag);
					}
					unset($keyList);
				}
				unset($listing['triggers']);
			}

			// $this->record("For block: $index, before createListing", 3);

			try {
				// listing returns as an object from createListing() if successful
				$listing = $this->createListing($listing, $listing['error'], $isDup, $seller, $foundTagCount); 
				if ($listing)
					$this->updateTags($tags,
									  $listing->id);
			}
			catch(\Exception $e) {
				$this->updateToDb("parseOneListing - createListing() - Exception caught: ".$e->getMessage(), self::FATAL);
				return false;
			}

			$endTime = microtime();
			if ($listing === false)
			{
				$this->record( "Failed to create listing for index: $index, continuing to next if it exists\n", 3 );
				if (!$rejected) // don't double count
					$this->rejected++;
				unset($tags);
				return false;
			}
			else {
				try {
					$placement = $this->activeToStr($listing->active); 
					$this->record("Time to record one listing: ".$listing->id.", key: ".$listing->listhub_key.", index: $index, placement: $placement, was ".$this->diffTime($startTime, $endTime)." secs\n", 3);
					$status = $isDup ? "DUPLICATE" : "SUCCESS";
					$reason = $isDup ? "Pre-existing in database": "Added to database";
					$count = count($tags);
					if ($count < ($this->minimumTagCount)) {
						$status = "WARNING";
						$reason = "Listing only has $count tags, the cutoff is $this->minimumTagCount";
					}
					$this->listHubRecord[] = array('listhub_key' => $listing->listhub_key,
													'status' => $status,
													'id' => $listing->id,
													'reason' => $reason);
				}
				catch(\Exception $e) {
					$this->updateToDb("parseOneListing - record for listhub - Exception caught: ".$e->getMessage(), self::FATAL);
					return false;
				}
			}
			unset($tags);

			// $this->record("For block: $index, before createActivity", 3);

			if ($listing->id) {
				$activity = new \stdClass();
				$activity->listing_id = $listing->id;
				$activity->author = (int)$author_id;
				try {
					//$activity->type = "listhub";
					$activity = $this->createActivity($activity);
				}
				catch(\Exception $e) {
					$this->updateToDb("parseOneListing - createActivity() - Exception caught: ".$e->getMessage(), self::FATAL);
					return false;
				}
				if ($activity === false)
				{
					$this->record( "Failed to create activity for index: $index, continuing to next if it exists\n", 3);
					//$xml->next();
					//$index++;
					return true;
				}
				unset($activity);
				
			}
			else
				$this->record( "Cannot update tags nor activity. listing->id is empty, with MLS: ".$listing->listhub_key."\n", 3);

			$geo = null;
			$cityGeo = null;
			try {
				$this->record("Geocode section\n", 2);
				$checkCityId = false;
				if ( ($geoCode = $this->hasGeoCodeAlready($geo)) == false) {
					$this->record("No Geocode found\n", 2);
					$doGeoCodingAnyway = false;
					if ($listing->active > AH_ACTIVE &&
						$this->doExtraGeoCoding) {
						if ( ($this->extraGeoCodingMask & (1 << $listing->active)) ) {
							if ( $this->geoCodeAllMaskType ||
							    ($this->extraGeoCodingSet & (1 << $listing->active)) == 0 ) 
								$doGeoCodingAnyway = true;
						}
					}

					if ($listing->active == AH_ACTIVE ||
						$doGeoCodingAnyway) {
						$google = null;
						if (count($geoInfo) && isset($geoInfo['lat']) && isset($geoInfo['lng']) &&
							!empty(floatval($geoInfo['lat'])) && !empty(floatval($geoInfo['lng'])) &&
							 abs(floatval($geoInfo['lat'])) <= 90 &&  abs(floatval($geoInfo['lng'])) <= 180 &&
							 abs(floatval($geoInfo['lat'])) >= -90 &&  abs(floatval($geoInfo['lng'])) >= -180) {// then get address
							$fields_geoinfo = [];
							$fields_geoinfo['lat'] = floatval($geoInfo['lat']);
							$fields_geoinfo['lng'] = floatval($geoInfo['lng']);
							$fields_geoinfo['address'] = $listing->street_address;
							if ( !$this->getClass('ListingsGeoinfo')->exists([ 'listing_id' => $listing->id ]) ){
								$fields_geoinfo['listing_id'] = $listing->id;
								$this->getClass('ListingsGeoinfo')->add($fields_geoinfo);
								$this->record( "Add geoCode for lat:{$geoInfo['lat']}, lng:{$geoInfo['lng']}, id:$listing->id at ".$listing->street_address."\n", 3);
							} else {
								$geo = $this->getClass('ListingsGeoinfo')->get((object)['where'=>[ 'listing_id' => $listing->id ]]);
								if ($geo[0]->lng != $fields_geoinfo['lng'] ||
									$geo[0]->lat != $fields_geoinfo['lat'] ||
									$geo[0]->address != $fields_geoinfo['address'] )
								$this->getClass('ListingsGeoinfo')->set([ (object)[ 'where' => [ 'listing_id' => $listing->id ], 'fields' => $fields_geoinfo ]]);
								$this->record( "Updated geoCode to lat:{$geoInfo['lat']}, lng:{$geoInfo['lng']}, id:$listing->id at ".$listing->street_address."\n", 3);
							}
							$fields_geoinfo['id'] = $listing->id;
							$fields_geoinfo['flags'] = $listing->flags;
							$this->record( "createListing calling updateListingFromTagSpace for listing:$listing->id with flags:$listing->flags");
							$this->updateListingFromTagSpace((object)$fields_geoinfo);
							unset($fields_geoinfo);
							// $geoCode = $geoInfo['lat'].'+'.$geoInfo['lng'];
							// $this->record("Doing getGeoCode with $geoCode\n", 3);
							// $geoCode = $this->getGeoCode($geoCode);
							// if ($geoCode->status == 'OK')
							// {
							// 	$this->record( "Got geoCode: ".$geoCode->data->formatted_address."\n", 3);
							// 	$geo = new \stdClass();
							// 	$geo->listing_id = $listing->id;
							// 	$geo->lat = $geoInfo['lat'];
							// 	$geo->lng = $geoInfo['lng'];
							// 	$geo->address = $geoCode->data->formatted_address;
							// }
							// else
							// 	$this->updateToDb("Failed geocodeLatLng for listing:".$listing->listhub_key." with lng:".$geoInfo['lng'].",".$geoInfo['lat']." with status: ".$geoCode->status, self::ERROR);
						} // try to get lng/lat using address
						else  {
							$this->record("[".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600))."] about to call geocodeListing for $listing->listhub_key at $listing->street_address, $listing->city $listing->state, active:$listing->active\n", 3);
							$forced = $this->getClass('Listings')->forceEnableLog();
							try {
								$geo = new \stdClass();
								$geo->listing_id = $listing->id;
								if ( $this->getClass('Listings')->geocodeListing($this->getClass("GoogleLocation"), $listing, $google, $this->getClass('Cities')) ) {
									$this->record("[".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600))."] geocodeListing successful for $listing->listhub_key at $listing->street_address, $listing->city $listing->state, active:$listing->active\n", 3);
									$geo->lat = $listing->result->query->lat;
									$geo->lng = $listing->result->query->lng;
									if (isset($listing->result->query->address))
										$geo->address = $listing->result->query->address;
									else
										$geo->address = $listing->street_address.', '.$listing->city.', '.$listing->state.(isset($listing->zip) ? ', '.$listing->zip : '').(isset($listing->country) ? ', '.$listing->country : '');
									if (isset($listing->update)) {
										$c = $this->getClass('Cities');
										$q = new \stdClass();
										$q->where = array('city' => $listing->city,
									  					  'state' => $listing->state);
										$x = $c->get($q);
										unset($q->where);
										$q->fields = array();
										if (!empty($x)) {
											if ((isset($listing->city_id) &&
											     $listing->city_id != $x[0]->id) ||
											   !isset($listing->city_id))
											$q->fields['city_id'] = $x[0]->id;
										} // empty!
										else { // so make a new one if possible
											if ( ($city_id = $this->getCityGeoCode($listing->city, $listing->state, $listing->zip, $listing->country, $cityGeo)) != null)
												$q->fields['city_id'] = $city_id;
										}
										if ($listing->update & 1)
											$q->fields['city'] = $listing->city;
										if ($listing->update & 2)
											$q->fields['state'] = $listing->state;
										$q->where = array('id'=>$listing->id);
										$a = array($q);
										try {
											$this->getClass('Listings')->set($a); // update city/state as needed.
										}
										catch(\Exception $e) {
											parseException($e);
										}
										unset($q, $a);
									}
									if ($doGeoCodingAnyway)
										$this->extraGeoCodingSet |= (1 << $listing->active);
								}
								else {
									$this->updateToDb("Failed geocodeListing for listing:".$listing->listhub_key." with ".$listing->street_address.", ".$listing->city." ".$listing->state." with status: ".$listing->result->status, self::ERROR);
									$this->record("Failed geocodeListing for listing:".$listing->listhub_key." with ".$listing->street_address.", ".$listing->city." ".$listing->state." with status: ".$listing->result->status."\n", 3);
									$geo->address = $listing->street_address.', '.$listing->city.', '.$listing->state.(isset($listing->zip) ? ', '.$listing->zip : '').(isset($listing->country) ? ', '.$listing->country : '');
									$geo->lat = -1;
									$geo->lng = -1;
									$checkCityId = true;
								}
							}
							catch(\Exception $e) {
								parseException($e);
								$this->record("Caught exception from geocodeListing - ".$e->getMessage()."\n", 3);
							}
							if ($forced)
								$this->getClass('Listings')->forceDisableLog();
							if ($geo) {
								$this->createGeoCode($geo);
								$geo->id = $listing->id;
								$geo->flags = $listing->flags;
								$this->record( "createListing calling updateListingFromTagSpace for listing:$listing->id with flags:$listing->flags");
								$this->updateListingFromTagSpace($geo);
								// unset($geo);
							}
							else
								$this->record( "Failed to create geocode for index: $index, continuing to next if it exists\n", 3);
						}
					}
				}
				// check city_id
				else
					$checkCityId = true;

				if ($checkCityId &&
					$this->preExisting &&
					(empty($this->preExisting->city_id) ||
					 ($this->preExisting->city_id == -1 &&
					  $this->forceRedoGeoCodeListing)) ) {
					$this->record("Pre-existing city has empty city_id for ".$this->preExisting->city.", ".$this->preExisting->state, 3);
					if ($this->getClass('Cities')->exists(['city'=>$this->preExisting->city,
															'state'=>$this->preExisting->state])) {
						$city = $this->getClass('Cities')->get((object)['where'=>[ 	'city'=>$this->preExisting->city,
																					'state'=>$this->preExisting->state]]);
						if (!empty($city)) {
							$this->record("Found pre-existing city: ".$this->preExisting->city.", ".$this->preExisting->state." with id: ".$city[0]->id, 3);
							try {
								$x = $this->getClass('Listings')->set([(object)['where'=>['id'=>$this->preExisting->id],
																		   		'fields'=>['city_id'=>$city[0]->id]]]);
								$this->record("Assigning city_id: ".$city[0]->id." to listing ".$this->preExisting->id.' was '.(!empty($x) ? 'successful' : 'a failure'), 3);
								if ($city[0]->lng == -1 &&
									$geo &&
									$geo->lat == -1) {
									if ($this->getCityGeoCode($this->preExisting->city, $this->preExisting->state, $this->preExisting->zip, $this->preExisting->country, $cityGeo) != null) {
										$this->getClass("ListingsGeoinfo")->set([(object)['where'=>['listing_id'=>$geo->listing_id],
																						  'fields'=>['lng'=>$cityGeo->lng,
																						  			 'lat'=>$cityGeo->lat]]]);
										$this->record("getCityGeoCode - updated @1 listing:$geo->listing_id with geocode from city:{$this->preExisting->city}, {$this->preExisting->state}, {$this->preExisting->country}, lat:$cityGeo->lat, lng:$cityGeo->lng", 3);
									}
								}
							}
							catch( \Exception $e) {
								$this->record("Failed to set city_id to {$city[0]->id} for listing:".$this->preExisting->id, 3);
							}
						}
						else
							$this->record("Failed to get existing city: ".$this->preExisting->city.", ".$this->preExisting->state, 3);
					}
					elseif ( ($city_id = $this->getCityGeoCode($this->preExisting->city, $this->preExisting->state, $this->preExisting->zip, $this->preExisting->country, $cityGeo)) != null) {
						$this->record("Got city_id:$city_id for listingId:{$this->preExisting->id} in {$this->preExisting->city}, {$this->preExisting->state}, {$this->preExisting->country}, lat:$cityGeo->lat, lng:$cityGeo->lng", 3);
						try {
							$x = $this->getClass('Listings')->set([(object)['where'=>['id'=>$this->preExisting->id],
																	   		'fields'=>['city_id'=>$city_id]]]);
							$this->record("Assigning city_id: ".$city_id." from getCityGeoCode() to listing ".$this->preExisting->id.' was '.(!empty($x) ? 'successful' : 'a failure'), 3);
							if ($geo &&
								$geo->lat == -1) {
								$this->getClass("ListingsGeoinfo")->set([(object)['where'=>['listing_id'=>$geo->listing_id],
																				  'fields'=>['lng'=>$cityGeo->lng,
																				  			 'lat'=>$cityGeo->lat]]]);
								$this->record("getCityGeoCode - updated @2 listing:$geo->listing_id with geocode from city:{$this->preExisting->city}, {$this->preExisting->state}, {$this->preExisting->country}, lat:$cityGeo->lat, lng:$cityGeo->lng", 3);
							}
						}
						catch( \Exception $e) {
							$this->record("Failed to set city_id to $city_id for listing:".$this->preExisting->id);
						}	
					}	
					else try {
						if (empty($this->preExisting->city_id) || $this->preExisting->city_id != -1) {
							$x = $this->getClass('Listings')->set([(object)['where'=>['id'=>$this->preExisting->id],
																	   		'fields'=>['city_id'=> -1]]]);	
							$this->record("Assigning city_id: -1 to listing ".$this->preExisting->id.' was '.(!empty($x) ? 'successful' : 'a failure'));
						}
					}
					catch( \Exception $e) {
						$this->record("Failed to set city_id to -1 for listing:".$this->preExisting->id);
					}								
				}
			}
			catch(\Exception $e) {
				$this->updateToDb("parseOneListing - geocoding - Exception caught: ".$e->getMessage(), self::FATAL);
				return false;
			}
			if (isset($geo)) unset($geo);
			if (isset($cityGeo)) unset($cityGeo);
			// TODO:
			// create listing points

			//$xml->next('Listing');
			//$this->record("After next it is now: ".$xml->name."\n");
			return true;
		}
		else
			return false;
	}

	protected function bailOutListing(&$listing, &$why = null, $didDetailedCharacteristics = true, $haveListingKey = true)
	{
		$error = 0;
		$mls = ($listing['listhub_key'] ? $listing['listhub_key'] : "N/A");

		if ( $haveListingKey &&
			 array_key_exists('price', $listing) &&
			 intval($listing['price']) < $this->minimumPrice)	
		{
			// $price = $listing['price'];
			// if ($why !== null)
			// 	$why = "Priced ($price) below the threshold of $this->minimumPrice";
			$err = TOOCHEAP;
			$error |= $err;
		}

		if ( $haveListingKey &&
			 (array_key_exists('beds', $listing) &&
			  array_key_exists('baths', $listing) ) &&
			  ($listing['beds'] == 0 || $listing['baths'] == 0) )
		{
			if ($why !== null) {
				if (strlen($why))
					$why .= ": ";
				$why .= "no baths or beds";
			}
			//return $this->saveRejected == false ? 1 : 2;
			$error |= NOBEDBATH;
		}

		if ( $haveListingKey) {
			 if ( array_key_exists('images', $listing) ) {
				if ( ($count = count($listing['images'])) == 0)		
				{
					if ($why !== null) {
						if (strlen($why))
							$why .= ": ";
						$why .= "no images";
					}			
					$error |= NOIMAGE;
				}
				else {
					// $removed = 0;
					// $savedCount = $count;
					// while($count)
					// {
					// 	if (!isset($listing['images'][$count-1]['file']) &&
					// 		!isset($listing['images'][$count-1]['url']))
					// 	{
					// 		$removed++;
					// 	}
					// 	$count--;
					// }
					
					// $savedCount -= $removed;
					$haveImage = false;
					if (isset($listing['images'])) foreach($listing['images'] as $img) {
						$img = (object)$img;
						if (isset($img->file) ||
							isset($img->url)) {
							$haveImage = true;
							break;
						}
					}
					// if ($savedCount == 0) {
					if (!$haveImage) {
						if ($why !== null) {
							if (strlen($why))
								$why .= ": ";
							$why .= "no images";
						}			
						$error |= NOIMAGE;
					}
				}
			}
			else
				$error |= NOIMAGE;
		}

		if ( $didDetailedCharacteristics &&
			array_key_exists('tags', $listing) &&
			count($listing['tags']) < $this->minimumTagCount)	
		{	
			$count = count($listing['tags']);
			if ($why !== null) {
				if (strlen($why))
					$why .= ": ";
				$why .= "needs at least $this->minimumTagCount tags, has $count tags";
			}	
			//return $this->saveRejected == false ? 1 : 2;
			if (!count($listing['tags']) ||
				 (isset($listing['about']) && strlen($listing['about']) < 80))
				$error |= LACK_DESC;
		}

		if ($listing['error'] & COMMERCIAL)
		{
			if ($why !== null) {
				if (strlen($why))
					$why .= ": ";
				$why .= "commercial property";
				$this->record("Listing MLS:".$listing['listhub_key']. " is set with COMMERCIAL in bailOutListing\n", 3);
			}	
			$error |= COMMERCIAL;
		}
		
		return $error;
	}


	protected function parseListing(&$newListing, &$listing, &$seller, &$geoInfo, &$checked)
	{
		$this->record( "parseListing\n" );
		$stopTheMadness = 0; // 0 = keep going, 1 = get out now, 2 = seems bad, but keep rejected flag is true so keep reading data
		$didDetailedCharacteristics = false;
		$haveListingKey = false;
		$skipHeavyData = false;
		$why = '';
		$recorded = false;
		$this->record("Begin parseListing - saveRejected: $this->saveRejected\n");
		$startTimeListing = microtime();
		$isNever = false;

		try {
			foreach($newListing->children() as $element)
			{
				$name = $element->getName();
				$count = $element->count();
				$this->record( "element count: $count, name: $name\n" );

				if (in_array($name, $this->ignoreList))
				{
					$this->record( "Ignoring element: $name\n" );
					continue;
				}

				if ($this->preExisting == null &&
					$checked  == 0 &&
					$haveListingKey)
				{
					$startTimeDb = microtime();
					$listings = $this->getClass("Listings", $this->debugListingGeoCoding);
					$q = new \stdClass(); 
					$q->where = array('listhub_key'=>$listing['listhub_key']);
					if(!empty( ($x= $listings->get($q)) ) ) {
						$this->preExisting = $x[0];
						// $this->record("Found Pre-existing:".print_r($x[0], true), 3);
					}
					// else if (isset($listing['mls'])) {
					// 	$q->where = array('mls'=>$listing['mls']);
					// 	if(!empty( ($x= $listings->get($q)) ) ) {
					// 		$this->preExisting = $x[0];
					// 	}
					// }
					if ($this->preExisting &&
						$this->preExisting->active == AH_NEVER)
						$isNever = true;
					$endTimeDb = microtime();
					$this->record("Time to get existing listing with MLS:".$listing['listhub_key'].", was ".$this->diffTime($startTimeDb, $endTimeDb)." secs, isNever: ".($isNever ? '1':'0').", got it:".(empty($this->preExisting) ? "no" : "yes"). ", has ".(!empty($this->preExisting) ? count($this->preExisting->images) : 0)." images.\n", 3);
					$checked = 1;
				}

				// if ($stopTheMadness != 0 &&
				// 	$checked != 0 &&
				// 	$haveListingKey) {
				// 	if (!$recorded) {
				// 		$startTimeDb = microtime();
				// 		$this->listHubRecord[] = array('listhub_key' => $listing['listhub_key'],
				// 										'status' => ($this->preExisting->active != AH_REJECTED &&
				// 													 $this->preExisting->active != AH_INACTIVE &&
				// 													 $this->preExisting->active != AH_TOOCHEAP ? "DROPPED" : "ERROR"),
				// 										'id' => ($listing['id'] ? $listing['id'] : 0),
				// 										'reason' => $why);
				// 		$endTimeDb = microtime();
				// 		$this->record("Time to record listhub for listing with MLS:".$listing['listhub_key'].", was ".$this->diffTime($startTimeDb, $endTimeDb)." secs\n", 3);
				// 		$recorded = true;
				// 	}
				// 	// if ($stopTheMadness == 1) {
				// 	// 	$this->record( "Ignoring element: $name\n" );
				// 	// 	continue;
				// 	// }
				// }


				// if ( $stopTheMadness == 0 &&
				// 	($stopTheMadness = $this->bailOutListing($listing, $why, $didDetailedCharacteristics, $haveListingKey)) )
				// {
				// 	$this->record("parseListing got from bailOutListing: $stopTheMadness, saveRejected: $this->saveRejected, for MLS:".$listing['listhub_key'].", why:$why\n");
				// 	// if ($stopTheMadness & TOOCHEAP) {
				// 	// 	$this->record( "Ignoring element: $name\n" );
				// 	// 	continue;
				// 	// }
				// }

				if ($name == 'DetailedCharacteristics' &&
					!$isNever)
					$didDetailedCharacteristics = true;

				if (!isset($this->parseRules[$name])) {
					$this->record( "Unknown element type: $name\n", 4 );
					continue;
				}

				if ( $haveListingKey &&
					 isset($listing['price']) &&
					 intval($listing['price']) < $this->minimumAcceptedPrice ) {
					// stop needless processing
					$skipHeavyData = true;
				}				

				if ($name == 'Photos' ||
					$name == 'DetailedCharacteristics') {
					if ($isNever ||
						($skipHeavyData && $name == 'Photos')) // these takes the most time, but would like the rest of the data...
						continue;
					$startTime = microtime();
				}


				$pre = null;
				if (isset($this->parseRules[$name]) &&
					array_key_exists('primary', $this->parseRules[$name]))
				{
					switch($this->parseRules[$name]['primary'])
					{
						case 'listing': $pre = &$listing; break;
						case 'seller': $pre = &$seller; break;
						case 'geoInfo': $pre = &$geoInfo; break;
						default:
							$this->record( "Unknown primary: $this->parseRules[$name]['primary']" );
					}
				}

				$this->parseElement($element, $listing, $seller, $geoInfo, $pre);

				if ($name == 'ListingKey')
					$haveListingKey = true;

				if ($name == 'Photos' ||
					$name == 'DetailedCharacteristics')
				{
					$endTime = microtime();
					$this->record("Time to process ".$name." was ".$this->diffTime($startTime, $endTime)." secs\n", 2);
				}

				if (is_array($pre)) {
					unset($pre);
					$pre = null;
				}
			} // end foreach()

			if ( ($stopTheMadness = $this->bailOutListing($listing, $why, $didDetailedCharacteristics, $haveListingKey)) )
			{
				$this->record("parseListing got from bailOutListing: $stopTheMadness, saveRejected: $this->saveRejected, for MLS:".$listing['listhub_key'].", why:$why\n");
				$startTimeDb = microtime();
				$this->listHubRecord[] = array('listhub_key' => $listing['listhub_key'],
												'status' => ($this->preExisting->active != AH_REJECTED &&
															 $this->preExisting->active != AH_INACTIVE &&
															 $this->preExisting->active != AH_TOOCHEAP ? "DROPPED" : "ERROR"),
												'id' => ($listing['id'] ? $listing['id'] : 0),
												'reason' => $why);
				$endTimeDb = microtime();
				$this->record("Time to record listhub for listing with MLS:".$listing['listhub_key'].", was ".$this->diffTime($startTimeDb, $endTimeDb)." secs\n", 3);
			}

			// if ($this->preExisting)
			// 	$this->record("Pre-existing:".print_r($this->preExisting, true), 3);
			// $this->record("Read-in:".print_r($listing, true), 3);

			$endTimeListing = microtime();
			$this->record("Time for listing with MLS:".$listing['listhub_key'].", was ".$this->diffTime($startTimeListing, $endTimeListing)." secs\n", 3);
			return true;
		}
		catch (\Exception $e){ $this->parseException($e); return false;}		
	}

	protected function parseElement(&$element, &$listing, &$seller, &$geoInfo, 
									&$predetermined = null,
									$index = -1)
	{
		$parentname = $element->getName();
		$parentRule = null;
		$prefix = null;
		$nth = 1.0;
		$startTime = microtime();
		if (!isset($this->parseRules[$parentname]))
		{
			$this->record( "parseElement - rules not set for $parentname\n", 4);
			return;
		}
		if (array_key_exists('prefix', $this->parseRules[$parentname]))
		{
			$prefix = $this->parseRules[$parentname]['prefix'];
			$pCount = count($element->children($prefix, true));
		}
		else
			$pCount = $element->count();
		$this->record( "parseElement parent: $parentname, count: $pCount, prefix: $prefix, index: $index\n", 2 );
		if ($parentname == 'Photos' && 
			$pCount > $this->maxPhotosPerListing)
			$nth = (float)$pCount / (float)$this->maxPhotosPerListing;

		if ($pCount) // then has sub children
		{
			if ($index == -1)
				$this->index = -1; // reset
			$nthCount = 0;
			$lastValidIndex = -1;
			foreach($element->children($prefix, $prefix ? true : false) as $sub)
			{
				if ($index == -1)
					$this->index++;

				$nthCount++;
				if ($nth != 1.0)
				{
					$test = (float)$nthCount / (float)$nth;
					if (floor($test) == $lastValidIndex)
						continue;
					$lastValidIndex = floor($test);
				}

				$subName = $sub->getName();
				if (isset($this->parseRules[$subName]) &&
					array_key_exists('prefix', $this->parseRules[$subName]))
				{
					$prefixSub = $this->parseRules[$subName]['prefix'];
					$count = count($sub->children($prefixSub, true));
				}
				else
					$count = $sub->count();
				$this->record( "parseElement sub: $subName, children: $count, rules: $this->parseRules[$parentname][$subName]\n", 2 );
				if(in_array($subName, $this->ignoreList))
					continue;

				$rule = $this->parseRules[$parentname][$subName];
				$parentRule = $this->parseRules[$parentname];
				if(!isset($rule) &&
					$rule !== 0)
				{
					$this->record( "parseElement sub: $subName has no rule!\n", 4 );
					continue;
				}

				if($rule == 0)
				{
					$this->record( "parseElement sub: $subName is ignored.\n" );
					continue;
				}

				$pre = (int)!is_null($predetermined);
				$set = (int)isset($rule['override']);
				$this->record( "parseElement - sub: $subName, setOverrride: $set, pre: $pre, count: $count, rule: $rule\n" , 2);
				if ($count) // nested data, recurse...
				{
					$savedIndex = $this->index;
				 	$nextIndex = in_array('retainIndex', $this->parseRules[$parentname]) ? $savedIndex : -1;
					if (is_array($rule) &&
						$set &&
						$pre)
						$this->override($subName, $predetermined);
					$this->parseElement($sub, $listing, $seller, $geoInfo, $predetermined, $nextIndex);
					$this->index = $savedIndex; // restore
					$this->record( "parseElement - sub: $subName, returned from recursion, index: $savedIndex\n", 2);
					continue;
				}

				if ($set && $pre)
				{
					$this->override($subName, $predetermined);
					$rule = $this->parseRules[$subName]['override'];
					$this->setValue($rule, $parentRule, $sub, $listing, $seller, $geoInfo, $predetermined);
				}
				else
					$this->setValue($rule, $parentRule, $sub, $listing, $seller, $geoInfo, $predetermined);

				$this->record( "parseElement - sub: $subName, after setValue\n", 2);
			} // end foreach($sub)
			if ($parentname == 'Photos')
			{
				$count = (int)count($listing['images']);
				$startCount = $count;
				$removed = 0;
				while($count)
				{
					if (!isset($listing['images'][$count-1]['file']) &&
						!isset($listing['images'][$count-1]['url']) &&
						!isset($listing['images'][$count-1]['discard']))
					{
						//array_splice($listing['images'], $count, 1);
						$removed++;
					}
					$count--;
				}
				
				//$count = (int)count($listing['images']);
				$startCount -= $removed;
				if ($removed)
				{
					if (!$startCount)
					{
						unset($listing['images']); // no good images left..
						$this->record("No more images left in this listing.\n", 3);
					}
				}
				$msg = "This listing has $startCount images, and kept ".($pCount - $removed)." images.\n";
				$this->record($msg, 3);
			}
		}
		elseif(isset($this->parseRules[$parentname]))
			$this->setValue($this->parseRules[$parentname], $parentRule, $element, $listing, $seller, $geoInfo, $predetermined);

		$endTime = microtime();
		if ( ($endTime - $startTime) > 2000)
			$this->record("Time to parse ".$parentname." was ".$this->diffTime($startTime, $endTime)." secs\n", 3);
	}

	/*
	// override existing values
	*/
	protected function override($subName, &$predetermined)
	{
		foreach($this->parseRules[$subName] as $tag=>$value)
		{
			if (!is_array($value) ||
				!isset($value))
				continue;
			if (isset($predetermined[$value[1]]))
				unset($predetermined[$value[1]]); // clear out
		}
	}

	/*
	// chose which object to populate
	*/
	protected function setValue($rule, $parentRule, &$data, &$listing, &$seller, &$geoInfo, &$predetermined = null)
	{	
		//print_r($rule);	
		//$empty = (int)!is_null($predetermined);
		//$this->record( "setValue - pre: $empty\n" );

		if (!is_null($predetermined))
		{
			$this->setObjectValue($rule, $parentRule, $data, $predetermined);
		}
		else {
			//$this->record( "setValue - using: $rule[0]\n" );
			switch($rule[0]) {
			case 'listing':
				$this->setObjectValue($rule, $parentRule, $data, $listing);
				break;
			case 'seller':
				$this->setObjectValue($rule, $parentRule, $data, $seller);
				break;
			case 'geoinfo':
				$this->setObjectValue($rule, $parentRule, $data, $geoInfo);
				break;
			}
		}
	}

	protected function setObjectValue($rule, $parentRule, $data, &$object)
	{
		$haveObj = (int)!is_null($object);
		$value = $data->__toString();
		$name = $data->getName();

		if (is_array($rule)) {
			$field = gettype($rule[1]);
			$callable = (int)array_key_exists('func', $rule);
			if (!$callable)
				$field = $rule[1];
			$count = count($rule);
			$this->record( "setObjectValue - type: $field, value: $value, callable: $callable, count: $count, object: $haveObj\n" );
		}
		elseif ($rule !== 2)
		{
			$this->record("Hmmm... the rule is neither array nor value of 2, skipping for value: $value\n", 3);
			return;
		}

		if (!$haveObj)
		{
			$this->record( "setObjectValue - $object is NULL!!!\n" );
			return;	
		}

		if ($name == 'ArchitectureStyle' &&
			$value == 'Other')
		{
			foreach($data->attributes() as $attr=>$value)
			{
				if ($attr == 'otherDescription')
				{
					// Oriental architecture is defined as 'Other'
					$type = explode(',', $value, 1);
					if ($type[0] == 'Oriental')
						$value = 'Oriental';
					break;
				}
			}
		}

		$attr = array();
		if (is_array($rule) &&
			array_key_exists('prefix', $rule) === true)
		{
			$prefix = $rule['prefix'];
			foreach($data->attributes($prefix, true) as $a=>$b)
				$attr[$a] = $b;
		}

		if ($rule === 2)
			$parentRule['func']($object, $name, $value);
		elseif (count($rule) == 2) // basic stuff
		{
			if ($callable == 1)
				$rule['func']($object, $value, $data);
			elseif (isset($object[$rule[1]]) === false) // new value
				$object[$rule[1]] = $value;
				//$object->{$rule[1]} = $data->__toString();
			else
				$object[$rule[1]] += $value;
		}
		elseif (array_key_exists('func', $rule) === true) // must be an anonymous function
			$rule['func']($object, $rule[1], $value, $attr);
		elseif (is_int($rule[2]) || is_float($rule[2]))
		{
			$type = gettype($rule[2]);
			$value = $rule[2];
			$set = isset($object[$rule[1]]);
			$content = $set ? $object[$rule[1]] : "blank";
			$contentType = $set ? gettype($object[$rule[1]]) : "N/A";
			$str = $data->__toString();
			$calc = (float)($rule[2] * (is_int($rule[2])  ? intval($str) : floatval($str)));
			$this->record( "factor is $value, type: $type, set: $set, content: $content, contentType: $contentType, str: $str, calc: $calc\n" );
			if (!isset($object[$rule[1]])) // new value
				$object[$rule[1]] = $calc;
			else
				$object[$rule[1]] += $calc;
		}
		else // should be a char
		{
			if (isset($object[$rule[1]]) === false)
				$object[$rule[1]] = (count($rule) == 2) ? $value : $rule[2].$value;
			else
				$object[$rule[1]] .= (count($rule) == 2) ? ' '.$value : ' '.$rule[2].$value;
		}
	}

	// Must be tested with ===, as in if(isXML($xml) === true){}
	// Returns the error message on improper XML
	protected function isXML($xml){
	    libxml_use_internal_errors(true);

	    $doc = new DOMDocument('1.0', 'UTF-8');
	    $doc->loadXML($xml);

	    $errors = libxml_get_errors();

	    if(empty($errors)){
	        return true;
	    }

	    $error = $errors[0];
	    if($error->level < 3){
	        return true;
	    }

	    $explodedxml = explode("r", $xml);
	    $badxml = $explodedxml[($error->line)-1];

	    $message = $error->message . ' at line ' . $error->line . '. Bad XML: ' . htmlentities($badxml);
	    return $message;
	}

	/************************  end parsing xml section of code ***********************/

	private function formatEmail($email) {
		$email = str_replace(';', ' ', $email);
		$email = str_replace(',', ' ', $email);
		$email = str_replace('/', ' ', $email);
		$email = str_replace('  ', ' ', $email);
		$email = trim($email);
		$parts = explode(' ', $email);
		$email = trim($parts[0]);

		return $email;
	}
	/*
	// find seller if one exists
	*/
	protected function findSellerHelper(&$q, $first, $last, $email, $info, $mls, &$turn) {
		$sellers = $this->getClass("Sellers", 1);
		$x = $sellers->get((object)['where'=>['first_name'=>$first,
											  'last_name'=>$last,
											  'email'=>$email]]);
		if (empty($x)) {
			$turn++;
			$x = $sellers->get($q); // first_name, last_name and email
			
			if (empty($x)) { // let's try it with formatName() applied to last name and with email only.
				$this->record("findSeller - unable to find seller using $first $last - {$info['email']}", 3);
				$turn++;
				unset($q->like2); // get rid of last_name
				$x = $sellers->get($q); // only first_name and email
				if (empty($x)) {
					$this->record("findSeller - unable to find seller using $first:".strlen($first)." - $email", 3);
					$turn++;
					unset($q->like); // get rid of first_name
					$q->like2 = ['last_name'=>$last]; // put back in last_name
					$x = $sellers->get($q); // only last_name and email
					if (!empty($x)) { // then test to see if 1st character of first_name is the same or not, if not, reject
						if ($x[0]->first_name[0] != $first[0]) {
							$this->record("findSeller - just last and email got for first:{$x[0]->first_name}, compared with $first and failed", 3);
							$x = 0;
						}
					}
					if (empty($x)) {
						$this->record("findSeller - unable to find seller using $last:".strlen($last)." - $email", 3);
						$turn++;
						// try it with first, last name and either phone or mobile
						unset($q->likeonlycase); // email gone!
						$havePhone = !empty($info['phone']) || !empty($info['mobile']);
						$q->like = ['first_name'=>$first]; // put back first_name
						if ($havePhone) {
							if (!empty($info['phone']) && !empty($info['mobile'])) {
								$q->or = ['elements' => [['phone'=>$info['phone']],
										   				 ['mobile'=>$info['mobile']]] ];
								$x = $sellers->get($q); // first_name, last_name and phone or mobile
								if (empty($x)) { // if not found, switch them around
									$q->or = ['elements' => [['mobile'=>$info['phone']],
										   				 	 ['phone'=>$info['mobile']]] ];
									$x = $sellers->get($q); // first_name, last_name and phone or mobile
								}
							}
							else {
								$phone = !empty($info['phone']) ? $info['phone'] : $info['mobile'];
								// $q->like3 = !empty($info['phone']) ? ['phone'=>$info['phone']] : ['mobile'=>$info['mobile']];
								$q->or = ['elements' => [['phone'=>$phone],
										   				 ['mobile'=>$phone]] ];
								$x = $sellers->get($q); // first_name, last_name and phone or mobile
							}
						}
						
						if (empty($x)) {
							if ($havePhone)
								$this->record("findSeller - unable to find seller using $first $last - phone:".(!empty($info['phone']) ? $info['phone'] : 'N/A')." or mobile:".(!empty($info['mobile']) ? $info['mobile'] : 'N/A'), 3);
							$turn++;
							if (isset($q->or)) unset($q->or); // get rid of phone/mobile
							$q->likeonlycase = ['city'=>$this->formatName($info['city'], false, true)];
							$x = $sellers->get($q); // first_name, last_name, and city
							if (empty($x)) {
								$this->record("findSeller - unable to find seller using $first $last - {$info['city']}", 3);
								$turn++;
							}
							else
								$this->record("findSeller - found seller:{$x[0]->id} using $first $last and city:{$info['city']}", 3);
						}
						else
							$this->record("findSeller - found seller:{$x[0]->id} using $first $last - phone:".(!empty($info['phone']) ? $info['phone'] : 'N/A')." or mobile:".(!empty($info['mobile']) ? $info['mobile'] : 'N/A'), 3);
						// }
					}
					else
						$this->record("findSeller - found seller:{$x[0]->id} using $last - $email", 3);
				}
				else
					$this->record("findSeller - found seller:{$x[0]->id} using $first - $email", 3);
			}
			else
				$this->record("findSeller - found seller:{$x[0]->id} using $first $last - $email", 3);
		}
		else
			$this->record("findSeller - found seller:{$x[0]->id} using simple WHERE sql for $first $last - $email", 3);

		return $x;
	}

	protected function updateAlternateListhubData(&$seller, $data) {
		$alternate = null;
		$metas = [];
		if (!empty($seller->meta)) foreach($seller->meta as $origId=>$meta) {
			$meta = (object)$meta;
			if ($meta->action == SELLER_ALTERNATE_LISTHUB_DATA) {
				$alternate = $meta;
			}
			else
				$metas[] = $meta;
			unset($meta);
		}
		$needUpdate = false;
		if (!$alternate) {
			if (count($data)) {
				$alternate = ['action'=>SELLER_ALTERNATE_LISTHUB_DATA];
				foreach($data as $id=>$value) {
					$alternate[$id] = [];
					$alternate[$id][] = $value;
					$this->record("updateAlternateListhubData - new: seller:$seller->id adding for $id: $value", 3);
					$needUpdate = true;
				}
			}
		}
		else {
			$alternate = (array)$alternate;
			foreach($data as $id=>$value) {
				if (!isset($alternate[$id])) {
					$alternate[$id] = [];
					$needUpdate = true;
				}
				$found = false;
				foreach($alternate[$id] as $element) {
					if ($element == $value) {
						$found = true;
						break;
					}
				}
				if (!$found) {
					$alternate[$id][] = $value;
					$this->record("updateAlternateListhubData - update: seller:$seller->id adding for $id: $value", 3);
					$needUpdate = true;
				}
			}
		}
		if ($needUpdate) {
			$metas[] = (object)$alternate;
			$seller->meta = $metas;
		}
		if (isset($alternate)) unset($alternate);
		unset($metas);
		return $needUpdate;
	}

	protected function findSeller(&$info, &$author_has_account, $mls)
	{
		$sellers = $this->getClass("Sellers", 1);
		$first = null; $last = null; $email = null;
		$q = new \stdClass(); 
		if (!empty($info['first_name']))
			$first = $this->formatName($info['first_name'], true);
		if (!empty($info['last_name']))
			$last = $this->formatName($info['last_name'], true);
		if (!empty($info['email']))
			$email = $this->formatEmail($info['email']);
		$office_email = '';
		if (!empty($info['office_email']))
			$office_email = $this->formatEmail($info['office_email']);

		$q->like = ['first_name'=>$first];
		$q->like2 = ['last_name'=>$last];
		$q->likeonlycase = [ 'email' =>$email ];
		$q->keepListHubNaming = true;
		try { 
			$startTime = microtime();
			$needUpdateLastName = false;
			$needUpdateFirsttName = false;
			$turn = 0;
			$author_has_account = false;
			$usedParticipantKey = false;

			if (isset($info['participant_key']) &&
				!empty($info['participant_key'])) {
				$x = $sellers->get((object)['where'=>['participant_key' => $info['participant_key']]]);
				if (!empty($x)) {
					$this->record("findSeller - found seller:{$x[0]->id} using participant_key - {$info['participant_key']}", 3);
					$usedParticipantKey = true;
				}
				else
					$x = $this->findSellerHelper($q, $first, $last, $email, $info, $mls, $turn);
			}
			else
				$x = $this->findSellerHelper($q, $first, $last, $email, $info, $mls, $turn);

			$endTime = microtime();
			$this->record("findSeller - search for seller, $first $last - $email took:".$this->diffTime($startTime, $endTime)." secs for $mls\n", 3);


			if(!empty($x)) {
				$this->record("findSeller - found seller:{$x[0]->id} on $turn using $first $last - $email", 3);
				$seller = $x[0];
				$alternateData = [];

				if (!empty($last) &&
					$seller->last_name != $last) {
					$seller->last_name = $info['last_name'] = $last;
					$needUpdateLastName = true;
				}
				if (!empty($first) &&
					$seller->first_name != $first) {
					$seller->first_name = $info['last_name'] = $first;
					$needUpdateFirstName = true;
				}
				// $author_has_account = $x[0]->author_id == undefined || $x[0]->author_id == null ? false : true;
				$author_has_account = empty($seller->author_id) ? false : true;
				

				$mod_meta = null;
				foreach($seller->meta as $meta)
					if ($meta->action == SELLER_MODIFIED_PROFILE_DATA) {
						$mod_meta = $meta;
						break;
					}
				// check to see if this seller has address, city, state
				$fields = array();
				if (!empty($info['street_address']) &&
					empty($seller->street_address)) 
					$seller->street_address = $fields['street_address'] = $info['street_address'];
				else {
					$addr1 = trim(strtolower(substr($seller->street_address, 0, ($pos = strpos($seller->street_address,"#")) !== false ? $pos : -1 )));
					$addr2 = trim(strtolower(substr($info['street_address'], 0, ($pos2 = strpos($info['street_address'],"#")) !== false ? $pos2 : -1)));
					if (levenshtein($addr1, $addr2) > 2) {// replace it
						$this->record("findSeller - updating street_address for sellerId:$seller->id, $first $last from $seller->street_address to {$info['street_address']}\n", 3);
						$seller->street_address = $fields['street_address'] = $info['street_address'];
					}
				}
				// strcmp(strtolower(substr($seller->street_address, 0, ($pos = strpos($seller->street_address,"#")) !== false ? $pos : -1 )), strtolower(substr($info['street_address'], 0, ($pos2 = strpos($info['street_address'],"#")) !== false ? $pos2 : -1))) == 0 ) ) {
				// $fields['street_address'] = $pos2 === false ? $this->formatName($info['street_address']) : $this->formatName(substr($info['street_address'], 0, $pos2)).substr($info['street_address'], $pos2, -1);
					

				if (!empty($info['city']) &&
					(empty($seller->city) ||
					strtolower($seller->city) != strtolower($info['city']))) {
						$info['city'] = $this->stripCity($info['city']);
						$info['city'] = fix2CharCity($info['city'], $info['state']);
						$info['city'] = fixCity($info['city'], $info['state']);
						$info['state'] = fixNY($info['city'], $info['state']);
						$info['city'] = $this->formatName($info['city'], false, true);
						// check again
						if ($info['city'] != $seller->city) {
							$this->record("findSeller - updating city for sellerId:$seller->id, $first $last from $seller->city to {$info['city']}\n", 3);
							$seller->city = $fields['city'] = $info['city'];
						}
				}

				if (!empty($info['state'])) {
					global $usStates;
					$testState = ucwords($info['state']);
					if (array_key_exists($testState, $usStates))
						$testState = $usStates[$testState];
					else
						$testState = strtoupper($testState);

					if (empty($seller->state) ||
						strtolower($seller->state) != strtolower($testState)) {
						$this->record("findSeller - updating state for sellerId:$seller->id, $first $last from $seller->state to $testState\n", 3);
						$seller->state = $fields['state'] = $testState;
					}
				}

				if (!empty($info['zip']) &&
					(empty($seller->zip) ||
					 $seller->zip != $info['zip']) ) {
					if (!empty($seller->zip)) $alternateData['zip'] = $seller->zip;
					$seller->zip = $fields['zip'] = $info['zip'];
				}

				if (!empty($info['country']) &&
					(empty($seller->country) ||
					strtolower($seller->country) != strtolower($info['country']))) {
					$seller->country = $fields['country'] = $info['country'];
				}

				if (!empty($info['phone'])) {
					$split = str_split($info['phone']);
					$tmp = '';
					foreach($split as $i) 
						$tmp .= is_numeric($i) ? $i : '';
					$info['phone'] = $tmp;
				}

				if (!empty($info['phone']) &&
					(empty($seller->phone) ||
					strcmp($seller->phone,$info['phone'])) &&
					(empty($mod_meta) ||
					 empty($mod_meta->phone)) ) {
					$this->record("findSeller - updating phone for sellerId:$seller->id, $first $last from $seller->phone to {$info['phone']}\n", 3);
					if (!empty($seller->phone)) $alternateData['phone'] = $seller->phone;
					$seller->phone = $fields['phone'] = $info['phone'];
				}

				if (!empty($info['mobile'])) {
					$split = str_split($info['mobile']);
					$tmp = '';
					foreach($split as $i)
						$tmp .= is_numeric($i) ? $i : '';
					$info['mobile'] = $tmp;
				}

				if (!empty($info['mobile']) &&
					(empty($seller->mobile) ||
					strcmp($seller->mobile,$info['mobile'])) &&
					(empty($mod_meta) ||
					 empty($mod_meta->mobile)) ) {
					$this->record("findSeller - updating mobile for sellerId:$seller->id, $first $last from $seller->mobile to {$info['mobile']}\n", 3);
					if (!empty($seller->mobile)) $alternateData['mobile'] = $seller->mobile;
					$seller->mobile = $fields['mobile'] = $info['mobile'];
				}

				if ($needUpdateFirstName) {
					$this->record("findSeller - updating first_name for sellerId:$seller->id, $first $last from $seller->first_name to {$info['first_name']}\n", 3);
					if (!empty($seller->first_name)) $alternateData['first_name'] = $seller->first_name;
					$fields['first_name'] = $info['first_name'];
				}

				if ($needUpdateLastName ) {
					$this->record("findSeller - updating last_name for sellerId:$seller->id, $first $last from $seller->last_name to {$info['last_name']}\n", 3);
					if (!empty($seller->last_name)) $alternateData['last_name'] = $seller->last_name;
					$fields['last_name'] = $info['last_name'];
				}

				if (!empty($info['company']) &&
					(empty($seller->company) ||
					strcmp($seller->company,$info['company'])) &&
					(empty($mod_meta) ||
					 empty($mod_meta->company)) ) {
					$this->record("findSeller - updating company for sellerId:$seller->id, $first $last from $seller->company to {$info['company']}\n", 3);
					$seller->company = $fields['company'] = $info['company'];
				}

				if (!empty($info['website']) &&
					(empty($seller->website) ||
					strcmp($seller->website,$info['website'])) &&
					(empty($mod_meta) ||
					 empty($mod_meta->website)) ) {
					$this->record("findSeller - updating website for sellerId:$seller->id, $first $last from $seller->website to {$info['website']}\n", 3);
					$seller->website = $fields['website'] = $info['website'];
				}

				if (!empty($info['email']) &&
					(empty($seller->email) ||
					strcmp($seller->email, $email)) ) {
					$this->record("findSeller - updating email for sellerId:$seller->id, $first $last from $seller->email to $email for $mls\n", 3);
					if (!empty($seller->email)) $alternateData['email'] = $seller->email;
					$seller->email = $fields['email'] = $email;
				}

				if (!empty($info['office_email']) &&
					(empty($seller->office_email) ||
					strcmp($seller->office_email, $office_email)) ) {
					$this->record("findSeller - updating office_email for sellerId:$seller->id, $first $last from $seller->office_email to $office_email for $mls\n", 3);
					if (!empty($seller->office_email)) $alternateData['office_email'] = $seller->office_email;
					$seller->office_email = $fields['office_email'] = $office_email;
				}

				if (isset($info['participant_key']) &&
					!empty($info['participant_key']) &&
					empty($seller->participant_key) ) {
					$this->record("findSeller - updating participant_key for sellerId:$seller->id, $first $last to {$info['participant_key']} for $mls\n", 3);
					$seller->participant_key = $fields['participant_key'] = $info['participant_key'];
				}

				if ($usedParticipantKey)
					if ($this->updateAlternateListhubData($seller, $alternateData)) {
						$fields['meta'] = $seller->meta;
						$this->record("findSeller - updating ALTERNATE_LISTHUB_DATA for sellerId:$seller->id for $mls\n", 3);
						if ( !($seller->flags & SELLER_HAS_ALTERNATE_LISTHUB_DATA) ) {
							$fields['flags'] = $seller->flags |= SELLER_HAS_ALTERNATE_LISTHUB_DATA;
						}
					}
				unset($alternateData);

				if ( count($fields) ) {
					unset($q);
					$startTime = microtime();
					$q = new \stdClass();
					$q->where = array('id'=>$seller->id);
					$q->fields = $fields;
					$a = array();
					$a[] = $q;
					$x2 = false;
					$this->lock();
					try {
						$x2 = $sellers->set($a); // updated it
					}
					catch (\Exception $e){ 
						$this->parseException($e, $info);
					}
					$this->unlock();
					$endTime = microtime();
					$this->record("findSeller - updating ".count($fields)." fields for seller, $first $last - {$info['email']} was ".(!empty($x2) ? 'successful' : 'failure').", took:".$this->diffTime($startTime, $endTime)." secs\n", 3);
					unset($fields);
					$x = $seller;
					// $startTime = microtime();
					// $x = $sellers->get($q); // get updated seller info
					// $endTime = microtime();
					// $this->record("findSeller - retrieving seller, $first $last - {$info['email']} took:".$this->diffTime($startTime, $endTime)." secs\n", 3);
				}
				$info = $seller; // it's an object now!
				
				return !$author_has_account ? $seller->id : $seller->author_id;
			}
			else
				return false;
		}
		catch (\Exception $e){ $this->parseException($e); return false;}
	}

	protected function createSeller(&$info, &$author_has_account, $listhub_key )
	{
		if (isset($info['tags']))
			unset($info['tags']);
		$sellers = $this->getClass("Sellers");
		try{
			$first = null; $last = null;
			if (!empty($info['first_name']))
				$first = $info['first_name'] = $this->formatName($info['first_name'], true);
			if (!empty($info['last_name']))
				$last = $info['last_name'] = $this->formatName($info['last_name'], true);
			if (!empty($info['street_address']))
				$info['street_address'] = $this->formatName($info['street_address']);
			if (!empty($info['city']))
				$info['city'] = $this->formatName($info['city'], false, true);
			$this->lock();
			$x = $sellers->add((object)$info);
			$this->unlock();
			// if (!empty($x)) {
			// 	$info = (object)$info;
			// 	$info->id = $x;
			// 	// author_has_account should be always be false, here, right?
			// 	$id = !$author_has_account ? $info->id : $info->author_id;
			// 	$this->record("createSeller - new seller at row:$x, id:$id ".(!$author_has_account ? "no" : "yes").", for ".(!empty($first) ? $first.' ' : "N/A ").(!empty($last) ? $last.' ' : "N/A ")." - city:{$info->city}, listhub_key:$listhub_key", 3);
			// }
			// $info will come back as an object if findSeller() succeeds!
			$id = $this->findSeller($info, $author_has_account);
			if ($id) {
				$this->record("createSeller - new seller at row:$x, id:$id ".(!$author_has_account ? "no" : "yes").", for ".(!empty($first) ? $first.' ' : "N/A ").(!empty($last) ? $last.' ' : "N/A ")." - city:{$info->city}, listhub_key:$listhub_key", 3);
				if ($x != $id) {// uh oh
					$this->record("createSeller - the new row:$x should have the same id:$id value!  removing duplicate!\n", 3);
					$sellers->delete(['id'=>$x]);
				}
			}
			else {
				$this->record("createSeller - failed to find new seller!\n", 3);
				$id = 0;
			}
			return $id;
		}
		catch (\Exception $e){ $this->parseException($e); return false;}
	}

	protected function objectToArray($o) { 
		$a = array(); 
		foreach ($o as $k => $v) 
			$a[$k] = (is_array($v) || is_object($v)) ? $this->objectToArray($v): $v; 
		return $a; 
	}

	protected function array_isDifferent(&$ar1, &$ar2)
	{
		// $this->record("entered array_isDifferent for ar1:".print_r($ar1, true).", ar2:".print_r($ar2, true), 3);
		$c1 = is_array($ar1) ? count($ar1) : count(get_object_vars($ar1));
		$c2 = is_array($ar2) ? count($ar2) : count(get_object_vars($ar2));
		if ($c1 != $c2)
			return true;

		/*
		$t1 = (array)(is_object($ar1) ? $this->objectToArray($ar1) : $ar1);
		$t2 = (array)(is_object($ar2) ? $this->objectToArray($ar2) : $ar2);
		$diff = array_diff( $t1, $t2 );
		*/
		foreach($ar1 as $k=>$v)
		{
			//$this->record("array_isDifferent - orig: $k => $v\n", 3);
			//$this->record(" new: ".$ar2[$k]."\n", 3);
			//if (empty($ar2[$k]) && $ar2[$k] !== 0)
			//if ($ar2[$k] != $v)
			if ( is_array($v) || is_object($v) )
			{
				if (!isset($ar2[$k])) // then element $k that exists in $ar1 doesn't exist in $ar2
					return true;

				if ( is_array($ar2[$k]) || is_object($ar2[$k]) ) 
					if ( $this->array_isDifferent($v, $ar2[$k]) )
						return true;
					else
						continue;
				else
					return true;
			}
			if (!isset($ar2[$k])) {// then element $k that exists in $ar1 doesn't exist in $ar2
				$this->record("array_isDifferent - tested $k, which ar2 doesn't have, returning true", 3);
				return true;
			}
			// if (empty($ar2[$k]) && $ar2[$k] !== 0)
				// return true;
			if ($ar2[$k] != $v) {
				$this->record("array_isDifferent - tested $k: {$ar2[$k]} against $v, return true", 3);
				return true;
			}

			// $this->record("array_isDifferent - tested $k: {$ar2[$k]} against $v", 3);

		}

		return false;
	}
										// array, stdClass
	protected function checkSameAddress($newOne, $oldOne)
	{
		$sameAddr = true;
		if ( strtolower($newOne['state']) != strtolower($oldOne->state) )
			$sameAddr = false;
		elseif ( strtolower($newOne['city']) != strtolower($oldOne->city) )
			$sameAddr = false;
		//elseif ( strtolower($newOne['street_address']) != strtolower($oldOne->street_address) )
		elseif ( strtolower(substr($oldOne->street_address, 0, ($pos = strpos($oldOne->street_address,"#")) !== false ? $pos : -1 )) != strtolower(substr($newOne['street_address'], 0, ($pos2 = strpos($newOne['street_address'],"#")) != false ? $pos2 : -1)) ) 
			$sameAddr = false;
		
		return $sameAddr;
	}

	protected function formatName(&$data, $isPerson = false, $isCity = false) {
		if (!isset($data) || empty($data))
			return $data;
		$data = html_entity_decode($data, ENT_QUOTES | ENT_HTML5);
		$data = strtolower(trim($data));
		$data = str_replace('-',' ',$data);
		$data = str_replace(',',' ',$data);
		if (!$isPerson)
			$data = str_replace('&',' ',$data);
		else
			$data = preg_replace('/[0-9]/', '', $data);
		$data = str_replace('  ',' ',$data);
		$data = str_replace("''","'",$data);
		$data = str_replace("`","'",$data);
		$t = explode(' ', $data);
		$data = '';
		$doNotUpperFirst = ['the','for','by','on'];
		if ( !isset($this->agentAcronymMakeUppercase) ||
			 count($this->agentAcronymMakeUppercase) == 0)
			$makeUpperCase = ['crs','mba','gri','cdpe','qsc','mrp','abr','ii','(dr)','(e)','pa','iii','p.a.','a.b.','ab','cpa','csr','dpp','sres','rli','ltg','sfr','cips','rebac','crb','ccim','epro','bpor','sf','csp','sfr.','pc','cai','pro','srs','cne','tahs','rs','cdrs','asr','chms','mre','reos','csn','cbr','ahwd','trc','afh.','gri.','trlp'];
		else
			$makeUpperCase = &$this->agentAcronymMakeUppercase;

		if ( !isset($this->agentAcronymDoubleCheck) ||
			  count($this->agentAcronymDoubleCheck) == 0)
			$doubleCheck = ['reos','reo'];
		else
			$doubleCheck = &$this->agentAcronymDoubleCheck;

		$x = [];
		foreach($t as $str)
			if (!empty($str))
				$x[] = $str;

		$t = $x;
		unset($x);

		foreach ($t as $i=>$str){
			if ($str == ' ' ||
				empty($str))
				continue;
			if ($i > 0) $data .= ' ';
			if ($i < 1 || !in_array($str, $doNotUpperFirst)) {
				if ($isPerson &&
					in_array($str, $makeUpperCase) ) {
					if (!in_array($str, $doubleCheck) ||
						(in_array($str, $doubleCheck) &&
						 count($t) > 1) )
						$str = strtoupper($str);
					else
						$str = ucfirst($str);
				}
				else {
					if ($isCity) {
						if ($i == 0) { // first word
							if ($str == 'st' ||
							 	$str == 'saint')
								$str = 'st.'; // fix St and Saint to be St.
							elseif ( strlen($str) > 3 &&
								 	 strpos($str, 'st.') === 0 ) {// then this must be a concatenated name like St.matthews
								$tmp = explode('.', $str);
								$str = '';
								foreach($tmp as $x) {
									if (strlen($str)) $str .= ' ';
									$str .= $x == 'st' ? 'st.' : $x;
									unset($x);
								}
								unset($tmp);
							}
						}
						else if ($str == 'st' || // after first word
								 $str == 'st.')
							$str = 'saint';

						if ($str == 'islan')
							$str = 'island';
					}
					elseif (!$isPerson && // not isCity and not isPerson, then must be street_address
							 $str == 'st.')
						$str = 'st'; // simplify St. to St
					$str = ucwords($str);
				}
				if ( (substr($str, 0,2) == "Mc" ||
					  substr($str, 0,2) == "O'"||
					  substr($str, 0,2) == "D'") &&
					strlen($str) > 2)
					$str[2] = chr( ord($str[2]) - 32 ); // cap that 3rd letter

				if ( $isPerson &&
					 ($str[0] == '(' ||
					  $str[0] == '"') &&
					  strlen($str) > 2) // then cap 2nd letter
					$str[1] = chr( ord($str[1]) - 32 );
			}
			$data .= trim($str);
			unset($i, $str);
		} unset($t);
		return $data;
	}

	protected function formatNameOldWay(&$data) {
		if (!isset($data) || empty($data))
			return $data;

		$data = strtolower(trim($data));
		$data = str_replace('-',' ',$data);
		$data = str_replace('  ',' ',$data);
		$t = explode(' ', $data);
		$data = '';
		$doNotUpperFirst = ['the','for','by','on'];
		foreach ($t as $i=>$str){
			if ($i > 0) $data .= ' ';
			if ($i < 1 || !in_array($str, $doNotUpperFirst)) $str = ucfirst($str);
			$data .= $str;
		} unset($i, $t, $str);
		return $data;
	}

	protected function stripCity(&$data) {
		if (!isset($data) || empty($data))
			return $data;

		$pos = -1;
		if ( ($pos = stripos($data, "(city)")) !== false)
			$data = str_ireplace("(city)", '', $data);
		return $data;
	}

	protected function assignCategory(&$owner, &$ownerName, $price, $bail) {
		if ($bail & COMMERCIAL) {
			$owner = AH_NEVER; $ownerName = 'Never';
		}
		else if ( $bail & RENTAL) {
			$owner = AH_RENTAL; $ownerName = 'Rental';
		}
		elseif (($bail == 0 ||
			 	 $bail == LACK_DESC) && // if just this and not LOWTAGS, allow it to go through
				 $price >= $this->minimumPrice) {
			$owner = AH_ACTIVE; $ownerName = "Main Listing";
		}
		elseif ($bail == 0 ||
				($bail && 
				 !($bail & TOOCHEAP))  ) {
			$owner = AH_WAITING; $ownerName = "Waiting"; 
		}
		elseif ($price > $this->baseConsideredPrice) {
			if ($bail & NOBEDBATH) {
				$owner = AH_NEVER; $ownerName = 'Never'; // TOOCHEAP & NOBEDBATH
			}
			else if ($bail & (LACK_DESC | LOWTAGS | NOIMAGE)) {
				$owner = AH_REJECTED; $ownerName = 'Rejected'; // TOOCHEAP & LACK_DESC OR LOWTAGS OR NOIMAGE
			}
			else {
				$owner = AH_TOOCHEAP; $ownerName = 'TooCheap';
			}
		} 
		elseif ($bail & (NOBEDBATH | NOIMAGE | LACK_DESC | LOWTAGS) ) {
			$owner = AH_NEVER; $ownerName = 'Never';
		} 
		else { // below BASE_CONSIDERED_PRICE
			$owner = AH_AVERAGE; $ownerName = 'Average';
		}
	}

	// called only for AH_ACTIVE or AH_WAITING
	protected function matchCity(&$listing) {
		// if ($listing['active'] != AH_ACTIVE && $listing['active'] != AH_WAITING)
		// 	return;
		if ($listing['active'] == AH_INACTIVE)
			return;

		$c = $this->getClass('Cities');
		$q = new \stdClass();

		if ($this->preExisting &&
			!empty($this->preExisting->city_id) &&
			$this->preExisting->city_id != -1) {
			if (!empty($this->preExisting->city)) {
				$q->where = array('id'=>$this->preExisting->city_id);
				$x = $c->get($q);
				if (!empty($x)) {
					if ($x[0]->lng != -1 &&
						!empty($x[0]->city) &&
						$this->preExisting->city == $x[0]->city) {
						$listing['city_id'] = $this->preExisting->city_id; // this will reduce the log saying 'city_id' doesn't exist in incoming listing
						$listing['city'] = $x[0]->city;
						unset($q->where, $x);
						return;
					}
				}
				unset($q->where, $x);
			}
		}

		// try to find it by ZIP if city/state is no good
		if (!isset($listing['city']) ||
			empty($listing['city']) || 
			nonConformingLocation($listing['city']) ||
			!isset($listing['state']) ||
			empty($listing['state']) ) {
			if (!empty($listing['zip']) &&
				$listing['zip'] != '00000' &&
				$listing['zip'] != '99999') {
				$result_country = $result_state = $result_city = $google_result = null;
				try {
					if (!$this->getClass('Cities')->geocodeZip($this->getClass('GoogleLocation'), $listing['zip'], $result_city, $result_state, $result_country, $google_result)) {
						$this->record("Failed to geocodeZip - zip:{$listing['zip']}\n", 3);
						return null;
					}
					$this->record("geocodeZip - found $result_city, $result_state $result_country from {$listing['zip']}", 3);
					$ok = true;
					if (!empty($result_country) ) {
						if (!empty($listing['country']) &&
							!nonConformingLocation($listing['country'])) {
							if ( $listing['country'] != $result_country) {
								$ok = false;
								$this->record("geocodeZip - revoking $result_city since incoming country:{$listing['country']} does not match $result_country", 3);
							}
						}
						else {
							$listing['country'] = $result_country;
							$this->record("geocodeZip - assigning $result_country to {$listing['listhub_key']}", 3);
						}
					}

					if ($ok) {
						if (!empty($result_state) ) {
							if (!empty($listing['state']) &&
								!nonConformingLocation($listing['state'])) {
								if ( $listing['state'] != $result_state) {
									$ok = false;
									$this->record("geocodeZip - revoking $result_city since incoming state:{$listing['state']} does not match $result_state", 3);
								}
							}
							else {
								$listing['state'] = $result_state;
								$this->record("geocodeZip - assigning $result_state to {$listing['listhub_key']}", 3);
							}
						}
						elseif (nonConformingLocation($listing['state']))
							unset($listing['state']);
					}

					if ($ok) {
						$listing['city'] = $this->formatName($result_city, false, true);
						$this->record("geocodeZip - assigning $result_city to {$listing['listhub_key']}", 3);
						$x = $c->get((object)['where'=>['city' => $listing['city'],
						  								'state' => (isset($listing['state']) ? $listing['state'] : null) ]]);
						$cityId = 0;
						if (!empty($x)) {
							$listing['city_id'] = $x[0]->id;
							if (empty($x[0]->lat) ||
								$x[0]->lat == -1) {
								$c->set([(object)['where'=>['id'=>$x[0]->id],
												  'fields'=>['lat'=>$google_result->geometry->location->lat,
												  			 'lng'=>$google_result->geometry->location->lng]]]);
							}
							$cityId = $x[0]->id;
							$city = $x[0];
						}
						else {
							$city = (object)['city'=>$listing['city'],
											 'state' => (isset($listing['state']) ? $listing['state'] : null),
											 'country' => $listing['country'],
											 'lng' => $google_result->geometry->location->lng,
											 'lat' => $google_result->geometry->location->lat,
											 'flags'=> 0];
							$x = $c->add($city);
							unset($city);
							if ($x)
								$cityId = $listing['city_id'] = $x;

						}
						$this->updateCityFromTagSpace($cityId, $city);
						return;
					}
					
				}
				catch(\Exception $e) {
					$this->record("Exception for geocodeZip:".$e->getMessage(), 3);
					parseException($e);
					return null;
				}
			}
			else
				return;
		}

		$listing['state'] = fixNY($listing['city'], $listing['state']);
		$listing['city'] = fixCity($listing['city'], $listing['state']);
		$listing['city'] = ucwords(fixCityStr($listing['city']));

		$q->where = array('city' => $listing['city'],
						  'state' => $listing['state']);
		$x = $c->get($q);
		if (!empty($x)) {
			if (count($x) == 1) {
				// if ($x[0]->lng != -1) 
				$listing['city_id'] = $x[0]->id;
				return;
			}
			else {
				foreach($x as $city) {
					if (!empty($city->zip)) {
						// don't know why I was using json_decode() here... ??  6/17/2016
						// $zip = json_decode($city->zip);
						$zip = $city->zip;
						foreach($zip as $code) {
							// 
							// if ($code == intval($listing['zip']) ) {  test as-is, may be alphanumeric, not all numeric  6/17/2016
							if ($code == $listing['zip'] ) {
								$listing['city_id'] = $city->id;
								return;
							}
						}
					}
				}
				$this->log("matchCity - no match for cities:".print_r($x, true).", zip:".$listing['zip'], 3);
			}
		}

		if (nonConformingLocation($listing['city']))
			return;
		if (nonConformingLocation($listing['state']))
			return;
		if (nonConformingLocation($listing['country']))
			return;

		$listing['city_id'] = $this->getCityGeoCode($listing['city'], $listing['state'], $listing['zip'], $listing['country']);
	}

	protected function getCityGeoCode($cityStr, $state, $zip, $country, &$geoCode = -1) {
		$c = $this->getClass('Cities');
		// do getCode for this city
		$state = fixNY($cityStr, $state);
		$cityStr = fixCity($cityStr, $state);
		$cityStr = ucwords(fixCityStr($cityStr));

		if (nonConformingLocation($cityStr))
			return null;
		if (nonConformingLocation($state))
			return null;

		$city = (object)['city'=>$cityStr,
						 'state' => $state,
						 'zip' => $zip,
						 'country' => nonConformingLocation($country) ? '' : $country,
						 'lng' => null,
						 'lat' => null];
		// if (!empty($listing['zip']))
		// 	$city->zip = $listing['zip'];
		$lng = $lat = -1;
		$result_city = $google_result = null;
		try {
			$this->getClass('Cities')->forceEnableLog();
			if (!$this->getClass('Cities')->geocodeCity($this->getClass('GoogleLocation'), $city, $lng, $lat, $result_city, $google_result)) {
				$this->record("Failed to geocode city:$city->city, $state $country\n", 3);
				$this->getClass('Cities')->forceDisableLog();
				return null;
			}
		}
		catch(\Exception $e) {
			$this->record("Exception from geocodeCity:".$e->getMessage(), 3);
			parseException($e);
			$this->getClass('Cities')->forceDisableLog();
			return null;
		}
		$this->getClass('Cities')->forceDisableLog();
		
		$city->lng = $lng;
		$city->lat = $lat;
		
		if ($c->exists(['city'=>$cityStr,
						'state'=>$state])) {
			$c->set([(object)['where'=>['city'=>$cityStr,
										'state'=>$state],
							  'fields'=>['lng'=>$lng,
							  			 'lat'=>$lat]]]);
			$x = $c->get((object)['where'=>['city'=>$cityStr,
											'state'=>$state]]);
			$x = $x[0]->id; // return row id
		}
		else
			$x = $c->add($city);
		if ($geoCode !== -1) {
			$geoCode = $city;
			$geoCode->id = $x; // row id
		}
		
		if ($x) {
			$city = $c->get((object)['where'=>['id'=>$x]]);
			$this->updateCityFromTagSpace($x, $city[0]);
		}
		else 
			$x = null;

		unset($city);
		return $x;
	}

	protected function checkSellerEmailDb($listing, $seller) {
		if ($listing->active != 1 &&
			$listing->active != 2)
			return;

		$EmailDb = $this->getClass("SellersEmailDb");
		$this->lock();
		$emailDb = $EmailDb->get((object)['where'=>['seller_id'=>$seller->id]]);
		$this->unlock();
		if (empty($emailDb)) {
			$email = explode('@', $seller->email);
			if (count($email) == 2) {
				$db = (object)['seller_id'=>$seller->id,
								'name'=>$email[0],
								'flags'=>(1 << (20+$listing->active)),
								'host'=>strtolower( $email[1] )];
				$msg = (object)['msg'=>"sellerId: {$seller->id}, email:{$seller->email}"];
				$this->updateToDb(json_encode($msg), self::EMAIL_DB_ADDED);
				$this->lock();
				try {
					$EmailDb->add($db);
				}
				catch( \Exception $e) {
					$this->log("checkSellerEmailDb - caught an exception: ".$e->getMessage(), 3);
				}
				$this->unlock();
				unset($db);
			}
		}
		else if (!empty($seller->email)) {
			$emailDb = array_pop($emailDb);
			if ( ($emailDb->flags & (1 << (20+$listing->active))) == 0) {
				$this->lock();
				$EmailDb->set([(object)['where'=>['id'=>$emailDb->id],
										'fields'=>['flags'=>($emailDb->flags | (1 << (20+$listing->active)))]]]);
				$this->unlock();
				$msg = (object)['msg'=>"sellerId: {$seller->id}, email:{$seller->email}"];
				$this->updateToDb(json_encode($msg), self::EMAIL_DB_UPDATED);
			}
		}
	}

	protected function createListing(&$info, $bail, &$isDup, &$seller, $foundTagCount)
	{
		$listings = $this->getClass('Listings');
		$ownerName = "Waiting"; // default
		$owner = AH_WAITING;
		$price = intval($info['price']);
		$author_has_account = !empty($seller->author_id) ? 1 : 0;

		$pos2 = strpos('#', $info['street_address']);
		$info['street_address'] = $pos2 === false ? $this->formatName($info['street_address']) : $this->formatName(substr($info['street_address'], 0, $pos2)).substr($info['street_address'], $pos2, -1);
		// city is normalized now before calling createListings() and mergeCityTags()....
		$info['country'] = $info['country'] == 'USA' ? 'US' : $info['country'];
		$state = ucwords( strtolower($info['state']) );
		if (array_key_exists ( $state , $this->usStates ))
			$info['state'] = $this->usStates[$state];

		if ($this->preExisting == null) {// check if there's a 'mls' match 
			$q = new \stdClass(); 
			$q->where = array('mls'=>$info['mls'],
							  'author'=>$info['author']);
			if(!empty( ($x= $listings->get($q)) ) ) 
				$x = $this->preExisting = $x[0];
			else { // try it again, just in case
				$q->where = array('listhub_key'=>$listing['listhub_key']);
				if(!empty( ($x= $listings->get($q)) ) ) {
					$this->preExisting = $x[0];
				}
				else
					$x = null; // don't want false
			}
		}

		if ($this->preExisting != null) {
			$owner = $this->preExisting->active;
			$activity = $this->getClass("ListingsActivity");
			$q = new \stdClass(); 
			$q->where = array('listing_id'=>$this->preExisting->id);
			$activities = $activity->get($q); 
			$hasForcedActivity = false;
			$dateOfChange = null;
			if (!empty($activities)) { //may end up with multiple activities from multiple authors
				foreach($activities as $acts) {
					foreach($acts->data as $act) {
						if ($act->action == "active state change") {
							$dateTime = explode(" ", $act->date);
							$date = explode("-", $dateTime[0]);
							$time = explode(":", $dateTime[1]);
							$unixTime = mktime($time[0],$time[1],$time[2],$date[1],$date[2],$date[0]);
							if ($dateOfChange != null &&
								$dateOfChange > $unixTime) // get latest change
								continue;
							$dateOfChange = $unixTime;
							$hasForcedActivity = $act->to;
						}
					}
				}
			}

			if ($hasForcedActivity !== false)
			{
				if ($this->preExisting->active != $hasForcedActivity) {
					$this->record("This pre-existing listing:".$this->preExisting->listhub_key.", was moved to ".$this->activeToStr($hasForcedActivity).", but it's db record is:".$this->activeToStr($this->preExisting->active)."\n", 3);
				}
				else
					$this->record("This pre-existing listing:".$this->preExisting->listhub_key.", was moved to $hasForcedActivity\n", 3);

				if ($hasForcedActivity == AH_INACTIVE) // too bad huh, it's in the feed, so let's put it in AH_WAITING
					$hasForcedActivity = AH_WAITING;

				switch($hasForcedActivity) {
					case AH_ACTIVE: $owner = AH_ACTIVE; $ownerName = "Main Listing"; break;
					case AH_NEVER: $owner = AH_NEVER; $ownerName = "Never"; break;
					case AH_AVERAGE: $owner = AH_AVERAGE; $ownerName = 'Average'; break;
					case AH_INACTIVE: $owner = AH_INACTIVE; $ownerName = 'Inactive'; break;
					case AH_WAITING: $owner = AH_WAITING; $ownerName = "Waiting"; break;
					case AH_REJECTED: $owner = AH_REJECTED; $ownerName = 'Rejected'; break;
					case AH_TOOCHEAP: $owner = AH_TOOCHEAP; $ownerName = "TooCheap"; break;
				}
			}
			else switch($this->preExisting->active) {
				//case AH_INACTIVE: $ownerName = "Inactive"; $owner = AH_INACTIVE; break;
				case AH_NEVER: $ownerName = "Never"; $owner = AH_NEVER; break;
				case AH_RENTAL: $ownerName = "Rental"; $owner = AH_RENTAL; break;
				case AH_ACTIVE: 
				case AH_AVERAGE:
				case AH_INACTIVE:
				case AH_WAITING: 
				case AH_TOOCHEAP: 
					$this->assignCategory($owner, $ownerName, $price, $bail);
					break;
				case AH_REJECTED: 
					if (!$bail ||
			 			 $bail == LACK_DESC) { // no errors now
						$owner = AH_WAITING; $ownerName = 'Waiting'; 
					} else if ($bail && !($bail & TOOCHEAP)) {
						$owner = AH_REJECTED; $ownerName = 'Rejected'; 
					} else {
						$owner = AH_TOOCHEAP; $ownerName = 'TooCheap';
					} break;
				// case AH_TOOCHEAP: 
				// 	if ((!$bail ||
			 // 			 $bail == LACK_DESC) && 
			 // 			 $price >= $this->minimumPrice) { // no errors now
				// 		// $owner = AH_WAITING; $ownerName = 'Waiting'; 
				// 		$owner = AH_ACTIVE; $ownerName = "Main Listing";
				// 	} else if ($bail && !($bail & TOOCHEAP)) {
				// 		$owner = AH_REJECTED; $ownerName = 'Rejected';
				// 	} else {
				// 		 $owner = AH_TOOCHEAP; $ownerName = "TooCheap";
				// 	} break;
			}
		}
		
		$this->record("entered createListing with author_has_account:".(!$author_has_account ? "no" : "yes")." for MLS(xml):".$info['listhub_key'].", price:".$info['price'].", author:".$info['author'].", owner will be: $ownerName, bail: $bail, preExisting - id: ".($this->preExisting != null ? $this->preExisting->id : "N/A").", MLS: ".($this->preExisting != null ? $this->preExisting->listhub_key : "N/A").", active:".($this->preExisting != null ? $this->activeToStr($this->preExisting->active) : "N/A").", minPrice:$this->minimumPrice\n", 3 );
		$x = $this->preExisting;
		
		if ($info['author_has_account'] != $author_has_account) {
			$this->log("createListing - author_has_account not the same, array has ".($info['author_has_account'] ? "1" : "0").", but seller->author_id is ".($seller->author_id ? $seller->author_id : "N/A"));
			$info['author_has_account'] = $author_has_account;
		}

		if ($x != null)
		{
			$info['active'] = $owner; // let's see if it needs updating..
			if (!isset($x->tier) ||
				$x->tier != 1)
				$info['tier'] = 1; // seen in xml stream
			if (!$this->checkSameAddress($info, $x))
			{
				$why = "Existing listing with db id: ".$x->id.", addr is $x->street_address, $x->city $x->state $x->zip, but the incoming  address is ".$info['street_address'].", ".$info['city']." ".$info['state']." ".$info['zip'];
				$this->listHubRecord[] = array('listhub_key' => $info['listhub_key'],
												'status' => "DUPLICATE",
												'id' => ($x->id ? $x->id : 0),
												'reason' => $why);
				$this->record($why."\n", 3);
				if ($info['author'] != $x->author) {
					$this->record("Author id has changed for this listing db id:".$x->id.", from ".$x->author." to ".$info['author']."\n", 3);
				}
			}
			$diff = array();

			// $this->matchCity($info);
			$this->record("createListing - Preexisting listing: $x->id has for address:$x->street_address, city_id: $x->city_id, for city:$x->city, with flags:$x->flags, incoming has city_id:".(isset($info['city_id']) ? $info['city_id'] : "N/A")." for {$info['city']}", 3);
			if ($this->medianCityId == 0 &&
				$this->medianId != 0 && 
				isset($info['city_id']) &&
				!empty($info['city_id']) &&
				 $info['city_id'] != -1) {// update it
				$this->getClass('MedianPrices')->set([(object)['where'=>['id'=>$this->medianId],
															   'fields'=>['city_id'=>$info['city_id']]]]);
				$this->record( 'createListing updated @1 median with new city_id:'.$info['city_id'], 3);
			}
			
			$meta = null;
			if ( !empty($x->meta) ) foreach($x->meta as $data) {
				if ($data->action == LISTING_MODIFIED_DATA) {
					$meta = $data;
					// break;
				}
			}
		
			foreach($x as $field=>$value)
			{
				if ( !empty($meta) &&
					  property_exists($meta, $field) &&
					  $meta->$field == 1 ) {
					if ($field == 'images' ||
						$field == 'video' ||
						$field == 'beds' ||
						$field == 'baths' ||
						$field == 'about' ||
						$field == 'lotsize_std' ||
						$field == 'lotsize' ||
						$field == 'interior' ||
						$field == 'interior_std' ||
						$field == 'street_address') {
						$this->record("Listing: {$x->id} has $field set to modified by seller, not updating it in parser\n", 3);
						continue;
					}
				}
				if ($field != 'id' &&
					$field != 'added' &&
					$field != 'updated' &&
					$field != 'listhub_key') {
						// $this->record( "Comparing ".$field, 3 );
						// if ($field == 'images')
						// 	$this->record("images - existing:".print_r($value, true).", incoming:".print_r($info['images'], true), 3);

						if (!isset($info[$field]))
							continue;

						if ( empty($info[$field]) && 
							($info[$field] === null || $info[$field] === undefined) )
							continue;
				
						if ( (is_array($value) || is_object($value)) && 
							 (is_array($info[$field]) || is_object($info[$field])) ) {
							$isDifferent = $this->array_isDifferent($value, $info[$field]);
							$this->record("createListing - testing compound element: $field and isDifferent:".($isDifferent ? 'yes' : 'no'), 3);
							if ($isDifferent)
								$diff[$field] = $info[$field];
							continue;
						}

						if ( (!empty($info[$field]) || $info[$field] === 0) && ($value == $info[$field]) )
							continue;


						if ( $field == 'lotsize' ) {
							if ($info['lotsize_std'] == 'ft'){
								$val1 = round(floatval($info[$field]));
								$val2 = round($value);
							}
							else {
								$val1 = round(floatval($info[$field]), 4);
								$val2 = round($value, 4);
							}
							if ($val1 == $val2)
								continue;
						}		

						if ($field == 'price')	
						{   // convert to decimal
							$val1 = round($price);
							$val2 = round($value);
							if ($val1 == $val2)
								continue;
						}		

						if ($field == 'beds') {
							$val1 = round(intval($info[$field]));
							$val2 = round($value);
							if (intval($info[$field]) == 0 &&
								$value > 0) // existing value must have been changed by admin or seller
								continue;
						} 

						if ($field == 'baths') {
							$val1 = number_format(floatval($info[$field]), 3);
							$val2 = number_format($value, 3);
							if (floatval($info[$field]) == 0 &&
								$value > 0) // existing value must have been changed by admin or seller
								continue;
							else // fix it out to three decimal places
								$info[$field] = $val1;
						} 

						// if ($field == 'images' ||
						// 	$field == 'videos')
						// {
						// 	$needUpdate = false;
						// 	if (count($info[$field]) != count($value)) {
						// 		$this->record( "This ".$field." has a new count: ".count($info[$field]).", was ".count($value)."\n", 4);
						// 		$needUpdate = true;
						// 	}
						// 	elseif ($field == 'images') {
						// 		foreach($value as $key=>$image) {
						// 			if (!property_exists($image, 'width') ||
						// 			    !property_exists($image, 'height')) {
						// 				$needUpdate = true;
						// 				break;
						// 			}
						// 		}
						// 	}

						// 	if ($needUpdate) {
						// 		$arObject = new \ArrayObject($info[$field]);
						// 		// create a copy of the array
						// 		$diff[$field] = $arObject->getArrayCopy();
						// 	}
						// 	//print_r($diff[$field]);
						// }
						// else 
						if ( ($field == 'city' ||
							   $field == 'state' || 
							   $field == 'country') &&
							 nonConformingLocation($info[$field]) ) // then ignore it so the city/state can be corrected by the geocoder.
							continue;
						else if (!empty($info[$field]) &&
								$info[$field] != $value )
						{
							$this->record( "This ".$field." has a new value: ".$info[$field].", was ".$value."\n", 4);
							$diff[$field] = $info[$field];
						}
						else if ($info[$field] === 0 || $info[$field] === false || $info[$field] === '0') {
							if ($value != undefined &&
								$info[$field] == $value)
								continue;
							$diff[$field] = $info[$field];
						}
						else {
							$this->record( "This field:".$field.", does not exist as a field in the incoming listing with MLS:".$info['listhub_key'].", so it is ignored.\n", 3);
							continue;
						}
				}
			} // end for() loop

			if (!property_exists($x, 'listhub_key') ||
				!isset($x->listhub_key)) {
				$x->listhub_key = $info['listhub_key'];
				$diff['listhub_key'] = $info['listhub_key'];
			}
			else if ($x->listhub_key != $info['listhub_key']) {
				$this->log("createListing - updating listhub_key from {$x->listhub_key} to {$info['listhub_key']}");
				$x->listhub_key = $info['listhub_key'];
				$diff['listhub_key'] = $info['listhub_key'];
			}

			if (!property_exists($x, 'error') ||
				!isset($x->error) ||
				$x->error != $info['error']) {
				$x->error = $info['error'];
				$diff['error'] = $info['error'];
			}

			$this->duplicates++;
			$isDup = true;
			if (count($diff) || $foundTagCount)
				$this->checkIfSellerUpdatedListing($seller, $owner, $info, $diff, $foundTagCount);

			if (count($diff)) {
				$this->preExisting->active = $owner; // may have changed
				if ( !(count($diff) == 1 &&
					   array_key_exists('tier', $diff)) ) {
					$msg =  "This listing, with id: $x->id, already exists in $ownerName table, with MLS:".$x->listhub_key.", will update now with ".count($diff)." new fields - ";
					$i = 0;
					foreach($diff as $field=>$value) {
						if ($i++ != 0)
							$msg .= ", ";
						$msg .= $field."-".$this->valToString($value);
					}
					$this->record($msg."\n", 4 );
				}
				$q->fields = $diff;
				$q->where = array('id'=>$x->id);
				$a = array();
				$a[] = $q;

				try {
					$this->lock();
					$out = $listings->set($a);
					unset($out, $q->fields);
					$x = $listings->get($q); // get lateset
					$this->unlock();
					$msg =  "This listing, with id: {$x[0]->id}, updated as $ownerName table, with MLS:".$x[0]->listhub_key.", author_has_account:".($x[0]->author_has_account ? "1" : "0");
					$this->log($msg);
					$this->checkSellerEmailDb($x[0], $seller);
					// $x[0]->zip = str_pad( number_format($x[0]->zip), )
					$x = array_pop($x);
					if (strpos($x->zip,'-')) {
						$parts = explode('-', $x->zip);
						if (strlen($parts[0]) != 5)
							$parts[0] = str_pad($parts[0], 5, '0', STR_PAD_LEFT);
						$x->zip = implode('-', $parts);
					}
					elseif (strlen($x->zip)!= 5)
						$x->zip = str_pad($x->zip, 5, '0', STR_PAD_LEFT);
					$this->preExisting = $x;
					return $x;	
				}
				catch (\Exception $e){ $this->parseException($e); return $x;}
			}
			else
				$this->record( "This listing already exists in $ownerName table, with MLS:".$x->listhub_key.", with identical data, id: ".$x->id."\n", 3);

			$this->checkSellerEmailDb($x, $seller);
			return $x;	
		}
		try{
			$this->assignCategory($owner, $ownerName, $price, $bail);
			$info['active'] = $owner;
			$info['baths'] = number_format(floatval($info['baths']), 3);
			$info['tier'] = 1; // seen in xml stream
			// if (array_key_exists ( $info['state'] , $usStates ))
			// 	$info['state'] = $usStates[$info['state']];

			// $this->matchCity($info);
			$this->record("createListing - new incoming has city_id:".(isset($info['city_id']) ? $info['city_id'] : "N/A")." for {$info['city']}", 3);
			if ($this->medianCityId == 0 &&
				$this->medianId != 0 && 
				isset($info['city_id']) &&
				!empty($info['city_id']) &&
				 $info['city_id'] != -1) {// update it
				$this->lock();
				$this->getClass('MedianPrices')->set([(object)['where'=>['id'=>$this->medianId],
															   'fields'=>['city_id'=>$info['city_id']]]]);
				$this->unlock();
				$this->record( 'createListing updated @2 median with new city_id:'.$info['city_id'], 3);
			}

			if (isset($info['images']) && !empty($info['images'])) foreach($info['images'] as $img) {
				$img = (object)$img;
				if (isset($img->file)) {
					$info['first_image'] = $img;
					break;
				}
			}

			$this->lock();
			try {
				$r = $listings->add((object)$info);
				$this->record( 'createListing- new listing at row:'.$r.", with author_has_account:".($info['author_has_account'] ? "1" : "0")." for MLS(xml):".$info['listhub_key'].", price:".$info['price'].", city_id:".$info['city_id']."\n", 3);
				$q->where = array('listhub_key'=>$info['listhub_key'],							
								  'id' => $r);
				$x = $listings->get($q); 
			}
			catch(\Exception $e) {
				$this->record("createListing() caught exception:".$e->getMessage(), 3, self::ERROR);
				$x = false;
			}
			$this->unlock();
			if(!empty($x))
			{
				$x = array_pop($x);
				if ($bail == 0 ||
			 		$bail == LACK_DESC) // no error
					$this->addedToDb++;
				$msg = "Created a new listing in $ownerName table with MLS:".$x->listhub_key.", id:".$x->id.", with author_has_account:".($x->author_has_account ? "1" : "0");
				$this->record( $msg. "\n", 3 );
				$this->log($msg);
				$this->checkSellerEmailDb($x, $seller);
				if (strpos($x->zip,'-')) {
					$parts = explode('-', $x->zip);
					if (strlen($parts[0]) != 5)
						$parts[0] = str_pad($parts[0], 5, '0', STR_PAD_LEFT);
					$x->zip = implode('-', $parts);
				}
				elseif (strlen($x->zip)!= 5)
					$x->zip = str_pad($x->zip, 5, '0', STR_PAD_LEFT);

				return $x;	
			}
			$this->record( "Failed to retrieve a new listing from $ownerName table, but created on row $r, with MLS:".$info['listhub_key']."\n", 3 );
			return false;
		}
		catch (\Exception $e){ $this->parseException($e); return false;}
	}

	protected function valToString($value)
	{
		$val = 'NULL';
		switch(getType($value)) {
			case "boolean": $val = $value ? "true" : "false"; break;
			case "integer": $val = number_format($value); break;
			case "double" : $val = number_format($value, 4); break;
			case "string" : $val = $value; break;
			case "array"  : $val = "array"; break;
			case "object" : $val = "object"; break;
			case "resource": $val = "resource"; break;
			case "NULL"   : $val = "NULL"; break;
			default: $val = "unknown"; break;
		}
		return $val;
	}
											    // stdClass, int,   array,    array  int
	protected function checkIfSellerUpdatedListing($seller, $owner, $listing, $diff, $foundTagCount) {
		if ($owner == AH_ACTIVE) // from Main Listing
			return; // no need

		if ($seller->meta == null) // nothing to do really
			return;

		$tagCountExisting = 0;
		if ($this->preExisting != null) {
			$tags = $this->getClass('ListingsTags');
			$q = new \stdClass();
			$q->where = array('listing_id'=>$this->preExisting->id);
			$x = $tags->get($q); 
			$tagCountExisting = count($x);
		}
		$hasUpdate = false;
		$meta = array();

		foreach($eller->meta as $field=>$value) {
			if (is_object($value) &&
				property_exists($value, 'listhub_key') &&
				$value->listhub_key == $listing['listhub_key'] &&
				property_exists($value, 'action') &&
				$value->action ==  SELLER_IMPROVEMENT_TASKS &&
				!property_exists($value, 'completed')) { // then we have a listing that the seller was informed to update and yet completed
				// create array to hold updated keys
				$updated = array();

				// treat tags separately, since it won't be in the diff array
				// NOTE: check for tags is now deferred to seller's edit listings.
				// TODO: added to to seller's edit listings to remove meta->tags properly if tags are edited.
				// if ($foundTagCount > $tagCountExisting && // then there are now more tags than before
				// 	property_exists($value, 'tags') ) {   // and the seller has been marked to update the tags
				// 	$updated[] = 'tags';
				// 	unset($value->tags); // get rid of this one
				// 	$hasUpdate = true;
				// }

				foreach($diff as $key=>$data) {
					if (property_exists($value, $key) &&
						$value->$key != $data) { // then seller changed something in the incoming listing, let's hope it for the good.
						$updated[] = $key;
						//$value->$key = 0;
						unset($value->$key); // get rid of this one
						$hasUpdate = true;
					}
				}
				$props = get_object_vars($value);
				$msg = "Seller with id:".$seller->id;
				if (count($updated))
					$msg .= " has updated ".count($updated)." items for ";
				else 
					$msg .= " still has ".count($props)." items to update for ";
				$msg .= "listing with MLS:".$value->listhub_key.".  Task status: ";
				$i = 0;
				foreach($value as $key=>$data) {
					if ($i)
						$msg .= ", ";
					$msg .= $key.":".$data;
					$i++;
				}
				$this->record($msg."\n", 3);
				unset($updated); 
				// if there is only one key the listhub_key
				if ($hasUpdate && count($props) == 1)
					$value->completed = 'true';

				$meta[] = $value;
			}	
			else {
				$meta[] = $value; // gather up the ones that don't match
			}
		}
		if ($hasUpdate) // write it back to the seller
		{
			$q = new \stdClass();
			$q->fields = array('meta'=>$meta);
			$q->where = $listing['author_has_account'] ? array('author_id'=>$listing['author']) : array('id'=>$listing['author']); // depends on whether the author has an account;
			$a = array();
			$a[] = $q;
			try {
				$this->getClass('Sellers')->set($a);
			}
			catch(\Exception $e) {
				parseException($e);
			}
			unset($a, $q, $meta);
		}
	}

	protected function createActivity(&$info)
	{
		if (empty($info->listing_id) && ($info->listing_id === 0 || $info->listing_id == null))
		{
			$this->record("createActivity got an empty listing_id!\n", 3);
			return;
		}
		$activity = $this->getClass("ListingsActivity");
		$q = new \stdClass(); 
		$q->where = array('listing_id'=>$info->listing_id);
		$x = $activity->get($q); 
		unset($q);

		// new update
		$msg = new \stdClass();
		$msg->action = "existence check";
		$msg->date = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		$msg->author = $info->author;
		$msg->source = 'listhub';

		$myArray = array();

		if(!empty($x))
		{
			$x = array_pop($x);
			$this->record( "This activity already exists, with listing id:".$info->listing_id."\n",3 );
			$q = new \stdClass();
			if (!empty($x->data))
				foreach($x->data as $field=>$value) {
					if (is_object($value) &&
						property_exists($value, 'action') &&
						$value->action != "existence check") // ignore any "existance checks"
						$myArray[] = $value; // preserve existing data
					elseif ($field == 'edit') // old style
						$info->data[] = array($field=>$value);
					elseif ( ($field == 'query' ||
							  $field == 'Query') &&
							strpos($value, "Check for existance") !== false) // ignore old style "Check for existance"
						continue;
				}

			$myArray[] = $msg;
			$q->fields = array('data'=>$myArray);
			$q->where = array('listing_id'=>$info->listing_id);
			$a = array();
			$a[] = $q;
			try {
				$out = $activity->set($a);
				$this->record( "createActivity found existing activity, updated with: ".$msg->action."\n", 3);
				// unset($out);
				return $x;	
			}
			catch (\Exception $e){ $this->parseException($e); return $x;}
		}
		try{
			$msg->action = 'add';
			$myArray[] = $msg;
			$info->data = $myArray;
			$x = $activity->add((object)$info);
			$this->record( 'createActivity- new activity at row:'.$x."\n");
			return $x;
		}
		catch (\Exception $e){ $this->parseException($e); return false;}
	}

	protected function hasGeoCodeAlready(&$geoCode) {
		$geoCode = null;
		if (!$this->preExisting)
			return false;

		$geo = $this->getClass("ListingsGeoinfo");
		$q = new \stdClass(); 
		$q->where = array('listing_id'=>$this->preExisting->id);
		$x = $geo->get($q);
		if (!empty($x)) $geoCode = $x[0];
		return empty($x) ? false : ($x[0]->lng == -1 && $this->forceRedoGeoCodeListing ? false : true);
	}

	protected function getGeoCode(&$info)
	{
		$geo = $this->getClass("GoogleLocation");
		try{
			$x = $geo->geocodeLatLng($info, 1);
			return $x;
		}
		catch (\Exception $e){ $this->parseException($e); return false;}
	}

	protected function createGeoCode(&$info)
	{
		$geo = $this->getClass("ListingsGeoinfo");
		$q = new \stdClass(); 
		$q->where = array('listing_id'=>$info->listing_id);
		$this->lock();
		$x = $geo->get($q); 
		$this->unlock();
		unset($q);
		if(empty($x))
		{
			$this->lock();
			try{
				$x = $geo->add((object)$info);
				$this->record( 'createGeoCode - new activity at row:'.$x."\n", 3);
				return $x;
			}
			catch (\Exception $e){ 
				$this->parseException($e); 
				$this->unlock();
				return false;
			}
			$this->unlock();
		}
		else
		{
			$this->record( "This GeoInfo already exists, with listing id:".$info->listing_id."\n", 3 );		
			$curGeo = array_pop($x);
			$fields = [];
			if ($curGeo->lat == -1 &&
				$info->lat != -1) {
				$fields['lat'] = $info->lat;
				$fields['lng'] = $info->lng;
			}
			if ($curGeo->address != $info->address)
				$fields['address'] = $info->address;

			if (count($fields)) {
				$this->lock();
				$geo->set([(object)['where'=>['id'=>$curGeo->id],
								    'fields'=>$fields]]);
				$this->unlock();
				$this->record( "Updated GeoInfo exiting one, for listing id:".$info->listing_id.", with lng:$info->lng, lat:$info->lat\n", 3 );	
			}
			unset($curGeo);
			return true;
		}	
	}

	protected function mergeCityTags(&$tags, $listing) {
		return;

		$key = ucwords(strtoupper($listing['city'])).'-'.strtoupper($listing['state']);
		$this->record("mergeCityTags = for $key", 3);
		if (array_key_exists($key, $this->cityTags)) {
			$this->record("mergeCityTags - found $key, tags:".print_r($this->cityTags[$key], true), 3);
			foreach($this->cityTags[$key] as $tag=>$value) {
				if (!array_key_exists($tag, $tags) &&
					$value->score >= 7) {
					$tags[$tag] = $value->id;
					$this->record("mergeCityTags adding $tag to {$listing['listhub_key']}", 3);
				}
			}
		}
	}

	// $newTags has the merged city tags now
	protected function updateTags($newTags, 
								  $listing_id)
	{
		$oldTags = [];	
		$t = $this->getClass('Tags');
		$tagTable = getTableName($t->table);
		$tags = $this->getClass("ListingsTags");
		$listingsTagsTable = getTableName($tags->table);
		$q = new \stdClass(); 
		$q->where = array('listing_id'=>$listing_id);
		$sql = 'SELECT a.tag, b.tag_id FROM `'.$tagTable.'` AS a ';
		$sql .= 'INNER JOIN '.$listingsTagsTable.' AS b ON a.id = b.tag_id WHERE b.listing_id = '.$listing_id;
		$x = $t->rawQuery($sql);
		$newCount = count($newTags);
		if ($newCount) 
			foreach($newTags as &$tag) 
                $tag = intval($tag);

        $this->record("updateTags - newCount:$newCount, existing:".count($x), 3);

		if(!empty($x))
		{
			$curCount = count($x);
			$y = [];
			foreach($x as $tag) {
				$y[strtolower($tag->tag)] = intval($tag->tag_id);
				unset($tag);
			}
			unset($x);
			$x = $y;

			$isSame = true;
			if ($curCount == $newCount) {
               	asort($newTags);
               	asort($x);
               	// usort($x, function($a,$b) {
                //     return $a->tag_id - $b->tag_id;
               	// });

               	$index = 0;
               	reset($newTags);
               	reset($x);
               	foreach($newTags as $tag=>$tag_id) {
                    // if ($x[$index]->tag_id != $tag_id) {
               		if ( !array_key_exists($tag, $x) ) {
                       	// $this->record ("Listing: $listing_id - tag is different at $index, existing:".$x[$index]->tag_id.", new:$tag_id\n", 3);
                       	$this->record ("Listing: $listing_id - new tag:$tag doesn't exist in existing ListingsTags at index:$index\n", 3);
                       	$this->record ("Listing: $listing_id - existing:".print_r($x, true)."\n", 2);
                       	$this->record ("Listing: $listing_id - new:".print_r($newTags, true)."\n", 2);
                       	$isSame = false; break;
                    }
                    $index++;
               }
           	}
           	else
           		$isSame = false;

           	if ($isSame)
           		return;
		}

		if(!empty($x)) {
			// make a simple array version
			$haveCondo = false;
			foreach($newTags as $tag=>$id)
				if ($tag == 'condo') {
					$haveCondo = true;
					break;
				}

			if ($haveCondo &&
				array_key_exists('house', $newTags)) {
				$this->record ("Listing: $listing_id had both 'house' and 'condo' new tags, removed 'house'\n", 3);
				unset($newTags['house']);
			}

			
			// go thru existing tags
			foreach($x as $tag=>$id) {
				if ($haveCondo &&
					$tag == 'house') {
					$this->record ("Listing: $listing_id has an existing 'house' tag and is being replaced by 'condo' tag\n", 3);
					$oldTags[$tag] = $id;
					continue;  // strip out the house tag if incoming tags has 'condo'
				}
				elseif (!$haveCondo &&
					$tag == 'condo') {
					$this->record ("Listing: $listing_id has an existing 'condo' tag but new parse has no condo\n", 3);
					$oldTags[$tag] = $id;
					continue;  // strip out the condo tag if incoming tags doesn't have 'condo'
				}
				if (array_key_exists( $tag, $newTags) )
					unset($newTags[$tag]); // remove pre-existing ones
				else {
					$this->record ("updateTags = Listing: $listing_id - remove $tag, $id", 3);
					$oldTags[$tag] = $id; // must have been removed by author or city was retagged
				}
			}
			// $sendTags = array_merge($oldTags, $newTags);
			$sendTags = $newTags;
		}
		else
			$sendTags = $newTags;

		
		if (!empty($sendTags)) foreach($sendTags as $tag=>$value)
		{
			if ( empty($value) ||
				!is_numeric($value))
				continue;

			$data= array('tag_id'=>$value,
						 'listing_id' => $listing_id,
						 'clicks' => 0,
						 'flags' => LISTING_TAG_FROM_PARSE);
			try {
				$retval = $tags->add($data);
			}
			catch(\Exception $e) {
				parseException($e);
			}
			unset($data);
		}

		if (!empty($oldTags)) foreach($oldTags as $value) {
			$tags->delete((object)['tag_id'=>$value,
								   'listing_id' => $listing_id]);
		}

		$this->record("Updated listing: $listing_id with ".count($sendTags)." tags, removed ".count($oldTags)." tags.\n", 3);
	}

	public function cleanTags($active) {
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;
		$result = array('active'=>$active);
		$this->updateProgress(json_encode($result), self::UNTAG_CMD_RECEIVED);
		$this->saveLogToDb = $oldFlag;

		$l = $this->getClass('Listings');
		$q = new \stdClass();
		//$q->what = array('id', 'images');
		$q->where = array('active'=>$active);
		//$x = $l->get($q);
		$x = $l->count($q);
		$exitNow = false;
		$terminal_msg = '';
		$reason = self::UNTAG_START;
		if (empty($x)) {
			$exitNow = true;
			$terminal_msg = "No listings with active mode:".$this->activeToStr($active);
			$reason = self::UNTAG_ERROR;
		}
		else
			$terminal_msg = "Going to process ".$x." listings in active mode:".$this->activeToStr($active);

		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;
		if ($reason == self::UNTAG_START) 
			$result = array('active'=>$active,
							'listingCount'=>$x,
							'msg'=>$terminal_msg);
		else // must be UNTAG_ERROR
			$result = array('active'=>$active,
							'msg'=>$terminal_msg);
		$this->updateProgress(json_encode($result), $reason);
		$this->saveLogToDb = $oldFlag;

		if ($exitNow) 
			return new Out('fail', $terminal_msg);

		$perPage = 1000;
		$pages = $x/$perPage;
		$pages += ($x % $perPage) ? 1 : 0;
		$page = 0;
		$q->what = array('id', 'about');
		$q->order_by = 'ASC'; // AH_ACTIVE, AH_REJECTED, etc..
	    $q->limit = $perPage;
	    $tags = $this->getClass("ListingsTags");
	    $t = new \stdClass();
	    $remCount = 0;
		while ($page < $pages) {
		    $q->page = $page; // 0-based
		    $page++; // ready for next page
			$x = $l->get($q);
			if (empty($x))
				break;
			$count = count($x);
			$reason = self::UNTAG_GROUP;
			$oldFlag = $this->saveLogToDb;
			$this->saveLogToDb = true;
			$result = array('page'=>$page,
							'start'=>$x[0]->id,
							'end'=>$x[$count-1]->id,
							'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)),
							'msg'=>"Starting page $page with ".count($x)." listings");
			$this->updateProgress(json_encode($result), $reason);
			$this->saveLogToDb = $oldFlag;

			foreach($x as $listing) {
				$t->where = array('listing_id'=>$listing->id);
				$tagList = $tags->get($t);
				$badTags = array();
				if (!empty($tagList))
					$this->getClass('ListHubMatchTags')->cleanTags($listing->about, $tagList, $badTags);
				if (count($badTags)) {
					$rem = new \stdClass();
					foreach($badTags as $bad) {
						$rem->where = array('tag_id'=>$bad,
											'listing_id'=>$listing->id);
						$tags->delete($rem->where);
						unset($rem->where);
					}
					unset($rem);
					$remCount++;
					$oldFlag = $this->saveLogToDb;
					$this->saveLogToDb = true;
					$array = str_replace("\n "," ", print_r($badTags, true));
					$result = array('id'=>$listing->id,
									'tags'=>$array,
									'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)));
					$this->updateProgress(json_encode($result), self::UNTAG_MATCHED);
					$this->saveLogToDb = $oldFlag;
				}
				unset($badTags);
			} // foreach
		} // while
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;
		$result = array('count'=>$remCount,
						'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)));
		$this->updateProgress(json_encode($result), self::UNTAG_END);
		$this->saveLogToDb = $oldFlag;

		return new Out('OK', $remCount);
	}

	public function applyKeywords($keywords, $active) {
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;
		$result = array('active'=>$active,
						'keywords'=>$keywords);
		$this->updateProgress(json_encode($result), self::KEYWORD_CMD_RECEIVED);
		$this->saveLogToDb = $oldFlag;

		$l = $this->getClass('Listings');
		$q = new \stdClass();
		//$q->what = array('id', 'images');
		$q->where = array('active'=>$active);
		//$x = $l->get($q);
		$x = $l->count($q);
		$exitNow = false;
		$terminal_msg = '';
		$reason = self::KEYWORD_START;
		if (empty($x)) {
			$exitNow = true;
			$terminal_msg = "No listings with active mode: $active to keyword on";
			$reason = self::KEYWORD_ERROR;
		}
		else
			$terminal_msg = "Going to process ".$x." listings in active mode:".$this->activeToStr($active)." for keywords";

		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;
		if ($reason == self::KEYWORD_START) 
			$result = array('active'=>$active,
							'listingCount'=>$x,
							'msg'=>$terminal_msg);
		else // must be IMAGE_BORDER_ERROR
			$result = array('active'=>$active,
							'msg'=>$terminal_msg);
		$this->updateProgress(json_encode($result), $reason);
		$this->saveLogToDb = $oldFlag;

		if ($exitNow) 
			return new Out('fail', $terminal_msg);

		$sql = "UPDATE ".getTableName($l->table)." SET `key_match` = 0 WHERE `active` = $active AND (`key_match` != 0 OR `key_match` IS NULL)";
		$numReset = $l->rawQuery($sql);
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;
		$result = array('active'=>$active,
						'numReset'=>$numReset);
		$this->updateProgress(json_encode($result), self::KEYWORD_RESET_COUNT);
		$this->saveLogToDb = $oldFlag;

		$perPage = 1000;
		$pages = $x/$perPage;
		$pages += ($x % $perPage) ? 1 : 0;
		$page = 0;
		$q->what = array('id', 'about', 'key_match');
		$q->order_by = 'ASC'; // AH_ACTIVE, AH_REJECTED, etc..
	    $q->limit = $perPage;
	    $matchCount = 0;
		while ($page < $pages) {
		    $q->page = $page; // 0-based
		    $page++; // ready for next page
			$x = $l->get($q);
			if (empty($x))
				break;
			$count = count($x);
			$reason = self::KEYWORD_GROUP;
			$oldFlag = $this->saveLogToDb;
			$this->saveLogToDb = true;
			$result = array('page'=>$page,
							'start'=>$x[0]->id,
							'end'=>$x[$count-1]->id,
							'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)),
							'msg'=>"Starting page $page with ".count($x)." listings");
			$this->updateProgress(json_encode($result), $reason);
			$this->saveLogToDb = $oldFlag;

			foreach($x as $listing) {
				$tmp = strtolower( $listing->about );
				foreach($keywords as $key) {
					if (strpos($tmp, $key) !== false) {
						$listing->key_match = 1;
						$matchCount++;
						break;
					}
				}
				if ($listing->key_match) {
					$q2 = new \stdClass();
					$q2->fields = array('key_match' => 1);
					$q2->where = array('id'=>$listing->id);
					$a = array();
					$a[] = $q2;
					try {
						$ret = $l->set($a);
					}
					catch(\Exception $e) {
						parseException($e);
					}
					unset($q2, $a);
					if ($ret) {
						$reason = self::KEYWORD_MATCHED;
						$msg = "Updated key_match for listing:".$listing->id;
					}
					else {
						$reason = self::KEYWORD_ERROR;
						$msg = "Failed to update key_match for listing:".$listing->id;
					}
					$oldFlag = $this->saveLogToDb;
					$this->saveLogToDb = true;
					$result = array('id'=>$listing->id,
									'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)),
									'msg'=>$msg);
					$this->updateProgress(json_encode($result), $reason);
					$this->saveLogToDb = $oldFlag;
				}
			} // outer foreach()
		} // while(paages)
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;
		$result = array('count'=>$matchCount,
						'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)));
		$this->updateProgress(json_encode($result), self::KEYWORD_END);
		$this->saveLogToDb = $oldFlag;

		return new Out('OK', $matchCount);
	}

	public function removeListings($start, $end, $preserveActive) {
		$q = new \stdClass();
		$q->between = ['id'=>[$start, $end]];
		$q->where = ['listhub_key'=>'notnull'];
		if ($preserveActive)
			$q->not = ['active'=> 1];
		$listings = $this->getClass('Listings')->get($q);
		if (empty($listings)) {
			return new Out('fail', "No listings found with id between $start and $end");
		}

		$this->doingParallel = true; // so it appends, not overwrite
		$this->openDebugFile(1);
		$this->beginOutputBuffering();

		$this->record("removing ".count($listings)." listings\n", 3);

		$Listings = $this->getClass("Listings");
		$Image = $this->getClass('Image');
		foreach($listings as $data)
			$this->removeOneListing($data, $Image, $Listings);

		$this->stopOutputBuffering();

		return new Out('OK', "Removal of listings between $start and $end completed");

	}

	public function getActiveListingCount($mode, $fromLast, $fromCron, $specialOp) {
		$msg = "getActiveListingCount entered for mode:$mode, fromLast:$fromLast, fromCron:$formCron, specialOp:$specialOp";
		$this->updateToDb($msg, self::UPDATE);

		$Listings = $this->getClass('Listings');
		$msg = "getActiveListingCount Listings:".(empty($Listings) ? "is empty" : "got one");
		$this->updateToDb($msg, self::UPDATE);

		$q = new \stdClass();
		//$q->what = array('id', 'images');
		$q->where = array('active'=>$mode);
		$lastId = 0;
		if ($fromLast) {
			$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'LastImageBorderId']]);
			if (!empty($opt))
			{
				$lastId = $opt[0]->value;
				if (!empty($lastId))
					$q->greaterthanequal = array('id' => $lastId);
			}
		}
		//$x = $l->get($q);
		$msg = "getActiveListingCount before Listings->count with ".print_r($q, true);
		$this->updateToDb($msg, self::UPDATE);
		// $x = $Listings->get($q);
		$x = $Listings->count($q);
		$q->what = ['id'];
		if (empty($x)) {
			$msg = "Failed to get any listings for mode:$mode, fromLast:$fromLast, lastId:$lastId";
			$this->updateToDb($msg, self::IMAGE_BORDER_FAILED_GET_COuNT);
			return new Out('fail', $msg);
		}
		else {
			$doingPP = false;
			$opt = $this->getClass('Options')->get((object)['like'=>['opt'=>'active']]);
			foreach($opt as $act)
				if ( $act->opt == 'activePP')
					$doingPP = true;
				
			if (!$doingPP &&
				 empty($fromCron) &&
				 $this->okToEmptyProgressTableBeforeIP)
				$this->getClass("ListhubProgress")->emptyTable(true);
			$imageBlocksTarget = $specialOp != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 'ImageBlocks' : "CustomImageBlocks";
			$imageBlockClass = $this->getClass($imageBlocksTarget);
			$imageBlockClass->emptyTable(true);
			// $msg = "getActiveListingCount ready with ".count($x)." listings";
			$msg = "getActiveListingCount ready with ".$x." listings";
			$this->updateToDb($msg, self::UPDATE);
			// $total = count($x);
			$total = $x;
			// $chunk = array_chunk($x, 1000);
			$this->createDebugFile();
			$this->beginOutputBuffering();

			$pagePer = 1000;
			$page = 0;
			$q->limit = $pagePer;
			$q->page = $page;
			while( !empty($block = $Listings->get($q)) ) {
			// foreach($chunk as $block) {
				$msg = "getActiveListingCount ready for page:$page with ".count($block)." listings";
				$this->record($msg, 3);
				$list = '';
				$first = true;
				foreach($block as $id) {
					$list .= ($first ? '' : ',');
					$list .= "('".$id->id."')";
					$first = false;
				}
				$sql = "INSERT INTO {$imageBlockClass->getTableName()} (`data`) VALUES $list";
				$x = $imageBlockClass->rawQuery($sql);
				$this->record("Saved $x ids", 3);
				unset($block, $list);
				$q->page = ++$page;
			}

			$this->stopOutputBuffering();
			return new Out('OK', $total);
		}
	}

	public function fixImageBorders($mode, $op, $onlyNew, $fromLast) {
		// $fromLast = 1;
		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;
		$result = array('mode'=>$mode,
						'op'=>$op,
						'onlynew'=>$onlyNew,
						'fromLast'=>$fromLast);
		$this->updateProgress(json_encode($result), self::IMAGE_BORDER_CMD_RECEIVED);
		$this->saveLogToDb = $oldFlag;

		$l = $this->getClass('Listings');
		$q = new \stdClass();
		//$q->what = array('id', 'images');
		$q->where = array('active'=>$mode);
		$lastId = 0;
		if ($fromLast) {
			$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'LastImageBorderId']]);
			if (!empty($opt))
			{
				$lastId = $opt[0]->value;
				if (!empty($lastId))
					$q->greaterthanequal = array('id' => $lastId);
			}
		}

		$imageUploadDir = 'uploaded';
		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'ImageUploadDir']]);
		if (!empty($opt))
			$imageUploadDir = $opt[0]->value;
		
		//$x = $l->get($q);
		$x = $l->count($q);
		$exitNow = false;
		$terminal_msg = '';
		$onlyNew = $onlyNew == 'true' || $onlyNew == '1' ? 1 : 0;
		$reason = self::IMAGE_BORDER_START;
		if (empty($x)) {
			$exitNow = true;
			$terminal_msg = "No listings with active mode: $mode, onlyNew: $onlyNew";
			$reason = self::IMAGE_BORDER_ERROR;
		}
		else
			$terminal_msg = "Going to process ".$x." listings in active mode: $mode, onlyNew: $onlyNew";

		$this->exitNow($terminal_msg);
		
		$this->haveOutputFile = false;

		$oldFlag = $this->saveLogToDb;
		$this->saveLogToDb = true;
		if ($reason == self::IMAGE_BORDER_START) 
			$result = array('mode'=>$mode,
							'listingCount'=>$x,
							'msg'=>$terminal_msg);
		else // must be IMAGE_BORDER_ERROR
			$result = array('mode'=>$mode,
							'msg'=>$terminal_msg);
		$this->updateProgress(json_encode($result), $reason);
		$this->saveLogToDb = $oldFlag;

		if ($exitNow) 
			return false;

		$perPage = 1000;
		$pages = $x/$perPage;
		$pages += ($x % $perPage) ? 1 : 0;
		$page = 0;
		$q->what = array('id', 'images', 'first_image');
		$q->order_by = 'ASC'; // AH_ACTIVE, AH_REJECTED, etc..
	    $q->limit = $perPage;
		$path = realpath(__DIR__."/../_img/_listings/$imageUploadDir");

		while ($page < $pages) {
		    $q->page = $page; // 0-based
		    $page++; // ready for next page
			$x = $l->get($q);
			if (empty($x))
				break;
			$count = count($x);
			$reason = self::IMAGE_BORDER_GROUP;
			$oldFlag = $this->saveLogToDb;
			$this->saveLogToDb = true;
			$result = array('page'=>$page,
							'start'=>$x[0]->id,
							'end'=>$x[$count-1]->id,
							'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)),
							'msg'=>"Starting page $page with ".count($x)." listings");
			$this->updateProgress(json_encode($result), $reason);
			$this->saveLogToDb = $oldFlag;

			foreach($x as $listing) {
				if (!empty($listing->listhub_key))
					try {
						$this->fixOneImageBorder($listing, $op, $path, $onlyNew, $lastId);
					}
					catch( \Exception $e) {
						parseException($e);
					}
			} // for
			unset($x);
			@popen("rm /tmp/alr*.img /tmp/alr??????", 'r');
		}// while
		
		if ($this->getClass('Options')->exists(['opt'=>'LastImageBorderId'])) {
			$x = $this->getClass('Options')->set([(object)['where'=>['opt'=>'LastImageBorderId'],
													  	   'fields'=>['value'=>$lastId]]]);
			$msg = array('msg'=>'Updated Options for LastImageBorderId',
						'setResult'=>$x);
			$this->updateToDb(json_encode($msg), self::IMAGE_BORDER_UPDATE);
		}
		else {
			$x = $this->getClass('Options')->add((object)['opendir(path)t'=>'LastImageBorderId',
											 		 	  'value'=>$lastId]);
			$msg = array('msg'=>'Added Options for LastImageBorderId',
						'addResult'=>$x);
			$this->updateToDb(json_encode($msg), self::IMAGE_BORDER_UPDATE);
		}
		$reason = self::IMAGE_BORDER_END;
		$result = array('mode'=>$mode,
						'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)),
						'msg'=>"Completed image processing");
		$this->updateToDb(json_encode($result), $reason);	
	}

	protected function getBaseImageName($name) {
		$file_info = pathinfo($name);

		if (count($file_info) <= 1) {
			$msg = "getBaseImageName - for $name, got:".print_r($file_info, true);
			$this->updateToDb($msg, self::IMAGE_BORDER_GETBASENAME_ERROR, 3, true);
		}

		$base = $file_info['filename'];
		$base = preg_replace('/[^a-zA-Z0-9]/', '_', $base);

		$dir = $file_info['dirname'];
		$dir = explode("/", $dir);
		$prefix = '';
		$used = 0;
		if (count($dir) > 2) {
			for($i = count($dir)-1; $i > 0 && $used < 2; $i--) {
				if (!empty($dir[$i]) &&
					substr($dir[$i], 0, 4) != 'http') {
					$used++;
					$prefix .= preg_replace('/[^a-zA-Z0-9]/', '_', $dir[$i]).'_';
				}
			}
		}
		return $prefix.$base;
	}

	public function processBorder($in) {
		if (empty($in))
			return new Out('fail', "No files sent to processBorder");

		$path = __DIR__."/../test";
		$imageClass = $this->getClass('Image');

		foreach($in as $file) {
			$name = $file['tmp_name'];
			$img = null;
			$top = 0; $bottom = 0;
			// $left = 0; $right = 0;
			// $dest = '';

			if ($imageClass->hasWhiteBorder($name, $img, $name, $top, $bottom)) {
				$dest = $path."/".$this->getBaseImageName($name).".jpeg";
				$cropped = $imageClass->trimTopBottom($img, $top, $bottom);
				imagedestroy($img);
				$img = $cropped;
				if (imagejpeg($img, $dest, 95)) {
					imagedestroy($img);
					$this->log("processBorder - trimTopBottom, top:$top, bottom:$bottom, saved to $dest");
					$name = $dest; // run it through side borders
				}
				else
					return new Out('fail', "did not save to $dest after trimTopBottom");
			}

			if ($imageClass->hasWhiteBorderSides($name, $img, $name, $left, $right)) {
				$dest = empty($dest) ? $path."/".$this->getBaseImageName($name).".jpeg" : $dest;
				$cropped = $imageClass->trimLeftRight($img, $left, $right);
				imagedestroy($img);
				$img = $cropped;
				if (imagejpeg($img, $dest, 95)) {
					imagedestroy($img);
					return new Out('OK', "file saved to $dest after trimLeftRight, left:$left, rigth:$right");
				}
				else
					return new Out('fail', "did not save to $dest after trimLeftRight");
			}
		}
		return new Out('fail', "why am I here!?");
	}

	protected function readImage($url, $tries = 1, $useCurl = 0, $useReadFile = 0) {
		$timeout = 240; // in secs
		//$tempFile = tempnam(__DIR__.'/../uploads/', 'alr').'gz';
		$tempFile = tempnam("/tmp", 'alr').'.img';

		
		if ($useCurl) {
			$sizeUp = (int)0;
			$soFarUp = (int)0;
			set_time_limit($timeout); // set time out max value
			$startTime = microtime();
			$tmpFile = fopen($tempFile, "wb");

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1); 
			// curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			// curl_setopt($ch, CURLINFO_HEADER_OUT, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); //timeout in seconds
			curl_setopt($ch, CURLOPT_FILE, $tmpFile);

			if ( !($output = curl_exec($ch)) ) {
				$errno = curl_errno($ch);
				throw new \Exception(curl_error($ch), $errno);
			}
			$info = curl_getinfo($ch);
			curl_close($ch);
			set_time_limit(0);
			fclose($tmpFile);
		}
		elseif ($useReadFile) {
			$tmpFile = fopen($tempFile, "wb");
			$imgInfo = getimagesize($url);
			// header("Content-type: {$imgInfo['mime']}");
			// ob_start();
			// $info = $size = readfile($url);
			// if ($size == ob_get_length()) {
			// 	fwrite($tmpFile, ob_get_flush());
			// 	$info = [];
			// 	ob_end_clean();
			// }
			// else {
			// 	ob_end_clean();
			// 	$this->record("image size expected is $size, but ".ob_get_length()." is in the buffer", 3);
			// }
			// $handle = fopen($url, "rb");
			header("Content-type: {$imgInfo['mime']}");
			$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
			if (fwrite($tmpFile, file_get_contents($url, false, $context)) !== false)
				$info = [];
			else {
				$info = false;
				$this->record("readImage failed to read in $url", 3);
			}
			unset($context);
			// fwrite($tmpFile, fread($handle, filesize($url)));
			fclose($tmpFile);
		}
		else {
			$imgInfo = getimagesize($url);
			header("Content-type: {$imgInfo['mime']}");
			$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
			if ( file_put_contents($tempFile, file_get_contents($url), 0, $context) !== false)
				$info = [];
			else {
				$info = false;
				$this->record("file_get_contents failed to read in $url", 3);
			}
			unset($context);
		}
		

		if ($info !== false)
			$info['filename'] = $tempFile;

		return $info;
	}

	protected function doOneImageBorder($listing, &$image, $alreadyDone, $index, $imageClass, &$fixedTB, &$fixedLR, &$retrieveCount, $path, &$imgTmpName, &$madeImages, &$needUpdate, &$imageWasProcessed, &$first_image){
		$localImgTmpName = '';
		$img = $image_info = null;
		$width = $height = $type = $attr = 0;
		$imageWasProcessed = false;
		$baseProcessValue = BASE_IMAGE_PROCESSED_VALUE;

		if ( !$alreadyDone &&
			 ((isset($image->file) &&
			   !empty($image->file) &&
			   substr($image->file, 0, 4 ) == 'http') ||
			  isset($image->url)) ) { // only one of them can can be 'http' address
			$name = (isset($image->file) && !empty($image->file) && substr($image->file, 0, 4 ) == 'http') ? removeslashes($image->file) : removeslashes($image->url);

			$readCount = 5;
			$tries = 1;
			$image_info = null;
			while( $readCount) {
				try {
					$image_info = $this->readImage($name, $tries);
					if ($image_info !== false) 
						break;
				}
				catch(\Exception $e) {
					$msg = $e->getMessage();
					$code = $e->getCode();
					$image_info = null;
					$msg = "Listing: $listing->id - index:$index, name:$name, try:$tries, errno:$code, msg:$msg";
					$this->updateToDb($msg, self::IMAGE_BORDER_ERROR);
					if ($code != 28) // some other error than a timeout!
						break;
				}
				$readCount--;
				$tries++;
			}
			$localImgTmpName = $imgTmpName = !empty($image_info) ? $image_info['filename'] : '';
			if ( !empty($image_info) ) {
				 if ( ($tries > 1 ||
				  	   $image_info['total_time'] > 4) ) {
					$msg = "Listing: $listing->id - index:$index, name:$name, tries:$tries, size:{$image_info['size_download']}, time to download:{$image_info['total_time']}, speed:{$image_info['speed_download']}";
					$this->updateToDb($msg, self::IMAGE_BORDER_UPDATE);
				}
				$retrieveCount++;
			}
			else {
				$msg = "Listing: $listing->id - index:$index, name:$name, failed to be retrieved";
				$this->updateToDb($msg, self::IMAGE_BORDER_ERROR_FATAL);
			}					

			if (!$alreadyDone &&
				 !empty($localImgTmpName) &&
				 $imageClass->hasWhiteBorder($localImgTmpName, $img, $name, $top, $bottom)) {
				$dest = empty($dest) ? $path."/".$this->getBaseImageName($name).".jpeg" : $dest;
				// $dest = __DIR__."/../_img/_listings/uploaded/$base.jpeg";
				$fixedTB++;
				switch($op) {
					case TRIM_IMAGE:
						$cropped = $imageClass->trimTopBottom($img, $top, $bottom);
						imagedestroy($img);
						$img = $cropped;
						break;
					case BLACKOUT_IMAGE:
						$imageClass->blackOutTopBottom($img, $top, $bottom);
						break;
				}
				if (imagejpeg($img, $dest, 100)) {
					// $height = imagesy($img);
					// $width = imagesx($img);
					imagedestroy($img);
					$img = null;
					list($width, $height, $type, $attr) = getimagesize($dest);
					$out = $imageClass->generateSizes('listings', $dest, true);
					if ($out->status != 'OK') {
							$msg = "Listing: $listing->id - error generateSizes(1): ".(gettype($out->data) == 'string' ? $out->data : print_r($out->data, true)).", source: $name, processed: $image->processed";
						$this->updateToDb($msg, self::IMAGE_BORDER_ERROR);
					}
					$madeImages = true;
					$imageWasProcessed = true;

					if (!isset($image->url)) {
						$image->url = addslashes($image->file);
						unset($image->file);
						$image->file = basename($dest);
						$image->width = $width;
						$image->height = $height;
						$needUpdate = true;
						// $msg = "Listing: $listing->id - index:$index, width:$width, height:$height, needUpdate:".self::IMAGE_BORDER_MODIFIED_X;
						// $this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_X, 2);
					}
					else {//if (!isset($image->file)) {
						$image->file = basename($dest);
						$image->width = $width;
						$image->height = $height;
						$needUpdate = true;
						// $msg = "Listing: $listing->id - index:$index, width:$width, height:$height, needUpdate:".self::IMAGE_BORDER_MODIFIED_4;
						// $this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_4, 2);
					}
					if (empty($image->processed)) {
						$needUpdate = true;
						// $msg = "Listing: $listing->id - index:$index, needUpdate:".self::IMAGE_BORDER_MODIFIED_5;
						// $this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_5, 2);
					}
					$image->processed = $baseProcessValue;
					$localImgTmpName  = $name = $dest; // run it through side borders
				}
				else {
					$msg = "Listing: $listing->id - Failed image_jpeg for $dest";
					$this->updateToDb($msg, self::IMAGE_BORDER_FAILED_CREATE_IMAGE_JPEG);
					$image->processed = -1;
					$needUpdate = true;
				}
			} // if hasWhiteBorder()

			if ($img) imagedestroy($img);
			

			if (!$alreadyDone &&
				!empty($localImgTmpName) &&
				 $imageClass->hasWhiteBorderSides($localImgTmpName, $img, $name, $left, $right)) {
				$width = imagesx($img);
				if ( ($width - $left - $right) < MINIMUM_IMAGE_WIDTH) {// reject image
					$image->discard = (isset($image->file) && !empty($image->file) && substr($image->file, 0, 4 ) == 'http') ? removeslashes($image->file) : removeslashes($image->url);
					if (isset($image->file)) { unset($image->file); $needUpdate = true; }
					if (isset($image->url)) { unset($image->url); $needUpdate = true; }
				}
				else {
					$fixedLR++;
					$dest = empty($dest) ? $path."/".$this->getBaseImageName($name).".jpeg" : $dest;
					switch($op) {
						case TRIM_IMAGE:
							$cropped = $imageClass->trimLeftRight($img, $left, $right);
							imagedestroy($img);
							$img = $cropped;
							break;
						case BLACKOUT_IMAGE:
							$imageClass->blackOutLeftRight($img, $left, $right);
							break;
					}
					if (imagejpeg($img, $dest, 100)) {
						// $height = imagesy($img);
						// $width = imagesx($img);
						imagedestroy($img);
						$img = null;
						list($width, $height, $type, $attr) = getimagesize($dest);
						$out = $imageClass->generateSizes('listings', $dest, true);
						if ($out->status != 'OK') {
							$msg = "Listing: $listing->id - error generateSizes(2): ".(gettype($out->data) == 'string' ? $out->data : print_r($out->data, true)).", source: $name, processed: $image->processed";
							$this->updateToDb($msg, self::IMAGE_BORDER_ERROR);
						}
						$madeImages = true;
						$imageWasProcessed = true;

						if (!isset($image->url)) {
							$image->url = addslashes($image->file);
							unset($image->file);
							$image->file = basename($dest);
							$needUpdate = true;
							// $msg = "Listing: $listing->id - index:$index, needUpdate:".self::IMAGE_BORDER_MODIFIED_7;
							// $this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_7, 2);
						}
						else { //if (!isset($image->file)) {
							$image->file = basename($dest);
							$needUpdate = true;
							// $msg = "Listing: $listing->id - index:$index, needUpdate:".self::IMAGE_BORDER_MODIFIED_8;
							// $this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_8, 2);
						}
						if ( (!property_exists($image, 'width') || // new case
							  !property_exists($image, 'height')) ||
							  ($image->width != $width ||          // if it had top/bottom border case
							   $image->height != $height) ) {
							$image->width = $width;
							$image->height = $height;
							$needUpdate = true;
							// $msg = "Listing: $listing->id - index:$index, needUpdate:".self::IMAGE_BORDER_MODIFIED_9;
							// $this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_9, 2);
						}
						if (empty($image->processed)) {
							$needUpdate = true;
							// $msg = "Listing: $listing->id - index:$index, needUpdate:".self::IMAGE_BORDER_MODIFIED_10;
							// $this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_10, 2);
						}
						$image->processed = $baseProcessValue;
					}
					else {
						$msg = "Listing: $listing->id - Failed @2 image_jpeg for $dest";
						$this->updateToDb($msg, self::IMAGE_BORDER_FAILED_CREATE_IMAGE_JPEG);
						$image->processed = -1;
						$needUpdate = true;
					}
				}
			}

			if ($img) imagedestroy($img);

			if (!$imageWasProcessed &&
				((isset($image->file) &&
				  !empty($image->file) &&
			   	  substr($image->file, 0, 4 ) == 'http') ||
			  	 isset($image->url)) &&
			  	!empty($imgTmpName) && // this is the temporary image file read using the http url
				$image->width > self::MAX_IMAGE_WIDTH &&
				$image->height > self::MAX_IMAGE_HEIGHT) {
				$name = (isset($image->file) && !empty($image->file) && substr($image->file, 0, 4 ) == 'http') ? removeslashes($image->file) : removeslashes($image->url);
				$msg = "Listing: $listing->id - index:$index, width:$image->width, height:$image->height, $name".self::IMAGE_BORDER_HUGE_FILE;
				$this->updateToDb($msg, self::IMAGE_BORDER_HUGE_FILE);
				if ($this->processHugeImageFile) {
					$dest = $path."/".$this->getBaseImageName($name).".jpeg";
					
					$out = $imageClass->generateSizes('listings', $imgTmpName, $this->forceRegenerateHugeImageFile ? true : false, $dest);
					if ($out->status != 'OK') {
						$msg = "Listing: $listing->id - error generateSizes(3): ".(gettype($out->data) == 'string' ? $out->data : print_r($out->data, true)).", source: $name, processed: $image->processed";
						$this->updateToDb($msg, self::IMAGE_BORDER_ERROR);
					}
					else {
						$madeImages = true;
						$imageWasProcessed = true;
						// $this->process("touch $dest"); // create empty file
						$this->process("mv $imgTmpName $dest"); // move temp file to uploaded

						if (!isset($image->url)) {
							$image->url = addslashes($image->file);
							unset($image->file);
							$image->file = basename($dest);
							$needUpdate = true;
						}
						else if (!isset($image->file)) {
							$image->file = basename($dest);
							$needUpdate = true;
						}
						if (empty($image->processed)) {
							$needUpdate = true;
						}
						$image->processed = $baseProcessValue;
					}
				}
			}

			if (!empty($image_info)) {
				unlink($image_info['filename']); // remove the temp file
				unset($image_info);
			}

			if (isset($image->file) && // static image is present, but if no width and height values, so save them
				!empty($image->file) &&
		  	    substr($image->file, 0, 4 ) != 'http') {
				if (!isset($image->width) || // then get width/height from static image and record it.
		  			!isset($image->height)) {  
					list($width, $height, $type, $attr) = getimagesize($path."/".removeslashes($image->file));
					$image->width = $width;
					$image->height = $height;
					$needUpdate = true;
					// $msg = "Listing: $listing->id - index:$index, width:$width, height:$height, needUpdate:".self::IMAGE_BORDER_MODIFIED_11;
					// $this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_11, 2);
				}
			}
			// restore file if necessary from url
			if ((!isset($image->file) || empty($image->file)) &&
				 isset($image->url)) {
				$image->file = addslashes($image->url);
				unset($image->url);
				$needUpdate = true;
				$msg = "Listing: $listing->id - index:$index, needUpdate:".self::IMAGE_BORDER_MODIFIED_12;
				$this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_12, 3);
			}

			if (!isset($image->processed) ||
				$image->processed == 0) { // means it was either processed or agent added, so set flag so it doesn't get reprocessed
				$image->processed = $baseProcessValue;
				$needUpdate = true;						
				$msg = "Listing: $listing->id - index:$index, needUpdate:".self::IMAGE_BORDER_MODIFIED_13;
				$this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_13, 2);						
			}
		} // if url is in 'file' or 'url'
		if ($first_image == null &&
			isset($image->file))
			$first_image = $image;
	}

	// this will now create a list_view entry for EVERY listing, regardless of whether any border processing was done
	public function fixOneImageBorder($listing, $op, $path, $onlyNew, &$lastId) {
		$imageModified = false;
		$imageClass = $this->getClass('Image');
		$needUpdate = false;
		$madeImages = false;
		$reason = self::IMAGE_BORDER_LISTING_START;
		
		$result = array('id'=>$listing->id,
						'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)),
						'msg'=>'Begin image processing for listing id:'.$listing->id);
		$this->updateToDb(json_encode($result), $reason);
		$lastId = $listing->id;
		$baseProcessValue = BASE_IMAGE_PROCESSED_VALUE;
		$startTime = time();
		global $sizes;

		if (!empty($listing->images)) {
			$deleteIndices = [];
			$retrieveCount = 0;
			$assignListImageCount = 0;
			$makeListImageCount = 0;
			$failMakeListImageCount = 0;
			$fixedTB = 0;
			$fixedLR = 0;
			$first_image = null;
			$list_image = null;

			$this->record("fixOneImageBorder - before for loop for listing: $listing->id starting at ".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)), 3);
			foreach($listing->images as $index=>&$image) {
				$img = null;
				$name = '';
				$dest = '';
				$top = 0; $bottom = 0;
				$left = 0; $right = 0;
				$alreadyDone = false;
				$imageWasProcessed = false;
				$regenSizes = false;
				$imgTmpName = '';
				if ( $onlyNew ) {
					if (isset($image->file) && 
						!empty($image->file) &&
						substr($image->file, 0, 4 ) != 'http') {  // then a static image exists for this 
						$name = $path.'/'.removeslashes($image->file);
						// $name = __DIR__."/../_img/_listings/uploaded/".removeslashes($image->file);
						if (file_exists(($name))) {
							$msg = "Listing: $listing->id, found static file: $name, processed: $image->processed";
							$this->updateToDb($msg, self::IMAGE_BORDER_UPDATE);
							if (!isset($image->url)) {
								$alreadyDone = true; // must be one of our artificial listings for the home page or organically added image by agent
								if ($image->processed < JPG_QUALITY_AT_80)
									$regenSizes = true;
							}
							else {
								// $alreadyDone = $image->processed >= $baseProcessValue ? true : false;
								$alreadyDone = true;
								if ($image->processed < JPG_QUALITY_AT_80)
									$regenSizes = true;
								if (!isset($image->width) ||
									!isset($image->height) ||
									empty($image->width) ||
									empty($image->height)) {
									list($width, $height, $type, $attr) = getimagesize($name);
									$image->width = $width;
									$image->height = $height;
									$needUpdate = true;
									$msg = "Listing: $listing->id - index:$index, needUpdate:".self::IMAGE_BORDER_MODIFIED_14;
									$this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_14);
								}
								else {
									$msg = "Listing: $listing->id - index:$index, processed:$image->processed, alreadyDone:".($alreadyDone ? 'yes' : 'no');
									$this->updateToDb($msg, self::IMAGE_BORDER_UPDATE);
									unset($image->file);
								}
							}
						}
						else if (isset($image->url)) {
							$msg = "Listing: $listing->id, failed to locate file: $name, processed: $image->processed";
							$this->updateToDb($msg, self::IMAGE_BORDER_ERROR, 3, true);
							unset($image->file);
							$image->file = removeslashes($image->url);
							unset($image->url);
							$needUpdate = true;
						}
						else {
							$msg = "Listing: $listing->id, failed to locate file: $name, processed: $image->processed and has no URL";
							$this->record($msg, 3, self::IMAGE_BORDER_ERROR_FATAL);
							$deleteIndices[] = $index;
							continue;
						}
					}
					elseif (isset($image->file) && 
							!empty($image->file) &&
							isset($image->processed)) {
						$name = removeslashes($image->file);
						$dest = $path."/".$this->getBaseImageName($name).".jpeg";
						if (file_exists(($dest))) { // then 'file' has http, but a processed image exists!
							$startImgGen = microtime();
							$out = $imageClass->generateSizes('listings', $dest, false);
							$endImgGen = microtime();
							if ($out->status != 'OK') {
								$generated = 0;
								foreach($out->data as $stats)
									if ($stats->status == 'OK')
										$generated++;
									// else {
									// 	$msg = "Failed listingId:$listing->id to generate image: $stats->data";
									// 	$this->updateToDb($msg, self::IMAGE_GEN_IMAGE_ERROR);
									// }

								$msg = "Listing: $listing->id - Got back ".count($out->data)." results, ".($generated ? "Generated $generated images" : "All present, none generated")." , took:".$this->diffTime($startImgGen, $endImgGen)." secs , source: $dest";
								$this->updateToDb($msg, self::IMAGE_BORDER_HTTP_HAD_LOCAL_IMAGES, 3);
							}
							else {
								$msg = "Listing: $listing->id - generateSizes all images, generate:".$this->diffTime($startImgGen, $endImgGen)." secs, $dest";
								$this->updateToDb($msg, self::IMAGE_BORDER_HTTP_HAD_LOCAL_IMAGES, 3);
							}
							$image->url = addslashes($image->file);
							unset($image->file);
							$image->file = addslashes(basename($dest));
							$needUpdate = true;
							$alreadyDone = true;
							$image->processed = $baseProcessValue;
							if (!isset($image->width) ||
								!isset($image->height) ||
								empty($image->width) ||
								empty($image->height)) {
								list($width, $height, $type, $attr) = getimagesize($dest);
								$image->width = $width;
								$image->height = $height;
								$needUpdate = true;
								$msg = "Listing: $listing->id - index:$index, needUpdate:".self::IMAGE_BORDER_MODIFIED_15;
								$this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_15, 2);
							}
							// else {
							// 	$msg = "Listing: $listing->id - index:$index, needUpdate:".self::IMAGE_BORDER_MODIFIED_0;
							// 	$this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_0, 2);
							// }
						}
						else {
							if ($this->processHugeImageFile &&
								isset($image->width) &&
								isset($image->height) &&
								$image->width > self::MAX_IMAGE_WIDTH &&
								$image->height > self::MAX_IMAGE_HEIGHT &&
								isset($image->file) && substr($image->file, 0, 4 ) == 'http') { // resize huge files!
								$alreadyDone = false;
							}
							else { // these are regular http images that we don't make sub size for yet, so set alreadyDone to true 
								$alreadyDone = isset($image->processed) && $image->processed >= JPG_QUALITY_AT_95 && $image->processed <= $baseProcessValue ? true : false; // if we ever looked at it before or not
								$msg = "Listing: $listing->id - index:$index, http file, processed:$image->processed, alreadyDone:$alreadyDone";
								$this->updateToDb($msg, self::IMAGE_BORDER_UPDATE, 3, true);
								if (!isset($image->width) ||
									!isset($image->height) ||
									empty($image->width) ||
									empty($image->height)) {
									list($width, $height, $type, $attr) = getimagesize($name);
									$image->width = $width;
									$image->height = $height;
									$needUpdate = true;
									// $msg = "Listing: $listing->id - index:$index, needUpdate:".self::IMAGE_BORDER_MODIFIED_1;
									// $this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_1);
								}
							}
						}
					}
					elseif (!isset($image->file) ||
							 empty($image->file)) {
						if ( isset($image->url) ) {
							$msg = "Listing: $listing->id - index:$index, failed to find file:image->file, resetting to url:$image->url, processed: $image->processed";
							$this->updateToDb($msg, self::IMAGE_BORDER_NO_FILE, 3, true, $listing->id);
							$image->file = $image->url;
							$image->processed = 0;
							$needUpdate = true;
						}
						else {
							$msg = "Listing: $listing->id, index:$index failed, has no file nor URL";
							$this->record($msg, 3, self::IMAGE_BORDER_ERROR_FATAL);
							$deleteIndices[] = $index;
							continue;
						}
					}
				}

				if (isset($image->discard)) // must be discard
					continue;

				$msg = "Listing: $listing->id - index:$index, onlyNew:".($onlyNew ? 'yes' : 'no').", alreadyDone:".($alreadyDone ? 'yes' : 'no').", regenSizes:".($regenSizes ? 'yes' : 'no');
				$this->updateToDb($msg, self::IMAGE_BORDER_UPDATE, 2, true);

				$this->doOneImageBorder($listing, $image, $alreadyDone, $index, $imageClass, $fixedTB, $fixedLR, $retrieveCount, $path, $imgTmpName, $madeImages, $needUpdate, $imageWasProcessed, $first_image);
				if ($regenSizes &&
					!$imageWasProcessed) {
					$out = $imageClass->generateSizes('listings', $name, $this->forceRegenerateHugeImageFile ? true : false);
					if ($out->status != 'OK') {
						$msg = "Listing: $listing->id - error generateSizes(3): ".(gettype($out->data) == 'string' ? $out->data : print_r($out->data, true)).", source: $name, processed: $image->processed";
						$this->updateToDb($msg, self::IMAGE_BORDER_ERROR, 3, true,$listing->id);
					}
					else {
						$needUpdate = true;
						$image->processed = $baseProcessValue;
					}
					$msg = "Listing: $listing->id - regenSizes - index:$index, was ".$out->status.($out->status != 'OK' ? ", results:".print_r($out, true) : '');
					$this->updateToDb($msg, self::IMAGE_BORDER_REGEN_OK, 3, true, $listing->id);
					unset($out);
				}

				// set the list_image
				if ( !$list_image ) {
					$checkExistingListImage = false;
					if (empty($listing->list_image)) 
						$needMakeListImage = true;
					elseif ($listing->list_image->file != $image->file) {
						if (substr($image->file, 0, 4 ) != 'http') // then it's a processed file
							$needMakeListImage = true;
						elseif ($this->getBaseImageName($image->file) != $listing->list_image->file)
							$needMakeListImage = true;
						else
							$checkExistingListImage = true;
					}
					else
						$checkExistingListImage = true;

					if ($checkExistingListImage) { // name matches, make sure they exist
						$name = $path.'/'.$listing->list_image->file;
						$sizedName1 = $path.'/845x350/'.$listing->list_image->file;
						$sizedName2 = $path.'/210x120/'.$listing->list_image->file;
						if (!file_exists($name)) {
							if (file_exists($sizedName1) &&
								file_exists($sizedName2)) {
								@popen("touch $name", 'r');
								$msg = "Listing: $listing->id - touch $name";
								$this->updateToDb($msg, self::IMAGE_BORDER_TOUCH_LIST_IMAGE_UPLOADED_FILE, 3, true, $listing->id);
								$needMakeListImage = false;
							}
							else
								$needMakeListImage = true;
						}
						elseif (file_exists($sizedName1) &&
								file_exists($sizedName2)) 
							$needMakeListImage = false;
						else
							$needMakeListImage = true;						
					}

					if ($needMakeListImage) {
						if (isset($image->file) && !empty($image->file) && substr($image->file, 0, 4 ) != 'http') {// then a static image exists for this 
							$list_image = $image;
							$msg = "Listing: $listing->id - associate index:$index as list_image";
							$this->updateToDb($msg, self::IMAGE_BORDER_ASSOC_LIST_IMAGE, 3, true, $listing->id);
							$assignListImageCount++;
						}
						else {// it's a http file, so we need to create the sizes for it
							$saved = $imgTmpName;
							$this->makeListImage($listing, $index, $image, $sizes, $imageClass, $path, $imgTmpName, $retrieveCount, $list_image);
							$msg = "Listing: $listing->id - makeListImage called index:$index was ".(!empty($list_image) ? "success" : "failure");
							$this->updateToDb($msg, self::IMAGE_BORDER_MADE_LIST_IMAGE, 3, true, $listing->id);
							if (!empty($list_image))
								$makeListImageCount++;
							else
								$failMakeListImageCount++;

							if (!empty($saved) && $saved != $imgTmpName)
								@popen("rm $saved", 'r');
						}
					}
				}
				
				if (!empty($imgTmpName)) 
					@popen("rm $imgTmpName", 'r');
			} // foreach()

			$diffTime = time() - $startTime;
			if ( $retrieveCount) {
				 $perImageFloat =  $diffTime / $retrieveCount;
				 $perImage = number_format($perImageFloat, 2 );
				 $msg = (object)['listing' => $listing->id,
				 				 'retrieved' => $retrieveCount,
				 				 'perImage' => $perImage,
				 				 'fixedTB' => $fixedTB,
				 				 'fixedLR' => $fixedLR];
				 $this->updateToDb(json_encode($msg), self::IMAGE_BORDER_PROCESSING_STAT, 3, true); // force logging
				 unset($msg);
				 if ($perImageFloat > self::DELAY_NEXT_IMAGE_RETIEVAL_IF_PER_IMAGE_IS_OVER_THIS) {
				 	$diff = $perImageFloat - self::DELAY_NEXT_IMAGE_RETIEVAL_IF_PER_IMAGE_IS_OVER_THIS;
				 	$sleepTime = $diff > 5.0 ? 5.0 : $diff;
				 	usleep( $sleepTime * 1000000);
				 	$this->record("fixOneImageBorder - Sleeping for ".number_format($sleepTime, 3)." secs, as perImage is at $perImage", 3, true);
				 }
			}

			$this->record("fixOneImageBorder - completed for loop for listing: $listing->id, number of imaged retrieved:$retrieveCount, fixedTB:$fixedTB, fixedLR:$fixedLR, assignListImageCount:$assignListImageCount, makeListImageCount:$makeListImageCount, failMakeListImageCount:$failMakeListImageCount, needUpdate:".($needUpdate ? 'yes' : 'no').", regenSizes:".($regenSizes ? 'yes' : 'no').", list_image:".$list_image->file." end at ".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600))." duration:$diffTime", 3);
			$reason = self::IMAGE_BORDER_LISTING_DONE;
			$finalMsg = '';
			$clearListing = false;

			if (!empty($deleteIndices)) {// uh oh, had some files as processed, but no url
				$first = true;
				if (!empty($listing->listhub_key)) { // remove these images, it will be re-read in the next parse
					array_reverse($deleteIndices); // so we get the higher index first
					$errorList = '';
					$this->record("fixOneImageBorder - before deleting indices for listing: $listing->id:".print_r($deleteIndices, true), 3);
					foreach($deleteIndices as $i) {
						unset($listing->images[$i]);
						$errorList .= ($first ? '' : ',').$i;
						$first = false;
					}
					$msg = "The following image indices for listing:$listing->id has lost image information: $errorList";
					$this->record($msg, 3, self::IMAGE_BORDER_ERROR, $listing->id);
					$needUpdate = true;

				}
				else { // uh oh, organically created listing has lost all info! yikes!
					$msg = "The following image indices for listing:$listing->id has lost image information\n";
					foreach($deleteIndices as $i) {
						$msg .= ($first ? '' : ',').$i;
						$first = false;
					}
					$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
													   'Image data lost',
													   $msg,
													   "Technical Problem",
													   "See why this listing has lost image information");
				}
			}
			unset($deleteIndices);
			
			if (!$needUpdate) {
				$clearListing = true;
				if ($madeImages)
					$finalMsg = "Listing: $listing->id - Updated images ";
				else {
					$finalMsg = "Listing: $listing->id - No images had white borders ";
					$this->duplicates++;
				}
				$updatedProcessedValue = false;
				foreach($listing->images as $index=>&$image) {
					if (!isset($image->processed) ||
						($image->processed >= 0 && $image->processed < $baseProcessValue)) { // means it was either processed or agent added, so set flag so it doesn't get reprocessed
						$image->processed = $baseProcessValue;
						$needUpdate = true;	
						$updatedProcessedValue = true;					
					}
				}
				if ($updatedProcessedValue) {
					$msg = "Listing: $listing->id - index:$index, needUpdate:".self::IMAGE_BORDER_MODIFIED_17;
					$this->updateToDb($msg, self::IMAGE_BORDER_MODIFIED_17, 2);						
				}
			}

			$fields = [];

			if ( $first_image &&
				(empty($listing->first_image) ||
				 $listing->first_image->file != $first_image->file) ) {
				$fields['first_image'] = $first_image;
			}

			if ( $list_image &&
				(empty($listing->list_image) ||
				 $listing->list_image->file != $list_image->file) ) {
				$fields['list_image'] = $list_image;
			}

			if ($needUpdate ||
				count($fields) ){
				if (!$clearListing) $this->addedToDb++;
				if ($needUpdate)
					$fields['images'] = $listing->images;
				if ($this->specialOpType != IMAGE_BORDER_PARALLEL_BATCH) {
					try {
						$x = $this->getClass('Listings')->set([(object)['where'=>['id'=>$listing->id],
																		'fields'=>$fields]]);
					}
					catch(\Exception $e) {
						parseException($e);
					}
				}
				else {
					$listing->needUpdate = true; // do it in batch later
					$listing->fields = $fields;
					$x = true;
				}

				if ($x == false) {
					$finalMsg .= "Listing: $listing->id - ".(!$clearListing ? "Updated images " : "clean ")."but could not update data";
					$reason = self::IMAGE_BORDER_ERROR;
				}
				else {
					$finalMsg .= "Listing: $listing->id - ".(!$clearListing ? "Updated images and " : 'Updated ')."listing's data";
					$reason = self::IMAGE_BORDER_LISTING_UPDATED;
				}
			}
			unset($fields);
		}
		else {
			$finalMsg = "Listing: $listing->id has no images";
			$this->rejected++;
		}


		$result = array('id'=>$listing->id,
						'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)),
						'msg'=>$finalMsg);
		$this->updateToDb(json_encode($result), $reason);
	}

	protected function makeListImage($listing, $index, $image, $sizes, $imageClass, $path, &$imgTmpName, &$retrieveCount, &$list_image) {
		$image_info = null;
		$name = (isset($image->file) && substr($image->file, 0, 4 ) == 'http') ? removeslashes($image->file) : removeslashes($image->url);
		if (empty($imgTmpName) ||
			!file_exists($imgTmpName)) {
			$image_info = $this->readImage($name, $tries);
			if ($image_info == false) {
				$msg = "Listing: $listing->id - makeListImage failed to retrieve image: $name, index: $index, processed: $image->processed";
				$this->updateToDb($msg, self::IMAGE_BORDER_ERROR, 3, true, $listing->id);
				return;
			}
			// $name = $path.'/'.removeslashes( basename($image->file) );
			$retrieveCount++;
			$imgTmpName = $image_info['filename'];
		}
		$base = $this->getBaseImageName($name);
		if (empty($base)) {
			$msg = "Listing: $listing->id - makeListImage failed to create basename from: $name, index: $index, processed: $image->processed";
			$this->updateToDb($msg, self::IMAGE_BORDER_FAILED_TO_CREATE_BASENAME, 3, true, $listing->id);
			return;
		}
		$dest = $path."/".$base.".jpeg";
		$made = 0;
		foreach($sizes as $size) {
			$out = $imageClass->generateSingleSize($imgTmpName, 'listings', $size, true, $dest);
			if ($out->status != 'OK') {
				$msg = "Listing: $listing->id - makeListImage failed to create size:{$size['width']}x{$size['height']} image: $name, imgTmpName:$imgTmpName, index: $index, dest:$dest, processed: $image->processed, results:".print_r($out, true);
				$this->updateToDb($msg, self::IMAGE_BORDER_ERROR, 3, true, $listing->id);
			}
			else
				$made++;
			unset($size);
		}
		if ($made == count($sizes)) {
			$bname = basename($dest);
			if ( $bname == '.jpeg') {
				$msg = "Listing: $listing->id - makeListImage failed to create list_image from $name, endedup with $bname, imgTmpName:$imgTmpName, index: $index, dest:$dest, processed: $image->processed";
				$this->updateToDb($msg, self::IMAGE_BORDER_BAD_DEST_MAKING_LIST_IMAGE, 3, true, $listing->id);
			}
			else {
				$list_image = (object)['file'=>$bname];
				if (!file_exists($dest)) {
					@popen("cp $imgTmpName $dest", 'r');
					$msg = "Listing: $listing->id - copied tmpFile:$imgTmpName to $dest";
					$this->updateToDb($msg, self::IMAGE_BORDER_CREATE_LIST_IMAGE_UPLOADED_FILE, 3, true, $listing->id);
				}
			}
		}
		else {
			$msg = "Listing: $listing->id - makeListImage failed to set list_image, $name, imgTmpName:$imgTmpName, index: $index, dest:$dest, processed: $image->processed";
			$this->updateToDb($msg, self::IMAGE_BORDER_ERROR, 3, true, $listing->id);
		}
		unset($image_info);
	}

	// what we want this custom task to do:
	// 1: visit the listing, as if was previously processed and:
	//    a: has processed image:
	//       aa: if baseProcessValue < JPG_QUALITY_AT_80 then regenerate all processed image based on $img->file
	//       bb: else set first_image and list_image
	//    b: has no processed image:
	//       aa: process 1st image that is not a discard and set first_image (to http) and list_image with the processed image filename (selected sizes only)
	// 2: was never processed before:
	//    a: process 1st image that is not a discard and set first_image (to http) and list_image with the processed image filename (selected sizes only)
	// 3: check to see the image->file is not empty, if it is, reset to image->url value and unset() image->url
	//    a: if image->url isn't there either, discard image
	// 4: if list_image is already defined, make sure the file is in the uploaded folder.  If not, check 845x250 and 210x120 sizes and if they exist, then touch uploaded folder
	//
	public function doOneCustomImageTask($listing, $op, $path, $onlyNew, &$lastId) {
		$imageModified = false;
		$imageClass = $this->getClass('Image');
		$needUpdate = false;
		$madeImages = false;
		$reason = self::IMAGE_BORDER_LISTING_START;
		
		$result = array('id'=>$listing->id,
						'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)),
						'msg'=>'Begin image processing for listing id:'.$listing->id);
		$this->updateToDb(json_encode($result), $reason);
		$lastId = $listing->id;
		$baseProcessValue = BASE_IMAGE_PROCESSED_VALUE;
		$startTime = time();
		global $sizes;

		if (!empty($listing->images)) {
			$deleteIndices = [];
			$retrieveCount = 0;
			$fixedTB = 0;
			$fixedLR = 0;
			$first_image = null;
			$list_image = null;
			$assignListImageCount = 0;
			$makeListImageCount = 0;
			$failMakeListImageCount = 0;

			$this->record("doOneCustomImageTask - before for loop for listing: $listing->id", 3);
			foreach($listing->images as $index=>&$image) {
				// this custom task is for redoing existing processed images to 80% compression
				if (!isset($image->processed) ||
					$image->processed == 0 )
					continue; // leave alone for regular image border processing...

				$img = null;
				$name = '';
				$dest = '';
				$top = 0; $bottom = 0;
				$left = 0; $right = 0;
				$alreadyDone = false;
				$haveProcessedImage = false;
				$currentProcessLevel = false;
				$reprocessImageBorder = false;
				$imageWasProcessed = false;
				$imgTmpName = '';
				$currentProcessLevel = $image->processed >= $baseProcessValue ? true : false;
				if ( $onlyNew ) { // should always be true for custom
					if (isset($image->file) && 
						!empty($image->file) &&
						substr($image->file, 0, 4 ) != 'http') {  // then a static image exists for this 
						$name = $path.'/'.removeslashes($image->file);
						// $name = __DIR__."/../_img/_listings/uploaded/".removeslashes($image->file);
						if (file_exists($name)) {
							$haveProcessedImage = true;
							$msg = "Listing: $listing->id - index:$index, currentProcessLevel:$currentProcessLevel, haveProcessedImage:".($haveProcessedImage ? 'yes' : 'no');
							$this->updateToDb($msg, self::IMAGE_BORDER_UPDATE);
							$currentProcessLevel = $image->processed >= JPG_QUALITY_AT_80;						
						}
						else if (isset($image->url)) { // do we need to re-image border this one?
							$msg = "Listing: $listing->id, failed to locate file: $name, processed: $image->processed";
							$this->updateToDb($msg, self::IMAGE_BORDER_ERROR, 2);
							unset($image->file);
							$image->file = removeslashes($image->url);
							unset($image->url);
							$needUpdate = true;
							$reprocessImageBorder = true;
						}
						else { // utter failure...  no static image located, and lost the url...
							$msg = "Listing: $listing->id, failed to locate file: $name, processed: $image->processed and has no URL";
							$this->record($msg, 3, self::IMAGE_BORDER_ERROR_FATAL);
							$deleteIndices[] = $index;
							continue;
						}
					}
					elseif (isset($image->discard))
						continue; // must be a discard
					elseif (!isset($image->file) ||
							 empty($image->file)) {
						if ( isset($image->url) ) {
							$msg = "Listing: $listing->id - index:$index, failed to find file:image->file, resetting to url:$image->url, processed: $image->processed";
							$this->updateToDb($msg, self::IMAGE_BORDER_NO_FILE, 3, true, $listing->id);
							$image->file = removeslashes($image->url);
							$image->processed = 0;
							$needUpdate = true;
							$reprocessImageBorder = true;
						}
						else {
							$msg = "Listing: $listing->id, failed to has no file nor URL";
							$this->record($msg, 3, self::IMAGE_BORDER_ERROR_FATAL);
							$deleteIndices[] = $index;
							continue;
						}
					}
				}
				else {
					$msg = "Listing: $listing->id, onlyNew is set to false!  Nothing will be done!";
					$this->record($msg, 3, self::IMAGE_BORDER_ERROR_FATAL, true);
					continue;
				}

				$msg = "Listing: $listing->id - index:$index alreadyDone:".($alreadyDone ? 'yes' : 'no');
				$this->updateToDb($msg, self::IMAGE_BORDER_UPDATE, 2);

				$fields = [];

				if ($reprocessImageBorder) {
					$this->doOneImageBorder($listing, $image, $alreadyDone, $index, $imageClass, $fixedTB, $fixedLR, $retrieveCount, $path, $imgTmpName, $madeImages, $needUpdate, $imageWasProcessed, $first_image);
					$currentProcessLevel = true; // this image is now up-to-date, 
					// first_image will be set by doOneImageBorder()...
					if (!$list_image) {
						$list_image = $image;
						$$assignListImageCount++;
					}
				}

				if (!$currentProcessLevel){
					if ($haveProcessedImage) { // then regenerate all sizes again to 80%
						$out = $imageClass->generateSizes('listings', $name, true);
						if ($out->status != 'OK') {
							$msg = "Listing: $listing->id - doOneCustomImageTask error generateSizes(1): ".(gettype($out->data) == 'string' ? $out->data : print_r($out->data, true)).", source: $name, processed: $image->processed";
							$this->updateToDb($msg, self::IMAGE_BORDER_ERROR);
							continue;
						}
						// we know this is processed...
						if (!$first_image)
							$first_image = $image;
						if (!$list_image) {
							$list_image = $image;
							$$assignListImageCount++;
						}
					}
					else if (!$first_image ||
							 !$list_image) { // then http file, generate specific sizes for list view image
						$first_image = $image; // save unprocessed image
						$saved = $imgTmpName;
						$this->makeListImage($listing, $index, $image, $sizes, $imageClass, $path, $imgTmpName, $retrieveCount, $list_image);
						$msg = "Listing: $listing->id - makeListImage called index:$index was ".(!empty($list_image) ? "success" : "failure");
						$this->updateToDb($msg, self::IMAGE_BORDER_MADE_LIST_IMAGE, 3, true, $listing->id);
						if (!empty($list_image))
							$makeListImageCount++;
						else
							$failMakeListImageCount++;
						if (!empty($saved) && $saved != $imgTmpName)
							@popen("rm $saved", 'r');
					}
					$image->processed = $baseProcessValue; // mark process version
				}
				else { // make sure first_image and list_image is up to date
					if ( !$first_image ) {
						if ( empty($listing->first_image) ||
						 	 $listing->first_image->file != $image->file)
							$$first_image = $image;
					}

					if ( !$list_image ) {
						$checkExistingListImage = false;
						if (empty($listing->list_image)) 
							$needMakeListImage = true;
						elseif ($listing->list_image->file != $image->file) {
							if (substr($image->file, 0, 4 ) != 'http') // then it's a processed file
								$needMakeListImage = true;
							elseif ($this->getBaseImageName($image->file) != $listing->list_image->file)
								$needMakeListImage = true;
							else
								$checkExistingListImage = true;
						}
						else
							$checkExistingListImage = true;

						if ($checkExistingListImage) { // name matches, make sure they exist
							$name = $path.'/'.$listing->list_image->file;
							$sizedName1 = $path.'/845x350/'.$listing->list_image->file;
							$sizedName2 = $path.'/210x120/'.$listing->list_image->file;
							if (!file_exists($name)) {
								if (file_exists($sizedName1) &&
									file_exists($sizedName2)) {
									@popen("touch $name", 'r');
									$msg = "Listing: $listing->id - touch $name";
									$this->updateToDb($msg, self::IMAGE_BORDER_TOUCH_LIST_IMAGE_UPLOADED_FILE, 3, true, $listing->id);
									$needMakeListImage = false;
								}
								else
									$needMakeListImage = true;
							}
							elseif (file_exists($sizedName1) &&
									file_exists($sizedName2)) 
								$needMakeListImage = false;
							else
								$needMakeListImage = true;						
						}

						if ($needMakeListImage) {
							if (isset($image->file) && !empty($image->file) && substr($image->file, 0, 4 ) != 'http') {// then a static image exists for this 
								$list_image = $image;
								$msg = "Listing: $listing->id - associate index:$index as list_image";
								$this->updateToDb($msg, self::IMAGE_BORDER_ASSOC_LIST_IMAGE, 3, true, $listing->id);
								$assignListImageCount++;
							}
							else {// it's a http file, so we need to create the sizes for it
								$saved = $imgTmpName;
								$this->makeListImage($listing, $index, $image, $sizes, $imageClass, $path, $imgTmpName, $retrieveCount, $list_image);
								$msg = "Listing: $listing->id - makeListImage called index:$index was ".(!empty($list_image) ? "success" : "failure");
								$this->updateToDb($msg, self::IMAGE_BORDER_MADE_LIST_IMAGE, 3, true, $listing->id);
								if (!empty($list_image))
									$makeListImageCount++;
								else
									$failMakeListImageCount++;
								if (!empty($saved) && $saved != $imgTmpName)
									@popen("rm $saved", 'r');
							}
						}
					}
				}
				if (!empty($imgTmpName)) 
					@popen("rm $imgTmpName", 'r');
			} // foreach()

			$diffTime = time() - $startTime;
			if ( $retrieveCount) {
				 $perImageFloat =  $diffTime / $retrieveCount;
				 $perImage = number_format($perImageFloat, 2 );
				 $msg = (object)['listing' => $listing->id,
				 				 'retrieved' => $retrieveCount,
				 				 'perImage' => $perImage,
				 				 'fixedTB' => $fixedTB,
				 				 'fixedLR' => $fixedLR];
				 $this->updateToDb(json_encode($msg), self::IMAGE_BORDER_PROCESSING_STAT, 3, true); // force logging
				 unset($msg);
				 if ($perImageFloat > self::DELAY_NEXT_IMAGE_RETIEVAL_IF_PER_IMAGE_IS_OVER_THIS) {
				 	$latency = $perImageFloat - self::DELAY_NEXT_IMAGE_RETIEVAL_IF_PER_IMAGE_IS_OVER_THIS;
				 	$diff = $latency > 5.0 ? 5.0 : $latency;
				 	usleep( $diff * 1000000);
				 	$this->record("doOneCustomImageTask - Sleeping for ".number_format($diff, 3)." secs, as perImage is at $perImage", 3, true);
				 }
			}

			$this->record("doOneCustomImageTask - completed for-loop for listing: $listing->id, number of imaged retrieved:$retrieveCount, fixedTB:$fixedTB, assignListImageCount:$assignListImageCount, makeListImageCount:$makeListImageCount, failMakeListImageCount:$failMakeListImageCount, fixedLR:$fixedLR, needUpdate:".($needUpdate ? 'yes' : 'no'), 3);
			$reason = self::IMAGE_BORDER_LISTING_DONE;
			$finalMsg = '';
			//$clearListing = false;

			if (!empty($deleteIndices)) {// uh oh, had some files as processed, but no url
				$first = true;
				if (!empty($listing->listhub_key)) { // remove these images, it will be re-read in the next parse
					array_reverse($deleteIndices); // so we get the higher index first
					$errorList = '';
					$this->record("doOneCustomImageTask - before deleting indices for listing: $listing->id:".print_r($deleteIndices, true), 3);
					foreach($deleteIndices as $i) {
						unset($listing->images[$i]);
						$errorList .= ($first ? '' : ',').$i;
						$first = false;
					}
					$msg = "The following image indices for listing:$listing->id has lost image information: $errorList";
					$this->record($msg, 3, self::IMAGE_BORDER_ERROR);
					$needUpdate = true;

				}
				else { // uh oh, organically created listing has lost all info! yikes!
					$msg = "The following image indices for listing:$listing->id has lost image information\n";
					foreach($deleteIndices as $i) {
						$msg .= ($first ? '' : ',').$i;
						$first = false;
					}
					$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
													   'Image data lost',
													   $msg,
													   "Technical Problem",
													   "See why this listing has lost image information");
				}
			}
			unset($deleteIndices);

			$fields = [];

			if ( $first_image &&
				(empty($listing->first_image) ||
				 $listing->first_image->file != $first_image->file) ) {
				$fields['first_image'] = $first_image;
			}

			if ( $list_image &&
				(empty($listing->list_image) ||
				 $listing->list_image->file != $list_image->file) ) {
				$fields['list_image'] = $list_image;
			}

			if ($needUpdate ||
				count($fields) ){
				$this->addedToDb++;
				if ($needUpdate)
					$fields['images'] = $listing->images;

				$listing->needUpdate = true; // do it in batch later
				$listing->fields = $fields;

				$finalMsg .= "Listing: $listing->id - updated images:".($needUpdate ? 'yes' : 'no').", first_image:".(isset($fields['first_image']) ? 'yes' : 'no').", list_image:".(isset($fields['list_image']) ? 'yes' : 'no').", list_image:".$list_image->file;
				$reason = self::IMAGE_BORDER_LISTING_UPDATED;
			}
			unset($fields);
		}
		else {
			$finalMsg = "Listing: $listing->id has no images";
			$this->rejected++;
		}


		$result = array('id'=>$listing->id,
						'time'=>date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)),
						'msg'=>$finalMsg);
		$this->updateToDb(json_encode($result), $reason);
	}

	public function getListingImportImageCount($useImpressions = false) {
		$Listings = $this->getClass('Listings');
		$sql = "SELECT a.id FROM {$Listings->getTableName()} AS a ";
		$sql.= "INNER JOIN {$Listings->getTableName('cities-tags')} AS b ON a.city_id = b.city_id ";
		$sql.= "WHERE ((b.score >= 7 OR b.score = -1) AND a.active = 1 ";
		if ($useImpressions)
			$sql .= " AND a.impressions > 0 ";
		$sql .= ") OR a.author = 3 ";
		$sql.= "GROUP BY a.id";
		$x = $Listings->rawQuery($sql);
		if (empty($x))
			return new Out('fail', "Failed to get any listings with query: $sql");
		else {
			$doingPP = false;
			$opt = $this->getClass('Options')->get((object)['like'=>['opt'=>'active']]);
			foreach($opt as $act)
				if ( $act->opt == 'activePP')
					$doingPP = true;
			if (!$doingPP)
				$this->getClass("ListhubProgress")->emptyTable(true);
			$this->getClass('ImageBlocks')->emptyTable(true);
			$total = count($x);
			$chunk = array_chunk($x, 1000);
			$this->createDebugFile();
			$this->beginOutputBuffering();

			foreach($chunk as $block) {
				$list = '';
				$first = true;
				foreach($block as $id) {
					$list .= ($first ? '' : ',');
					$list .= "('".$id->id."')";
					$first = false;
				}
				$sql = "INSERT INTO {$Listings->getTableName('listhub-blocks')} (`data`) VALUES $list";
				$x = $this->getClass('ImageBlocks')->rawQuery($sql);
				$this->record("Saved $x ids", 3);
				unset($block, $list);
			}

			$this->stopOutputBuffering();
			return new Out('OK', $total);
		}
	}

	public function processListingImportImageBlocks($list, $cpuLimit, $id, $saveRejected, $minTags, $minPrice, $ppRunId) {
		$this->specialOpType = IMPORT_ACTIVE_LISTINGS_IMAGE;
		return $this->processBlocks($list, $cpuLimit, $id, $saveRejected, $minTags, $minPrice, $ppRunId, IMPORT_ACTIVE_LISTINGS_IMAGE);
	}

	public function processGenerateImages($list, $cpuLimit, $id, $saveRejected, $minTags, $minPrice, $ppRunId) {
		$this->specialOpType = CREATE_IMAGES_FROM_IMPORTED;
		return $this->processBlocks($list, $cpuLimit, $id, $saveRejected, $minTags, $minPrice, $ppRunId, CREATE_IMAGES_FROM_IMPORTED);
	}

	public function processImageBorders($list, $cpuLimit, $id, $saveRejected, $minTags, $minPrice, $ppRunId,
										$op, $onlyNew, $specialOp, $locksql = 0) {
		$this->imageBorderOp = $op;
		$this->processOnlyNewImages = $onlyNew;
		$this->specialOpType = $specialOp;
		// $this->specialOpType = IMAGE_BORDER_PARALLEL;
		// $retval = $this->processBlocks($list, $cpuLimit, $id, $saveRejected, $minTags, $minPrice, $ppRunId, IMAGE_BORDER_PARALLEL);
		// $this->specialOpType = IMAGE_BORDER_PARALLEL_BATCH;
		$retval = $this->processBlocks($list, $cpuLimit, $id, $saveRejected, $minTags, $minPrice, $ppRunId, $this->runPPAsBatch ? IMAGE_BORDER_PARALLEL_BATCH : IMAGE_BORDER_PARALLEL, $locksql);
		// popen("rm /tmp/alr??????", 'r');
		return $retval;
	}

	public function processCustomImageProcessing($list, $cpuLimit, $id, $ppRunId, $op, $onlyNew, $specialOp, $locksql = 0) {
		$this->imageBorderOp = $op;
		$this->processOnlyNewImages = $onlyNew;
		$this->specialOpType = $specialOp;
		$retval = $this->processBlocks($list, $cpuLimit, $id, 1, 0, 0, $ppRunId, IMAGE_BORDER_PARALLEL_CUSTOM_TASK, $locksql);
		return $retval;
	}
}
	
