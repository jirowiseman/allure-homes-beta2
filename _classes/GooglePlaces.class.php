<?php
namespace AH;
require_once(__DIR__.'/Utility.class.php');
require_once(__DIR__.'/Logger.class.php');

class GooglePlaces extends Logger {
	private $APIkey = array(
		// 'AIzaSyA-X7VaeHvCIgXkiIHLao8_Yb1D5qmYtd8', // alluretechnologies@gmail
		// 'AIzaSyDTLoaYGWCE1_EOwB5ty4LV5piq58WIo_A',// Jiro's
		// 'AIzaSyBbUep2qTJOj4eI6lF294Fu6Sv5WK-JEvA', // JD's
		'AIzaSyBsFKxxkkugepI7DETjTA0BlZrJVeyRzME', // Tom Tong's
		// 'AIzaSyDSYHMelyurUAoz6DiyM6HEjFdjg2RbQyI', // Tom Tong's
		'AIzaSyCftrncjafmT1NCabvd0R-fHj-3Tqnk-V8', // Tom Tong's 2nd key
		'AIzaSyAdiX3IG6HthATU9zf1tfl--E--3d0E7E4', // Tom Tong's 3rd key
		// 'AIzaSyA7ioHUCvf56kfnyrdESQ5g0eu3ryftH6Q', // Tom Tong's 3rd key
		'AIzaSyDTSLyYeovFCgNaIhBDcrd6qqxAfCVQecc', // Tom Tong's 4th key
		// 'AIzaSyDzCP3SPXOAs2-40Ezt_cWTwCjLwF8mAmY', // Tom Tong's 4th key
		// 'AIzaSyBWTfABVg56b_IfYMH0W8Xx7gkiMwNqYOY', // Omar (Jiro's friend)
		// 'AIzaSyByGEoVzj7i6hrIQFhe-CcsP_9Y0NwY2f0', // lifestyledlistings@gmail
		// 'AIzaSyCwksr2EPQr2UTd0UowCmiE0lPNLz5WRB0', // teamwfo - Bennett
		// 'AIzaSyCIRoNiD_fQ2LYzgx-NEXw4snFtRKvsMG8', // ditto
		// 'AIzaSyAkmMgHoQH20FUR929Zqtw_NHaCd6Oqmu0', // jiro@lifestyledlistings.com
		// 'AIzaSyBQKOOaKiUtVfKUV5-pY7KrPl7EF9Go58k' // ditto
		'AIzaSyAkgHhmp2qpoJsZAVLIVfANDfvTFTn-QCk',	// Tom Tong's 
		'AIzaSyBpJITf3EKisv9PT0cu_R16a8WcAmTR89A',	// Tom Tong's
		);

	public function __construct($logIt) {
		parent::__construct($logIt, 'geoplaces');
		require_once(__DIR__.'/Options.class.php'); $this->Options = new Options();
		$this->apiIndex = 0;
		$this->timeToReset = 20; // in hours, this will guarantee at least one day cycle between feed parsing
		$this->startTime = microtime(true);
		$this->lockfile = null;
		$this->lockfile = fopen(__DIR__.'/_logs/geoCode.lock', 'c'); 

		$q = new \stdClass();
		$q->where = array('opt' => 'GeoCodePlacesKeyIndex');
		$x = $this->Options->get($q);
		if (!empty($x)) {
			$data = json_decode($x[0]->value);
			$time = microtime(true);
			// time in seconds in float format.
			// 3600 secs/hr
			if ( ($time - floatval($data->time)) > (3600 * $this->timeToReset) )
				$this->apiIndex = 0;
			else
				$this->apiIndex = intval($data->index);
		}
		$this->recordIndex();
	}

	public function __destruct() {
		if ($this->lockfile);
			fclose($this->lockfile);
	}

	private function recordIndex() {
		//require_once(__DIR__.'/Options.class.php'); $options = new Options();
		$q = new \stdClass();
		$q->where = array('opt' => 'GeoCodePlacesKeyIndex');
		$x = $this->Options->get($q);
		if (is_array($x)) // delete it
			$this->Options->delete($q->where);

		$data = array('index'=>(string)$this->apiIndex,
					  'time'=>microtime(true));
		$data = array('opt'=>'GeoCodePlacesKeyIndex',
					  'value' => json_encode($data));
		$this->Options->add((object)$data);
	}

	protected function makeOneCall($call, &$geocoded, &$lastTime) {
		try {
			$json = file_get_contents($call);
			if ( !empty($json) ) {
				$orig = $geocoded = json_decode($json);
				if (!empty($geocoded) &&
					gettype($geocoded) == 'object' &&
					property_exists($geocoded, 'status') ) {
					$lastTime->count++;
					$this->Options->set([(object)['where'=>['opt'=>'GeoCodePlacesTimeStamp'],
											'fields'=>['value'=>json_encode($lastTime)]]]);
				}
				else {
					$this->log("result from file_get_contents() is unexpected:".(!empty($orig) ? $orig : "N/A").", for $call");
					$geocoded = (object)['results'=>[],
						     'status'=>'REQUEST_DENIED'];
				}
			}
			else {
				$this->log("Got empty json for $call");
				$geocoded = (object)['results'=>[],
					     'status'=>'REQUEST_DENIED'];
			}
		}
		catch( \Exception $e) {
			$this->log("runGeocode caught an exception:".$e->getMessage());
			$geocoded = (object)['results'=>[],
					     'status'=>'REQUEST_DENIED'];
		}
	}

	protected function runGeocode($call, &$geocoded) {
		flock($this->lockfile, LOCK_EX);
		$lastTime = $this->Options->get((object)['where'=>['opt'=>'GeoCodePlacesTimeStamp']]);
		$lastTime = json_decode($lastTime[0]->value);
		$curTime = microtime(true);
		$done = false;
		if ( ($curTime-$lastTime->time) < 1.0 ) {
			if ( $lastTime->count < 5 ) { // 5 calls per sec limit
				$this->makeOneCall($call, $geocoded, $lastTime);
				$done = true;
			}
			else
				usleep( ($curTime-$lastTime->time+0.1) * 1000000); // sleep off whatever time we need to since last fifth call
				// time_nanosleep(0, ($curTime-$lastTime->time+0.01)*100000000 );
		}
		
		if (!$done) {
			$lastTime->count = 1;
			$lastTime->time = microtime(true);
			$this->makeOneCall($call, $geocoded, $lastTime);
		}

		flock($this->lockfile, LOCK_UN);
	}

	public function geoPlaceDetail() {
		if (func_num_args() !== 2) return new Out(0, 'geoPlaceDetail: invalid number of arguments'); else {
			$id = func_get_arg(0);
			$nthTry = func_get_arg(1);
			$nthTry = is_string($nthTry) ? intval($nthTry) : $nthTry;
			if ($nthTry == count($this->APIkey))
				return new Out(0,  (object)['status'=>'OVER_QUERY_LIMIT'] );
			
			$call = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='.$id;
			$call .= '&key='.$this->APIkey[$this->apiIndex];
			$geocoded = null;
			$this->runGeocode($call, $geocoded);
			if (property_exists($geocoded, 'status') &&
				($geocoded->status == 'OVER_QUERY_LIMIT' ||
				 $geocoded->status == 'REQUEST_DENIED')) {
				if ($this->apiIndex == (count($this->APIkey)-1) ) // out of keys
					$this->apiIndex = 0; // reset
				else 
					$this->apiIndex++;
				$this->recordIndex();
				return $this->geoPlaceDetail(func_get_arg(0), $nthTry+1);
			}

			if ($geocoded->status == 'OK') return new Out('OK', $geocoded->result);
			return new Out(0, $geocoded);
		}
	}

	/**
	 * Returns Geocoded result from Google Maps API
	 * @param  [string] func_get_arg(0) [address string to parse]
	 * @return [AH\Out] [status of Google response, result dataset]
	 */
	public function geocodeNearby(){ // requires address
		if (func_num_args() !== 2) return new Out(0, 'geocodeAddress: invalid number of arguments'); else {
			$packet = func_get_arg(0);
			$packet = (object)$packet; 
			if (!isset($packet->lng)) return new Out(0, 'lng not sent');
			if (!isset($packet->lat)) return new Out(0, 'lat not sent');
			if (!isset($packet->radius)) return new Out(0, 'radius not sent');
			if (!isset($packet->query)) return new Out(0, 'query not sent'); // query is an array of type and/or name
			$nthTry = func_get_arg(1);
			$nthTry = is_string($nthTry) ? intval($nthTry) : $nthTry;
			if ($nthTry == count($this->APIkey))
				return new Out(0,  (object)['status'=>'OVER_QUERY_LIMIT'] );
			// try the call without a key...
			$call = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='.$packet->lat.','.$packet->lng.'&rankby=distance';
			if (isset($packet->query['type']))
				$call .= '&type='.$packet->query['type'];
			if (isset($packet->query['name']))
				$call .= '&name='.rawurlencode($packet->query['name']);
			if (isset($packet->page_token) &&
				!empty($packet->page_token))
				$call .= '&pagetoken='.$packet->page_token;
			$call .= '&key='.$this->APIkey[$this->apiIndex];
			$geocoded = null;
			$this->runGeocode($call, $geocoded);
			
			if (property_exists($geocoded, 'status') &&
				($geocoded->status == 'OVER_QUERY_LIMIT' ||
				 $geocoded->status == 'REQUEST_DENIED')) {
				if ($this->apiIndex == (count($this->APIkey)-1) ) // out of keys
					$this->apiIndex = 0; // reset
				else 
					$this->apiIndex++;
				$this->recordIndex();
				return $this->geocodeNearby(func_get_arg(0), $nthTry+1);
			}

			if ($geocoded->status == 'OK') return new Out('OK', $geocoded);
			return new Out(0, $geocoded);
		}
	}

	
	/**
	 * Returns the distance between points as a float rounded to 2 decimal places
	 * @param  [float] $lat1 [latitude input 1]
	 * @param  [float] $lon1 [longitude input 1]
	 * @param  [float] $lat2 [latitude input 2]
	 * @param  [float] $lon2 [longitude input 2]
	 * @return [float]       [distance between points]
	 */
	public function getDistance($lat1, $lng1, $lat2, $lng2){
	  	$theta = $lng1 - $lng2;
	    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	    $dist = acos($dist);
	    $dist = rad2deg($dist);
	    return round($dist * 60 * 1.1515, 2);
	}

	//Returns Distance between two latlng objects using haversine formula
	public function distance($p1, $p2) {
	 	if (!$p1 || !$p2) 
	  		return 0;
		$R = 6371000; // Radius of the Earth in m
		$dLat = ($p2->lat - $p1->lat) * pi() / 180;
		$dLon = ($p2->lng - $p1->lng) * pi() / 180;
		$a = sin($dLat / 2) * sin($dLat / 2) +
			 cos($p1->lat * pi() / 180) * cos($p2->lat * pi() / 180) *
		 	 sin($dLon / 2) * sin($dLon / 2);
		$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
		$d = $R * $c;
		return $d;
	}

	public function getPhotoUrl($photo_reference) {
		return "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$photo_reference&key=$this->APIkey";
	}

	// public function detailSearch($id, $query = null){
	// 	$nearby = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/place/details/json?placeid='.$id.'&key='.$this->APIkey));
	// 	if ($nearby->status == 'OK'){
	// 		return $nearby->result;
	// 	} else return $nearby;
	// }

	public function nearbySearch($lat, $lng, $query = ['type'=>'point_of_interest'], $radius = 50000, &$defunct){
		$packet = (object)['lat'=>$lat,
							'lng'=>$lng,
							'query'=>$query,
							'radius'=>$radius];
		$results = [];
		$defunct = [];
		$callsMade = 0;
		while(true) {
			$nearby = $this->geocodeNearby($packet, 0);
			$callsMade++;
			if ($nearby->status == 'OK'){
				$nearby = $nearby->data;
				foreach ($nearby->results as $point){

					// check for closed business
					if ( isset($point->permanently_closed) &&
						 is_true($point->permanently_closed) ) {
						$this->log("disallowed: permanently_closed - $point->name ({$point->geometry->location->lng}, {$point->geometry->location->lat}) at $point->vicinity".(!empty($query['state']) ? ' '.$query['state'] : ''));
						$x = (object) [
									'place_id'=>$point->place_id,
									'name' => $point->name,
									'lat' => $point->geometry->location->lat,
									'lng' => $point->geometry->location->lng
									];
						$defunct[$x->place_id] = $x;
						continue;
					}

					$gotName = false;
                    // test for strings we must have in the name
                    if ( isset($query['mustHaveText']) && !empty($query['mustHaveText'])) {
                        $skip = true;
                        $name = strtolower($point->name);
                        foreach( $query['mustHaveText'] as $accept)
                            if ( strpos($name, $accept) !== false ) {
                                $skip = false;
								$gotName = true;
                                $this->log("allowed: mustHaveText:$accept - for $point->name at $point->vicinity".(!empty($query['state']) ? ' '.$query['state'] : ''));
                                break;
                        	}

                        if ($skip) {
                            $this->log("disallowed: mustHaveText:$accept - $point->name at $point->vicinity".(!empty($query['state']) ? ' '.$query['state'] : ''));
                            continue;
                        }
                    }

                    // test for strings we must have in the name
                    if ( !$gotName &&
                    	 isset($query['acceptText']) && !empty($query['acceptText'])) {
                        $name = strtolower($point->name);
                        foreach( $query['acceptText'] as $accept)
                            if ( strpos($name, $accept) !== false ) {
								$gotName = true;
                                $this->log("allowed: acceptText:$accept - for $point->name at $point->vicinity".(!empty($query['state']) ? ' '.$query['state'] : ''));
                                break;
                        	}
                     }

					// test for acceptable rating if defined
					if ( !$gotName &&
						isset($query['rating']) &&
						 !empty($query['rating']) &&
						 ((isset($point->rating) &&
						   floatval($point->rating) < $query['rating'])   ||
						  !isset($point->rating)) ) {
						$this->log("disallowed: rating - limit:{$query['rating']}, got:".(isset($point->rating) ? $point->rating : "none")." - $point->name at $point->vicinity".(!empty($query['state']) ? ' '.$query['state'] : ''));
						continue;												
					}


					// test for icon to avoid
					if ( isset($query['avoidIcon']) && !empty($query['avoidIcon']) ) {
						$skip = false;
						foreach( $query['avoidIcon'] as $avoid)
							if ( strpos( $point->icon, $avoid) !== false ) {
								$skip = true;
								$this->log("disallowed: avoidIcon:$avoid - $point->name at $point->vicinity".(!empty($query['state']) ? ' '.$query['state'] : '').", icon:$point->icon");
								break;
							}
						if ($skip)
							continue;
					}

					// test for must have icon name
					if ( !$gotName &&
						isset($query['icon']) && !empty($query['icon']) &&
						 strpos( $point->icon, $query['icon']) === false ) {
						$this->log("disallowed: icon - $point->name at $point->vicinity".(!empty($query['state']) ? ' '.$query['state'] : '').", icon:$point->icon");
						continue;
					}

					// check for text to avoid in name
					if ( isset($query['avoidText']) && !empty($query['avoidText'])) {
						$skip = false;
						$name = strtolower($point->name);
						foreach($query['avoidText'] as $avoid) {
							try {
								if ( gettype($avoid) != 'string') {
									$this->log("WARNING: avoidText is not a string, it is ".gettype($avoid).":".print_r($avoid, true));
									continue;
								}
								if ( strpos( $name, $avoid) !== false) {
									if ( isset($query['icon']) && !empty($query['icon']) &&
								 		 strpos( $point->icon, $query['icon']) !== false )
										$this->log("allowed: avoidText:$avoid - $point->name at $point->vicinity".(!empty($query['state']) ? ' '.$query['state'] : '').", icon:$point->icon");
									else {
										$skip = true;
										$this->log("disallowed: avoidText:$avoid - $point->name at $point->vicinity".(!empty($query['state']) ? ' '.$query['state'] : ''));
										break;
									}
								}
							}
							catch(\Exception $e) {
								$this->log("avoidText exception for '$avoid':".$e->getMessage());
							}
						}
						if ($skip)
							continue;
					}
					
					$x = (object) [
						'address'=> $point->vicinity.(!empty($query['state']) ? ' '.$query['state'] : ''),
						'distance' => $this->getDistance(floatval($lat), floatval($lng), $point->geometry->location->lat, $point->geometry->location->lng),
						'place_id'=>$point->place_id,
						'icon' => $point->icon,
						'name' => $point->name,
						'photo' => isset($point->photos) && count($point->photos) ? $point->photos[0]->photo_reference : '',
						'types' => $point->types,
						'rating' => isset($point->rating) ? $point->rating : -1,
						'lat' => $point->geometry->location->lat,
						'lng' => $point->geometry->location->lng,
						'category' => $query['cat']
					];
					if ( ($dist = $this->distance($packet, $x)) > $radius) {
						unset($x);
						$this->log("disallowed: radius:$radius, dist:$dist, $x->name - $x->address, lng:$x->lng, lat:$x->lat, placeid:$x->place_id, cat:$x->category");
						continue;
					}

					$results[$point->place_id] = $x;
					$this->log("added: $x->name - $x->address, lng:$x->lng, lat:$x->lat, rating:$x->rating, icon:$x->icon, placeid:$x->place_id, cat:$x->category");
				}
				if ( isset($nearby->next_page_token) ) {
					$packet->page_token = $nearby->next_page_token;
					sleep(2); // stall for a moment
				}
				else
					break; // done
			}
			else
				break;
		}

		$this->log("nearbySearch - callsMade:$callsMade, type:{$query['type']} found:".count($results));
		return new Out(count($results) ? 'OK': 'fail', $results);
	}
}
