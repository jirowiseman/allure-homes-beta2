<?php
namespace AH;
require_once(__DIR__.'/_Base.class.php');
require_once(__DIR__.'/Utility.class.php');
require_once(__DIR__.'/Options.class.php'); 
require_once(__DIR__.'/BingLocation.class.php'); 

class Listings extends Base {
	// comes from Base->Logger now
	// public $timezone_adjust = -7;

	public function __construct($logIt = 0) {
		parent::__construct($logIt);
		// $this->log_to_file = $logIt;
		// $this->logFile = $this->log_to_file ? new Log(__DIR__.'/_logs/Listing.class.log') : null;
		$this->listingsLogGeoCodeDetail = false;
		$Options = new Options();
		$opt = $Options->get((object)['where'=>['opt'=>'ListingsLogGeoCodeDetail']]);
		if ($opt) $this->listingsLogGeoCodeDetail = intval($opt[0]->value);
		$this->BingLocation = new BingLocation();
	}

	public function __destruct() {
		if ($this->log !== null)
			$this->log->writeStringToFile();
	}
	
	// todo: get other info using with instead of multiple queries
	// public function get($in = null){
	// 	if (empty($in)) $in = new \stdClass();
	// 	if (empty($in->with)) $in->with = array();
	// 	$in->with[] = array('table'=>'tags-taxonomy','as'=>array('id'=>'tag_id'));
	// }
	// public function set($in = null){
	// 	require_once(__DIR__.'/ListingsActivity.class.php');
	// 	$Activity = new ListingsActivity();
	// 	$out = array( 'set-listing'=>parent::set($in), 'add-activity'=>array());
	// 	foreach ($in as $a) $out['add-activity'][] = $Activity->add(array('listing_id'=>$a->where['id'],'author'=>wp_get_current_user()->ID,'type'=>'listings-set','value'=>$a->fields));
	// 	return $out;
	// }

	protected function nonConformingLocation($loc) {
		if (empty($loc))
			return false;

		$loc = strtolower($loc);
		if ( ($pos = strpos($loc, "out of")) !== false ||
			 ($pos = strpos($loc, "outside")) !== false ||
			 ($pos = strpos($loc, "other")) !== false ||
			 ($pos = strpos($loc, "unincorporated")) !== false ||
			 ($pos = strpos($loc, "foreign")) !== false ||
			 ($pos = strpos($loc, "n/a")) !== false ||
			 ($pos = strpos($loc, "koh chang")) !== false) {
			return true;
		}
		return false;
	}

	protected function stripHyphenatedStreetAddress($addr) {
		$pieces = explode(" ", $addr);
		$pos = -1;
		if ( ($pos = strpos($pieces[0], '-')) !== false )
			$pieces[0] = substr($pieces[0], $pos+1);

		return implode(' ', $pieces);
	}

	public function geocodeListing(&$GoogleLocation, &$listing, &$google, $Cities = null) {
		$street_address = '';
		$tryBing = false;
		$formatted_address = $listing->street_address.', '.$listing->city.', '.$listing->state.' '.$listing->zip;

		if (!$Cities) {
			require_once(__DIR__.'/Cities.class.php'); 
			$Cities = new Cities();
		}

		$city = $Cities->get((object)['where'=>['city'=>$listing->city,
												'state'=>$listing->state]]);
		$doWithoutCity = (empty($city) || $city[0]->lat == -1) && !empty($listing->zip) && !empty($listing->state);
		$this->log("geocodeListing for listing:$listing->id, doWithoutCity:".($doWithoutCity ? 'yes' : 'no'));
		try {
			if (!$this->googleGeoCodeListing($GoogleLocation, $listing, $google, $street_address, $doWithoutCity) )
				$tryBing = true;
		}
		catch( \Exception $e) {
			$this->log("Exception for $formatted_address - googleGeoCodeListing:".$e->getMessage());
			$tryBing = true;
		}

		if ($tryBing) {
			if (!empty($listing->state) &&
				!$this->nonConformingLocation($listing->city) &&
				!$this->nonConformingLocation($listing->state) &&
				!$this->nonConformingLocation($listing->country) ) {
				$zip = !empty($listing->zip) ? ( ($pos = strpos($listing->zip, '-') !== false) ? substr($listing->zip, 0, $pos) : $listing->zip ) : '';
				$zip = !empty($zip) ? str_pad($zip, 5, '0', STR_PAD_LEFT) : '';
				if ( strlen($zip) <= 5 && // pass over Canadian zip like L43 07D
					 intval($zip) == 0 )
					$zip = '';
				$location = (!empty($listing->country) ? $listing->country.'/' : 'US/').$listing->state.'/'.(!empty($zip) ? $zip.'/' : '').rawurlencode($listing->city).'/'.rawurlencode($street_address);
				$bing = (object)[ 'query' => $location ];
				
				$this->log("geocodeListing trying BingLocation with $street_address, $listing->city, $listing->state $zip");
				$bing->result = $this->BingLocation->geocodeAddress($bing->query, 0);
				if ( $bing->result->status == 'OK' &&
					 !empty($bing->result->data)) {

					if ( $bing->result->data[0]->confidence != 'High' &&
						 $bing->result->data[0]->confidence != 'Medium') {
						$this->log("geocodeListing - BingLocation confidence:{$bing->result->data[0]->confidence}");
						return false;
					}

					$coords = $bing->result->data[0]->point->coordinates;
					if ( $bing->result->data[0]->confidence == 'High')
						$formatted_address = $bing->result->data[0]->address->formattedAddress;
					elseif ( !isset( $bing->result->data[0]->address->postalCode) &&
							 $bing->result->data[0]->entityType == 'PopulatedPlace' ) // we'll take it..
						$this->log("geocodeListing - BingLocation: no zip but entityType == PopulatedPlace with Medium confidence level");
					elseif ( $bing->result->data[0]->address->postalCode != $zip) {
						$this->log("geocodeListing - BingLocation zip mismatch {$bing->result->data[0]->address->postalCode} vs $zip, data:".print_r($bing->result->data, true));
						return false;
					}

					$this->log("BingLocation succeeded - confidence:{$bing->result->data[0]->confidence } for $formatted_address, ".print_r($coords, true));
					$listing->result = (object)[
						'status' => 'accepted',
						'query' => (object)[ 'lat' => $coords[0], 'lng' => $coords[1], 'address' => $formatted_address, 'listing_id' => $listing->id ],
						'city'=> 0,
						'street_address' =>0,
						'state' => 0
					];
					return true;
				}
				else
					$this->log("geocodeListing - BingLocation failed");
			}
		}
		else
			return true;

		return false;
	}

	protected function googleGeoCodeListing(&$GoogleLocation, &$listing, &$google, &$street_address, $retryWithZipNoCity = false) {
		$beforeMod = $listing->street_address;
		$listing->street_address = $this->stripHyphenatedStreetAddress($listing->street_address);
		$listing->street_address = fixCompass($listing->street_address);
		$listing->street_address = formatName($listing->street_address);
		$street_address = $listing->street_address;

		$badCity = 0;
		$city = strtolower($listing->city);
		if ($this->nonConformingLocation($city)) {
			$listing->result = new \stdClass();
			$listing->result->status = 'non-conformance';
			$badCity = 1;
		}

		$badState = 0;
		$state = strtolower($listing->state);
		if ($this->nonConformingLocation($state)) {
			$listing->result = new \stdClass();
			$listing->result->status = 'non-conformance';
			$badState = 2;
		}

		$country = !empty($listing->country) ? strtolower($listing->country) : '';
		if ($this->nonConformingLocation($country)) {
			if (!$badState && !$badCity && !empty($listing->zip)) // then give it a try
				$country = '';
			else {
				$listing->result = new \stdClass();
				$listing->result->status = 'non-conformance';
				return false;
			}
		}

		if ($badCity && $badState && empty($listing->zip))
			return false;


		if ( ($pos = strpos($street_address, " and ")) !== false)
			$street_address = substr($street_address, $pos+5); // there an 'and' in the address, so take everything behind that.

		if ( ($pos = strpos($street_address, ",")) !== false)
			$street_address = substr($street_address, 0, $pos);
		elseif ( ($pos = strpos($street_address, "Apt.")) !== false)
			$street_address = substr($street_address, 0, $pos);
		elseif ( ($pos = strpos($street_address, "Apt ")) !== false)
			$street_address = substr($street_address, 0, $pos);
		elseif ( ($pos = strpos($street_address, "Unit")) !== false)
			$street_address = substr($street_address, 0, $pos);
		elseif ( ($pos = strpos($street_address, "Suite")) !== false)
			$street_address = substr($street_address, 0, $pos); 
		elseif ( ($pos = strpos($street_address, "Ste ")) !== false)
			$street_address = substr($street_address, 0, $pos);
		elseif ( ($pos = strpos($street_address, "Ste. ")) !== false)
			$street_address = substr($street_address, 0, $pos);

		// if ( ($pos = strrpos($street_address, "#")) !== false) {
		// 	// check if this '#' is on the last word of the address, if so, strip it all off
		// 	if ( strpos(substr($street_address, $pos), ' ') === false )
		// 		$street_address = substr($street_address, 0, $pos);
		// 	else // just replace it with a blank and hope for the best
		// 		$street_address = str_replace("#", "", $street_address);
		// }
		$haveHash = false;
		$preStripHashAddress = $street_address;
		if ( ($pos = strrpos($street_address, "#")) !== false) {
			// strip it all off
			$haveHash = true;
			$street_address = substr($street_address, 0, $pos);
		}

		if ( ($pos = strpos($street_address, "0 ")) !== false &&
			  $pos === 0)
			$street_address = substr($street_address, $pos+2); // address with '0' only start, strip that off

		// strip out and '(....)'
		if ( ($pos = strrpos($street_address, "(")) !== false) {
			$pos2 = strrpos($street_address, ")");
			$street_address = substr($street_address, 0, $pos).( $pos2 !== false && ($pos2+1) < strlen($street_address) ?  ' '.substr($street_address, $pos2+1) : '');
			$street_address = trim($street_address);
			$this->log("geocodeListing - stripped of (), now: $street_address");
		}

		$saved_street_address = $listing->street_address;
		$listing->street_address = $street_address;
		$cleanedUpAddress = $street_address;

		// make street_number and route elements
		$pieces = explode(' ', $cleanedUpAddress);
		$first = true;
		foreach($pieces as $i=>$word) {
			if ($this->listingsLogGeoCodeDetail) $this->log("geocodeListing - pieces:$i - $word");
			switch($i) {
				case 0: $listing->street_number = $word; break;
				default:
					if ($word == ' ')
						break;
					if (!isset($listing->route))
						$listing->route = '';
					if (!$first) $listing->route .= ' ';
					$listing->route .= $word;
					$first = false;
			}
		}

		if ($this->listingsLogGeoCodeDetail) $this->log("geocodeListing - street_number:".$listing->street_number.", route:".$listing->route.", pieces:".print_r($pieces, true));

		$city = $listing->city;
		if ( ($pos = strpos($city, ",")) !== false) {
			$pieces = explode(',', $city);
			$city = '';
			foreach($pieces as $piece)
				if (strlen($piece) > strlen($city)) // let's go with the longest piece
					$city = $piece;

			$listing->city = $city;
		}


		
		$listing->state = fixNY($listing->city, $listing->state);
		$listing->city = fixCity($listing->city, $listing->state);
		$listing->city = ucwords(fixCityStr($listing->city));

		if ($badCity)
			$listing->city = '';
		if ($badState)
			$listing->state = '';

		$zip = !empty($listing->zip) ? ( ($pos = strpos($listing->zip, '-') !== false) ? substr($listing->zip, 0, $pos) : $listing->zip ) : '';
		if ( strlen($zip) <= 5 && // pass over Canadian zip like L43 07D
			 intval($zip) == 0 )
			$zip = '';

		$google = (object)[ 'query' => $street_address . (!$retryWithZipNoCity && !empty($listing->city) ? ', ' . $listing->city : '') . (!empty($listing->state) ? ', '.$listing->state : ''). (!empty($zip) ? ', ' .$zip: '') . ($listing->country ? ', ' .$listing->country : '') ];
		$this->log("geoAddress query for listing:$listing->id is $google->query");

		$google->result = $GoogleLocation->geocodeAddress( $google->query, 1 );
		if ($google->result->data->status == 'OVER_QUERY_LIMIT') throw new \Exception("Over daily geocoding limit. Aborting.");
		else if ($google->result->data->status == 'ZERO_RESULTS' ||
				 $google->result->data->status == 'REQUEST_DENIED'||
				 $google->result->data->status == 'INVALID_REQUEST' ||
				 $google->result->data->status == 'UNKNOWN_ERROR') {
			if (!$retryWithZipNoCity &&
				!empty($zip)) {
				$this->log("retrying googleGeoCodeListing with no city name:$listing->city");
				return $this->googleGeoCodeListing($GoogleLocation, $listing, $google, $street_address, true);
			}

			$listing->result = (object)[
								'query' => (object)[ 'lat' => -1, 'lng' => -1, 'listing_id' => $listing->id ],
								'data' => $google->result->data,
							];
			$listing->result->status = $google->result->data->status == 'ZERO_RESULTS' ? 'rejected' : 
									   $google->result->data->status == 'REQUEST_DENIED' ? 'denied' : 'error';		
			$listing->street_address = $saved_street_address;

			$log = "geocodeListing - Failed: ".$google->result->data->status.", id:$listing->id, listing: $listing->listhub_key, beforeMod:$beforeMod, cleanedUp:".$saved_street_address.", mod:".$street_address;
			$this->log($log, true);
		
			return false;
		}
		
		$result = $google->result->data;
		// check postal_code first.. if that matches, then we should really be good to go...
		$zipMatches = false;
		$havePostalCode = false;
		if (isset($result->address_components) && !empty($result->address_components) ){
			foreach ($result->address_components as &$address_component){
				if (in_array('postal_code', $address_component->types) ) {
					$this->log("geocodeListing - postal_code:$address_component->long_name, zip:$zip");
					$havePostalCode = true;
					if ( $address_component->long_name == $zip) 
						$zipMatches = true;
					break;
				}
			}
		}

		$this->log("geocodeListing - results has havePostalCode:".($havePostalCode ? 'yes' : 'no').", partial_match:".(empty($result->partial_match) ? 'no' : 'yes')." type:".print_r($result->types, true));

		// if (empty($result->partial_match) && in_array('street_address', $result->types)){
		if (($zipMatches || empty($result->partial_match)) && 
			(in_array('street_address', $result->types) || 
			 in_array('natural_feature', $result->types) || 
			 in_array('postal_code', $result->types) || 
			 in_array('locality', $result->types) || 
			 in_array('sublocality', $result->types) ||
			 in_array('neighborhood', $result->types) ||
			 in_array('premise', $result->types) ||
			 in_array('subpremise', $result->types) ||
			 in_array('route', $result->types)) ){
			if ($this->listingsLogGeoCodeDetail) $this->log("geocodeListing - zip matched: $zip");
			$listing->result = (object)[
				'status' => 'accepted',
				'query' => (object) [
					'lat' => $result->geometry->location->lat,
					'lng' => $result->geometry->location->lng,
					'address' => in_array('street_address', $result->types) ? $result->formatted_address : $listing->street_address.', '.$listing->city.', '.$listing->state.' '.$zip,
					'listing_id' => $listing->id,
				],
				'city' => '0',
				'street_address' => '0',
				'state' => '0'
			];
			if ($badCity || $badState) {
				if (isset($result->address_components) && !empty($result->address_components) ){
					foreach ($result->address_components as &$address_component){
						if (in_array('locality', $address_component->types) && $badCity) {
							$listing->city = $address_component->short_name;
						}
						if (in_array('administrative_area_level_1', $address_component->types) && $badState) {
							$listing->state = $address_component->short_name;
						}
					}
				}
				$listing->update = $badCity | $badState;
			}
		} elseif (isset($result->address_components) && !empty($result->address_components) ){
			$resultHasStreetAddress = false;
			if ($this->listingsLogGeoCodeDetail) $this->log("geocodeListing - address_components:".print_r($result->address_components, true));
			foreach ($result->address_components as &$address_component){
				if (in_array('street_address', $address_component->types))
					$resultHasStreetAddress = true;
				if ($this->listingsLogGeoCodeDetail) {
					if (in_array('street_number', $address_component->types))
						$this->log("geocodeListing - listing:".$listing->street_number.", street_number short:".$address_component->short_name.", long:".$address_component->long_name);
					if (in_array('route', $address_component->types))
						$this->log("geocodeListing - listing:".$listing->route.", route short:".$address_component->short_name.", long:".$address_component->long_name);
				}

				$address_component->distance = new \stdClass();
				foreach (['city','street_address','street_number','route','state'] as &$address_type){
					$short = levenshtein($address_component->short_name, $listing->$address_type);
					$long = levenshtein($address_component->long_name, $listing->$address_type);
					$address_component->distance->$address_type = $short <= $long ? $short : $long;
					unset($short, $long, $address_type);
				}
			}
			$listing->result = (object)[
				'city'=>(object)[
					'distance' => 99,
					'name' => null
				],
				'street_address'=>(object)[
					'distance' => 99,
					'name' => null
				],
				'street_number'=>(object)[
					'distance' => 99,
					'name' => null
				],
				'route'=>(object)[
					'distance' => 99,
					'name' => null
				],
				'state'=>(object)[
					'distance' => 99,
					'name' => null
				]
			];
			foreach ($result->address_components as &$address_component)
				foreach (['city','street_address','street_number','route','state'] as $address_type)
					if ($address_component->distance->$address_type < $listing->result->$address_type->distance)
						$listing->result->$address_type = (object)[ 'distance' => $address_component->distance->$address_type, 'name' => $address_component->long_name ];
			unset($address_component, $address_type);

			if ($listing->result->city->distance <= 3 && 
				(($resultHasStreetAddress && $listing->result->street_address->distance <= 3) ||
				 ($listing->result->street_number->distance < 2 &&
				  $listing->result->route->distance <= 3)) &&
				$listing->result->state->distance == 0 ) {// check string similarity
				if ($this->listingsLogGeoCodeDetail) $this->log("geocodeListing - resultHasStreetAddress:".($resultHasStreetAddress ? 'yes' : 'no').", found geocode.");
				if (!$resultHasStreetAddress)
					if ($this->listingsLogGeoCodeDetail) $this->log("geocodeListing - street_number:".$listing->result->street_number->distance.", route:".$listing->result->route->distance);
				$cityDistance = $listing->result->city->distance;
				$streetDistance = $resultHasStreetAddress ? $listing->result->street_address->distance : $listing->result->street_number->distance + $listing->result->route->distance;
				$stateDistance = $listing->result->state->distance;
				$listing->result = (object)[
					'status' => 'accepted',
					'query' => (object)[ 'lat' => $result->geometry->location->lat, 'lng' => $result->geometry->location->lng, 'address' => $result->formatted_address, 'listing_id' => $listing->id ],
					'city'=> $cityDistance,
					'street_address' =>$streetDistance,
					'state' => $stateDistance
				];
			}
			else
				$listing->result = (object)[
					'status' => 'out-of-bounds',
					'query' => (object)[ 'lat' => -1, 'lng' => -1, 'listing_id' => $listing->id ],
					'data' => $google,
				];
		}
		else $listing->result = (object)[
			'status' => 'unlocated',
			'query' => (object)[ 'lat' => -1, 'lng' => -1, 'listing_id' => $listing->id ],
			'data' => $google,
		];

		$listing->street_address = $saved_street_address;

		$log = "geocodeListing - id:$listing->id, listing: $listing->listhub_key, beforeMod:$beforeMod, zipMatches:".($zipMatches ? 'yes' : 'no')." resultHasStreetAddress:".($resultHasStreetAddress ? 'yes' : 'no')." ,cleanedUp:".$cleanedUpAddress.",".$listing->city.",".$listing->state;
		$log .= ", google returned:".$result->formatted_address;
		$log .= ", status:".$listing->result->status;
		$log .= ", lng:".$listing->result->query->lng;
		$log .= " / lat:".$listing->result->query->lat;
		$log .= ", city distance:".(isset($listing->result->city) ? $listing->result->city : "N/A");
		$log .= ", street distance:".(isset($listing->result->street_address) ? $listing->result->street_address : "N/A");
		$log .= ", state distance:".(isset($listing->result->state) ? $listing->result->state : "N/A");
		$this->log($log, true);

		return $listing->result->status == 'accepted' ?  true : false;
	}

	public function geocodeListings($page = 0, $per_page = 5){
		try {
			require_once(__DIR__.'/GoogleLocation.class.php');
			require_once(__DIR__.'/ListingsGeoinfo.class.php');
			$GoogleLocation = new GoogleLocation();
			$GeoInfo = new ListingsGeoinfo();

			$listings_without_geoinfo = $this->rawQuery("SELECT id, street_address, city, state, country FROM {$this->getTableName($this->table)} WHERE active = 1 AND street_address IS NOT NULL AND city IS NOT NULL AND state IS NOT NULL AND NOT EXISTS (SELECT 1 FROM {$this->getTableName('listings-geoinfo')} WHERE {$this->getTableName('listings-geoinfo')}.listing_id = {$this->getTableName($this->table)}.id) LIMIT ".($page).", $per_page");

			if (!empty($listings_without_geoinfo))
				foreach ($listings_without_geoinfo as &$row){
					$google = null;
					$this->geocodeListing($GoogleLocation, $row, $google);
				}
				unset($row);

			$results = [];
			if (!empty($listings_without_geoinfo)) foreach ($listings_without_geoinfo as &$row){
				if (isset($row->result) && isset($row->result->status)){
					if ($row->result->status == 'accepted' || $row->result->status == 'rejected')
						$results[] = (object)[
							'query' => $row->result,
							'result' => $GeoInfo->add($row->result->query),
						];
				}
			}

			return empty($results) ? false : $results;
		} catch (\Exception $e) { parseException($e); die(); }
	}
	public function countListingsWithoutGeoinfo(){
		$sql = "SELECT 1 FROM {$this->getTableName($this->table)} WHERE active = 1 AND street_address IS NOT NULL AND city IS NOT NULL AND state IS NOT NULL AND NOT EXISTS (SELECT 1 FROM {$this->getTableName('listings-geoinfo')} WHERE {$this->getTableName('listings-geoinfo')}.listing_id = {$this->getTableName($this->table)}.id)";
		return intval($this->wpdb->query($sql));
	}

	public function resetImageOrigins($page, $pagePer) {
		require_once(__DIR__.'/Image.class.php'); 
		$Image = new Image();

		$this->log("resetImageOrigins - entered for page:$page, pagePer:$pagePer");

		$row = 0;
		$count = 0;
		$processed = 0;
		if ( !empty($listings = $this->get((object)['where'=>['active'=>1],
													  'limit'=>$pagePer,
													  'page'=>$page])) ) {
			$row = $page * $pagePer;
			$this->log("resetImageOrigins - $page, $row - listingCount:".count($listings));
			foreach($listings as $l) {
				$count++;
				if (!empty($l->images)) {
					$processed++;
					$needUpdate = false;
					$imageCount = 0;
					foreach($l->images as &$img) {				
						if (isset($img->file) && substr($img->file, 0, 4 ) != 'http') {  // then a static image exists for this 
							if (isset($img->url)) { // otherwise leave it alone!
								$Image->unlinkListingImage($img); // remove exiting image set
								$img->file = $img->url;
								unset($img->url);
								if (isset($img->processed)) unset($img->processed); 
								if (isset($img->width)) unset($img->width); 
								if (isset($img->height)) unset($img->height); 
								$needUpdate = true; 
								$imageCount++;
							}
						}
					}
					if ($needUpdate) {
						$x = $this->set([(object)['where'=>['id'=>$l->id],
											 	  'fields'=>['images'=>$l->images]]]);
						if (empty($x))
							$this->log("resetImageOrigins - failed to update listing:$l->id with reset images array");
						else
							$this->log("resetImageOrigins - updated $imageCount images out of ".count($l->images)." for listing:$l->id");
					}
				}
				unset($l);
				$this->flush();
			}
			unset($listings);
			return new Out('OK', "Total listings: $count, updated: $processed");
		}
		else
			return new Out('fail', "No listings on page:$page with pagerPer:$pagePer");
	}

	public function setFirstImage($page, $pagePer) {
		require_once(__DIR__.'/Image.class.php'); 
		$Image = new Image();

		$this->log("setFirstImage - entered for page:$page, pagePer:$pagePer");

		$row = 0;
		$count = 0;
		$processed = 0;
		try {
			$listings = $this->get((object)['where'=>['active'=>1],
													'what'=>['id', 'images', 'first_image'],
													  'limit'=>$pagePer,
													  'page'=>$page]);
		}
		catch( \Exception $e) {
			$this->log("setFirstImage - caught Exception:".$e->getMessage());
			return new Out('fail', "caught exception:".$e->getMessage());
		}

		if ( !empty($listings) ) {
			$row = $page * $pagePer;
			$this->log("setFirstImage - $page, $row - listingCount:".count($listings));
			$imgPath = realpath(__DIR__.'/../_img/_listings/845x350');
			foreach($listings as $l) {
				$count++;
				if (!empty($l->images) &&
					empty($l->first_image)) {
					$processed++;
					$needUpdate = false;
					$imageCount = 0;
					$first_image = null;
					$haveFile = false;
					foreach($l->images as $img) {				
						if (!empty($img->file)) { // can be a discard
							$isHttp = substr($img->file, 0, 4 ) == 'http';
							if ($isHttp) {
								$first_image = $img;
								$haveFile = true;
							}
							elseif (!file_exists($imgPath.'/'.$img->file)) {
								if (isset($img->url)) {
									$img->file = $img->url; // revert to url...
									$first_image = $img;
									$haveFile = true;
								}
							}
							else { // then this is a processed image
								$first_image = $img;
								$haveFile = true;
							}
							if ($haveFile)
								break;
						}
						unset($img);
					}
					if ($haveFile) {
						$x = $this->set([(object)['where'=>['id'=>$l->id],
											 	  'fields'=>['first_image'=>$first_image]]]);
						if (empty($x))
							$this->log("setFirstImage - failed to update listing:$l->id with first image");
						else
							$this->log("setFirstImage - updated first_image for listing:$l->id");
					}
				}
				unset($l);
				$this->flush();
			}
			unset($listings);
			return new Out('OK', "Total listings: $count, updated: $processed");
		}
		else
			return new Out('fail', "No listings on page:$page with pagerPer:$pagePer");
	}
}
