<?php
namespace AH;
require_once(__DIR__.'/_Base.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php');

class MedianPrices extends Base {
	// comes from Base->Logger now
	// global $timezone_adjust;
	// protected $timezone_adjust = $timezone_adjust;
	public function __construct($logIt = 0) {
		parent::__construct($logIt);
	}

	public function getActiveZipCount() {
		$sql = "SELECT COUNT( DISTINCT(zip) ) FROM {$this->getTableName('listings')} WHERE active = 1";
		$x = $this->rawQuery($sql);
		if (!empty($x)) {
			return new Out('OK', current((array)$x[0]) );
		}
		return new Out('fail', "Did not get a count");
	}

	public function resetDaily() {
		$page = 0;
		$pagePer = 1000;

		$reset = 0;
		while ( !empty($medians = $this->get((object)['limit'=>$pagePer,
													  'page'=>$page])) ) {
			foreach($medians as $median) {
				$fields = [];
				foreach(['home','condo'] as $target) {
					$counter = $target.'_count';
					$daily = 'daily_'.$target;
					if ($median->$counter != 0)
						$fields[$counter] = 0;
					if ($median->$daily != 0)
						$fields[$daily] = 0;
				}
				if (count($fields)) {
					$this->set([(object)['where'=>['id'=>$median->id],
										 'fields'=>$fields]]);
					$reset++;
				}
				unset($fields, $median);
			}
			unset($medians);
			$page++;
		}
		return new Out('OK', ['reset'=>$reset]);
	}

	protected function makeDecimal(&$median) {
		foreach(['median_home', 'median_condo', 'monthly_home', 'monthly_condo', 'daily_home', 'daily_condo'] as $target)
			$median->$target = floatval($median->$target);

		foreach(['home_count', 'condo_count'] as $target)
			$median->$target = intval($median->$target);
	} 

	public function consolidateDailyMedianPrices() {
		$sql = "SELECT * FROM {$this->getTableName()} WHERE (`home_count` > 0 OR `condo_count` > 0) ";
		$sqlNotSetYet = $sql."AND (`median_home` = ".DEFAULT_MEDIAN_HOME." OR `median_condo` = ".DEFAULT_MEDIAN_CONDO.")";
		$page = 0;
		$pagePer = 1000;

		// first look for medians that has default values, but we found something during the parse
		// then update the median_home/condo values so that they are more 'real'
		$assigned = 0;
		while ( !empty($medians = $this->rawQuery($sqlNotSetYet." LIMIT ".($page*$pagePer).", $pagePer")) ){
			foreach($medians as $median) {
				$fields = [];
				$this->makeDecimal($median);

				if ($median->home_count &&
					$median->median_home == DEFAULT_MEDIAN_HOME)
					$fields['median_home'] = $median->daily_home;
				if ($median->condo_count &&
					$median->median_condo == DEFAULT_MEDIAN_CONDO)
					$fields['median_condo'] = $median->daily_condo;
				if (count($fields)) {
					$this->set([(object)['where'=>['id'=>$median->id],
										 'fields'=>$fields]]);
					$msg = "consolidateDailyMedianPrices - assigned new median values to $median->id with zip:$median->zip";
					$msg.= ", home:".( $median->home_count && $median->median_home == DEFAULT_MEDIAN_HOME ? $median->daily_home : 'not today');
					$msg.= ", condo:".( $median->condo_count && $median->median_condo == DEFAULT_MEDIAN_CONDO ? $median->daily_condo : 'not today');
					$this->log($msg);
					$assigned++;
				}
				unset($fields, $median);
			}
			unset($medians);
			$page++;
		}

		// now that we have all zip codes out of default values, where possible
		// consolidate daily values into monthly and annualized (rolling )value
		$updated = 0;
		$page = 0;
		while ( !empty($medians = $this->rawQuery($sql." LIMIT ".($page*$pagePer).", $pagePer")) ){
			foreach($medians as $median) {
				$fields = [];
				$this->makeDecimal($median);

				foreach(['home','condo'] as $target) {
					$counter = $target.'_count';
					$annual = 'median_'.$target;
					$monthly = 'monthly_'.$target;
					$daily = 'daily_'.$target;

					if ($median->$counter) {// if no activity, leave alone
						if ($target == 'condo' &&
							$median->$daily < ($target == 'home' ? DEFAULT_ROCK_BOTTOM_PRICE : DEFAULT_ROCK_BOTTOM_PRICE*0.60))  // disregard it, most likely rent price
							continue;

						$origAnnual = $median->$annual;
						$median->$annual = (($median->$annual*364) + $median->$daily)/365; // rolling average
						if ($origAnnual != $median->$annual)
							$fields[$annual] = $median->$annual;

						$origMonthly = $median->$monthly;
						if ( !empty($median->$monthly) )
							$median->$monthly = (($median->$monthly*29) + $median->$daily)/30; // rolling average
						else
							$median->$monthly = $median->$daily;
						if ($origMonthly != $median->$monthly)
							$fields[$monthly] = $median->$monthly;

						// NOTE!!!  
						// The $daily and $counter values are not zero'ed here.
						// This is purely to save this information until the next parse, incase it needs to be reviewed
						// $this->resetDaily() should be called before a new parse
					}
				}
				if (count($fields)) {
					$this->set([(object)['where'=>['id'=>$median->id],
										 'fields'=>$fields]]);
					$msg = "consolidateDailyMedianPrices - updated median values to $median->id with zip:$median->zip";
					$msg.= ", home:".( $median->home_count ? $median->median_home : 'not today');
					$msg.= ", condo:".( $median->condo_count ? $median->median_condo : 'not today');
					$this->log($msg);
					$updated++;
				}
				unset($fields, $median);				
			}
			unset($medians);
			$page++;
		}

		return new Out('OK', ['assigned'=>$assigned,
							  'updated'=>$updated]);
	}

	public function getMedianPrice($zip, $listing_price, $isHome = 1, $isRental = 0) { // $isHome == 0 means condo
		$zip = str_replace(" ", '', $zip); // take out spaces
		$zip = strpos($zip, "-") !== false ? explode("-", $zip)[0] : $zip; // get only first part of compound zip, 12345-1234
		$price = $this->get((object)['where'=>['zip'=>$zip]]);
		if (empty($price)) {
			$this->log("getMedianPrice - failed to find $zip, calling updateMedianPrice");
			$out = $this->updateMedianPrice($zip);
			if ($out['id'] == 0)
				return $out;

			$price = $this->get((object)['where'=>['id'=>$out['id']]]);
		}
		else {
			$out = ['median_home'=>$price[0]->median_home,
					'median_condo'=>$price[0]->median_condo,
					'api_count'=>0,
					'id'=>$price[0]->id
					];
		}

		$price = array_pop($price);
		$target = 'daily_'.($isHome ? 'home' : 'condo');
		$counter = ($isHome ? 'home' : 'condo').'_count';
		$now = time() + ($this->timezone_adjust*3600);
		$fields = [];
		$origCounter = $price->$counter;
		// update daily average for $target
		if ($price->$target != $listing_price) {
			if (!$isRental &&
				$listing_price > ($isHome ? DEFAULT_ROCK_BOTTOM_PRICE : DEFAULT_ROCK_BOTTOM_PRICE*0.60)) {
				$price->$target = (($price->$target * $price->$counter++) + $listing_price)/$price->$counter; // make new average
				$fields[$target] = $price->$target;
				$this->log("getMedianPrice = updating $zip - $target with {$price->$target}, counter:{$price->$counter}");
			}
			else
				$this->log("getMedianPrice - ignoring zip:$zip with listing_price:$listing_price for ".($isHome ? 'home' :'condo').", rental:".($isRental ? "yes" : "no"));
		}
		else // just increment counter, since if what we have is the same as incoming price, the monthly average won't change
			$price->$counter++;

		if ($origCounter != $price->$counter)
			$fields[$counter] = $price->$counter;

		if (count($fields))
			$this->set([(object)['where'=>['id'=>$price->id],
								 'fields'=>$fields]]);
		$out['city_id'] = $price->city_id;
		$out['id'] = $price->id;
		unset($fields, $price);

		return $out;
	}

	public function updateMedianPrices($page, $pagePer) {
		$this->log("updateMedianPrices - entered for page:$page, pagePer:$pagePer");
		$row = 0;
		$count = 0;
		$processed = 0;
		$sql = "SELECT city, state, zip, city_id FROM {$this->getTableName('listings')} WHERE active = 1 GROUP BY zip ";
		$sql.= "LIMIT ".($page*$pagePer).",$pagePer";

		if ( !empty($zips = $this->rawQuery($sql)) ) {
			$row = $page * $pagePer;
			$this->log("updateMedianPrices - $page, $row - zipCount:".count($zips));
			foreach($zips as $city) {
				$count++;
				
				$out = $this->updateMedianPrice($city->zip, $city->city_id);
				$processed += $out['api_count'];
			}
		}
		else
			return new Out('fail', "All done!");
		
		$this->log("updateMedianPrices - count:$count, apiProcessing:$processed for page:$page, pagePer:$pagePer");
		$this->flush();
		return new Out('OK', ['count'=>$count,
							  'api_count'=>$processed]);
	}

	public function updateMedianPrice($zip, $city_id = 0) {
		if (empty($zip) ||
			$zip == '00000')
			return ['median_home'=>DEFAULT_MEDIAN_HOME,
					'median_condo'=>DEFAULT_MEDIAN_CONDO,
					'api_count'=>0,
					'id'=>0
					];

		$zip = str_replace(" ", '', $zip); // take out spaces
		$zip = strpos($zip, "-") !== false ? explode("-", $zip)[0] : $zip; // get only first part of compound zip, 12345-1234
		$entry = $this->get((object)['where'=>['zip'=>$zip]]);
		$fields = [];
		$url1 = "https://www.quandl.com/api/v3/datasets/ZILL/Z{$zip}_MLP.csv";
		$url2 = "https://www.quandl.com/api/v3/datasets/ZILL/Z{$zip}_MSP.csv";
		$url3 = "https://www.quandl.com/api/v3/datasets/ZILL/Z{$zip}_C.csv";
		$row = 0; // id of entry
		$median_home = 0;
		$median_condo = 0;
		$home_empty = false;
		$condo_empty = false;
		$apiCount = 0;
		if (!empty($entry)) {
			$row = $entry[0]->id;
			$now = time();
			if (empty($entry[0]->median_home) ||
				(($now - strtotime($entry[0]->updated)) > MONTH_SECS) ) {
				$median = $this->fetchQuandl($url1, 'home-mlp', $zip); $apiCount++;
				$median2 = $this->fetchQuandl($url2, 'home-msp', $zip); $apiCount++;
				$home_empty = empty($median) && empty($median2);
				$median_home = $home_empty ? DEFAULT_MEDIAN_HOME : ($median+$median2)/((empty($median) || empty($median2) ? 1 : 2));
				$fields['median_home'] = $entry[0]->median_home = $median_home;
			}
			else
				$median_home = $entry[0]->median_home;

			// if (empty($entry[0]->median_condo) ) {
			// 	$fields['median_condo'] = $entry[0]->median_condo = $home_empty ? DEFAULT_MEDIAN_CONDO : $median_home*0.60;
			// }
			// else
			// 	$median_condo = $entry[0]->median_condo;

			$home60Percent = $median_home*0.60;

			if (empty($entry[0]->median_condo) ||
				(($now - strtotime($entry[0]->updated)) > MONTH_SECS) ) {
				$median = $this->fetchQuandl($url3, 'condo', $zip); $apiCount++;
				if ( ($condo_empty = empty($median)) ) {
					if ( $home_empty )
						$median_condo = DEFAULT_MEDIAN_CONDO;
					else
						$median_condo = $home60Percent;
				}
				elseif ( $median_condo > $home60Percent )
					$median_condo = $home60Percent;
				else
					$median_condo = ($home60Percent+$median_condo)/2;


				// $median_condo = $condo_empty ? ( $home_empty ? DEFAULT_MEDIAN_CONDO : $median_home*0.60 ) : $median;
				$fields['median_condo'] = $entry[0]->median_condo = $median_condo;
			}
			else
				$median_condo = $entry[0]->median_condo;

			if (empty($entry[0]->city_id) &&
				!empty($city_id) &&
				$city_id != -1)
				$fields['city_id'] = $city_id;

			if (count($fields)) {
				$this->set([(object)['where'=>['id'=>$entry[0]->id],
									 'fields'=>$fields]]);
				$this->log("updateMedianPrice - updated $zip with home:".$fields['median_home'].", condo:".$fields['median_condo']);
			}
			unset($entry);
		}
		else {
			$median = $this->fetchQuandl($url1, 'home', $zip); $apiCount++;
			$median2 = $this->fetchQuandl($url2, 'home', $zip); $apiCount++;
			$home_empty = empty($median) && empty($median2);
			$median_home = $home_empty ? DEFAULT_MEDIAN_HOME : ($median+$median2)/((empty($median) || empty($median2) ? 1 : 2));
			$fields['median_home'] = $median_home;

			$home60Percent = $median_home*0.60;
			$median = $this->fetchQuandl($url3, 'condo', $zip); $apiCount++;
			if ( ($condo_empty = empty($median)) ) {
				if ( $home_empty )
					$median_condo = DEFAULT_MEDIAN_CONDO;
				else
					$median_condo = $home60Percent;
			}
			elseif ( $median > $home60Percent )
				$median_condo = $home60Percent;
			else
				$median_condo = ($home60Percent+$median)/2;
			$fields['median_condo'] = $median_condo;
			// $fields['median_condo'] = $home_empty ? DEFAULT_MEDIAN_CONDO : $median_home*0.60;
			// $median_home = $this->fetchQuandl($url1, 'home', $zip);
			// $home_empty = empty($median_home);
			// $fields['median_home'] = $median_home = empty($median_home) ? DEFAULT_MEDIAN_HOME : $median_home;
			// $median_condo = $this->fetchQuandl($url2, 'condo', $zip);
			// $condo_empty = empty($median_condo);
			// $fields['median_condo'] = $median_condo = empty($median_condo) ? ( $home_empty ? DEFAULT_MEDIAN_CONDO : $median_home*0.60 ) : $median_condo;
			$fields['zip'] = $zip;
			if (!empty($city_id) &&
				$city_id != -1)
				$fields['city_id'] = $city_id;
			try {
				$row = $this->add($fields);
			}
			catch( \Exception $e) {
				$this->log("updateMedianPrice - caught exception when adding $zip - ".$e->getMessage());
			}
			$this->log("updateMedianPrice - added $zip with home:".$fields['median_home'].", condo:".$fields['median_condo'].", row:$row");
		}
		unset($fields);
		$msg = ($home_empty ? "home -default:".DEFAULT_MEDIAN_HOME : "home:$median_home").", condo:$median_condo";
		$this->log("updateMedianPrice - $zip returning $msg");
		// $msg2 = $condo_empty ? "home -default:".DEFAULT_MEDIAN_CONDO : "condo:$median_condo";
		// $this->log("updateMedianPrice - $zip returning $msg, $msg2");
		$this->flush();
		return ['median_home'=>(empty($median_home) ? DEFAULT_MEDIAN_HOME : $median_home),
				'median_condo'=>(empty($median_condo) ? DEFAULT_MEDIAN_CONDO : $median_condo),
				'api_count'=>$apiCount,
				'id'=>$row];
	}

	protected function fetchQuandl($url1, $what, $zip) {
		$handle = @fopen($url1."?rows=1&api_key=".QUANDL_API_KEY, "r");
		$median = 0;
		$count = 0;
		if (!empty($handle)) {
			while(!feof($handle)) {
				$count++;
				$line = fgets($handle);
				if ($count == 1) continue;
				$data = explode(",",$line);
				$this->log("zip - median $what: $".$data[1]);
				$median = intval($data[1]);
				break;
			}
			fclose($handle);
		}
		else
			$this->log("fetchQuandl - failed to get median price for $what at $zip");
		return $median;
	}

}
