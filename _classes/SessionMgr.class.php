<?php
namespace AH;

// Load WP components, no themes
if ( ! defined( 'WP_USE_THEMES' ) )
	define('WP_USE_THEMES', false);
// define('WP_ADMIN', true);2

require_once(__DIR__.'/_Controller.class.php');
require_once(__DIR__.'/Utility.class.php');
require_once(__DIR__.'/IPv6.php');
require_once(__DIR__.'/States.php');



require_once(__DIR__.'/../../../../wp-load.php');
require_once(__DIR__.'/../../../../wp-includes/pluggable.php');
require_once(__DIR__.'/../../../../wp-admin/includes/admin.php' );
	
/** Load Ajax Handlers for WordPress Core */
require_once( __DIR__.'/../../../../wp-admin/includes/ajax-actions.php' );

global $wpdb;
global $activeSessionID;

$activeSessionID = 0;

if ( ! defined( 'INVITATION_LIST' ) )
	define( 'INVITATION_LIST', 'invitationList');

global $restrictedPortalNames;
$restrictedPortalNames = [
	'dreamhomes',
	'dreamhome',
	'luxuryhomes',
	'luxuryhome',
	'myhome',
	'yourhome',
	'style',
	'mystyle',
	'yourstyle',
	'dazzle',
	'jewels',
	'gold',
	'golden',
	'treasure',
	'treasures',
	'thefind',
	'tomtong',
	'ttong',
	'tommy',
	'tom',
	'thebest',
	'best',
	'admin',
	'administrator',
	'user',
	'root',
	'support',
	'help',
	'it',
	'guest',
	'purchase',
	'purchaseportal',
	'purchaselifestyle',
	'purchasell',
	'lifestyle',
	'lifestyles',
	'purchaselifestyle',
	'purchaselifestyles',
	'wine_tasting',
	'golf',
	'skiing',
	'ocean_boating',
	'surfing',
	'beach',
	'equestrian',
	'casinos',
	'freshwater_boating',
	'fishing',
	'hunting',
	'hiking',
	'biking',
	'scuba',
	'nascar',
	'road_racing',
	'kayak',
	'inline_skating',
	'ice_skating',
	'mlb_baseball',
	'fifa_soccer',
	'nhl_hockey',
	'nfl_football',
	'nba_basketball',
	'freebird'
];

class SessionMgr extends Controller {
	protected $cassie = '';
	// comes from Controller now
	// protected $timezone_adjust = -7;
	protected $staleLock = 8.0;

	public function __construct(){
		parent::__construct();
		require_once(__DIR__.'/Options.class.php');
		$o = $this->getClass('Options');
		$this->browser = getBrowser();
		$logIt = $this->browser['userAgent'] != 'Site24x7' ? 3 : 0;

		$x = $o->get((object)array('where'=>array('opt'=>'SessionClassDebugLevel')));
		$this->log_to_file = empty($x) || !$logIt ? 0 : intval($x[0]->value);
		$this->log = $this->log_to_file ? new Log(__DIR__.'/_logs/.sessions.log') : null;
		$this->debugLevel = !empty($x) ? intval($x[0]->value) : 3;
		$this->cassie = '';
		// $this->log2 = $this->log_to_file ? new Log(__DIR__.'/_logs/.registration.log') : null;

		// $x = $o->get((object)array('where'=>array('opt'=>'RegisterTimeDurationAllowNoRegSessionSecs')));
		// $this->allowNoRegDuration = empty($x) ? 172800 : intval($x[0]->value);
	}

	public function __destruct() {
		if ($this->log !== null)
			$this->log->writeStringToFile();
	}


	public function checkPortalName(&$portal) {
		if (empty($portal))
			return new Out('fail', "Empty portal name");

		$this->log("checkPortalName - entered with $portal");
		$Sessions = $this->getClass('Sessions');
		global $wpdb;
		$sql = "SELECT * FROM ".$wpdb->prefix."usermeta WHERE meta_key = 'nickname'";
		$x = $Sessions->rawQuery($sql);

		$wpUser = wp_get_current_user();
		$userId = ($wpUser && !is_wp_error( $wpUser )) ? $wpUser->ID : 0;

		$portal = preg_replace("/[-\s]+/", '_', $portal);
		$portal = preg_replace('/[^a-zA-Z0-9_]/', '', $portal); 
		$portal = strtolower($portal);

		$havePortal = true;
		$msg = '';
		// $owners = get_users( array( 'search' => $portal ) );
		// if (!empty($owners)) foreach($owners as $user) {
		// 	$diff1 = 0; $diff2 = 0;
		// 	// if ( ($diff1 = levenshtein($user->user_nicename, $portal)) <= 1 &&
		// 	if ( ($diff2 = levenshtein($user->nickname, $portal)) <= 1 ) { // just check nickname in case user_nicename was set to 'unknown' due to expired invitation code
		// 		if ( ($diff1 == 0 && $diff2 == 0) ||
		// 			  strlen($portal) > 5 ) {
		// 			$havePortal = false;
		// 			break;
		// 		}
		// 	}
		// }

		if (!empty($x)) foreach ($x as $key => $value) {
			$diff1 = 0; $diff2 = 0;
			if ( ($diff2 = levenshtein( strtolower($value->meta_value), $portal) ) <= 1 ) { // just check nickname in case user_nicename was set to 'unknown' due to expired invitation code
				if ( ($diff1 == 0 && $diff2 == 0) ||
					  strlen($portal) <= 6 ) {
					if ($value->user_id == $userId) {
						return new Out('OK', "This portal:$portal belongs to current user:$userId");
					}
					$havePortal = false;
					$msg = "Portal:$portal is owned by user:$value->user_id";
					break;
				}
			}
		}

		// still ok?  check user_login
		if ($havePortal) {
			if (!empty($x))
				unset($x);

			$sql = "SELECT * FROM ".$wpdb->prefix."users WHERE user_login = '".$portal."'";
			$x = $Sessions->rawQuery($sql);
			if (!empty($x)) {
				$sameUser = false;
				foreach($x as $login) {
					if ($login->ID == $userId) {
						$sameUser = true;
						break;
					}
						
				}
				if (!$sameUser) {
					$havePortal = false;
					$msg = "Portal:$portal is user login of user:{$x[0]->ID}";
				}
			}
		}

		$this->log("checkPortalName - user:$userId, havePortal:".($havePortal ? "available" : "not available")." - $portal");

		if ($havePortal) {
			$company = $this->getClass('Associations')->get((object)['like'=>['company_code'=>$portal]]);
			$diff = 0;
			if (!empty($company) &&
				 ($diff = levenshtein($company[0]->company, $portal)) <= 2) {
				if ($diff == 0 ||
					strlen($portal) <= 6) {
					$msg = "checkPortalName - {$company[0]->company} is too close to this $portal as their company_code";
					$this->log($msg);
					$havePortal = false;
				}
			}
		}

		global $usStates;
		if ($havePortal) foreach($usStates as $key=>$state) {
			if ( strtolower($key) == $portal) {
				$msg = "checkPortalName - $key matches $portal, denied.";
				$this->log($msg);
				$havePortal = false;
			}
		}

		global $restrictedPortalNames;
		if ($havePortal) foreach($restrictedPortalNames as $name) {
			if ( $name == $portal ) {
				$msg = "checkPortalName - restricted:$name matches $portal, denied.";
				$this->log($msg);
				$havePortal = false;
			}
		}


		if ($havePortal) {
			// if ($this->getClass('Reservations')->exists(['portal'=>$portal])) {
			$seller = $userId ? $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$userId]]) : null;
			$reservation = $this->getClass('Reservations')->get((object)['like'=>['portal'=>$portal]]);
			if (!empty($reservation)) {
				foreach($reservation as $res) {
					$diff = 0;
					if ( ($diff = levenshtein($res->portal, $portal)) <= 1 ) {
						if ($diff == 0 ||
							strlen($portal) <= 6 ) {
							$havePortal = false;
							break;
						}
					}
				}

				if (!$havePortal) { // lose it?
					if ( !empty($seller) ) {
						if ( $seller[0]->id != $res->seller_id ) {// someone else has reserved this portal
							$msg = $msg = "checkPortalName - reservation:$res->id with owner:$res->seller_id owns $portal already, userId:$userId cannot get it.";
							$this->log($msg);
						}
						else {
							$havePortal = true; // got it back!
						}
					}
					else {
						$msg = "checkPortalName - find reservation:$res->id with $res->portal to be too close to $portal";
						$this->log($msg);
					}
				}
			}
		}

		// save it for this seller to use after payment
		if ($havePortal &&
			!empty($userId)) {			
			$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$userId]]);
			if (empty($seller)) {
				$msg = "checkPortalName - could not find seller with author_id of {$userId} to save the portal:$portal";
				$this->log($smg);
			}
			else {
				$seller = array_pop($seller);
				if ( ($seller->flags & SELLER_IS_PREMIUM_LEVEL_1) == 0 ) {
					$metas = [];
					$needUpdate = false;
					$gotOne = false;
					if (!empty($seller->meta)) foreach($seller->meta as $meta) {
						if ($meta->action != SELLER_NICKNAME)
							$metas[] = $meta;
						else {
							if ($meta->nickname != $portal) {
								$meta->nickname = $portal;
								$metas[] = $meta;
								$needUpdate = true;
							}
							$gotOne = true;
						}
					}

					if (!$gotOne) {
						$meta = new \stdClass();
						$meta->action = SELLER_NICKNAME;
						$meta->nickname = $portal;
						$metas[] = $meta;
						$needUpdate = true;
					}
					if ($needUpdate) {
						$this->getClass('Sellers')->set([(object)['where'=>['author_id'=>$userId],
																			'fields'=>['meta'=>$metas]]]);
						$this->log("checkPortalName - updated sellerId:$userId with $portal");
					}
				}
				else
					$this->log("checkPortalName - sellerId:$seller->id already has a portal!");
				$msg = ($havePortal ? $portal : "This $portal is not available</br>Please try a different one.");
			}
		}
		$this->log("checkPortalName - exiting for $portal, havePortal:".($havePortal ? "available" : "not available"));
		// $havePortal = get_user_by('user_nicename', $portal) === false && get_user_by('nickname', $portal) === false;
		return new Out( $havePortal, ($havePortal ? $portal : (!empty($msg) ? $msg : "This $portal is not available</br>Please try a different one.")) );
	}

	public function getPortalName($portal, $code, $id = 0, $mobile = null) { // $mobile is coming from agent-purchase-portal and agent-purchase-lifestyle pages
		$out = $this->checkPortalName($portal);
		if ($out->status == 'OK') {
			$user = wp_get_current_user();
			$author_id = $code == 'paywhirl' ? $id : $user->ID;
			$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$author_id]]);
			$sellerId = !empty($seller) ? $seller[0]->id : 0;
			if ($code != 'paywhirl' &&
				$id &&
				$sellerId != $id) {
				$this->log("SellerId:$id is not the same person as one logged in with authorId:$user->ID");
				return new Out('fail', "SellerId:$id is not the same person as one logged in with authorId:$user->ID");
			}
			$seller = !empty($seller) ? array_pop($seller) : null;

			if ($code != '0' &&
				$seller) {
				$ic = $this->getClass('Invitations');
				$x = $ic->get((object)['where'=>['code'=>$code]]);
				if ($code != 'paywhirl' &&
					(empty($x) ||
					 !($x[0]->flags & IC_FEATURE_PORTAL) ||
					 $x[0]->number_usage == 0)) {
					$msg = empty($x) ? "Invalid code, please try another." : (!($x[0]->flags & IC_FEATURE_PORTAL) ? "This code not authorized for Portal Agent use." : "Already used, please try another.");
					$this->log($msg);
					return new Out('fail', $msg);
				}
				else if ($code == 'paywhirl' ||
						 (!empty($x) &&
						  ($x[0]->flags & IC_FEATURE_PORTAL)) ) {
					
					
					if ($code != 'paywhirl') {
						$x[0]->number_usage--;
						$x[0]->flags |= IC_USED;
						$x[0]->count_usage++;
						$usage = $x[0]->number_usage;
						$count = $x[0]->count_usage;
						if (!isset($x[0]->seller_id))
							$x[0]->seller_id = [];
						$x[0]->seller_id[] = $sellerId;
						$x = $ic->set([(object)['where'=>['code'=>$code],
												'fields'=>['number_usage'=>$x[0]->number_usage,
															'count_usage'=>$x[0]->count_usage,
															'seller_id'=>$x[0]->seller_id,
														    'flags'=>$x[0]->flags]]]);
						if (empty($x))
							$this->log("getPortalName - failed to update number_usage for $code, sellerId:$sellerId, portal:$portal");
						else
							$this->log("getPortalName - updated number_usage for $code to ".$usage.", count:$count, sellerId:$sellerId, portal:$portal");
					}

					$metas = [];
					$modMeta = null;
					$needUpdate = false;
					$needModUpdate = false;
					$needOrderUpdate = false;
					$amOrder = null;
					if (!empty($seller->meta)) foreach($seller->meta as $meta) {
						if ($meta->action == SELLER_MODIFIED_PROFILE_DATA) {
							$modMeta = $meta;
						}
						elseif ($meta->action == SELLER_AGENT_ORDER) {
							$amOrder = $meta;
							$havePortalItem = false;
							if (!empty($amOrder->item)) foreach($amOrder->item as &$sellerItem) {
								$sellerItem = (object)$sellerItem;
								if ($sellerItem->type == ORDER_PORTAL) {
									$havePortalItem = true;
									if ($sellerItem->mode == ORDER_BUYING ||
										$sellerItem->mode == ORDER_IDLE) {
										$sellerItem->mode = ORDER_BOUGHT;
										$sellerItem->order_id = DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM;
										$sellerItem->inviteCode = $code;
										$sellerItem->inviteUsedDate = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
										$sellerItem->cart_item_key = $code;
										$needOrderUpdate = true;
										$this->log("getPortalName - updated SELLER_AGENT_ORDER for ORDER_PORTAL, sellerId:$sellerId, portal:$portal");
									}
								}
								unset($sellerItem);
							}
							if (!$havePortalItem) {
								$sellerItem = new \stdClass();
								$sellerItem->mode = ORDER_BOUGHT;
								$sellerItem->type = ORDER_PORTAL;
								$sellerItem->order_id = DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM;
								$sellerItem->inviteCode = $code;
								$sellerItem->inviteUsedDate = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
								$sellerItem->cart_item_key = $code;
								$needOrderUpdate = true;
								$this->log("getPortalName - added SELLER_AGENT_ORDER for ORDER_PORTAL, sellerId:$sellerId, portal:$portal");
								$amOrder->item[] = $sellerItem;
							}
							$metas[] = $amOrder;
						}
						elseif ($meta->action != SELLER_NICKNAME)
							$metas[] = $meta;
						else {
							$meta->nickname = $portal;
							$meta->inviteCode = $code;
							$meta->inviteUsedDate = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
							$needUpdate = true;
							$this->log("getPortalName - updated SELLER_NICKNAME, sellerId:$sellerId, portal:$portal");
							$metas[] = $meta;
						}
					}

					if (!$needUpdate) { // uh oh, need one!
						$meta = new \stdClass();
						$meta->action = SELLER_NICKNAME;
						$meta->nickname = $portal;
						$meta->inviteCode = $code;
						$meta->inviteUsedDate = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
						$metas[] = $meta;
						$needUpdate = true;
						$this->log("getPortalName - added SELLER_NICKNAME, sellerId:$sellerId, portal:$portal");
					}

					if (!$amOrder) {
						$item = new \stdClass();
						$item->type = ORDER_PORTAL;
						$item->mode = ORDER_BOUGHT;
						$item->order_id = DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM;
						$item->inviteCode = $code;
						$item->priceLevel = GRADE_6;
						$item->subscriptionType = MONTHLY_SUBSCRIPTION;
						$item->inviteUsedDate = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
						$item->cart_item_key = $code;
						$item->nickname = $portal;
						$amOrder = (object) ['action'=>SELLER_AGENT_ORDER,
											 'order_id_last_purchased'=>0,
											 'order_id_last_cancelled'=>0,
											 'agreed_to_terms'=>0,
											 'item'=>[$item]
										 	];
						$metas[] = $amOrder;
						$needUpdate = true;
						$this->log("getPortalName - added SELLER_AGENT_ORDER, sellerId:$sellerId, portal:$portal");
					}

					if ($code != 'paywhirl')
						add_user_meta(	$user->ID, 
										'inviteCodeUsed', 
										date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)),
										true);
					if (!($seller->flags & SELLER_IS_PREMIUM_LEVEL_1)) {
						$fields = ['flags'=>($seller->flags | SELLER_IS_PREMIUM_LEVEL_1)];
						$seller->flags |= SELLER_IS_PREMIUM_LEVEL_1;
					}

					if ($mobile) {
						if (empty($seller->mobile))
							$fields['mobile'] = $mobile;
						elseif ($seller->mobile != $mobile) {
							$fields['mobile'] = $mobile;
							if ($modMeta &&
								empty($modMeta->mobile)) {
								$modMeta->mobile = 1;
								$needModUpdate = true;
								$this->log("getPortalName - updated SELLER_MODIFIED_PROFILE_DATA, sellerId:$sellerId, portal:$portal");
							}
							else {
								$modMeta = new \stdClass();
								$modMeta->action = SELLER_MODIFIED_PROFILE_DATA;
								$modMeta->first_name = 0;
								$modMeta->last_name = 0;
								$modMeta->company = 0;
								$modMeta->website = 0;
								$modMeta->phone = 0;
								$modMeta->mobile = 1;
								$modMeta->email = 0;
								$needModUpdate = true;
								$this->log("getPortalName - added SELLER_MODIFIED_PROFILE_DATA, sellerId:$sellerId, portal:$portal");
							}
						}						
					}
					if ($mobile &&
						empty($seller->phone))
						$fields['phone'] =$mobile;

					if ($modMeta)
						$metas[] = $modMeta;
					if ($needUpdate ||
						$needModUpdate ||
						$needOrderUpdate) {
						$fields['meta'] = $metas;
						$seller->meta = $metas;
					}

					if (count($fields))
						$this->getClass('Sellers')->set([(object)['where'=>['author_id'=>$user->ID],
																  'fields'=>$fields]]);
					unset($metas, $fields);
				}
			}

			$author_id = $code == 'paywhirl' ? $id : $user->ID;
			$status = wp_update_user( array( 'ID' => $author_id, 'nickname' => $portal ) );
			if ( is_wp_error($status) ) {
				$this->log("getPortalName - Failed @1 to update user, status: ".$status->get_error_message());
				return new Out('fail', "Failed to create portal, please contact the administrator.");
			}
			$status = wp_update_user( array( 'ID' => $author_id, 'user_nicename' => $portal ) );
			if ( is_wp_error($status) ) {
				$this->log("getPortalName - Failed @2 to update user, status: ".$status->get_error_message());
				return new Out('fail', "Failed to create portal, please contact the administrator.");
			}

			return new Out('OK', ['status'=>"Portal created",
								  'seller'=>$seller]);
		}
		else
			return $out;
	}

	public function getCassie() {
		if (!empty($this->cassie) &&
			strlen($this->cassie) == 64)
			return $this->cassie;

		$cassie = isset($_COOKIE['CassiopeiaDirective']) ? $_COOKIE['CassiopeiaDirective'] : (isset($_SESSION['CassiopeiaDirective']) ? $_SESSION['CassiopeiaDirective'] : '');
		if (!empty($cassie) &&
			strlen($cassie) == 64) {
			$this->cassie = $cassie;
			$this->log("getCassie - found one ".$cassie.", in cookie:".(isset($_COOKIE['CassiopeiaDirective']) ? 'yes' : 'no'));
			return $cassie;
		}
		elseif (!empty($cassie))
			$this->log("getCassie - not empty but not 64 len:$cassie");
		else
			$this->log("getCassie is empty, make a new one");

		$random = '';
		for($i = 0; $i < 16; $i++) {
			$what = rand(1, 1000) % 3; // 0 = number, 1 = Caps, 2 = letter
			switch($what) {
				case 1: $random .= chr(rand(0, 25) + ord('a')); break;
				case 2: $random .= chr(rand(0, 25) + ord('A')); break;
				case 0: $random .= number_format(rand(0, 9));
			}
		}

		$ip = $this->userIP();
		// $cassie = $_COOKIE['CassiopeiaDirective'];
		// if (!isset($cassie) || empty($cassie)) {
		// 	$seed = isset($seller) && !empty($seller) ? $seller->first_name.$seller->last_name.microtime() : $ip;
		// 	$cassie = hash("sha256", $seed);
		// }
		// refresh

		$user = wp_get_current_user();
		$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;

		$seller = $userId != 0 ? $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$userId]]) : null;
		$seed = isset($seller) && !empty($seller) ? $seller[0]->first_name.$seller[0]->last_name.$userId : $ip.$random;
		$timeSeed = time();
		$seed .= number_format( is_numeric($timeSeed) ? (float)$timeSeed : 8312349705.2);
		$this->cassie = hash("sha256", $seed);

		if (!headers_sent()) {
			$this->log("getCassie, before header set cookie:$this->cassie, ip:$ip, browser:{$this->browser['name']}, platform:{$this->browser['platform']}, userAgent:{$this->browser['userAgent']}");
			setcookie('CassiopeiaDirective', $this->cassie, time() + (86400 * 365), "/"); // 86400 = 1 day
		}
		else {
			$this->log("getCassie, header already sent, cookie:$this->cassie, ip:$ip, browser:{$this->browser['name']}, platform:{$this->browser['platform']}, userAgent:{$this->browser['userAgent']}");
		}
		$_COOKIE['CassiopeiaDirective'] = $this->cassie;
		$_SESSION['CassiopeiaDirective'] = $this->cassie;
			
		// setcookie('CassiopeiaDirective', $cassie, time() + (86400 * 365), "/"); // 86400 = 1 day
		return $this->cassie;
	}

	protected function sessionIsLocked(&$ses) {
		if ( !($ses->type & SESSION_LOCKED) )
			return false;

		$date = date("Y-m-d H:i:s");
		$now = strtotime($date);
		$added = strtotime($ses->added);

		if ( ($now - $added) > $this->staleLock) {
			$x = $this->getClass('Sessions')->set([(object)['fields'=>['type'=>$ses->type ^ SESSION_LOCKED],
															'where'=>['id'=>$ses->id]]]);
			$this->log("sessionIsLocked - finds sessionId:$ses->id, session:$ses->session_id has been locked for ".($now - $added)." secs, unsetting SESSION_LOCKED");
			$ses->type ^= SESSION_LOCKED;
			return false;
		}

		return true;
	}

	public function getDirective($target, $fromInit = false, $row = 0, $portalId = 0, $page = 'home') {
		$this->log("getDirective - entered for target:$target");
		global $activeSessionID;
		$len = strlen($target);
		if (empty($target))
			$target = $this->getCassie();
		else if (strlen($target) != 64) {
			$oldTarget = $target;
			$target = $this->getCassie();
			$this->log("getDirective - target:$oldTarget has wrong length, replacing it with $target, page:$page");
		}
		else {
			$ip = $this->userIP();
			$this->log("getDirective - existing target:".(empty($this->cassie) ? "not set" : $this->cassie).", replacing it with $target, page:$page");
			if (!headers_sent()) {
				$this->log("getDirective, before header set cookie:$target, ip:$ip, browser:{$this->browser['name']}, platform:{$this->browser['platform']}, userAgent:{$this->browser['userAgent']}");
				setcookie('CassiopeiaDirective', $this->cassie, time() + (86400 * 365), "/"); // 86400 = 1 day
			}
			else {
				$this->log("getDirective, header already sent, cookie:$target, ip:$ip, browser:{$this->browser['name']}, platform:{$this->browser['platform']}, userAgent:{$this->browser['userAgent']}");
			}
			$_COOKIE['CassiopeiaDirective'] = $target;
			$_SESSION['CassiopeiaDirective'] = $target;
		}

		$activeSessionID = 0;
		$this->cassie = $target;
		$agentID = '';
		$nickname = '';
		$trackIt = 0;
		$ip = $this->userIP();
		$user = wp_get_current_user();
		$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
		$tempId = $userId;
		$waitPortalRow = isset($_COOKIE['WAIT_PORTAL_ROW']) ? intval($_COOKIE['WAIT_PORTAL_ROW']) : 0;
		$this->log("getDirective - userId:$userId, fromInit:$fromInit. row:$row, directive:$target, waitPortalRow:$waitPortalRow, portalId:$portalId");

		$reallyNoSessions = false;
		$browserMismatch = false;
		if ( empty($row) ) {
			if ($this->ipHasActivePortalRow()) {
				$this->log("getDirective is going to deflect this call since row is 0 and detected a locked session on this ip:$ip");
				$seller = new \stdClass();
				$seller->directive = $this->cassie;
				$seller->session = '';
				return new Out('OK', $seller);
			}
			// $sessions = $this->getIPAssoc($ip, $tempId);
			$sessions = $this->getIPAssoc($ip, $tempId);
			$this->log("getDirective - 1st search ".(empty($sessions) ? "was empty" : "got ".count($sessions)." sessions"), 3);
			if ( empty($sessions) ) {
				$this->log("getDirective - no sessions 1st try with userId:$userId", 3);
				// tempId should be 0 now since the first call, so if userId was already 0, then try forcing it to IP
				$forceToIp = $userId == 0; // otherwise will use the browser id to find match, which would have been done in prevous call is userid was 0.
				$sessions2 = $this->getIPAssoc($ip, $tempId, $forceToIp); 
				$this->log("getDirective - 2nd try with forceToIp:$forceToIp, ip:$ip, got for sessions:".(empty($sessions2) ? 'nothing' : 'something'), 3);
				$reallyNoSessions = empty($sessons2);
				$sessions = [];
				$decimal = inet_ptoi($ip);
				if (!empty($sessions2)) foreach($sessions2 as $ses)
					// if ( !($ses->type & SESSION_LOCKED) ) {
					if ( !($this->sessionIsLocked($ses)) ) {
						if ( (!empty($ses->browser_id) &&
						 	   $ses->browser_id == $target) &&
							 (empty($userId) || // then use whoever owned this one
							  $userId == $ses->user_id) ) // otherwise, better be the same owner
							// then no one own this one, but hit off same browser id, make a list of these
							$sessions[] = $ses;		
						else
							$browserMismatch = true; // this will signal that not all disallowed sessions were LOCKED, but had different browser id
					}

				if (count($sessions))
					$this->log("getDirective - taking over sessions from 0 userID", 3);
			}
		}
		else {
			$sessions = $this->getClass('Sessions')->get((object)['where'=>['id'=>$row]]);
			$this->log("getDirective - row:$row ".(empty($sessions) ? 'not' : '')." found:", 3);
			setcookie('WAIT_PORTAL_ROW', '', time() + (86400 * 1), "/"); // 86400 = 1 day
			if (!empty($sessions) &&
				($sessions[0]->type & SESSION_LOCKED) ) {
				$date = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
				$x = $this->getClass('Sessions')->set([(object)['fields'=>array('type'=>($sessions[0]->type ^ SESSION_LOCKED),
																		   	    'browser_id'=>$target),
															    'where'=>array('id'=>$row)]]);
				$this->log("getDirective - setting row:$row to undo SESSION_LOCKED and set browser_id to $target was ".(empty($x) ? "a failure" : "successful"), 3);
			}
		}

		$this->log("getDirective - sessions is ".(empty($sessions) ? "empty" : "retrieved").", WAIT_PORTAL_ROW:".(isset($_COOKIE['WAIT_PORTAL_ROW']) ? $_COOKIE['WAIT_PORTAL_ROW'] : "N/A"), 3);
		if (empty($row) && 
			// ((isset($_COOKIE['WAIT_PORTAL_ROW']) && !empty($_COOKIE['WAIT_PORTAL_ROW'])) ||
			empty($sessions) &&
			!$reallyNoSessions && // found some, but rejected them due to SESSION_LOCKED
			!$browserMismatch )   // and also didn't find any sessions not LOCKED and owned by a different browser
		{
			$this->log("getDirective is going to deflect this call since either SESSION_LOCKED is active or nothing was found and row is 0", 2);
			$seller = new \stdClass();
			$seller->directive = $this->cassie;
			$seller->session = '';
			return new Out('OK', $seller);
		}

		$session = null;
		$sessionWithId = null;
		$diff = 0;
		$curSession = '';
		// $row will be valid if setPortalAgent() was called without any browser_id(directive) and a new Session row was
		// forcefully created.  That new row id was passed to the home page, which is initiating this call to getDirection()
		// in header.js.  header.js should have checked to see if WAIT_PORTAL_ROW cookie was set, so it didn't automatically
		// call get-directive before the portal row was avaiable.
		if ( empty($row) ) {
			// $this->findSession($sessions, $session, $curSession, $sessionWithId, $userId, $diff, true);
			$agentId = 0;
			$nickname = '';
			$gotOne = false;
			if ($userId &&
				!empty($sessions) &&
				$sessions[0]->user_id == 0) {
				if (!empty($sessions[0]->value)) foreach($sessions[0]->value as $info) {
					if ($info->type == SESSION_PREMIER_AGENT) {
						$this->log("getPortalAgent - found agent:{$info->agentID}, nickname:{$info->nickname} for session:{$sessions[0]->session_id}");
						$agentID = $info->agentID;
						$nickname = $info->nickname;							
						$gotOne = !empty($agentID) && !empty($nickname);
						break;
					}
				}
				// first session for this browser id has user_id == 0, but userId is not 0, so go find one that this userId owns
				// and see if it has a portal agent associated with it, 
				if ($gotOne) 
					$session = $sessions[0];
				else foreach($sessions as $ses)
					if ($ses->user_id == $userId) { // match the user, don't just arbitrarily take the first one.
						$session = $ses;
						break;
					}
			}
			else
				$session = !empty($sessions) ? $sessions[0] : null;
		}
		else {
			$session = !empty($sessions) ? array_pop($sessions) : null;
			// if ($session)
			// 	$curSession = $session->session_id;
		}
		if ($session) {
			$curSession = $session->session_id;
			$activeSessionID = $session->id;
		}

		$haveProfile = false;
		if ($session) {
			$agent = 0;
			$trackIt = 0;
			$gotOne = false;
			$data = $session->value;
			if (isset($data)) 
				foreach($data as $i=>$info) {
					if (isset($info->type) && $info->type == SESSION_PREMIER_AGENT) {
						$agent = $info->agentID;
						$nickname= $info->nickname;
						$gotOne = true;
					}
					if (isset($info->type) && $info->type == SESSION_PROFILE_NAME) {
						$haveProfile = true;
						if ($gotOne)
							break;
					}
				}
			$trackIt = $session->type & SESSION_PORTAL_ENTRY;
			$agentID = gettype($agent) == 'string' ? $agent : number_format($agent, 0, '.', '');
			$this->log("getDirective - agentID:$agentID, nickname:$nickname, trackIt:".($trackIt ? "yes" : "no").", user_id:$session->user_id for session:$session->session_id", 3);

		 	// $msg = "getDirective - set premier agent to ".$this->setPremierAgent($session->session_id, $nickname, $agentID, 0).", session: $session->session_id";
		 	// $this->log($msg);
		}

		if ( empty($userId) &&
			!empty($session) &&
			(!empty($portalId) &&
			 !empty($agentID) &&
			  $portalId != $agentID) ) {
			if ( !empty($session->user_id) ) {
				$haveAgent = !empty($agentID);
				$haveQuiz = !empty($this->getClass('QuizActivity')->get((object)['where'=>['session_id'=>$session->id]]));
				$this->log("getDirective - got userId:$userId, with session - haveAgent:".($haveAgent ? "yes" : "no").", haveProfile:".($haveProfile ? "yes" : "no").", haveQuiz:".($haveQuiz ? "yes" : "no"), 3);
				if ( $haveAgent || $haveProfile || $haveQuiz) { // then this session was used by someone
					// now see if there are other sessions that are 0-user first before regenerating another for this browser
					// TODO: posibly save unsaved last search here?
					$gotOne = false;
					foreach($sessions as $ses) {
						if (empty($ses->user_id)) {
							$session = $ses;
							$curSession = $ses->session_id;
							$activeSessionID = $session->id;
							$gotOne = true;
							$this->log("getDirective - using $curSession with userId:".$ses->user_id." instead", 3);
							break;
						}
					}

					if (!$gotOne)
						$this->log("getDirective - using $curSession with userId:".$ses->user_id." for userId:0 anyway... hope it's the right person", 3);
					else {
						$data = $session->value;
						if (isset($data)) 
							foreach($data as $i=>$info) {
								if ($info->type == SESSION_PREMIER_AGENT) {
									$agentID = $info->agentID;
									$nickname= $info->nickname;
									break;
								}
							}
						$this->log("getDirective - will be using agentID:$agentID, nickname:$nickname instead.", 3);
					}
				}
			}
		}

		$force = false;
		if ( !empty($session) &&
			 (!empty($session->status) &&
			  !empty($session->data) &&
			  $session->status == 'done') ) {
			if ( $session->data->quiz_id > MAX_QUIZ_ACTIVITY) {
				if ($fromInit) {
					$force = true;
					$this->log("getDirective - setting force to true, quiz_id detected: {$session->data->quiz_id}, fromInit:$fromInit", 3);
					setcookie('NEED_FORCED_SESSION_ID', '', time() + (86400 * 1), "/"); // 86400 = 1 day
				}
				else {
					setcookie('NEED_FORCED_SESSION_ID', 'true', time() + (86400 * 1), "/"); // 86400 = 1 day
					$this->log("getDirective - setting NEED_FORCED_SESSION_ID to true, quiz_id detected: {$session->data->quiz_id}, fromInit:$fromInit", 3);
				}
			}
			else
				$this->log("getDirective - sees quiz_id set at {$session->data->quiz_id}, fromInit:$fromInit", 3);
		}
			 	

		if ( empty($session) || // create new one
			 $force ||
			 ($session && (empty($session->user_id) || $session->user_id == $userId)) ) {// or update it
			if (empty($session)) {
				$force = true;
				$this->log("getDirective - setting force to true since there is no session found for directive:$target", 3);
			}
			$this->log("getDirective - calling setIPAssoc - ip:".$this->userIP()." session:$curSession, userID:$userId, force:".($force ? "yes" : "no"), 3);
			$this->setIPAssoc($this->userIP(), $curSession, $userId, $force, $session);	
		}
		else {
			$this->log("getDirective - NOT calling setIPAssoc - ip:".$this->userIP()." session:$curSession, userID:$userId", 3);
			if ($fromInit) {
				if ( empty(session_id()) || (session_status() != PHP_SESSION_ACTIVE) ) {
					session_id($curSession);
					session_start();$this->log("getDirective - called session_start() with $curSession", 3);
				}
			}
		}

		if ( ($trackIt == 'true' ||
			  $trackIt == 1) &&
			!empty($nickname) &&
			!empty($agentID)) {
				$this->log("getDirective about to call trackPremierAgent for $nickname, $agentID, page:$page", 3);
				$this->trackPremierAgent($curSession, $nickname, $agentID, $page);
			
				// setcookie('PHPSESSID', $curSession, time() + (86400 * 1), "/"); // 86400 = 1 day
				// $this->setPortalAgent($nickname, $agentID, false, false);
				$this->setPremierAgent($curSession, $nickname, $agentID, $trackIt);
		}

		$this->log("getDirective - is searching for portal agent:$agentID", 2);
		$seller = !empty($agentID) ? $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$agentID]]) : null;
		if (empty($seller))
			$seller = new \stdClass();
		else
			$seller = array_pop($seller);
		$seller->directive = $this->cassie;
		$seller->session = $curSession;
		$seller->activeSessionID = $activeSessionID;

		if (!empty($nickname))
			$seller->nickname = $nickname;

		$this->log("getDirective - is returning for directive:{$this->cassie}, session:$curSession, activeSessionID:$activeSessionID, seller:".(isset($seller->id) ? $seller->id : "N/A").", nickname:".(!empty($nickname) ? $nickname : "N/A"));
		!empty($nickname) ? $seller->nickname = $nickname : $this->log("getDirective - no portal agent found.");
		return new Out('OK', $seller);
	}

	public function userIP() {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		    $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = $_SERVER['REMOTE_ADDR'];
		}

		if ( ($pos = strpos($ip, ',')) !== false)
			$ip = substr($ip, 0, $pos);
		return $ip;
	}

	public function getUserIPDecimal() {
		return round(inet_ptoi($this->userIP()));
	}

	protected function sessionInArray($session, $list) {
		foreach($list as $ses)
			if ($ses->session_id == $session)
				return $ses;
		return false;
	}

	public function setIPAssoc($ip, &$session, $id = 0, $force = false, $retiringSession = null, $agentID = 0, $nickname = '', $directive = null) {
		global $activeSessionID;
		$s = $this->getClass('Sessions');
		$ses = 0;
		$dbId = $id;
		$date = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));

		$this->log("setIPAssoc - entered with ip:$ip, session:$session, cassie:{$this->cassie}, id:$id, force:$force, agentID:$agentID, nickname:$nickname, directive:".(!empty($directive) ? $directive : "N/A").", date:$date");

		$noSessionsFound = !$force ? (($dbSession = $this->getIPAssoc($ip, $dbId)) === false) : true; // dbId == id at this point
		if (!$noSessionsFound) {
			if (empty($dbSession[0]->user_id) && $id) {
				$this->log("setIPAssoc - taking over ".$dbSession[0]->session_id." for userID:$id");
				$session = $dbSession[0]->session_id; //then this will force the issue...
				$activeSessionID = $dbSession[0]->id;
			}			
		}
		else if (!$force && !empty($id)) {  // then must be new user!!
			if ( ($dbSession = $this->getIPAssoc($ip, $dbId)) ) {
				$needNew = true;
				foreach($dbSession as $ses) {
					if (empty($ses->user_id)) {
						$session = $ses->session_id;
						$activeSessionID = $ses->id;
						$needNew = false;
						break;
					}
				}
				$noSessionsFound = $needNew;
				if (!$needNew) { //hurray, found an empty one!
					$session = $ses->session_id;
					$activeSessionID = $ses->id;
					$this->log("setIPAssoc - found 0-userId $session, taking over for userID:$id");
				}
			}
		}
		else if (!$force && empty($id)) {  // then try it with the ip
			if ( ($dbSession = $this->getIPAssoc($ip, $dbId, true)) ) { // true will force check by ip
				$needNew = true;
				foreach($dbSession as $ses) {
					if ($ses->session_id == $session) {
						$session = $ses->session_id;
						$activeSessionID = $ses->id;
						$needNew = false;
						break;
					}
				}
				$noSessionsFound = $needNew;
				$this->log("setIPAssoc - ".($needNew ? 'Not found' : 'found')." 0-userId session:".($session ? $session : "none")." using the ip:$ip");
			}
		}

		if ( $noSessionsFound ||
			 ($ses = $this->sessionInArray($session, $dbSession)) === false ) {
			if ($force || empty($session)) {
				if ( (empty(session_id()) || (session_status() != PHP_SESSION_ACTIVE)) &&
					!headers_sent() ) 
					session_start(); 	
				if (session_status() == PHP_SESSION_ACTIVE)		
					session_destroy();
				session_unset();
				if (!headers_sent()) {
					session_start();
					session_regenerate_id(true); 
				}
				$oldSession = $session;
				$session = session_id();
				// $noSessionsFound = true; // forces a write of this new session to db
				$this->log("setIPAssoc - incoming session id is ".( empty($oldSession) ? "null" : $oldSession).", id:$id, force:$force, agentID:$agentID, nickname:$nickname.  Forcing it to $session");
			}
			$decimal = inet_ptoi($ip);
			$q = new \stdClass();
			$q->ip_address = $decimal;
			$q->ip_dot = $ip;
			$q->session_id = $session;
			$q->user_agent = $_SERVER['HTTP_USER_AGENT'];
			if ($retiringSession) {
				$q->type = $retiringSession->type;
				$q->value = $retiringSession->value;
				if (!empty($retiringSession->status) &&
					!empty($retiringSession->data) &&
					isset($retiringSession->data->results) &&
					$retiringSession->status == 'done' &&
					!empty($retiringSession->data->results)) { // never mind with half backed quizzes
					$q->status = $retiringSession->status;
					$q->data = $retiringSession->data;
					$q->data->quiz_id = 0;
					// make a new QuizActivity so last results are available for this session
					$this->getClass('QuizActivity')->add((object) array(
								'session_id' => $session,
								'status' => 'done',
								'data' => [$q->data]));
					
				}
				$mode = ($retiringSession->type | SESSION_RETIRED) & ~SESSION_LOCKED; // undo any locks on it
				$x = $s->set([(object)['where'=>['id'=>$retiringSession->id],
								  	  'fields'=>['type'=>$mode]]]);
				$this->log("setIPAssoc - update for session id:{$retiringSession->id}-{$retiringSession->session_id} with new mode:$mode was ".(empty($x) ? "not successful": "successful"));
			}
			// if agentID is not empty, then getting this forced call from setPortalAgent() because
			// no session was found for this userID nor IP address nor directive (probably a brand new connection)
			if (!empty($agentID)) { 
				$q->type = SESSION_PORTAL_ENTRY | SESSION_LOCKED;
				$this->log("setIPAssoc - setting type to: {$q->type}");
				if (!empty($directive))
					$q->browser_id = $directive;
				else
					$q->browser_id = $this->getCassie();
				$info = new \stdClass;
				$info->type = SESSION_PREMIER_AGENT;
				$info->agentID = is_numeric($agentID) ? number_format($agentID, 0, '.', '') : $agentID;
				$info->nickname = $nickname;
				$q->value = [$info];
			}
			else
				$q->browser_id = $this->getCassie();
			if ($id)
				$q->user_id = $id;
			$x = $s->add($q);
			$activeSessionID = $x;
			$msg = "setIPAssoc - added row:$x, ip:$ip, id:$id, session:$session, agentID:$agentID, nickname:$nickname, directive:".$q->browser_id.", user_agent:".$_SERVER['HTTP_USER_AGENT'];
			if (!headers_sent()) {
				setcookie('PHPSESSID', $session, time() + (86400 * 30), "/"); // 86400 = 1 day
				setcookie('ACTIVE_SESSION_ID', $x, time() + (86400 * 30), "/"); // 86400 = 1 day
			}
			$this->log($msg);
			return $x;
		} 
		elseif ($ses->user_id != $id) {// need to update it
			$q = new \stdClass();
			// $q->where = array('session_id'=>$session);
			$q->where = array('id'=>$ses->id);
			$q->fields = array('user_id'=> $id);
			if (empty($ses->browser_id))
				$q->fields['browser_id'] = $this->getCassie();
			// $q->fields['updated'] = $date;
			$a = array($q);
			$x = $s->set($a);
			$activeSessionID = $ses->id;
			$msg = "setIPAssoc - update ip:$ip, id:$id, session:$session";
			if (!headers_sent())
				setcookie('ACTIVE_SESSION_ID', $activeSessionID, time() + (86400 * 30), "/"); // 86400 = 1 day
			$this->log($msg);
		}
		elseif ($ses->updated != $date) {
			// updaet the time of access
			$q = new \stdClass();
			$q->where = array('id'=>$ses->id);
			$activeSessionID = $ses->id;
			if (!headers_sent())
				setcookie('ACTIVE_SESSION_ID', $activeSessionID, time() + (86400 * 30), "/"); // 86400 = 1 day
			// $q->fields = array('updated'=> $date);
			if (empty($ses->browser_id)) {
				$q->fields['browser_id'] = $this->getCassie();
				$a = array($q);
				$x = $s->set($a);
				$msg = "setIPAssoc - update time of access:$date, from:".$ses->updated.", for ip:$ip, id:$id, session:$session was ".(!empty($x) ? "successful" : " a failure");
				$this->log($msg);
			}
			else
				$this->log("setIPAssoc - at $date, all info still the same");
		}
		else {
			$this->log("setIPAssoc - did not update for ip:$ip, id:$id, session:$session with $ses->updated, current: $date");
			$activeSessionID = $ses->id;
			if (!headers_sent())
				setcookie('ACTIVE_SESSION_ID', $activeSessionID, time() + (86400 * 30), "/"); // 86400 = 1 day
		}

		return true;
	}

	protected function getIPAssoc($ip, &$id, $forceToIp = false, $forceToUserId = false) {
		$s = $this->getClass('Sessions');
		$decimal = inet_ptoi($ip);
		$q = new \stdClass();
		$q->where = array(); 
		$cassie = $this->getCassie();
		// $this->log("getIPAssoc - entered with ip:$ip, userId:$id, directive:$cassie");

		if ($id &&
			$forceToUserId) // takes precedence
			$q->where['user_id'] = $id; //find all sessions associated with this user id
		else if (!$forceToIp)
			$q->where['browser_id'] = $cassie;
		else	
			$q->where['ip_address'] = $decimal; // just get all sessions run on this ip
		$q->orderby = 'updated';
		$q->order = 'desc'; // most recent one on top..

		$x = $s->get($q);
		$this->log("getIPAssoc - initially got:".(empty($x) ? "none" : count($x)).", id:$id, forceToUserId:$forceToUserId, directive:$cassie, forceToIp:$forceToIp, ip:$decimal", 2);
		// if (empty($x)) {
		// 	// try it with the ip_address
		// 	// $q->where = ['ip_address' => $decimal]; // just get all sessions run on this ip
		// 	// $x = $s->get($q);
		// 	// if (empty($x)) { // still empty...
		// 	// 	$this->log("getIPAssoc - none found for $decimal-$ip, id:$id, browser_id:$cassie");
		// 	// 	return false;
		// 	// }
		// 	$this->log("getIPAssoc - did not find any sessions, setting id=0, ip:$ip, userId:$id, directive:$cassie");
		// 	$id = 0;
		// 	return false;
		// }
		// else {
			$y = [];
			// strip out retired sessions
			$index = 0;
			if (!empty($x)) foreach($x as $ses) 
				if ( (($ses->type & SESSION_RETIRED) == 0 ||
					  ($id && $forceToUserId)) &&
					 (empty($id) ||
					  $id == $ses->user_id) )
					$y[] = $ses;
			
			$x = $y;
			$this->log("getIPAssoc - after filtering, got:".(empty($x) ? "none" : count($x)).", id:$id, forceToUserId:$forceToUserId, directive:$cassie, forceToIp:$forceToIp, ip:$decimal", 2);
			if (empty($x)) {
				$this->log("getIPAssoc - did not find any sessions, setting id=0, ip:$ip, userId:$id, directive:$cassie", 2);
				$id = 0;
				return false;
			}

			foreach($x as $ses)
				if ($index < 4) {
					$this->log("getIPAssoc - session:$ses->session_id, updated:$ses->updated, user:$ses->user_id", 2, false, 1);
					$index++;
				}
				else
					break;

			$ses = $x[0];
			$this->log("getIPAssoc - ip:$ip, incoming->id:$id, updated:$ses->updated, browser_id:$this->cassie, session:$ses->session_id, ses->user_id:$ses->user_id", 2);
			if (isset($x[0]->user_id) && $x[0]->user_id != 0)
				$id = $x[0]->user_id;
			// return $x[0]->session_id; // contains session id
			return $x; // returna ll of them
		// }
	}

	protected function trackPremierAgent($curSession, $nickname, $agentID, $page = 'unknown') {
		$ip = $this->userIP();
		$user = wp_get_current_user();
		$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;

		// $nickname = isset($_COOKIE['PROFILE_AGENT_NAME']) && $_COOKIE['PROFILE_AGENT_NAME'] != 'unknown' ? $_COOKIE['PROFILE_AGENT_NAME'] : $_SESSION['PROFILE_AGENT_NAME'];
		// $agentID = isset($_COOKIE['PROFILE_AGENT_ID']) && intval($_COOKIE['PROFILE_AGENT_ID']) != 0 ? intval($_COOKIE['PROFILE_AGENT_ID']) : intval($_SESSION['PROFILE_AGENT_ID']) ;

		if ($agentID == 0 &&
			!empty($nickname)) {
			$this->log("trackPremierAgent - agentID:$agentID, nickname:$nickname - calling get_users()");
			$blogusers = get_users( array( 'search' => $nickname ) );
			if (!empty($blogusers)) foreach($blogusers as $user) {
				if ($user->user_nicename == $nickname ||
					$user->nickname == $ponicknamertal) {
					$agentID = $user->ID;
					$this->log("trackPremierAgent - found agentID:$agentID for nickname:$nickname");
					break;
				}
			}
		}

		// we have the agentId and userId.  so record the agnetID into the session
		// and update the agentID (seller) meta data with this userId's visit and
		// record the date/id/IP

		$msg = "trackPremierAgent - ip:$ip, id:$userId, nickname:$nickname, agentID:$agentID, curSession:".$curSession.", page:$page";
		$this->log($msg);
		if ($agentID == 0) {
			$this->log("trackPremierAgent - agentID:$agentID for nickname:$nickname so exiting");
			return;
		}

		$q = new \stdClass();
		$q->where = array('session_id'=>$curSession);
		$q->orderby = 'updated';
		$q->order = 'desc';
		$ses = $this->getClass('Sessions')->get($q);
		// $ses = $this->getClass('Sessions')->get(['where'=>['session_id'=>$curSession]]);
		$needUpdate = false;
		$gotOne = false;
		$session_id = 0;
		if (!empty($ses)) {
			$data = (array)$ses[0]->value;
			$session_id = $ses[0]->id;
			if (isset($data)) 
				foreach($data as $i=>&$info) {
					if ($info->type == SESSION_PREMIER_AGENT) {
						if ($info->agentID != $agentID) {
							$info->agentID = $agentID;
							$needUpdate = true;
						}
						$gotOne = true;
						break;
					}
			}
			if (!$gotOne) {
				$info = new \stdClass;
				$info->type = SESSION_PREMIER_AGENT;
				$info->agentID = $agentID;
				$data[] = $info;
				$needUpdate = true;
			}
			if ($needUpdate) {
				$sessions = $this->getClass('Sessions')->get((object)['where'=>['session_id'=>$curSession],
																	  'orderby'=>'updated',
																	  'order'=>'asc']); // 'asc' so that the most recently upated will remain that way
				if (!empty($sessions)) foreach($sessions as $ses) {
					$this->getClass('Sessions')->set([(object)['fields'=>array('value'=>$data),
																'where'=>array('id'=>$ses->id)]]);
					unset($ses);
				}
				$this->log("trackPremierAgent - updated @1 session:$curSession");
			}
		}

		$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$agentID]]);
		if (!empty($seller)) {
			// update the seller meta data with userId and IP and date
			$meta = array();
			$visits = null;
			if (!empty($seller[0]->meta)) foreach($seller[0]->meta as $data) {
				if ($data->action != SELLER_VISITATIONS)
					$meta[] = $data;
				else {
					$visits = $data;
					// $visits->users = json_decode($visits->users, true);
				}
			}
			if (!$visits) {
				$visits = new \stdClass();
				$visits->action = SELLER_VISITATIONS;
				$visits->users = array();
				$visits->portalUsers = array();
			}
			elseif (!isset($visits->portalUsers))
				$visits->portalUsers = [];

			$needUpdateFromPortVisitations = false;

			if ($userId) {
				$portalUser = $this->getClass('PortalUsers')->get((object)['where'=>['wp_user_id'=>$userId]]);
				if (empty($portalUser)) {
					$this->log("calling addPortalUserFromWP with page:$page");
					$portalUserID = $this->getClass('PortalUsersMgr')->addPortalUserFromWP($session_id, $userId, $page, $agentID);
					if ($portalUserID) 
						$needUpdate = $this->updatePortalUserVisitations($meta, $visits, $portalUserID, $ip);
					else
						$needUpdate = $this->updateUserVisitations($meta, $visits, $userId, $ip);
				}
				else {
					$portalUserID = $portalUser[0]->id;
					$needUpdate = $this->updatePortalUserVisitations($meta, $visits, $portalUserID, $ip);
					$this->log("trackPremierAgent - after updatePortalUserVisitations, needUpdate:".($needUpdate ? 'yes' : 'no'));
				}
				if ($portalUserID)
					$needUpdateFromPortVisitations = $this->portVisitations($meta, $visits, $userId, $portalUserID, $agentID);
			}
			else
				$needUpdate = $this->updateUserVisitations($meta, $visits, $userId, $ip);

			if ($needUpdate ||
				$needUpdateFromPortVisitations) {
				// $visits->users = json_encode($visits->users);
				$meta[] = $visits;
				$x=$this->getClass('Sellers')->set([(object)['fields'=>['meta'=>$meta,
														  			'portal_visits'=>($seller[0]->portal_visits+1)],
														  'where'=>['author_id'=>$agentID]]]);
				$this->log("trackPremierAgent - update for $agentID was ".(!empty($x) ? 'successful' : 'failed'));
			}
		}		
	}

	public function portVisitations(&$meta, &$visits, $userId, $portalUserID, $agentID) {
		$gotOne = false;
		$needUpdate = false;
		$stamp = date("Y-m-d", time() + ($this->timezone_adjust*3600));
		// $visit == array of user IDs.  Each of those are arrays of IPs, Each IP is an array of dates
		if (count($visits->users)) {
			foreach($visits->users as $id=>&$ips) {
				$id = intval($id);
				if ($id == $userId) {
					// move these over to $visits->portalUsers
					foreach($ips as $addr=>$theDates) {
						foreach($theDates as $date) {
							$this->log("portVisitations - for user:$userId to portalUser:$portalUserID, ip:$addr on $date", 2);
							$this->updatePortalUserVisitations($meta, $visits, $portalUserID, $addr, $date);
							$this->getClass('PortalUsersMgr')->updatePageVisitations($portalUserID, 'home', $addr, $date);
							unset($date);
						}
						unset($theDates);
					}
					$needUpdate = true;
					if (gettype($visits->users) == 'array')
						unset($visits->users[$id]);
					else
						unset($visits->users->$id);
					break;
				}
				unset($ips);
			}
		}
		return $needUpdate;
	}

	protected function updateUserVisitations(&$meta, &$visits, $userId, $ip) {
		$gotOne = false;
		$needUpdate = false;
		$stamp = date("Y-m-d", time() + ($this->timezone_adjust*3600));
		// $visit == array of user IDs.  Each of those are arrays of IPs, Each IP is an array of dates
		if (count($visits->users)) 
			foreach($visits->users as $id=>&$ips) {
				$id = intval($id);
				if ($id == $userId) {
					foreach($ips as $addr=>&$theDates) {
						if ($addr == $ip) {						
							foreach($theDates as $date) {
								if ($date == $stamp) {
									$gotOne = true;
									$this->log("updateUserVisitations - user:$userId, already have for $ip, date:$stamp");
								}
								unset($data);
								if ($gotOne) break;
							}
							if (!$gotOne) {
								$theDates[] = $stamp;
								$this->log("updateUserVisitations - user:$userId, adding for $ip, date:$stamp");
								$gotOne = true;
								$needUpdate = true;
							}
						}
						unset($theDates);
						if ($gotOne) break;
					}
					if (!$gotOne) { // then same user, but different IP
						$ips = (array)$ips;
						$ips[$ip] = [$stamp];
						$ips = (object)$ips;
						$this->log("updateUserVisitations - user:$userId, adding new $ip, date:$stamp");
						$gotOne = true;
						$needUpdate = true;
					}
					break;
				}
				unset($ips);
			}

		if (!$gotOne) { // fresh visit
			$users = (array)$visits->users;
			$users[$userId] = (object)[$ip=>[$stamp]];
			$visits->users = (object)$users;
			$this->log("updateUserVisitations - new user:$userId, adding new $ip, date:$stamp");
			$needUpdate = true;
		}
		return $needUpdate;
	}	

	public function updatePortalUserVisitations(&$meta, &$visits, $userId, $ip, $fixedStamp = null) {
		$gotOne = false;
		$needUpdate = false;
		$stamp = empty($fixedStamp) ? date("Y-m-d", time() + ($this->timezone_adjust*3600)) : $fixedStamp;
		// $visit == array of user IDs.  Each of those are arrays of IPs, Each IP is an array of dates
		if (count($visits->portalUsers)) 
			foreach($visits->portalUsers as $id=>&$ips) {
				$id = intval($id);
				if ($id == $userId) {
					foreach($ips as $addr=>&$theDates) {
						if ($addr == $ip) {						
							foreach($theDates as $date) {
								if ($date == $stamp) {
									$gotOne = true;
									$this->log("updatePortalUserVisitations - user:$userId, already have for $ip, date:$stamp", 2);
								}
								unset($date);
								if ($gotOne) break;
							}
							if (!$gotOne) {
								$theDates[] = $stamp;
								$this->log("updatePortalUserVisitations - user:$userId, adding for $ip, date:$stamp", 2);
								$gotOne = true;
								$needUpdate = true;
							}
						}
						unset($theDates);
						if ($gotOne) break;
					}
					if (!$gotOne) { // then same user, but different IP
						$ips = (array)$ips;
						$ips[$ip] = [$stamp];
						$ips = (object)$ips;
						$this->log("updatePortalUserVisitations - user:$userId, adding new $ip, date:$stamp");
						$gotOne = true;
						$needUpdate = true;
					}
					break;
				}
				unset($ips);
			}

		if (!$gotOne) { // fresh visit
			$users = (array)$visits->portalUsers;
			$users[$userId] = (object)[$ip=>[$stamp]];
			$visits->portalUsers = (object)$users;
			$this->log("updatePortalUserVisitations - new user:$userId, adding new $ip, date:$stamp");
			$needUpdate = true;
		}
		return $needUpdate;
	}

	protected function getPremierAgent($curSession, &$nickname, &$trackIt, $userId = 0, $directive = '', $ip = '') {
		$q = new \stdClass();
		$q->where = array('session_id'=>$curSession);
		$q->orderby = 'updated';
		$q->order = 'desc';
		$ses = $this->getClass('Sessions')->get($q);
		if (!empty($userId))
			$q->where['user_id'] = $userId;
		else if (!empty($directive))
			$q->where['browser_id'] = $directive;
		else if (!empty($ip))
			$q->where['ip_address'] = inet_ptoi($ip);

		$gotOne = false;
		$agent = 0;
		if (!empty($ses)) {
			$data = $ses[0]->value;
			if (isset($data)) 
				foreach($data as $i=>$info) {
					if (isset($info->type) && $info->type == SESSION_PREMIER_AGENT) {
						$agent = isset($info->agentID) ? $info->agentID : 0;
						$nickname= isset($info->nickname) ? $info->nickname : '';
						$gotOne = true;
						break;
					}
			}
			$trackIt = $ses[0]->type & SESSION_PORTAL_ENTRY;
		}
		$agentID = gettype($agent) == 'string' ? $agent : number_format($agent, 0, '.', '');
		$this->log("getPremierAgent - session:$curSession, $agentID, $nickname, $trackIt, userId:$userId, directive:$directive, ip:$ip");

		return $agentID;
	}

	protected function setPremierAgent($curSession, $nickname, $lastAgent, $setPortalEntry = false) {
		$nick = ''; $track = 0;
		$agent = $this->getPremierAgent($curSession, $nick, $track);
		$agentID = empty($lastAgent) ? 0 : is_string($lastAgent) ? intval($lastAgent) : $lastAgent;
		$this->log("setPremierAgent - session:$curSession, getPremierAgent:$agent,$nick,$track, incoming:$agentID,$nickname,$setPortalEntry");

		// if ($agent == "0" &&
		// 	$lastAgent != "0") {
		// write back info to $curSession
		$q = new \stdClass();
		$q->where = array('session_id'=>$curSession);
		$q->orderby = 'updated';
		$q->order = 'desc';
		$ses = $this->getClass('Sessions')->get($q);
		// $ses = $this->getClass('Sessions')->get(['where'=>['session_id'=>$curSession]]);
		$needUpdate = false;
		$gotOne = false;
		$values = [];
		$date = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		if (!empty($ses)) {
			$data = (array)$ses[0]->value;
			if (isset($data)) 
				foreach($data as $i=>&$info) {
					if ($info->type == SESSION_PREMIER_AGENT) {
						if ($info->agentID != $agentID) {
							$this->log("setPremierAgent - updating from $info->agentID to $agentID for ".$ses[0]->session_id);
							$info->agentID = is_string($agentID) ? $agentID : number_format($agentID, 0, '.', '');
							$info->nickname = $nickname;							
							$needUpdate = true;
						}
						else if (!isset($info->nickname) || $info->nickname != $nickname) {
							$this->log("setPremierAgent - updating for agent:$agentID to $nickname for ".$ses[0]->session_id);
							$info->nickname = $nickname;
							$needUpdate = true;
						}
						$agent = $agentID;
						$gotOne = true;
					}
					$values[] = $info;
			}

			if (!$gotOne) {
				$info = new \stdClass;
				$info->type = SESSION_PREMIER_AGENT;
				$info->agentID = is_numeric($agentID) ? number_format($agentID, 0, '.', '') : $agentID;
				$info->nickname = $nickname;
				$values[] = $info;
				$agent = $agentID;
				$needUpdate = true;
				$this->log("setPremierAgent - adding new $agentID, $nickname - for ".$ses[0]->session_id);
			}

			$fields = [];
			if ($needUpdate)
				$fields['value'] = $values;

			if ( $setPortalEntry &&
				 !($ses[0]->type & SESSION_PORTAL_ENTRY) ) {
				$needUpdate = true;
				$fields['type'] = $ses[0]->type | SESSION_PORTAL_ENTRY;
				// $fields['updated'] = $date;
				$this->log("setPremierAgent - updating @1, $date, type to ".$fields['type']." for ".$ses[0]->session_id);
			}
			elseif ( !$setPortalEntry &&
				     ($ses[0]->type & SESSION_PORTAL_ENTRY) ) {
				$needUpdate = true;
				$fields['type'] = $ses[0]->type ^ SESSION_PORTAL_ENTRY;
				// $fields['updated'] = $date;
				$this->log("setPremierAgent - updating @2, $date, type to ".$fields['type']." for ".$ses[0]->session_id);
			}
			else {
				$needUpdate = true;
				// $fields['updated'] = $date;
				$fields['access'] = intval($ses[0]->access) + 1;
				$this->log("setPremierAgent - updating @3, $date, for access to ".(intval($ses[0]->access) + 1)." for ".$ses[0]->session_id);
			}

			if ($needUpdate) {
				$x = $this->getClass('Sessions')->set([(object)['fields'=>$fields,
																'where'=>array('id'=>$ses[0]->id)]]);
				$this->log("setPremierAgent - update:".(!empty($x) ? "success" : "fail")." for ".$ses[0]->session_id.", on row:{$ses[0]->id}");
			}
			else
				$this->log("setPremierAgent - all ok for ".$ses[0]->session_id);
		}
		// }
		// setcookie('PROFILE_AGENT_ID', $agent, time() + (86400 * 1), "/"); // 86400 = 1 day
		// setcookie('PROFILE_AGENT_NAME', $nickname, time() + (86400 * 1), "/"); // 86400 = 1 day
		// setcookie('TRACK_PREMIER_AGENT_USAGE', (isset($fields['type']) && $fields['type'] ? 'true' : 'false'), time() + (86400 * 1), "/"); // 86400 = 1 day

		return $agent;
	}

	public function freebird() {
		$id = 0;
		$this->log("freebird called, set to 0 for ip:".$this->userIP().', session:'.$this->getCurrentSession($id).', directive:'.(isset($_COOKIE['CassiopeiaDirective']) ? $_COOKIE['CassiopeiaDirective'] : "N/A"));
		$this->setPortalAgent('', 0, false, true);

		// $curSession = $_COOKIE['PHPSESSID'];
		// $this->log("freebird - $curSession");
		// if (empty($curSession)) {
		// 	$ip = $this->userIP();
		// 	$sessions = $this->getIPAssoc($ip);
		// 	$session = null;
		//  	$sessionWithId = null;
		//  	$curSession = '';
		//  	$diff = 0;
		//  	$user = wp_get_current_user();
		// 	$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
		//  	$this->findSession($sessions, $session, $curSession, $sessionWithId, $userId, $diff, false);
		//  	$this->log("freebird - after findSession $curSession");
		// }
		// $ses = $this->getClass('Sessions')->get((object)['where' => ['session_id' => $curSession ]]);
		// if (!empty($ses)) {
		// 	$needUpdate = false;
		// 	$agent = 0;
		// 	$data = $ses[0]->value;
		// 	if (isset($data)) {
		// 		$data = (array)$data;
		// 		foreach($data as $i=>&$info) {
		// 			if ($info->type == SESSION_PREMIER_AGENT) {
		// 				$agent = $info->agentID;
		// 				$data[$i]->agentID = 0;
		// 				$needUpdate = true;
		// 				break;
		// 			}
		// 		}
		// 	}

		// 	if ($needUpdate) {
		// 		$msg = "freebird - released agent:$agent from session:$curSession";
		// 		$this->log($msg);
		// 		$this->getClass('Sessions')->set([(object)['fields'=>array('value'=>$data),
		// 													'where'=>array('session_id'=>$curSession)]]);
		// 		setcookie('PROFILE_AGENT_NAME', 'unknown', time() + (86400 * 1), "/"); // 86400 = 1 day
		// 		setcookie('PROFILE_AGENT_ID', "0", time() + (86400 * 1), "/"); // 86400 = 1 day
		// 		setcookie('TRACK_PREMIER_AGENT_USAGE', 'false', time() + (86400 * 1), "/"); // 86400 = 1 day
		// 		$_SESSION['PROFILE_AGENT_NAME'] = 'unknown';
		// 		$_SESSION['PROFILE_AGENT_ID'] = "0";
		// 		$_SESSION['TRACK_PREMIER_AGENT_USAGE'] = 'false';
		// 	}
		// }
	}

	public function findSession($sessions, &$session, &$curSession, &$sessionWithId, $userId, &$diff, $quizZap) {
		$now = time();
		// this loop should get the most recent
		if ($sessions) foreach($sessions as $ses) {
			$time = $now - ($session ? strtotime($session->updated) : 0);
			$time2 = $now - strtotime($ses->updated);

			// if this session has no owner and is too old, delete any quiz associated with it, 
			// so the table doesn't crowded with old quizzes.
			if ( $quizZap &&
				($diff = $now - strtotime($ses->updated)) > $this->allowNoRegDuration &&
				 empty($ses->user_id) &&
				 $this->getClass('QuizActivity')->exists( (object)['session_id'=>$ses->session_id]) ) {
				$this->getClass('QuizActivity')->delete(array('session_id'=>$ses->session_id));
			}

			if ($time2 < $time) {
				// if (!empty($ses->user_id) &&
				// 	 $ses->user_id == $userId)
				// 	$sessionWithId = $session = $ses;
				// else if (empty($ses->user_id))
				$session = $ses;
				$diff = $time2;
			}				
		}

		if ($sessionWithId &&
			$sessionWithId != $session)
			$session = $sessionWithId;

		if (empty($session)) {
			session_regenerate_id(true);
			$curSession = session_id();
			// session_commit();
		}
		else {
			if (session_status() == PHP_SESSION_ACTIVE) {
				if (session_id() == $session->session_id) {
					$curSession = $session->session_id;
					return;
				}
				session_destroy();
			}
			// if (!session_id()) session_start();
			session_id($session->session_id);
			$curSession = session_id();
			// session_commit();
		}
	}

	public function releasePortalUser() {
		$ip = $this->userIP();
		$user = wp_get_current_user();
		$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
		$testId = $userId;
		$directive = $_COOKIE['CassiopeiaDirective'];
		$freebird = empty($nickname) && empty($agentID);

		$this->log("releasePortalUser - userId:$userId, ip:$ip, directive:".(empty($directive) ? "none" : $directive));
		$sessions = $this->getIPAssoc($ip, $testId, (!isset($_COOKIE['CassiopeiaDirective']) || empty($_COOKIE['CassiopeiaDirective'])));

		if (!empty($sessions) && 
			$freebird) {
			foreach($sessions as $ses) {
				if (!empty($ses->value)) foreach($ses->value as &$info) {
					if ($info->type == SESSION_PORTAL_USER &&
						!empty($info->portalUser)) {
						$info->portalUser = 0;
						$this->getClass('Sessions')->set([(object)['where'=>['id'=>$ses->id],
																   'fields'=>['value'=>$ses->value]]]);
						$this->log("releasePortalUser - session:$ses->id");
					}
					unset($info);
				}
				unset($ses);
			}
		}
	}

	public function setPortalAgent($nickname, $agentID = 0, $trackIt = true, $beforeHeader = false, $portalUserID = 0) {
		$ip = $this->userIP();
		$user = wp_get_current_user();
		$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
		$testId = $userId;
		$directive = $this->getCassie();
		$freebird = empty($nickname) && empty($agentID);


		$this->log("setPortalAgent - $nickname, $agentID, $trackIt, userId:$userId, ip:$ip, directive:".(empty($directive) ? "none" : $directive));
		$sessions = $this->getIPAssoc($ip, $testId, (!isset($_COOKIE['CassiopeiaDirective']) || empty($_COOKIE['CassiopeiaDirective'])));

		$session = null;
		if (!empty($sessions)) {
			$agent = 0;
			$nick_name = '';
			$gotOne = false;
			if ($userId &&
				$sessions[0]->user_id == 0) {
				if (!empty($sessions[0]->value)) foreach($sessions[0]->value as $info) {
					if ($info->type == SESSION_PREMIER_AGENT) {
						$this->log("setPortalAgent - found agent:{$info->agentID}, nickname:{$info->nickname} for session:{$sessions[0]->session_id}");
						$agent = $info->agentID;
						$nick_name = $info->nickname;							
						$gotOne = !empty($agent) && !empty($nick_name);
					}
					if ($freebird &&
						$info->type == SESSION_PORTAL_USER) {
						$info->portalUser = 0;
					}
					unset($info);
				}
				// first session for this browser id has user_id == 0, but userId is not 0, so go find one that this userId owns
				// and see if it has a portal agent associated with it, 
				if ($gotOne) {
					$session = &$sessions[0];
					$agentID = $agent;
					$nickname = $nick_name;
				}
				else foreach($sessions as &$ses) {
					if ($ses->user_id == $userId) { // match the user, don't just arbitrarily take the first one.
						$session = &$ses;
					}
					unset($ses);
					if ($session) break;
				}
			}
			else
				$session = &$sessions[0];
		}

		// if (!empty($sessions)) foreach($sessions as $ses)
		// 	if ($ses->user_id == $userId &&
		// 		!($ses->type & SESSION_LOCKED) ) { // match the user, don't just arbitrarily take the first one.
		// 		$session = $ses;
		// 		break;
		// 	}
			// if (($ses->user_id == $userId ||
			// 	 ($userId == 0 && !empty($directive))) &&
			// 	!($ses->type & SESSION_LOCKED) ) { // match the user, don't just arbitrarily take the first one.
			// 	$session = $ses;
			// 	break;
			// }

		// if (!empty($sessions) && 
		// 	$freebird) {
		// 	foreach($sessions as $ses) {
		// 		if (!empty($ses->value)) foreach($ses->value as &$info) {
		// 			if ($info->type == SESSION_PORTAL_USER &&
		// 				!empty($info->portalUser)) {
		// 				$info->portalUser = 0;
		// 				$this->getClass('Sessions')->set([(object)['where'=>['id'=>$ses->id],
		// 														   'fields'=>['value'=>$ses->value]]]);
		// 				$this->log("setPortalAgent freebirded session:$ses->id");
		// 			}
		// 			unset($info);
		// 		}
		// 		unset($ses);
		// 	}
		// }

		if ($session && 
			(!empty($directive) || !empty($userId)) ) {
			$this->log("setPortalAgent - calling setPremierAgent() - nickname:$nickname, agentID:$agentID, trackIt:$trackIt, session:$session->session_id, session userId:{$session->user_id}");
			$agent = $this->setPremierAgent($session->session_id, $nickname, $agentID, $trackIt); // true will set type to SESSION_PORTAL_ENTRY
			$this->log("setPortalAgent::setPremierAgent return agent:$agent, userId:$userId, beforeHeader:$beforeHeader, directive:$directive");
			if ($beforeHeader) {
				setcookie('PROFILE_AGENT_ID', $agentID, time() + (86400 * 1), "/"); // 86400 = 1 day
				setcookie('PROFILE_AGENT_NAME', $nickname, time() + (86400 * 1), "/"); // 86400 = 1 day
				setcookie('CassiopeiaDirective', $directive, time() + (86400 * 365), "/"); // 86400 = 1 day
				setcookie('ACTIVE_SESSION_ID', $session->id, time() + (86400 * 1), "/"); // 86400 = 1 day
			}
			if (!empty($userId)) {
				// then set all sessions owned by this user with the portal agent
				$needUpdate = false;
				$gotOne = false;
				$gotPortalUser = false;
				$values = [];
				$sessions = $this->getClass('Sessions')->get((object)['where'=>['user_id'=>$userId],
																	  'orderby'=>'updated',
																	  'order'=>'asc', // this way, the oldest will be updated last
																	  'notand'=>['type'=>SESSION_RETIRED]]);

				if (!empty($sessions)) foreach($sessions as $ses) {
					$data = (array)$ses->value;
					if (isset($data)) 
						foreach($data as $i=>&$info) {
							if ($info->type == SESSION_PREMIER_AGENT) {
								if ($info->agentID != $agentID) {
									$this->log("setPortalAgent - updating from $info->agentID to $agentID for ".$ses->session_id);
									$info->agentID = is_string($agentID) ? $agentID : number_format($agentID, 0, '.', '');
									$info->nickname = $nickname;							
									$needUpdate = true;
								}
								else if (!isset($info->nickname) || $info->nickname != $nickname) {
									$this->log("setPortalAgent - updating for agent:$agentID to $nickname for ".$ses->session_id);
									$info->nickname = $nickname;
									$needUpdate = true;
								}
								$agent = $agentID;
								$gotOne = true;
							}
							if ($info->type == SESSION_PORTAL_USER) {
								$gotPortalUser = true;
								if ($freebird) {
									$info->portalUser = 0;
									$needUpdate = true;
								}
								elseif ($portalUserID &&
										$portalUserID != $info->portalUser) {
									$info->portalUser = $portalUserID;
									$needUpdate = true;
								}
							}
							
							$values[] = $info;
						}

					if (!$gotOne) {
						$info = new \stdClass;
						$info->type = SESSION_PREMIER_AGENT;
						$info->agentID = is_numeric($agentID) ? number_format($agentID, 0, '.', '') : $agentID;
						$info->nickname = $nickname;
						$values[] = $info;
						$agent = $agentID;
						$needUpdate = true;
						$this->log("setPortalAgent - adding new $agentID, $nickname - for ".$ses->session_id);
					}

					if (!$gotPortalUser &&
						$portalUserID) {
						$info = new \stdClass;
						$info->type = SESSION_PORTAL_USER;
						$info->portalUser = $portalUserID;
						$values[] = $info;
						$needUpdate = true;
					}

					$fields = [];
					if ($needUpdate)
						$fields['value'] = $values;

					if ( !($ses->type & SESSION_PORTAL_ENTRY) ) {
						$needUpdate = true;
						$fields['type'] = $ses->type | SESSION_PORTAL_ENTRY;
						$this->log("setPortalAgent - updating type to ".$fields['type']." for ".$ses->session_id);
					}

					if ($needUpdate) {
						$x = $this->getClass('Sessions')->set([(object)['fields'=>$fields,
																		'where'=>array('id'=>$ses->id)]]);
						$this->log("setPortalAgent - update:".(is_array($x) ? "success" : "fail")." for ".$ses->session_id);
					}
					else
						$this->log("setPortalAgent - all ok for ".$ses->session_id);
					unset($values, $fields, $ses);
				}
			}
			elseif ($freebird) {
				$id = 0;
				$session = null;
			// require_once(__DIR__.'/../_classes/SessionMgr.class.php'); $SessionMgr = new SessionMgr();
				$session_id = $this->getCurrentSession($id, $session);
				$needUpdate = false;
				if (!empty($session->value)) foreach($session->value as &$info) {
					if ($info->type == SESSION_PORTAL_USER) {
						$info->portalUser = 0;
						$needUpdate = true;
					}
					unset($info);
				}
				if ($needUpdate) {
					$x = $this->getClass('Sessions')->set([(object)['fields'=>['value'=>$session->value],
																	'where'=>array('id'=>$session->id)]]);
					$this->log("setPortalAgent - update SESSION_PORTAL_USER for freebird:".(is_array($x) ? "success" : "fail")." for ".$session->session_id);
				}
			}
			return true;
		}
		else { // then must be a first time connection via agent portal...
			$session = null;
			$this->log("setPortalAgent - calling setIPAssoc() - nickname:$nickname, agentID:$agentID, trackIt:$trackIt, directive:$directive");
			$row = $this->setIPAssoc($ip, $session, $userId, true, null /*retiring session*/, $agentID, $nickname, $directive);
			setcookie('WAIT_PORTAL_ROW', $row, time() + (86400 * 1), "/"); // 86400 = 1 day
			$this->log("setPortalAgent::setIPAssoc return $row, with session:$session, set WAIT_PORTAL_ROW");
			if ($beforeHeader) {
				setcookie('PROFILE_AGENT_ID', $agentID, time() + (86400 * 1), "/"); // 86400 = 1 day
				setcookie('PROFILE_AGENT_NAME', $nickname, time() + (86400 * 1), "/"); // 86400 = 1 day
			}
			$_COOKIE['PROFILE_AGENT_ID'] = $agentID;
			$_COOKIE['PROFILE_AGENT_NAME'] = $nickname;
			return $row;
		}
	}

	protected function ipHasActivePortalRow() {
		$ip = $this->userIP();
		$testId = 0;
		$sessions = $this->getIPAssoc($ip, $testId, true);
		if (!empty($sessions)) foreach($sessions as $ses) {
			// if ($ses->type & SESSION_LOCKED)
			if ($this->sessionIsLocked($ses))
				return true;
		}
		return false;
	}

	public function getPortalAgent(&$nickname, &$trackIt) {
		$ip = $this->userIP();
		$user = wp_get_current_user();
		$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
		$testId = $userId;
		$useBrowserId = false;
		$useIp = false;
		$waitPortalRow = isset($_COOKIE['WAIT_PORTAL_ROW']) ? intval($_COOKIE['WAIT_PORTAL_ROW']) : 0;
		$this->log("getPortalAgent - entered for userId:$userId, directive:{$this->getCassie()}, waitPortalRow:$waitPortalRow");

		if ($this->ipHasActivePortalRow()) {
			$this->log("getPortalAgent - sees a locked session on ip:$ip");
			return 0;
		}
		// if ( ($sessions = $this->getIPAssoc($ip, $testId, (!isset($_COOKIE['CassiopeiaDirective']) || empty($_COOKIE['CassiopeiaDirective'])))) === false ) {
		// 	// then failed using browser id, so try the ip
		// 	$this->log("getPortalAgent - retrying with forced ip:$ip");
		// 	if ( ($sessions2 = $this->getIPAssoc($ip, $testId, true)) !== false ) {			
		// 		$sessions = [];
		// 		foreach($sessions2 as $ses)
		// 			if (($userId != 0 && $ses->user_id == $userId) || // a match
		// 				($userId != 0 && $ses->user_id == 0)) {// then take over it
		// 				$sessions[] = $ses;
		// 				$useIp = true;
		// 			}
		// 			else if ($ses->user_id == 0 &&
		// 					 empty($ses->browser_id))
		// 			{
		// 				$sessions[] = $ses;
		// 				$useIp = true;
		// 			}
		// 		if ($useIp)
		// 			$this->log("getPortalAgent - found session for userId:$userId using ip:$ip");
		// 	}	
		// }
		// else {
		// 	$this->log("getPortalAgent - found session for userId:$userId, directive:".$this->cassie);
		// 	if (empty($userId))
		// 		$useBrowserId = true;
		// }
		// $session = !empty($sessions) ? $sessions[0] : null;

		$sessions = $this->getIPAssoc($ip, $testId, (!isset($_COOKIE['CassiopeiaDirective']) || empty($_COOKIE['CassiopeiaDirective'])));
		$session = null;
		if (!empty($sessions)) {
			$y = [];
			foreach($sessions as $ses)
			// if (!($ses->type & SESSION_LOCKED) ) { // then it has portal data
			if (!($this->sessionIsLocked($ses)) ) { // then it has portal data
				$y[] = $ses;
			}
			$sessions = $y;
		}

		if (!empty($sessions)) {
			$agentId = 0;
			$nickname = '';
			$gotOne = false;
			if ($userId &&
				$sessions[0]->user_id == 0) {
				if (!empty($sessions[0]->value)) foreach($sessions[0]->value as $info) {
					if ($info->type == SESSION_PREMIER_AGENT) {
						$this->log("getPortalAgent - found agent:{$info->agentID}, nickname:{$info->nickname} for session:{$sessions[0]->session_id}");
						$agentID = $info->agentID;
						$nickname = $info->nickname;							
						$gotOne = !empty($agentID) && !empty($nickname);
						break;
					}
				}
				// first session for this browser id has user_id == 0, but userId is not 0, so go find one that this userId owns
				// and see if it has a portal agent associated with it, 
				if ($gotOne) 
					$session = $sessions[0];
				else foreach($sessions as $ses)
					if ($ses->user_id == $userId) { // match the user, don't just arbitrarily take the first one.
						$session = $ses;
						break;
					}
			}
			else
				$session = $sessions[0];
		}

		if ($session) {
			$directive = isset($_COOKIE['CassiopeiaDirective']) && !empty($_COOKIE['CassiopeiaDirective']) ? $_COOKIE['CassiopeiaDirective'] : 'N/A';
			$this->log("getPortalAgent - user_id:$session->user_id for session:$session->session_id, userId:$userId, directive:$directive, ip:$ip");
			return $this->getPremierAgent($session->session_id, $nickname, $trackIt); //, $userId, $directive, $ip);
		}
		else
			$this->log("getPortalAgent - did not find a valid session for userId:$userId, ip:$ip");
	}

	public function getCurrentSession(&$id, &$session_return = 'undefined') {
		$ip = $this->userIP();
		$user = wp_get_current_user();
		$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
		// $sessions = $this->getIPAssoc($ip, $userId);
		$curSession = '';
		$session = null;
		$sessionWithId = null;
		$diff = 0;

		$sessions = $this->getIPAssoc($ip, $userId, (!isset($_COOKIE['CassiopeiaDirective']) || empty($_COOKIE['CassiopeiaDirective'])));
		$session = null;
		
		if (!empty($sessions)) {
			$agentId = 0;
			$nickname = '';
			$gotOne = false;
			if ($userId &&
				$sessions[0]->user_id == 0) {
				if (!empty($sessions[0]->value)) foreach($sessions[0]->value as $info) {
					if ($info->type == SESSION_PREMIER_AGENT) {
						$this->log("getPortalAgent - found agent:{$info->agentID}, nickname:{$info->nickname} for session:{$sessions[0]->session_id}");
						$agentID = $info->agentID;
						$nickname = $info->nickname;							
						$gotOne = !empty($agentID) && !empty($nickname);
						break;
					}
				}
				// first session for this browser id has user_id == 0, but userId is not 0, so go find one that this userId owns
				// and see if it has a portal agent associated with it, 
				if ($gotOne) 
					$session = $sessions[0];
				else foreach($sessions as $ses)
					if ($ses->user_id == $userId) { // match the user, don't just arbitrarily take the first one.
						$session = $ses;
						break;
					}
			}
			else
				$session = $sessions[0];
		}

		if (!empty($session)) {
			$curSession = $session->session_id;
			$id= $session->id;
			$msg = "getCurrentSession - session_return:".((isset($session_return) || $session_return === null) && $session_return !== 'undefined' ? 'valid' : 'not set');
			$msg.= ":".($session_return === null ? 'null' : 
						(gettype($session_return) == 'object' ? print_r($session_return, true) : $session_return));
			$msg.= ", for session:$id";
			$this->log($msg, 3);
			if ((isset($session_return) || $session_return === null) &&
				$session_return !== 'undefined')
				$session_return = $session;
		}
		// $this->findSession($sessions, $session, $curSession, $sessionWithId, $userId, $diff, false);
		// if ( empty($userId) &&
		// 	!empty($session) ) {
		// 	if ( !empty($session->user_id) ) {
		// 		$haveAgent = !empty($agentID);
		// 		$haveQuiz = !empty($this->getClass('QuizActivity')->get((object)['where'=>['session_id'=>$session->id]]));
		// 		$this->log("getCurrentSession - got userId:$userId, with session - haveAgent:".($haveAgent ? "yes" : "no").", haveProfile:".($haveProfile ? "yes" : "no").", haveQuiz:".($haveQuiz ? "yes" : "no"));
		// 		if ( $haveAgent || $haveProfile || $haveQuiz) { // then this session was used by someone
		// 			// now see if there are other sessions that are 0-user first before regenerating another for this browser
		// 			// TODO: posibly save unsaved last search here?
		// 			$gotOne = false;
		// 			foreach($sessions as $ses) {
		// 				if (empty($ses->user_id)) {
		// 					$session = $ses;
		// 					$curSession = $ses->session_id;
		// 					$gotOne = true;
		// 					$this->log("getCurrentSession - using $curSession with userId:".$ses->user_id." instead");
		// 					break;
		// 				}
		// 			}

		// 			if (!$gotOne)
		// 				$this->log("getCurrentSession - using $curSession with userId:".$ses->user_id." for userId:0 anyway... hope it's the right person");
		// 		}
		// 	}
		// }
		$this->log("getCurrentSession - session:$curSession", 2);
		return $curSession;
	}

	public function getSessions() {
		$ip = $this->userIP();
		// $user = wp_get_current_user();
		$userId = get_current_user_id();
		if ($userId == 0)
			return [];
		
		$this->log("getSessions - userId:$userId, directive:".$this->getCassie());
		$sessions = $this->getIPAssoc($ip, $userId, false, true); // force to userId
		return $sessions;
	}

}
