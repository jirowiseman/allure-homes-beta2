<?php
namespace AH;
require_once(__DIR__.'/_Base.class.php');
require_once(__DIR__.'/Options.class.php'); 

class Cities extends Base {
	private $options = [
		'show-inactive' => false,
		'log-to-file' => true,
		'log-buffer' => 65536, // characters to buffer before saving to file
		'logfile' => null, // set in __construct()
	];
	public function __construct($logIt = 0){
		parent::__construct($logIt);
		$this->citiesLogGeoCodeDetail = 0;
		$Options = new Options();
		$opt = $Options->get((object)['where'=>['opt'=>'CitiesLogGeoCodeDetail']]);
		if ($opt) $this->citiesLogGeoCodeDetail = intval($opt[0]->value);
	}

	public function importFromListings($page = 0, $per_page = null){
		try {
			if ($per_page == null) $per_page = $this->rows_per_page;
			$this->log('importFromListings (page: '.$page.', rows_per_page: '.$per_page.')');
			$start_time = microtime(true);
			require_once(__DIR__.'/Listings.class.php'); $Listings = new Listings();
			$CitiesDB = $this;
			$Cities = [];
			$CitiesToAdd = [];
			$CitiesDBResult = $CitiesDB->get((object)[ 'what'=>['city','state','country'] ]);
			if (!empty($CitiesDBResult)) foreach ($CitiesDBResult as &$city){
				if (!array_key_exists($city->country, $Cities)) $Cities[$city->country] = [];
				if (!array_key_exists($city->state, $Cities[$city->country])) $Cities[$city->country][$city->state] = [];
				if (!array_key_exists($city->city, $Cities[$city->country][$city->state])) $Cities[$city->country][$city->state][$city->city] = 1;
			}
			$ListingQuery = (object)[ 'what'=>['id','city','state','country'], 'page'=>$page, 'limit'=>$per_page ];
			if (!$this->options['show-inactive']) $ListingQuery->where = [ 'active' => 1 ];
			$Listings = $Listings->get($ListingQuery);
			if (!empty($Listings)) foreach ($Listings as $i=>&$listing)
				if (isset($listing->city, $listing->state, $listing->country)) {
					$count_start = microtime(true);
					$listing->country = strtoupper(trim($listing->country));
					$listing->state = strtoupper(trim($listing->state));
					$listing->city = str_replace('-',' ',strtolower(trim($listing->city)));
					$listing->city = str_replace('  ',' ',strtolower(trim($listing->city)));
					if ($listing->city == 'los angeles (city)') $listing->city = 'los angeles';

					$t = explode(' ', $listing->city);
					$listing->city = '';
					$doNotUpperFirst = ['the','for','by','on'];
					foreach ($t as $i=>$str){
						if ($i > 0) $listing->city .= ' ';
						if ($i < 1 || !in_array($str, $doNotUpperFirst)) $str = ucfirst($str);
						$listing->city .= $str;
					} unset($i, $t, $str);
					$log_string = '';

					if (!array_key_exists($listing->country, $Cities)) {
						$log_string.= '[New country: '.$listing->country . "] ";
						$Cities[$listing->country] = [];
					}
					if (!array_key_exists($listing->state, $Cities[$listing->country])){
						$log_string.= '[New state: '.$listing->state . "] ";
						$Cities[$listing->country][$listing->state] = [];
					}
					if (!array_key_exists($listing->city, $Cities[$listing->country][$listing->state])){
						$CitiesToAdd[] = [ 'country'=>$listing->country, 'state'=>$listing->state, 'city'=>$listing->city ];
						$log_string.= '[New city: '.$listing->city . '] ';
						$Cities[$listing->country][$listing->state][$listing->city] = 1;
					} else {
						$Cities[$listing->country][$listing->state][$listing->city]++;
						$log_string.= '[count: ' . $Cities[$listing->country][$listing->state][$listing->city]. '] ';
					}

					$log_string = $listing->id.': '.$listing->city.', '.$listing->state . ' ' .$log_string;
					$log_string.= ' ('. round((microtime(true) - $count_start), 4) .')';
					$count_start = null;
					$this->log($log_string);
					unset($log_string, $listing);
				}

			$this->log( 'starting to add to DB, parsing took '.round((microtime(true) - $start_time), 4).' seconds', true );
			if (!empty($CitiesToAdd)) foreach ($CitiesToAdd as $x) $CitiesDB->add($x);
			$this->log('importFromListings finished in '.round((microtime(true) - $start_time), 4).' seconds', true);
			// return $Cities;
			return true;
		} catch (\Exception $e) { parseException($e); }
	}
	public function countCities(){
		$this->log('countCities');
		$start_time = microtime(true);
		require_once(__DIR__.'/Listings.class.php'); $Listings = new Listings();
		$CitiesDB = $this;
		$Cities = [];
		foreach ($CitiesDB->get((object)[ 'what'=>['city','state','country'] ]) as &$city){
			if (isset($city->city, $city->state, $city->country)) {
				if (!array_key_exists($city->country, $Cities)) $Cities[$city->country] = [];
				if (!array_key_exists($city->state, $Cities[$city->country])) $Cities[$city->country][$city->state] = [];
				if (!array_key_exists($city->city, $Cities[$city->country][$city->state])) $Cities[$city->country][$city->state][$city->city] = 0;
			}
			unset($city);
		}
		$ListingQuery = (object)[ 'what'=>['id','city','state','country'] ];
		if (!$this->options['show-inactive']) $ListingQuery->where = [ 'active' => 1 ];
		$Listings = $Listings->get($ListingQuery);
		if (empty($Listings)) $this->log('No listings found.');
		else foreach ($Listings as $i=>&$listing){
			if (isset($listing->city, $listing->state, $listing->country)) {
				$start_parse = microtime(true);
				$listing->country = strtoupper(trim($listing->country));
				$listing->state = strtoupper(trim($listing->state));
				if ($listing->city == 'los angeles (city)') $listing->city = 'los angeles';
				if ( ($pos = strpos($listing->city, "(")) !== false ) {
					$listing->city = trim(substr($listing->city, 0, $pos));
				}
				$listing->city = formatName($listing->city);

				$log_string = '';
				if (strlen($listing->city) == 0 ||
					nonConformingLocation($listing->city) ||
					nonConformingLocation($listing->state) ||
					nonConformingLocation($listing->country) ) {
					$log_string = $listing->id.': '.$listing->city.', '.$listing->state . ' '.$listing->country.' is non-conforming.';
					$this->log($log_string);
					unset($log_string, $listing);
					continue;
				}

				if (!array_key_exists($listing->country, $Cities)){
					$log_string.= '[New country: '.$listing->country . "] ";
					$Cities[$listing->country] = [];
				}
				if (!array_key_exists($listing->state, $Cities[$listing->country])){
					$log_string.= '[New state: '.$listing->state . "] ";
					$Cities[$listing->country][$listing->state] = [];
				}
				if (!array_key_exists($listing->city, $Cities[$listing->country][$listing->state])){
					$log_string.= '[New city: '.$listing->city . '] ';
					$Cities[$listing->country][$listing->state][$listing->city] = 1;
				} else {
					$Cities[$listing->country][$listing->state][$listing->city]++;
					$log_string.= '[count: ' . $Cities[$listing->country][$listing->state][$listing->city]. '] ';
				}

				$now = microtime(true) - $start_parse;
				$log_string = $listing->id.': '.$listing->city.', '.$listing->state . ' ' .$log_string . ' ('. ($now < .0001 ? '< .0001' : round($now,4)) .')';
				$start_parse = null;
				$this->log($log_string);
				unset($log_string, $listing);
			}
			unset($listing);
		} unset($Listings);
		if (!empty($Cities)){
			$this->log('Starting to prepare DB rows, counting took '.round((microtime(true) - $start_time), 4).' seconds');
			$queries = [];
			foreach ($Cities as $country=>&$states){
				foreach ($states as $state=>&$cities){
					foreach ($cities as $city=>&$count){
						$query = (object)[ 'fields'=>['count'=>$count]];
						if (!empty($city))
						 	$query->where = ['city'=>$city];
						else
							$query->where = ['city'=>'isnull'];
						if (!empty($state))
							$query->where['state'] = $state;
						else
							$query->where['state'] = 'isnull';
						if (!empty($country))
							$query->where['country'] = $country;
						else
							$query->where['country'] = 'isnull';
						$queries[] = $query;
						unset($city, $count, $query);
					} unset($state, $cities);
				} unset($country, $states);
			} unset($Cities);
			if (!empty($queries)) {
				foreach ($queries as &$query){
					$x = $query->where;
					$x['count'] = $query->fields['count'];
					$save_start = microtime(true);
					$logstring = 'Saving DB row: (count: '. $x['count'] . ', city: '. $x['city'] .', '. $x['state'] .')... ';
					try {
						$logstring .= !$CitiesDB->exists($x) ?
							($CitiesDB->set([$query]) ? 'Success' : 'Failed') . '('.(round(microtime(true) - $save_start,4)).')' :
							'Already exists ('.(round(microtime(true) - $save_start,4)).')';
					}
					catch(\Exception $e) {
						parseException($e);
						$logstring = "Exception caught in countCities:185";
					}
					$this->log($logstring);

					$save_start = $logstring = null;
					unset($x, $query);
				}
				$this->updateCitiesCounts();
				$this->log('countCities finished in '.round((microtime(true) - $start_time), 4).' seconds', true);
				return true;
			}
			else $this->log('No queries found.');
		} else $this->log('No cities found.');
	}

	public function updateCitiesCounts() {
		$CitiesDB = $this->get((object)[ 'what' => ['id','city','state','count','lat','lng'], 'where' => [ 'lat' => 'notnull' ], 'orderby' => 'count' ]);
		if (empty($CitiesDB)) return;
		$cities = [];
		foreach ($CitiesDB as &$row) {
			if ($row->count < 1 || ($row->lat == -1 && $row->lng == -1)) unset($row);
			else {
				$cities[$row->id] = (object)[ 'id' => $row->id,
											  'label'=>$row->city.', '.$row->state, 
											  'value'=>$row->city.', '.$row->state, 
											  'count'=>$row->count, 
											  'lng' => $row->lng,
											  'lat' => $row->lat ];
				unset($row);
			}
		} unset($CitiesDB);
		$cities = array_reverse($cities);

		require_once(__DIR__.'/CitiesCounts.class.php'); $CitiesCounts= new CitiesCounts();
		$CitiesCounts->emptyTable(true);
		$CitiesCounts->addMultiple($cities);
	}

	public function geocodeZip($GoogleLocation, $zip, &$result_city, &$result_state, &$result_country, &$google_result) {
		$google_result = $GoogleLocation->geocodeZip($zip, 0);
		if ($google_result->status !== 'OK') {
			if ($google_result->data->status == 'ZERO_RESULTS'){
				return false;
			}
			elseif ($google_result->data->status == 'OVER_QUERY_LIMIT') throw new \Exception("Over daily geocoding limit. Aborting.");
			else throw new \Exception('Unable to geocode: '.$google_result->data->status);
		} else {
			$google_result = $google_result->data;
			if (isset($google_result->address_components) && !empty($google_result->address_components)) {
				foreach ($google_result->address_components as &$address_component){
					if (in_array('locality', $address_component->types)) {
						$result_city = $address_component->long_name;
					}
					if (in_array('administrative_area_level_1', $address_component->types)) {
						$result_state = $address_component->short_name;
					}
					if (in_array('country', $address_component->types)) {
						$result_country = $address_component->short_name;
					}
				}
			}
			return !empty($result_city);
		}
	}

	public function geocodeCity($GoogleLocation, $city, &$lng, &$lat, &$result_city, &$google_result) {
		$lng = -1; $lat = -1;
		$cityStr = $city->city;
		if ( ($pos = strpos($cityStr, ",")) !== false)
			$cityStr = substr($cityStr, 0, $pos);

		$city->state = fixNY($cityStr, $city->state);
		$cityStr = fixCity($cityStr, $city->state);
		$cityStr = ucwords(fixCityStr($cityStr));

		if (nonConformingLocation($cityStr))
			return false;
		if (nonConformingLocation($city->state))
			return false;

		if (empty($city->lat) && empty($city->lng) && !empty($city->city) && !empty($city->state)){
			$query = $cityStr . ',' . $city->state . (!empty($city->zip) ? ',' . $city->zip : ''). ($city->country ? ',' . $city->country : '');
			$this->log('['. \date('Y-m-d H:i:s') .'] ' . json_encode($query));

			if ($this->citiesLogGeoCodeDetail)
				$GoogleLocation->forceEnableLog();
			$google_result = $GoogleLocation->geocodeAddress($query, 0);
			if ($this->citiesLogGeoCodeDetail)
				$GoogleLocation->forceDisableLog();

			if ($google_result->status !== 'OK') {
				if ($google_result->data->status == 'ZERO_RESULTS'){
					$this->log("geocodeCity - status is {$google_result->data->status}");
					return false;
				}
				elseif ($google_result->data->status == 'OVER_QUERY_LIMIT') throw new \Exception("Over daily geocoding limit. Aborting.");
				else throw new \Exception('Unable to geocode: '.$google_result->data->status);
			} else {
				$google_result = $google_result->data;

				// get city name from result set
				$haveCity = $haveState = $haveCountry = false;
				if (isset($google_result->address_components) && !empty($google_result->address_components)){
					$result_city = (object)[ 'distance' => 99, 'name' => null ];
					if ($city->country == 'US') {
						$zipMatches = false;
						$googleZip = '';
						foreach ($google_result->address_components as $address_component){
							if (in_array('postal_code', $address_component->types) ) {
								$googleZip = $address_component->short_name;
								if ($address_component->short_name == $city->zip)
									$zipMatches = true;
							}
							unset($address_component);
							if ($zipMatches)
								break;
						}

						$lev1 = 0;
						$lev2 = 0;
						foreach ($google_result->address_components as $address_component){
							if ((in_array('locality', $address_component->types) ||
								 in_array('sublocality', $address_component->types) ||
								 in_array('neighborhood', $address_component->types) ||
								 in_array('administrative_area_level_3', $address_component->types) ||
								 in_array('natural_feature', $address_component->types)) &&
								(strpos(strtolower($address_component->short_name), strtolower($cityStr)) === 0 ||
								 strpos(strtolower($address_component->long_name), strtolower($cityStr)) === 0 ||
								 strpos(strtolower($cityStr), strtolower($address_component->short_name)) === 0 ||
								 strpos(strtolower($cityStr), strtolower($address_component->long_name)) === 0 ||
								 ($lev1 = levenshtein(strtolower($address_component->short_name), strtolower($cityStr))) < 3 ||
								 ($lev2 = levenshtein(strtolower($address_component->long_name), strtolower($cityStr))) < 3 ||
								 ($zipMatches &&
								  ($lev1 < 5 || $lev2 < 5)) ) ) {
								$result_city->distance = 0;
								// $result_city->name = $address_component->long_name;
								$result_city->name = $cityStr;
								$haveCity = true;
							}

							if (in_array('administrative_area_level_1', $address_component->types) && 
								(strpos($address_component->short_name, $city->state) === 0 ||
								 strpos($address_component->long_name, $city->state) === 0) ) {
								$haveState = true;
								$result_city->state = $city->state;
							}
							unset($address_component);						
						}
						if (!$haveState) // booo hooo!
							$result_city->distance = 10;
						$this->log("geocodeCity -  zipMatches:$zipMatches, zip:$city->zip, googleZip:$googleZip, lev1:$lev1, lev2:$lev2");
					}
					// not one of 50 states, relax requirement!
					else {
						$zipMatches = false;
						$googleZip = '';
						foreach ($google_result->address_components as $address_component){
							if (in_array('postal_code', $address_component->types) ) {
								$googleZip = $address_component->short_name;
								if ($address_component->short_name == $city->zip)
									$zipMatches = true;
							}
							unset($address_component);
							if ($zipMatches)
								break;
						}
						$lev1 = 0;
						$lev2 = 0;
						foreach ($google_result->address_components as $address_component){
							if ((in_array('locality', $address_component->types) ||
								 in_array('sublocality', $address_component->types) ||
								 in_array('neighborhood', $address_component->types) ||
								 in_array('administrative_area_level_3', $address_component->types) ||
								 in_array('natural_feature', $address_component->types)) &&
								(strpos(strtolower($address_component->short_name), strtolower($cityStr)) === 0 ||
								 strpos(strtolower($address_component->long_name), strtolower($cityStr)) === 0 ||
								 strpos(strtolower($cityStr), strtolower($address_component->short_name)) === 0 ||
								 strpos(strtolower($cityStr), strtolower($address_component->long_name)) === 0 ||
								 ($lev1 = levenshtein(strtolower($address_component->short_name), strtolower($cityStr))) < 3 ||
								 ($lev2 = levenshtein(strtolower($address_component->long_name), strtolower($cityStr))) < 3 ||
								 ($zipMatches &&
								  ($lev1 < 5 || $lev2 < 5)) ) ) {
								$result_city->distance = 0;
								// $result_city->name = $address_component->long_name;
								$result_city->name = $cityStr;
								$haveCity = true;
							}

							if (in_array('administrative_area_level_1', $address_component->types) && 
								(strpos($address_component->short_name, $city->state) === 0 ||
								 strpos($address_component->long_name, $city->state) === 0) ) {
								$haveState = true;
								$result_city->state = $city->state;
							}

							if (in_array('country', $address_component->types) && 
								(strpos($address_component->short_name, $city->country) === 0 ||
								 strpos($address_component->long_name, $city->country) === 0) ) {
								$haveCountry = true;
								$result_city->country = $city->country;
							}
							unset($address_component);
						}
						if (!$haveState && !$haveCountry) // booo hooo!
							$result_city->distance = 10;

					}									
				}

				if ($result_city != null &&
					$result_city->distance < 3) {// check string similarity
					$lat = $google_result->geometry->location->lat;
					$lng = $google_result->geometry->location->lng;
					return true;
				}
			}
		}
		return false;
	}

	public function geocodeCities($page = 0, $per_page = 5){
		$this->log('geocodeCities', true);
		try {
			$start_time = microtime(true);
			require_once(__DIR__.'/GoogleLocation.class.php');
			$GoogleLocation = new GoogleLocation();
			$this->log('geocodeCities');
			$accepted = [];
			$rejected = (object)[ 'info' => [], 'query' => [] ];
			$cities = $this->get((object)[ 'what'=>['id','city','state','country','zip','lat','lng'], 'where' => [ 'lat' => null, 'lng' => null ], 'page'=>$page, 'limit'=>$per_page ]);
			if (!empty($cities)) foreach ($cities as &$row){
				$result_city = $google_result = null;
				$lng = $lat = -1;
				$row_city = $row->city;
				$row_state = $row->state;
				if (!empty($row->zip)) {
					$zip = json_decode($row->zip);
					$row->zip = $zip; // get the first one
				}
				if ($this->geocodeCity($GoogleLocation, $row, $lng, $lat, $result_city, $google_result)) {
					$result = (object)[
									'where' => [ 'id' => $row->id ],
									'fields' => [ 'lat' => $lat, 'lng' => $lng ],
								];
					if ($result_city->name != $row_city)
						$result->fields['city'] = $result_city->name;
					if ($result_city->state != $row_state)
						$result->fields['state'] = $result_city->state;
					$accepted[] = $result;
				}
				else {
					$rejected->info[] = (object)[
						'city_id' => $row->id,
						'city' => $row->city,
						'state' => $row->state,
						'result_city' => $result_city,
						'result' => $google_result,
					];
					$rejected->query[] = (object)[
						'where' => [ 'id' => $row->id ],
						'fields' => [ 'lat' => -1, 'lng' => -1 ],
					];
				}
				
				// if (empty($row->lat) && empty($row->lng) && !empty($row->city) && !empty($row->state)){
				// 	$query = $row->city . ', ' . $row->state . ($row->country ? ' ' . $row->country : '');
				// 	$this->log('['. \date('Y-m-d H:i:s') .'] ' . json_encode($query));

				// 	$google_result = $GoogleLocation->geocodeAddress($query);
				// 	if ($google_result->status !== 'OK') {
				// 		if ($google_result->data->status == 'ZERO_RESULTS'){
				// 			$rejected->info[] = (object)[
				// 				'city_id' => $row->id,
				// 				'city' => $row->city,
				// 				'state' => $row->state,
				// 				'result_city' => $result_city,
				// 				'result' => $google_result,
				// 			];
				// 			$rejected->query[] = (object)[
				// 				'where' => [ 'id' => $row->id ],
				// 				'fields' => [ 'lat' => -1, 'lng' => -1 ],
				// 			];
				// 		}
				// 		elseif ($google_result->data->status == 'OVER_QUERY_LIMIT') throw new \Exception("Over daily geocoding limit. Aborting.");
				// 		else throw new \Exception('Unable to geocode: '.$google_result->data->status);
				// 	} else {
				// 		$google_result = $google_result->data;

				// 		// get city name from result set
				// 		if (isset($google_result->address_components) && !empty($google_result->address_components)){
				// 			foreach ($google_result->address_components as &$address_component){
				// 				$short = levenshtein($address_component->short_name, $row->city);
				// 				$long = levenshtein($address_component->long_name, $row->city);
				// 				$address_component->distance = $short <= $long ? $short : $long;
				// 			}
				// 			$result_city = (object)[ 'distance' => 99, 'name' => null ];
				// 			foreach ($google_result->address_components as &$address_component){
				// 				if ($address_component->distance < $result_city->distance){
				// 					$result_city = (object)[ 'distance' => $address_component->distance, 'name' => $address_component->long_name ];
				// 				}
				// 			}
				// 		}

				// 		if ($result_city->distance < 3) // check string similarity
				// 			$accepted[] = (object)[
				// 				'where' => [ 'id' => $row->id ],
				// 				'fields' => [ 'lat' => $google_result->geometry->location->lat, 'lng' => $google_result->geometry->location->lng ],
				// 			];
				// 		else {
				// 			$rejected->info[] = (object)[
				// 				'city_id' => $row->id,
				// 				'city' => $row->city,
				// 				'state' => $row->state,
				// 				'result_city' => $result_city,
				// 				'result' => $google_result,
				// 			];
				// 			$rejected->query[] = (object)[
				// 				'where' => [ 'id' => $row->id ],
				// 				'fields' => [ 'lat' => -1, 'lng' => -1 ],
				// 			];
				// 		}
				// 	}
				// }
				unset($row, $result_city);
			}

			$this->log('geocodeCities finished in '.round((microtime(true) - $start_time), 4).' seconds', true);
			return (object)[
				'rejected' => [ $rejected->info, (empty($rejected->query) ? [] : $this->set($rejected->query)) ],
				'accepted' => ( empty($accepted) ? [] : $this->set($accepted) ),
			];
		} catch (\Exception $e) { parseException($e); die(); }
	}
	public function createListingsPivot($page = 0, $per_page = 500, $recheck_all = false){
		try {
			$results = [];
			require_once(__DIR__.'/Listings.class.php');
			$Listings = new Listings();
			$listing_query = (object)[ 'where' => [ 'city' => 'notnull', 'state' => 'notnull', 'country' => 'notnull' ], 'page' => $page, 'limit' => $per_page, 'what' => ['id', 'city', 'state', 'country'] ];
			if (!$recheck_all) $listing_query->where['city_id'] = null;
			$listings_db = $Listings->get( $listing_query );
			if (!empty($listings_db)) foreach ($listings_db as &$row){
				$row->country = strtoupper(trim($row->country));
				$row->state = strtoupper(trim($row->state));
				$row->city = str_replace('-',' ',strtolower(trim($row->city)));
				$row->city = str_replace('  ',' ',strtolower(trim($row->city)));
				if ($row->city == 'los angeles (city)') $row->city = 'los angeles';

				$t = explode(' ', $row->city);
				$row->city = '';
				$doNotUpperFirst = ['the','for','by','on'];
				foreach ($t as $i=>$str){
					if ($i > 0) $row->city .= ' ';
					if ($i < 1 || !in_array($str, $doNotUpperFirst)) $str = ucfirst($str);
					$row->city .= $str;
				} unset($i, $t, $str);

				$city_id = $this->get( (object)[ 'where' => ['city' => $row->city, 'state' => $row->state, 'country' => $row->country], 'what' => ['id', 'city', 'state'] ]);
				if ($city_id !== false){
					$city_id = array_pop($city_id);
					$city_id = $city_id->id;
				}

				if (empty($city_id)) $city_id = -1;
				$already_exists = $Listings->exists((object)[ 'id' => $row->id, 'city_id' => $city_id ]);

				if ( !$already_exists )
					$results[] = [
						[ 'id' => $row->id, 'city' => $city_id ],
						$Listings->set([ (object)[ 'where' => [ 'id' => $row->id ], 'fields' => [ 'city_id' => $city_id ] ] ])
					];

				unset($row);
			}
			return $results;
		} catch (\Exception $e) { parseException($e); die(); }
	}

	public function updateQuandlCityCodes($in) {
		if (empty($in))
			return new Out('fail', "No files sent to updateQuandlCityCodes");

		$total = 0;
		foreach($in as $file) {
			$name = $file['tmp_name'];
			$qfile = fopen($name, 'r');
			if ($qfile === false)
				return new Out('fail', "Unable to open file: $name");
			$count = 0;
			while( !feof($qfile) ) {
				$count++;
				$line = fgets($qfile);
				if ($count == 1) continue;
				if ($line === false)
					break;

				$pieces = explode("|", $line);
				if (count($pieces) == 2) {
					$location = explode(",", $pieces[0]);
					if (count($location) >= 2) {
						$city = $this->get((object)['where'=>['city'=>$location[0],
															  'state'=>$location[1]]]);
						if (!empty($city)) {
							if (empty($city[0]->quandl)) {
								$x = $this->set([(object)['where'=>['id'=>$city[0]->id],
														  'fields'=>['quandl'=>$pieces[1]]]]);
								$this->log("line:$count - updated city:".$location[0].', '.$location[1].", with quandl code:".$pieces[1]);
							}
						}
						else
							$this->log("line:$count - failed to find city:".$location[0].', '.$location[1]);
					}
					else
						$this->log("line:$count - failed to explode: ".$pieces[0]);
				}
				else
					$this->log("line:$count - failed to explode: $line");
				$this->flush();
			}
			$total += $count;
		}

		return new Out('OK', "Read in $total lines");
	}

	public function getActiveCityCount() {
		$sql = "SELECT COUNT( DISTINCT(a.id) ) FROM {$this->getTableName()} AS a ";
		$sql.= "INNER JOIN {$this->getTableName('listings')} AS b WHERE b.city_id = a.id AND b.active = 1";
		$x = $this->rawQuery($sql);
		if (!empty($x)) {
			return new Out('OK', current((array)$x[0]) );
		}
		return new Out('fail', "Did not get a count");
	}

	public function updateMedianPrices($page, $pagePer) {
		$this->log("updateMedianPrices - entered for page:$page, pagePer:$pagePer");
		$row = 0;
		$count = 0;
		$processed = 0;
		$sql = "SELECT a.id, a.city, a.state, a.zip, a.city_id, b.quandl, b.median_home, b.median_condo, b.updated FROM {$this->getTableName('listings')} AS a ";
		$sql.= "INNER JOIN {$this->getTableName()} AS b WHERE a.city_id = b.id AND a.active = 1 GROUP BY a.city_id ";
		$sql.= "LIMIT ".($page*$pagePer).",$pagePer";

		$now = time();

		if ( !empty($cities = $this->rawQuery($sql)) ) {
			$row = $page * $pagePer;
			$this->log("updateMedianPrices - $page, $row - citiesCount:".count($cities));
			foreach($cities as $city) {
				$count++;
				if ( ($now - strtotime($city->updated)) < MONTH_SECS)
					continue;

				if ( $this->updateMedianPrice($city) )
					$processed++;
			}
		}
	}

	public function updateMedianPrice($city) {
		$now = time();
		if ( ($now - strtotime($city->updated)) < MONTH_SECS)
			return false;

		if (!empty($city->quandl)) {
			$quandl = intval($city->quandl);
			$quandl = str_pad( number_format($quandl, 0, '.', ''), 5, "0", STR_PAD_LEFT );
			$url1 = "https://www.quandl.com/api/v3/datasets/ZILL/C{$quandl}_MLP.csv";
			$url2 = "https://www.quandl.com/api/v3/datasets/ZILL/C{$quandl}_C.csv";
			$this->fetchQuandl($url1, $url2, $city);
			return true;
		}
		elseif (!empty($city->zip) &&
				$city->zip != '00000') {
			$zip = str_replace(" ", '', $city->zip); // take out spaces
			$zip = strpos($zip, "-") !== false ? explode("-", $zip)[0] : $zip; // get only first part of compound zip, 12345-1234
			$url1 = "https://www.quandl.com/api/v3/datasets/ZILL/Z{$zip}_MLP.csv";
			$url2 = "https://www.quandl.com/api/v3/datasets/ZILL/Z{$zip}_C.csv";
			$this->fetchQuandl($url1, $url2, $city);
			return true;
		}
		return false;
	}

	protected function fetchQuandl($url1, $url2, $city) {
		$handle = fopen($url1."?rows=1&api_key=".QUANDL_API_KEY, "r");
		$median_home = 0;
		$median_condo = 0;
		$count = 0;
		if (!empty($handle)) {
			while(!feof($handle)) {
				$count++;
				$line = fgets($handle);
				if ($count == 1) continue;
				$data = explode(",",$line);
				$this->log("$city->city $city->state - median home: ${$data[1]}");
				$median_home = intval($data[1]);
				break;
			}
			fclose($handle);
		}
		$count = 0;
		$handle = fopen($url2."?rows=1&api_key=".QUANDL_API_KEY, "r");
		if (!empty($handle)) {
			while(!feof($handle)) {
				$count++;
				$line = fgets($handle);
				if ($count == 1) continue;
				$data = explode(",",$line);
				$this->log("$city->city $city->state - median condo: ${$data[1]}");
				$median_condo = intval($data[1]);
				break;
			}
			fclose($handle);
		}
		$fields = [];
		if (!empty($median_home))
			$fields['median_home'] = $median_home;
		if (!empty($median_condo))
			$fields['median_condo'] = $median_condo;
		if (count($fields))
			$this->set([(object)['where'=>['id'=>$city->id],
								 'fields'=>$fields]]);

		$this->flush();
		return true;
		// $tempFile = tempnam("/tmp", 'alr').'.csv';
		// $dir = dirname($tempFile);
		// popen('rm '.$dir.'/alr*.csv', "r");
		// $timeout = 10; // in secs

		// $ch = curl_init();
		// curl_setopt($ch, CURLOPT_URL, $url);
	}

}
