<script type="text/javascript">
	jQuery(document).ready(function($){
		$('#faq .question#1 #title').on('click', function() {
			$('#faq .question#1 .dropdown').slideToggle();
		});
		$('#faq .question#2 #title').on('click', function() {
			$('#faq .question#2 .dropdown').slideToggle();
		});
		$('#faq .question#3 #title').on('click', function() {
			$('#faq .question#3 .dropdown').slideToggle();
		});
		$('#faq .question#4 #title').on('click', function() {
			$('#faq .question#4 .dropdown').slideToggle();
		});
		$('#faq .question#5 #title').on('click', function() {
			$('#faq .question#5 .dropdown').slideToggle();
		});
		$('#faq .question#6 #title').on('click', function() {
			$('#faq .question#6 .dropdown').slideToggle();
		});
		$('#faq .question#7 #title').on('click', function() {
			$('#faq .question#7 .dropdown').slideToggle();
		});
		$('#faq .question#8 #title').on('click', function() {
			$('#faq .question#8 .dropdown').slideToggle();
		});
		$('#faq .question#9 #title').on('click', function() {
			$('#faq .question#9 .dropdown').slideToggle();
		});
		$('#faq .question#10 #title').on('click', function() {
			$('#faq .question#10 .dropdown').slideToggle();
		});
		$('#faq .question#11 #title').on('click', function() {
			$('#faq .question#11 .dropdown').slideToggle();
		});
		$('#faq .question#12 #title').on('click', function() {
			$('#faq .question#12 .dropdown').slideToggle();
		});
	});
</script>
<div id="faq">
	<!--<div class="c3 c4 c0"id="contents"><span>Contents<span></div>
	<ul>
		<li><a href="#Q1">1: What is Lifestyled Listings?</a></li>
		<li><a href="#Q2">2: Isn&rsquo;t standard searching faster?</a></li>
		<li><a href="#Q3">3: Why don&rsquo;t the homes have everything I asked for in the assessment?</a></li>
		<li><a href="#Q4">4: Where do the listings come from?</a></li>
		<li><a href="#Q5">5: How are location matches created?</a></li>
		<li><a href="#Q6">6: What are &ldquo;tags&rdquo;?</a></li>
		<li><a href="#Q7">7: I found an amazing property but when I contacted the agent it wasn&rsquo;t actually on the market!</a></li>
		<li><a href="#Q8">8: Agent FAQ</a></li>
		<li><a href="#Q9">9: How do I know if my listing is being displayed?</a></li>
		<li><a href="#Q10">10: Can any listing be displayed on Lifestyled Listings?</a></li>
		<li><a href="#Q11">11: How do I know if my listing will qualify to be displayed?</a></li>
		<li><a href="#Q12">12: I created an account but I don&rsquo;t see any of my listings.</a></li>
		<li><a href="#Q13">13: How can I add my listing to be displayed?</a></li>
	</ul>-->
	<div class="main-section">
		<span class="main-title">Frequently Asked Questions</span>
		<div class="question" id="1">
			<span id="title" class="odd">What is Lifestyled Listings?</span>
			<div class="dropdown" style="display: none;">
				<p>Lifestyled Listings is a new way to search for homes that helps you find listings and locations that fit your lifestyle and needs together in one fun, easy, and effective process. Much like you can answer questions by typing them in a google search bar, as opposed to going directly to a website to find your answer, we allow buyers to essentially ask the question: Where are the best locations and homes for my lifestyle?</p>
			</div>
		</div>
		<div class="question" id="2">
			<span id="title" class="even">Isn&rsquo;t standard searching faster?</span>
			<div class="dropdown" style="display: none;">
				<p>It depends on what you count as the finish line. A basic search bar that lets you enter a location and basic filters will show you homes faster, but are those homes right for your unique lifestyle? Are you seeing all the options that fit your needs or only the options you know of? If your search process ends when you find the best location and home for your lifestyle, then there&rsquo;s no question that the Lifestyled search process gets you further, faster.</p>
			</div>
		</div>
		<div class="question" id="3">
			<span id="title" class="odd">Why don&rsquo;t the homes have everything I asked for in the assessment?</span>
			<div class="dropdown" style="display: none;">
				<p>Sounds like you made a really unique search and we don&rsquo;t have a listing in an area that gives you everything that you asked for. We still want you to see what the closest matches currently on the market are so you don&rsquo;t miss properties that are really close to your preferences. .Another possibility is that your price range is limiting your results, maybe your dream really is on the market exists but is outside of the price range you selected.<br><br>If you only want to see homes that meet your exact criteria, use the toggles next to your tags on the quiz results page.</p>
			</div>
		</div>
		<div class="question" id="4">
			<span id="title" class="even">Where do the listings come from?</span>
			<div class="dropdown" style="display: none;">
				<p>We get our data from a company called Listhub. They are the original data providers for other portals you may be familiar with such as Zillow and Trulia. We refresh our listing data twice per day so that we can provide as close to accurate data as possible on this scale.</p>
			</div>
		</div>
		<div class="question" id="5">
			<span id="title" class="odd">How are location matches created?</span>
			<div class="dropdown" style="display: none;">
				<p>Our lifestyle hotspot team scours the web, consults industry experts, and visits locations to gather information on where to send you. Through these sources, we assign scores to each lifestyle measure so that locations can be compared directly with one another. For example, you can play golf in the middle of nowhere, or you can play in Pebble Beach on a PGA Tour course with some of the most breathtaking views of the Pacific Ocean. We use the lifestyle scores we gather to send you to Pebble Beach rather than a drab location with some rundown courses, that is if golf is what you&rsquo;re looking for.</p>
			</div>
		</div>
		<div class="question" id="6">
			<span id="title" class="even">What are &ldquo;tags&rdquo;?</span>
			<div class="dropdown" style="display: none;">
				<p>Tags are like markers or signals that we can use to communicate information about a listing, a person, or between the two. They allow us to quickly and accurately organize complex things in ways that make sense. A tag can be very straightforward like &ldquo;high ceilings&rdquo;, or more abstract like &ldquo;adventurous&rdquo; to characterize a place where lots of outdoor activities and sports are common.</p>
			</div>
		</div>
		<div class="question" id="7">
			<span id="title" class="odd">I found an amazing property but when I contacted the agent it wasn&rsquo;t actually on the market!</span>
			<div class="dropdown" style="display: none;">
				<p>We do our best to only show the most accurate information available to us and we update our data feed twice per day. Unfortunately, somewhere in the data chain between the listing agent and us information can be lost, listings duplicated, or listing status&rsquo; not updated. If you find a listing that has inaccuracies we appreciate your help in notifying us so we take appropriate action.</p>
			</div>
		</div>

		<span class="main-title">Agent FAQ</span>
		<div class="question" id="8">
			<span id="title" class="even">How do I know if my listing is being displayed?</span>
			<div class="dropdown" style="display: none;">
				<p>The surest way to see your listing is to create an account. It takes less than a minute, is free, and we don&rsquo;t clog your inbox. If you create an account and we don&rsquo;t recognize your email address, that means you either used a different email address than the one provided to us by Listhub, or you are not connected to Listhub.</p>
			</div>
		</div>
		<div class="question" id="9">
			<span id="title" class="odd">Can any listing be displayed on Lifestyled Listings?</span>
			<div class="dropdown" style="display: none;">
				<p>Currently, we only accept listings from licensed real estate agents (no For Sale by Owner) in the United States. We don&rsquo;t have absolute limits on minimum price but we do work to maintain standards of quality in our listings. Quality refers to the desirability of the location, the quality of the home and the actual images and description provided.</p>
			</div>
		</div>
		<div class="question" id="10">
			<span id="title" class="even">How do I know if my listing will qualify to be displayed?</span>
			<div class="dropdown" style="display: none;">
				<p>If you are a licensed real estate agent, your listing is of good quality, offers an attractive lifestyle, and is priced at $800,000 or greater you can be confident that it will qualify.</p>
			</div>
		</div>
		<div class="question" id="11">
			<span id="title" class="odd">I created an account but I don&rsquo;t see any of my listings.</span>
			<div class="dropdown" style="display: none;">
				<p>This either means you used an email address to sign up for your account that is different than the one you provide to listhub, or you have not approved Lifestyled Listings as a publisher for your data. We use the listhub provided email address as one of our security measures to prevent the wrong parties from accessing your listings on our site.<br><br>If your local MLS is a partner with Listhub, visit your agent account and approve Lifestyled Listings as a publisher for your listings. A complete list of listhub partnered MLSs can be seen <a href="https://www.listhub.com/mls-list.html" target="_blank">here</a>.</p>
			</div>
		</div>
		<div class="question" id="12">
			<span id="title" class="even">How can I add my listing to be displayed?</span>
			<div class="dropdown" style="display: none;">
				<p>You have two options. One, you can view the complete list of listhub partnered MLSs <a href="https://www.listhub.com/mls-list.html" target="_blank">here</a> to see if your local MLS is a serviced partner of Listhub. If so, you simply need to navigate to your agent dashboard at Listhub and approve Lifestyled Listings as a publisher, your data will be shared with us within 24 hours and updated automatically if you ever make a changes to it with your MLS. As soon as it is in our system you can make changes, add higher quality images, and assign tags through your Agent Dashboard.<br><br>Second, you can opt to create a listing from scratch using our Listing Wizard that guides you easily through the process of uploading photos at least 600 pixels wide, filling out the critical listing information, description, and the very important step of assigning the appropriate tags to your listing. If you have high quality video you can link it in the Listing Wizard.</p>
			</div>
		</div>
	</div>
</div>