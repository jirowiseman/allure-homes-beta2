
<?php
global $ALR; 
require_once(__DIR__.'/../_classes/Sellers.class.php');
require_once(__DIR__.'/../_classes/Reservations.class.php');
$s = new AH\Sellers;
$Reservations = new AH\Reservations;

$amTags = $s->getAMTagList();
$intro = new \stdClass();
$intro->id= 0;
$intro->tag = "Choose LifeStyle";
array_unshift($amTags, $intro);

$seller_id = get_query_var('id');
$seller = null;
if ( !empty($seller_id) ) {
	$seller = $s->get((object)['where'=>['id'=>$seller_id]]);
	if (!empty($seller)) {
		$seller = $seller[0];
	}
}
	
if (empty($seller)) {
	if (is_user_logged_in()) {
		$seller = $ALR->get('seller');	 
		if (empty($seller)) {
			$user = wp_get_current_user();
			if ($user && !is_wp_error( $user )) {
				$seller = new \stdClass();
				$seller->id = 0;
				$seller->first_name = $user->user_firstname;
				$seller->last_name = $user->user_lastname;
				$seller->email = $user->user_email;
			}
		}
	}
}
	
if (empty($seller)) {
	$seller = new \stdClass();
	$seller->id = 0;
	$seller->first_name = '';
	$seller->last_name = '';
	$seller->email = '';
}


$service_area = '';
$meta = null;
if (!empty($seller)) {
	if (isset($seller->service_areas) &&
		!empty($seller->service_areas)) {
		$areas = explode(":", $seller->service_areas);
		$areas = explode(',', $areas[0]);
		if (count($areas) == 2) {
			$city = trim($areas[0]);
			$state = trim($areas[1]);
			$service_area = $city.', '.$state;
		}
	}
	elseif (isset($seller->city) &&
			isset($seller->state) &&
			!empty($seller->city) &&
			!empty($seller->state))
		$service_area = $seller->city.", ".$seller->state;

	$reservation = !empty($seller->id) ?  $Reservations->get((object)['where'=>['email'=>$seller->email]]) : null;
	if ($reservation && !empty($reservation[0]->meta)) foreach($reservation[0]->meta as $data) 
		if ($data->action == ORDER_AGENT_MATCH) {
			$meta = $data;
			break;
		}
}

?>

<script type="text/javascript">
var amTagList = <?php echo json_encode($amTags); ?>;
var seller = <?php echo json_encode($seller); ?>;
var service_area = '<?php echo $service_area; ?>';
</script>

<div id="agent-reservation">
	<div id="left">
		<span id="title1">Become a <span style="color:#2289df">Lifestyled Agent</span></br></span>
		<span id="title2">Act Now, Exclusively Available to only 3 Agents Per Lifestyle</br></span>
		<span id="title3">Dominate Your Areas of Expertise By Being An Exclusive Agent in Your Area.</br></span>	
		<div class="desktopinman">
			<p style="margin-bottom:-.5em">Featured at</p>
			<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-agent-reservation/inman.png" />
		</div>
		<div id="main-graphics">
			<div class="step1">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-agent-reservation/step1.png" />
				<div class="steptext">
					<span class="number">1</span>
					<span class="title">Stand out from your peers</span>
					<p class="sub">Tell us what lifestyles you're an expert in</p>
				</div>
			</div>
			<div class="step2">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-agent-reservation/step2.png" />
				<div class="steptext">
					<span class="number">2</span>
					<span class="title">Work with your dream clients</span>
					<p class="sub">We match you to luxury buyers looking for homes with those lifestyles</p>
				</div>
			</div>
			<div class="step3">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-agent-reservation/step3.png" />
				<div class="steptext">
					<span class="number">3</span>
					<span class="title">Sell more luxury homes</span>
					<p class="sub">You represent buyers on homes that are a minimum of $800K</p>
				</div>
			</div>
		</div>
	</div>
	<div class="mobileinman">
			<p style="margin-bottom:-.5em">Featured at</p>
			<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-agent-reservation/inman.png" />
		</div>
	<!--<span class="toptext"><span style="color:#333;margin-right:10px;font-size:.9em">Regularly</span><span style="color:#2289df;font-size:1.6em">$149.<span style="font-size:.7em">00</span></span><span style="color:#333;font-size:.8em">/Month</span></span-->
	<div id="information">
		<div id="message">
			<span id="line1">Join Our Team</span>
			<!-- <span id="line2">Early Adopter Price</span>
			<span id="line3">$99 / Month</span> -->
			<span id="line4">No payment required at this time</span>
		</div>
		<table id="user-data">
			<tbody>
				<tr>
					<td><label>First</label></td><td><input type="text" id="first_name" placeholder="first name" value="<?php echo (!empty($seller) && isset($seller->first_name) && !empty($seller->first_name) ? $seller->first_name : ''); ?>" /></td>
				</tr>
				<tr>
					<td><label>Last</label></td><td><input type="text" id="last_name" placeholder="last name" value="<?php echo (!empty($seller) && isset($seller->last_name) && !empty($seller->last_name) ? $seller->last_name : ''); ?>" /></td>
				</tr>
				<tr>
					<td><label>Phone</label></td><td><input type="text" id="phone" name="phone" placeholder="phone" value="<?php echo (!empty($seller) && isset($seller->phone) && !empty($seller->phone) ? $seller->phone : ''); ?>" /></td>
				</tr>
				<tr>
					<td><label>Email</label></td><td><input type="text" id="email" placeholder="email" value="<?php echo (!empty($seller) && isset($seller->email) && !empty($seller->email) ? $seller->email : ''); ?>" /></td>
				</tr>
				<tr>
					<td><label>City</label></td><td><input type="text" class="service_areas" placeholder="City" value="<?php echo (!empty($service_area) ? $service_area : '');?>" /></td>
				</tr>
			</tbody>
		</table>
		<div id="lifestyles">
				<select class="field_expertise" style="margin:20px auto 7px" for="1" >
						<?php 
						$h = '';
						$index = $meta && count($meta->item)? $meta->item[0]->specialty : -1;
						foreach($amTags as $id=>$tag)
							$h .= '<option value="'.$id.'" '.($index != -1 && $index == $tag->id ? 'selected' : '').' >'.$tag->tag.'</option>';
						$h .= '</select>';
						echo $h;
						?>
				<select class="field_expertise" for="2" >
						<?php 
						$h = '';
						$index = $meta && count($meta->item) > 1? $meta->item[1]->specialty : -1;
						foreach($amTags as $id=>$tag)
							$h .= '<option value="'.$id.'" '.($index != -1 && $index == $tag->id ? 'selected' : '').'>'.$tag->tag.'</option>';
						$h .= '</select>';
						echo $h;
						?>
				<select class="field_expertise" style="margin:7px auto 20px" for="3" >
						<?php 
						$h = '';
						$index = $meta && count($meta->item) > 2? $meta->item[2]->specialty : -1;
						foreach($amTags as $id=>$tag)
							$h .= '<option value="'.$id.'" '.($index != -1 && $index == $tag->id ? 'selected' : '').'>'.$tag->tag.'</option>';
						$h .= '</select>';
						echo $h;
						?>
		</div>
		<div class="reserve-now">
			<button id="reserve">Reserve Spot</button>
			<span class="reserveportal">Learn more about getting your own Lifestyled <a href="javascript:;" id="checkPortal">AGENT PORTAL</a></span>
			<span id="disclaimer">*Reserving your spot does not guarantee you will be accepted.&nbsp;On a first come basis, each agent will be verified of their expertise.<span>
		</div>
	</div>
</div>