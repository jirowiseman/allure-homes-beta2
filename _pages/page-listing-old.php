<?php
$listing_id = get_query_var('id');
$extra = get_query_var('extra');

if ( empty($listing_id) ) :
	?>
	<h2>Query not set.</h2>
	<?php
else :
	require_once(__DIR__.'/../_classes/Utility.class.php');
	require_once(__DIR__.'/../_classes/Listings.class.php'); global $Listings; $Listings = new AH\Listings(1);
	require_once(__DIR__.'/../_classes/ListingsGeoinfo.class.php'); global $ListingsGeoinfo; $ListingsGeoinfo = new AH\ListingsGeoinfo();

	function userIP() {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		    $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}

	function trackViewer($listing, $userId) {
		global $Listings;
		$myTagList = isset($_COOKIE['TagsForListing']) && !empty($_COOKIE['TagsForListing']) ? $_COOKIE['TagsForListing'] : '0';
		if (!empty($listing)) {
			// update the seller meta data with userId and IP and date
			$ip = userIP();
			$meta = array();
			$visits = null;
			if (!empty($listing->meta)) foreach($listing->meta as $data) {
				if ($data->action != LISTING_VISITATIONS)
					$meta[] = $data;
				else {
					$visits = $data;
					// $visits->users = json_decode($visits->users, true);
				}
			}
			$newVisit = false;
			if (!$visits) {
				$visits = new \stdClass();
				$visits->action = LISTING_VISITATIONS;
				$visits->users = array();
				$newVisit = true;
			}

			$gotOne = false;
			$gotDate = false;
			$needUpdate = false;
			$stamp = date("Y-m-d", time() + ($Listings->timezone_adjust*3600));
			// $visit == array of user IDs.  Each of those are arrays of IPs, Each IP is an array of dates
			if (count($visits->users)) 
				foreach($visits->users as $id=>&$ips) {
					$id = intval($id);
					if ($id == $userId) {
						foreach($ips as $addr=>&$theDates) {
							$theDates = (array)$theDates;
							if ($addr == $ip) {						
								foreach($theDates as $date=>$data)
									if ( $date == $stamp) {
										$gotDate = true;
										foreach($data as $tagList) {
											if ($tagList == $myTagList) {
												$gotOne = true;
												$Listings->log("trackViewer - user:$userId, already have for $ip, date:$stamp, tagList:$myTagList");
												break;
											}
										}
									}

								if (!$gotDate) {
									$theDates[] = [$stamp=>[$myTagList]];
									$Listings->log("trackViewer - user:$userId, new date added for $ip, date:$stamp, tagList:$myTagList");
									$gotOne = true;
									$needUpdate = true;
								}
								elseif (!$gotOne) { // then got date, but not this tagList
									$theDates[$stamp] = (array)$theDates[$stamp];
									$theDates[$stamp][] = $myTagList;
									$Listings->log("trackViewer - user:$userId, adding new tags for $ip, date:$stamp, tagList:$myTagList");
									$gotOne = true;
									$needUpdate = true;
								}
							}
						}
						if (!$gotOne) { // then same user, but different IP
							$ips = (array)$ips;
							$ips[$ip] = [$stamp=>[$myTagList]];
							$ips = (object)$ips;
							$Listings->log("trackViewer - user:$userId, adding new $ip, date:$stamp, tagList:$myTagList");
							$gotOne = true;
							$needUpdate = true;
						}
						break;
					}
				}

			if (!$gotOne) { // fresh visit
				$users = (array)$visits->users;
				$users[$userId] = (object)[$ip=>[$stamp=>[$myTagList]]];
				$visits->users = (object)$users;
				$Listings->log("trackViewer - new user:$userId, adding new $ip, date:$stamp, tagList:$myTagList");
				$needUpdate = true;
			}
			if ($needUpdate) {
				// $visits->users = json_encode($visits->users);
				$meta[] = $visits;
				$Listings->set([(object)['fields'=>['meta'=>$meta],
										'where'=>['id'=>$listing->id]]]);
			}
		}		
	}

	$isAdmin = 0;
	$track = 0;
	$trackId = 0;
	$tag = 0;
	$sampleView = false;
	$newTab = false;
	$timezone_adjust = 7;
	$isCityGeo = false;

	// $browser = AH\getBrowser();
	global $browser;

	if (strpos($listing_id, "-") != false) {
		$parts = explode("-", $listing_id);
		$listing_id = $parts[1];
		
		if (strpos($parts[0], "S") !== false) { // sample view
			$tag = substr($parts[0], 1);
			$sampleView = true;
			$newTab = true;
		}
		elseif (strpos($parts[0], "N") !== false) { // new tab view
			$newTab = true;
		}
		elseif (strpos($parts[0], "T") !== false) {
			$trackId = substr($parts[0], 1);
			require_once(__DIR__.'/../_classes/EmailTracker.class.php'); $et = new AH\EmailTracker();
			$track = $et->get((object)['where'=>['id'=>$trackId]]);
			if (!empty($track)) {
				$track = array_pop($track);
			}
		}
		elseif (strpos($parts[0][0], "L") !== false) { // from social media sharing link
			$referer = isset($_SERVER["HTTP_REFERER"]) ? (!empty($_SERVER["HTTP_REFERER"]) ? strtolower($_SERVER["HTTP_REFERER"]) : "") : '';
			$date = date("Y-m-d H:i:s", time() - ($timezone_adjust*3600));
			global $browser;
			require_once(__DIR__.'./../_classes/Analytics.class.php'); $Analytics = new AH\Analytics();
			require_once(__DIR__.'./../_classes/SessionMgr.class.php'); $SessionMgr = new AH\SessionMgr();
			$id = 0;
			$sessionID = $SessionMgr->getCurrentSession($id);
			$from = '';
			$ch = $parts[0][1];
			if (strlen($parts[0]) > 1) switch($ch) {
				case 'F': $from = "Facebook"; break;
				case 'I': $from = "LinkedIn"; break;
				case 'G': $from = "Google+"; break;
				case 'T': $from = "Twitter"; break;
				case 'M': $from = "Email"; break;
				case 'C': $from = "Contact"; break;
			}
			$Analytics->add(['type'=>ANALYTICS_TYPE_EVENT,
							 'origin'=>'listing',
							 'session_id'=>$id,
							 'referer'=>$referer,
							 'what'=>'sharing',
							 'value_str'=>$from,
							 'value_int'=>$listing_id,
							 'added'=>$date,
							 'browser'=>$browser['shortname'],
							 'connection'=>$browser,
							 'ip'=>userIP()]);
		}
		elseif ($parts[0][0] == '1') // admin view
			$isAdmin = intval($parts[0]);
	}

	$userId = wp_get_current_user()->ID;
	$x = $Listings->get((object)[ 'where'=>[ 'id'=>$listing_id ] ]);
	$first_image = '';
	if (empty($x)) :
		?>
		<h2>Unable to find listing <?php echo $listing_id; ?></h2>
		<?php
	else :
		$x = array_pop( $x );
		$baths = floor($x->baths);
		$baths += floor( (fmod(floatval($x->baths), 1)*1000)+0.1 ) > 0 ? 0.5 : 0;
		trackViewer($x, $userId);

		if (!empty($x->images)) foreach($x->images as &$img) {
			$img = (object)$img;
			$url = '';
			if (isset($img->file)) { $img->file = AH\removeslashes($img->file); $url = $img->file; }
			if (isset($img->url)) { $img->url = AH\removeslashes($img->url); if (empty($url)) $url = $img->url; }

			if (substr($url, 0, 4 ) != 'http' &&
				(!file_exists(__DIR__.'/../_img/_listings/uploaded/'.$url) ||
				 !file_exists(__DIR__.'/../_img/_listings/1440x714/'.$url)) &&
				isset($img->url)) {
				$img->file = $img->url;
			}

			if (empty($first_image) &&
				isset($img->file)) {
				if (substr($img->file, 0, 4 ) != 'http')
					$first_image = get_template_directory_uri().'/_img/_listings/900x500/'.$img->file;
				else
					$first_image = $img->file;
			}
		}
		$x->about = AH\removeslashes($x->about);

		$sql = "UPDATE ".$Listings->getTableName($Listings->table)." SET `clicks` = `clicks` + 1 WHERE `id` = ".$x->id;
		$Listings->rawQuery($sql);

		require_once(__DIR__.'/../_classes/Cities.class.php'); $Cities = new AH\Cities();
		$city_id = $x->city_id;
		if (empty($city_id) ||
			intval($city_id) == -1) {
			$city_id = $Cities->get((object)[ 'what' => ['id'], 'where'=>[ 'city'=>$x->city,
																			'state'=>$x->state ] ]);
			if (!empty($city_id))
				$x->city_id = $city_id = $city_id[0]->id;
		}
		if (!empty($x->city_id)) {
			$sql = "UPDATE ".$Cities->getTableName($Cities->table)." SET `clicks` = `clicks` + 1 WHERE `id` = ".$x->city_id;
			$Cities->rawQuery($sql);
		}

		require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new AH\Sellers(1);
		require_once(__DIR__.'/../_classes/ListingsTags.class.php'); $ListingsTags = new AH\ListingsTags();
		require_once(__DIR__.'/../_classes/Tags.class.php'); $Tags = new AH\Tags();

		$ListingsTags = $ListingsTags->getGroup((object)[ 'field'=>'tag_id', 'where'=>[ 'listing_id'=>$x->id ] ]);
		// add incoming sample tag id into the mix if it's not in there already
		if ($tag &&
			!in_array($tag, $ListingsTags))
			$ListingsTags[] = $tag;

		$x->tags = $Tags->get((object)[ 'what' => ['id','tag','type'], 'where'=>[ 'id'=>$ListingsTags ] ]);
		$gotCityTags = false;
		$tagList = [];
		if (!empty($x->tags)) foreach($x->tags as $tag) {
			$tagList[$tag->id] = $tag;
			if ($tag->type == 1) {
				$gotCityTags = true;
			}
		}
		unset($x->tags);
		$x->tags = $tagList;

		if (!$gotCityTags &&
			!empty($x->city_id)) {
			require_once(__DIR__.'/../_classes/CitiesTags.class.php'); $CitiesTags = new AH\CitiesTags();
			$city_tags = $CitiesTags->get((object)[ 'what'=>['tag_id','score'], 'where' => ['city_id' => $city_id] ]);
			if (!empty($city_tags)) {
				$goodTags = [];
				foreach($city_tags as $tag) {
					if (gettype($tag->score) == 'string')
						$tag->score = intval($tag->score);
					if ($tag->score >= 7 || $tag->score == -1)
						$goodTags[] = $tag->tag_id;
				}
				if (!empty($goodTags)) {
					$city_tags = $Tags->get((object)[ 'what' => ['id','tag','type'], 'where'=>[ 'id'=>$goodTags ] ]);
					if (!empty($city_tags))
						if (!empty($x->tags))
							$x->tags = array_merge($x->tags, $city_tags);
						else
							$x->tags = $city_tags;
				}
			}
		}
		unset($Tags, $ListingsTags);

		if ($x->author_has_account) $query = (object) [ 'where'=>[ 'author_id'=>$x->author ] ];
		else $query = (object) [ 'where'=>[ 'id'=>$x->author ] ];
		$agent = $Sellers->get($query);
		if (!empty($agent)) {
			$agent[0]->phone = AH\fixPhone($agent[0]->phone);
			$agent = array_pop($agent);
			$info = !empty($agent->author_id) ? get_userdata($agent->author_id) : null;
			$nickname = !empty($info) && !empty($info->nickname) ? $info->nickname : (!empty($info) && !empty($info->user_nicename) ? $info->user_nicename : 'unknown');
			$agent->nickname = stripos($nickname, 'unknown') === false ? $nickname : '';
			$agentPage = get_home_url().'/agent/'.(!empty($agent->nickname) ? $agent->nickname : $agent->author_id);
		}

		$images = [];
		if (!empty($x->images)) foreach ($x->images as &$image) {
			if (isset($image->discard)) continue; // discarded image
			if (isset($image->title)) unset($image->title);
			if (isset($image->desc) && $image->desc == 'blank') unset($image->desc);
			$images[] = $image;
		}

		$videoStr = null;
		if (!empty($x->video)) {
			if (gettype($x->video) == 'array')
				$videoStr = isset($x->video[0]->file) && !empty($x->video[0]->file) ? $x->video[0]->file : null;
			elseif (gettype($x->video) == 'object')
				$videoStr = isset($x->video->file) && !empty($x->video->file) ? $x->video->file : null;
			elseif (gettype($x->video) == 'string') {
				$videoStr = $x->video;
				if (strpos($videoStr, "ttp:") !== false) // no url supported directly
					$videoStr = null;
			}
		}

		if (!empty($videoStr))
		 	if ( ($pos = strpos($videoStr, "watch?v=")) !== false )
		 		$videoStr = substr($videoStr, $pos);
		 	elseif ( ($pos = strpos($videoStr, "v=")) !== false )
		 		$videoStr = "watch?".substr($videoStr, $pos);
		 	else
		 		$videoStr = "watch?v=".$videoStr;

		$video = !empty($videoStr) && AH\validYouTubeURL("www.youtube.com/".$videoStr) ? $videoStr : null;
		$Listings->log("listing:$listing_id has videoStr:$videoStr, video:$video, x:".(!empty($x->video) ? print_r($x->video, true) : "N/A"));

		$geoInfo = $ListingsGeoinfo->get((object)['where'=>['listing_id'=>$listing_id]]);
		if (empty($geoInfo) ||
			$geoInfo[0]->lng == -1) {
			if ( !empty($x->city_id) ) {
				$geoInfo = $Cities->get((object)[ 'what' => ['id','lng','lat'], 'where'=>[ 'id' => $x->city_id ]]);
				$isCityGeo = !empty($geoInfo) && $geoInfo[0]->lng != -1;
			}
		}

		$Listings->log("geocode for $listing_id:".(!empty($geoInfo) ? print_r($geoInfo, true) : "N/A") );
		
		$listing = (object)[
			'id' => $listing_id,
			'street_address' => $x->street_address,
			'city' => $x->city,
			'city_id'=> $x->city_id,
			'state' => $x->state,
			'country' => $x->country,
			'price' => $x->price,
			'beds' => $x->beds,
			'baths' => $x->baths,
			'lotsize_std' => $x->lotsize_std,
			'lotsize' => $x->lotsize,
			'interior_std' => $x->interior_std,
			'interior' => $x->interior,
			'tags'=> $x->tags,
			'error' => $x->error,
			'author_has_account' => $x->author_has_account,
			'author' => $x->author,
			'listhub_key' => $x->listhub_key,
			'active' => $x->active,
			'url' => $x->url,
			'disclaimer' => $x->disclaimer,
			'video' => $video,
			'title' => $x->title,
			'lng' => !empty($geoInfo) ? $geoInfo[0]->lng : -1,
			'lat' => !empty($geoInfo) ? $geoInfo[0]->lat : -1
		];

		$listingAgentDetail = '<h4>Listing Agent Detail</h4>';
		if (!empty($agent)) {
			$listingAgentDetail .= '<div id="agent-detail">';
			if (!empty($agent->phone))
				$listingAgentDetail .= '<span class="phone">'.$agent->phone.'</span><a href="tel:'.$agent->phone.'" class="phone mobile">'.$agent->phone.'</a>';

			if (!empty($agent->first_name) ||
			 	!empty($agent->last_name)) {
				$listingAgentDetail .= !empty($agent->phone) ? '<span class="desktopspace">&nbsp;&nbsp;</span><span class="mobilespace"><br/></span>' : '';
				$listingAgentDetail .= '<a href="'.$x->url.'" target="_blank">'."$agent->first_name $agent->last_name".'</a>';
			}
			$listingAgentDetail .= '<br/><br/></div>';
		}
		$listingAgentDetail .= !empty($x->disclaimer) ? $x->disclaimer : '';

		// portal agent
		global $ALR; 
		$isPortal = true;
		$portalAgent = $ALR->get('portal-agent');
		if ($portalAgent == "0")
			$isPortal = false;

		$premierAgents = $Sellers->premierAgents($x->tags, $x->city_id, $portalAgent, $x->id, $sampleView, $userId);
		// }
		// else
		// 	$premierAgent = [$premierAgent];

		if (!empty($track)) {
			$today = date('Y-m-d H:i:s', time() - (7*3600));
			$track->listhub_key = $listing->listhub_key;
			$meta = new \stdClass();
			$meta->action = ET_LISTING_VIEWED;
			$meta->date = $today;
			$meta->listing = $listing_id;
			if ( !isset($track->meta) || 
				 empty($track->meta) )
				$track->meta = [$meta]; 
			else
				$track->meta[] = $meta;
			$et->set([(object)['where'=>['id'=>$track->id],
							   'fields'=>['meta'=>$track->meta]]]);
			require_once(__DIR__.'/../_classes/SellersEmailDb.class.php'); $SellersEmailDb = new AH\SellersEmailDb();
			if ( $SellersEmailDb->exists((object)['seller_id'=>$track->agent_id])) {
				$sellerEmailDb = $SellersEmailDb->get((object)['where'=>['seller_id'=>$track->agent_id]]);
				if (!empty($sellerEmailDb)) {
					$fields = [];
					if ( ($sellerEmailDb[0]->flags & RESPONDED_TO_VIEW_LISTING) == 0) {
						$sellerEmailDb[0]->flags |= RESPONDED_TO_VIEW_LISTING;
						$fields['flags'] = $sellerEmailDb[0]->flags;
					}

					$respondMeta = new \stdClass();
					$respondMeta->action = VIEWED_LISTING;
					$respondMeta->date = $today;
					$respondMeta->emailTrackerId = $track->id;
					$respondMeta->listing = $listing_id;
					if (empty($sellerEmailDb[0]->meta))
						$sellerEmailDb[0]->meta = [$respondMeta];
					else {
						$sellerEmailDb[0]->meta = (array)$sellerEmailDb[0]->meta; // force to array
						$sellerEmailDb[0]->meta[] = $respondMeta;
					}
					$fields['meta'] = $sellerEmailDb[0]->meta;

					$SellersEmailDb->set([(object)['where'=>['id'=>$sellerEmailDb[0]->id],
												   'fields'=>$fields]]);
					unset($fields, $sellerEmailDb);
				}
			}
			require_once(__DIR__.'/../_classes/Email.class.php'); $Email = new AH\Email();
			$msg = "Seller - id:$agent->id, $agent->first_name $agent->last_name, looked at their listing:$listing_id";
			$Email->sendMail(SUPPORT_EMAIL, 'Agent viewed listing', $msg,
							'Tracking views',
							"Seller - id:$agent->id viewed listing:$listing_id");
		}

		$haveAgentMatchLogo = file_exists(__DIR__.'/../_img/_sellers/agent-match-logo.png');

		$emailBody = 'Go check out this listing at Lifestyled Listings!  Copy and paste into browser: '.get_home_url().'/listing/LM-'.$listing_id;
		$emailSubject = ('Check out this listing at '.$listing->street_address);
		$emailHref = "mailto:?subject=$emailSubject&body=$emailBody";
?>
<script type="text/javascript">
	var gallery_images = <?php echo json_encode($images); ?>;
	var listing_info = <?php echo json_encode($listing); ?>;
	var seller = <?php echo json_encode($agent); ?>;
	var listingAgentDetail = '<?php echo rawurlencode($listingAgentDetail); ?>';
 	// var listingAgentDetail = '<?php echo $listingAgentDetail; ?>';
 	var premierAgents = <?php echo $premierAgents != "0" ? json_encode($premierAgents) : "0"; ?>;
	var portalAgent = <?php echo $portalAgent != "0" ? json_encode($portalAgent) : "0"; ?>;
	var isPortal = <?php echo $isPortal ? 1 : 0; ?>;
	var emailTracker = <?php echo empty($track) ? 0 : json_encode($track); ?>;
	var etId = <?php echo $trackId; ?>;
	var haveAgentMatchLogo = <?php echo $haveAgentMatchLogo ? 1 : 0; ?>;
	var userID = '<?php echo $userId; ?>';
	var isCityGeo = <?php echo $isCityGeo ? 1 : 0; ?>;
	var command = '<?php echo !isset($extra) || empty($extra) ? "none" : $extra; ?>';

	if (ah_local.ip != "1")
		lh('submit', 'DETAIL_PAGE_VIEWED', {lkey:'<?php echo $x->listhub_key; ?>'});
	
	jQuery(document).ready(function($){
		$('.mobile-listing-meta .seemore').on('click', function() {
			$('.mobile-listing-meta .abouttext').show();
			$('.mobile-listing-meta .abouttexthidden').hide();
			$('.mobile-listing-meta .seemore').hide();
			$('.mobile-listing-meta .seeless').show();
		});
		$('.mobile-listing-meta .seeless').on('click', function() {
			$('.mobile-listing-meta .abouttext').hide();
			$('.mobile-listing-meta .abouttexthidden').show();
			$('.mobile-listing-meta .seeless').hide();
			$('.mobile-listing-meta .seemore').show();
		});
	});
</script>
<div class="listing">
	<div class="videoDiv" id="listing" style="display: none;">
	  <button class="closeVid">Close</button>
	  <!-- <a id="closeVid" href="javascript:reg.closeFrame();">[X]</a>
	  <iframe src="<?php echo !empty($listing->video) ? "http://www.youtube.com/embed/".$listing->video : null; ?>" width="900" height="600" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen>
	  </iframe> -->
	  <video id="listing" class="video-js vjs-default-skin" controls
			 preload="auto" width="900" height="600" 
			 data-setup='{"techOrder":["youtube"], "src":"<?php echo !empty($listing->video) ? "http://www.youtube.com/".$listing->video : null; ?>"}'>
			
			<p class="vjs-no-js">
			    To view this video please enable JavaScript, and consider upgrading to a web browser
			    that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
			</p>
	  </video>
	</div>	
	<?php if (!$newTab && $isAdmin == 0 && empty($track) ): ?>
	<!-- <a href="<?php bloginfo('wpurl'); ?>/quiz-results/" class="exit-page"><span class="entypo-left-open-big" style="font-size:.6em;left:102px;position:absolute;"></span><p>Return to Profile</p></a> -->
	<a href="javascript:window.history.back();" class="exit-page">
		<span class="desktop"><span class="entypo-left-open-big" style="font-size:.6em;left:102px;position:absolute;"></span><p>Return to Results</p></span>
		<span class="mobile"><p><span class="entypo-left-open-big" style="font-size:.6em;left:102px;position:absolute;"></span> Return to Results</p></span>
	</a>
	<?php else: ?>
		<?php if ($isAdmin) : ?>
			<a href="javascript:window.history.go(-2);" class="exit-page"><span class="entypo-left-open-big"></span> &nbsp;Close Window</a>
		<?php else: ?>
			<a href="javascript:window.close();" class="exit-page"><span class="entypo-left-open-big"></span> &nbsp;Close Window</a>
		<?php endif; ?>
	<?php endif; ?>
	<a class="gallery-full"><span class="entypo-resize-full"></span><p>All Images</p><span class="subtitle">Full Screen</span></a>
	<section>
		<ul class="listing-gallery">
			<?php $count = 0;
				if ($x->images) foreach ($x->images as $i=>$img){
				if ($count >= 6) break;
				if (isset($img->file) && substr($img->file, 0, 4 ) == 'http') {
				?><li class="listing-image"><img src="<?php echo $img->file; ?>" /></li><?php
					if ($count == 0)
						$first = $img->file;
					$count++;
				}
				elseif (isset($img->discard))
					continue;
				else {
				?><li class="listing-image"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/_listings/1440x714/<?php echo $img->file; ?>" /></li><?php
				if ($count == 0)
					$first = get_bloginfo('stylesheet_directory','raw').'/_img/_listings/1440x714/'.$img->file;
				$count++;
				}
			} ?>
		</ul>
		<div class="listing-meta" >
			<?php
			$listingHTML = '<table>';
			//$listingHTML .= '<tr><td class="title" colspan="2">'.strlen($x->title) > 40 ? substr($x->title, 0, 40) : $x->title.'</td></tr>';
			if ($isAdmin ||
				$x->flags & LISTING_PERMIT_ADDRESS )
				$listingHTML .= '<tr><td class="street_address" colspan="2">'.$x->street_address.'</td></tr>';
			else
				$listingHTML .= '<tr><td class="street_address" colspan="2">'.'Address hidden'.'</td></tr>';

			$listingHTML .= '<tr><th>Location</th><td>';
				if ($x->city != '' && $x->city != -1)
					$listingHTML .= $x->city.', ';
				if ($x->state != '' && $x->state != -1)
					$listingHTML .= $x->state;
			$listingHTML .= '</td></tr>';
			if (!empty($x->country) && $x->country != 'US') $listingHTML .= '<tr><th>Country</th><td>'.$x->country.'</td></tr>';
			$listingHTML .= '<tr><th>Offered Price</th><td>$'.number_format($x->price).'</td></tr>';
			if ($x->lotsize > 0){
				if ($x->lotsize_std == 'meters') $x->lotsize_std = 'm<sup>2</sup>';
				else if ($x->lotsize_std == 'acres') $x->lotsize_std = 'acres';
				else $x->lotsize_std = 'ft<sup>2</sup>';
				$listingHTML .= '<tr><th>Lot Size</th><td>'.number_format($x->lotsize).' '.$x->lotsize_std.'</td></tr>';
			}
			if ($x->interior > 0){
				if ($x->interior_std == 'meters') $x->interior_std = 'm<sup>2</sup>';
				else if ($x->interior_std == 'acres') $x->interior_std = 'acres';
				else $x->interior_std = 'ft<sup>2</sup>';
				$listingHTML .= '<tr><th>Interior</th><td>'.number_format($x->interior).' '.$x->interior_std.'</td></tr>';
			}
			$listingHTML .= '<tr><th>Beds</th><td>'.$x->beds.'</td></tr>';
			$x->baths = explode('.', $x->baths);
			$listingHTML .= '<tr><th>Baths (Full / Partial)</th><td>'.intval($x->baths[0]).' / '.(!empty($x->baths[1]) ? intval($x->baths[1]) : 0).'</td></tr>';
			$listingHTML .= '</table>';
			echo $listingHTML;
			?>
			<a href="javascript:listings.gotoExploreMap();">
				<div class="explore"></div>
			</a>
		</div>
		<div class="mobile-listing-meta" >
			<?php
			$listingHTML = '<div class="mobileinfo">';
			//$listingHTML .= '<tr><td class="title" colspan="2">'.strlen($x->title) > 40 ? substr($x->title, 0, 40) : $x->title.'</td></tr>';
			if ($isAdmin ||
				$x->flags & LISTING_PERMIT_ADDRESS )
				$listingHTML .= '<div class="street_address" colspan="2">'.$x->street_address.'</div>';
			else
				$listingHTML .= '<div class="street_address" colspan="2">'.'Address hidden'.'</div>';

			$listingHTML .= '<div class="city">';
				if ($x->city != '' && $x->city != -1)
					$listingHTML .= $x->city.', ';
				if ($x->state != '' && $x->state != -1)
					$listingHTML .= $x->state;
			$listingHTML .= '</div>';
			$listingHTML .= '<div class="price">$'.number_format($x->price).'</div>';
			$listingHTML .= '<div class="bottominfo"><div id="boxes" class="beds"><span class="toptext">'.$x->beds.'</span><span>Beds</span></div>';
			$listingHTML .= '<div id="boxes" class="baths"><span class="toptext">'.intval($x->baths[0]).' / '.(!empty($x->baths[1]) ? intval($x->baths[1]) : 0).'</span><span>Baths</span></div>';
			if ($x->lotsize > 0){
				if ($x->lotsize_std == 'meters') $x->lotsize_std = '<span class="smalltext">m<span class="tinytext">2</span></span>';
				else if ($x->lotsize_std == 'acres') $x->lotsize_std = '<span class="smalltext">acres</span>';
				else $x->lotsize_std = '<span class="smalltext">ft<span class="tinytext">2</span></span>';
				$listingHTML .= '<div id="boxes" class="lotsize"><span class="toptext">'.number_format($x->lotsize).' '.$x->lotsize_std.'</span><span>Lot Size</span></div>';
			}
			if ($x->interior > 0){
				if ($x->interior_std == 'meters') $x->interior_std = '<span class="smalltext">m<span class="tinytext">2</span></span>';
				else if ($x->interior_std == 'acres') $x->interior_std = '<span class="smalltext">acres</span>';
				else $x->interior_std = '<span class="smalltext">ft<span class="tinytext">2</span></span>';
				$listingHTML .= '<div id="boxes" class="interior"><span class="toptext">'.number_format($x->interior).' '.$x->interior_std.'</span><span>Home Size</span></div></div>';
			}
			$listingHTML .= '</div>';
			echo $listingHTML;
			?>
			<div id="shareDiv">
				<a id="facebook-share" href="javascript:shareSocialNetwork('FB',1,'https://www.facebook.com/sharer/sharer.php?u=<?php echo get_home_url().'/listing/LF-'.$listing_id; ?>')" target="_blank"><img src="<?php echo get_template_directory_uri().'/_img/_promo/facebook.png'; ?>" /></a>
				<a id="linkedin-share" href="javascript:shareLinkedIn({	comment:'Hi friends check out this listing!', title:'Another listing from LifeStyled Listings', description:'<?php echo (!empty($listing->title) ? $listing->title : $listing->street_address.', '.$listing->city).(!empty($listing->price) ? ', $'.$listing->price : '').(!empty($listing->beds) ? ', Beds:'.$listing->beds : '').(!empty($baths) ? ", Baths:".number_format($baths, 1) : ''); ?>', url:'<?php echo get_home_url().'/listing/LI-'.$listing_id; ?>', img:'<?php echo $first_image; ?>', where: 1});" target="_blank"><img src="<?php echo get_template_directory_uri().'/_img/_promo/linkedin.png'; ?>" /></a>
				<a id="googleplus-share" href="javascript:shareSocialNetwork('GP',1,'https://plus.google.com/share?url=<?php echo get_home_url().'/listing/LG-'.$listing_id; ?>')" target="_blank"><img src="<?php echo get_template_directory_uri().'/_img/_promo/googleplus.png'; ?>" /></a>
				<a id="twitter-share" href="javascript:shareSocialNetwork('TW',1,'https://twitter.com/share?url=<?php echo get_home_url().'/listing/LT-'.$listing_id; ?>')" target="_blank"><img src="<?php echo get_template_directory_uri().'/_img/_promo/twitter.png'; ?>" /></a>
				<!-- <a id="email-share" href="mailto:%20?subject=<?php echo $emailSubject; ?>&body=<?php echo $emailBody; ?>"><img src="<?php echo get_template_directory_uri().'/_img/_promo/email.png'; ?>" /></a> -->
				<a id="email-share" href="javascript:shareSocialNetwork('EM',1,'<?php echo $emailHref; ?>');"><img src="<?php echo get_template_directory_uri().'/_img/_promo/email.png'; ?>" /></a>
				<a id="client-share" href="javascript:;">Share with client</a>
				<a id="video-view" href="#videoPosition"><span class="video" id="listing" style="background: none; display: <?php echo empty($listing->video) ? "none;" : "inline;"?>">Watch Video<span class="entypo-play" style="font-size: .8em;margin-left: 8px;"></span></span></a>
			</div>
			<a href="javascript:listings.gotoExploreMap();">
				<div class="explore"></div>
			</a>
			<div class="seemore">Read More<span class="entypo-down-open-big"></span></div><div class="abouttexthidden"><span><?php echo $x->about; ?></span></div><div class="abouttext" style="display: none;"><span><?php echo $x->about; ?></span><div class="seeless" style="display: none;"><span class="entypo-up-open-big"></span>Collapse</div></div>
		</div>
		<script type="text/javascript">
			$.fn.capitalise = function() {
				return this.each(function() {
					var $this = $(this),
							text = $this.text(),
							split = text.split(' '),
							res = [],
							i,
							len,
							component;
					for (i = 0, len = split.length; i < len; i++) {
							component = split[i];
							res.push(component.substring(0, 1).toUpperCase());
							res.push(component.substring(1).toLowerCase());
							res.push(" "); // put space back in
					}
					$this.text(res.join(''));
				});
			};
			$('.listing>section .mobile-listing-meta .mobileinfo .street_address').capitalise();
		</script>
	</section>
	<?php
	if ( current_user_can('add_users') &&
		 current_user_can('manage_options') && // then pretty damn sure this is being called by admin
		 $isAdmin == 1 ) : // then came from admin's Developer/listing section
	?>
	<section class="adminControl"></section>
	<?php endif; ?>
	<div class="listing-info" id="videoPosition">
		<div class="listing-about">
			<h3>About</h3>
			<div class="discover-this-listing"><span class="subtitle1">This</span><br/><span class="subtitle2">Listing</span></div>
			<div id="shareDiv">
				<a id="facebook-share" href="javascript:shareSocialNetwork('FB',1,'https://www.facebook.com/sharer/sharer.php?u=<?php echo get_home_url().'/listing/LF-'.$listing_id; ?>')" target="_blank"><img style="width:32px; height:32px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/facebook.png'; ?>" /></a>
				<a id="linkedin-share" href="javascript:shareLinkedIn({	comment:'Hi friends check out this listing!', title:'Another listing from LifeStyled Listings', description:'<?php echo (!empty($listing->title) ? $listing->title : $listing->street_address.', '.$listing->city).(!empty($listing->price) ? ', $'.$listing->price : '').(!empty($listing->beds) ? ', Beds:'.$listing->beds : '').(!empty($baths) ? ", Baths:".number_format($baths, 1) : ''); ?>', url:'<?php echo get_home_url().'/listing/LI-'.$listing_id; ?>', img:'<?php echo $first_image; ?>', where: 1});" target="_blank"><img style="width:32px; height:32px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/linkedin.png'; ?>" /></a>
				<a id="googleplus-share" href="javascript:shareSocialNetwork('GP',1,'https://plus.google.com/share?url=<?php echo get_home_url().'/listing/LG-'.$listing_id; ?>')" target="_blank"><img style="width:32px; height:32px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/googleplus.png'; ?>" /></a>
				<a id="twitter-share" href="javascript:shareSocialNetwork('TW',1,'https://twitter.com/share?url=<?php echo get_home_url().'/listing/LT-'.$listing_id; ?>')" target="_blank"><img style="width:32px; height:32px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/twitter.png'; ?>" /></a>
				<!-- <a id="email-share" href="mailto:%20?subject=<?php echo $emailSubject; ?>&body=<?php echo $emailBody; ?>"><img style="width:32px; height:32px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/email.png'; ?>" /></a> -->
				<a id="email-share" href="javascript:shareSocialNetwork('EM',1,'<?php echo $emailHref; ?>');"><img style="width:32px; height:32px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/email.png'; ?>" /></a>
				<a id="client-share" href="javascript:;">Share with client</a>
				<a id="video-view" href="#videoPosition"><span class="video" id="listing" style="background: none; display: <?php echo empty($listing->video) ? "none;" : "inline;"?>">Watch Video<span class="entypo-play" style="font-size: .8em;margin-left: 8px;"></span></span></a>
			</div>
			<!--<div class="favorite">
				Favorite this home
				<a href="#">
					<img class="bottom" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/favorite.jpg" style="cursor:pointer" />
					<img class="top" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/favorite-on.jpg" style="cursor:pointer" />
				</a>
			</div>-->
			<div class="text scrollbar">
				<?php echo $x->about; ?>
			</div>
		</div>
		<div class="agent">
			<h3>Listing Agent</h3>
			<div class="meta">
				<span class="mobileimg">
					<?php if (!empty($agent) && !empty($agent->author_id))
						echo '<a href="javascript:agentProfile('."'".$agentPage."'".');" target="_blank">'; ?>
					<img src="<?php bloginfo('template_directory'); ?>/_img/_authors/<?php  echo !$isPortal ? '250x250/' :  '250x250/'; ?><?php echo $agent && $agent->photo ? $agent->photo : '_blank.jpg' ; ?>" />
					<?php if (!empty($agent) && !empty($agent->author_id))
						echo '</a>'; ?>
				</span>
				<span class="mobiletext">
					<h4 class="name"><?php echo ($agent && $agent->first_name ? $agent->first_name : '').' '.($agent && $agent->last_name ? $agent->last_name : ''); ?></h4>
					<!-- <span class="phone"><?php echo $agent && strlen($agent->phone) ? $agent->phone : ''; ?></span> -->
					<span class="company"><?php echo $agent && $agent->company ? $agent->company : ''; ?></span>
					<?php if (!empty($agent))
					echo '<a href="javascript:;" class="contact-button" for="'.$agent->id.'">Email Me</a>';
					?>
				</span>
			</div>
		</div>
		<?php if ($isPortal) : ?>
		<div class="premier-agent">
			<h3>Have a question?</h3>
			<p>I'm here to help you</p>
			<a href="javascript:agentProfile('<?php echo get_home_url().'/agent/'.(empty($portalAgent->nickname) ? $portalAgent->author_id : $portalAgent->nickname); ?>');"><img src="<?php bloginfo('template_directory'); ?>/_img/_authors/250x250/<?php echo $portalAgent->photo ? $portalAgent->photo : '_blank.jpg' ; ?>" /></a>
			<div class="meta">
				<h4 class="name"><?php echo $portalAgent->first_name.' '.$portalAgent->last_name; ?></h4>
				<span class="company"><?php echo $portalAgent->company; ?></span>
				<span class="phone"><?php echo strlen($portalAgent->phone) ? $portalAgent->phone : ''; ?></span>
				<a href="javascript:;" class="contact-button" for="<?php echo $portalAgent->id; ?>">Contact Me</a>
			</div>
		</div>
		<?php endif; ?>
	 	<?php if ($premierAgents != "0") : ?>
		<div class="premier-agents">
			<div id="header" style="display: inline;">
				<h1 class="premier-agent-label"><span class="line1"></span><p class="title">Agents</p><span class="line2"></span></h1>
				<div class="sortlanguage"><label id="languageLabel" style="display: none;">Sort by language: </label><select name="languages" id="languages" style="display: none;"></select></div>
				<span id="agents-header">Local Agents that are experts in your lifestyle</span>
			</div>
			<div id="mobileheader" style="display: inline;">
				<h1 class="premier-agent-label"></span><p class="title">Lifestyled Agents</p></span></h1>
				<!--<div class="sortlanguage"><label id="languageLabel" style="display: none;">Sort by language: </label><select name="languages" id="languages" style="display: none;"></select></div>-->
				<span id="agents-header">Meet some real estate experts that share and love your lifestyle.</span>
			</div>
	 		<div class="agent-list" style="overflow: hidden; height: 100%; margin-top: 1.2em;"><ul class="premier-agents-list" style="display: inline; overflow: auto;" /></div>
		</div>
	 	<?php endif; ?>
		<a href="javascript:listings.gotoExploreMap();">
			<div class="explore">
				<div class="text">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/explore-icon.png" />
					<span class="title">Discover what's around this listing</span>
					<p>Check out all the local hotspots and activity in the area.</p>
					<p>Click here to discover the world around your listing.</p>
				</div>
			</div>
		</a>
    <!--
		<div class="video">
      <div class="player">
	      <div onclick="thevid=document.getElementById('thevideo'); thevid.style.display='block'; this.style.display='none'"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/youtube.jpg" onmouseover="this.src='<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/youtube-on.jpg'" onmouseout="this.src='<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/youtube.jpg'" style="cursor:pointer" /></div>
				<div id="thevideo" style="display:none">
					<object width="706" height="397"><param name="movie" value="//www.youtube.com/v/3UHOY_VwjOk?rel=0&vq=hd1080;version=3&amp;hl=en_US&amp;autoplay=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="//www.youtube.com/v/3UHOY_VwjOk?rel=0&vq=hd1080;version=3&amp;hl=en_US&amp;autoplay=1" type="application/x-shockwave-flash" width="706" height="397" allowscriptaccess="always" allowfullscreen="true"></embed>
					</object>
				</div>
			</div>
		</div>
		-->
			<div class="tags">
				<div class="titleListings">Listing Tags</div>
				<ul class="showTagsListings">
					<?php
						if (!empty($x->tags)){
							sort($x->tags);
							foreach ($x->tags as $tag) if ($tag->type == 0 ) echo '<li><span>'.$tag->tag.'</span></li>';
						}
					?>
				</ul>
				<div class="titleCity">City Tags</div>
				<ul class="showTagsCity">
					<?php
						if (!empty($x->tags)){
							sort($x->tags);
							foreach ($x->tags as $tag) if ($tag->type == 1 ) echo '<li><span>'.$tag->tag.'</span></li>';
						}
					?>
				</ul>
			</div>
	</div>
</div>
<?php endif; endif; ?>