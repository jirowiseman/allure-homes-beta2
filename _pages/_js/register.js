// function submitForm() {
// 	console.log("submitForm clicked.");
// 	var form = jQuery('#login-form');
// 	form.submit();
// }
var matchTitleBuyer = "Define Your Lifestyle.";
var subTitleBuyer = "Find your dream home";
var smallBuyer = "Take our quiz and tell us what you'd want in your ideal unique home.  We will find listings from around the country that fits your lifestyle.";

var matchTitleSeller = "Get Specific.";
var subTitleSeller = "Match buyers to your home";
var smallSeller = "Market the things that really matter about your listing by connecting with buyers over the unique features and lifestyle of your property.";


jQuery(document).ready(function($){

	submitForm = function() {
		console.log("submitForm clicked.");
		var form = $('#login-form');
		form.submit();
	}
	
	$('#user_login').prop('placeholder', 'Username')
	$('#user_pass').prop('placeholder', 'Password')

	reg = new function() {
		this.state = -1;

		this.init = function() {
			if (hideAgent) { // defined in header.js
				$('#professional').hide();
				$('#bre').hide();
			}
		}

		// this.showVid = function() {
		// 	$('.left-col #outerdiv').show();
		// 	$('.left-col #outerdiv .closeVid').show();
		// }
		// this.closeFrame = function() {
		// 	var src= $('iframe').attr('src');
  //       	$('iframe').attr('src',src); 
		// 	$('.left-col #outerdiv').css("display", "none");
		// }

		$('.userDiv .licenseData #states').change(function() {
			var val = $(".userDiv .licenseData #states option:selected").val();
			if (val == -1)
				ahtb.alert("Please pick the state where your license is registered.",
					{height: 150});
			else {
				reg.state = val;
				console.log("State is now set to "+val);
			}
		});
	}

	reg.init();

	// $('.left-col #outerdiv .closeVid').hide();
	// $('.left-col #outerdiv').hide();
	// $('.left-col #outerdiv .closeVid').on('click', function() {
	// 	reg.closeFrame();
	// });

	var login = '<a href="javascript:submitForm();"><span class="entypo-right"/></a>';
    $('#login-form .login-button').html(login);
    // $('#login-form').attr('action', ah_local.wp);

    $('#marker').hide();
    $('#verify').keyup(function(e) {
    	e.preventDefault();
    	var len = $(this).val().length;
    	len ? $('#marker').show() : $('#marker').hide();
    	//$('#marker').attr('visibility', len ? 'visible' : 'hidden');
    	var hasCancel = $('#marker').hasClass('entypo-attention');
    	if ($(this).val() == $('#password').val()) {
    		if (hasCancel) {
    			$('#marker').removeClass('entypo-attention');
    			$('#marker').addClass('entypo-check');
    			$('#marker').css('color', 'lightgreen');
    		}
    	}
    	else if (!hasCancel) {
    			$('#marker').removeClass('entypo-check');
    			$('#marker').addClass('entypo-attention');
     			$('#marker').css('color', 'red');
   		}
    	
    })

    $('#password').keyup(function(e) {
    	e.preventDefault();
    	var len = $(this).val().length;
    	var vLen = $('#verify').val().length;
    	len && vLen ? $('#marker').show() : $('#marker').hide();
    	//$('#marker').attr('visibility', len ? 'visible' : 'hidden');
    	var hasCancel = $('#marker').hasClass('entypo-attention');
    	if ($(this).val() == $('#verify').val()) {
    		if (hasCancel) {
    			$('#marker').removeClass('entypo-attention');
    			$('#marker').addClass('entypo-check');
    			$('#marker').css('color', 'lightgreen');
    		}
    	}
    	else {
    		if (!hasCancel) {
    			$('#marker').removeClass('entypo-check');
    			$('#marker').addClass('entypo-attention');
     			$('#marker').css('color', 'red');
   			}
    	}
    })

    $('#bre').hide();
    if (seller != null &&
    	seller != '' &&
    	hideAgent == false) {
    	$('#register input[name=first-name]').val(seller.first_name);
    	$('#register input[name=last-name]').val(seller.last_name);
    	$('#register input[name=email]').val(seller.email);
    	//$('#register input[name=login-id]').val(seller.email);
    	$('#states option[value="'+sellerState+'"]').prop('selected', true);
    	reg.state = sellerState;
    } else if (user != null) {
    	$('#register input[name=first-name]').val(user.first_name);
    	$('#register input[name=last-name]').val(user.last_name);
    	$('#register input[name=email]').val(user.email);
    	$('#register input[name=login-id]').val(user.user_login);
    }
    else if ( typeof ah_local.portalUserFirstName != 'undefined' &&
    		  ah_local.portalUserFirstName.length ) {
    	$('#register input[name=first-name]').val(typeof ah_local.portalUserFirstName != 'undefined' && ah_local.portalUserFirstName && ah_local.portalUserFirstName.length ? ah_local.portalUserFirstName : '');
    	$('#register input[name=last-name]').val(typeof ah_local.portalUserLastName != 'undefined' && ah_local.portalUserLastName && ah_local.portalUserLastName.length ? ah_local.portalUserLastName : '');
    	$('#register input[name=email]').val(typeof ah_local.portalUserEmail != 'undefined' && ah_local.portalUserEmail && ah_local.portalUserEmail.length ? ah_local.portalUserEmail : '');
    }

    if (source != null &&
    	hideAgent == false) {
    	$('#professional #realtor').prop('checked', true);
    	$('#bre').show();
	    $('.left-col .matchtitle').html(matchTitleSeller);
    	$('.left-col .sub-title').html(subTitleSeller);
    	$('.left-col .small').html(smallSeller);
   	}
    else {
    	$('.left-col .matchtitle').html(matchTitleBuyer);
    	$('.left-col .sub-title').html(subTitleBuyer);
    	$('.left-col .small').html(smallBuyer);
    }
    $('#professional #realtor').on('change', function() {
    	var val = $(this).prop('checked');
    	if (val) {
    		$('#profData').show();
    		$('#bre').show();
	     	$('.left-col .matchtitle').html(matchTitleSeller);
	    	$('.left-col .sub-title').html(subTitleSeller);
	    	$('.left-col .small').html(smallSeller);
    	}
    	else {
    		$('#profData').hide();
    		$('#bre').hide();
	     	$('.left-col .matchtitle').html(matchTitleBuyer);
	    	$('.left-col .sub-title').html(subTitleBuyer);
	    	$('.left-col .small').html(smallBuyer);
	    }
    })

    function registerUser(data) {
    	$.ajax({
				type: "POST",
				dataType: "json",
				url:  ah_local.tp+"/_pages/ajax-register.php",
				data: { 
					query: 'basic-register',
					data: data
				},
				error: function($xhr, $status, $error){
					$('div#overlay-bg').hide();
					$('#result').html('Registering...').fadeOut();
					$('div#page-register').animate({opacity: 1},{duration: 500});
					$("#submitbtn").prop('disabled', false);
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					ahtb.open({ title: 'You Have Encountered an Error during Registration', 
								height: 150, 
								html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
							});
				},		
				success: function(d){
					$('div#overlay-bg').hide();
					$('#result').html('Registering...').fadeOut();
					//if (d.status == 'OK') window.location = ah_local.wp+'/sellers';
					console.log(typeof d.data == 'string' ? d.data : (typeof d.data.msg != 'undefined' ? d.data.msg : "no message to show"));
					if (d.status == 'OK') {
						var msg = "Welcome to our site!  Please enjoy finding the listing that fits your lifestyle!";
						var extra = 0;
						if (bre != '') 
							if (data.inviteCode == '' ||
								seller == 'seller') {
								msg = "Welcome aboard!  Please browse our site.  Your agent page will be available once you've been verified.";
								extra = 90;
							}
							else {
								msg = "Welcome to our site!  Please use the seller's page and update your photo and description, then add your listings.";
								extra = 40;
							}
						console.log(msg);
						ah_local.author_id = parseInt(d.data.id);
						ah_local.seller_id = parseInt(d.data.seller_id);
						d.data.userMode = typeof d.data.userMode != 'undefined' ? parseInt(d.data.userMode) : 0;
						if (isAdmin ||
							(seller != null &&
							 seller != '' &&
							 hideAgent == false) ||
							 data.inviteCode != '' ||
							 (d.data.userMode == AgentType.AGENT_LISTHUB ||
							  d.data.userMode == AgentType.AGENT_INVITE)) // then invited or listhub agent
							window.location = ah_local.wp+'/sellers';
						else if (d.data.userMode == 0) // NON-AGENT
							window.location = ah_local.wp; 
						else if (d.data.userMode == AgentType.AGENT_ORGANIC) {
							$('div#page-register').animate({opacity: 1},{duration: 500});
							ahtb.open({
							    //title: 'Define Your Lifestyle',
							    height: 185 + extra,
							    width: 600,
							    html: (typeof d.data.msg != 'undefined' ? d.data.msg : msg),
									hideTitle: true,
							    	buttons:[
										{text:"OK", action:function(){ 
											window.location = ah_local.wp; 
										}},
									],
							    closed: function(){
							      ahtb.showClose(250);
							      ahtb.closeOnClickBG(1);
							    }
							});
						}
					}
					else {
						ahtb.alert(d.data, {height: 210});
						$('div#page-register').animate({opacity: 1},{duration: 500});
						$("#submitbtn").prop('disabled', false);
					}
				}
			})
	}

    function completeRegistration() {
		var bre = '';
		if ( $('#professional #realtor').prop('checked') &&
			 (bre = $('#register input[name=realtor-id]').val()) != '' ) {
			if (bre.length < 5) {
				ahtb.alert("Please enter a valid Realtor ID.",
						{height: 150});
				return;
			}
			if (reg.state == -1) {
				ahtb.alert("Please select the state your Realtor ID is registered.",
							{height: 150});
				return;
			}
		}
		var first = '';
		if ( (first = $('#register input[name=first-name]').val()) == '') {
			ahtb.alert("Please enter your first name.",
						{height: 150});
			return;
		}
		var last = '';
		if ( (last = $('#register input[name=last-name]').val()) == '') {
			ahtb.alert("Please enter your last name.",
						{height: 150});
			return;
		}

		var email = '';
		if ( (email = $('#register input[name=email]').val()) == '') {
			ahtb.alert("Please enter your email adress.",
						{height: 150});
			return;
		}

		var login = '';
		if ( (login = $('#register input[name=login-id]').val()) == '') 
			login = email;

		if (!validateEmail(email)) {
			ahtb.alert("Please check your email adress, it doesn't appear to be valid.",
						{height: 150});
			return;
		}
		var passwd = '';
		if ( (passwd = $('#register input[name=password]').val()) == '' ||
			 passwd.length < 8) {
			ahtb.alert("Please enter a password of at least 8 characters.",
						{height: 150});
			return;
		}
		if ( passwd != $('#register #verify').val() ) {
			ahtb.alert("Passwords do not match.",
						{height: 150});
			return;
		}

		var isAgent = false;
		if ($('#professional #realtor').prop('checked')) {
			if (bre.length < 5) {
				ahtb.alert("Please enter a valid Realtor ID.",
						{height: 150});
				return;
			}
			if (reg.state == -1) {
				ahtb.alert("Please select the state your Realtor ID is registered.",
							{height: 150});
				return;
			}
			isAgent = true;
		}

		var invitationCode = $('#register #invitation_code').val();
		var optIn = $('input#opt-in ').prop('checked');

		$('#result').html('Registering...').fadeIn();

		prepOverlaySpinner( 
			registerUser,{
							first_name: first, 
							last_name: last,
							email: email, 
							login: login,
							password: passwd,
							realtor_id: bre,
							state: reg.state,
							inviteCode: invitationCode,
							special: isAdmin ? RegistrationMode.REGISTRATION_SPECIAL_ADMIN : RegistrationMode.REGISTRATION_SPECIAL_NONE,
							isAdmin: isAdmin ? RegistrationMode.REGISTRATION_SPECIAL_ADMIN : RegistrationMode.REGISTRATION_SPECIAL_NONE,
							optIn: optIn
						}
		);
	}

	function goGetIt() {
		var bre = '';
		if ( $('#professional #realtor').prop('checked') &&
			 (bre = $('#register input[name=realtor-id]').val()) != '' ) {
			if (bre.length < 5) {
				ahtb.alert("Please enter a valid Realtor ID.",
						{height: 150});
				return;
			}
			if (reg.state == -1) {
				ahtb.alert("Please select the state your Realtor ID is registered.",
							{height: 150});
				return;
			}
		}
		var first = '';
		if ( (first = $('#register input[name=first-name]').val()) == '') {
			ahtb.alert("Please enter your first name.",
						{height: 150});
			return;
		}
		var last = '';
		if ( (last = $('#register input[name=last-name]').val()) == '') {
			ahtb.alert("Please enter your last name.",
						{height: 150});
			return;
		}

		var email = '';
		if ( (email = $('#register input[name=email]').val()) == '') {
			ahtb.alert("Please enter your email adress.",
						{height: 150});
			return;
		}

		var login = '';
		if ( (login = $('#register input[name=login-id]').val()) == '') 
			login = email;

		if (!validateEmail(email)) {
			ahtb.alert("Please check your email adress, it doesn't appear to be valid.",
						{height: 150});
			return;
		}
		var passwd = '';
		if ( (passwd = $('#register input[name=password]').val()) == '' ||
			 passwd.length < 8) {
			ahtb.alert("Please enter a password of at least 8 characters.",
						{height: 150});
			return;
		}
		if ( passwd != $('#register #verify').val() ) {
			ahtb.alert("Passwords do not match.",
						{height: 150});
			return;
		}

		var isAgent = false;
		if ($('#professional #realtor').prop('checked')) {
			if (bre.length < 5) {
				ahtb.alert("Please enter a RE license number",
						{height: 150});
				return;
			}
			if (reg.state == -1) {
				ahtb.alert("Please select the state your Realtor ID is registered.",
							{height: 150});
				return;
			}
			isAgent = true;
		}

		var invitationCode = $('#register #invitation_code').val();
		var optIn = $('input#opt-in ').prop('checked');

		$('#result').html('Registering...').fadeIn();

		if (!isAgent) {
			$('div#page-register').animate({opacity: 0.5},{duration: 1000});
			$("#submitbtn").prop('disabled', true);
			completeRegistration();
			return;
		}

		var parent = $('div#page-register');
		var button = $("#submitbtn");
		var emailEle = $('#register input[name=email]');
		var firstNameEle = $('#register input[name=first-name]');
		var lastNameEle = $('#register input[name=last-name]');
		var phoneEle = null;
		var phone = '';
		checkSellerInDB(first, last, email, reg.state, phone, login,
						completeRegistration, // successful callback
						goGetIt, // retry callback
						parent, // div to make opaque
						button, // button to enable disablity (this one)
						emailEle, // email element
						firstNameEle, // first_name element
						lastNameEle, // last_name element
						phoneEle); // phone element
	}

	$("#submitbtn").click(function(e){
		e.preventDefault();
		goGetIt();
	});
	// $('#register input[type=text]').one('click', function(){
	// 	$(this).val('');
	// });

	$('#bre #states').change(function() {
			var val = $("#bre #states option:selected").val();
			if (val == -1 &&
				$('#register [name=realtor-id]').val() != '')
				ahtb.alert("Please pick the state where your license is registered.",
					{height: 150});
			else {
				reg.state = val;
				console.log("State is now set to "+reg.state);
			}
		});
})


