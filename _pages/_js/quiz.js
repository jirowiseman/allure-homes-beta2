var quiz;

var isOnIOS = navigator.userAgent.match(/iPad/i)|| navigator.userAgent.match(/iPhone/i);
var eventName = isOnIOS ? "pagehide" : "beforeunload";
var hasNoShowStartHint = getCookie("NoShowStartHint").length ? getCookie("NoShowStartHint") : false;
var isMobile = '';
var waitedForFirstDirective = false;
var showPopup = false;
var predefinedLength = $('.predefined.desktop .slides-wrapper .slides>ul li').length;
var slideLengthVersion = 4;
var listWidth = predefinedLength - 1;
var slideListWidth = 273 + (271 * listWidth);
var maxSlideList = slideListWidth - (271 * slideLengthVersion);
var maxSlideListpx = maxSlideList+'px';

window.addEventListener(eventName, function (e) { 
	if (SystemMaintenance)
		return;
	
	var e = e || window.event;
	var msg = "You will lose your progress."
	if (quiz.state == "DoneQuiz" ||
		quiz.state == null)
		return;

	// clear profile cache
	setCookie('ProfileData', '', 1);	

	// For IE and Firefox
	if (e) {
	    e.returnValue = msg;
	}

	// For Safari / chrome
	return msg;
} );

window.onpopstate = function(e) {
  // alert("location: " + document.location + ", state: " + JSON.stringify(e.state));
  quiz.previousQuestion(true);
}

// window.onbeforeunload = function (e) {
// 	var e = e || window.event;
// 	var msg = "You will lose your progress."
// 	if (quiz.state == "DoneQuiz" ||
// 		quiz.state == null)
// 		return;

// 	// clear profile cache
// 	setCookie('ProfileData', '', 1);	

// 	// For IE and Firefox
// 	if (e) {
// 	    e.returnValue = msg;
// 	}

// 	// For Safari / chrome
// 	return msg;
// };

var theRange = [ noAskAllowMinPrice, lastPrice ];
var priceOptionmin = {
	800000: "Min Price",
	800001: "$800k",
	1000000: "$1 Million",
	1200000: "$1.2 Million",
	1400000: "$1.4 Million",
	1600000: "$1.6 Million",
	1800000: "$1.8 Million",
	2000000: "$2 Million",
	2500000: "$2.5 Million",
	3000000: "$3 Million",
	3500000: "$3.5 Million",
	4000000: "$4 Million",
	4500000: "$4.5 Million",
	5000000: "$5 Million",
	6000000: "$6 Million",
	7000000: "$7 Million",
	8000000: "$8 Million",
	9000000: "$9 Million",
	10000000: "$10 Million",
	12000000: "$12 Million",
	14000000: "$14 Million",
	16000000: "$16 Million",
};	
var priceOptionmax = {
	1000000: "$1 Million",
	1500000: "$1.5 Million",
	2000000: "$2 Million",
	2500000: "$2.5 Million",
	3000000: "$3 Million",
	3500000: "$3.5 Million",
	4000000: "$4 Million",
	4500000: "$4.5 Million",
	5000000: "$5 Million",
	6000000: "$6 Million",
	7000000: "$7 Million",
	8000000: "$8 Million",
	9000000: "$9 Million",
	10000000: "$10 Million",
	12000000: "$12 Million",
	14000000: "$14 Million",
	16000000: "$16 Million",
	20000000: "Max Price",
};	

var QuizState = {
	HOME_FEATURES: 11,
	MORE_HOME_FEATURES: 31,
	LIFE_PACE: 16,
	FAV_ACTIVITES_NOT_USED: 10,
	//FAV_ACTIVITES: ah_local.wp.indexOf('local') != -1 ? 33 : (ah_local.wp.indexOf('alpha') != -1 ? 33 : 34),
	FAV_ACTIVITES: ah_local.wp.indexOf('local') != -1 ? 33 : 34,
	HOME_TYPE: 13,
	DIRECT_ACCESS: 4,
	ARCHITECTURE: 8,
	GARAGE_SIZE: 15,
	FAMOUSE_HOMES: 22,
	VIEW_TYPE: 24,
	PRIVACY: 9,
	IDEAL_WINTER: 3,
	IDEAL_SUMMER: 18,
	AIRPORT_PROXIMITY: 25
}

	function dimPredfinedQuizDialog(url) {
		$('div.predefined').css('opacity', '0.8');	
		la( AnalyticsType.ANALYTICS_TYPE_CLICK,
          		'PredefinedQuiz',
          		ah_local.agentID,
          		url,
          		function() {
            		window.location = url;
				},
				function() {
					window.location = url;
				}
		);
		
		return true;
	}

	function setPredefinedQuizArrows() {
		isThreeSlide = $('section.isthreeslide-identifier').css('display') != 'none';
		isTwoSlide = $('section.istwoslide-identifier').css('display') != 'none';

		if (!isThreeSlide &&
			 !isTwoSlide) {
			slideLengthVersion = 4;
		}
		if (isThreeSlide) {
			slideLengthVersion = 3;
		}
		if (isTwoSlide) {
			slideLengthVersion = 2;
		}
		listWidth = predefinedLength - 1;
		slideListWidth = 273 + (271 * listWidth);
		maxSlideList = slideListWidth - (271 * slideLengthVersion);
		maxSlideListpx = maxSlideList+'px';
	}

	function fixHex(str) {
		var newStr = str.replace(/%([a-fA-F0-9]{2})/g, function(match, numStr) {
	        var num = parseInt(numStr, 16); // read num as normal number
	        var chr = String.fromCharCode(num);
	        return chr;
	    });
	    return newStr;
	}


jQuery(document).ready(function($){
	isRetina = $('section.isretina-identifier').css('display') != 'none';
	isMobile = $('section.mobile-identifier').css('display') != 'none';
	isAllMobile = $('section.mobile-identifier-all').css('display') != 'none';
	isMobileLandscape = $('section.mobile-identifier-landscape').css('display') != 'none';
	// isThreeSlide = $('section.isthreeslide-identifier').css('display') != 'none';
	// isTwoSlide = $('section.istwoslide-identifier').css('display') != 'none';
	$('.home-options').hide();

	setPredefinedQuizArrows();
	// hash = fixHex( window.location.hash.length ? window.location.hash : '#'+getCookie("IN_HASH") );
	hash = window.location.hash.length ? window.location.hash : '';

	// $(window).on('orientationchange', function(e) {
	$(window).on('resize', function(e) {
		var previous = isMobile;
		isMobile = $('section.ismobile-identifier').css('display') != 'none';
		// isThreeSlide = $('section.isthreeslide-identifier').css('display') != 'none';
		// isTwoSlide = $('section.istwoslide-identifier').css('display') != 'none';
		// ahtb.alert("orientation change, isMobile:"+(isMobile ? 'yes' : 'no')+", isRetina:"+(isRetina ? 'yes' : 'no'));
		if (previous != isMobile ||
			isMobile) {
			if (quiz.doingPricePage ||
				quiz.currentQuestion == -1) {
				if (typeof quizOptions != 'undefined' &&
					typeof quizOptions.doPredefinedQuiz != 'undefined' &&
					quizOptions.doPredefinedQuiz) {
					var whichPopup = '';
					if (!isMobile) {
						$('div#overlay-bg div.predefined.mobile').hide();
						$('div#overlay-bg div.predefined.desktop').show();
						whichPopup = 'div#overlay-bg div.predefined.desktop';
						$('div.predefined .intro #subtitle').show();
					}
					else {
						$('div#overlay-bg div.predefined.desktop').hide();
						$('div#overlay-bg div.predefined.mobile').show();
						whichPopup = 'div#overlay-bg div.predefined.mobile';
						$('div.predefined .intro #subtitle').hide();
					}

					var predefinedQuizDivHeight = $(whichPopup).innerHeight();
					var predefinedQuizDivHeightHalf = (predefinedQuizDivHeight) / 2;
					$(whichPopup).css('margin-top', '-'+predefinedQuizDivHeightHalf+'px');
					
					$('.predefined .slides .image .text').each(function(){
						var predefinedQuizTextHeight = $(this).innerHeight();
						var predefinedQuizTextHeightHalf = (predefinedQuizTextHeight) / 2;
						$(this).css('margin-top', '-'+predefinedQuizTextHeightHalf+'px');
					});
					
					/*var predefinedQuizDivWidth = $(whichPopup).innerWidth();
					var buttonWidth = $(whichPopup+" button#custom-quiz").innerWidth();
					var marginLeft = (predefinedQuizDivWidth - buttonWidth) / 2;
					$(whichPopup+" button#custom-quiz").css('margin-left', marginLeft+'px');*/
					
					$('body').css('overflow', 'hidden');
					if (isMobile)
						$('body').css('position','fixed');
				}
				return;
			}

			var selected = [];
			$('.selectimages .slide a.selected').each(function(){selected.push(parseInt( $(this).parent().attr('data-slide') ))});
			quiz.loadQuestion( quiz.currentQuestion, false, selected );
		}
		else if (!isMobile) {
			var whichPopup = 'div#overlay-bg div.predefined.desktop';
			var predefinedQuizDivHeight = $(whichPopup).innerHeight();
	        var predefinedQuizDivHeightHalf = (predefinedQuizDivHeight) / 2;
	        $(whichPopup).css('margin-top', '-'+predefinedQuizDivHeightHalf+'px');
			
					$('.predefined .slides .image .text').each(function(){
						var predefinedQuizTextHeight = $(this).innerHeight();
						var predefinedQuizTextHeightHalf = (predefinedQuizTextHeight) / 2;
						$(this).css('margin-top', '-'+predefinedQuizTextHeightHalf+'px');
					});

	        // var predefinedQuizDivWidth = $(whichPopup).innerWidth();
	        // var buttonWidth = $(whichPopup+" button#custom-quiz").innerWidth();
	        // var marginLeft = (predefinedQuizDivWidth - buttonWidth) / 2;
	        //  $(whichPopup+" button#custom-quiz").css('margin-left', marginLeft+'px');
		}
		
		// if (!isThreeSlide &&
		// 	 !isTwoSlide) {
		// 	slideLengthVersion = 4;
		// }
		// if (isThreeSlide) {
		// 	slideLengthVersion = 3;
		// }
		// if (isTwoSlide) {
		// 	slideLengthVersion = 2;
		// }
		// listWidth = predefinedLength - 1;
		// slideListWidth = 273 + (271 * listWidth);
		// maxSlideList = slideListWidth - (271 * slideLengthVersion);
		// maxSlideListpx = maxSlideList+'px';

		setPredefinedQuizArrows();
		if (predefinedLength > slideLengthVersion) {
			var slideEle = $('.predefined.desktop .slides-wrapper .slides>ul');
			slideEle.css('right','0px');
			$('.predefined.desktop .slides-wrapper .arrow#left').addClass( 'disabled' );
			$('.predefined.desktop .slides-wrapper .arrow#right').removeClass( 'disabled' );
		}
	})

	if (typeof quizOptions != 'undefined' &&
		typeof quizOptions.doPredefinedQuiz != 'undefined' &&
		quizOptions.doPredefinedQuiz) {
		var whichPopup = '';
		var noReferer = Referer.length == 0 ||
					    Referer == 'not set';
		var usedAsLandingPage = (arg.length &&
					 			 arg.indexOf('F-') != -1);
		var fromPortalLandingPage = Referer.indexOf('portal-landing') != -1;
		var forcePopupPredefinedQuiz = 	typeof quizOptions.forcePopupPredefinedQuiz != 'undefined' &&
					 					quizOptions.forcePopupPredefinedQuiz;
		var forcePopupPredefinedQuizWhenUsedAsLandingPage = 
										typeof quizOptions.forcePopupPredefinedQuizWhenUsedAsLandingPage != 'undefined' &&
										quizOptions.forcePopupPredefinedQuizWhenUsedAsLandingPage &&
										usedAsLandingPage;
		var disablePopupPredefinedQuizWhenReferrerIsPortalLanding =
										typeof quizOptions.disablePopupPredefinedQuizWhenReferrerIsPortalLanding != 'undefined' &&
										quizOptions.disablePopupPredefinedQuizWhenReferrerIsPortalLanding;
		showPopup = ((noReferer ||
					  (fromPortalLandingPage && !disablePopupPredefinedQuizWhenReferrerIsPortalLanding)) &&
					  !usedAsLandingPage) ||
					(forcePopupPredefinedQuiz && !usedAsLandingPage && !fromPortalLandingPage )||
					forcePopupPredefinedQuizWhenUsedAsLandingPage;

		if (showPopup)
			$('div#overlay-bg').show();
		else
			window.history.pushState('pure','Title',ah_local.wp+'/quiz');

		if (!isMobile) {
			$('div.predefined.desktop').show();
			whichPopup = 'div#overlay-bg div.predefined.desktop';
		}
		else {
			$('div#overlay-bg div.predefined.mobile').show();
			whichPopup = 'div#overlay-bg div.predefined.mobile';
			$('div.predefined .intro #subtitle').hide();
		}

		if (showPopup) {
			var predefinedQuizDivHeight = $(whichPopup).innerHeight();
			var predefinedQuizDivHeightHalf = (predefinedQuizDivHeight) / 2;
			$(whichPopup).css('margin-top', '-'+predefinedQuizDivHeightHalf+'px');
			
			$('.predefined .slides .image .text').each(function(){
				var predefinedQuizTextHeight = $(this).innerHeight();
				var predefinedQuizTextHeightHalf = (predefinedQuizTextHeight) / 2;
				$(this).css('margin-top', '-'+predefinedQuizTextHeightHalf+'px');
			});
			
			if (predefinedLength > slideLengthVersion) {
				$('.predefined.desktop .slides-wrapper .slides').addClass( 'scrollable' );
				$('.predefined.desktop .slides-wrapper .arrow').addClass( 'active' );

				$('.predefined.desktop .slides-wrapper .slides>ul').css( 'width',slideListWidth );

				var slideEle = $('.predefined.desktop .slides-wrapper .slides>ul');

				function checkSlidePositionleft() {
					if (slideEle.css('right') == '0px')
						$('.predefined.desktop .slides-wrapper .arrow#left').addClass( 'disabled' );
					else
						$('.predefined.desktop .slides-wrapper .arrow#left').removeClass( 'disabled' );
				}
				function checkSlidePositionright() {
					if (slideEle.css('right') == maxSlideListpx)
						$('.predefined.desktop .slides-wrapper .arrow#right').addClass( 'disabled' );
					else
						$('.predefined.desktop .slides-wrapper .arrow#right').removeClass( 'disabled' );
				}
				function checkSlidePosition() {
					checkSlidePositionleft();
					checkSlidePositionright();
				}
				checkSlidePosition();

				$('.predefined.desktop .slides-wrapper .arrow#left span.click').on('click', function() {
					$('.predefined.desktop .slides-wrapper .arrow span.click').css('pointer-events','none');
						if (slideEle.css('right') == '273px')
							$('.predefined.desktop .slides-wrapper .slides>ul').animate({right:'-=273px'}, 'normal', function() {
								checkSlidePosition();
								$('.predefined.desktop .slides-wrapper .arrow span.click').css('pointer-events','auto');
							});
						else
							$('.predefined.desktop .slides-wrapper .slides>ul').animate({right:'-=271px'}, 'normal', function() {
								checkSlidePosition();
								$('.predefined.desktop .slides-wrapper .arrow span.click').css('pointer-events','auto');
							});
					});
				$('.predefined.desktop .slides-wrapper .arrow#right span.click').on('click', function() {
					$('.predefined.desktop .slides-wrapper .arrow span.click').css('pointer-events','none');
					if (slideEle.css('right') == '0px')
						$('.predefined.desktop .slides-wrapper .slides>ul').animate({right:'+=273px'}, 'normal', function() {
							checkSlidePosition();
							$('.predefined.desktop .slides-wrapper .arrow span.click').css('pointer-events','auto');
						});
					else if (slideEle.css('right') != '0px')
						$('.predefined.desktop .slides-wrapper .slides>ul').animate({right:'+=271px'}, 'normal', function() {
							checkSlidePosition();
							$('.predefined.desktop .slides-wrapper .arrow span.click').css('pointer-events','auto');
						});
				});
			}
		}

        // var predefinedQuizDivWidth = $(whichPopup).innerWidth();
        // var buttonWidth = $(whichPopup+" button#custom-quiz").innerWidth();
        // var marginLeft = (predefinedQuizDivWidth - buttonWidth) / 2;
        //  $(whichPopup+" button#custom-quiz").css('margin-left', marginLeft+'px');

        if (showPopup) {
	        $('body').css('overflow', 'hidden');
	        if (isMobile)
	          	$('body').css('position','fixed');
	    }

        $('.predefined .slides .image .text').addClass('animate');

		$('div#overlay-bg button#custom-quiz').on('click', function() {
			$('div#overlay-bg').hide();
		})
	}


	quiz = new function() {
		this.state = null;
		this.selected = 0;
		this.currentQuestion = -1;
		this.doingPricePage = false;
		this.doingChooseQuiz = false;
		this.hasActivity = false;
		this.dbBusy = 0;
		this.tasks = [];
		this.stack = [];
		this.price = [ noAskAllowMinPrice, -1 ];
		this.stackIt = true;
		this.leavingPage = false;
		this.doDistance = true;
		this.distance = 8;
		this.quizID = QuizType.QUIZ_NATION;

		this.fixMinPriceRange = function() {
			if (typeof priceList != 'undefined' &&
				priceList != null)
				priceOptionmin = priceList;
		}
		
		this.fixMaxPriceRange = function() {
			if (typeof priceListMax != 'undefined' &&
				priceListMax != null)
				priceOptionmax = priceListMax;
		}
		
		this.setMin = function(val) {
				self = quiz;
				val = parseInt(val);
				console.log("MIN selected: "+val);
				var newTop = self.price[1];
				if (val >= self.price[1]) {
					if (self.price[1] == theRange[1] ||
						self.price[1] == -1) {
						val = 20000000;
						newTop = -1;
					}
					else {
						var diff = val < 800000 ? 50000 : (val < 2000000 ? 200000 : (val < 5000000 ? 500000 : (val < 10000000 ? 1000000 : 2000000)));
						newTop = val+diff;
					}
				}
				else
					newTop = self.price[1] >= theRange[1] ? -1 : self.price[1];

				self.price[0] = val;
				self.price[1] = newTop;
				self.setPriceSelections();
				la( AnalyticsType.ANALYTICS_TYPE_CLICK,
          			'SetMin', 
          			val);
				ga('send', {
				  'hitType': 'event',          // Required.
				  'eventCategory': 'button',   // Required.
				  'eventAction': 'click',      // Required.
				  'eventLabel': 'SetMin'
				});
				// self.get.results({ price: [val, newTop]  }, false);
		}
			
		this.setMax = function(val) {
			  self = quiz;
				val = parseInt(val);
				console.log("MAX selected: "+val);
				var newTop = val == theRange[1] ? -1 : (val == (lastPrice-1) ? lastPrice : val);
				if (val <= self.price[0])
					if (self.price[0] == theRange[0]) {
						newTop = theRange[0] + 100000;
					}
					else {
						var diff = val < 800000 ? 50000 : (val < 2000000 ? 200000 : (val < 5000000 ? 500000 : (val < 10000000 ? 1000000 : 2000000)));
						self.price[0] = newTop-diff;
					}

				self.price[1] = newTop;
				self.setPriceSelections();
				la( AnalyticsType.ANALYTICS_TYPE_CLICK,
          			'SetMax', 
          			newTop);
				ga('send', {
				  'hitType': 'event',          // Required.
				  'eventCategory': 'button',   // Required.
				  'eventAction': 'click',      // Required.
				  'eventLabel': 'SetMax'
				});
				// self.get.results({ price: [self.query.price[0], newTop]  }, false);
		}

		this.setPriceSelections = function() {
			self = quiz;
			$('.selectBoxmin ul#min li[value="'+self.price[0]+'"]').prop('selected', true);
			$('.selectBoxmax ul#max li[value="'+(self.price[1] == -1 ? 20000000 : self.price[1])+'"]').prop('selected', true);
			$('.selectBoxmin .selected').html(priceOptionmin[self.price[0]]);
			$('.selectBoxmax .selected').html(priceOptionmax[self.price[1] == -1 ? 20000000 : self.price[1]]);
		}

		this.getReady = function() {
			if (!waitedForFirstDirective) {
				// ahtb.alert("Initalizing...", {width: 400, height: 150});
			    window.setTimeout(function() {
			      quiz.waitForDirective();
			    }, 500);
			    waitedForFirstDirective = true;
			 }
		}

		this.waitForDirective = function() {
		  	$('button').prop('disabled', gettingDirective);
		  	if (gettingDirective) {
			    window.setTimeout(function() {
			      quiz.waitForDirective();
			      // console.log("wating for directive");
			    }, 1000);
			    return;
			}
			else {
				console.log("Got directive, sessionID:"+ah_local.sessionID);
				quiz.init();
			}
		}

		this.init = function(){
			if (SystemMaintenance)
				return;

			this.fixMinPriceRange();
			this.fixMaxPriceRange();

			setupAutocomplete($('.specific .autocomplete input'));
			setupAddressAutocomplete($('.address .autocomplete input'));

			var options = '';
			for(var index in priceOptionmin) {
				options += "<li value='"+index+"'>"+priceOptionmin[index]+'</li>';
			}
			var options2 = '';
			for(var index in priceOptionmax) {
				options2 += "<li value='"+index+"'>"+priceOptionmax[index]+'</li>';
			}
			var options3 = '';
			for(var index in BedsOptions) {
				options3 += "<li value='"+BedsOptions[index].value+"'>"+BedsOptions[index].text+'</li>';
				console.log("bed index:"+index+", value:"+BedsOptions[index].value);
			}
			console.log("beds - "+options3);
			
			var options4 = '';
			for(var index in BathsOptions) {
				options4 += "<li value='"+BathsOptions[index].value+"'>"+BathsOptions[index].text+'</li>';
				console.log("bath index:"+index+", value:"+BathsOptions[index].value);
			}

			// $('input[type=radio][value=0]').prop('checked', true);

			$('.selectBed').attr('value', 0); // default value - ALL
			$('.selectBed ul').html(options3);
			$('.selectBed ul').find('li').click(function(){
				$('.selectBed ul li[value]').removeClass('active');
				var val = $(this).attr('value');
				$(this).parent().css('display','none');
				$(this).addClass('active');
				$(this).closest('.selectBed').attr('value',val);
				var beds = '&nbsp;'+(val != 1 ? 'Bedrooms' : 'Bedroom');
				$(this).parent().siblings('.selected').html($(this).html()+beds);
				console.log("Bed value:"+val);
			});
			$('.selectBed').each(function(){
				$(this).children('.selected,span.selectArrow').click(function(event){
					event.stopPropagation();
					if($(this).parent().children('ul').css('display') == 'none'){
						$(this).parent().children('ul').css('display','block');
						// $('ul#max').css('display','none');
						$('.selectBed').addClass('active');
						$('.selectBath ul').hide();
						$('.selectBath').removeClass('active');
						$('.selectBoxmin').removeClass('active');
						$('.selectBoxmax').removeClass('active');
						$('.selectBoxmin ul').hide();
						$('.selectBoxmax ul').hide();
					}
					else {
						$(this).parent().children('ul').css('display','none');
						$('.selectBed').removeClass('active');
					}
				});
    		});

			$('.selectBath').attr('value', 0); // default value - ALL
			$('.selectBath ul').html(options4);
			$('.selectBath ul').find('li').click(function(){
				$('.selectBath ul li[value]').removeClass('active');
				var val = $(this).attr('value');
				$(this).parent().css('display','none');
				$(this).addClass('active');
				$(this).closest('.selectBath').attr('value',val);
				var baths = '&nbsp;'+(val != 1 ? 'baths' : 'bath');
				$(this).parent().siblings('.selected').html($(this).html()+baths);
				console.log("Bath value:"+val);
			});
			$('.selectBath').each(function(){
				$(this).children('.selected,span.selectArrow').click(function(event){
					event.stopPropagation();
					if($(this).parent().children('ul').css('display') == 'none'){
						$(this).parent().children('ul').css('display','block');
						// $('ul#max').css('display','none');
						$('.selectBath').addClass('active');
						$('.selectBed ul').hide();
						$('.selectBed').removeClass('active');
						$('.selectBoxmin').removeClass('active');
						$('.selectBoxmax').removeClass('active');
						$('.selectBoxmin ul').hide();
						$('.selectBoxmax ul').hide();
					}
					else {
						$(this).parent().children('ul').css('display','none');
						$('.selectBath').removeClass('active');
					}
				});
    		});
			
			$('.selectBoxmin ul#min').html(options);
			$('.selectBoxmin ul#min').find('li').click(function(){
				$('.selectBoxmin ul#min li[value]').removeClass('active');
				var val = $(this).attr('value');
				$(this).parent().css('display','none');
				$(this).addClass('active');
				$(this).closest('.selectBoxmin').attr('value',val);
				$(this).parent().siblings('.selected').html($(this).html());
				quiz.setMin(val);
			});
			$('.selectBoxmax ul#max').html(options2);
			$('.selectBoxmax ul#max').find('li').click(function(){
				$('.selectBoxmax ul#max li[value]').removeClass('active');
				var val = $(this).attr('value');
				$(this).parent().css('display','none');
				$(this).addClass('active');
				$(this).closest('.selectBoxmax').attr('value',val);
				$(this).parent().siblings('.selected').html($(this).html());
				quiz.setMax(val);
			});

			$('.selectBoxmin').each(function(){
				$(this).children('.selected,span.selectArrow').click(function(event){
					event.stopPropagation();
					if($(this).parent().children('ul#min').css('display') == 'none'){
						$(this).parent().children('ul#min').css('display','block');
						$('ul#max').css('display','none');
						$('.selectBoxmin').addClass('active');
						$('.selectBoxmax').removeClass('active');
						$('.selectBed').removeClass('active');
						$('.selectBath').removeClass('active');
						$('.selectBoxmax ul').hide();
						$('.selectBed ul').hide();
						$('.selectBath ul').hide();
					}
					else {
						$(this).parent().children('ul#min').css('display','none');
						$('.selectBoxmin').removeClass('active');
					}
				});
    		});
			$('.selectBoxmax').each(function(){
				$(this).children('.selected,span.selectArrow').click(function(event){
					event.stopPropagation();
					if($(this).parent().children('ul#max').css('display') == 'none'){
						$(this).parent().children('ul#max').css('display','block');
						$('ul#min').css('display','none');
						$('.selectBoxmax').addClass('active');
						$('.selectBoxmin').removeClass('active');
						$('.selectBed').removeClass('active');
						$('.selectBath').removeClass('active');
						$('.selectBoxmin ul').hide();
						$('.selectBed ul').hide();
						$('.selectBath ul').hide();
					}
					else {
						$(this).parent().children('ul#max').css('display','none');
						$('.selectBoxmax').removeClass('active');
					}
				});
    		});

			$('input[name=commercial-ok]').on('change', function() {
				// keep desktop and mobile checkboxes in synch
				var checked = $(this).is(':checked');
				// var otherParent = "div#median-range"+(isMobile ? '' : '.mobile');
				// var otherCheckbox = $(otherParent+' input[name=commercial-ok]');
				// otherCheckbox.prop('checked', checked);
				$('input[name=rental-only]').prop('checked', false);
				console.log("commercial-ok is now "+(checked ? 'checked' : 'unchecked'));
			});

			$('input[name=rental-ok]').on('change', function() {
				// keep desktop and mobile checkboxes in synch
				var checked = $(this).is(':checked');
				// var otherParent = "div#median-range"+(isMobile ? '' : '.mobile');
				// var otherCheckbox = $(otherParent+' input[name=rental-ok]');
				// otherCheckbox.prop('checked', checked);
				$('input[name=rental-only]').prop('checked', false);
				console.log("rental-ok is now "+(checked ? 'checked' : 'unchecked'));
			});

			$('input[name=rental-only]').on('change', function() {
				// keep desktop and mobile checkboxes in synch
				var checked = $(this).is(':checked');
				// var otherParent = "div#median-range"+(isMobile ? '' : '.mobile');
				// var otherCheckbox = $(otherParent+' input[name=rental-only]');
				// otherCheckbox.prop('checked', checked);
				$('input[name=commercial-ok]').prop('checked', false);
				$('input[name=rental-ok]').prop('checked', false);
				console.log("rental-only is now "+(checked ? 'checked' : 'unchecked'));
			});

			$(document).click( function(){
	        	$('ul#min').css('display','none');
				$('ul#max').css('display','none');
				$('.selectBoxmin').removeClass('active');
				$('.selectBoxmax').removeClass('active');
				$('.selectBed').removeClass('active');
				$('.selectBath').removeClass('active');
				$('.selectBoxmax ul').hide();
				$('.selectBed ul').hide();
				$('.selectBath ul').hide();
	    	});

			$('article.quiz-question').hide();
			$('.quiz-wrap header').hide();
			$('.quiz-nav').hide();
			
			if(isMobile) {
				$(document).scrollTop(65);
			}
			// alert("iOS:"+isOnIOS);

			if (showAgentInfo) {
				extra = '<img src="'+ah_local.tp+'/_img/_authors/250x250/'+agent.photo+'"; id="agent-img" />';
				$(".questiondiv .question-number").before(extra);
				$(".questiondiv .question-number").addClass('haveImg');
				$(".questiondiv .main-question").addClass('haveImg');
			}

			// quiz type : even == city, odd == nationwide or statewide
			this.DB({ query: 'get-status',
				done:function(d){
					var usedAsLandingPage = (arg.length &&
					 			 			 arg.indexOf('F-') != -1);
					var fromPortalLandingPage = Referer.indexOf('portal-landing') != -1;
					var fromQuizResults = Referer.indexOf('quiz-results') != -1;

					if (typeof d['sessionData'] != 'undefined') {
						ah_local.activeSessionID = d['sessionData'].session_id;
						ah_local.sessionID = d['sessionData'].session;
					}
					if (d['response'] == 'null') quiz.initCont();
					else if (d['response'] == 'started') {
						ahtb.open({ height: 160, width: 475, closeOnClickBG: false, hideClose: true, title: 'Previous Quiz Found',
							html: '<p>You previously started a quiz</p>', buttons:[
								{text:'Continue', action: function(){ quiz.getQuiz({id: d['quizPosition'].quiz, question: d['quizPosition'].question_id}); }},
								{text:'Start Over', action: function(){quiz.initCont();ahtb.close();}}
							]
						});		
					}		
					else if (hash == '#sq=1' ||
							 hash == '#sq=0' ||
							d['response'] == 'new' ||
							usedAsLandingPage ||
							fromPortalLandingPage ||
							fromQuizResults) {
						// window.location.hash = '';
						// if (d[0] == 'done' ||
						// 	window.location.hash != '#sq=1')
							quiz.initCont();
						// else 
						// 	quiz.getQuiz({id: d[1].quiz, question: d[1].question_id, sq: 1});							
					} else if (d['response'] == 'done') {
						ahtb.open({ height: 160, 
									width: 450, 
									closeOnClickBG: false, 
									hideClose: true, 
									title: 'Quiz Previously Finished',
									html: '<p>You previously completed a quiz</p>', 
									buttons:[
										{text: 'View Results',action:function(){ 
											window.location = ah_local.wp+'/quiz-results'; }},
										{text: 'Start Over',action:function(){
											quiz.initCont();ahtb.close();}}
									]
								}); 
					}
					else if ( d['response'] == 'processing') {
						ahtb.open({ height: 160, 
									width: 450, 
									closeOnClickBG: false, 
									hideClose: true, 
									title: 'Previous Quiz Processing',
									html: '<p>Old quiz still processing</p>', 
									buttons:[
										{text: 'Start Over',action:function(){
											quiz.initCont();ahtb.close();}}
									]
								}); 
					}
					else //if (d[0] == 'new')
						quiz.initCont();
					//else console.error('invalid status', d[0]);
				}
			});
			$('button#button-skip').on('click', function(){quiz.skipQuestion()});
			$('button#button-back').on('click', function(){quiz.previousQuestion()});
			$('button#button-next').on('click', function(){
				if (!quiz.doingPricePage)
					quiz.selectedSlide();
				else {
					$('.home-options').hide();
					quiz.loadQuestion(0);
				}
			});
		}

		this.doAction = function() {
			if (listing_id != 0)
				window.location = ah_local.wp+"/listing/Q-"+listing_id;
		}

		this.getMode = function() {
			return quiz.quizID;
		}

		this.class = function() {
			return 'quiz';
		}

		this.initCont = function(){
			$('.loading-overlay').fadeOut(250, function() { $(this).css('opacity', '0'); });
			$('.quiz-wrap .basic-info-bg').show();
			$('.skip-to-end').css('visibility', 'hidden');
			$('.mobilequizbuttons button#button-next').hide();
			$('.mobilequizbuttons button#button-back').hide();
			$('.dekstopquizbuttons button#button-next').hide();
			$('.dekstopquizbuttons button#button-back').hide();
			$('.quiz-wrap article.quiz-question').hide();
			$('.quiz-select').show();
			if (typeof quizOptions != 'undefined' &&
				typeof quizOptions.doPredefinedQuiz != 'undefined' &&
				quizOptions.doPredefinedQuiz &&
				!showPopup) {
				$('div.predefined').show(); // in case it was mobile to begin with
				$('div.predefined-quiz').show();
			}
			$('.home-options-progress#progress-bar').show();

			$('.choose').on('click', function() {
				var quizID = parseInt($(this).attr('quiz'));
				$(this).addClass('selected');
				console.log("clicked on quiz:"+quizID);
				quiz.quizID = quizID;
				switch(quizID) {
					case QuizType.QUIZ_LOCALIZED:
						$('.choose[quiz="'+QuizType.QUIZ_BY_ADDRESS+'"]').removeClass('selected');
						$('.choose[quiz="'+QuizType.QUIZ_NATION+'"]').removeClass('selected');
						if (testCityId &&
							testCityId > cityLimit) {
							$('.searchexact').show();
							$('button#showAll').show();
							$('div#median-range').show();
						}
						else {
							$('.searchexact').hide();
							$('button#showAll').show();
							$('button#showAll').hide();
							$('div#median-range').hide();
						}
						$('.specific').show();
						$('.address').hide();
						quiz.showAll();
						break;
					case QuizType.QUIZ_NATION:
						$('.choose[quiz="'+QuizType.QUIZ_BY_ADDRESS+'"]').removeClass('selected');
						$('.choose[quiz="'+QuizType.QUIZ_LOCALIZED+'"]').removeClass('selected');
						$('.specific').hide();
						$('.address').hide();
						quiz.showAll();
						break;
					case QuizType.QUIZ_BY_ADDRESS:
						$('.choose[quiz="'+QuizType.QUIZ_LOCALIZED+'"]').removeClass('selected');
						$('.choose[quiz="'+QuizType.QUIZ_NATION+'"]').removeClass('selected');
						$('.specific').hide();
						$('.address').show();
						quiz.showAll();
						break;
				}
			})

			$('button#nextQ').on('click', function() {
				var ele = $('.autocomplete.area input');
				var location = $('.autocomplete.area input').val();
				if (quiz.quizID != QuizType.QUIZ_NATION) {
					if (location.length == 0) 
						ahtb.alert('Please choose a location to search');
					else {
						var city = $('.autocomplete.area input').attr('city_id');
						testCityName = $('.autocomplete.area input').attr('city_name');
				 		testCityId = city? parseInt(city) : 0;
				 		if (!testCityId || testCityId <= cityLimit) {
				 			getCityId(testCityId, location, quiz.sendNew);
				 			return;
				 		}
				 		else {
				 			testCityId -= cityLimit;
				 			quiz.sendNew(QuizType.QUIZ_LOCALIZED, [testCityId], quiz.doDistance ? quiz.distance : 0);
				 		}
				 	}
					return;
				}
				else
					quiz.sendNew(QuizType.QUIZ_NATION, [], 0);
				console.log("starting Quiz");
			})

			$('button#showAll').on('click', function(){
				if (quiz.quizID == QuizType.NATION) {
					ahtb.alert("Show all homes should only work for localized city searches");
					return;
				}
				if (testCityId == 0 || testCityId <= cityLimit) {
					ahtb.alert("Please pick a city to show homes in.");
					return;
				}
				testCityId -= cityLimit;
				quiz.sendNew(QuizType.QUIZ_LOCALIZED, [testCityId], quiz.doDistance ? quiz.distance : 0, true);
			})

			$('button#findListing').on('click', function() {
				if (listing_id != 0) {
					la( AnalyticsType.ANALYTICS_TYPE_EVENT,
	          			'QuizStartAddress', 
	          			listing_id,
	          			listing_address,
	          			function() {
	          				window.location = ah_local.wp+"/listing/Q-"+listing_id;
	          			});
				}
				else
					ahtb.alert("Listing id not defined");
			})

			$('div.searchexact .distance').on('change', function() {
				var checked = $(this).prop('checked');
				console.log("doDistance - checked:"+checked);
				quiz.doDistance = checked;
			})

			$('.specific #miles').change(function() {
			    distance = parseInt($(".specific #miles").val());
			    console.log("distance is "+distance);
			    quiz.distance = distance;
			});

			// quiz.getQuiz({id: 1});

			// var cities = [];
			// for (var i in ah_local.cities) cities.push({label: ah_local.cities[i].label+' ('+ah_local.cities[i].count+')', value: ah_local.cities[i].label, id: ah_local.cities[i].id });
			// var h = '<div id="split-wrap"><div class="quiz-split-0"><h2>I know where I want to live</h2><p>Only show me homes here</p><input type="text" value="Start typing a location" class="location-autocomplete" /><button class="next-button">Start</button></div>'+
			// '<div class="quiz-split-1"><h2>Show me all homes that fit me</h2><p>I&#39;m open to new places that match my lifestyle</p><button class="next-button">Start</button></div></div>';
			// $(".quiz-wrap").prepend(h);
			// $('input.location-autocomplete').one('click', function(){
			// 	$(this).val('');
			// 	$(this).autocomplete({ source: cities, select: function(e,ui){ $('input.location-autocomplete').attr('city_id', ui.item.id) } });
			// });
			// $('div.quiz-split-0 button').click(function(){
			// 	if ( $('input.location-autocomplete').val() == 'Start typing a location' || $('input.location-autocomplete').val() == '' ) ahtb.alert('Please enter a location before continuing',  {title: 'Invalid Entry', width: 450, height: 150} );
			// 	else quiz.getQuiz({id: 0, location: parseInt( $('input.location-autocomplete').attr('city_id') ) });
			// });
			// $('div.quiz-split-1 button').click(function(){ quiz.getQuiz({id: 1}); });
		}

		this.showAll = function() {
			if (quiz.quizID == QuizType.QUIZ_LOCALIZED) {
				$('button#nextQ').show();
				$('button#findListing').hide();
				if (testCityId &&
					testCityId > cityLimit) {
					$('.searchexact').show();
					$('button#showAll').show();
					$('div#median-range').show();
				}
				else {
					$('.searchexact').hide();
					$('button#showAll').hide();
					$('div#median-range').hide();
				}
			}
			else if (quiz.quizID == QuizType.QUIZ_NATION) {
				$('button#nextQ').show();
				$('.searchexact').hide();
				$('button#showAll').hide();
				$('div#median-range').hide();
				$('button#findListing').hide();
			}
			else { // quiz.quizID == QuizType.QUIZ_BY_ADDRESS
				$('button#nextQ').hide();
				$('.searchexact').hide();
				$('button#showAll').hide();
				$('div#median-range').hide();
				$('button#findListing').show();
				$('button#findListing').prop('disabled', listing_id == 0);
			}
		}
		this.sendNew = function(quizID, location, distance, skipQuiz) {
			console.log("sendNew called with quizId:"+quizID+", distance:"+distance+", cities:"+JSON.stringify(location));
			var quizType = quizID == QuizType.QUIZ_LOCALIZED ? 'Localized' :
						   (quizID == QuizType.QUIZ_STATE ? 'Statewide' :
						   (quizID == QuizType.QUIZ_NATION ? 'Nationwide' :
						   (quizID == QuizType.QUIZ_BY_ADDRESS ? 'Address' : 'Unknown')));
			var locale =   quizID == QuizType.QUIZ_LOCALIZED ? testCityName :
						   (quizID == QuizType.QUIZ_STATE ? location[0] :
						   (quizID == QuizType.QUIZ_NATION ? '' :
						   (quizID == QuizType.QUIZ_BY_ADDRESS ? listing_address : 'Unknown')));
			var valInt =   quizID == QuizType.QUIZ_LOCALIZED ? distance :
						   (quizID == QuizType.QUIZ_STATE ? 0 :
						   (quizID == QuizType.QUIZ_NATION ? 0 :
						   (quizID == QuizType.QUIZ_BY_ADDRESS ? listing_id : 0)));
			la( AnalyticsType.ANALYTICS_TYPE_EVENT,
          			'QuizStart'+quizType, 
          			valInt,
          			locale);
			var data = {
	  			query: 'save-quiz-action',
	  			data: {
	  				// need this  QuizMode.STATE + 1 to send an odd number to save-quiz-axtion
	  				// so that it will grab '1' type questions
	  				quiz: quizID == QuizType.QUIZ_STATE ? QuizType.QUIZ_STATE + 1 : (quizID == QuizType.QUIZ_LOCALIZED ? 0 : quizID), // dictates which quiz questions to load
	  				action: 'start',
	  				location: quizID == QuizType.QUIZ_STATE ? [] : location,
	  				distance: distance,
	  				sessionID:ah_local.activeSessionID,
	  				cassie:ah_local.candy,
	  				user_agent:ah_local.user_agent,
	  				state: quizID == QuizType.QUIZ_STATE ? location[0] : '',
	  				portalAgent: ah_local.agentID,
	  				portalUser: ah_local.portalUser,
	  				wpUser: ah_local.author_id
	  			},
	  			done: function(d){
  					if ( (typeof skipQuiz == 'undefined' ||
  						  !skipQuiz) ) {
						$('.home-options-progress#progress-bar').hide();
						$('#progress-bar > .bar > .fill').stop();
						$('#progress-bar > .bar > .fill').animate({ width: '10%' }, 750);
						if (typeof quizOptions != 'undefined' &&
							typeof quizOptions.doPredefinedQuiz != 'undefined' &&
							quizOptions.doPredefinedQuiz) {
							$('div.predefined-quiz').hide();
						}
  						$('.quiz-select').fadeOut(250, function() {
  							quiz.getQuiz({id: 1,
  										  sq: 1});
  						});
  					}
  					else {
  						console.log("initiate -start");
  						// this will force closure of quiz activity and begin Quiz with no tags.
  						setCookie("QuizCompleted", -1, 2);
						setCookie("QuizListingPos", 0, 2);
						setCookie("QuizParsed", 0, 2);
						setCookie('QuizViewSelector', 'list', 1);
						setCookie("QuizSaveProfile", ah_local.author_id == "0" ? 0 : 1, 1);
						setCookie("QuizNeedSaveProfile", 1, 2);
						la( AnalyticsType.ANALYTICS_TYPE_EVENT,
		          			'ViewAllListings', 
		          			0,
		          			'',
		          			function() {
		          				quiz.sendDone(quizID);
		          			});
						// window.location = ah_local.wp+'/quiz-results/-1';
  					}
	      		},
	      		error: function(d) {
	      			ahtb.alert('Failed to get quiz questions. ' + (typeof d == 'string' ? d : ''));
	      		}
	      	};
	      	quiz.reallyDB(data);
		}
		this.sendDone = function(quizID) {
		  	// this will force closure of quiz activity and begin Quiz with no tags.
		  	$('.loading-overlay p').html("Gathering your listings...");
		  	$('.loading-overlay').fadeIn(250, function() { $(this).css('opacity', '1'); });
		  	var filter = $('input[name=no-limit]').is(':checked') ? QuizFilter.QUIZ_NO_LOWER_PRICE_LIMIT : 0;
			filter |= $('input[name=commercial-ok]').is(':checked') ? QuizFilter.QUIZ_INCLUDE_COMMERCIAL : 0;
			filter |= $('input[name=rental-ok]').is(':checked') ? QuizFilter.QUIZ_INCLUDE_RENTAL : 0;
			filter |= $('input[name=rental-only]').is(':checked') ? QuizFilter.QUIZ_ONLY_RENTAL : 0;
		  	var data = {
		  			query: 'save-quiz-action',
		  			data: {
		  				quiz: quizID,
		  				filter: filter,
		  				action: 'done' },
	  				done: function(d){
	  					console.log("sendDone returned from sending done");
	  					window.setTimeout(function() {
							window.location = ah_local.wp+'/quiz-results/-1';
						}, 500);
	  				}
				}
			console.log("sendDone is executing.");
			quiz.reallyDB(data);
			
	  	}
		this.getQuiz = function(xx){
			//$('article.quiz-question').show();
			$('.quiz-wrap header').show();
			// $('#split-wrap').fadeOut(250, function(){$(this).remove()});
			$('.skip-to-end').one('click', function(){ 
				if (quiz.selected &&
					quiz.stackIt) {
					var theResponse = [];
					$('.selectimages .slide a.selected').each(function(){
						var id = parseInt( $(this).parent().attr('slide-id') );
						if (theResponse.indexOf(id) == -1)
							theResponse.push(id)
					});
					//this.loadingAlert();
					var data = { quiz: quiz.quizID,
									action: 'selected '+quiz.qDB[quiz.currentQuestion].id,
									selected: theResponse,
									next: quiz.qDB[quiz.currentQuestion+1] ? quiz.qDB[quiz.currentQuestion+1].id : null,
									time: quiz.getTimeStamp()
								};
					quiz.stack.push(data);

				}
				quiz.quizCompleted(); 
			}).css('visibility', 'hidden');
			// $('.quiz-nav a').each(function(index){
			// 	$(this).on('click', function(){ for (var i in quiz.qDB){ if(quiz.qDB[i].section == index){quiz.loadQuestion(parseInt(i));break;} } });
			// });
			quiz.quizID = xx.id;
			//quiz.loadingAlert();
			quiz.DB({ query:'get-quiz', data: {quiz: xx.id}, done: function(x){
				if (x){
					quiz.qDB = x;
					if (xx.question) quiz.loadQuestion(xx.question)
					else if (typeof xx.sq == 'undefined') quiz.DB({
						query: 'save-quiz-action',
						data: { action: (xx.action == 'restart' ? 'restart' : 'start'), quiz: xx.id, location: xx.location },
						done: function(){ 
							quiz.loadQuestion(-1); 
							quiz.showStartHint();
						}
					});
					else { 
						quiz.loadQuestion(-1); 
						quiz.showStartHint();
					}
				} else ahtb.alert('Unable to retrieve questions from the database.');
			}});
		}

		this.showStartHint = function() {
			 //ahtb.alert("<p>hasNoShowStartHint:"+hasNoShowStartHint+", isMobile:"+isMobile+"</p>",
			 //			{height:180, width: 400});
			 //return;

//			if (hasNoShowStartHint != 'true') {
//				var h = '<div id="quiz-intro-box" >'+
//							'<div class="close" ></div>'+
//							'<div class="top"><img src="'+ah_local.tp+'/_img/lifestyled-logo-med.png" /></div>'+
//	            			'<div class="main" style="height: 232px;"><p id="welcome" class="quizpopup" style="padding: 20px 0 2px 0;">Welcome to our Lifestyle Assessment</br></p>' +
//								'<p class="subtitle"><span style="font-weight: 600; color: #4E8396;">We will be asking you a few questions</span> about your lifestyle and home preferences. Read each question and answer by selecting the images. </p>'+
//								'<p class="subtitle"><span style="font-weight: 600; color: #4E8396;">At the end of the assessment we will match you</span> to the top homes and locations that fit your lifestyle.</p>' +
//								'<button class="startquiz" style="margin-top: 13px;width: auto!important;padding: .4em 3em;height: auto;">Start</button>' +
	            				//'<input type="checkbox" id="hasNoShowStartHint" /><span id="no-show">&nbsp;'+"Don't show me this again" +'</span>' +
//	            			'</div>'+
//            			'</div>';
//				ahtb.open({html: h,
//		               hideTitle: true,
//		               hideSubmit: true,
//		               width: 600,
//		               height: 310,
//		               opened: function() {
//		                  $('input#hasNoShowStartHint').on('click', function() {
//		                    var val = $(this).prop('checked');
//		                    console.log("NoShowStartHint - "+(val ? "true" : "false"));
//		                    setCookie("NoShowStartHint", (val ? "true" : "false"), 30);
//		                  })
											
//						  $('#quiz-intro-box .close').on('click', function() {
//		                    ahtb.close();
		                    // setCookie("NoShowBenefits", (val = "true"), 30);
//		                  })

//		                  $('button.startquiz').on('click', function() {
//		                    ahtb.close();
												// setCookie("NoShowBenefits", (val = "true"), 30);
//		                  })
//		        	   }});
//			}
//			else
				ahtb.close();
		}

		this.loadSlides = function(q, preSelected) {
			var question = this.qDB[q];
			$('.question-info').fadeOut({duration:250, queue: false});
			$(".selectimages div.slides").fadeOut({duration:250, queue: false, complete: function(){
				// if (question.multi == 0) $('#button-next').hide();
				// else 
				if (quiz.doingPricePage)
					return;

				$('.mobilequizbuttons button#button-next .text, .dekstopquizbuttons button#button-next .text').html((q == quiz.qDB.length-1) ? 'Go to Results' : 'Next Question');
				if (question.type == 3 && question.slides.length != 3) {
					if (question.slides.length <= 6) question.type = 6;
					else question.type = 12;
				} else if (question.type == 12 && question.slides.length <= 6) question.type = 6;
				var h = '';
				$(".selectimages div.slides").attr("class", "slides type-"+question.type);
				//var mobilePath = '580x550';
				// var retinaPath = '';
				var thePath = '580x550';
				var lowResLoaded = 0;
				if ( !isMobile && (question.type == 3 || question.type == 6 || question.type == 12) ) {
					if (isRetina) {
						if (question.type == 3){
							$(".selectimages div.slides").attr("class", "slides type-3");
							thePath = '700x900';
						} else if (question.type == 6){
							$(".selectimages div.slides").attr("class", "slides type-6");
							thePath = '650x530';
						} else if (question.type == 12) {
							$(".selectimages div.slides").attr("class", "slides type-12");
							thePath = '530x340';
						}
					}
					else {
						if (question.type == 3){
							$(".selectimages div.slides").attr("class", "slides type-3");
							thePath = '350x450';
						} else if (question.type == 6){
							$(".selectimages div.slides").attr("class", "slides type-6");
							thePath = '325x265';
						} else if (question.type == 12) {
							$(".selectimages div.slides").attr("class", "slides type-12");
							thePath = '265x170';
						}
					}
				}
				
				var classdef = isMobile ? 'mobile' : 'desktop';
				//ahtb.alert("loading "+classdef+", path:"+thePath);
				for (var i = 0; i < question.slides.length; i++) {
					// if (!isMobile) {
						h+= '<li class="slide '+classdef+' main-slide-'+i+' slide-'+i+'" data-slide="'+i+'" slide-id="'+question.slides[i].id+'">'+
								'<a href="javascript:'+(quiz.doingPricePage ? '' : 'quiz.selectedSlide('+i+')' )+';">'+
									'<h2>'+question.slides[i].title+'</h2>'+
									'<span class="checkmarkoff"></span>'+
									'<img class="image low-res" src="'+ah_local.tp+'/_img/_quiz/'+thePath+'/'+question.slides[i].file+'"  data-slide="'+i+'" slide-id="'+question.slides[i].id+'"/>'+
									'<div class="meta2 '+(quiz.doingPricePage ? '' : 'active')+'"></div>'+
										'<div class="meta '+(quiz.doingPricePage ? '' : 'active')+'">'+
										'<span class="checkmark"><img class="checkmarkon" src="'+ah_local.tp+'/_img/page-quiz/checkmark.png" /></span>'+
									'</div>'+
								'</a>';
						// if (isRetina)
						// 	h+=	'<img style="width: 0; height: 000;" class="retina" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-slide="'+i+'" slide-id="'+question.slides[i].id+'"/>';
						h+=	'</li>';
					// }
					// else
					// 	h+= '<li class="slide mobile slide-'+i+'" data-slide="'+i+'" slide-id="'+question.slides[i].id+'" >'+
					// 			'<a href="javascript:'+(quiz.doingPricePage ? '' : 'quiz.selectedSlide('+i+')' )+';">'+
					// 				'<h2>'+question.slides[i].title+'</h2>'+
					// 				'<span class="checkmarkoff"></span>'+
					// 				'<img class="image" src="'+ah_local.tp+'/_img/_quiz/'+mobilePath+'/'+question.slides[i].file+'" />'+
					// 				'<div class="meta2 '+(quiz.doingPricePage ? '' : 'active')+'"></div>'+
					// 				'<div class="meta '+(quiz.doingPricePage ? '' : 'active')+'">'+
					// 					'<span class="checkmark"><img class="checkmarkon" src="'+ah_local.tp+'/_img/page-quiz/checkmark.png" /></span>'+
					// 				'</div>'+
					// 			'</a>'+
					// 		'</li>';
				}

				$('.selectimages div.slides').html('<ul>'+h+'</ul>');
				// if (isRetina) {
				// 	var ele = $('.slide .low-res');
				// 	ele.on('load', function() {
				// 		lowResLoaded++;
				// 		console.log("low-res loaded, now at "+lowResLoaded);
				// 		if (lowResLoaded == question.type) {
				// 			console.log("load hi-res, count:"+question.type+", path:"+retinaPath);
				// 			for (var i = 0; i < question.slides.length; i++) {
				// 				// $(this).parent().siblings('.retina').attr('src', ah_local.tp+'/_img/_quiz/'+retinaPath+'/'+question.slides[i].file);
				// 				var slide =  $('.main-slide-'+i);
				// 				console.log("slide length:"+slide.length);
				// 				slide.append('<img style="width: 0; height: 000;" class="retina" src="'+ah_local.tp+'/_img/_quiz/'+retinaPath+'/'+question.slides[i].file+'" data-slide="'+i+'" slide-id="'+question.slides[i].id+'"/>');
				// 				//var ele = $('.slide[data-slide='+i+'] .retina');
				// 				//console.log("retina ele:"+ele.length+", file:"+ah_local.tp+'/_img/_quiz/'+retinaPath+'/'+question.slides[i].file);
				// 				// ele.src = ah_local.tp+'/_img/_quiz/'+retinaPath+'/'+question.slides[i].file;
				// 				// $('.slide[data-slide='+i+'] .retina').attr('src', ah_local.tp+'/_img/_quiz/'+retinaPath+'/'+question.slides[i].file);
				// 				// ele.attr('src', function() {
				// 				// 	return ah_local.tp+'/_img/_quiz/'+retinaPath+'/'+question.slides[i].file;
				// 				// });
				// 				// console.log("double check src:"+ele.attr('src'));
				// 				console.log("appended retina");
				// 			}

				// 			$('.slide').find('.retina').on('load',function() {
				// 				var slide = $(this).attr('data-slide');
				// 				var id = $(this).attr('slide-id');
				// 				var hasRetina = $(this).hasClass('retina');
				// 				var ele = $('.main-slide-'+slide+' img.low-res');
				// 				console.log("Retina for slide:"+slide+", id:"+id+", hasRetina:"+(hasRetina ? 'yes' : 'no')+" loaded "+$(this).attr('src')+", ele:"+ele.length);
				// 				var src = $(this).attr('src');
				// 				// ele.animate({src: src},
				// 				// 			{queue: false, duration: 500, function() {
				// 				// 				console.log("animated for slide:"+$(this).attr('data-slide')+", src:"+$(this).attr('src'));
				// 				// 			}})
				// 				ele.fadeOut('fast', function() {
				// 					console.log("fadeOut..slide:"+$(this).attr('data-slide'));
				// 					// $(this).attr('src', src).fadeIn('fast', function() {
				// 					// 	console.log("fadeIn for slide:"+$(this).attr('data-slide')+", src:"+$(this).attr('src'));
				// 					// });
				// 				})
				// 				// ele.attr('src', function() {
				// 				// 	return src;
				// 				// });
				// 				// console.log("double check retina src:"+ele.attr('src'));
				// 			}).each(function() {
				// 			  if(this.complete) $(this).load();
				// 			});
				// 		}

				// 	}).each(function() {
				// 	  if(this.complete) $(this).load();
				// 	});
				// }

				$('.selectimages div.slides').fadeIn({duration:250, complete:function() {
					if (quiz.doingPricePage) {
						$('.selectimages div.slides li.slide').addClass("hideimg");
						$('.quiz-wrap .selectimages').hide();
						$('.quiz-wrap .basic-info-bg').show();
						$('.quiz-wrap .basic-info-title').show();
						$('.loading-overlay').css('opacity', '0');
						timeoutID = window.setTimeout(delayhide, 500);
						function delayhide() {
							$('.loading-overlay').hide();
						}
					}
					else {
						$('.selectimages div.slides .image').removeClass('hideimg');
						$('.quiz-wrap .basic-info-bg').hide();
						$('.quiz-wrap .basic-info-title').hide();
						$('.quiz-wrap .selectimages').show();
							if ( typeof preSelected != 'undefined') {
								// ahtb.alert("orientation change, isMobile:"+(isMobile ? 'yes' : 'no')+", isRetina:"+(isRetina ? 'yes' : 'no')+", preSelected:"+preSelected.length);
								quiz.selected = preSelected.length;
								for(var i in preSelected)
									$('.selectimages .slide-'+preSelected[i]+' a').addClass('selected');
						}
					}
				}});
				 // $('div.slide').on('touchstart', function(e) {
				 //        e.preventDefault();
				 //        $(this).find('a').addClass('selected');
				 //    });
				 //    $('div.slide').on('touchend', function(e) {
				 //        e.preventDefault();
				 //        if (!$(this).find('a').hasClass("selected"))
				 //        	$(this).find('a .meta').css('opacity', '0');
				 //    });
				if (question.multi == 1 && !quiz.doingPricePage) $('button#button-next').css('display', 'inline');
				if (question.multi == 1 && !quiz.doingPricePage) $('.mobilequizbuttons button#button-next').css('display', 'block');
				
				q == 0 ? $('button#button-back').css('display', 'none') : $('button#button-back').css('display', 'inline');
				q == 0 ? $('button#button-back').css('display', 'none') : $('.mobilequizbuttons button#button-back').css('display', 'block');
				quiz.progressBar(q);
				if (quiz.doingPricePage) $("span.mobileinfo").html();
				else if (!question.multi_num && question.multi) $("span.mobileinfo").html('Select all slides that apply');
				else $("span.mobileinfo").html('Select '+(question.multi_num && question.multi_num > 1 ? 'up to ' : '') +(question.multi_num ? question.multi_num : 1 )+' slide'+(question.multi_num && question.multi_num > 1 ? 's' : '')+'' );

				if (quiz.doingPricePage)  $("span.selected.info").html();
				else if (!question.multi_num && question.multi) $("span.selected").html('Please choose all slides that apply');
				else $("span.selected").html('Please select '+(question.multi_num && question.multi_num > 1 ? 'up to ' : '') +(question.multi_num ? question.multi_num : 1 )+' slide'+(question.multi_num && question.multi_num > 1 ? 's' : '') );
				//var clickRight = '<span class="desktop-question"> CLICK ON THE IMAGES TO THE RIGHT</span>';
				var prompt = quiz.doingPricePage ? "First, the basics." : question.prompt;
				$('.question-info .main-question').html('<span class="agent-quotes left">"</span>'+prompt+'<span class="agent-quotes">"</span>'); //+(doingActivitiesNow ? clickRight : ''));
				$('.question-info').fadeIn(250);
				if (quiz.hasActivity) {
					$('.skip-to-end').css('visibility', 'visible');
				}  
				else {
					$('.skip-to-end').css('visibility', 'hidden');
				}
				$('.selectimages div.slides').scrollLeft(0);
			}})
		}

		this.loadQuestion = function(q, fromPop, preSelected){
			if (!quiz.state) quiz.state = "InQuiz";
			if (q >= this.qDB.length) this.quizCompleted();
			else if (q != -1 && !(this.qDB[q].enabled & 1)) this.loadQuestion(q+1);
			else {
				quiz.selected = 0;
				quiz.doingPricePage = false;
				if (q == -1) {
					quiz.doingPricePage = true;
					q = 0;
					$('.mobilequizbuttons button#button-next').hide();
					$('.dekstopquizbuttons button#button-next').hide();
					$('.quiz-wrap article.quiz-question').hide();
					$('.home-options').show();
				}
				else {
					$('.mobilequizbuttons button#button-next').show();
					$('.dekstopquizbuttons button#button-next').show();
				}
				quiz.currentQuestion = q;
				var doingActivitiesNow = false;
				if (quiz.qDB[quiz.currentQuestion].id == QuizState.FAV_ACTIVITES) {
					quiz.hasActivity = false;
					doingActivitiesNow = true;
				}
				if (q && typeof fromPop == 'undefined') history.pushState({quiz: q}, "Quiz"+q, "?quiz="+q);
				quiz.loadSlides(q, preSelected);
			}

			$('button#button-skip').hide();
			// (quiz.doingPricePage || quiz.qDB[quiz.currentQuestion].id == QuizState.FAV_ACTIVITES) ? $('button#button-skip').hide() : $('button#button-skip').show();
			
			if ($('article.quiz-question').css('display') == 'none' &&
				 !quiz.doingPricePage) {
				$('.quiz-wrap header').fadeIn({duration: 250, queue: false});
				$('article.quiz-question').fadeIn({duration: 250, queue: false});
			}

		}
		this.selectedSlide = function(slidePosition){
			// if (this.qDB[quiz.currentQuestion].multi === 1 && slidePosition != -1) {
				if (slidePosition != null) {
					var removed = false;
					if ( $('.selectimages .slide-'+slidePosition+' a').hasClass('selected') ) {
						$('.selectimages .slide-'+slidePosition+' a').removeClass('selected');
						removed = true;
						la(	AnalyticsType.ANALYTICS_TYPE_CLICK,
		          			'DeselectSlide', 
		          			quiz.qDB[quiz.currentQuestion].slides[slidePosition].id,
		          			quiz.qDB[quiz.currentQuestion].slides[slidePosition].title);
						ga('send', {
						  'hitType': 'event',          // Required.
						  'eventCategory': 'button',   // Required.
						  'eventAction': 'click',      // Required.
						  'eventLabel': 'DeselectSlide'
						});
					}
					quiz.selected = 0;
					// if (!isMobile)
					// 	$('.slide.desktop a.selected').each(function(){ quiz.selected++ });
					// else
					// 	$('.slide.mobile a.selected').each(function(){ quiz.selected++ });
					$('.selectimages .slide a.selected').each(function(){ quiz.selected++ });
					
					// check if this is a single select and already have one selected
					if (quiz.selected && 
						this.qDB[quiz.currentQuestion].multi == 0) {
						$('.selectimages .slide a.selected').each(function(){ 
							$(this).removeClass('selected');
						});
						quiz.selected--; 
					}
					if (quiz.selected < this.qDB[quiz.currentQuestion].multi_num ||
						this.qDB[quiz.currentQuestion].multi_num == 0 ||
						this.qDB[quiz.currentQuestion].multi_num == null){
						if ( !$('.selectimages .slide-'+slidePosition+' a').hasClass('selected') && !removed ) {
							$('.selectimages .slide-'+slidePosition+' a').addClass('selected');
							quiz.selected++;
							la(	AnalyticsType.ANALYTICS_TYPE_CLICK,
			          			'SelectSlide', 
			          			quiz.qDB[quiz.currentQuestion].slides[slidePosition].id,
			          			quiz.qDB[quiz.currentQuestion].slides[slidePosition].title);
							ga('send', {
							  'hitType': 'event',          // Required.
							  'eventCategory': 'button',   // Required.
							  'eventAction': 'click',      // Required.
							  'eventLabel': 'SelectSlide'
							});
						}
					} else ahtb.alert('Maximum number of choices already selected.', {title: 'Invalid Entry', width: 450, height: 180});
				} else {
					if (quiz.qDB[quiz.currentQuestion].id == QuizState.FAV_ACTIVITES) {
						if (quiz.selected == 0) {
							ahtb.alert("Please pick at least one activity",
										{height: 150});
							return;
						}
						else
							quiz.hasActivity = true;
					}
					var theResponse = [];
					// if (!isMobile)
					// 	$('.slide.desktop a.selected').each(function(){theResponse.push(parseInt( $(this).parent().attr('slide-id') ))});
					// else
					// 	$('.slide.mobile a.selected').each(function(){theResponse.push(parseInt( $(this).parent().attr('slide-id') ))});
					$('.selectimages .slide a.selected').each(function(){theResponse.push(parseInt( $(this).parent().attr('slide-id') ))});
					//this.loadingAlert();
					
					la(	AnalyticsType.ANALYTICS_TYPE_CLICK,
		      			'Next',
		      			quiz.currentQuestion);
					ga('send', {
						  'hitType': 'event',          // Required.
						  'eventCategory': 'button',   // Required.
						  'eventAction': 'click',      // Required.
						  'eventLabel': 'Next'
						});
					
					var data = { quiz: quiz.quizID,
									action: 'selected '+quiz.qDB[quiz.currentQuestion].id,
									selected: theResponse,
									next: quiz.qDB[quiz.currentQuestion+1] ? quiz.qDB[quiz.currentQuestion+1].id : null,
									time: quiz.getTimeStamp()
								};
					if (!quiz.stackIt) 
						this.DB({
							query: 'save-quiz-action',
							data: data,
							// done: function(){ quiz.loadQuestion(quiz.currentQuestion+1); ahtb.close(); }
							done: function(){}
						});
					else
						quiz.stack.push(data);
					quiz.loadQuestion(quiz.currentQuestion+1);
				}
			// } else {
			// 	// this.loadingAlert();
			// 	var data = {
			// 			quiz: quiz.quizID,
			// 			action: 'selected '+quiz.qDB[quiz.currentQuestion].id,
			// 			selected: [quiz.qDB[quiz.currentQuestion].slides[slidePosition].id],
			// 			next: quiz.qDB[quiz.currentQuestion+1] ? quiz.qDB[quiz.currentQuestion+1].id : null,
			// 			time: quiz.getTimeStamp()
			// 		};
			// 	if (!quiz.stackIt) 
			// 		this.DB({
			// 			query: 'save-quiz-action',
			// 			data: data,
			// 			// done: function(){ quiz.loadQuestion(quiz.currentQuestion+1); ahtb.close(); }
			// 			done: function(){}
			// 		});
			// 	else
			// 		quiz.stack.push(data);
			// 	quiz.loadQuestion(quiz.currentQuestion+1);
			// }
		}
		this.skipQuestion = function(){
			if (quiz.selected == 0 &&
				quiz.qDB[quiz.currentQuestion].id == QuizState.FAV_ACTIVITES) {
				ahtb.alert("Please pick at least one activity",
							{height: 150});
				return;
			}

			// this.loadingAlert();
			var data = { quiz: quiz.quizID,
					action: 'skipped '+quiz.qDB[quiz.currentQuestion].id,
					next: quiz.qDB[quiz.currentQuestion+1] ? quiz.qDB[quiz.currentQuestion+1].id : null,
					time: quiz.getTimeStamp()
				};
			la(	AnalyticsType.ANALYTICS_TYPE_CLICK,
      			'Skip',
      			quiz.currentQuestion);
			ga('send', {
			  'hitType': 'event',          // Required.
			  'eventCategory': 'button',   // Required.
			  'eventAction': 'click',      // Required.
			  'eventLabel': 'Skip'
			});
			
			if (!quiz.stackIt) 
				this.DB({ query: 'save-quiz-action',
					data: data,
					done: function(){
						// quiz.loadQuestion(quiz.currentQuestion+1); ahtb.close();
					}
				});
			else
				quiz.stack.push(data);
			quiz.loadQuestion(quiz.currentQuestion+1); 
		}
		this.previousQuestion = function(fromPop){
			// this.loadingAlert();
			var data = { quiz: quiz.quizID,
						action: 'back '+quiz.qDB[quiz.currentQuestion].id,
						next: quiz.qDB[quiz.currentQuestion-1] ? quiz.qDB[quiz.currentQuestion-1].id : null,
						time: quiz.getTimeStamp()
				};
			la(	AnalyticsType.ANALYTICS_TYPE_CLICK,
      			'Prev',
      			quiz.currentQuestion);
			ga('send', {
			  'hitType': 'event',          // Required.
			  'eventCategory': 'button',   // Required.
			  'eventAction': 'click',      // Required.
			  'eventLabel': 'Prev'
			});
			if (!quiz.stackIt) 
				this.DB({query: 'save-quiz-action',
					data: data,
					done: function(){
						// quiz.loadQuestion(quiz.currentQuestion-1); ahtb.close();
					}
				});
			else
				quiz.stack.pop();
				// quiz.stack.push(data);
			quiz.loadQuestion(quiz.currentQuestion-1, fromPop);
		}
		this.quizCompleted = function(){
			if (quiz.tasks.length ||
				quiz.dbBusy) {
				if (!ahtb.opened)
					quiz.loadingAlert();
				window.setTimeout(function() {
					console.log("quizCompleted, but dbBusy, tasks/stack remaining: "+(quiz.tasks.length ? quiz.tasks.length : quiz.stack.length));
					if (!quiz.leavingPage)
						quiz.quizCompleted();
				}, 200);
				return;
			}
			else if (quiz.stack.length) {
				if (!ahtb.opened)
					quiz.loadingAlert();
				var data = { quiz: quiz.quizID,
							action: 'done',
							time: quiz.getTimeStamp()
				};
				quiz.stack.push(data);
				var homeOptions = { beds: $('.selectBed').attr('value'),
									baths: $('.selectBath').attr('value')
								  };
				var filter = $('input[name=no-limit]').is(':checked') ? QuizFilter.QUIZ_NO_LOWER_PRICE_LIMIT : 0;
				filter |= $('input[name=commercial-ok]').is(':checked') ? QuizFilter.QUIZ_INCLUDE_COMMERCIAL : 0;
				filter |= $('input[name=rental-ok]').is(':checked') ? QuizFilter.QUIZ_INCLUDE_RENTAL : 0;
				filter |= $('input[name=rental-only]').is(':checked') ? QuizFilter.QUIZ_ONLY_RENTAL : 0;				
				console.log("beds:"+homeOptions.beds+", baths:"+homeOptions.baths+", filter:"+filter);
				quiz.reallyDB({ query: 'save-all-actions',
								data: {stack: quiz.stack,
									   price: quiz.price,
									   homeOptions: homeOptions,
									   presetProcessing: 1,
									   filter: filter },
								done: function(d) {
									console.log("save-all-actions returned");
									la(	AnalyticsType.ANALYTICS_TYPE_CLICK,
							      		'QuizCompleted',
							      		parseInt(ah_local.agentID),
							      		quiz.currentQuestion.toString());
									ga('send', {
									  'hitType': 'event',          // Required.
									  'eventCategory': 'button',   // Required.
									  'eventAction': 'click',      // Required.
									  'eventLabel': 'QuizCompleted'
									});

									// quiz.stack.length = 0;
									data = {quiz: -1};
									quiz.reallyDB({ query: 'run-quiz',
													data: data,
													done: function(d) {
														quiz.stack.length = 0;
														console.log("run-quiz returned");
													} });	
									// console.log("run-quiz returned");
									la(	AnalyticsType.ANALYTICS_TYPE_EVENT,
							      		'run-quiz completed',
							      		parseInt(ah_local.agentID));
									// window.setTimeout(function() {
									// 	console.log("going to reallyGoToResults after run-quiz");
									// 	quiz.reallyGoToResults();
									// }, 3500);		
									return;					
								},
								error: function(d) {
									ahtb.alert("Please try again, there was a problem saving your quiz.", { height: 150, width: 500 });
									$('.skip-to-end').one('click', function(){ quiz.quizCompleted(); }).css('visibility', 'visible');
								} });
				window.setTimeout(function() {
					console.log("quizCompleted, sent up stack to backend");
					quiz.quizCompleted();
				}, 500);
				// quiz.stack.length = 0;
			}
			else {
				if (!quiz.leavingPage)
					window.setTimeout(function() {
						console.log("quizCompleted, ready to go to reallyGoToResults");
						quiz.reallyGoToResults();
					}, 500);
				return;
			}
			// setCookie("QuizCompleted", quiz.quizID, 2);
			
			// this.loadingAlert();
			// $('div.quiz-wrap').html('<h2 style="text-align:center; margin:7em auto;">Assessment completed. Sorting through 180,000 listings...</h2>');
			// this.DB({query: 'save-quiz-action',
			// 	data: { quiz: quiz.quizID, action: 'done' },
			// 	done: function(){
			// 		ahtb.loading('Loading your quiz results...',{ height: 100 });
			// 		setCookie("QuizListingPos", 0, 2);
			// 		quiz.state = "DoneQuiz";
			// 		window.location = ah_local.wp+'/quiz-results';
			// 	}
			// });
		}
		this.reallyGoToResults = function() {
			console.log("reallyGoToResults entered");
			quiz.leavingPage = true;

			setCookie("QuizCompleted", '-1', 2);
			setCookie("QuizListingPos", 0, 2);
			setCookie("QuizParsed", 0, 2);
			setCookie('QuizViewSelector', 'list', 1);
			setCookie("QuizSaveProfile", ah_local.author_id == "0" ? 0 : 1, 1);
			setCookie("QuizNeedSaveProfile", 1, 2);
			setCookie("LastViewedProfileIndex", "-1", 7);
			setCookie("QuizResultsHistory",'', 1);
			quiz.state = "DoneQuiz";
			
			// window.location.replace(ah_local.wp+'/quiz-results/-1'); 
			console.log("reallyGoToResults - doing redirect");
			// la(	AnalyticsType.ANALYTICS_TYPE_EVENT,
			// 	'redirect to quiz-results');

			// $.each(ajaxRequests, function(i, v) {
			// 	console.log("aborting ajax request:"+ i);
			//     v.abort();
			// });
			// $(location).attr('href', ah_local.wp+'/quiz-results/-1');
			window.location = ah_local.wp+'/quiz-results/-1';
			console.log("reallyGoToResults - after redirect");			
		}
		this.progressBar = function(theValue){
			theValue = theValue + 3;
			var extra = '';
			$("#progress-bar .progress .number").html(theValue);
			$(".questiondiv .question-number .number").html(theValue);
			theValue = theValue - 1;
			theValue = 100*( theValue / (this.qDB.length + 2) );
			theValue = Math.round(theValue);
			$("#progress-bar > .bar > .fill").stop();
			$("#progress-bar > .bar > .fill").animate({ width: theValue+'%' }, 1000);
			if (theValue < 1) $("#progress-bar > .bar > .fill").addClass('empty');
			else $("#progress-bar > .bar > .fill").removeClass('empty');
		}
		this.DB = function(xx){
			if (quiz.dbBusy) {
				quiz.tasks.push(xx);			
				return;
			}
			quiz.reallyDB(xx);
		}
		this.reallyDB = function(xx) {
			if (xx != null){
				if (xx.data == null) xx.data = {};
				// xx.data.sessionID = ah_local.sessionID;
				xx.data.sessionID = ah_local.activeSessionID;
				quiz.dbBusy++;
				var call = $.ajax({
					type: "POST",
					dataType: "json",
					url: ah_local.tp+'/_pages/ajax-quiz.php',
					data: { query: xx.query, data: xx.data },
					async: true,
					error: function($xhr, $status, $error) {
						var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
						if (xx.query != 'run-quiz') {
							quiz.dbBusy--;
							ahtb.alert("Failed:"+xx.query+", msg: "+msg);
						}
						else
				    		console.log("Got error back for run-quiz: "+msg);
					},
					success: function(d) {
						quiz.dbBusy--;
						console.log("returned from "+xx.query);
				        if (d && d.status != 'OK') xx.error == null ? ahtb.alert('There was a problem.<br/><small>'+d.data+'</small>') : xx.error(d.data);
				        else if (d && xx.done != null) xx.done(d.data);
				        else console.log(d);
				        if (quiz.tasks.length) {
				        	console.log("Quiz has "+quiz.tasks.length+" tasks left to do.");
				        	quiz.DB(quiz.tasks.shift());
				        }
					}
				});
				console.log("Made ajax call to "+xx.query);
				// ajaxRequests.push( call );

				// $.post(ah_local.tp+'/_pages/ajax-quiz.php', 
				// 	{ query: xx.query, data: xx.data }, 
				// 	function(){}, 'JSON') 
				// .done(function(d){
				// 	quiz.dbBusy--;
				// 	console.log("returned from "+xx.query);
			 //        if (d && d.status != 'OK') xx.error == null ? ahtb.alert('There was a problem.<br/><small>'+d.data+'</small>') : xx.error(d.data);
			 //        else if (d && xx.done != null) xx.done(d.data);
			 //        else console.log(d);
			 //        if (quiz.tasks.length) {
			 //        	console.log("Quiz has "+quiz.tasks.length+" tasks left to do.");
			 //        	quiz.DB(quiz.tasks.shift());
			 //        }
			 //    })
			 //    .fail(function(d){
			 //    	if (xx.query != 'run-quiz') {
				//     	quiz.dbBusy--;
				//     	if (typeof d == 'object') {
				//     		if (typeof d.responseText != 'undefined')
				//     			ahtb.alert(d.responseText, { height: 200 });
				//     		else if (typeof d.data != 'undefined')
				//     			ahtb.alert(d.data, { height: 200 });
				//     	}
				//     	else
				//     		ahtb.alert("Caught an error :(", { height: 150 });
				//     }
				//     else
				//     	console.log("Got error back for run-quiz");
			 //    });
			}
		}
		this.loadingAlert = function(){
			ahtb.loading('Loading...', {
				//hideTitle: true,
				width: 350,
				height: 100
			});
		}
		this.getTimeStamp = function() {
			var ts = '';
			try {
				ts = Date().toLocaleFormat("%Y-%m-%d %H:%M:%S");
			}
			catch(err) {
				ts = Date().toLocaleString();
			}
			return ts;
		}
	}
	controller = quiz;
	quiz.getReady();
});