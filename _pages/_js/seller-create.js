
// function validateEmail(email){
// 	var ele = email.match(/(?:[A-z0-9!#$%&'*+/=?^_`{|}~-]*)+(?:\.[A-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9]*(?:[A-z0-9-]*[A-z0-9]*)?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)/); /* ' this just to make the regex exppression not screw up the text colorization */
// 	return ele == null ? false : true;
// }

jQuery(document).ready(function($){

	reg = new function() {
		this.state = -1;

		this.init = function() {
			if (hideAgent) { // defined in header.js
				$('#professional').hide();
				$('#bre').hide();
			}

			if (user) {
				$('.bottom').show();
				$('.bottom #upgrade').on('click', function() {
					register(true);
				})
			}
			else
				$('.top').show();
		}
	}

	reg.init();

	$('#bre #states').change(function(e) {
		e.preventDefault();
		var val = $("#bre #states option:selected").val();
		if (val == -1 &&
			$('#register [name=realtor-id]').val() != '')
			ahtb.alert("Please pick the state where your license is registered.",
				{height: 150});
		else if (breMeta == null ||
				 (breMeta != null &&
				  breMeta.state.length == 0)) {
			reg.state = val;
			console.log("State is now set to "+reg.state);
		}
		else {
			var ele = $('#bre #states option[value="'+breMeta.state+'"]');
			ele.prop('selected', true);
		}
	});

    $('#marker').hide();
    $('#verify').keyup(function(e) {
    	e.preventDefault();
    	var len = $(this).val().length;
    	len ? $('#marker').show() : $('#marker').hide();
    	//$('#marker').attr('visibility', len ? 'visible' : 'hidden');
    	var hasCancel = $('#marker').hasClass('entypo-attention');
    	if ($(this).val() == $('#password').val()) {
    		if (hasCancel) {
    			$('#marker').removeClass('entypo-attention');
    			$('#marker').addClass('entypo-check');
    			$('#marker').css('color', 'lightgreen');
    		}
    	}
    	else if (!hasCancel) {
    			$('#marker').removeClass('entypo-check');
    			$('#marker').addClass('entypo-attention');
     			$('#marker').css('color', 'red');
   		}
    	
    })

    $('#password').keyup(function(e) {
    	e.preventDefault();
    	var len = $(this).val().length;
    	var vLen = $('#verify').val().length;
    	len && vLen ? $('#marker').show() : $('#marker').hide();
    	//$('#marker').attr('visibility', len ? 'visible' : 'hidden');
    	var hasCancel = $('#marker').hasClass('entypo-attention');
    	if ($(this).val() == $('#verify').val()) {
    		if (hasCancel) {
    			$('#marker').removeClass('entypo-attention');
    			$('#marker').addClass('entypo-check');
    			$('#marker').css('color', 'lightgreen');
    		}
    	}
    	else {
    		if (!hasCancel) {
    			$('#marker').removeClass('entypo-check');
    			$('#marker').addClass('entypo-attention');
     			$('#marker').css('color', 'red');
   			}
    	}
    })

    $('#professional #realtor').on('change', function() {
    	var val = $(this).prop('checked');
    	if (val) {
    		$('#bre').show();
    	}
    	else {
    		$('#bre').hide();
	    }
    }).click();

    if (breMeta != null) {
		// sets the selected state
		// using <option selected="selected"> doesn't work!!!
		if (breMeta.state.length) {
			$('#bre #states').val(breMeta.state);
			reg.state = breMeta.state.length ? breMeta.state : -1;
		}
		$('#breID').val(breMeta.BRE);
		$('#breID').prop('disabled', true);
	}

    // $('#bre').hide();
    if (seller != null &&
    	seller != '' &&
    	hideAgent == false) {
    	$('#register input[name=first-name]').val(seller.first_name);
    	$('#register input[name=last-name]').val(seller.last_name);
    	$('#register input[name=email]').val(seller.email);
    	$('#register input[name=login-id]').val(seller.email);
    	$('#states option[value="'+sellerState+'"]').prop('selected', true);
    	reg.state = sellerState;
    } else if (user != null) {
    	$('#register input[name=first-name]').val(user.first_name);
    	$('#register input[name=last-name]').val(user.last_name);
    	$('#register input[name=email]').val(user.email);
    	$('#register input[name=login-id]').val(user.user_login);
    }

    $("#submitbtn").click(function(e){
		e.preventDefault();
		var bre = '';
		if ( $('#professional #realtor').prop('checked') &&
			 (bre = $('#register input[name=realtor-id]').val()) != '' ) {
			if (bre.length < 5) {
				ahtb.alert("Please enter a valid Realtor ID.",
						{height: 150});
				return;
			}
			if (reg.state == -1) {
				ahtb.alert("Please select the state your Realtor ID is registered.",
							{height: 150});
				return;
			}
		}
		var first = '';
		if ( (first = $('#register input[name=first-name]').val()) == '') {
			ahtb.alert("Please enter your first name.",
						{height: 150});
			return;
		}
		var last = '';
		if ( (last = $('#register input[name=last-name]').val()) == '') {
			ahtb.alert("Please enter your last name.",
						{height: 150});
			return;
		}

		var email = '';
		if ( (email = $('#register input[name=email]').val()) == '') {
			ahtb.alert("Please enter your email adress.",
						{height: 150});
			return;
		}

		var login = '';
		if ( (login = $('#register input[name=login-id]').val()) == '') 
			login = email;

		if (!validateEmail(email)) {
			ahtb.alert("Please check your email adress, it doesn't appear to be valid.",
						{height: 150});
			return;
		}
		var passwd = '';
		if ( (passwd = $('#register input[name=password]').val()) == '' ||
			 passwd.length < 8) {
			ahtb.alert("Please enter a password of at least 8 characters.",
						{height: 150});
			return;
		}
		if ( passwd != $('#register #verify').val() ) {
			ahtb.alert("Passwords do not match.",
						{height: 150});
			return;
		}

		if ($('#professional #realtor').prop('checked')) {
			if (bre.length < 5) {
				ahtb.alert("Please enter a valid Realtor ID.",
						{height: 150});
				return;
			}
			if (reg.state == -1) {
				ahtb.alert("Please select the state your Realtor ID is registered.",
							{height: 150});
				return;
			}
		}

		var invitationCode = $('#register #invitation_code').val();
		var optIn = $('input#opt-in ').prop('checked');

		$('#result').html('Registering...').fadeIn();
		$.ajax({
			type: "POST",
			dataType: "json",
			url:  ah_local.tp+"/_pages/ajax-register.php",
			data: { 
				query: 'basic-register',
				data: {
					first_name: first, 
					last_name: last,
					email: email, 
					login: login,
					password: passwd,
					realtor_id: bre,
					state: reg.state,
					inviteCode: invitationCode,
					special: RegistrationMode.REGISTRATION_SPECIAL_ADMIN,
					track: trackable ? source : "0",
					optIn: optIn
				}
			},
			error: function($xhr, $status, $error){
				$('#result').html('Registering...').fadeOut();
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error during Registration', 
							height: 150, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},		
			success: function(d){
				$('#result').html('Registering...').fadeOut();
				//if (d.status == 'OK') window.location = ah_local.wp+'/sellers';
				console.log(d.data);
				if (d.status == 'OK') {
					if (seller.flags & SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 ||
						typeof seller.flags == 'undefined' ||
						seller.flags == 0 ||
						seller.flags == null)
						window.location = ah_local.wp+'/sellers/#agent-match-sales';
					else
						window.location = ah_local.wp+'/sellers/#portal';
				}
				else {
					ahtb.alert(d.data, {height: 210});
				}
			}
		});
	});
	// $('#register input[type=text]').one('click', function(){
	// 	$(this).val('');
	// });
})


