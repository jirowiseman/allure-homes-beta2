var listings;
jQuery(document).ready(function($){
  listings = {
    init: function(){
      listings.gallery.images = [];
      $('.gallery-full').on('click',function(){ listings.gallery.open() });
    },
    gallery: {
      counter: 0,
      wasInit: false,
      open: function(){ ahtb.open({
        hideTitle: true, buttons: [], close: function(){
          listings.gallery.wasInit = false;
          listings.gallery.images = [];
        },
        html: '<div class="gallery-window">'+
          '<section class="top">'+
            '<div class="gallery-image">'+
              '<div class="spin-wrap"><div class="spinner">'+
                '<div class="cube1"></div>'+
                '<div class="cube2"></div>'+
              '</div></div>'+
              '<div class="image"></div>'+
              '<div class="controls">'+
                '<a class="prev"><span class="entypo-left-open-big"></a>'+
                '<a class="next"><span class="entypo-right-open-big"></a>'+
              '</div>'+
            '</div>'+
            '<div class="gallery-meta">'+
              '<h3 class="image-title"></h3>'+
              '<span class="image-description"></span>'+
            '</div>'+
          '</section>'+
          '<div class="gallery-thumbnails-wrap">'+
            '<div class="scrollbar"><div class="handle"><div class="mousearea"></div></div></div>'+
            '<div class="frame" id="gallery-thumbnails"><ul class="clearfix"></ul></div>'+
          '</div>'+
        '</div>',
        opened:function(){
          for (var i in gallery_images) if (gallery_images[i])
            listings.gallery.printThumbnail(i, function(){ 
              listings.gallery.loadThumbnail(i);
              setTimeout(function(){
                var $frame  = $('#gallery-thumbnails');
                var $slidee = $frame.children('ul').eq(0);
                var $wrap   = $frame.parent();
                var $sly_options = {
                    horizontal: 1,
                    itemNav: 'centered',
                    smart: 1,
                    activateOn: 'click',
                    mouseDragging: 1,
                    touchDragging: 1,
                    releaseSwing: 1,
                    startAt: 0,
                    scrollBar: $wrap.find('.scrollbar'),
                    scrollBy: 1,
                    speed: 300,
                    elasticBounds: 1,
                    easing: 'easeOutExpo',
                    dragHandle: 1,
                    dynamicHandle: 1,
                    clickBar: 1,
                    keyboardNavBy: 'items',
                    prev: $('.controls .prev'),
                    next: $('.controls .next'),
                }
                var $sly_events = {
                  load: function(){ if (!listings.gallery.wasInit) setTimeout(function(){listings.gallery.loadImage(0);},100); },
                  active: function(){ listings.gallery.loadImage(this.rel.activeItem); }
                }
                listings.gallery.sly = new Sly($frame, $sly_options, $sly_events).init();
              }, 25);
            });

        }
      })},
      startLoadingThumbImage: function(index, callback){
        if (typeof listings.gallery.images[index] == 'undefined' || typeof listings.gallery.images[index].thumb == 'undefined' || typeof listings.gallery.images[index].thumb.loading == 'undefined'){
          if (typeof listings.gallery.images[index] == 'undefined'){
            listings.gallery.images[index] = {};
            for (var i in gallery_images[index]) listings.gallery.images[index][i] = gallery_images[index][i];
            listings.gallery.images[index].hasThumb = listings.gallery.images[index].file.substr(0,7) != 'http://';
          }
          $img = listings.gallery.images[index];
          if (!$img.hasThumb) listings.gallery.startLoadingFullImage(index, callback ? callback : null);
          else {
            $image = new Image();
            $image.loaded = false;
            $image.loading = true;
            $image.onerror = function(){ listings.gallery.images[index].thumb.loading = false; console.error($img); }
            $image.onload = function(){
              listings.gallery.images[index].thumb.loading = false;
              listings.gallery.images[index].thumb.loaded = true;
              if (callback) callback();
            }
            $img.thumb = $image;
            listings.gallery.images[index] = $img;
            listings.gallery.images[index].thumb.src = (!$img.hasThumb ? $img.file : ah_local.tp + '/_img/_listings/210x120/' + $img.file);
          }
        }
        else if (listings.gallery.images[index].thumb.loaded && callback) callback();
      },
      startLoadingFullImage: function(index, callback){
        if (typeof listings.gallery.images[index] == 'undefined' || typeof listings.gallery.images[index].img == 'undefined' || typeof listings.gallery.images[index].img.loading == 'undefined'){
          if (typeof listings.gallery.images[index] == 'undefined'){
            listings.gallery.images[index] = {};
            for (var i in gallery_images[index]) listings.gallery.images[index][i] = gallery_images[index][i];
            listings.gallery.images[index].hasThumb = listings.gallery.images[index].file.substr(0,7) != 'http://';
          }
          $img = listings.gallery.images[index];
          $image = new Image();
          $image.loaded = false;
          $image.loading = true;
          $image.onerror = function(){ listings.gallery.images[index].img.loading = false; console.error($img); }
          $image.onload = function(){
            listings.gallery.images[index].img.loading = false;
            listings.gallery.images[index].img.loaded = true;
            if (callback) callback();
          }
          $img.img = $image;
          listings.gallery.images[index] = $img;
          listings.gallery.images[index].img.src = (!$img.hasThumb ? $img.file : ah_local.tp + '/_img/_listings/900x500/' + $img.file);
        }
        else if (listings.gallery.images[index].img.loaded && callback) callback();
        // else console.log(index)
      },
      loadImage: function(index){
        this.current = parseInt(index);
        if (this.images[index] && this.images[index].img && this.images[index].img.loaded) $('.gallery-image .image').fadeOut(125, function(){
          var $img = listings.gallery.images[index].img;
          $('.gallery-meta .image-title').html(listings.gallery.images[index].title ? listings.gallery.images[index].title : 'Image '+(index+1)+' / '+listings.gallery.images.length);
          $('.gallery-meta .image-description').html(listings.gallery.images[index].desc ? listings.gallery.images[index].desc : '');
          var width,height;
          var maxWidth = 1200, maxHeight = 500, borderX = 350, borderY = 140;
          if ($img.width > $img.height){
            width = $img.width+borderX < maxWidth ? $img.width : maxWidth-borderX;
            height = ($img.height * width / $img.width);
          } else {
            height = $img.height+borderY < maxHeight ? $img.height : maxHeight-borderY;
            width = ($img.width * height / $img.height);
          }
          console.log(width, height);
          $('.gallery-image .image').html('<img src="'+$img.src+'" height="'+height+'" width="'+width+'" />');
          ahtb.resize(width+borderX, height+borderY);
          $('.gallery-image, .gallery-image .image').css({ 'width': width, 'height': height });
          $('.gallery-image .image').fadeIn(250);
          $('.gallery-image .spinner').fadeOut(250, function(){
            if (!listings.gallery.wasInit) {
              listings.gallery.wasInit = true;
              setTimeout(function(){ listings.gallery.sly.reload();}, 250);
            }
          });
        }); else $('.gallery-image .spinner').fadeIn(250, function(){
          $('.gallery-image .image').fadeOut(250, function(){ $(this).empty() });
          listings.gallery.startLoadingFullImage(index, function(){setTimeout(function(){ listings.gallery.loadImage(index) }, 500)});
        });
      },
      loadThumbnail: function(index){
        if ( this.images[index] && (
          (this.images[index].hasThumb && typeof this.images[index].thumb != 'undefined' && this.images[index].thumb.loaded) ||
          (!this.images[index].hasThumb && typeof this.images[index].img != 'undefined' && this.images[index].img.loaded)
        )) {
          $('.gallery-thumb[index='+index+'] .image').fadeOut(125, function(){
            var $img = listings.gallery.images[index];
            $img = $img.hasThumb ? $img.thumb : $img.img;
            $(this).html('<img src="'+$img.src+'" />');
            $('.gallery-thumb[index='+index+'], .gallery-thumb[index='+index+'] .image').css({ 'width':Math.round($img.width * 90 / $img.height), 'height': 90 });
            $(this).fadeIn(250);
            $('.gallery-thumb[index='+index+'] .spinner').fadeOut(250);
          });
        } else $('.gallery-thumb[index='+index+'] .spinner').fadeIn(250, function(){
          $('.gallery-thumb[index='+index+'] .image').fadeOut(250, function(){ $(this).empty() });
          listings.gallery.startLoadingThumbImage(index, function(){setTimeout(function(){ listings.gallery.loadThumbnail(index) }, 500)});
        });
      },
      printThumbnail: function(index, callback){
        $('#gallery-thumbnails ul').append('<li class="gallery-thumb" index="'+index+'">'+
          '<div class="spin-wrap"><div class="spinner">'+
            '<div class="cube1"></div>'+
            '<div class="cube2"></div>'+
          '</div></div>'+
          '<div class="image"></div>'+
        '</li>');
        if (callback) callback();
      },
    }
  }
  listings.init();
});