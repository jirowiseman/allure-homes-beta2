var selected = [0,0,0];
var savedInfo = false;

// function ajax(d){
// 	if(typeof d.query !== 'undefined'){
// 		var h = [];
// 		if (typeof d.data === 'undefined') h.headers = {query:d.query}; else h.headers = {query:d.query,data:d.data};
// 		if (typeof d.done === 'undefined') h.done = function(){}; else h.done = d.done;
// 		// if (typeof d.fail === 'undefined') h.fail = function(){}; else h.fail = d.fail;
// 		if (typeof d.error !== 'undefined') h.error = d.error;
// 		if (typeof d.before === 'undefined') h.before = function(){}; else h.before = d.before;
// 		$.post(ah_local.tp+'/_sellers/_ajax.php', h.headers, function(){h.before()},'json')
// 			.done(function(x){
// 				if (x.status == 'OK') h.done(x.data);
// 				else if (h.error) h.error(x.data);
// 				else ahtb.close(function(){ahtb.alert(x.data, 'Database Error')});
// 			})
// 			.fail(function(x){
// 				if (h.fail) h.fail(x);
// 				else ahtb.alert("Database failure, please retry.", {height: 150});
// 			});
// 	} else ahtb.alert('Query was undefined');
// }

/*
function setupAutocomplete(element) {
	element.autocomplete({
    source: cities,
    select: function(e, ui){ 
      if (ui.item.value != null) {
        element.attr('city_id', ui.item.id); 
        element.attr('city_name', ui.item.label); 
        cityName = ui.item.label;
        element.change();
      }
    }
  }).on('focus',function(){
    if ($(this).val() == 'ENTER CITY') {
      $(this).val(''); $(this).removeClass('inactive');
    }
  }).on('keypress', function(e){
    var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
    if(keynum == 13) {//13 is the enter char code
      e.preventDefault();
    }
    $(this).attr('city_id', 0);
    $(this).attr('city_name', '');	
  });
}

function reallyReserve() {
	testAgentMatchList.item.splice(0, testAgentMatchList.item.length); // remove any items
	var ele = $('#information #lifestyles select');
	ele.each(function() {
 		var which = parseInt($(this).attr('for'));
		var val = selected[which];
		if (val != 0) {
			var item = {};
	 		item.location = testCityId;
	 		item.locationStr = testCityName;
	 		item.specialty = amTagList[val].id;
	 		item.specialtyStr = amTagList[val].tag;
	 		testAgentMatchList.item[testAgentMatchList.item.length] = item;
	 		console.log("AM specialty is "+val+' for:'+which);
	 	}
 	})

 	if (testAgentMatchList.item.length == 0) {
 		ahtb.push();
	    window.setTimeout(function() {
 			ahtb.alert("Please pick at least one lifestyle.", 
 				{height: 150,
                 buttons: [
                      {text: 'OK', action: function() {
                        ahtb.pop();
                      }}]});
 		}, 150);
 		return;
 	}
 	else {
 		savedInfo = true;
 		ahtb.close();
 	}
}

function pickedCity(wasPushed, city_id, city) {
	$('#information #user-data .service_areas').attr('city_id', city_id); 
    $('#information #user-data .service_areas').attr('city_name', city); 
	$('#information #user-data .service_areas').val(city);
	testCityName = city;
	testCityId = city_id;
	if (wasPushed)
		ahtb.pop();

	window.setTimeout(function() {
		reallyReserve();
	}, 350);
}

function getCityId(id, city) {
	$.post(ah_local.tp+'/_pages/ajax-quiz.php',
        { query:'find-city',
          data: { city: city,
          		  distance: 0 }
        },
        function(){}, 'JSON')
		.done(function(d){
			if (d &&
                d.data &&
                d.data.city &&
                (typeof d.data.city == 'object' ||
                d.data.city.length) ){
                if (d.status == 'OK' &&
                    (typeof d.data.city == 'object' || d.data.city.length) ) {							                        
                    console.log(JSON.stringify(d.data));

                	if (d.data.allState == 'true' || d.data.allState == true || d.data.allState == 1) {// doing statewide
                        ahtb.alert("Please enter a city name along with the state.", {height: 150});
                        return;
                    }

                    var len = length(d.data.city); //(typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
                	// var len = (typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
					if (len > 1) {
						if (d.data.allState == 'false' || d.data.allState == false) {
	                        var h = '<p>Please pick one of the cities:</p>' +
	                                  '<ul class="cities">';
	                        for(var i in d.data.city) {
	                        	var location = d.data.city[i].city+', '+d.data.city[i].state;
	                          	h += '<li><a href="javascript:pickedCity('+1+','+d.data.city[i].id+",'"+location+"'"+');" >'+location+'</a></li>';
	                        }
	                        h += '</ul>';
	                        ahtb.push();
	                        window.setTimeout(function() {
	                        	ahtb.open(
		                          { title: 'Cities List',
		                            width: 380,
		                            height: (150 + (len * 25)) < 680 ? (150 + (len * 25)) : 680,
		                            html: h,
		                            buttons: [
		                                  {text: 'Cancel', action: function() {
		                                    ahtb.pop();
		                                  }}],
		                            opened: function() {
		                            }
		                          })
	                        }, 150);
	                        
	                    }
	                    else {
	                    	ahtb.push();
	                    	window.setTimeout(function() {
	                    		ahtb.alert("Only one city/state combination accepted.", 
	                    			{height: 150,
	                    			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		ahtb.pop();
			 						 	}}]});
	                    	}, 150);
                          	
                        }
                    } // only one!
                    else {
                    	pickedCity(0, d.data.city[0].id, d.data.city[0].city+', '+d.data.city[0].state);
                    }
                }
                else {
                	ahtb.push();
                	window.setTimeout(function() {
                		ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                  			{title: 'City Match', 
	                  			 height:180, 
	                  			 width:600,
	                  			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		ahtb.pop();
			 						 	}}]});
                	}, 150);
                }
            }
            else {
            	ahtb.push();
            	window.setTimeout(function() {
	                ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                  			{title: 'City Match', 
	                  			 height:180, 
	                  			 width:600,
	                  			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		ahtb.pop();
			 						 	}}]});
	            }, 150);
            }
        })
        .fail(function(d){
        	ahtb.push();
            window.setTimeout(function() {
	        	ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                  			{title: 'City Match', 
	                  			 height:180, 
	                  			 width:600,
	                  			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		ahtb.pop();
			 						 	}}]});
	        }, 150);
        });
}


function agentMatchDetail() {
	var agent_match_detail = 
		'<div id="agent-reservation">' +
			'<div id="left">' +
				'<span id="title1">Be A Luxury Lifestyled Agent</br></span>' +
				'<span id="title2">Act Now, Exclusively Available to only 3 Agents Per Lifestyle</br></span>' +
				'<span id="title3">Dominate Your Areas of Expertise By Being An Exclusive Agent in Your Area.</br></span>' +	
				'<div id="main-graphics">' +
					'<div class="step1">' +
						'<img src="'+ah_local.tp+'/_img/page-agent-reservation/step1.png" />' +
						'<div class="steptext">' +
							'<span class="number">1</span>' +
							'<span class="title">Stand out from your peers</span>' +
							'<p class="sub">Tell us what lifestyles you&#39;re an expert in</p>' +
						'</div>' +
					'</div>' +
					'<div class="step2">' +
						'<img src="'+ah_local.tp+'/_img/page-agent-reservation/step2.png" />' +
						'<div class="steptext">' +
							'<span class="number">2</span>' +
							'<span class="title">Work with your dream clients</span>' +
							'<p class="sub">We match you to luxury buyers looking for homes with those lifestyles</p>' +
						'</div>' +
					'</div>' +
					'<div class="step3">' +
						'<img src="'+ah_local.tp+'/_img/page-agent-reservation/step3.png" />' +
						'<div class="steptext">' +
							'<span class="number">3</span>' +
							'<span class="title">Stand out from your peers</span>' +
							'<p class="sub">You represent buyers on homes that are a miniumum of $800K</p>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>' +
			'<span class="toptext"><span style="color:#333;margin-right:10px;font-size:.9em">Regularly</span><span style="color:#2289df;font-size:1.6em">$149.99/</span><span style="color:#2289df;font-size:.8em">Month</span></span>' +
			'<div id="information">' +	
				'<div id="message">' +	
					'<span id="line1">GET IN NOW!</span>' +	
					'<span id="line2">Early Adopter Price</span>' +	
					'<span id="line3">$99 / Month</span>' +	
					'<span id="line4">No payment required at this time</span>' +	
				'</div>' +				
				'<table id="user-data">' +
					'<tbody>' +
						'<tr>' +
							'<td><label>City</label></td><td><input type="text" class="service_areas" placeholder="City" value="'+service_area+'" /></td>' +
						'</tr>' +
					'</tbody>' +
				'</table>' +
				'<div id="lifestyles">' +
						'<select class="field_expertise" style="margin:20px auto 7px" for="0" >';
						var h = '';
						var index = meta && meta.item.length ? meta.item[0].specialty : -1;
						for(var id in amTagList)
							h += '<option value="'+id+'" '+(index != -1 && index == amTagList[id].id ? 'selected' : '')+' >'+amTagList[id].tag+'</option>';
					h+= '</select>';
		agent_match_detail += h;
  agent_match_detail += '<select class="field_expertise" for="1" >';
  						h = '';
  						index = meta && meta.item.length > 1 ? meta.item[1].specialty : -1;
						for(var id in amTagList)
							h += '<option value="'+id+'" '+(index != -1 && index == amTagList[id].id ? 'selected' : '')+' >'+amTagList[id].tag+'</option>';
					h+= '</select>';
		agent_match_detail += h;
  agent_match_detail += '<select class="field_expertise" style="margin:7px auto 20px" for="2" >';
  						h = '';
  						index = meta && meta.item.length > 2 ? meta.item[2].specialty : -1;
						for(var id in amTagList)
							h += '<option value="'+id+'" '+(index != -1 && index == amTagList[id].id ? 'selected' : '')+' >'+amTagList[id].tag+'</option>';
					h+= '</select>';
		agent_match_detail += h;
agent_match_detail += 
				'</div>' +		
			'</div>' +
			'<div>' +
				'<button id="reserve">Save Information</button>&nbsp;<button id="done">Done</button>' +
			'</div>' +
		'</div>';

	ahtb.open({
		html: agent_match_detail,
		width: 1200,
		height: 625,
		hideSubmit: true,
		hideTitle: true,
		opened: function() {
			setupAutocomplete($('#information #user-data .service_areas'));

			$('#information #user-data .service_areas').attr('city_id', testCityId); 
    		$('#information #user-data .service_areas').attr('city_name', testCityName); 
			$('#information #user-data .service_areas').val(testCityName);

			$('#information #lifestyles select').on('change', function() {
				var item = parseInt($(this).attr('for'));
				var val = parseInt($(this,' option:selected').val());
				console.log("option for "+item+", val: "+val);
				selected[item] = val;				
			});

			$('#information #lifestyles select[for="0"] option[value="'+selected[0]+'"]').prop('selected', true);
			$('#information #lifestyles select[for="1"] option[value="'+selected[1]+'"]').prop('selected', true);
			$('#information #lifestyles select[for="2"] option[value="'+selected[2]+'"]').prop('selected', true);

			$('#agent-reservation #done').on('click', function(e) {
				e.preventDefault();
				testCityId = $('#information #user-data .service_areas').attr('city_id');
				testCityName = $('#information #user-data .service_areas').attr('city_name');
				if (savedInfo == false &&
					testCityId != 0 &&
					testCityName.length &&
					(selected[0] || selected[1] || selected[2])) {
					ahtb.alert("Do you want to save your information?",
								{height: 150,
								 width: 450,
								 buttons:[
		 						 	{text:'OK', action: function() {
		 						 		reallyReserve();
		 						 		ahtb.close();
		 						 	}},
		 						 	{text:'No Thanks', action: function() {
		 						 		ahtb.close();
		 						 	}}]});
					return;

				}
				ahtb.close();
			})

			$('#agent-reservation #reserve').on('click', function(e) {
				var city = $('#information #user-data .service_areas').attr('city_id');
				testCityName = $('#information #user-data .service_areas').attr('city_name');
			 	testCityId = city? parseInt(city) : 0;
			 	if (!testCityId ||
			 		 testCityName.length == 0) {
			 		testCityName = $('#information #user-data .service_areas').val();
			 		if (testCityName.length == 0) {
			 			msg = "Please pick your city.";
			 			ahtb.push();
			 			window.setTimeout(function() {
			 				ahtb.open({html: msg, 
			 						   width: 400,
			 						   height: 120,
			 						   buttons:[
			 						 	{text:'OK', action: function() {
			 						 		ahtb.pop();
			 						 	}}]});
			 			}, 250);
						return;
			 		}
			 		else {
			 			getCityId(testCityId, testCityName);
			 			return;
			 		}
			 	}
			 	reallyReserve();		 	
			});
		}
	})
}*/

jQuery(document).ready(function($){
	console.log("starting agent-purchase-portal page");
	window.history.pushState('pure','Title',ah_local.wp+'/'+thisPage);
	
	var msg = 	'Lifestyledlistings.com wants you to <strong>capture leads and receive referral fees</strong> by using the PORTAL feature of our website.  ' +
				'This user friendly tool allows you to <strong>attract and manage interested buyers</strong>.  '+
				'Once a buyer enters the site through your PORTAL <strong>they will remain tied to you*.' +
				'</strong>  The benefit for you is you get a highly developed, lifestyle site that captures leads whom will remain with you <strong>' +
				'whether you refer them out or close an escrow.</strong><br/><br/> Share your link through your social medias, email contacts, advertisements, '+
				'add it to your business cards and email signatures. Spread the word and <strong>keep your business growing faster than ever!</strong>  ' +
				'Call us, and our specialists can help you work your social media sites to show your portal URL to your clients!' +
				'<span style="font-size:.9em;width:90%;text-align:center;display:block;margin-top:35px">*Disclaimer: Lifestyled Listings is legally required ' +
				'to give users the ability to detach from your PORTAL if they choose to.</span>';

	var state = -1;
	var password = '';
	var verify = '';
	var bre = '';
	var invitationCode = coupon;
	// var availableStates = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming', 'American Samoa', 'Guam', 'Northern Mariana Islands', 'Puerto Rico', 'Virgin Islands', 'Federated States of Micronesia', 'Marshall Islands', 'Palau', 'Armed Forces Africa \ Canada \ Europe \ Middle East', 'Armed Forces America (Except Canada)', 'Armed Forces Pacific'];

	$('input#states').autocomplete({
		source: allStates,
	    select: function(e, ui){ 
	      if (ui.item.value != null) {
	        $('input#states').attr('state_id', ui.item.id); 
	        $('input#states').attr('state_name', ui.item.label); 
	        // state = ui.item.id;
	        $('input#states').change();
	      }
	    }
	  }).on('focus',function(){
	    if ($(this).val() == 'ENTER STATE') {
	      $(this).val(''); $(this).removeClass('inactive');
	    }
	  }).on('keypress', function(e){
	    var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
	    if(keynum == 13) {//13 is the enter char code
	      e.preventDefault();
	    }
	    $(this).attr('state_id', '');
	    $(this).attr('state_name', '');	
	  });
	

	if (seller.portal.length)
		testPortal = seller.portal;
	//$('#message').html(msg);
	$('[name=mobile], [name=phone]').mask('(999) 000-0000');
	$('[name=email]').val(sellerEmail);
	// $('[name=login-id]').val(sellerEmail);

	$('#define-portal #portal').val(testPortal);
	$('#message #marker').css('color', 'lightgreen');
	$('#message #marker').hide();
	$('#message #base-url').text('LifestyledListings.com/');
	$('#message #portal').keyup(function(e){
		e.preventDefault();
		$('#message #marker').hide();
	})

	$('#invitation_code').val(coupon);
	var price = 39 - discount;
	$('.maintext .pricebox').html("$"+price.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" Monthly"+(discount ? " with Code" : ''));
	// if (et != '0') {
	// 	$('#introDiscount').html("Special intro discount price of only $"+price.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"!");
	// 	$('#introDiscount').show();
	// 	$('#couponRow').hide();
	// }
	// else {
	// 	//$('#introDiscount').html("Use this coupon: "+coupon+" to get it for $"+price.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" today!");
	// 	//$('#introDiscount').show();
	// }

	$('#user-data #marker').hide();

	$('#invitation_code').keyup(function(e) {
    	e.preventDefault();
    	code = $(this).val();
    	if (code.length == 0) {
			var price = 39;
			var discount = 0;
			$('.maintext .pricebox').html("$"+price.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" Monthly"+(discount ? " with Code" : ''));
   			return;
    	}
    	ajax({
			query: 'check-coupon',
			data: {coupon: code},
			done: function(d) {
				d = d.data;
				var discount = parseInt(d);
				if (discount != InviteCodeMode.IC_ACCEPTED) {
					var price = 39 - discount;
					$('.whatisportal .right  p + p').html("And perhaps best of all - it's $"+discount+" off!");
					$('.bottom .sidetext .title').html('Why is it so affordable?');
					// $('.maintext .pricebox').html("$"+price.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" Monthly"+(discount ? " with Code" : ''));
				}
				else {
					$('.whatisportal .right  p + p').html("And perhaps best of all - it's free!");
					$('.bottom .sidetext .title').html('Why is it free?');
					// $('.maintext .pricebox').html("Gratis - Invitation");
				}
    		},
    		error: function(d) {
    			d = d.data;
    			var price = 39;
    			var discount = 0;
    			$('.maintext .pricebox').html("$"+price.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" Monthly"+(discount ? " with Code" : ''));
				if (typeof d == 'object' &&
					typeof d.msg != 'undefined') {
					ahtb.open({html: d.msg,
							  width: 450,
							  height: 110,
							  buttons:[{text:'OK', action:function() {
							  	ahtb.close();
							  }}]});
				}
				else {
					var msg = typeof d == 'string' ? d : "We're very sorry, had trouble finding the code in the system.";
					console.log("Failed: "+msg);
					// ahtb.alert(msg, {height: 150, width: 450});
				}
			}
		})
    })

	$('#verify').keyup(function(e) {
    	e.preventDefault();
    	verify = $(this).val();
    	var vlen = $(this).val().length;
    	var len = $('#password').val().length;
    	len || vlen ? $('#user-data #marker').show() : $('#user-data #marker').hide();
    	//$('#marker').attr('visibility', len ? 'visible' : 'hidden');
    	var hasCancel = $('#user-data #marker').hasClass('entypo-attention');
    	if ($(this).val() == $('#password').val()) {
    		if (hasCancel) {
    			$('#user-data #marker').removeClass('entypo-attention');
    			$('#user-data #marker').addClass('entypo-check');
    			$('#user-data #marker').css('color', 'lightgreen');
    		}
    	}
    	else if (!hasCancel) {
    			$('#user-data #marker').removeClass('entypo-check');
    			$('#user-data #marker').addClass('entypo-attention');
     			$('#user-data #marker').css('color', 'red');
   		}
    	
    })

    $('#password').keyup(function(e) {
    	e.preventDefault();
    	password = $(this).val();
    	var len = $(this).val().length;
    	var vLen = $('#verify').val().length;
    	len || vLen ? $('#user-data #marker').show() : $('#user-data #marker').hide();
    	//$('#marker').attr('visibility', len ? 'visible' : 'hidden');
    	var hasCancel = $('#user-data #marker').hasClass('entypo-attention');
    	if ($(this).val() == $('#verify').val()) {
    		if (hasCancel) {
    			$('#user-data #marker').removeClass('entypo-attention');
    			$('#user-data #marker').addClass('entypo-check');
    			$('#user-data #marker').css('color', 'lightgreen');
    		}
    	}
    	else {
    		if (!hasCancel) {
    			$('#user-data #marker').removeClass('entypo-check');
    			$('#user-data #marker').addClass('entypo-attention');
     			$('#user-data #marker').css('color', 'red');
   			}
    	}
    })


	$('button#check-it').on('click', function() {
		var ele = $(this).prev('#portal');
		var val = ele.val();
		if (val.length == 0) {
			ahtb.alert("Please enter a portal name.", 
						{height: 150,
						buttons: [{text: "OK", action: function() {
							ahtb.close();
						}}]});
			return;
		}
		console.log("testing portal:"+val);

		ajax({
			query: 'check-portal-name',
			data: val,
			done: function(d) {
				d = d.data;
    			$('#message #marker').show();
    			testPortal = d;
    			ele.val(d);
					var infodropped = $('#information').css('display') != 'none';
					if (!infodropped) {
						$('.top #information').slideToggle('fast');
						$('.top .more').hide();
						$('.top .less').show();
						//$('.top .hiddendiv').hide();
					}
					else 
						return;
					},
			error: function(d) {
				d = d.data;
				ahtb.alert("The portal name "+val+" is not available.</br>Please try a different name.", {height: 180, width: 450});
				ele.val('');
				$('#message #marker').hide();
			}
		});
	});
	// $('.portalinputmobile #check-it').on('click', function() {
	// 	var val = $('.portalinputmobile #portal').val();
	// 	if (val.length == 0) {
	// 		ahtb.alert("Please enter a portal name.", 
	// 					{height: 150,
	// 					buttons: [{text: "OK", action: function() {
	// 						ahtb.close();
	// 					}}]});
	// 		return;
	// 	}
	// 	ajax({
	// 		query: 'check-portal-name',
	// 		data: val,
	// 		done: function(d) {
 //    			$('#message #marker').show();
 //    			testPortal = d;
 //    			 $('.portalinputmobile #portal').val(d);
	// 				var infodropped = $('#information').css('display') != 'none';
	// 				if (!infodropped) {
	// 					$('.top #information').slideToggle('fast');
	// 					$('.top .more').hide();
	// 					$('.top .less').show();
	// 					//$('.top .hiddendiv').hide();
	// 				}
	// 				else 
	// 					return;
	// 				},
	// 		error: function(d) {
	// 			ahtb.alert("The portal name "+val+" is not available.</br>Please try a different name.", {height: 180, width: 450});
	// 			$('.portalinputmobile #portal').val('');
	// 			$('#message #marker').hide();
	// 		}
	// 	});
	// });

	$('#information .reserveportal').on('click', function() {
		agentMatchDetail();
	})

	// sets the selected state
	// using <option selected="selected"> doesn't work!!!
	$('#states').val(seller.state);
	state = seller.state.length ? seller.state : -1;

	var val = $('select[name=States] option:selected').val();
	console.log("states is "+(val));

	$('select[name=States]').on('change', function() {
		var val = $(this,'option:selected').val();
		console.log("states is "+(val));
		state = val;
	})

	function completeRegistration() {
		if (testPortal.length == 0) {
			ahtb.alert('Please enter and check your desired portal name to purchase it.', {height: 170});
			return;
		}

		var first = $('#user-data #first_name').val();
		var last = $('#user-data #last_name').val();
		var phone = $('#user-data #phone').val().replace(/[^0-9]/g, '');
		var email = $('#user-data #email').val();
		var coupon = $('#user-data #invitation_code').val();
		var bre = $('#user-data #breID').val();
		var login = $('#user-data #login-id').val();
		var msg = '';
		if (first.length == 0)
			msg += "first name";
		if (last.length == 0)
			msg += (msg.length ? ", ": "") + "last name";
		if (phone.length == 0)
			msg += (msg.length ? ", ": "") + "phone";
		if (email.length == 0)
			msg += (msg.length ? " and ": "") + "email";

		if (msg.length) {
			msg = "Please enter "+msg+" so we can contact you.";
			ahtb.alert(msg, {height: 170});
			return;
		}

		if (!validateEmail(email)) {
			msg = "Email entered does not appear correct.";
			ahtb.alert(msg, {height: 170});
			return;
		}

		if (password.length < 8) {
			msg = "Please use a password with at least 8 characters";
			ahtb.alert(msg, {height: 170});
			return;
		}

		if (password != verify) {
			msg = "Password and verification does not match"
			ahtb.alert(msg, {height: 170});
			return;
		}

		if (bre.length == 0 || bre.length < 5) {
			msg = "Please enter a BRE number"
			ahtb.alert(msg, {height: 170});
			return;
		}

		var test = $('input#states').val();
		var state_id = $('input#states').attr('state_id');
		state = -1;
		if (test.length) {
			if (state_id.length &&
				test.toUpperCase() ==  state_id) 
				state = state_id;
			else {
				test = test.toLowerCase();
				for(var i in allStates) {
					if (allStates[i].value.toLowerCase() == test) {
						state = allStates[i].id;
						break;
					}
				}
			}
		}
	
		if (state == -1 || state == "-1") {
			msg = "Please select the state the BRE number is registered"
			ahtb.alert(msg, {height: 170});
			return;
		}

		if (typeof login == 'undefined' ||
			login.length == 0)
			login = email;
		
		ahtb.open({	html:"Registering...", 
				   	width: 450, 
				   	height: 110,
					hideSubmit: true});

		var optIn = $('input#opt-in ').prop('checked');

		// try{
		//     __adroll.record_user({"adroll_segments": "c0156c72"})    
		// } catch( err ) {
		// 	if (typeof err == 'object' && typeof err != null && typeof err.message != 'undefined')
		// 		console.log("Adroll exception - "+err.message);
		// 	else
		// 		console.log("Adroll exception of some sort");
		// 	// ahtb.alert("AdRoll exception!", {height: 150});
		// 	// return;
		// }

		ajax({
			query: 'register-pre-portal',
			data: {id: seller.id,
				   // what: SellerFlags.SELLER_IS_PREMIUM_LEVEL_1,
				   // lifestyles: testAgentMatchList,
				   portal: testPortal,
				   email: email,
				   first_name: first,
				   last_name: last,
				   phone: phone,
				   password: password,
				   login: login,
				   realtor_id: bre,
				   state: state,
				   coupon: coupon,
				   et: et,
				   key: key,
				   special: promo.length ? RegistrationMode.REGISTRATION_SPECIAL_CAMPAIGN_PORTAL : RegistrationMode.REGISTRATION_SPECIAL_NONE,
				   optIn: optIn
			},
			done: function(d) {
				d = d.data;
				$('button#buyIt').prop('disabled', false);
				if (typeof d.url != 'undefined' &&
					typeof d.msg != 'undefined') {
					console.log(d.msg);
					if (d.msg.indexOf('check your email') == -1)
						window.location = d.url;
					else
						ahtb.open({html: d.msg,
								  width: 600,
								  height: 350,
								  buttons:[{text:'OK', action:function() {
								  	window.location = d.url;
								  }}],
								  closed: function() {
								  	window.location = d.url;
								  }
								});
					return;
				}
				ahtb.alert("Creating Cart...", 
						   {width: 450, height: 140,
							hideSubmit: true});
				ajax({
					query: 'purchase-portal-post-registration',
					data: {id: d.id,
						   // what: SellerFlags.SELLER_IS_PREMIUM_LEVEL_1,
						   // lifestyles: testAgentMatchList,
						   portal: testPortal,
						   email: email,
						   first_name: first,
						   last_name: last,
						   phone: phone,
						   password: password,
						   login: login,
						   realtor_id: bre,
						   state: state,
						   coupon: d.coupon,
						   et: et,
						   key: key },
					done: function(d) {
						d = d.data;
						console.log("register-purchase-portal - "+d.status);
						if (d.status == "Cart made") {
							seller.am_order = d.order;
							window.location = ah_local.wp+'/cart';		
						}
						else if (d.status == "Coupon error") {
							seller.am_order = d.order;
							ahtb.open({html: "Coupon had "+d.errorCount+" errors:"+d.error,
									   width: 450,
									   heigth: 110 + (d.errorCount *25),
									   buttons: [{text:'OK', action: function() {
									   		window.location = ah_local.wp+'/cart';	
									   }}],
									   closed: function(){
									   		window.location = ah_local.wp+'/cart';	
									   }
									});
						}
						else 
							ahtb.alert(d.status);	
					},		
					error: function(d) {
						d = d.data;
						$('div#agent-portal-reservation').animate({opacity: 1},{duration: 500});
						var msg = typeof d == 'string' ? d : "We're very sorry, had trouble registering you into the system.";
						console.log("Failed: "+msg);
						ahtb.alert(msg, {height: 150, width: 450});
					}	
				})	
    		},
			error: function(d) {
				d = d.data;
				$('div#agent-portal-reservation').animate({opacity: 1},{duration: 500});
				$('button#buyIt').prop('disabled', false);
				if (typeof d == 'object' &&
					typeof d.url != 'undefined' &&
					typeof d.msg != 'undefined') {
					ahtb.open({html: d.msg,
							  width: 450,
							  height: 110,
							  buttons:[{text:'OK', action:function() {
							  	window.location = d.url;
							  }}],
							  closed: function() {
							  	window.location = d.url;
							  }
							});
				}
				else {
					var msg = typeof d == 'string' ? d : "We're very sorry, had trouble registering you into the system.";
					console.log("Failed: "+msg);
					ahtb.alert(msg, {height: 150, width: 450});
				}
			},
			fail: function(d) {
				d = d.data;
				$('div#agent-portal-reservation').animate({opacity: 1},{duration: 500});
				$('button#buyIt').prop('disabled', false);
				var msg = typeof d == 'string' ? d : "We're very sorry, there was a system failure.";
				if (typeof d == 'object') {
					if (typeof d.msg != 'undefined')
						msg += "  "+d.msg;
					else if (typeof d.responseText != 'undefined' &&
							d.responseText.length)
						msg += "  "+d.responseText;
					else if (typeof d.statusText != 'undefined' &&
							d.statusText.length &&
							d.statusText != 'error')
						msg += "  "+d.statusText;
					else if (typeof d.status != 'undefined' &&
							 typeof d.status == 'string' &&
							 d.status.length)
						msg += "  "+d.status;
					
					ahtb.open({html: msg,
							  width: 450,
							  height: 110 + (Math.floor(msg.length / 60) * 25),
							  buttons:[{text:'OK', action:function() {
							  	ahtb.close();
							  }}]});
				}
				else {
					console.log("Failed: "+msg);
					ahtb.alert(msg, {height: 150, width: 450});
				}
			}
		});
	}

	function goGetIt() {
		if (testPortal.length == 0) {
			ahtb.alert('Please enter and check your desired portal name to purchase it.', {height: 170});
			return;
		}

		var first = $('#user-data #first_name').val();
		var last = $('#user-data #last_name').val();
		var phone = $('#user-data #phone').val(); //.replace(/[^0-9]/g, '');
		var email = $('#user-data #email').val();
		var coupon = $('#user-data #invitation_code').val();
		var bre = $('#user-data #breID').val();
		var login = $('#user-data #login-id').val();
		var msg = '';
		if (first.length == 0)
			msg += "first name";
		if (last.length == 0)
			msg += (msg.length ? ", ": "") + "last name";
		if (phone.length == 0)
			msg += (msg.length ? ", ": "") + "phone";
		if (email.length == 0)
			msg += (msg.length ? " and ": "") + "email";

		if (msg.length) {
			msg = "Please enter "+msg+" so we can contact you.";
			ahtb.alert(msg, {height: 170});
			return;
		}

		if (!validateEmail(email)) {
			msg = "Email entered does not appear correct.";
			ahtb.alert(msg, {height: 170});
			return;
		}

		if (password.length < 8) {
			msg = "Please use a password with at least 8 characters";
			ahtb.alert(msg, {height: 170});
			return;
		}

		if (password != verify) {
			msg = "Password and verification does not match"
			ahtb.alert(msg, {height: 170});
			return;
		}

		if (bre.length == 0 || bre.length < 5) {
			msg = "Please enter a RE license number"
			ahtb.alert(msg, {height: 170});
			return;
		}

		var test = $('input#states').val();
		var state_id = $('input#states').attr('state_id');
		state = -1;
		if (test.length) {
			if (state_id.length &&
				test.toUpperCase() ==  state_id) 
				state = state_id;
			else {
				test = test.toLowerCase();
				for(var i in allStates) {
					if (allStates[i].value.toLowerCase() == test) {
						state = allStates[i].id;
						break;
					}
				}
			}
		}
	
		if (state == -1 || state == "-1") {
			msg = "Please select the state the BRE number is registered"
			ahtb.alert(msg, {height: 170});
			return;
		}

		if (typeof login == 'undefined' ||
			login.length == 0)
			login = email;
		
		// ahtb.alert("Registering...", 
		// 		   {width: 450, height: 140,
		// 			hideSubmit: true});

		var optIn = $('input#opt-in ').prop('checked');

		$('button#buyIt').prop('disabled', true);

		var parent = $('div#agent-portal-reservation');
		var button = $('button#buyIt');
		var emailEle = $('#user-data #email');
		var firstNameEle = $('#user-data #first_name');
		var lastNameEle = $('#user-data #last_name');
		var phoneEle = $('#user-data #phone');
		checkSellerInDB(first, last, email, state, phone, login,
						completeRegistration, // successful callback
						goGetIt, // retry callback
						parent, // div to make opaque
						button, // button to enable disablity (this one)
						emailEle, // email element
						firstNameEle, // first_name element
						lastNameEle, // last_name element
						phoneEle); // phone element
	}

	$('button#buyIt').on('click', function() {
		goGetIt();
	})
})