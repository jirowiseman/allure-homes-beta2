var waitedForFirstDirective = false;
var tempSignInScrollTop = 0;

function waitForDirective() {
  // $('#quiz button').prop('disabled', gettingDirective);
  if (gettingDirective)
    window.setTimeout(function() {
      waitForDirective();
      // console.log("wating for directive");
    }, 1000);
  else
    $('a.start').prop('disabled', gettingDirective);
}

jQuery(document).ready(function($){
  var cityName = '';
  // console.log("home sees myPortalRow:"+myPortalRow);

  if (!waitedForFirstDirective) {
    $('a.start').prop('disabled', gettingDirective);
    window.setTimeout(function() {
      waitForDirective();
    }, 500);
    waitedForFirstDirective = true;
  }

  portal_landing = new function() {
    this.fromFBLogin = false;

    this.init = function() {
      console.log("entered init");
      // if (! (wpId == 8 || 
      //       wpId == 3 ||
      //       wpId == 4 ||
      //       wpId == 5 ||
      //       wpId == 80) )
      window.history.pushState('pure','Title',ah_local.wp+'/'+thisPage);

      deleteCookie('Landing-UserName');
      deleteCookie('Landing-UserEmail');

      $('button.agent-contact').on('click', function(e) {
        console.log("Contact Me clicked");

        var agentId = ah_local.sellerID;
        console.log("Contact button for ", agentId);
        var mode = FeedbackMode.MESSAGE_EMAIL_AGENT_VIA_PORTAL;

        ahfeedback.setCallback(portal_landing.sentMessage)
        ahfeedback.openModal(mode, agentId, -1);

        e.preventDefault();
        return false;
      })
    }

    this.sentMessage = function() {
      if (fbLogInData.first_name.length &&
          fbLogInData.last_name.length &&
          fbLogInData.email.length) {
          portal_user_register.userName = fbLogInData.first_name +' '+ fbLogInData.last_name;
          portal_user_register.userEmail = fbLogInData.email;
          $('div#userdata input.name').val(portal_user_register.userName);
          $('div#userdata input.email').val(portal_user_register.userEmail);
          setCookie('Landing-UserName', portal_user_register.userName, 1);
          setCookie('Landing-UserEmail', portal_user_register.userEmail, 1);
      }
    }

    this.actionFBInitialized = function() {
      $('#fbDiv').show();
    }

    this.actionFBLoggedIn = function() {
      portal_user_register.userName = fbLogInData.first_name +' '+ fbLogInData.last_name;
      portal_user_register.userEmail = fbLogInData.email;
      portal_user_register.userPhoto = fbLogInData.photo.length ? fbLogInData.photo : '';
      portal_user_register.fbId = fbLogInData.id;
      nameMode = PortalLandingElementMode.PORTAL_LANDING_MUST_HAVE;
      phoneMode = PortalLandingElementMode.PORTAL_LANDING_OPTIONAL;
      emailMode = PortalLandingElementMode.PORTAL_LANDING_MUST_HAVE;
      portal_landing.fromFBLogin = true;
      portal_user_register.reallyStart();
    }

    this.actionPortalUserRegistered = function(d) {
      console.log("successfully did new-portal-user, redirectPage:"+redirectPage);
      var redirection = '';
      var page = 'Go to '+redirectPage;
      var msg = 'Lifestyled Listings has detected you were <span style="font-weight:600;">previously logged in.</span>';
      if (redirectPage.length == 0) {
        if (typeof d == 'undefined' || typeof d.data.quiz_id == 'undefined') {
          redirection = ah_local.wp+"/quiz/#sq=0";
          page = 'Start search';
        }
        else {
          redirection = ah_local.wp+"/quiz-results/L-"+d.data.quiz_id;
          page = 'Go to my last search';
        }
      }
      else
        redirection = ah_local.wp+"/"+redirectPage;

      $('div#overlay-bg').fadeOut(!portal_landing.fromFBLogin ? 20000 : 500);

      if (!portal_landing.fromFBLogin)
        window.location = redirection;
      else {
        $('.logged-in-already-with-fb button#redirect').html(page);
        $('.logged-in-already-with-fb button#redirect').on('click', function() {
          window.location = redirection;
        });
        $('.logged-in-already-with-fb button#cancel').on('click', function() {
          // window.location = ah_local.wp;
          $('.logged-in-already-with-fb').hide();
        });
        $('.logged-in-already-with-fb .redirect-message span').html(msg);
        $('.logged-in-already-with-fb').show();
      }
        // ahtb.open({html:'<div>'+
        //                   '<span class="entypo-link"></span>'+
        //                   '<p>'+msg+'</p>',
        //            width: 450,
        //            height: 180,
        //            buttons:[
        //            {text: page, action: function() {
        //               window.location = redirection;
        //            }},
        //            {text: 'Cancel', action: function() {
        //               window.location = ah_local.wp;
        //            }} ],
        //            closed: function() {
        //             window.location = redirection;
        //            }});
    }

    this.login = function() {
      /*var reg_html_login =
        '<div class="close">x</div>'+
        '<div class="signin-header"><span style="font-weight:400;">Welcome back!</span> Please sign in</div>' +
				'<div id="fbDiv" style="display: none;">' +
						'<div id="fb-root"></div>' +
						'<fb:login-button scope="public_profile,email" onlogin="checkLoginState();" data-size="xlarge">Log in with Facebook</fb:login-button>' +
						'<a href="#" class="fb-fakebutton" onclick="fbLogin();">' +
							'<img src="'+ah_local.tp+'/_img/social_media/facebook-icon.png" id="fb_login"></img>Continue with facebook' +
						'</a>' +
						'<div class="or-wrapper">' +
							'<span class="line1"></span>' +
							'<span class="line2"></span>' +
							'<span id="or">OR</span>' +
						'</div>' +
				'</div>' +
        '<div class="signin-wrapper">' +
          '<form name="login-form" id="login-form" action="http://mobile.lifestyledlistings.com/wp-login.php" method="post"><p class="login-username">' +
          '<input type="text" name="log" id="user_login" class="input" value="" placeholder="Username">' +
          '<input type="password" name="pwd" id="user_pass" class="input" value="" placeholder="Password">' +
          '<input type="submit" name="wp-submit" id="wp-submit" class="button-primary" value="Log In">' +
          '<input type="hidden" name="redirect_to" value="http://mobile.lifestyledlistings.com/quiz/#sq=0">' +
          '<p class="forgot-password"><a href="'+lostPasswordUrl+'" title="Forgot your password?">Forgot your password?</a></p>' +
        '</div>';*/
        //$('.global-signin-popup .signin-content').html(reg_html_login);
        $('.global-signin-popup').show();
        tempSignInScrollTop = $(window).scrollTop();
        $('body').css('overflow','hidden');
				if (isMobile)
					$('body').css('position','fixed');
        $('.global-signin-popup .signin-content .close').on('click', function() {
          closeLoginForm();
        });
    }
    function closeLoginForm() {
      $('.global-signin-popup').hide();
      $('body').css('overflow','');
			if (isMobile)
					$('body').css('position','relative');
      $(window).scrollTop(tempSignInScrollTop);
    }
    $(document).keyup(function(e) {
      if (e.keyCode == 27) { // escape key
        if($('.global-signin-popup').css('display') == 'block'){
          closeLoginForm();
        }
      }
    });
    $(document).on('click', function() {
      if($('.global-signin-popup').css('display') == 'block'){
        closeLoginForm();
      }
    });
    $('.global-signin-popup .signin-content').click(function(event){
      event.stopPropagation();
    });
  }

  portal_landing.init();
  controller = portal_landing;
})