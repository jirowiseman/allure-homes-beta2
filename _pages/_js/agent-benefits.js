jQuery(document).ready(function($){
	console.log("entered agent-benefits.js");
	benefits = new function() {
		//$('#agent-match #learn').hide();
		// $('#portal #learn').hide();

		// $('.videoDiv').hide();

		$('section#portal').hide();

		$('#register').on('click', function() {
			window.location = ah_local.wp+'/register';
		})

		$('#agent-match #claim').on('click', function() {
			window.location = ah_local.wp+'/agent-purchase-lifestyle';
		})

		$('#portal #claim').on('click', function() {
			window.location = ah_local.wp+'/agent-purchase-portal';
		})
	}

	if (startOption == 'unsubscribe') {
	    var msg = "Sorry to see you unsubscribe out of our mailing system.<br/>Please continue to use our system and improve your listings.";
	    ahtb.alert(msg, {width: 550, height: 180});
	  }
})