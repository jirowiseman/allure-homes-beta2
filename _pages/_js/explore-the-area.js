var explore;

function initialize(){explore.init();}
function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&callback=initialize';
  document.body.appendChild(script);
}
window.onload = loadScript();

jQuery(document).ready(function($){
	explore = new function(){
		this.init = function(){
			this.visibleCategories = {};
			this.latlng = new google.maps.LatLng(explore_info.lat, explore_info.lng);
			this.latlngOffset = new google.maps.LatLng(explore_info.lat, explore_info.lng+.12);
			this.map = new google.maps.Map(document.getElementById('map-canvas'), {
				center: explore.latlngOffset,
				zoom: 11,
				scrollwheel: false,
				mapTypeControl: true,
		    mapTypeControlOptions: {
		        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
		        position: google.maps.ControlPosition.BOTTOM_CENTER
		    },
			});
			this.homeMarker = new google.maps.Marker({position: explore.latlng, map: explore.map, icon: ah_local.tp+'/_img/page-explore-the-area/map_icons/home.png'});
			this.parse.markers(explore_info.places, explore.parse.list);
			$('ul#places-menu li').on('click',function(){ explore.toggleCategory($(this).attr('category')) });
			this.toggleCategory('attractions');
			$('header section.banner li').on('click',function(){
				if ( $(this).hasClass('intro') ){
					$('.content-wrap > section').each(function(){
						if ($(this).hasClass('intro')) $(this).animate({left: '0%'},{duration: 500, queue: false});
						else $(this).animate({left: '100%'},{duration: 500, queue: false});
					});
				} else if ( $(this).hasClass('map') ){
					$('.content-wrap > section').each(function(){
						if ($(this).hasClass('map')) $(this).animate({left: '0%'},{duration: 500, queue: false});
						else if ($(this).hasClass('area-guides')) $(this).animate({left: '100%'},{duration: 500, queue: false});
						else $(this).animate({left: '-100%'},{duration: 500, queue: false});
					});
				} else if ( $(this).hasClass('area-guides') ){
					$('.content-wrap > section').each(function(){
						if (!$(this).hasClass('area-guides')) $(this).animate({left: '-100%'},{duration: 500, queue: false});
						else $(this).animate({left: '0%'},{duration: 500, queue: false});
					});
				}
			});
		}
		this.parse = new function(){
			this.markers = function(d, callback){
				explore.markers = {};
				for (var i in d){
					var category = i.toLowerCase();
					explore.markers[category] = {};
					for (var j in d[i]){
						var point = d[i][j];
						explore.markers[category][j] = {
							info: point,
							infowindow: new google.maps.InfoWindow({content:
								'<div class="info-content">'+
									'<h1>'+point.name+'</h1>'+
									'<div id="bodyContent">'+
										'<p>'+point.address+'</p>'+
									'</div>'+
								'</div>'
							}),
							marker: new google.maps.Marker({
								icon: ah_local.tp+'/_img/page-explore-the-area/map_icons/'+category+'_sm.png',
								position: new google.maps.LatLng(point.lat, point.lng),
								visible: false,
								id: parseInt(j),
								category: category
							})
						};
						google.maps.event.addListener(explore.markers[category][j].marker,'click',function(){
							for (var i in explore.markers) for (var j in explore.markers[i])
								if (j != this.id) explore.markers[i][j].infowindow.close();
							marker = explore.markers[this.category][this.id];
							marker.infowindow.open(explore.map, marker.marker);
						});
					}
				}
				if (callback && $.isFunction(callback)) callback();
			}
			this.list = function(){
				if (typeof explore.markers == 'undefined' || explore.markers == null || explore.markers.length < 1)
					console.log('no places to list');
				else for (var i in explore.markers){
					var points = explore.markers[i];
					var category = i;
					$('ul#places-list > li').hide();
					$('ul#places-list .'+category+' .count').html( Object.keys(points).length );


					var sorted = [];
					for (var j in explore.markers[category]) sorted.push({id:j, distance:explore.markers[category][j].info.distance});
					sorted.sort(function(a, b){ return a.distance - b.distance; });

					for (var j in sorted){
						var point = explore.markers[category][sorted[j].id].info;
						h = '<li class="place" place-id="'+sorted[j].id+'" category="'+category+'">'+
							'<div class="image"><img src="'+ah_local.tp+'/_img/page-explore-the-area/map_icons/'+category+'.png" /></div>'+
							'<span class="name">'+point.name.replace(/\//g,'')+'</span>'+
							'<span class="address">'+point.address+'</span>'+
							'<span class="distance">'+point.distance+' miles</span>'+
						'</li>';
						$('ul#places-list .'+category+' ul').append(h);
					}
					$('ul#places-list li.place').off('click').on('click',function(){
						var thisMarker = explore.markers[ $(this).attr('category') ][ $(this).attr('place-id') ];
						for (var i in explore.markers) for (var j in explore.markers[i])
								if (j != thisMarker.marker.id) explore.markers[i][j].infowindow.close();
						thisMarker.infowindow.open(explore.map, thisMarker.marker);
						explore.map.panTo(thisMarker.marker.getPosition());
					})
					$('ul#places-list li.place').hover(function(){
						explore.markers[ $(this).attr('category') ][ $(this).attr('place-id') ].marker.setAnimation(google.maps.Animation.BOUNCE);
					},function(){
						explore.markers[ $(this).attr('category') ][ $(this).attr('place-id') ].marker.setAnimation(null);
					});
					explore.visibleCategories[category] = false;
				}
			}
		}
		this.hideCategory = function(category){
			explore.visibleCategories[category] = false;
			$('ul#places-list .'+category).fadeOut(250);
			for (var i in this.markers) if (i == category) for (var j in this.markers[i]) {
				this.markers[i][j].marker.setVisible(false);
				this.markers[i][j].marker.setMap(null);
			}
		}
		this.toggleCategory = function(category){
			$('li.'+category).hasClass('selected') ? $('li.'+category).removeClass('selected') : $('li.'+category).addClass('selected');
			$('ul#places-list li.'+category).hasClass('selected') ? $('ul#places-list li.'+category).fadeIn(250) : $('ul#places-list li.'+category).fadeOut(250) ;
			if (explore.visibleCategories[category]) this.hideCategory(category);
			else this.showCategory(category);
		}
		this.showCategory = function(category){
			explore.visibleCategories[category] = true;
			$('ul#places-list .'+category).fadeIn(250);
			for (var i in this.markers) if (i == category) for (var j in this.markers[i]) {
				this.markers[i][j].marker.setAnimation(google.maps.Animation.DROP);
				this.markers[i][j].marker.setMap(explore.map);
				this.markers[i][j].marker.setVisible(true);
			}
		}
	}
});