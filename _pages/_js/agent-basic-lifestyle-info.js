var selected = [0,0,0];
var savedInfo = false;
var saving = false;
var files = null;
var dz = null;
// profileDesc = profileDesc.replace(/\\/g, "");
profileDesc = decodeURIComponent(profileDesc);

function ajax(d){
	if(typeof d.query !== 'undefined'){
		var h = [];
		if (typeof d.data === 'undefined') h.headers = {query:d.query}; else h.headers = {query:d.query,data:d.data};
		if (typeof d.done === 'undefined') h.done = function(){}; else h.done = d.done;
		// if (typeof d.fail === 'undefined') h.fail = function(){}; else h.fail = d.fail;
		if (typeof d.error !== 'undefined') h.error = d.error;
		if (typeof d.before === 'undefined') h.before = function(){}; else h.before = d.before;
		$.post(ah_local.tp+'/_sellers/_ajax.php', h.headers, function(){h.before()},'json')
			.done(function(x){
				if (x.status == 'OK') h.done(x.data);
				else if (h.error) h.error(x.data);
				else ahtb.close(function(){ahtb.alert(x.data, 'Database Error')});
			})
			.fail(function(x){
				if (h.fail) h.fail(x);
				else ahtb.alert("Database failure, please retry.", {height: 150});
			});
	} else ahtb.alert('Query was undefined');
}

/*
function setupAutocomplete(element) {
	element.autocomplete({
    source: cities,
    select: function(e, ui){ 
      if (ui.item.value != null) {
        element.attr('city_id', ui.item.id); 
        element.attr('city_name', ui.item.label); 
        cityName = ui.item.label;
        element.change();
      }
    }
  }).on('focus',function(){
    if ($(this).val() == 'ENTER CITY') {
      $(this).val(''); $(this).removeClass('inactive');
    }
  }).on('keypress', function(e){
    var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
    if(keynum == 13) {//13 is the enter char code
      e.preventDefault();
    }
    $(this).attr('city_id', 0);
    $(this).attr('city_name', '');	
  });
}

function reallyReserve() {
	testAgentMatchList.item.splice(0, testAgentMatchList.item.length); // remove any items
	var ele = $('#information #lifestyles select');
	ele.each(function() {
 		var which = parseInt($(this).attr('for'));
		var val = selected[which];
		if (val != 0) {
			var item = {};
	 		item.location = testCityId;
	 		item.locationStr = testCityName;
	 		item.specialty = amTagList[val].id;
	 		item.specialtyStr = amTagList[val].tag;
	 		testAgentMatchList.item[testAgentMatchList.item.length] = item;
	 		console.log("AM specialty is "+val+' for:'+which);
	 	}
 	})

 	if (testAgentMatchList.item.length == 0) {
 		ahtb.push();
	    window.setTimeout(function() {
 			ahtb.alert("Please pick at least one lifestyle.", 
 				{height: 150,
                 buttons: [
                      {text: 'OK', action: function() {
                        ahtb.pop();
                      }}]});
 		}, 150);
 		return;
 	}
 	else {
 		savedInfo = true;
 		ahtb.close();
 	}
}
*/
function checkValidLifestyle() {
	var tag = $('.lifestyletags').attr('value');
	console.log("testing lifestyle:"+amTagList[tag].tag+" at "+testCityName);

	if (isFree) {
		$('.top #information').slideToggle('fast');
		$('.top .more').hide();
		$('.top .less').show();
		return;
	}

	var val = {city_id: testCityId,
			   tag_id: tag
			  };

	ajax({
		query: 'check-lifestyle-availability',
		data: val,
		done: function(d) {
			// $('#message #marker').show();
			// testPortal = d;
			// ele.val(d);
				var infodropped = $('#information').css('display') != 'none';
				if (!infodropped) {
					$('.top #information').slideToggle('fast');
					$('.top .more').hide();
					$('.top .less').show();
					//$('.top .hiddendiv').hide();
				}
				else 
					return;
				},
		error: function(d) {
			ahtb.alert("The lifestyle, "+amTagList[tag].tag+" is not available for this city anymore.</br>Please try another one.", {height: 180, width: 450});
			console.log("There are "+d.length+" agents now");
			// ele.val('');
			// $('#message #marker').hide();
		}
	});
};

function pickedCity(wasPushed, city_id, city) {
	// $('#information #user-data .service_areas').attr('city_id', city_id); 
 //    $('#information #user-data .service_areas').attr('city_name', city); 
	// $('#information #user-data .service_areas').val(city);
	$('input#city').val(city);
	testCityName = city;
	testCityId = city_id;
	if (wasPushed)
		ahtb.close();

	window.setTimeout(function() {
		checkValidLifestyle();
	}, 350);
}

function getCityId(id, city) {
	$.post(ah_local.tp+'/_pages/ajax-quiz.php',
        { query:'find-city',
          data: { city: city,
          		  distance: 0 }
        },
        function(){}, 'JSON')
		.done(function(d){
			if (d &&
                d.data &&
                d.data.city &&
                (typeof d.data.city == 'object' ||
                d.data.city.length) ){
                if (d.status == 'OK' &&
                    (typeof d.data.city == 'object' || d.data.city.length) ) {							                        
                    console.log(JSON.stringify(d.data));

                	if (d.data.allState == 'true' || d.data.allState == true || d.data.allState == 1) {// doing statewide
                        ahtb.alert("Please enter a city name along with the state.", {height: 150});
                        return;
                    }

                    var len = length(d.data.city); //(typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
                	// var len = (typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
					if (len > 1) {
						if (d.data.allState == 'false' || d.data.allState == false) {
	                        var h = '<p>Please pick one of the cities:</p>' +
	                                  '<ul class="cities">';
	                        for(var i in d.data.city) {
	                        	var location = d.data.city[i].city+', '+d.data.city[i].state;
	                          	h += '<li><a href="javascript:pickedCity('+1+','+d.data.city[i].id+",'"+location+"'"+');" >'+location+'</a></li>';
	                        }
	                        h += '</ul>';
	                        ahtb.push();
	                        window.setTimeout(function() {
	                        	ahtb.open(
		                          { title: 'Cities List',
		                            width: 380,
		                            height: (150 + (len * 25)) < 680 ? (150 + (len * 25)) : 680,
		                            html: h,
		                            buttons: [
		                                  {text: 'Cancel', action: function() {
		                                    ahtb.pop();
		                                  }}],
		                            opened: function() {
		                            }
		                          })
	                        }, 150);
	                        
	                    }
	                    else {
	                    	ahtb.push();
	                    	window.setTimeout(function() {
	                    		ahtb.alert("Only one city/state combination accepted.", 
	                    			{height: 150,
	                    			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		ahtb.pop();
			 						 	}}]});
	                    	}, 150);
                          	
                        }
                    } // only one!
                    else {
                    	pickedCity(0, d.data.city[0].id, d.data.city[0].city+', '+d.data.city[0].state);
                    }
                }
                else {
                	ahtb.push();
                	window.setTimeout(function() {
                		ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                  			{title: 'City Match', 
	                  			 height:180, 
	                  			 width:600,
	                  			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		ahtb.pop();
			 						 	}}]});
                	}, 150);
                }
            }
            else {
            	ahtb.push();
            	window.setTimeout(function() {
	                ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                  			{title: 'City Match', 
	                  			 height:180, 
	                  			 width:600,
	                  			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		ahtb.pop();
			 						 	}}]});
	            }, 150);
            }
        })
        .fail(function(d){
        	ahtb.push();
            window.setTimeout(function() {
	        	ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                  			{title: 'City Match', 
	                  			 height:180, 
	                  			 width:600,
	                  			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		ahtb.pop();
			 						 	}}]});
	        }, 150);
        });
}

jQuery(document).ready(function($){
	console.log("starting agent-purchase-portal page");
	window.history.pushState('pure','Title',ah_local.wp+'/'+thisPage);
	
	//var msg = "<div id='basic-info'>Please supply some basic information to "+(isFree ? 'activate' : 'purchase')+" your Lifestyled Agent product.<br/><br/>" +
	//		  "This is information that a user will see when they view a listing that matches your expertise.  " +
	//		  "A photo and at least a 300-character description about why you are the expert in this city, " +
	//		  "along with some basic information about yourself as an agent.<br/><br/>"+
	//		  "Once we have this information, "+(isFree ? 'you will be activated and show up in appropriate listings' : 'we can process your order')+".</div>";

	//ahtb.open({html: msg,
	//		   width: 600,
	//			height: isFree ? 330 : 300});

	var state = -1;
	var password = '';
	var verify = '';
	var bre = '';
	var invitationCode = coupon;
	var price = 99 - discount;
	var callMe = false;
	var agreeToTerms = false;
	// var availableStates = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming', 'American Samoa', 'Guam', 'Northern Mariana Islands', 'Puerto Rico', 'Virgin Islands', 'Federated States of Micronesia', 'Marshall Islands', 'Palau', 'Armed Forces Africa \ Canada \ Europe \ Middle East', 'Armed Forces America (Except Canada)', 'Armed Forces Pacific'];

	var controller = new function() {
		this.cropImg = null;

		this.init = function() {
			$('div#nextStep #message').html('Congratulations, you are now an active Lifestyled Agent!');
			
			//this.showOptions();

			console.log("controller.init entered");
			$('input#city').val( item ? item.locationStr : service_area);
			testCityName = item ? item.locationStr : service_area;
			testCityId = item ? item.location : city_id;
			var desc = item ? item.desc.replace(/\\/g, "") : profileDesc.replace(/\\/g, "");
			$('textarea#description').val( desc );
			this.updateCounter( desc );

			if (tag) {
				$('.ul#tags li[value="'+tag+'"]').prop('selected', true);
				$('.selected').html(amTagList[tag].tag);
			}

			$('div#nextStep button').on('click', function() {
				var id = $(this).attr('id');
				var redirect = ah_local.wp;
				switch(id) {
					case 'gotoProfile':
						redirect = ah_local.wp+"/sellers/#profile";
						break;
					case 'gotoDashboard':
						redirect = ah_local.wp+"/sellers";
						break;
					case 'gotoLAPage':
						redirect = ah_local.wp+"/sellers/#agent_match_reserve";
						break;
					case 'gotoAddNewListing':
						redirect = ah_local.wp+"/new-listing";
						break;
					case 'gotoHome':
						redirect = ah_local.wp;
						break;
				}
				window.location = redirect;
			})

			$('input#city').autocomplete({
				source:  function( request, response ) {
					          var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
					          response( $.grep( cities, function( item ){
					              var match = matcher.test( item.label );
					              return match;
					          }) );
					      },
			    select: function(e, ui){ 
			      if (ui.item.value != null) {
			        $('input#states').attr('city_id', ui.item.id); 
			        $('input#states').attr('city_name', ui.item.label); 
			        testCityName = ui.item.label;
					testCityId = ui.item.id <= cityLimit ? 0 : ui.item.id - cityLimit;
			        $('input#states').change();
			        $('#information').css('opacity', '1.0');
			      }
			    }
			  }).on('focus',function(){
			    if ($(this).val() == 'ENTER CITY') {
			      $(this).val(''); $(this).removeClass('inactive');
			    }
			  }).on('keypress', function(e){
			    var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
			    if(keynum == 13) {//13 is the enter char code
			      e.preventDefault();
			    }
			    $(this).attr('city_id', '');
			    $(this).attr('city_name', '');	
			    $('#information').css('opacity', '0.5');
			  });

			$('input#states').autocomplete({
				source: allStates,
			    select: function(e, ui){ 
			      if (ui.item.value != null) {
			        $('input#states').attr('state_id', ui.item.id); 
			        $('input#states').attr('state_name', ui.item.label); 
			        // state = ui.item.id;
			        $('input#states').change();
			      }
			    }
			  }).on('focus',function(){
			    if ($(this).val() == 'ENTER STATE') {
			      $(this).val(''); $(this).removeClass('inactive');
			    }
			  }).on('keypress', function(e){
			    var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
			    if(keynum == 13) {//13 is the enter char code
			      e.preventDefault();
			    }
			    $(this).attr('state_id', '');
			    $(this).attr('state_name', '');	
			  });

			$('textarea').on('keyup', function(e) {
				var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
			    if(keynum == 13) {//13 is the enter char code
			      e.preventDefault();
			    }
				var len = MIN_LENGTH_DESC - $(this).val().length;
				var counter = len > 0 ? len : 0;
				$('#chCount').html(counter.toString());
			})
			// .on('focusout', function() {
			// 	var len = 300 - $(this).val().length;
			// 	var counter = len > 0 ? len : 0;
			// 	$('#chCount').html(counter.toString());
			// })

			var ele = $('.lifestyletags');
			$('.lifestyletags').attr('value', tag); // default value - ALL
			$('.lifestyletags ul').find('li').click(function(){
				$('.lifestyletags ul li[value]').removeClass('active');
				var val = $(this).attr('value');
				$(this).parent().css('display','none');
				$(this).addClass('active');
				$(this).closest('.lifestyletags').attr('value',val);
				$(this).parent().siblings('.selected').html($(this).html());
				console.log("lifestyle value:"+val);
				$('#information').css('opacity', '1.0');

				if (tag)
					controller.updateToProfile(tag); // save what we have
				tag = parseInt(val);
				controller.updateFromProfile(tag); // use what we may have or have saved

			});
			$('.lifestyletags').each(function(){
				$(this).children('.selected,span.selectArrow').click(function(event){
					event.stopPropagation();
					if($(this).parent().children('ul').css('display') == 'none'){
						$(this).parent().children('ul').css('display','block');
						// $('ul#max').css('display','none');
						$('.lifestyletags').addClass('active');
						$('#information').css('opacity', '0.5');
					}
					else {
						$(this).parent().children('ul').css('display','none');
						$('.lifestyletags').removeClass('active');
						$('#information').css('opacity', '1.0');
					}
				});
			});

			$(document).click( function(){
				$('ul#tags').css('display','none');
				$('.lifestyletags').removeClass('active');
				$('#information').css('opacity', '1.0');
			});


			$('[name=email]').val(sellerEmail);

			/* Phone numbers */
			$('[name=mobile], [name=phone]').mask('(999) 000-0000');
			/* Contact Email */
			$('input[name=contact_email_same]').on('click',function(){
				if ( $(this).prop('checked') ){
					$('[name=contact_email]').prop('disabled', true).val(validSeller.email);
				} else $('[name=contact_email]').prop('disabled', false).val('');
			});

			controller.setupValidSeller(true);

			// $('#define-portal #portal').val(testPortal);
			$('#message #marker').css('color', 'lightgreen');
			$('#message #marker').hide();
			$('#message #base-url').text('LifestyledListings.com/');
			$('#message #portal').keyup(function(e){
				e.preventDefault();
				$('#message #marker').hide();
			})

			$('#invitation_code').val(coupon);
			
			$('.maintext .pricebox').html("$"+price.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" Monthly"+(discount ? " with Code" : ''));
			$('#user-data #marker').hide();

			$('input#agreeToTerms').change( function() {
				var checked = $(this).prop('checked');
				agreeToTerms = checked;
				console.log("agreeToTerms is now "+checked);
			})

			$('input#callme').change( function() {
				var checked = $(this).prop('checked');
				callMe = checked;
				console.log("CallMe is now "+checked);
			})

			controller.initDropZone();
			controller.initUserData();
			controller.addKeyStrokeCallbacks();
			controller.addButtonCallbacks();
		}

		this.updateCounter = function( desc ) {
			var len = MIN_LENGTH_DESC - desc.length;
			var counter = len > 0 ? len : 0;
			$('#chCount').html(counter.toString());
		}

		this.showOptions = function() {
			$('div#agent-basic-lifestyle-info').animate({opacity: 0.5});
			$('div#nextStep').fadeIn({duration: 2000});
		}

		this.prepareUpload = function(e) {
			files = e.target.files;
		}

		this.upload = function(e) {
			setCookie("IMAGE_DIR", "_authors/uploaded/", 1);
			if (e) {
				e.stopPropagation(); // Stop stuff happening
			    e.preventDefault(); // Totally stop stuff happening
			}
		 		 
		    // Create a formdata object and add the files
			formData = new FormData();
			formData.append ('image_dir','_authors/uploaded/');
			formData.append ('gen_sizes', 1 );
			$.each(files, function(key, value)
			{
				console.log(key, value);
				formData.append(key, value);
			});

			$('.spin-wrap').show();

			$.ajax({
		        url: ah_local.tp+'/_classes/Image.class.php',
		        type: 'POST',
		        data: formData,
		        cache: false,
		        dataType: 'json',
		        processData: false, // Don't process the files
		        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				error: function($xhr, $status, $error){
					$('.spin-wrap').hide();
					if ( !$xhr.responseText &&
						 !$error)
						return; // ignore it

					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					ahtb.open({ title: 'You Have Encountered an Error', 
								height: 150, 
								html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
							});
				},					
			  	success: function(data){
			  		$('.spin-wrap').hide();
				  	if ( data == null || data.status == null) 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else {
				  		controller.updateAuthorPhoto(data);
				  		// ahtb.alert(data.data, {width: 450, height: 150});
				  	}
				}
			})
		}

		this.updateAuthorPhoto = function(x) {
			if (x.status != 'OK') ahtb.alert(typeof x.data == 'string' ? '<p>'+x.data+'</p>' : '<p>Unable to upload image.</p>');
			else ahtb.loading('Saving photo...',{
				opened: function(){
					saving = true;
					controller.DBget({	query:'update-author', 
										data:{ id: validSeller.id,
												fields: {photo:x.data[0]},
												fromBasicInfoPage: 1,
											  }, 
										done:function(d){
											if (typeof dz.disable == 'function') dz.disable();
											if (typeof d == 'object') {
												validSeller = d.seller;
												controller.setupValidSeller();
											}
											else
												validSeller.photo = x.data[0];
											$('#dropzone').hide();
											$('div.seller-photo img').attr('src', ah_local.tp+'/_img/_authors/272x272/'+x.data[0]);
											$('div.header-login img').attr('src', ah_local.tp+'/_img/_authors/55x55/'+x.data[0]);
											$('div.seller-photo').show();

											var warnIncomplete = validSeller.flags & SellerFlags.SELLER_NEEDS_BASIC_INFO;
											if (isFree) {
												if ( !warnIncomplete ) {// ha ha!  all done
													if (!agreeToTerms) {
														ahtb.close();
														return;
													}
													controller.tryCompletingTransaction();
													// ahtb.open({html: "<div id='congrats'>You now have all the basic information and have been activated.<br/>Press OK to be directed to the seller's profile page</div>",
													// 		   width: 550,
													// 		   height: 180,
													// 		   buttons: [{text:'OK', action: function() {
													// 		   		window.location = ah_local.wp+"/sellers/#profile";
													// 		   }}],
													// 		   closed: function() {
													// 		   		window.location = ah_local.wp+"/sellers/#profile";
													// 		   }});
												}
												else
													ahtb.close();
											}
											else
												ahtb.close();
										},
										error:function(x) {
											var msg = typeof x == 'string' ? x :
													  typeof x == 'object' ? JSON.stringify(x) :
													  "Unknown reason";
											ahtb.alert("Failure saving image: "+msg);
										}
									});
				}
			});
		}

		this.initDropZone = function() {
			console.log("controller.initDropZone entered");
			if ( $('#dropzone').length == 0)
				return;

			/* Dropzone and seller photo */
			Dropzone.autoDiscover = false;
			dz = new Dropzone("#dropzone", {
				acceptedFiles:'image/*',
				url: ah_local.tp+'/_classes/Image.class.php',
				maxFiles: 1,
				dictDefaultMessage: '<p>Upload Profile Image</p><span class="entypo-upload-cloud"></span><button>Browse</button>',
				dictFallbackText: 'Please choose your photo',
				headers:{"image_dir":"_authors/uploaded/","gen_sizes":1},
				init: function(){
					setCookie('IMAGE_DIR', '_authors/uploaded/', 1);
					$('.dz-message button').on('click', function(e){e.preventDefault()});
					$('#dropzone').hover(function(){
						$('.dz-message button, .dz-message span span, .dz-message p').addClass('hover');
					},function(){
						$('.dz-message button, .dz-message span span, .dz-message p').removeClass('hover');
					});
					this.on('maxfilesexceeded', function(){ ahtb.alert({html:'<p>You can only upload one image for your profile.</p>'}) });
					this.on('error', function(file, responseText) {
						var x = $.parseJSON(responseText);
					});
					this.on("success", function(file, responseText){
						var x = $.parseJSON(responseText);
						controller.updateAuthorPhoto(x);
					});
				}
			});
			console.log("photo: "+validSeller.photo);

			$('div.dz-fallback input[name="file"]').on('change', controller.prepareUpload);
			$('div.dz-fallback input[value="Upload!"]').on('click', controller.upload);

			if (typeof dz.disable != 'function')
				$('#dropzone').append('<div class="spin-wrap"><div class="spinner sphere"></div></div>');

			if (validSeller.photo){ 
				console.log("Found photo disabling dropzone");
				$('#dropzone').hide(); 
				if (typeof dz.disable == 'function') dz.disable(); 
				$('div.seller-photo').show();
			}
			else {
				$('div.seller-photo').hide();
				$('#dropzone').show(); 
				if (typeof dz.enable == 'function') dz.enable();
			}
			$('div.seller-photo .overlay a').on('click', function(){
				if ($(this).hasClass('upload-photo')) {
					if (typeof dz.enable == 'function') dz.enable();
					$('div.seller-photo').fadeOut(250,function(){$('#dropzone').fadeIn(250)});
				}
				else if ($(this).hasClass('edit-cropping')) 
					controller.editCrop();
			});
			$('#delete-photo').on('click', function() {
				if (!validSeller.photo) {
					ahtb.alert("No photo on file!");
					return;
				}
				controller.DBget({	query:'remove-photo', 
									data:{ id: validSeller.id,
										   name: validSeller.photo
								    }, 
									done:function(d){
										console.log(typeof d == 'string' ? d : "Deleted photo"); 
										$('.seller-photo').hide();
									  	if (dz.files.length)
									  		if (typeof dz.removeFile == 'function') dz.removeFile(dz.files[0]);
									  	$('#dropzone').show(); 
									  	if (typeof dz.enable == 'function') dz.enable();
									  	validSeller.photo = null;
									  	validSeller.flags |= SellerFlags.SELLER_NEEDS_BASIC_INFO;
									},
									error:function(x) {
										var msg = typeof x == 'string' ? x :
												  typeof x == 'object' ? JSON.stringify(x) :
												  "Unknown reason";
										ahtb.alert("Failure removing photo: "+msg);
									}
								}); 
			})
			$('#delete-photo-2').on('click', function() {
				if (!validSeller.photo) {
					ahtb.alert("No photo on file!");
					return;
				}
				controller.DBget({	query:'remove-photo', 
									data:{ id: validSeller.id,
										   name: validSeller.photo
								    }, 
									done:function(d){
										console.log(typeof d == 'string' ? d : "Deleted photo"); 
										$('div.seller-photo').hide();
									  	if (typeof dz.files != 'undefined' &&
									  		dz.files.length)
									  		if (typeof dz.removeFile == 'function') dz.removeFile(dz.files[0]);
									  	$('#dropzone').show(); 
									  	if (typeof dz.enable == 'function') dz.enable();
									  	validSeller.photo = null;
									  	validSeller.flags |= SellerFlags.SELLER_NEEDS_BASIC_INFO;
									},
									error:function(x) {
										var msg = typeof x == 'string' ? x :
												  typeof x == 'object' ? JSON.stringify(x) :
												  "Unknown reason";
										ahtb.alert("Failure removing photo: "+msg);
									}
								}); 
			})
		}
		
		this.applyCrop = function(x){
			ahtb.open({ html: '<p>Cropping image...</p>', 
						height: 150, 
						width: 450, 
						showClose: false,
						opened: function() {
						$.post(ah_local.tp+'/_classes/Image.class.php', 
							{query:'resize',
							 data:{
								file: validSeller.photo,
								src_path: '_authors/uploaded/',
								src_width: x.srcBounds[0], src_height: x.srcBounds[1],
								dst_path: '_authors/cropped/',
								dst_x: x.dstBounds.x, dst_y: x.dstBounds.y,
								dst_x2: x.dstBounds.x2, dst_y2: x.dstBounds.y2
							}},function(){},'json')
						.done(function(d){
							if (d.status == 'OK'){
								ahtb.edit({ html: '<p>Saving image...</p>' });
								controller.DBget({query:'update-author', 
											  data:{id: validSeller.id,
											  		fields: {photo:d.data},
													fromBasicInfoPage: 1,
											  	}, 
											  done:function(){
												validSeller.photo = d.data;
												controller.setupValidSeller();
												// ahtb.close();
												controller.cropImg = null;
												var toLoad = ['/55x55/'+x.data, '/420x260/'+d.data];
												ahtb.edit({html:'<p>Loading images...</p>'});
												var loaded = 0;
												var img = new Image();
												img.onload = function(){ loaded++; $('.seller-photo img').attr('src', ah_local.tp+'/_img/_authors/272x272/'+d.data); if (loaded == 2) ahtb.close(); }
												img.src = ah_local.tp+'/_img/_authors/272x272/'+d.data;
												var imgProf = new Image();
												imgProf.onload = function(){ loaded++; $('.header-login img.profile-picture').attr('src', ah_local.tp+'/_img/_authors/55x55/'+d.data); if (loaded == 2) ahtb.close(); }
												imgProf.src = ah_local.tp+'/_img/_authors/55x55/'+d.data;
											  },
											  error:function(x) {
												var msg = typeof x == 'string' ? x :
														  typeof x == 'object' ? JSON.stringify(x) :
														  "Unknown reason";
												ahtb.alert("Failure saving image: "+msg);
											  }
											});
							}
							else {
								console.log(x);
								ahtb.close();
							}
						})
						.error(function(d){
							ahtb.alert(d);
						})
					}
				})
		}
		this.printCrop = function(){
			img = controller.cropImg;
			var margin = 150;
			var i = 720;
			while (i*img.height/img.width > 500) i-=10;
			var x = { height: (i*img.height/img.width), width: i };
			// var x = { height: img.height+50, width: i };
			ahtb.edit({
				width: x.width,
				height: x.height+margin,
				html: 'Please adjust the cropping for your photo, then click save.<img src="'+img.src+'" />',
					// '<button class="save">Save</button><button class="cancel">Cancel</button></div>',
				opened: function(){
					ahtb.showClose(100);
					$('#tb-submit img').Jcrop({
						aspectRatio: 1.0,
						bgFade: true,
						bgOpacity: .3,
						setSelect: [ 0, 0, 10, 360 ],
						boxHeight: x.height,
						boxWidth: x.width
					},function(){ controller.jcrop = this; });
				},
				buttons:[{text:'Save', action:function() {
					var dstBounds = controller.jcrop.tellSelect();
					if (dstBounds.w == 0 || dstBounds.h == 0) 
						ahtb.edit({
							html: '<p>Please select an area to crop before hitting save.</p><button>OK</button>',
							width: 450, height: 155,
							opened: function(){
								$('#tb-submit button').on('click',function(e){ e.preventDefault(); controller.printCrop(); });
							}
						});
					else controller.applyCrop({ dstBounds: dstBounds, srcBounds: controller.jcrop.getBounds() });
				}},
				{text:'Cancel', action: function() {
					ahtb.close();
				}}]
			});
		}
		this.editCrop = function(){
			ahtb.loading('Loading full size image...',{
				opened: function(){
					var img = new Image();
					img.src =ah_local.tp+'/_img/_authors/272x272/'+validSeller.photo;
					img.onerror = function(){ ahtb.alert('<p>Sorry, an error occurred while loading your image.</p>', {width: 500, height: 180}); }
					img.onload = function(x){ controller.cropImg = img; controller.printCrop(); }
					img.src = ah_local.tp+'/_img/_authors/uploaded/'+validSeller.photo;
				}
			});
		}

		this.initUserData = function() {
			console.log("controller.initUserData entered");
			if (typeof validSeller == 'undefined')
				return;
			
			var h = '<div class="experience">';
					h+= '<label for="experience" style="display:inline">When did you receive your Real Estate License?</label>';
						var date = new Date();
						var n = date.getFullYear();
							h+= '<select name="year" id="year" style="display:inline;margin-right:1em;margin-top:.15em;">';
								for(var i = 0; i < 50; i++) {
									h+= '<option value="'+(n-i).toString()+'" '+(validSeller.profile_meta && validSeller.profile_meta.year == (n-i).toString() ? 'selected="selected"' : '')+' >'+(n-i).toString()+'</option>';
			                     }
			                     h+= '<option value="'+(n-i).toString()+'" '+(validSeller.profile_meta && parseInt(validSeller.profile_meta.year) <= (n-i) ? 'selected="selected"' : '')+'>Over 50 years</option>';
			     			h+= '</select>';
				h+= '</div>';
				h+= '<div class="experience">';
					h+= '<label for="homes-sold" style="display:inline">Total transactions in the past 12 months</label>';
							h+= '<select name="sold" id="sold" style="display:inline;margin-right:1em;margin-top:.15em;">';
								for(var i = 1; i <= 100; i++) {
									h+= '<option value="'+(i).toString()+'" '+(validSeller.profile_meta && validSeller.profile_meta.sold == (i).toString() ? 'selected="selected"' : '')+' >'+(i).toString()+(i == 1 ? " Transaction": " Transactions")+'</option>';
			                     }
							h+= '</select>';
				h+= '</div>';
				h+= '<div class="experience">';
					h+= '<label for="years-in-area" style="display:inline">When did you start living in your service area?</label>';
						h+= '<select name="inArea" id="inArea" style="display:inline;margin-right:4em;margin-top:.15em;">';
							for(var i = 0; i < 50; i++) {
								h+= '<option value="'+(n-i).toString()+'" '+(validSeller.profile_meta && validSeller.profile_meta.inArea == (n-i).toString() ? 'selected="selected"' : '')+' >'+(n-i).toString()+'</option>';
			                 }
			                 h+= '<option value="'+(n-i).toString()+'" '+(validSeller.profile_meta && parseInt(validSeller.profile_meta.inArea) <= (n-i) ? 'selected="selected"' : '')+'>Over 50 years</option>';
			     		h+= '</select>';
				h+= '</div>';

			$('div.right-col').append(h);
		}
		
		this.addKeyStrokeCallbacks = function() {
			console.log("controller.addKeyStrokeCallbacks entered");
			$('#invitation_code').keyup(function(e) {
		    	e.preventDefault();
		    	code = $(this).val();
		    	if (code.length == 0) {
					var price = 29;
					var discount = 0;
					$('.maintext .pricebox').html("$"+price.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" Monthly"+(discount ? " with Code" : ''));
		   			return;
		    	}
		    	ajax({
					query: 'check-coupon',
					data: {coupon: code},
					done: function(d) {
						var discount = parseInt(d);
						if (discount != InviteCodeMode.IC_ACCEPTED) {
							var price = 39 - discount;
							$('.whatisportal .right  p + p').html("And perhaps best of all - it's $"+discount+" off!");
							$('.bottom .sidetext .title').html('Why is it so affordable?');
							// $('.maintext .pricebox').html("$"+price.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" Monthly"+(discount ? " with Code" : ''));
						}
						else {
							$('.whatisportal .right  p + p').html("And perhaps best of all - it's free!");
							$('.bottom .sidetext .title').html('Why is it free?');
							// $('.maintext .pricebox').html("Gratis - Invitation");
						}
		    		},
		    		error: function(d) {
		    			var price = 29;
		    			var discount = 0;
		    			$('.maintext .pricebox').html("$"+price.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" Monthly"+(discount ? " with Code" : ''));
						if (typeof d == 'object' &&
							typeof d.msg != 'undefined') {
							ahtb.open({html: d.msg,
									  width: 450,
									  height: 110,
									  buttons:[{text:'OK', action:function() {
									  	ahtb.close();
									  }}]});
						}
						else {
							var msg = typeof d == 'string' ? d : "We're very sorry, had trouble finding the code in the system.";
							console.log("Failed: "+msg);
							// ahtb.alert(msg, {height: 150, width: 450});
						}
					}
				})
		    })

			$('#verify').keyup(function(e) {
		    	e.preventDefault();
		    	verify = $(this).val();
		    	var vlen = $(this).val().length;
		    	var len = $('#password').val().length;
		    	len || vlen ? $('#user-data #marker').show() : $('#user-data #marker').hide();
		    	//$('#marker').attr('visibility', len ? 'visible' : 'hidden');
		    	var hasCancel = $('#user-data #marker').hasClass('entypo-attention');
		    	if ($(this).val() == $('#password').val()) {
		    		if (hasCancel) {
		    			$('#user-data #marker').removeClass('entypo-attention');
		    			$('#user-data #marker').addClass('entypo-check');
		    			$('#user-data #marker').css('color', 'lightgreen');
		    		}
		    	}
		    	else if (!hasCancel) {
		    			$('#user-data #marker').removeClass('entypo-check');
		    			$('#user-data #marker').addClass('entypo-attention');
		     			$('#user-data #marker').css('color', 'red');
		   		}
		    	
		    })

		    $('#password').keyup(function(e) {
		    	e.preventDefault();
		    	password = $(this).val();
		    	var len = $(this).val().length;
		    	var vLen = $('#verify').val().length;
		    	len || vLen ? $('#user-data #marker').show() : $('#user-data #marker').hide();
		    	//$('#marker').attr('visibility', len ? 'visible' : 'hidden');
		    	var hasCancel = $('#user-data #marker').hasClass('entypo-attention');
		    	if ($(this).val() == $('#verify').val()) {
		    		if (hasCancel) {
		    			$('#user-data #marker').removeClass('entypo-attention');
		    			$('#user-data #marker').addClass('entypo-check');
		    			$('#user-data #marker').css('color', 'lightgreen');
		    		}
		    	}
		    	else {
		    		if (!hasCancel) {
		    			$('#user-data #marker').removeClass('entypo-check');
		    			$('#user-data #marker').addClass('entypo-attention');
		     			$('#user-data #marker').css('color', 'red');
		   			}
		    	}
		    })
		}

		this.addButtonCallbacks = function() {
			console.log("controller.addButtonCallbacks entered");
			$('button#check-it').on('click', function() {
				var tag = $('.lifestyletags').attr('value');
				if (tag == '0') {
					ahtb.alert('Please pick a lifestyle');
					return;
				}

				if (testCityId == 0) {
					var city = $('input#city').val();
					if (city.length == 0) {
						ahtb.alert('Please enter a city name');
						return;
					}
					getCityId(0, city);
					return;
				}

				pickedCity(0, testCityId, testCityName);
			});


			$('#information .reserveportal').on('click', function() {
				agentMatchDetail();
			})

			$('button#activateIt').on('click', function() {
			 	tag = $('.lifestyletags').attr('value');
				if (tag == '0') {
					ahtb.alert('Please pick a lifestyle');
					return;
				}

				if (testCityId == 0) {
					var city = $('input#city').val();
					if (city.length == 0) {
						ahtb.alert('Please enter a city name');
						return;
					}
					getCityId(0, city);
					return;
				}

				if (!showSimplifiedAgentLifestylePages)
					if ($('input[name=company]').val().length == 0) {
						ahtb.alert('Please your company name');
						return;
					}

				var desc = $('textarea').val();
				var descLen = desc.length;
				if (desc.length < MIN_LENGTH_DESC) {
					ahtb.alert('Please enter the lifestyle description with at least '+MIN_LENGTH_DESC+' characters.', {height: 180});
					return;
				}

				var data = $('input#coupon').val();
				if (!isFree && 
					 data.length)
					coupon = data;

				controller.updateAuthor();
			
			})
		}

		this.setupValidSeller = function(firstTime) {
			if (typeof validSeller == 'undefined')
				return;

			validSeller.profile_meta = null;
			if (typeof firstTime != 'undefined' &&
				firstTime) {
				validSeller.mod_meta = null;
				validSeller.am_order = null;
			}
			validSeller.am_meta = null; // this is the agent profile's specific lifestyle entries/descriptions
			for(var i in validSeller.meta)
				if (validSeller.meta[i].action == SellerMetaFlags.SELLER_PROFILE_DATA)
					validSeller.profile_meta = validSeller.meta[i];
				else if (validSeller.meta[i].action == SellerMetaFlags.SELLER_MODIFIED_PROFILE_DATA)
					validSeller.mod_meta = validSeller.meta[i];
				else if (validSeller.meta[i].action == SellerMetaFlags.SELLER_AGENT_MATCH_DATA)
					validSeller.am_meta = validSeller.meta[i];
				else if (validSeller.meta[i].action == SellerMetaFlags.SELLER_AGENT_ORDER)
					validSeller.am_order = validSeller.meta[i];

			if (validSeller.mod_meta == null) {
				validSeller.mod_meta = { 'action': SellerMetaFlags.SELLER_MODIFIED_PROFILE_DATA,
									'first_name': 0,
									'last_name': 0,
									'company': 0,
									'website': 0,
									'phone': 0,
									'mobile': 0,
									'email': 0};

			}

			if (validSeller.am_order) {
				validSeller.am_order.agreed_to_terms = parseInt(validSeller.am_order.agreed_to_terms);
				$('input#agreeToTerms').prop('checked', validSeller.am_order.agreed_to_terms ? true : false);
				agreeToTerms = validSeller.am_order.agreed_to_terms ? true : false;
				console.log("setupValidSeller - agreeToTerms:"+agreeToTerms);
			}
		}

		this.updateToProfile = function(tag) {
			if (validSeller.am_meta) {
				for(var i in validSeller.am_meta.specialties) {
					if ( validSeller.am_meta.specialties[i].id == tag) {
						if ( $('textarea').val().length )
							validSeller.am_meta.specialties[i].desc = $('textarea').val();
						return;
					}
				}
				for(var i in validSeller.am_meta.lifestyles) {
					if ( validSeller.am_meta.lifestyles[i].id == tag) {
						if ( $('textarea').val().length )
							validSeller.am_meta.lifestyles[i].desc = $('textarea').val();
						return;
					}
				}
			}
		}

		this.updateFromProfile = function(tag) {
			if (validSeller.am_meta) {
				for(var i in validSeller.am_meta.specialties) {
					if ( validSeller.am_meta.specialties[i].id == tag) {
						var desc = validSeller.am_meta.specialties[i].desc.replace(/\\/g, "");
						$('textarea').val(desc);
						this.updateCounter( desc );
						return;
					}
				}
				for(var i in validSeller.am_meta.lifestyles) {
					if ( validSeller.am_meta.lifestyles[i].id == tag) {
						var desc = validSeller.am_meta.lifestyles[i].desc.replace(/\\/g, "");
						$('textarea').val(desc);
						this.updateCounter( desc );
						return;
					}
				}
			}
		}

		this.validateCode = function(code, what, callback) {
			controller.DBget({
				query: 'verify-invitecode',
				data: {code: code,
					   option: what },
				done: function(d) {
					console.log("verify-invitecode - "+d);
					validSeller.am_invitation = code;
					if (typeof callback == 'function')
						callback();								
	    		},
				error: function(d) {
					ahtb.alert("We're very sorry, please try another code. "+(typeof d == 'string' ? d : ((typeof d == 'object' || Array.isArray(d)) &&
																										   typeof d.status != 'undefined' ? d.status : '')), 
								{height: 150, 
								width: 450,
								buttons: [
									{text:'OK', action: function() {
										ahtb.close();
									}}]});
				}
			});
		}

		this.processAMInvite = function() {
			// now do ajax to backend to get the order going...
			if (!agreeToTerms) {
				ahtb.alert('Please read and agree to our terms and conditions.', {width: 500, height: 150});
				return;
			}
			validSeller.am_order.agreed_to_terms = 1;
			ahtb.alert("Processing order...",
						{height: 140,
						 hideSubmit: true});
			controller.DBget({
				query: 'process-agent-match-invite',
				data: {order: validSeller.am_order,
						code: validSeller.am_invitation,
						id: validSeller.id
						// portalSpec needed for ORDER_PORTAL, and even then, if left out, will use default values
					  },
				done: function(d) {
					console.log("process-agent-match-invite - "+d.status);
					if (d.status == "Update made") {
						validSeller.am_order = d.order;
						ahtb.close();
						controller.showOptions();
						// window.location = ah_local.wp+"/sellers/#profile";
					}	
					else if (d.status == "Cart made") {
						validSeller.am_order = d.order;
						window.location = ah_local.wp+'/cart';		
					}
					else if (d.status == "Coupon error") {
						validSeller.am_order = d.order;
						ahtb.open({html: "Coupon had "+d.errorCount+" error(s):"+d.error,
								   width: 450,
								   heigth: 110 + (d.errorCount *25),
								   buttons: [{text:'OK', action: function() {
								   		window.location = ah_local.wp+'/cart';	
								   }}]});
					}
					else 
						ahtb.alert(d.status+' Please contact webmaster', {width: 450, height: 150});								
	    		},
				error: function(d) {
					ahtb.alert("We're very sorry, had trouble processing your invitation. "+(typeof d == 'string' ? d : ((typeof d == 'object' || Array.isArray(d)) &&
																										   				  typeof d.status != 'undefined' ? d.status : '')), 
								{height: 180, width: 450});
				}
			});
		}

		this.processPurchase = function() {
			if (!agreeToTerms) {
				ahtb.alert('Please read and agree to our terms and conditions.', {width: 500, height: 150});
				return;
			}
			ahtb.alert("Processing order...",
						{height: 140,
						 hideSubmit: true});
			validSeller.am_order.agreed_to_terms = 1;
			controller.DBget({
				query: 'process-agent-order',
				data: {	order: validSeller.am_order,
					   	product: AgentOrderMode.ORDER_AGENT_MATCH,
					   	id: validSeller.id,
					   	coupon: coupon
					   	// portalSpec needed for ORDER_PORTAL, and even then, if left out, will use default values
					  },
				done: function(d) {
					console.log("process-agent-order - "+d.status);
					if (d.status == "Cart made") {
						validSeller.am_order = d.order;
						window.location = ah_local.wp+'/cart';		
					}
					else if (d.status == "Coupon error") {
						validSeller.am_order = d.order;
						ahtb.open({html: "Coupon had "+d.errorCount+" error(s):"+d.error,
								   width: 450,
								   heigth: 110 + (d.errorCount *25),
								   buttons: [{text:'OK', action: function() {
								   		window.location = ah_local.wp+'/cart';	
								   }}]});
					}
					else 
						ahtb.alert(d.status);
	    		},
				error: function(d) {
					ahtb.alert("We're very sorry, had trouble making your cart order.<br/>"+d, {height: 180, width: 450});
				}
			});
		}

		this.tryCompletingTransaction = function() {
			if (!agreeToTerms) {
				ahtb.alert('Please read and agree to our terms and conditions.', {width: 500, height: 150});
				return;
			}
			var amOrder = null;
			for(var i in validSeller.meta) {
				if (validSeller.meta[i].action == SellerMetaFlags.SELLER_AGENT_ORDER) {
					validSeller.am_order = validSeller.meta[i];
					if (coupon.length)
						controller.validateCode(coupon,
												InviteCodeMode.IC_FEATURE_LIFESTYLE,
												controller.processAMInvite);
					else
						controller.processPurchase();
					return;
				}
			}
			ahtb.alert("Failed to find order!");
		}

		this.updateAuthor = function() {
			if (!agreeToTerms) {
				ahtb.alert('Please read and agree to our terms and conditions.', {width: 500, height: 150});
				return;
			}

			var meta = {};
			meta.action = SellerMetaFlags.SELLER_PROFILE_DATA;
			if (saving == false) {
				var san = {};
			 	san.data = this.sanitizeInput(meta);
			 	try {
					if (this.needsToSave(san, meta)) 
						ahtb.loading('Saving your profile...', {
									opened:function(){
										saving = true;
										controller.DBget({	query:'update-author', 
														data: {id: validSeller.id,
															   fields: san.data,
															   fromBasicInfoPage: 1,
															   callMe: callMe}, 
														done:function(d){
															saving=false;
															validSeller = d.seller;
															controller.setupValidSeller();
															var warnIncomplete = validSeller.flags & SellerFlags.SELLER_NEEDS_BASIC_INFO;
															if (!isFree) {
																if ( !warnIncomplete ) {
																	// go purchase it now
																	controller.tryCompletingTransaction();
																	return;
																}
															}
															// seller.initial_listings = validSeller.listings;
															// seller.updateMyListingsButton();
															if (!warnIncomplete) {
																controller.tryCompletingTransaction();
																// window.location = ah_local.wp+'/sellers';
															}
															else {
																var msg = typeof validSeller.photo == 'undefined' || 
																		  validSeller.photo == null ? 	(!isFree ? '<div id="inform-incomplete"><p>Before you can purchase and have this feature activated, you still need to upload a photo of yourself.</p></div>' 
																												 : '<div id="inform-incomplete"><p>Before we can activate this feature, you still need to upload a photo of yourself.</p></div>')
																									:	(!isFree ? '<div id="inform-incomplete"><p>Before you can purchase and have this feature activated, the description of your expertise must be at least '+MIN_LENGTH_DESC+' characters.</p></div>' 
																												 : '<div id="inform-incomplete"><p>Before we can activate this feature, the description of your expertise must be at least '+MIN_LENGTH_DESC+' characters.</p></div>');
																ahtb.open({html: msg,
																		  width: 500,
																		  height: 180});
																return;
															}
														},
														error:function(x) {
															var msg = typeof x == 'string' ? x :
																	  typeof x == 'object' ? JSON.stringify(x) :
																	  "Unknown reason";
															ahtb.alert("Failure saving profile: "+msg);
														}
													});
								}});
					else if (!validSeller.photo)
						ahtb.alert("Please upload a photo of yourself.", {height: 150});
					else 
						controller.tryCompletingTransaction();		
				}
				catch(e) {
					console.log( e.toString() );
					ahtb.alert('Please enter the lifestyle description with at least '+MIN_LENGTH_DESC+' characters.', {height: 180});
				}		
			}
		}

		// this method will only return an empty data, since this page has been simplied greatly
		// leaving it here, just in case we want to expand it again later
		this.sanitizeInput = function(meta){ // gets all the data from the form in the page
			var data = {};
			var vars = ['company','website','phone','mobile','email','about','service_areas','associations','contact_email'];
			for (var i in vars){
				var ele = $('[name='+vars[i]+']');
				if (ele.length == 0)
					continue;
				if (vars[i] == 'phone' || vars[i] == 'mobile') data[vars[i]] = $('[name='+vars[i]+']').val().replace(/[^0-9]/g, '');
				else if (vars[i] == 'contact_email') {
					meta[vars[i]] = ($.trim($('[name='+vars[i]+']').val()).length < 1) ? "" : $('[name='+vars[i]+']').val();
				}
				else if (vars[i] == 'service_areas')
					data[vars[i]] = $('[name='+vars[i]+']').val()+":"+$('select[name="areas"] option').filter(':selected').attr('value');
				else if (vars[i] == 'associations') {
					data[vars[i]] = {};
					$.each($('.associations input'), function() {
						data[vars[i]][$(this).attr('name')] = $(this).prop('checked') ? 1 : 0;
					})
				}
				else data[vars[i]] = $('[name='+vars[i]+']').val();
			}
			if (!showSimplifiedAgentLifestylePages) {
				vars = ['year','sold','inArea'];
				for (var i in vars){
					var ele = $('select[name='+vars[i]+']');
					if (ele.length)
					 	meta[vars[i]] = $('select[name='+vars[i]+'] option').filter(':selected').attr('value');
				}
			}
			return data;
		}

		this.needsToSave = function(san, meta){ // data verification, returns boolean
			var data = san.data;
			newData = {};
			var needsToSave = false;
			var needModMetaSaved = false;;
			var oldData = validSeller;
			// this is entire seller structure or whatever parts were saved in data from sanitizeInput()
			for (var i in data){
				var dataChanged = false;
				if ((typeof oldData[i] == 'undefined' ||
					 typeof oldData[i] == null) &&
					data[i]) {
					needsToSave = true; newData[i] = data[i];  dataChanged = true;
				}
				else if ( data[i] && data[i] != oldData[i] ){
					if (oldData[i]  && typeof data[i] == 'object' && !arrayIsSame(obj2array(data[i]), obj2array(oldData[i])) ) {
						needsToSave = true; newData[i] = data[i]; dataChanged = true;
					} else if (typeof data[i] != 'object' || (oldData[i] == null && length(data[i])) ) {
						needsToSave = true; newData[i] = data[i];  dataChanged = true;
					} else if (!arrayIsSame(data[i], oldData[i]) && length(data[i])) {
						needsToSave = true; newData[i] = data[i];  dataChanged = true;
					}
				}
				if (dataChanged) {
					if (!validSeller.mod_meta)
						validSeller.mod_meta = {action: SellerMetaFlags.SELLER_MODIFIED_PROFILE_DATA,
									'first_name': 0,
									'last_name': 0,
									'company': 0,
									'website': 0,
									'phone': 0,
									'mobile': 0,
									'email': 0 };
					if (typeof validSeller.mod_meta[i] == 'undefined')
						validSeller.mod_meta[i] = 0;
					needModMetaSaved = true;
					validSeller.mod_meta[i]++;							
				}
			}
			

			var needPDMetaSaved = false;
			// var firstNameDiff = (typeof seller.meta == 'undefined' ||
			// 					 seller.meta == null ||
			// 					 typeof seller.meta.first_name == 'undefined' ||
			// 	 				 seller.meta.first_name != meta.first_name);
			// var lastNameDiff  = (typeof seller.meta == 'undefined' ||
			// 					 seller.meta == null ||
			// 					 typeof seller.meta.last_name == 'undefined' ||
			// 	 				 seller.meta.last_name != meta.last_name);

			// NOTE: this page will have showSimplifiedAgentLifestylePages set to true until the time
			//       it is expanded to include all the profile elements in it
			if (!showSimplifiedAgentLifestylePages &&
				(typeof validSeller.profile_meta == 'undefined' ||
				 validSeller.profile_meta == null ||
				 validSeller.profile_meta.year != meta.year ||
				 validSeller.profile_meta.sold != meta.sold ||
				 validSeller.profile_meta.inArea != meta.inArea) ) {// ||
				//validSeller.profile_meta.contact_email != meta.contact_email ||
				//!arrayIsSame(seller.meta.languages, meta.languages) ||
				//firstNameDiff || 
				//lastNameDiff ) {
				// now pass the data over to the seller's info
				var metas = [];
				var havePD = typeof validSeller.profile_meta != 'undefined' &&
							 validSeller.profile_meta != null;
				for (var i in validSeller.meta)
					if (validSeller.meta[i].action != SellerMetaFlags.SELLER_PROFILE_DATA)
						metas[metas.length] = validSeller.meta[i];

				// pass on any existing profile data that this page is not dealing with
				if (havePD && typeof validSeller.profile_meta.first_name != 'undefined')
					meta.first_name = validSeller.profile_meta.first_name;
				if (havePD && typeof validSeller.profile_meta.last_name != 'undefined')
					meta.first_name = validSeller.profile_meta.last_name;
				if (havePD && typeof validSeller.profile_meta.contact_email != 'undefined')
					meta.first_name = validSeller.profile_meta.contact_email;
				if (havePD && typeof validSeller.profile_meta.languages != 'undefined')
					meta.first_name = validSeller.profile_meta.languages;
				metas[metas.length] = meta;
				newData.meta =  metas;
				validSeller.meta = metas; // for next meta check
				needPDMetaSaved = true;
				// if (firstNameDiff) {
				//  	seller.mod_meta['first_name'] = 1;
				//  	needModMetaSaved = true;
				// }
				// if (lastNameDiff) {
				//  	seller.mod_meta['last_name'] = 1;
				//  	needModMetaSaved = true;
				// }
			}

			var metas = [];
			var needAmOrderSaved = false;
			var haveModMeta = false;
			var haveOrderMeta = false;
			var orderMeta = null;
			var haveItem = false;
			var item = null;
			for (var i in validSeller.meta) {
				if (validSeller.meta[i].action == SellerMetaFlags.SELLER_MODIFIED_PROFILE_DATA) {
					haveModMeta = true;
					 if (!needModMetaSaved) 
					 	metas[metas.length] = validSeller.meta[i];
					 else
					 	metas[metas.length] = validSeller.mod_meta;
				}							
				else if (validSeller.meta[i].action == SellerMetaFlags.SELLER_AGENT_ORDER) { 
					haveOrderMeta = true;	
					orderMeta = validSeller.meta[i];	
					for(var j in orderMeta.item) {
						if (orderMeta.item[j].type == AgentOrderMode.ORDER_AGENT_MATCH &&
							(orderMeta.item[j].mode == AgentOrderMode.ORDER_BUYING ||
							 orderMeta.item[j].mode == AgentOrderMode.ORDER_IDLE) ) {
							item = orderMeta.item[j];
							haveItem = true;
							break;
						}
					}										 
														 
				}
				else
					metas[metas.length] = validSeller.meta[i];
			}

			var desc = $('textarea').val();	
			if (desc.length < MIN_LENGTH_DESC)
				throw new Exception("Description is less than "+MIN_LENGTH_DESC+" characters.", "needsToSave:"+(needsToSave ? 'yes' : 'no'));

			// var mask = 0;
			// if ( !(validSeller.flags & SellerFlags.SELLER_IS_PREMIUM_LEVEL_2) )
			// 	mask = SellerFlags.SELLER_IS_PREMIUM_LEVEL_2;
			// if ( !(validSeller.flags & SellerFlags.SELLER_NEEDS_BASIC_INFO) )
			// 	mask |= SellerFlags.SELLER_NEEDS_BASIC_INFO;

			// san.data.flags = validSeller.flags | mask;

			if (!haveOrderMeta) { // 1: not haveItem, and no meta so use current tag, city and desc
							 	  // 2: haveItem, and not meta so see if item and current tag,city and desc is the same
				needAmOrderSaved = true;
				if (!haveItem) { //make a new one
					item = {
						mode: AgentOrderMode.ORDER_BUYING,
						type: AgentOrderMode.ORDER_AGENT_MATCH,
						desc: desc,
						location: testCityId,
						locationStr: testCityName,
						specialty: tag,
						specialtyStr: amTagList[tag].tag,
						order_id: 0,
						subscriptionType: ProductType.MONTHLY_SUBSCRIPTION,
						priceLevel: PricingLevels.GRADE_6
					}
				}
				else { // update existing item if needed
					if (item.specialty != tag) {
						item.specialty = tag;
						item.specialtyStr = amTagList[tag].tag;
					}
					if (item.location != testCityId) {
						item.location = testCityId;
						item.locationStr = testCityName;
					}
					if (item.desc != desc)
						item.desc = desc;
				}	
				orderMeta = {
							action: SellerMetaFlags.SELLER_AGENT_ORDER,
							order_id_last_purchased: 0,
							order_id_last_cancelled: 0,
							agreed_to_terms: $('input#agreeToTerms').prop('checked') ? 1 : 0,
							item: [item]	
							}		 
			}
			else {				  // 4: not haveItem, have meta, check against current tag,city and desc.  NOTE: should never occur...
								  // 3: haveItem, have meta and current data
				// if (!haveItem) {
				// 	ahtb.alert("Invalid condition, not have original order, but we have one now during validation", {width: 450, height: 180});
				// 	return false;
				// }
				var testItem = null;
				var orderItems = [];
				orderMeta = JSON.parse(JSON.stringify(orderMeta)); // makes copy of it
				for(var i in orderMeta.item) {
					if (orderMeta.item[i].type == AgentOrderMode.ORDER_AGENT_MATCH &&
						// ((orderMeta.item[i].mode == AgentOrderMode.ORDER_BOUGHT &&
						//   orderMeta.item[i].order_id == DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM) ||
						 ((orderMeta.item[i].mode == AgentOrderMode.ORDER_BUYING ||
						   orderMeta.item[i].mode == AgentOrderMode.ORDER_IDLE) &&
						  (orderMeta.item[i].order_id == 0 ||
						   orderMeta.item[i].order_id == DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM)) )  
						// orderMeta.item[i].mode == (isFree ? AgentOrderMode.ORDER_BOUGHT : AgentOrderMode.ORDER_BUYING) &&
						// orderMeta.item[i].order_id == (isFree ? DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM : 0)) 
						testItem = orderMeta.item[i]; // so at this point, testItem and item (from PHP) should be the same
					else
						orderItems[orderItems.length] = orderMeta.item[i]; //keep track of any other ones in order
				}

				if (testItem) {
					// use item instead of testItem, must be the same at this point
					if (testItem.specialty != tag) {
						testItem.specialty = tag;
						testItem.specialtyStr = amTagList[tag].tag;
						needAmOrderSaved = true;
					}
					if (testItem.location != testCityId) {
						testItem.location = testCityId;
						testItem.locationStr = testCityName;
						needAmOrderSaved = true;
					}
					if (testItem.desc != desc) {
						testItem.desc = desc;
						needAmOrderSaved = true;
					}
					if (testItem.mode == AgentOrderMode.ORDER_IDLE)
						needAmOrderSaved = true;
					testItem.mode = AgentOrderMode.ORDER_BUYING;

					if (testItem.order_id != 0)
						needAmOrderSaved = true;
					testItem.order_id = 0;

					orderItems[orderItems.length] = testItem;
				}
				else {
					item = {
						mode: AgentOrderMode.ORDER_BUYING,
						type: AgentOrderMode.ORDER_AGENT_MATCH,
						desc: desc,
						location: testCityId,
						locationStr: testCityName,
						specialty: tag,
						specialtyStr: amTagList[tag].tag,
						order_id: 0,
						subscriptionType: ProductType.MONTHLY_SUBSCRIPTION,
						priceLevel: PricingLevels.GRADE_6
					}
					orderItems[orderItems.length] = item;
					needAmOrderSaved = true;
				}
				orderMeta.item = orderItems;
				var terms = $('input#agreeToTerms').prop('checked') ? 1 : 0;
				if (orderMeta.agreed_to_terms != terms) {
					orderMeta.agreed_to_terms = terms;
					needAmOrderSaved = true;
					console.log("order's agreed_to_terms set to "+(terms ? 'true' : 'false'));
				}
			}

			// save it back always, flag will determine need to save it.
			metas[metas.length] = orderMeta;

			if (!haveModMeta) {
				metas[metas.length] = validSeller.mod_meta;
			}

			if (!haveModMeta || needAmOrderSaved || needPDMetaSaved || needModMetaSaved) {
				newData.meta = metas;
				needsToSave = true;
			}

			san.data = newData;

			return needsToSave; // boolean
		}

		this.DBget = function(d){
			if(typeof d.query !== 'undefined'){
				var h = [];
				if (typeof d.data === 'undefined') h.headers = {query:d.query}; else h.headers = {query:d.query,data:d.data};
				if (typeof d.done === 'undefined') h.done = function(){}; else h.done = d.done;
				// if (typeof d.fail === 'undefined') h.fail = function(){}; else h.fail = d.fail;
				if (typeof d.error !== 'undefined') h.error = d.error;
				if (typeof d.before === 'undefined') h.before = function(){}; else h.before = d.before;
				saving = true;
				$.post(ah_local.tp+'/_sellers/_ajax.php', h.headers, function(){h.before()},'json')
					.done(function(x){
						saving = false;
						if (x.status == 'OK') h.done(x.data);
						else if (h.error) h.error(x.data);
						else ahtb.close(function(){ahtb.alert(x.data, 'Database Error')});
					})
					.fail(function(x){
						saving = false;
						if (h.fail) h.fail(x);
						if (h.error) h.error(x);
						else ahtb.alert("Database failure, please retry.", {height: 150});
					});
			} else ahtb.alert('Query was undefined');
		}
	}

	controller.init();
})