function portalDetail() {
	var msg = 	'Lifestyledlistings.com wants you to <strong>capture leads and receive referral fees</strong> by using the PORTAL feature of our website.  ' +
				'This user friendly tool allows you to <strong>attract and manage interested buyers</strong>.  '+
				'Once a buyer enters the site through your PORTAL <strong>they will remain tied to you*.' +
				'</strong>  The benefit for you is you get a highly developed, lifestyle site that captures leads whom will remain with you <strong>' +
				'whether you refer them out or close an escrow.</strong><br/><br/>' +
				'Call us, and our specialists can help you work your social media sites to show your portal URL to your clients!' +
				'<span style="font-size:.9em;width:90%;text-align:center;display:block;margin-top:35px">*Disclaimer: Lifestyled Listings is legally required ' +
				'to give users the ability to detach from your PORTAL if they choose to.</span>';

	var portal_details = 
		'<div id="portal-content" >' +
			'<div class="maincontent">' +
				'<div id="headers">' +
					'<span id="title1">Welcome to Your Portal</span>' +
					'<span id="title2">Capture Leads &amp Collect Referral Fees</span>' +
				'</div>' +
				'<div id="define-portal">' +
					'<span id="title3">Apply for our BETA test program and get your own PORTAL today.</span>' +
					'<span id="message">'+msg+'</span>' +
					'<span style="text-align:center;display:block;margin:.5em 0"><label id="base-url" style="letter-spacing:.05em;font-weight:400;font-size:1.1em;color:#333"></label><input id="portal" type="text" placeholder="Your name..." style="margin-left:4px;padding:3px;margin-right:10px" /><span id="marker" class="entypo-check"></span><button id="check-it">Check it</button></span>' +
				'</div>'  +
			'</div>' +
			'<button id="done">OK</button>' +
		'</div>';

	ahtb.open({
		html: portal_details,
		width: 1000,
		height: 675,
		hideSubmit: true,
		hideTitle: true,
		opened: function() {
			$('#done').on('click', function() { ahtb.close(); })

			$('#define-portal #portal').val(testPortal);
			$('#marker').css('color', 'lightgreen');
			$('#marker').hide();
			$('#base-url').html('LifestyledListings.com/');
			$('#define-portal #portal').keyup(function(e){
				e.preventDefault();
				$('#marker').hide();
			})
			$('#done').on('click', function(e) {
				e.preventDefault();
				ahtb.close();
			})
			$('#define-portal #check-it').on('click', function(e) {
				e.preventDefault();
				var val = $('#define-portal #portal').val();
				if (val.length == 0) {
					ahtb.push();
					ahtb.alert("Please enter a portal name.", 
								{height: 170,
								buttons: [{text: "OK", action: function() {
									ahtb.pop();
								}}]});
					return;
				}
				ajax({
					query: 'check-portal-name',
					data: val,
					done: function(d) {
		    			$('#marker').show();
		    			testPortal = d;
		    			$('#portal').val(d);
		    			// ahtb.push();
					//	ahtb.alert("Congratulations, this portal name, "+testPortal+", is available!", 
					//				{height: 180, 
					//				width: 450,
					//				buttons: [{text: "OK", action: function() {
					//					ahtb.close();
					//				}}]})
					},
					error: function(d) {
						ahtb.push();
						ahtb.alert("The portal name "+val+" is already in use.</br>Please try a different name.", 
									{height: 180, 
									 width: 450,
									 buttons: [{text: "OK", action: function() {
										ahtb.pop();
									}}]});
						testPortal = '';
						$('#portal').val('');
						$('#marker').hide();
					}
				});
			});
		}
	})
}

function ajax(d){
	if(typeof d.query !== 'undefined'){
		var h = [];
		if (typeof d.data === 'undefined') h.headers = {query:d.query}; else h.headers = {query:d.query,data:d.data};
		if (typeof d.done === 'undefined') h.done = function(){}; else h.done = d.done;
		// if (typeof d.fail === 'undefined') h.fail = function(){}; else h.fail = d.fail;
		if (typeof d.error !== 'undefined') h.error = d.error;
		if (typeof d.before === 'undefined') h.before = function(){}; else h.before = d.before;
		$.post(ah_local.tp+'/_sellers/_ajax.php', h.headers, function(){h.before()},'json')
			.done(function(x){
				if (x.status == 'OK') h.done(x.data);
				else if (h.error) h.error(x.data);
				else ahtb.close(function(){ahtb.alert(x.data, 'Database Error')});
			})
			.fail(function(x){
				if (h.fail) h.fail(x);
				else ahtb.alert("Database failure, please retry.", {height: 150});
			});
	} else ahtb.alert('Query was undefined');
}

jQuery(document).ready(function($){
	console.log("entered agent-reservation.js");
	reserve = new function() {
		var cities = [];
		//var cityName = '';
		for (var i in ah_local.cities)
		    cities[i] = {label: ah_local.cities[i].label, value: ah_local.cities[i].label, id: ah_local.cities[i].id};

		this.init = function() {
			$('[name=mobile], [name=phone]').mask('(999) 000-0000');
			this.setupAutocomplete($('#information #user-data .service_areas'));

			if (service_area.length)
				testCityName = service_area;

			$('#information #user-data .service_areas').val(testCityName);
			if (testAgentMatchList.item.length) {
				for(var index in testAgentMatchList.item) {
					var tagIndex = 0;
					for(var tIndex in amTagList)
						if (amTagList[tIndex].id == testAgentMatchList.item[index].specialty) {
							$('#information #lifestyles select[for="'+index+'"] option[value="'+tIndex+'"]').prop('selected', true);
							break;
						}				
				}
			}
			$('a#checkPortal').on('click', function() {
				portalDetail();
				console.log("After portalDetail");
			})

			$('button#register').on('click', function() {
				window.location = ah_local.wp+'/register';
			})

			$('button#more-info').hide();

			$('button#reserve').on('click', function() {
				reservePortal = $('#option #portal').prop('checked');
				if (reservePortal &&
					testPortal.length == 0) {
					ahtb.alert('Please click <a href="javascript:portalDetail();">here</a> and see if your desired portal name is available.  ', {height: 150});
					return;
				}

				var first = $('#user-data #first_name').val();
				var last = $('#user-data #last_name').val();
				var phone = $('#user-data #phone').val().replace(/[^0-9]/g, '');
				var email = $('#user-data #email').val();
				var msg = '';
				if (first.length == 0)
					msg += "first name";
				if (last.length == 0)
					msg += (msg.length ? ", ": "") + "last name";
				if (phone.length == 0)
					msg += (msg.length ? ", ": "") + "phone";
				if (email.length == 0)
					msg += (msg.length ? " and ": "") + "email";

				if (msg.length) {
					msg = "Please enter "+msg+" so we can contact you.";
					ahtb.alert(msg, {height: 170});
					return;
				}
				
			 	var city = $('#information #user-data .service_areas').attr('city_id');
			 	testCityId = city? parseInt(city) : 0;
			 	if (!testCityId ||
			 		 testCityName.length == 0) {
			 		testCityName = $('#information #user-data .service_areas').val();
			 		if (testCityName.length == 0) {
			 			msg = "Please pick your city.";
						ahtb.alert(msg, {height: 170});
						return;
			 		}
			 		else {
			 			reserve.getCityId(testCityId, testCityName);
			 			return;
			 		}
			 	}

			 	reserve.reallyReserve();		 	
			})
		}

		this.reallyReserve = function() {
			var first = $('#user-data #first_name').val();
			var last = $('#user-data #last_name').val();
			var phone = $('#user-data #phone').val().replace(/[^0-9]/g, '');
			var email = $('#user-data #email').val();

			testCityId = $('#information #user-data .service_areas').attr('city_id');
			testCityName = $('#information #user-data .service_areas').attr('city_name');
		 	//var itemList = {};
		 	testAgentMatchList.item.splice(0, testAgentMatchList.item.length); // remove any items
		 	//itemList.action = AgentOrderMode.ORDER_AGENT_MATCH;
		 	//itemList.item = [];
		 	$('#information #lifestyles select').each(function() {
		 		var which = $(this).attr('for');
				var val = $(this,'option:selected').val();
				if (val != "0") {
					var item = {};
			 		item.location = testCityId;
			 		item.locationStr = testCityName;
			 		item.specialty = amTagList[val].id;
			 		item.specialtyStr = amTagList[val].tag;
			 		testAgentMatchList.item[testAgentMatchList.item.length] = item;
			 		console.log("AM specialty is "+val+' for:'+which);
			 	}
		 	})

		 	if (testAgentMatchList.item.length == 0) {
		 		ahtb.alert("Please pick at least one lifestyle.", {height: 150});
		 		return;
		 	}

		 	ahtb.open({html: "Reserving..", 
			   			hideSubmit: true});
		 	try{
			    __adroll.record_user({"adroll_segments": "c0156c72"})    
			} catch(err) {
				if (typeof err == 'object' && typeof err != null && typeof err.message != 'undefined')
					console.log("Adroll exception - "+err.message);
				else
					console.log("Adroll exception of some sort");
				// ahtb.alert("AdRoll exception!", {height: 150});
				// return;
			}

			ajax({
				query: 'reserve-agent-product',
				data: {id: seller.id,
					   what: SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | (reservePortal ? SellerFlags.SELLER_IS_PREMIUM_LEVEL_1 : 0),
					   portal: testPortal,
					   lifestyles: testAgentMatchList,
					   first: first,
					   last: last,
					   phone: phone,
					   email: email },
				done: function(d) {
					console.log("reserve-agent-product - "+d);
					var msg = "Your LifeStyled&#8482; Agent reservation "+(reservePortal ? "and portal("+testPortal+")" : '');
					msg += ", is now reserved.  Please expect a call or an email from us soon.  Please contact us via our Feedback system!";
					var isAOL = false;
					ahtb.close();
					if (email.length &&
						email.toLowerCase().indexOf('aol') != -1) {
						isAOL = true;
						msg +=  "<br/><br/>AOL users, please put support@lifetyledlistings.com to your whitelist, otherwise AOL may bounce any email from us.";
					}
					var height = reservePortal ? (isAOL ? 300 : 240) : (isAOL ? 270 : 210);
					window.setTimeout(function() {
						ahtb.alert(msg,
									{height: height,
									 width: 600});	
					}, 250);					
	    		},
				error: function(d) {
					console.log("We're very sorry, had trouble reserving your portal. "+d);
					ahtb.close();
					window.setTimeout(function() {
						ahtb.alert("We're very sorry, had trouble reserving your portal. "+d, {height: 180, width: 450});
					}, 250);
				}
			});
		}

		this.pickedCity = function(id, city_id, city) {
			$('#information #user-data .service_areas').attr('city_id', city_id); 
		    $('#information #user-data .service_areas').attr('city_name', city); 
			$('#information #user-data .service_areas').val(city);
			if (ahtb.opened)
				ahtb.close();

			window.setTimeout(function() {
				reserve.reallyReserve();
			}, 250);
		}

		this.getCityId = function(id, city) {
			$.post(ah_local.tp+'/_pages/ajax-quiz.php',
                { query:'find-city',
                  data: { city: city,
                  		  distance: 0 }
                },
                function(){}, 'JSON')
				.done(function(d){
					if (d &&
                        d.data &&
                        d.data.city &&
                        (typeof d.data.city == 'object' ||
                        d.data.city.length) ){
	                    if (d.status == 'OK' &&
	                        (typeof d.data.city == 'object' || d.data.city.length) ) {							                        
	                        console.log(JSON.stringify(d.data));

	                    	if (d.data.allState == 'true' || d.data.allState == true || d.data.allState == 1) {// doing statewide
		                        ahtb.alert("Please enter a city name along with the state.", {height: 150});
		                        return;
		                    }

		                    var len = length(d.data.city); //(typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
	                    	// var len = (typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
							if (len > 1) {
								if (d.data.allState == 'false' || d.data.allState == false) {
			                        var h = '<p>Please pick one of the cities:</p>' +
			                                  '<ul class="cities">';
			                        for(var i in d.data.city) {
			                        	var location = d.data.city[i].city+', '+d.data.city[i].state;
			                          	h += '<li><a href="javascript:reserve.pickedCity('+1+','+d.data.city[i].id+",'"+location+"'"+');" >'+location+'</a></li>';
			                        }
			                        h += '</ul>';
			                        ahtb.open(
			                          { title: 'Cities List',
			                            width: 380,
			                            height: (150 + (len * 25)) < 680 ? (150 + (len * 25)) : 680,
			                            html: h,
			                            buttons: [
			                                  {text: 'Cancel', action: function() {
			                                    ahtb.close();
			                                  }}],
			                            opened: function() {
			                            }
			                          })
			                    }
			                    else {
		                          	ahtb.alert("Only one city/state combination accepted.", {height: 150});
		                        }
		                    } // only one!
		                    else {
		                    	reserve.pickedCity(0, d.data.city[0].id, d.data.city[0].city+', '+d.data.city[0].state);
		                    }
	                    }
	                    else {
	                      ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                      			{title: 'City Match', 
	                      			 height:180, 
	                      			 width:600});
	                    }
	                }
	                else {
	                    ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                      			{title: 'City Match', 
	                      			 height:180, 
	                      			 width:600});
	                }
                })
                .fail(function(d){
                	ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                      			{title: 'City Match', 
	                      			 height:180, 
	                      			 width:600});
                });
		}

		this.setupAutocomplete = function(element) {
			element.autocomplete({
		    source: cities,
		    select: function(e, ui){ 
		      if (ui.item.value != null) {
		        element.attr('city_id', ui.item.id); 
		        element.attr('city_name', ui.item.label); 
		        testCityName = ui.item.label;
		        element.change();
		      }
		    }
		  }).on('focus',function(){
		    if ($(this).val() == 'ENTER CITY') {
		      $(this).val(''); $(this).removeClass('inactive');
		    }
		  }).on('keypress', function(e){
		    var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
		    if(keynum == 13) {//13 is the enter char code
		      e.preventDefault();
		    }
		    $(this).attr('city_id', 0);
		    $(this).attr('city_name', '');	
		  });
		}
	}

	reserve.init();
})