var quizProfile = null;
var waitedForFirstDirective = false;
// var quiz_id = -1;

console.log("page-quiz-results: entered at "+getTime());

var isOnIOS = navigator.userAgent.match(/iPad/i)|| navigator.userAgent.match(/iPhone/i);
var eventName = isOnIOS ? "pagehide" : "beforeunload";
window.addEventListener(eventName, function (e) { 
	if (quizProfile.requestingPartialResults) {
		theData = { //sessionID: sessionToUse, // this will be added by this.DB()
					quizId: quiz_id }; // should be -1 or actual row index of QuizActivity
		quizProfile.DB({
			query: 'stop-partial-run',
			data: theData,
			error: function(d){},
			done: function(d){}
		});
	}
});

function initialize(){
	if (quizProfile == null) {
		setTimeout(initialize,
				  50);
		return;
	}
	console.log("page-quiz-results: quizProfile ready at "+getTime());
	quizProfile.getReady();
}
function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&callback=initialize';
  if (window.navigator.onLine) document.body.appendChild(script);
  else setTimeout(function(){ initialize(); }, 50);
}
window.onload = initialize();

var ListingFlag = {
	LISTING_PERMIT_ADDRESS: 1
}
// price slider
var theRange = [ noAskAllowMinPrice, lastPrice ];
var priceOptionmin = {
	800000: "Min Price",
	800001: "$800k",
	1000000: "$1 Million",
	1200000: "$1.2 Million",
	1400000: "$1.4 Million",
	1600000: "$1.6 Million",
	1800000: "$1.8 Million",
	2000000: "$2 Million",
	2500000: "$2.5 Million",
	3000000: "$3 Million",
	3500000: "$3.5 Million",
	4000000: "$4 Million",
	4500000: "$4.5 Million",
	5000000: "$5 Million",
	6000000: "$6 Million",
	7000000: "$7 Million",
	8000000: "$8 Million",
	9000000: "$9 Million",
	10000000: "$10 Million",
	12000000: "$12 Million",
	14000000: "$14 Million",
	16000000: "$16 Million",
};	
var priceOptionmax = {
	1000000: "$1 Million",
	1200000: "$1.2 Million",
	1400000: "$1.4 Million",
	1600000: "$1.6 Million",
	1800000: "$1.8 Million",
	2000000: "$2 Million",
	2500000: "$2.5 Million",
	3000000: "$3 Million",
	3500000: "$3.5 Million",
	4000000: "$4 Million",
	4500000: "$4.5 Million",
	5000000: "$5 Million",
	6000000: "$6 Million",
	7000000: "$7 Million",
	8000000: "$8 Million",
	9000000: "$9 Million",
	10000000: "$10 Million",
	12000000: "$12 Million",
	14000000: "$14 Million",
	16000000: "$16 Million",
	19999999: "$20 Million",
	20000000: "Max Price",
};	

var homeOptions = { beds: 0,
					baths: 0
				  };

var reverseListingKeyMap = {"p":  "price",
							"br": "beds",
							"b":  "baths",
							'i':  "id",
							'ci': "city_id",
							'pt': "percent",
							'pa': "percent_average",
							'ip': "image_path",
							'sa': "street_address",
							't':  "title",
							'c':  "country",
							'in': "interior",
							'ins':"interior_std",
							'l':  "lotsize",
							'ls': "lotsize_std",
							'f':  "flags",
							'a':  "active",
							'fi': "first_image",
							'li': "list_image",
							"lg": "lng",
							"lt": "lat",
							"tg": "tags"};

ListingMapMode = {
	DID_PRIMARY_LISTING_KEY_MAPPING: 1,
	DID_SECONDARY_LISTING_KEY_MAPPING: 2
};

function delaylistingscheck() {
	if (quizProfile.city_sorted.length)
	 	$('.view-selector .backtocity').show();
	else
		$('.view-selector .backtocity').hide();
	$('.view-selector .backtolistings').hide();
}

jQuery(document).ready(function($){

	$(window).on('resize', function(e) {
		var previous = isMobile;
		isMobile = $('section.ismobile-identifier').css('display') != 'none';
		// ahtb.alert("orientation change, isMobile:"+(isMobile ? 'yes' : 'no')+", isRetina:"+(isRetina ? 'yes' : 'no'));
		if (previous != isMobile) {
			var enableControls = isMobile ? '.mobile-nav' : '.right-col';
			var disableControls = isMobile ? '.right-col' : '.mobile-nav';
			$(enableControls).prop('disabled', false);
			$(disableControls).prop('disabled', true);
		}
	})

	quizProfile = new function(){
		this.scrollData = {city: {	pageNumber: 0,
									scrollTop: 0,
								  	scrollPage: 0},
						   listings:{pageNumber: 0,
						   			scrollTop: 0,
								  	scrollPage: 0}};
		// this.scrollTop = 0;
		// this.scrollPage = 0;
		this.doingQuery = 0;
		// this.profiles = null;
		this.justOpened = true;
		this.profileName = '';
		this.showHaveExcessQuizResults = true;
		this.myTagList = ''; // string list of tags from quiz to be passed into a cookie before changing to listing page
		this.primary = 'city';
		this.activeCityId = 0;
		this.maxTagsToShow = 8;
		this.doingSwitchToListing = false;
		this.maxTagsToShowListingView = 5;
		this.requestingPartialResults = false;
		this.ignorePartialResults = true;
		this.attemptSwitchToListing = 0;
		this.pleaseHoldTimer = 0;
		// this.goingToListing = false;

		this.defaults = {
			tag_category: 'activities', // default category to open when opening tag window
			// listings_per_page: 3, // listing per result to load at a time
			// listings_for_first_page: 7, // # of listings to load on page load
			perPage: {city: {	per_page: 3, // listing per result to load at a time
								for_first_page: 7, // # of listings to load on page load
							},
					  listings:{per_page: 3, // city per result to load at a time
								for_first_page: 7, // # of cities to load on page load
							}
					 },
			iconRowCount: 4, // # of icons to show per row 
			iconRowCountCity: 3, // # of icons to show per row 
			cellHeight: 43,
			actionButtonDiv: 60
		}

		this.resetCookies = function() {
			console.log("resetCookies entered");
			setCookie("QuizListingPos", 0, 2);
			setCookie('QuizListingTop', 0, 2);
			setCookie("QuizCityPos", 0, 2);
			setCookie('QuizCityTop', 0, 2);
			setCookie('ActiveCityId', 0, 2);
			setCookie("QuizNeedSaveProfile", 1, 2);
			setCookie("LastViewedProfileIndex", -1, 2);
			setCookie("PrimaryList", '', 1);
			//ah_local.sessionID = getCookie("PHPSESSID"); // restore sessionID, which could have been changed by a search profile
		}

		this.fixMinPriceRange = function() {
			if (typeof priceList != 'undefined' &&
				priceList != null)
				priceOptionmin = priceList;
		}
		
		this.fixMaxPriceRange = function() {
			if (typeof priceListMax != 'undefined' &&
				priceListMax != null)
				priceOptionmax = priceListMax;
		}
		
		this.setMin = function(val) {
				self = quizProfile;
				val = parseInt(val);
				console.log("MIN selected: "+val);
				var newTop = self.query.price[1] == -1 ? 20000000 : self.query.price[1];
				if (val >= newTop) {
					if (self.query.price[1] == theRange[1] ||
						self.query.price[1] == -1) {
						val = 20000000;
						newTop = -1;
					}
					else {
						var diff = val < 800000 ? 50000 : (val < 2000000 ? 200000 : (val < 5000000 ? 500000 : (val < 10000000 ? 1000000 : 2000000)));
						newTop = val+diff;
					}
				}
				else
					newTop = self.query.price[1] >= theRange[1] ? -1 : self.query.price[1];
				la( AnalyticsType.ANALYTICS_TYPE_CLICK,
          			'SetMin', 
          			val);
				ga('send', {
				  'hitType': 'event',          // Required.
				  'eventCategory': 'button',   // Required.
				  'eventAction': 'click',      // Required.
				  'eventLabel': 'SetMin'
				});
				self.get.results({ price: [val, newTop]  }, false);
		}
			
		this.setMax = function(val) {
			  self = quizProfile;
				val = parseInt(val);
				console.log("MAX selected: "+val);
				var newTop = val == theRange[1] ? -1 : (val == (lastPrice-1) ? lastPrice : val);
				if (val <= self.query.price[0])
					if (self.query.price[0] == theRange[0]) {
						newTop = theRange[0] + 100000;
					}
					else {
						var diff = val < 800000 ? 50000 : (val < 2000000 ? 200000 : (val < 5000000 ? 500000 : (val < 10000000 ? 1000000 : 2000000)));
						self.query.price[0] = newTop-diff;
					}
				la( AnalyticsType.ANALYTICS_TYPE_CLICK,
          			'SetMax', 
          			newTop);
				ga('send', {
				  'hitType': 'event',          // Required.
				  'eventCategory': 'button',   // Required.
				  'eventAction': 'click',      // Required.
				  'eventLabel': 'SetMax'
				});
				self.get.results({ price: [self.query.price[0], newTop]  }, false);
		}

		this.getReady = function() {
			if (!waitedForFirstDirective) {
				// ahtb.alert("Initalizing...", {width: 400, height: 150});
			    window.setTimeout(function() {
			      quizProfile.waitForDirective();
			    }, 500);
			    waitedForFirstDirective = true;
			 }
		}

		this.waitForDirective = function() {
		  	$('button').prop('disabled', gettingDirective);
		  	if (gettingDirective) {
			    window.setTimeout(function() {
			      quizProfile.waitForDirective();
			      // console.log("wating for directive");
			    }, 1000);
			    return;
			}
			else {
				console.log("Got directive, sessionID:"+ah_local.sessionID+", activeSessionID:"+ah_local.activeSessionID);
				quizProfile.init();
			}
		}
		
		this.init = function(){
			$('.quiz-tags .location-mod').hide();
			$('.results-error-overlay').hide();
			$('span#loading').css(isMobile ? 'top' : 'bottom', '0');

			this.cityPageNumber = 0;
			this.listingPageNumber = 0;

			this.fixMinPriceRange();
			this.fixMaxPriceRange();

			var options = '';
			for(var index in priceOptionmin) {
				options += "<li value='"+index+"'>"+priceOptionmin[index]+'</li>';
			}
			var options2 = '';
			for(var index in priceOptionmax) {
				options2 += "<li value='"+index+"'>"+priceOptionmax[index]+'</li>';
			}
			var options3 = '';
			for(var index in priceOptionmin) {
				options3 += "<option value='"+index+"'>"+priceOptionmin[index]+'</option>';
			}
			var options4 = '';
			for(var index in priceOptionmax) {
				options4 += "<option value='"+index+"'>"+priceOptionmax[index]+'</option>';
			}
			var options5 = '';
			for(var index in BedsOptions) {
				// options5 += "<li value='"+index+"'>"+BedsOptions[index]+'</li>';
				options5 += "<li value='"+BedsOptions[index].value+"'>"+BedsOptions[index].text+'</li>';
			}
			var options6 = '';
			for(var index in BathsOptions) {
				options6 += "<li value='"+BathsOptions[index].value+"'>"+BathsOptions[index].text+'</li>';
			}
			var options7 = '';
			for(var index in BedsOptions) {
				var suffix = index == 1 ? "&nbsp;bed" : "&nbsp;beds";
				// options7 += "<option value='"+index+"'>"+BedsOptions[index]+suffix+'</option>';
				options7 += "<option value='"+BedsOptions[index].value+"'>"+BedsOptions[index].text+suffix+'</option>';
			}
			var options8 = '';
			for(var index in BathsOptions) {
				var suffix = index == 1 ? "&nbsp;bath" : "&nbsp;baths";
				options8 += "<option value='"+BathsOptions[index].value+"'>"+BathsOptions[index].text+suffix+'</option>';
			}
			
			$('.selectBoxmin ul#min').html(options);
			$('.selectBoxmin ul#min').find('li').click(function(){
				$('.selectBoxmin ul#min li[value]').removeClass('active');
				var val = $(this).attr('value');
				$(this).parent().css('display','none');
				$(this).addClass('active');
				$(this).closest('.selectBoxmin').attr('value',val);
				$(this).parent().siblings('.selected').html($(this).html());
				quizProfile.setMin(val);
			});
			$('.selectBoxmax ul#max').html(options2);
			$('.selectBoxmax ul#max').find('li').click(function(){
				$('.selectBoxmax ul#max li[value]').removeClass('active');
				var val = $(this).attr('value');
				$(this).parent().css('display','none');
				$(this).addClass('active');
				$(this).closest('.selectBoxmax').attr('value',val);
				$(this).parent().siblings('.selected').html($(this).html());
				quizProfile.setMax(val);
			});
			$('select#min').html(options3);
			$('select#min').change(function(e){
				e.preventDefault();
				self = quizProfile;
				var val = parseInt($(this).val());
				console.log("MIN selected: "+val);
				var newTop = self.query.price[1];
				if (val >= self.query.price[1]) {
					if (self.query.price[1] == theRange[1] ||
						self.query.price[1] == -1) {
						val = 20000000;
						newTop = -1;
					}
					else {
						var diff = val < 800000 ? 50000 : (val < 2000000 ? 200000 : (val < 5000000 ? 500000 : (val < 10000000 ? 1000000 : 2000000)));
						newTop = val+diff;
					}
				}
				else
					newTop = self.query.price[1] >= theRange[1] ? -1 : self.query.price[1];
				la( AnalyticsType.ANALYTICS_TYPE_CLICK,
          			'SetMin', 
          			val);
				ga('send', {
				  'hitType': 'event',          // Required.
				  'eventCategory': 'button',   // Required.
				  'eventAction': 'click',      // Required.
				  'eventLabel': 'SetMin'
				});
				$('.option .dropdown').hide();
				self.get.results({ price: [val, newTop]  }, false);
			});
			$('select#max').html(options4);
			$('select#max').change(function(e){
				e.preventDefault();
				self = quizProfile;
				var val = parseInt($(this).val());
				console.log("MAX selected: "+val);
				var newTop = val == theRange[1] ? -1 : val;
				if (val <= self.query.price[0])
					if (self.query.price[0] == theRange[0]) {
						newTop = theRange[0] + 200000;
					}
					else {
						var diff = val < 800000 ? 50000 : (val < 2000000 ? 200000 : (val < 5000000 ? 500000 : (val < 10000000 ? 1000000 : 2000000)));
						self.query.price[0] = newTop-diff;
					}
				la( AnalyticsType.ANALYTICS_TYPE_CLICK,
          			'SetMax', 
          			newTop);
				ga('send', {
				  'hitType': 'event',          // Required.
				  'eventCategory': 'button',   // Required.
				  'eventAction': 'click',      // Required.
				  'eventLabel': 'SetMax'
				});
				$('.option .dropdown').hide();
				self.get.results({ price: [self.query.price[0], newTop]  }, false);
			});

			$('.selectBoxmin').each(function(){
				$(this).children('.selected,span.selectArrow').click(function(event){
					event.stopPropagation();
					if($(this).parent().children('ul#min').css('display') == 'none'){
						$(this).parent().children('ul#min').css('display','block');
						$('ul#max').css('display','none');
						$('.selectBoxmin').addClass('active');
						$('.selectBoxmax').removeClass('active');
						$('.selectBed').removeClass('active');
						$('.selectBath').removeClass('active');
						$('.selectBoxmax ul').hide();
						$('.selectBed ul').hide();
						$('.selectBath ul').hide();
					}
					else {
						$(this).parent().children('ul#min').css('display','none');
						$('.selectBoxmin').removeClass('active');
					}
				});
    		});
			$('.selectBoxmax').each(function(){
				$(this).children('.selected,span.selectArrow').click(function(event){
					event.stopPropagation();
					if($(this).parent().children('ul#max').css('display') == 'none'){
						$(this).parent().children('ul#max').css('display','block');
						$('ul#min').css('display','none');
						$('.selectBoxmax').addClass('active');
						$('.selectBoxmin').removeClass('active');
						$('.selectBed').removeClass('active');
						$('.selectBath').removeClass('active');
						$('.selectBoxmin ul').hide();
						$('.selectBed ul').hide();
						$('.selectBath ul').hide();
					}
					else {
						$(this).parent().children('ul#max').css('display','none');
						$('.selectBoxmax').removeClass('active');
					}
				});
    		});
			$(document).click( function(){
	        	$('ul#min').css('display','none');
				$('ul#max').css('display','none');
				$('.selectBoxmin').removeClass('active');
				$('.selectBoxmax').removeClass('active');
				$('.selectBed').removeClass('active');
				$('.selectBath').removeClass('active');
				$('.selectBoxmax ul').hide();
				$('.selectBed ul').hide();
				$('.selectBath ul').hide();
				$('.custom-menu').hide();
	    	});

	    	$('.selectBed').attr('value', 0); // default value - ALL
			$('.selectBed ul').html(options5);
			$('.selectBed ul').find('li').click(function(){
				$('.selectBed ul li[value]').removeClass('active');
				var val = $(this).attr('value');
				$(this).parent().css('display','none');
				$(this).addClass('active');
				$(this).closest('.selectBed').attr('value',val);
				var beds = '&nbsp;'+(val != 1 ? 'beds' : 'bed');
				$(this).parent().siblings('.selected').html($(this).html()+beds);
				console.log("Bed value:"+val);
				homeOptions.beds = val;

				la( AnalyticsType.ANALYTICS_TYPE_CLICK,
          			'SetBeds', 
          			val);
				ga('send', {
				  'hitType': 'event',          // Required.
				  'eventCategory': 'button',   // Required.
				  'eventAction': 'click',      // Required.
				  'eventLabel': 'SetBeds'
				});
				$('.selectBed ul').hide();
				self.get.results({ homeOptions: homeOptions  }, false);
			});
			$('.selectBed').each(function(){
				$(this).children('.selected,span.selectArrow').click(function(event){
					event.stopPropagation();
					if($(this).parent().children('ul').css('display') == 'none'){
						$(this).parent().children('ul').css('display','block');
						// $('ul#max').css('display','none');
						$('.selectBed').addClass('active');
						$('.selectBath ul').hide();
						$('.selectBath').removeClass('active');
						$('.selectBoxmin').removeClass('active');
						$('.selectBoxmax').removeClass('active');
						$('.selectBoxmin ul').hide();
						$('.selectBoxmax ul').hide();
					}
					else {
						$(this).parent().children('ul').css('display','none');
						$('.selectBed').removeClass('active');
					}
				});
    		});

			$('.selectBath').attr('value', 0); // default value - ALL
			$('.selectBath ul').html(options6);
			$('.selectBath ul').find('li').click(function(){
				$('.selectBath ul li[value]').removeClass('active');
				var val = $(this).attr('value');
				$(this).parent().css('display','none');
				$(this).addClass('active');
				$(this).closest('.selectBath').attr('value',val);
				var baths = '&nbsp;'+(val != 1 ? 'baths' : 'bath');
				$(this).parent().siblings('.selected').html($(this).html()+baths);
				console.log("Bath value:"+val);
				homeOptions.baths = val;

				la( AnalyticsType.ANALYTICS_TYPE_CLICK,
          			'SetBaths', 
          			val);
				ga('send', {
				  'hitType': 'event',          // Required.
				  'eventCategory': 'button',   // Required.
				  'eventAction': 'click',      // Required.
				  'eventLabel': 'SetBaths'
				});
				$('.selectBath ul').hide();
				self.get.results({ homeOptions: homeOptions  }, false);
			});
			$('.selectBath').each(function(){
				$(this).children('.selected,span.selectArrow').click(function(event){
					event.stopPropagation();
					if($(this).parent().children('ul').css('display') == 'none'){
						$(this).parent().children('ul').css('display','block');
						// $('ul#max').css('display','none');
						$('.selectBath').addClass('active');
						$('.selectBed ul').hide();
						$('.selectBed').removeClass('active');
						$('.selectBoxmin').removeClass('active');
						$('.selectBoxmax').removeClass('active');
						$('.selectBoxmin ul').hide();
						$('.selectBoxmax ul').hide();
					}
					else {
						$(this).parent().children('ul').css('display','none');
						$('.selectBath').removeClass('active');
					}
				});
    		});

    		$('select#bed').html(options7);
			$('select#bed').change(function(e){
				e.preventDefault();
				self = quizProfile;
				var val = parseInt($(this).val());
				console.log("Bed selected: "+val);
				homeOptions.beds = val;
				
				la( AnalyticsType.ANALYTICS_TYPE_CLICK,
          			'SetBeds', 
          			val);
				ga('send', {
				  'hitType': 'event',          // Required.
				  'eventCategory': 'button',   // Required.
				  'eventAction': 'click',      // Required.
				  'eventLabel': 'SetBeds'
				});
				$('.option .dropdown').hide();
				self.get.results({ homeOptions: homeOptions  }, false);
			});

			$('select#bath').html(options8);
			$('select#bath').change(function(e){
				e.preventDefault();
				self = quizProfile;
				var val = parseInt($(this).val());
				console.log("Bath selected: "+val);
				homeOptions.baths = val;
				
				la( AnalyticsType.ANALYTICS_TYPE_CLICK,
          			'SetBaths', 
          			val);
				ga('send', {
				  'hitType': 'event',          // Required.
				  'eventCategory': 'button',   // Required.
				  'eventAction': 'click',      // Required.
				  'eventLabel': 'SetBaths'
				});
				$('.option .dropdown').hide();
				self.get.results({ homeOptions: homeOptions  }, false);
			});
			

			$('.quiz-tags table #miles').change(function() {
				self = quizProfile;
				var val = parseInt($('.quiz-tags table #miles').val());
				console.log("Miles selected: "+val);
				self.resetCookies();
				self.get.results({ distance: val }, false);
			})

			$('.profile-tiles .save-profile').on('click', function() {
				self = quizProfile;
				if (ah_local.author_id == '0') {
					console.log("user wants to register!");
					self.login(redirect, ah_local.wp+'/quiz-results/saveProfile', 'Log in now to save this search.');
				}
				else
					self.saveProfile();
			})
			$('.resultsnav .more .save-profile').on('click', function() {
				self = quizProfile;
				if (ah_local.author_id == '0') {
					console.log("user wants to register!");
					self.login(redirect, ah_local.wp+'/quiz-results/saveProfile', 'Log in now to save this search.');
				}
				else
					self.saveProfile();
			})

			$('.profile-tiles .my-profiles').on('click', function() {
				self = quizProfile;
				if (ah_local.author_id == '0') {
					console.log("user wants to register!");
					self.login(redirect, ah_local.wp+'/quiz-results/selectMyProfile', 'Log in now to go to access your searches.');
				}
				else
					self.saveProfile(self.selectMyProfile);
			})
			$('.resultsnav .more .my-profiles').on('click', function() {
				self = quizProfile;
				if (ah_local.author_id == '0') {
					console.log("user wants to register!");
					self.login(redirect, ah_local.wp+'/quiz-results/selectMyProfile', 'Log in now to go to access your searches.');
				}
				else
					self.saveProfile(self.selectMyProfile);
			})
			
			$('.profile-tiles .new-profile').on('click', function() {
				self = quizProfile;
				ah.openModal('find-a-home');
			})
			$('.resultsnav .more .new-profile').on('click', function() {
				self = quizProfile;
				ah.openModal('find-a-home');
			})

			// view-selector stuff
			$('.view-selector li').on('click',function(){
				// $('.view-selector .backtocity').on('click', function() {
				// 	$('.view-selector li.city').click();
				// });
				// $('.view-selector .backtolistings').on('click', function() {
				// 	timeoutID = window.setTimeout(delaylistingscheck, 850);
				// 	quizProfile.primary = 'listings';
				// 	quizProfile.map.hide();
				// 	setCookie('QuizViewSelector', 'list');
				// });
		    	if ($(this).children().hasClass('city')) {
		    		quizProfile.primary = 'city';
		    		quizProfile.map.hide();
		    		setCookie('QuizViewSelector', 'city');
		    		quizProfile.doingSwitchToListing = false;
		    	}
		    	else if ($(this).children().hasClass('list')) {
		    		quizProfile.primary = 'listings';
		    		quizProfile.map.hide();
		    		setCookie('QuizViewSelector', 'list');
		    	}
		    	else if ($(this).children().hasClass('map')) {
		    		quizProfile.map.show();
		    		setCookie('QuizViewSelector', 'map');
		    	}
		    });

		    $('.view-selector .backtocity').on('click', function() {
					quizProfile.primary = 'city';
					quizProfile.map.hide();
					setCookie('QuizViewSelector', 'city');
				$('.view-selector li.map').removeClass( 'active' );
				$('.view-selector li.city').addClass( 'active' );
				$('.view-selector-bg .slide').addClass( 'left' );
				$('.view-selector-bg .slide').removeClass( 'right' );
				quizProfile.doingSwitchToListing = false;
			});
			$('.view-selector .backtolistings').on('click', function() {
					timeoutID = window.setTimeout(delaylistingscheck, 850);
					quizProfile.primary = 'listings';
					quizProfile.map.hide();
					setCookie('QuizViewSelector', 'list');
				$('.view-selector li.map').removeClass( 'active' );
				$('.view-selector li.city').addClass( 'active' );
				$('.view-selector-bg .slide').addClass( 'left' );
				$('.view-selector-bg .slide').removeClass( 'right' );
			});

			//$('.profile-tiles .your-cities').on('click', function() {
			//	self = quizProfile;
			//	if (ah_local.author_id == '0') {
			//		console.log("user wants to register!");
			//		self.login(redirect, ah_local.wp+'/quiz-results/selectCities', 'Log in now to go to access your searches.');
			//	}
			//	else
			//		self.popup.cities();
			//})

			if (hash == 'badQuizId') {
				var h = "We're very sorry, but we have a bad quiz.  Hit OK to go back to home page";
				ahtb.alert( h,
							{width: 450,
							height: 180,
							buttons: [{text: 'OK', action: function() {
								window.location = ah_local.wp;
							}}]});
				return;
			}

			this.loadingAlert();
			console.log("quiz-results before quiz_id:"+quiz_id);
			var quizID = getCookie("QuizCompleted");
			console.log("quiz-results entered with quizId:"+quizID);
			if (quizID != '' &&
				quizID != '-1')
				quiz_id = parseInt(quizID);

			var qr_page = thisPage.indexOf('-new') != -1 ? 'quiz-results-new' : 'quiz-results';
			console.log("before @1 pushState for "+qr_page);
			window.history.pushState('pure','Title',ah_local.wp+'/'+qr_page);

			var quizParsed = getCookie("QuizParsed"); // will be non-0 if gone through RunQuiz or get-last-result
			var showHaveExcessQuizResults = getCookie('ShowHaveExcessQuizResults');
			quizProfile.showHaveExcessQuizResults = showHaveExcessQuizResults.length && showHaveExcessQuizResults == 'false' ? false : true;

			var activeCityId = getCookie("ActiveCityId");
			this.activeCityId = activeCityId.length ? parseInt(activeCityId) : 0;
			if (!this.activeCityId) {
				var ele = $('.view-selector li.list');
				ele.hide();
			}

			if (hash == 'Reentry')
				hash = '';

			console.log("quiz-results using quiz_id:"+quiz_id+", QuizParsed:"+quizParsed+", showHaveExcessQuizResults:"+showHaveExcessQuizResults+", activeCityId:"+this.activeCityId+", hash:"+hash);
			if (hash == "RunQuiz") {
				hash = '';
				quizProfile.runQuiz();
			}
			else if (quizParsed == '' || quizParsed == '0') {
				quizProfile.basicGetLastResults();			
			}
			else {
				var resultsName = (hash == "Reload") ? 'results'+quiz_id : 'results';
				if (hash == "Reload") hash = '';
				var storage = quizProfile.retrieveResults(resultsName);
				if ( storage == null ||
					 storage.id != quiz_id) {
					if ( storage ) delete sessionStorage.resultsName;
					if (quiz_id != -1)
						quizProfile.showResults();
					else
						quizProfile.basicGetLastResults();
				}
				else
					quizProfile.showResults(storage.results);
			}
		}

		this.basicGetLastResults = function() {
			setCookie("LastViewedProfile", '', 7);
			setCookie("QuizCompleted", quiz_id, 2);
			setCookie("QuizId", quiz_id, 2);
			setCookie("QuizSession", ah_local.sessionID, 2);
			setCookie("ACTIVE_SESSION_ID", ah_local.activeSessionID, 2);
			quizProfile.profileName = getCookie("LastViewedProfile");
			quizProfile.price_slider_init = false;
			quizProfile.map.init();
			quizProfile.waitForQuiz();
		}

		this.waitForQuiz = function() {
			this.DB({
					query: 'get-last-result',
					data: { quiz: quiz_id },
					done: function(d) {
						if (typeof d == 'string') {
							console.log("waitForQuiz got "+d);
							if (d == 'pending' ||
								d == 'processing')
								window.setTimeout(function() {
									quizProfile.waitForQuiz();
								}, 250);
							else if (d == 'started') {
								ahtb.open({html:"<div><p>Quiz requested was just started.  Did you start a quiz on a different machine?  Do you want to re-run this quiz?</p></div>",
							  			   width: 450, 
							  			   height: 180,
							  			   buttons: [{text: 'OK', action: function() {
							  			   		ahtb.close();
							  			   		quizProfile.runQuiz();
							  			   }},
							  			   {text: 'Cancel', action: function() {
							  			   		ahtb.close();
							  			   		$('.results-loading-overlay').animate({opacity: '0'},
																	  {queue: false, duration: 1000, done: function() {
																	  	$(this).hide();
																	  	
																	  }})
							  			   }}]});
								
								return;
							}

						}
						else {
							quizProfile.processResults(d);
							setCookie("QuizParsed", 1, 2);		
							quizProfile.updateResultsAnalytics();
							// $('.view-selector li').on('click',function(){
							// 	$('.view-selector .backtocity').on('click', function() {
							// 		$('.view-selector li.city').click();
							// 	});
							// 	$('.view-selector .backtolistings').on('click', function() {
							// 		timeoutID = window.setTimeout(delaylistingscheck, 850);
							// 		quizProfile.primary = 'listings';
							// 		quizProfile.map.hide();
							// 		setCookie('QuizViewSelector', 'list');
							// 	});
							// 	if ($(this).children().hasClass('city')) {
						 //    		quizProfile.primary = 'city';
						 //    		quizProfile.map.hide();
						 //    		setCookie('QuizViewSelector', 'city');
						 //    	}
						 //    	else if ($(this).children().hasClass('list')) {
						 //    		quizProfile.primary = 'listings';
						 //    		quizProfile.map.hide();
						 //    		setCookie('QuizViewSelector', 'list');
						 //    	}
						 //    	else if ($(this).children().hasClass('map')) {
						 //    		quizProfile.map.show(); // can be either city or list data
						 //    		setCookie('QuizViewSelector', 'map');
						 //    	}
						 //    });
						    if (getCookie('QuizViewSelector') == 'map')
						    	quizProfile.map.show();

						    $('.tags-wrap .add-tags').on('click',function(){
						    	quizProfile.popup.tags();
						    	//else if ($(this).hasClass('your-cities')) quizProfile.popup.cities();
						  	});
						}	
					},
					error: function(d) {
						console.log("quizProfile got error for initial get-status.  running Quiz");
						quizProfile.runQuiz();
					}
				})
		}

		this.runQuiz = function() {
			this.DB({
					// query: 'save-quiz-action',
					// data: { quiz: quizID, action: 'done' },
					query: 'run-quiz',
					data: { quiz: quiz_id },
					error: function(d){
						ahtb.open({
							height: 165,
								width: 480,
								title: 'Results Failure',
								html:'<p>Oops!! Something went wrong.  Hit OK to go back.</p>',
								buttons: [
									{text: 'OK', action:function(){
										ahtb.close();
										window.history.back();
									}}
								]
						});
					},
					done: function(d){
						if (typeof d == 'string') {
							if (d.indexOf('processing') != -1) {
								console.log("runQuiz got back 'processing', so going to go wait now!");
								setCookie("LastViewedProfile", '', 7);
								setCookie("QuizCompleted", quiz_id, 2);
                                setCookie("QuizId", quiz_id, 2);
               					setCookie("QuizSession", ah_local.sessionID, 2);
                				setCookie("ACTIVE_SESSION_ID", ah_local.activeSessionID, 2);
                				quizProfile.profileName = getCookie("LastViewedProfile");
                				quizProfile.price_slider_init = false;
                				quizProfile.map.init();
								quizProfile.waitForQuiz();
								return;
							}
						}
						setCookie('ProfileData', '', 1);
						if (profiles) 
							profiles = null;
						quizProfile.showResults();
						setCookie("QuizParsed", 1, 2);		
						quizProfile.updateResultsAnalytics();			
					}
				})
		}

		this.updateResultsAnalytics = function() {
			this.DB({
				query: 'update-results-analytics',
				data: { //sessionID: ah_local.sessionID, // NOTE: this will be added in this.DB().
						quizId: quiz_id },
				error: function(d){
					ahtb.open({
						height: 165,
							width: 480,
							title: 'Results Failure',
							html:'<p>Oops!! Something went wrong updating results metric!.</p>',
							buttons: [
								{text: 'OK', action:function(){
									ahtb.close();
									window.history.back();
								}}
							]
					});
				},
				done: function(d){
					console.log(d);
				}
			})
		}

		this.showResults = function(d) {
			var index = parseInt(getCookie('LastViewedProfileIndex'));
			console.log("LastViewedProfileIndex - "+index);
			if (index != -1 &&
				!isNaN(index)) {
				if (!gotInitialProfiies) {
					window.setTimeout(function() {
						quizProfile.showResults();
					}, 250);
					return;
				}
				console.log('useThisProfile:'+profiles[index].name);
				quizProfile.profileName = profiles[index].name;
				quiz_id = profiles[index].quizId;
				// ah_local.sessionID = profiles[index].session_id;
				// NOTE THIS!!!
				// ah_local.activeSessionID = profiles[index].activeSessionID;
				// ah_local.sessionID = profiles[index].session_id;
				if ( (profiles[index].quizType % 2) == 0)
					$('#miles option[value="'+profiles[index].distance+'"]').prop('selected', true);
				setCookie("LastViewedProfile", profiles[index].name, 7);
				setCookie("LastViewedProfileIndex", index, 7);
				setCookie("QuizCompleted", quiz_id, 2);
				setCookie("QuizId", quiz_id, 2);
			}
			this.profileName = getCookie("LastViewedProfile");
			this.price_slider_init = false;
			this.map.init();
			if (typeof d == 'undefined')
				this.get.results();
			else
				this.processResults(d);
			// $('.view-selector li').on('click',function(){
		 //    	if ($(this).children().hasClass('city')) {
		 //    		quizProfile.primary = 'city';
		 //    		quizProfile.map.hide();
		 //    		setCookie('QuizViewSelector', 'city');
		 //    	}
		 //    	else if ($(this).children().hasClass('list')) {
		 //    		quizProfile.primary = 'listings';
		 //    		quizProfile.map.hide();
		 //    		setCookie('QuizViewSelector', 'list');
		 //    	}
		 //    	else if ($(this).children().hasClass('map')) {
		 //    		quizProfile.map.show();
		 //    		setCookie('QuizViewSelector', 'map');
		 //    	}
		 //    });
		    if (getCookie('QuizViewSelector') == 'map') {
		    	if (quizProfile.activeCityId != 0) {
		    		if ($('ul#listings').is(':empty'))
		    			quizProfile.switchToListing();
		    		quizProfile.map.show();
		    	}
		    	else if (typeof d != 'undefined' && d.city_sorted && length(d.city_sorted))
		    		setCookie('QuizViewSelector', 'city');
		    	else
		    		setCookie('QuizViewSelector', 'list');
		    }

		    $('.tags-wrap .add-tags').on('click',function(){
		    	quizProfile.popup.tags();
		    	//else if ($(this).hasClass('your-cities')) quizProfile.popup.cities();
		  	});
		}
		this.goToListing = function(id, newTab) {
			// setCookie('QuizListingTop', self.scrollTop, 2);
			// console.log("QuizListingTop set to: "+self.scrollTop);
			self = quizProfile;
			setCookie("ActiveCityId", quizProfile.activeCityId, 2);
			setCookie("VisitingListing", 1, 1);
			setCookie("TagsForListing", quizProfile.myTagList, 1);
			setCookie("PrimaryList", quizProfile.primary, 1);

			self.storeResults('city_sorted'+quizProfile.query.index, quizProfile.city_sorted);

			var listing_page = thisPage.indexOf('-new') != -1 ? 'listing-new' : 'listing';
			var qr_page = thisPage.indexOf('-new') != -1 ? 'quiz-results-new' : 'quiz-results';
			console.log("before @2 pushState for "+qr_page);
			ga('send', {
			  'hitType': 'event',          // Required.
			  'eventCategory': 'button',   // Required.
			  'eventAction': 'click',      // Required.
			  'eventLabel': 'Listing'
			});
			la( AnalyticsType.ANALYTICS_TYPE_CLICK,
          		'Listing',
          		id,
          		null,
          		function() {
          			// if (self.goingToListing) {
          			// 	console.log("goToListing called when goingToListing is true already!");
          			// 	return;
          			// }
         			if (typeof newTab == 'undefined') {
         				window.history.pushState('pure','Title',ah_local.wp+'/'+qr_page+'/L-'+quiz_id);
         				// quizProfile.storeResults('results'+quiz_id, quizProfile.query);
         				
         				// self.goingToListing = true;
						window.location = ah_local.wp+"/"+listing_page+"/"+id;
					}
					else
						window.open(ah_local.wp+"/"+listing_page+"/N-"+id);
				});
		}

		this.switchToListing = function(id) {
			if (quizProfile.doingSwitchToListing) {
				var city_sorted = self.get.city_sorted_by_id(id);
				if (city_sorted &&
					city_sorted.listings.length == city_sorted.total) {
					console.log("got all listings already");
					quizProfile.doingSwitchToListing = false;
				}
				else {
					// if (quizProfile.attemptSwitchToListing < 10)
					if (quizProfile.attemptSwitchToListing == 0)
						ahtb.open({html:'<div style="padding-top: 10px;">Please hold..</div>',
								   width: 300,
								   hideClose: true,
				                   closeOnClickBG: false,
				                   hideSubmit: true});
					quizProfile.pleaseHoldTimer = window.setTimeout(function() {
						quizProfile.switchToListing(id);					
					}, 500);

					quizProfile.attemptSwitchToListing++;
					return;
				}
			}

			if ( quizProfile.pleaseHoldTimer ) {
				window.clearTimeout(quizProfile.pleaseHoldTimer);
				quizProfile.pleaseHoldTimer = 0;
			}

			if (ahtb.opened)
				ahtb.close();

			quizProfile.attemptSwitchToListing = 0;
			quizProfile.doingSwitchToListing = true;
			timeoutID = window.setTimeout(delaylistingscheck, 850);
			// empty out any existing listing info
			$('div.citynamebanner .cityname').empty();
			$('div#listings ul').empty();
			$('div.citynamebanner .score').hide();

			quizProfile.primary = 'listings';
			// quizProfile.map.hide();
			setCookie('QuizViewSelector', 'list');
			$('.view-selector li.map').removeClass( 'active' );
			$('.view-selector li.city').addClass( 'active' );
			$('.view-selector-bg .slide').addClass( 'left' );
			$('.view-selector-bg .slide').removeClass( 'right' );
			if (typeof id == 'undefined') {
				if (quizProfile.activeCityId == 0) {
					ahtb.alert("City not selected");
					return;
				}
				id = quizProfile.activeCityId;
			}
			var self = quizProfile;
			if (quizProfile.activeCityId != id) {
				self.scrollData[self.primary].pageNumber = 0;
				self.scrollData[self.primary].scrollTop = 0;
				self.scrollData[self.primary].scrollPage = 0;
				self.defaults.perPage[self.primary].for_first_page = 7;
				setCookie("QuizListingTop", 0, 1);
			}
			quizProfile.activeCityId = id;
			console.log("switchToListing for city:"+id+", quiz_id:"+quiz_id);
			setCookie("ActiveCityId", id, 2);
			var sessionToUse = ah_local.sessionID;
			var activeSessionID = ah_local.activeSessionID;
			var city_sorted = self.get.city_sorted_by_id(id);

			if (city_sorted == null) {
				if (self.query.city_sorted)
					ahtb.alert("City cannot be found");
				return;
			}

			self.primary = 'listings';
			self.listings = [];
			self.listingPageNumber = 0;
			quizProfile.map.hide();
			setCookie("QuizListingPos", 0, 2);
			self.map.clearMarkers();
			$('.view-selector li.list').show();
			$('div#listings #loading').show();

			if (typeof city_sorted.mapped != 'undefined' &&
				city_sorted.mapped & ListingMapMode.DID_SECONDARY_LISTING_KEY_MAPPING) {
				self.updateListingsList(city_sorted.listings);
				if (city_sorted.listings.length != city_sorted.total)
					quizProfile.fetchListingData(city_sorted, id, quiz_id, city_sorted.listings.length);
				else {
					quizProfile.doingSwitchToListing = false;
					$('div#listings #loading').hide();
					if (ahtb.opened)
						ahtb.close();
				}
				return;
			}

			quizProfile.fetchListingData(city_sorted, id, quiz_id, 0);

			// theData = { //sessionID: sessionToUse, // This will be added by this.DB()
			// 			city_id: id,
			// 			quizId: quiz_id };
			// quizProfile.DB({
			// 				query: 'update-city-listings',
			// 				data: theData,
			// 				error: function(d){
			// 					quizProfile.doingSwitchToListing = false;
			// 					if (d.indexOf('No listings') != -1 || 
			// 						d.indexOf('No activity found for session') != -1 ) ahtb.open({
			// 						height: 165,
			// 							width: 480,
			// 							title: 'Listing Results',
			// 							html:'<p>'+(d.indexOf('No listings') == -1 ? 'You have not yet taken the quiz, please take the quiz before trying to view results.' : 
			// 									                                     'Failed to get listings for '+quizProfile.get.city_by_id(id).city )+
			// 								 '</p>',
			// 							buttons: [
			// 								{text: (d.indexOf('No listings') == -1 ? 'Take Quiz' : 'OK'), action:function(){
			// 									ahtb.close();
			// 									if ( d.indexOf('No listings') == -1 )
			// 										setTimeout( function(){ ah.openModal('find-a-home'); }, 250 );
			// 									else if ( !quizProfile.map.hidden ) {
			// 											//$('.view-selector a.list').parent().removeClass('active');
			// 											//$('.view-selector a.city').parent().addClass('active');
			// 									}
												
			// 								}}
			// 							]
			// 					});
			// 				},
			// 				done: function(d){
			// 					ah_local.sessionID = sessionToUse; // let's restore it, since ahtb call may have reverted it back.
			// 					ah_local.activeSessionID = activeSessionID; // let's restore it, since ahtb call may have reverted it back.

			// 					if (typeof d == 'string') {
			// 						quizProfile.doingSwitchToListing = false;
			// 						ahtb.open({
			// 							height: 165,
			// 								width: 480,
			// 								title: 'Listing Results',
			// 								html:'<p>Failed to get listings for '+quizProfile.get.city_by_id(id).city+'.</p>',
			// 								buttons: [
			// 									{text: 'OK', action:function(){
			// 										ahtb.close();
			// 										if ( !quizProfile.map.hidden ) {
			// 											//$('.view-selector a.list').parent().removeClass('active');
			// 											//$('.view-selector a.city').parent().addClass('active');
			// 										}
			// 									}}
			// 								]
			// 						});
			// 					}
			// 					else {
			// 						// d should be an array of listings with one-to-one order with city_sorted.listings
			// 						if ( typeof city_sorted.mapped  == 'undefined' ||
			// 							 !(city_sorted.mapped & ListingMapMode.DID_SECONDARY_LISTING_KEY_MAPPING) ) {
			// 							city_sorted.mapped |= ListingMapMode.DID_SECONDARY_LISTING_KEY_MAPPING;
			// 							// for(var i in d) {
			// 							// 	if (parseInt(i) >= city_sorted.listings.length)
			// 							// 		city_sorted.listings[city_sorted.listings.length] = {};
			// 							// 	for(var j in d[i])
			// 							// 		city_sorted.listings[i][ reverseListingKeyMap[j] ] = d[i][j];
			// 							// }
			// 							for(var i in d.results) {
			// 								var len = city_sorted.listings.length;
			// 								if ( typeof city_sorted.listings[len] == 'undefined')
			// 									city_sorted.listings[len] = {};
			// 								for(var j in d.results[i])
			// 									city_sorted.listings[len][ reverseListingKeyMap[j] ] = d.results[i][j];
			// 							}
			// 						}
			// 						self.updateListingsList(city_sorted.listings);
			// 					}		
			// 				}
			// 			}
			// );
		}

		this.fetchListingData = function(city_sorted, cityId, quiz_id, startId) {
			console.log("fetchListingData - cityId:"+cityId+", quizId:"+quiz_id+", startId:"+startId+", primary"+quizProfile.primary);
			if (quizProfile.primary != 'listings') {
				console.log("Primary is now "+quizProfile.primary+", so not getting data for cityId:"+cityId+", quizId:"+quiz_id+", startId:"+startId);
				$('div#listings #loading').hide();
				quizProfile.doingSwitchToListing = false;
				return;
			}
			theData = { //sessionID: sessionToUse, // This will be added by this.DB()
						city_id: cityId,
						quizId: quiz_id,
						start_id: startId,
					  };
			quizProfile.DB({
							query: 'update-city-listings',
							data: theData,
							error: function(d){
								$('div#listings #loading').hide();
								quizProfile.doingSwitchToListing = false;
								if (d.indexOf('No listings') != -1 || 
									d.indexOf('No activity found for session') != -1 ) ahtb.open({
									height: 165,
										width: 480,
										title: 'Listing Results',
										html:'<p>'+(d.indexOf('No listings') == -1 ? 'You have not yet taken the quiz, please take the quiz before trying to view results.' : 
												                                     'Failed to get listings for '+quizProfile.get.city_by_id(cityId).city )+
											 '</p>',
										buttons: [
											{text: (d.indexOf('No listings') == -1 ? 'Take Quiz' : 'OK'), action:function(){
												ahtb.close();
												if ( d.indexOf('No listings') == -1 )
													setTimeout( function(){ ah.openModal('find-a-home'); }, 250 );
												else if ( !quizProfile.map.hidden ) {
														//$('.view-selector a.list').parent().removeClass('active');
														//$('.view-selector a.city').parent().addClass('active');
												}
												
											}}
										]
								});
							},
							done: function(d){
								// ah_local.sessionID = sessionToUse; // let's restore it, since ahtb call may have reverted it back.
								// ah_local.activeSessionID = activeSessionID; // let's restore it, since ahtb call may have reverted it back.

								if (typeof d == 'string') {
									quizProfile.doingSwitchToListing = false;
									ahtb.open({
										height: 165,
											width: 480,
											title: 'Listing Results',
											html:'<p>Failed to get listings for '+quizProfile.get.city_by_id(cityId).city+'.</p>',
											buttons: [
												{text: 'OK', action:function(){
													ahtb.close();
													if ( !quizProfile.map.hidden ) {
														//$('.view-selector a.list').parent().removeClass('active');
														//$('.view-selector a.city').parent().addClass('active');
													}
												}}
											]
									});
								}
								else {
									// d should be an array of listings with one-to-one order with city_sorted.listings
									if ( typeof city_sorted.mapped  == 'undefined' ||
										 !(city_sorted.mapped & ListingMapMode.DID_SECONDARY_LISTING_KEY_MAPPING) ) 
										city_sorted.mapped |= ListingMapMode.DID_SECONDARY_LISTING_KEY_MAPPING;

									var startingLen = startId ? city_sorted.listings.length : 0;
									var initialCall = startId == 0;
									for(var i in d.results) {
										var len = initialCall ? i : city_sorted.listings.length;
										if ( typeof city_sorted.listings[len] == 'undefined')
											city_sorted.listings[len] = {};
										for(var j in d.results[i])
											city_sorted.listings[len][ reverseListingKeyMap[j] ] = d.results[i][j];
									}
									
									quizProfile.updateListingsList(city_sorted.listings, startingLen, city_sorted.listings.length);
									if (d.id_length > d.nextId)
										quizProfile.fetchListingData(city_sorted, cityId, quiz_id, d.nextId);
									else {
										$('div#listings #loading').hide();
										quizProfile.doingSwitchToListing = false;
									}
								}		
							}
						}
			);
		}

		this.updateListingsList = function(listings, startId, totalLen) {
			// $('.view-selector li.list').show();

			var self = quizProfile;
			if ( typeof startId == 'undefined')
				startId = 0;

			if ( typeof totalLen == 'undefined')
				totalLen = listings.length;
			// self.primary = 'listings';
			// self.listings = [];

			// self.map.clearMarkers();

			// need to reset quizProfile.results
			if (listings != null && length(listings)) {
				for (var i in listings) {
					if (i < startId)
						continue;
					listings[i].price = parseInt(listings[i].price);
					self.listings[i] = listings[i];
					self.map.createMarker( self.listings[i], 'listings' );
				}
			}

			// need to reset for map
			self.map.setMapBounds();
			// setCookie("QuizListingPos", 0, 2);
			// self.scrollData[self.primary].pageNumber = 0;
			// self.scrollData[self.primary].scrollTop = 0;
			// self.scrollData[self.primary].scrollPage = 0;
			// self.defaults.perPage[self.primary].for_first_page = 7;

			// quizProfile.map.hide();
			// show page
			if ( self.listingPageNumber == 0 &&
				 startId == 0) {
				self.get.page(self.listingPageNumber);
		    	// if (self.scrollData[self.primary].scrollPage) {
		    	// 	self.scrollData[self.primary].scrollPage = 0;
		    	// 	self.scrollData[self.primary].scrollTop = self.primary == 'listings' ? parseInt(getCookie('QuizListingTop')) : parseInt(getCookie('QuizCityTop'));
		    	// 	console.log("QuizListingTop retrieved: "+self.scrollData[self.primary].scrollTop);
		    	// 	var timeout = 250; //self.defaults.listings_for_first_page * 50;
		    	// 	// window.setTimeout(self.setTop, timeout);
		    	// 	// self.scrollTop = getCookie('QuizListingTop');
		    	// 	// $('#listings').scrollTop(self.scrollTop);
		    	// 	//$('#listings').scrollTop((self.defaults.listings_for_first_page-self.defaults.listings_per_page)*350);
		    	// }

		    	//$('.view-selector a.list').parent().addClass('active');
				//$('.view-selector a.city').parent().removeClass('active');

		    	if ( !quizProfile.map.hidden ) {
					quizProfile.map.hide();
				}
				else {
					var inactiveOne = quizProfile.primary == 'listings' ? 'cities' : 'listings';
					$('div#'+inactiveOne).animate(
							{ left: "100%" },
							{ queue: false, duration: 500, done:function(){
								$('div#'+inactiveOne).hide();
								var activeOne = quizProfile.primary == 'listings' ? 'listings' : 'cities';
								$('div#'+activeOne).show().animate({left: '0%'},{ queue: false, duration: 500 });
							}}
						);
				}
			}
			// quizProfile.doingSwitchToListing = false;
		}
		
		this.loadingAlert = function(){
			//ahtb.loading('<strong>Assessment completed. Sorting through '+listHubFeedSize.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+' listings...</strong>', {
			//	hideTitle: true,
			//	height: 80,
			//	width: 700
			//});
			return;
		}
		this.explainIcons = function() {
			var len = Object.keys(self.query.cityTags).length;
			var msg = "<p>These icons indicate lifestyle amenities that rated highly in the area.</p>";
			msg += "<div id='faq'>";
			for(var i in self.query.cityTags)
				msg += '<img src="'+ah_local.tp+'/_img/tag_icons/'+self.query.cityTags[i].icon+ '" style="width:40px;height:40px"/> - '+self.query.cityTags[i].desc+'<br/>';
			msg += '</div>';
			ahtb.open({	title: "Icon FAQ",
						html: msg,
						width: 830,
						 height: (len +1)* 54 ,
						 opened: function(){
						 	$('#TB_ajaxContent #tb-submit #faq').css('text-align','left');
						 	$('#TB_ajaxContent #tb-submit #faq').css('margin-left','20px');
						 	$('#TB_ajaxContent #tb-submit #faq').css('margin-bottom','20px');
						 }});
		}

		this.warnDuplicate = function(msg, callback, arg) {
			self = quizProfile;
			ahtb.alert( msg, {
						width: 450,
						buttons: [
							{text:"Retry", action: function() {
								self.saveProfile(callback, arg);
							}},
							{text:"Cancel", action: function() {
								ahtb.discard();
							}}]
					})
		}

		// this.showOption = function( msg, 
		// 							width, height,
		// 							okText, okCallback, okArgs,
		// 							cancelText, cancelCallback, cancelArgs) {
		// 	ahtb.alert(msg,
		// 				{width: width,
		// 				 height: height,
		// 				 buttons: [
		// 					{text:okText, action: function() {
		// 						console.log("user wants to register!");
		// 						self.login(okCallback, okArgs);
		// 					}},
		// 					{text:cancelText, action: function() {
		// 						ahtb.close();
		// 						if (typeof cancelCallback != 'undefined')
		// 							setTimeout( function(){ cancelCallback(cancelArgs); }, 250 );
		// 					}}
		// 				]});
		// }

		this.useThisProfile = function(index) {
			if (!gotInitialProfiies) {
				window.setTimeout(function() {
					quizProfile.useThisProfile(index);
				}, 250);
				return;
			}
			quizProfile.loadingAlert();
			console.log('useThisProfile:'+profiles[index].name);
			quizProfile.profileName = profiles[index].name;
			quiz_id = profiles[index].quizId;
			ah_local.sessionID = profiles[index].session_id;
			ah_local.activeSessionID = profiles[index].activeSessionID;
			if ( (profiles[index].quizType % 2) == 0)
				$('#miles option[value="'+profiles[index].distance+'"]').prop('selected', true);
			setCookie("LastViewedProfile", profiles[index].name, 7);
			setCookie("LastViewedProfileIndex", index, 7);
			setCookie("QuizCompleted", quiz_id, 2);
			setCookie("QuizId", quiz_id, 2);
			quizProfile.get.results( null, true );
		}

		this.selectMyProfile = function() {
			console.log("Entered selectMyProfile");
			if (!ahProf.selectMyProfile(quizProfile.useThisProfile))
				ahtb.alert("Failed to present searches popup!", {height: 150});
		}
		
		

		$('.dropdown .savecollapse .savebutton').on('click', function(callback, arg) {
									var val = $('input#mobilesaveProfile').val();
									if (val != '') {
										console.log("Saving search..."+val);
										var data = {query: 'save-profile',
													data: {//sessionID: ah_local.sessionID,  // this will be added by this.DB()
														   profile_name: val,
														   quizId: self.query.index}, // this should be the 'id' index of QuizActivity
													error: function(d) {
														if (typeof d == 'string' &&
															 d.indexOf("Another profile") != -1) {
															self.warnDuplicate(d, callback, arg);
														}
														else if (typeof d == 'string') {
													   		ahtb.alert(d);
															setCookie("QuizNeedSaveProfile", 1, 2);
														}
														else {
															ahtb.alert("Database failure while saving search.", { height: 150});
															setCookie("QuizNeedSaveProfile", 1, 2);
														}
													},
													done: function(d) {
														profiles = d;
														setCookie("QuizNeedSaveProfile", 0, 2);
														setCookie('ProfileData', JSON.stringify(profiles), 30);
															ahtb.close();
														$('.savecollapse').slideToggle();
													} };
										ahtb.alert("Saving search...", {
											height: 100,
											hideSubmit: true,
											buttons: []
										});
										self.DB(data);
									}
								})
		
		$('.dropdown .searchcollapse .savebutton').on('click', function(callback, arg) {
									var val = $('input#mobilesaveProfile2').val();
									if (val != '') {
										console.log("Saving search..."+val);
										var data = {query: 'save-profile',
													data: { // sessionID: ah_local.sessionID, // this will be added by this.DB()
														   profile_name: val,
														   quizId: self.query.index},
													error: function(d) {
														if (typeof d == 'string' &&
															 d.indexOf("Another profile") != -1) {
															self.warnDuplicate(d, callback, arg);
														}
														else if (typeof d == 'string') {
													   		ahtb.alert(d);
															setCookie("QuizNeedSaveProfile", 1, 2);
														}
														else {
															ahtb.alert("Database failure while saving search.", { height: 150});
															setCookie("QuizNeedSaveProfile", 1, 2);
														}
													},
													done: function(d) {
														profiles = d;
														setCookie("QuizNeedSaveProfile", 0, 2);
														setCookie('ProfileData', JSON.stringify(profiles), 30);
															ahtb.close();
														$('.searchcollapse .unsaved').slideToggle();
													} };
										ahtb.alert("Saving search...", {
											height: 100,
											hideSubmit: true,
											buttons: []
										});
										self.DB(data);
									}
								})
		
		$('.dropdown .newcollapse .savebutton').on('click', function(callback, arg) {
									var val = $('input#mobilesaveProfile3').val();
									if (val != '') {
										console.log("Saving search..."+val);
										var data = {query: 'save-profile',
													data: {//sessionID: ah_local.sessionID, // this will be added by this.DB
														   profile_name: val,
														   quizId: self.query.index},
													error: function(d) {
														if (typeof d == 'string' &&
															 d.indexOf("Another profile") != -1) {
															self.warnDuplicate(d, callback, arg);
														}
														else if (typeof d == 'string') {
													   		ahtb.alert(d);
															setCookie("QuizNeedSaveProfile", 1, 2);
														}
														else {
															ahtb.alert("Database failure while saving search.", { height: 150});
															setCookie("QuizNeedSaveProfile", 1, 2);
														}
													},
													done: function(d) {
														profiles = d;
														setCookie("QuizNeedSaveProfile", 0, 2);
														setCookie('ProfileData', JSON.stringify(profiles), 30);
															ahtb.close();
														$('.savecollapse').hide();
													} };
										ahtb.alert("Saving search...", {
											height: 100,
											hideSubmit: true,
											buttons: []
										});
										self.DB(data);
									}
								})

	$('.searchexpand').on('click', function() {
			if (getCookie("QuizNeedSaveProfile") != '1') {
					$('.unsaved').hide();
			}
			else {
					$('.unsaved').show();
		}
	});
		
	$('.dropdown .my-searches').on('click', function() {
		if (ah_local.author_id == '0') {
			console.log("user wants to register!");
			self.login(redirect, ah_local.wp+'/quiz-results/saveProfile', 'Log in now to save this search.');
		}
		else {
			quizProfile.selectMyProfile();
		}
	});
		
		
		this.reallySaveProfile = function(callback, arg) {
			self = quizProfile;
			//var h = "<div id='save-profile'><p>Name of profile:<input type='text' placeholder='Enter descriptive name' id='saveProfile'/></p></div>";
			var h = "<div id='save-profile'><p style='font-size:.9em'><input type='text' maxlength='33' placeholder='Name Your Search' id='saveProfile' style='width:65%;padding:6px 4px 4px 4px;height:18px;margin:5px auto 0'></p></div>";
			ahtb.open({ html: h,
					    title: "Save Your Search",
						width: 450,
						height: 160,
						buttons: [{text: "SAVE", action: function() {
									var val = $('input#saveProfile').val();
									if (val != '') {
										console.log("Saving search..."+val);
										var data = {query: 'save-profile',
													data: {// sessionID: ah_local.sessionID, // this will be added by this.DB
														   profile_name: val,
														   quizId: self.query.index}, // this is the row index of the actual QuizActivity
													error: function(d) {
														if (typeof d == 'string' &&
															 d.indexOf("Another profile") != -1) {
															self.warnDuplicate(d, callback, arg);
														}
														else if (typeof d == 'string') {
													   		ahtb.alert(d);
															setCookie("QuizNeedSaveProfile", 1, 2);
														}
														else {
															ahtb.alert("Database failure while saving search.", { height: 150});
															setCookie("QuizNeedSaveProfile", 1, 2);
														}
													},
													done: function(d) {
														// ahtb.alert(d);
														profiles = d;
														setCookie("QuizNeedSaveProfile", 0, 2);
														setCookie('ProfileData', JSON.stringify(profiles), 30);
														if (typeof callback != 'undefined')
															window.setTimeout( function() {
																callback(arg) }, 1500 );
														else
															ahtb.close();
													} };
										ahtb.alert("Saving search...", {
											height: 100,
											hideSubmit: true,
											buttons: []
										});
										self.DB(data);
									}
									// ahtb.close();
								}},
								{text: "Cancel", action: function() {
									ahtb.close();
									if (typeof callback != 'undefined')
										window.setTimeout( function() {
											callback(arg)
										}, 250 );
								}}],
						opened: function() {
							console.log("Save search dialog opened.");
							if (self.profileName != "View Last Search")
								$('input#saveProfile').val(self.profileName);
						}});
		}

		this.saveProfile = function(callback, arg) {
			if (getCookie("QuizNeedSaveProfile") != '1') {
				if (typeof callback != 'undefined')
					window.setTimeout( function() {
						callback(arg) }, 250 );
				return;
			}

			self = quizProfile;
			if (ahtb.opened) {
				window.setTimeout(function() {
					self.saveProfile(callback, arg);
				}, 200);
			}
			else { // wait just a bit more, just in case...
				window.setTimeout(function() {
					self.reallySaveProfile(callback, arg);
				}, 200);
			}		
		}

		this.login = function(callback, arg, message) {
			ahreg.setCallback(callback, arg);
			ahreg.extraMessage = message;
			if (typeof arg != 'undefined' &&
				arg.indexOf('http') != -1)
				ahreg.setRedirect(arg);
			ahreg.openModal();
		}

		this.storeResults = function(name, result) {
			if(typeof(Storage) !== "undefined") {
			    // Code for localStorage/sessionStorage.
			    try {
			    	sessionStorage.setItem(name, JSON.stringify( {id: result.index,
			    										  		  results: result} ) );
			    }
			    catch(e) {
			    	if (e.message.indexOf('exceeded the quota') !== -1) {
			    		console.log("Got a QUOTA_EXCEEDED_ERR exception, clearing sessionStorage and retry");
			    		sessionStorage.clear();
			    		try {
			    			sessionStorage.setItem(name, JSON.stringify( {id: result.index,
			    										  		 		 results: result} ) );
			    		}
			    		catch(e) {
			    			if (e.message.indexOf('exceeded the quota') !== -1)
			    				console.log("No Storage available on this browser:"+e.message);
			    			else
			    				console.log("Caught 2nd sessionStorage exception:"+e.message);
			    			return;
			    		}
			    		console.log("stored to sessionStorage on second try");
			    	}
			    	else
			    		console.log("Caught sessionStorage exception:"+e.message);
			    }
			} else {
			    console.log("No Storage available on this browser");
			}
		}

		this.retrieveResults = function(name) {
			if (typeof(Storage) == "undefined" ||
			    typeof sessionStorage.getItem(name) == 'undefined')
			    return null;
			else
				return JSON.parse( sessionStorage.getItem(name) );
		}

		this.get = {
			/**
			 * returns index in self.listings when given listing id
			 * @param  {int} id   listing id
			 * @return {int} index
			 */
			listing_index_by_id: function(id){
				self = quizProfile;
				if (self.listings && self.listings.length > 0)
					for (var i in self.listings)
						if (self.listings[i].id == id) {
							return i;
							break;
						}
			},

			city_by_id: function(id) {
				self = quizProfile;
				if (self.query.cities) {
					// for(var i in self.query.cities)
					// 	if (self.query.cities[i].id == id)
					// 		return self.query.cities[i];
					if (typeof self.query.cities[id] != 'undefined' &&
						typeof self.query.cities[id] != null)
						return self.query.cities[id];
				}
				return null;
			},

			city_sorted_by_id: function(id) {
				self = quizProfile;
				if (self.city_sorted) {
					for(var i in self.city_sorted)
						if (self.city_sorted[i].id == id)
							return self.city_sorted[i];
				}
				return null;
			},
			/**
			 * return HTML for listing, after image load
			 * @param  {int} id 		listing id
			 * @return {string}    	formatted HTML of listing
			 */
			listing_html: function(id){
				self = quizProfile;
				var d = self.get.listing_index_by_id( id );
				if (d !== null){
					d = self.listings[ d ];
					// local or remote file?
					var filepath;
					if (d.image_path.substr(0, 4) != 'http')
						filepath = ah_local.tp+'/_img/_listings/845x350/'+d.image_path;
					else filepath = d.image_path;

					var city = self.get.city_by_id(d.city_id);
					var cityName = typeof d.city == 'undefined' || d.city == null ? city.city : null;
					var state = typeof d.state == 'undefined' || d.state == null ? city.state : null;
					var street_address = typeof d.street_address == 'undefined' || d.street_address == null ? '' : d.street_address;

					var h = '<span class="listingimageclick"><a class="listing-link" href="javascript:quizProfile.goToListing('+d.id+')"></a><img src="'+filepath+'" /></span>'+
					// var h = '<a class="listing-link" href="'+ah_local.wp+"/listing/"+d.id+'"></a><img src="'+filepath+'" />'+
						'<a class="titlecenter" id="desktop" href="javascript:quizProfile.goToListing('+d.id+')"><span class="title">'+(d.flags & ListingFlag.LISTING_PERMIT_ADDRESS ? (street_address.length && street_address.length > 40 ? (street_address.substr(0, 40)+'...') : (street_address.length ? street_address : 'Address Hidden')) : 'Address Hidden') +'</span></a>'+
						'<a class="titlecenter" id="mobile" href="javascript:quizProfile.goToListing('+d.id+')"><span class="title">'+(d.flags & ListingFlag.LISTING_PERMIT_ADDRESS ? (street_address.length && street_address.length > 25 ? (street_address.substr(0, 25)+'...') : (street_address.length ? street_address : 'Address Hidden')) : 'Address Hidden') +'</span></a>'+
						'<div class="listing-info">'+
							'<div class="bottominfo">';
						if ( typeof d.percent_average != 'undefined' && d.percent_average )
							h += '<span id="split" class="percents"><span>'+d.percent_average+'%'+'</span> Match</span>';
							h += '<span id="split" class="bedandbath">'+
								(d.beds != null ? '<span class="beds">'+d.beds+' Bed</span>' : '');
									if (d.baths != null) {
										h+= '<span class="baths">, ';
										if (d.baths.toString().search('.') >= 0){
											var baths = d.baths.toString().split('.');
											h+= (isNaN(baths[0]) ? 0 : parseInt(baths[0])) + (isNaN(baths[1]) ? 0 : ('/' + parseInt(baths[1])) );
										} else h+= 0;
										h+= ' Bath</span>';
									}
								h+= '</span>'+
								(d.price != null ? '<span class="price" id="split">$'+d.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</span>' : '');
								h+= '<a class="viewlisting" href="javascript:quizProfile.goToListing('+d.id+')">View Listing <span class="entypo-right-dir"></span></a>'+
							'</div>' +
						'</div>';

					h +='<ul class="custom-menu" listing-id="'+d.id+'">'+
						  	'<li data-action="open">Open in New Tab</li>'+
						'</ul>';



					// city percent
					
					//var foundCity = city != null;
					//if (foundCity) 
					//	h+= '<div class="city-match">Area Score: <span>'+(typeof city.percent != 'undefined' && city.percent > 0 ? (city.percent >= 100 ? 100 : city.percent)+'%' : 'N/A')+'</span></div></div>';
					//else
					//	h+= '<div class="city-match">Area Score: <span>N/A</span></div>';
					
					// for (var i in self.query.cities)
					// 	if (self.query.cities[i].id == d.city_id) {
					// 		h+= '<div class="city-match">Area <span>'+(typeof self.query.cities[i].percent != 'undefined' && self.query.cities[i].percent > 0 ? (self.query.cities[i].percent >= 100 ? 100 : self.query.cities[i].percent)+'%' : 'N/A')+'</span></div></div>';
					// 		foundCity = true;
					// 		break;
					// 	}

					// if (!foundCity)
					// 	h+= '<div class="city-match">Area <span>N/A</span></div>';

					
					var t = '';
					if (city != null &&
						Object.keys(self.query.cityTags).length) {
						// var tagKeys = city.tags ? Object.keys(city.tags) : [];
						var rowCount = 0;
						var needTableHead = true;
						// for(var i in tagKeys) {
						for(var i in city.tags) {
							var tagKey = city.tags[i].tag;
							if (typeof self.query.cityTags[tagKey] != 'undefined' &&
								city.tags[i].score >= 7) {
								// got one!

								//console.log('City id:'+city.id+' - icon:'+self.query.cityTags[tagKeys[i]].icon);
								if (needTableHead) {
									needTableHead = false;
									t = '<table class="tag-icons">';
								}
								if ( (rowCount % self.defaults.iconRowCount) == 0 )
									t += '<tr>';
								t += '<td><a href="javascript:explainIcons(self.query.cityTags);"><img src="'+ah_local.tp+'/_img/tag_icons/'+self.query.cityTags[tagKey].icon+'"/></a></td>';
								if ( (rowCount % self.defaults.iconRowCount) == 3 )
									t += '</tr>';
								rowCount++;
							}
						}
						if (!needTableHead) // then close it!
							t += '</table>';
					}
					h += t;

					return h;
				} else {
					console.error( "Listing ID "+id+" not found." );
					return false;
				}
			},

			city_html: function(id){
				self = quizProfile;
				var d = self.get.city_by_id(id);
				if (typeof d != 'undefined' &&
					d !== null) {
					// local or remote file?
					var filepath;
					if (d.image.substr(0, 4) != 'http')
						filepath = ah_local.tp+'/_img/_cities/845x350/'+d.image;
					else filepath = d.image;

					var city_sorted = self.get.city_sorted_by_id(id);

					var h = '<span class="cityimageclick"><a class="city-link" href="javascript:quizProfile.switchToListing('+d.id+')"></a><img src="'+filepath+'" /></span>';
					// var h = '<a class="listing-link" href="'+ah_local.wp+"/listing/"+d.id+'"></a><img src="'+filepath+'" />'+
					h+= '<div class="city-info">'+
							'<div id="header">' +
								'<span class="title"><span class="number">'+(city_sorted.index+1)+'</span>'+d.city+', '+d.state+'</span>'+
								'<div class="city-match">Lifestyle Match: <span>'+(typeof d.percent != 'undefined' && d.percent > 0 ? (d.percent >= 100 ? 100 : d.percent)+'%' : 'N/A')+'</span></div>' +
								'<a class="city-link" href="javascript:quizProfile.switchToListing('+d.id+')"><button id="seeAll" city-id="'+id+'">See all '+city_sorted.total+' listings <span class="entypo-right-dir"></span></button></a>' +
							'</div>'+

							// '<span class="title">'+d.city+', '+d.state+'</span>'+
							//'<span class="mobiletitle">'+(d.city != null ? d.city+(d.state != null ? ', ' : '') : '')+(d.state != null ? d.state : '')+'</span>';
							
							//h+= '<table class="mobiletable">';
								
							//h+= '</table>'+
						'</div>';
						// '<div class="percents"><div class="home-match">Home <span>'+(typeof d.percent != 'undefined' && d.percent ? d.percent+'%' : 'N/A')+'</span></div>';

					
					// h+= '<div class="city-match">Area <span>'+(typeof d.percent != 'undefined' && d.percent > 0 ? (d.percent >= 100 ? 100 : d.percent)+'%' : 'N/A')+'</span></div></div>';
					
					//var thecityValue = city_sorted;
					//$(".city-info #header .title .number").thecityValue = function(thecityValue){
					//	thecityValue = thecityValue + 1;
					//	$(".city-info #header .title .number").html(theValue);
					//}
					//quiz.cityval();
					var city = d;
					var t = '';
					if (city != null &&
						Object.keys(self.query.cityTags).length) {
						// var tagKeys = city.tags ? Object.keys(city.tags) : [];
						var rowCount = 0;
						var needTableHead = true;
						// for(var i in tagKeys) {
						for(var i in city.tags) {
							var tagKey = city.tags[i].tag;
							if (typeof self.query.cityTags[tagKey] != 'undefined' &&
								city.tags[i].score >= 3) {
								// got one!

								//console.log('City id:'+city.id+' - icon:'+self.query.cityTags[tagKeys[i]].icon);
								if (needTableHead) {
									needTableHead = false;
									t = '<table class="tag-icons">';
								}
								if ( (rowCount % self.defaults.iconRowCountCity) == 0 )
									t += '<tr>';
								t += '<td>'+
										'<div id="tag">'+
											'<a href="javascript:explainIcons(self.query.cityTags);"><img src="'+ah_local.tp+'/_img/tag_icons/'+self.query.cityTags[tagKey].icon+'"/></a>'+
											'<span>'+self.query.cityTags[tagKey].tag+'</span>'+
											'<div class="rateit rateit-range" data-rateit-value="'+(city.tags[i].score/2).toFixed(2)+'" data-rateit-ispreset="true" data-rateit-readonly="true"></div>'+
										'</div>'+
									'</td>';
								if ( (rowCount % self.defaults.iconRowCountCity) == 3 )
									t += '</tr>';
								rowCount++;
							}
							if (rowCount >= self.maxTagsToShow)
								break;
						}
						if (!needTableHead) // then close it!
							t += '</table>';
					}
					t+= '<div id="sub-listing-div"><ul id="sub-listings">';
					
					if ( typeof city_sorted.mapped  == 'undefined' ||
						 !(city_sorted.mapped & ListingMapMode.DID_PRIMARY_LISTING_KEY_MAPPING) ) {
						city_sorted.mapped = ListingMapMode.DID_PRIMARY_LISTING_KEY_MAPPING;
						for(var i in city_sorted.listings) {
							for(var j in city_sorted.listings[i]) {
								if (typeof reverseListingKeyMap[j] != 'undefined') {
									city_sorted.listings[i][ reverseListingKeyMap[j] ] = city_sorted.listings[i][j];
									delete city_sorted.listings[i][j];
								}
							}
							city_sorted.listings[i].city_id = id; // set it here, save space
						}
					}

					for(var i in city_sorted.listings) {
						var id = city_sorted.listings[i].id; // to re-use code easier
						// var image_path = city_sorted.listings[i].image_path.substr(0, 4) != 'http' ? ah_local.tp+'/_img/_listings/210x120/'+ city_sorted.listings[i].image_path 
						// 																					:  city_sorted.listings[i].image_path;

						t+= '<li class="sub-listing sub-listing-collapsed" style="display:none;" listing-id="'+id+'">'+
								'<div class="spin-wrap"><div class="spinner sphere">'+
				              '</div></div>'+
				              'Loading listing: '+"ID:"+id.toString()+'</li>';
					}
					t+= '</ul></div>';
					h += t;


					return h;
				} else {
					console.error( "City ID "+id+" not found." );
					return false;
				}
			},

			subListing_html: function(d) {
				var image_path = d.image_path.substr(0, 4) != 'http'  ? ah_local.tp+'/_img/_listings/210x120/'+ d.image_path 
																	: d.image_path;
				var baths = '';
				if (d.baths != null) 
					if (d.baths.toString().search('.') >= 0) {
						var baths = d.baths.toString().split('.');
						baths = (isNaN(baths[0]) ? 0 : parseInt(baths[0])) + (isNaN(baths[1]) ? '' : (parseInt(baths[1]) > 0 ? '.5' : '') );
					} 
					else 
						baths = d.baths;

				var desc = (d.price != null ? '$'+d.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '');
						   
				var t = '<a href="javascript:quizProfile.goToListing('+d.id+');"><img src="'+image_path+'" >'+
						'<div class="listing-info">'+
							// '<span class="title">'+(d.flags & ListingFlag.LISTING_PERMIT_ADDRESS ? (d.street_address.length > 40 ? (d.street_address.substr(0, 40)+'...') : d.street_address) : 'Address hidden') +'</span>'+
							// '<span class="mobiletitle">'+(d.city != null ? d.city+(d.state != null ? ', ' : '') : '')+(d.state != null ? d.state : '')+'</span>'+
							'<table class="desktoptable">'+
								 '<tr class="desc"><th>'+desc+'</th><th>'+d.percent_average+'%'+'</th></tr>';
								// (d.city != null || d.state != null ? '<tr class="location"><th>'+(d.city != null ? d.city+(d.state != null ? ', ' : '') : '')+(d.state != null ? d.state : '')+'</th></tr>' : '')+
								// (d.country != null && d.country != 'US' ? '<tr class="country"><th>'+d.country+'</th></tr>' : '')+
								// (d.price != null ? '<tr class="price"><th>$'+d.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</th></tr>' : '')+
								// (d.lotsize != null ? '<tr class="lotsize"><th>Lot '+d.lotsize.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+' '+(d.lotsize_std == 'acres' ? d.lotsize_std : (d.lotsize_std == 'm' ? 'm<sup>2</sup>' : 'ft<sup>2</sup>'))+'</th></tr>' : '')+
								// (d.interior != null ? '<tr class="interior"><th>Home '+d.interior.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+' '+(d.interior_std == 'acres' ? d.interior_std : (d.interior_std == 'm' ? 'm<sup>2</sup>' : 'ft<sup>2</sup>'))+'</th></tr>' : '')+
								// (d.beds != null ? '<tr class="beds"><th>'+d.beds+' Beds</th></tr>' : '');
								// if (d.baths != null) {
								// 	t+= '<tr class="baths"><th>';
								// 	if (d.baths.toString().search('.') >= 0){
								// 		var baths = d.baths.toString().split('.');
								// 		t+= (isNaN(baths[0]) ? 0 : parseInt(baths[0])) + (isNaN(baths[1]) ? 0 : (' / ' + parseInt(baths[1])) );
								// 	} else t+= 0;
								// 	t+= ' Baths</th></tr>';
								// }
							t+= '</table>'+
								'<table class="mobiletable">'+
									'<tr class="desc"><th>'+desc+'</th></tr>';
								// (d.price != null ? '<tr class="price"><th>$'+d.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</th></tr>' : '')+
								// (d.beds != null ? '<tr class="beds"><th>'+d.beds+' Beds - ' : '');
								// if (d.baths != null) {
								// 	t+= '<span class="baths">';
								// 	if (d.baths.toString().search('.') >= 0){
								// 		var baths = d.baths.toString().split('.');
								// 		t+= (isNaN(baths[0]) ? 0 : parseInt(baths[0])) + (isNaN(baths[1]) ? 0 : (' / ' + parseInt(baths[1])) );
								// 	} else t+= 0;
								// 	t+= ' Baths</span></th></tr>';
								// }
							t+= '</table>'+
						'</div></img></a>' +
						'<ul class="custom-menu" listing-id="'+d.id+'">'+
						  	'<li data-action="open">Open in New Tab</li>'+
						'</ul>';
				return t;
			},

			/**
			 * loads and displays listings to stored by get.results() results list
			 * @param  {int} pageNumber 	page to get
			 */
			page: function(pageNumber){
				switch(quizProfile.primary) {
					case 'city':
						this.showCities(pageNumber);
						break;
					case 'listings':
						this.showListings(pageNumber);
						break;
				}
			},

			showListings: function(pageNumber) {
				self = quizProfile;
				if (self.listings && self.listings.length > 0){
					if (typeof pageNumber == 'undefined')
						pageNumber = 0;

					
					per_page = pageNumber === 0 ? self.defaults.perPage[self.primary].for_first_page : self.defaults.perPage[self.primary].per_page;
					index = pageNumber > 0 ? (pageNumber-1) * per_page + self.defaults.perPage[self.primary].for_first_page : pageNumber * per_page;
					max_index = index + per_page;

					if (self.defaults.perPage[self.primary].for_first_page != 7)
						self.justOpened = true;

					// if (self.scrollData[self.primary].scrollPage) {
					// 	self.listingPageNumber = pageNumber = self.scrollData[self.primary].scrollPage;
					// 	self.defaults.perPage[self.primary].for_first_page = 7;
					// }
					// setCookie("QuizListingPos", pageNumber, 2);

					var listhub_viewed = [];
					var listings_viewed = [];
					var cities_viewed = {};

					console.log("going to display "+max_index+" listings");
					
					if ( index == 0 ) {
						var city = self.get.city_by_id(quizProfile.activeCityId);
						var name = city && city.city && city.state ? city.city+', '+city.state : "Best Match Results";
						// make tab icons
						var t = '';
						if (city != null &&
							Object.keys(self.query.cityTags).length) {
							// var tagKeys = city.tags ? Object.keys(city.tags) : [];
							var rowCount = 0;
							var needTableHead = true;
							// for(var i in tagKeys) {
							for(var i in city.tags) {
								var tagKey = city.tags[i].tag;
								if (typeof self.query.cityTags[tagKey] != 'undefined' &&
									city.tags[i].score >= 5) {
									// got one!

									//console.log('City id:'+city.id+' - icon:'+self.query.cityTags[tagKeys[i]].icon);
									if ( (rowCount % self.defaults.iconRowCount) == 0 )
										t += '<tr>';
									t += '<td><a href="javascript:explainIcons(self.query.cityTags);"><img src="'+ah_local.tp+'/_img/tag_icons/'+self.query.cityTags[tagKey].icon+'"/></a></td>';
									if ( (rowCount % self.defaults.iconRowCount) == 3 )
										t += '</tr>';
									rowCount++;
								}
								if (rowCount == self.maxTagsToShowListingView)
									break;
							}
							
						}
						$('div.citynamebanner .tag-icons').html(t);
						$('div.citynamebanner .cityname').html(name);
						if (city.percent) {
							$('div.citynamebanner .score span').html(city.percent+'%');
							$('div.citynamebanner .score').show();
						}
						else
							$('div.citynamebanner .score').hide();
					}

					self.listingViewcount = 0;
					while ( index <= self.listings.length && index < max_index ){
						if (self.listings[index]) {
							var viewed = {lkey: self.listings[index].listhub_key};
							listhub_viewed[listhub_viewed.length] = viewed;
							if ( typeof cities_viewed[self.listings[index].city_id] == 'undefined' ||
								 cities_viewed[self.listings[index].city_id] == null)
								cities_viewed[self.listings[index].city_id] = 1;
							else
								cities_viewed[self.listings[index].city_id]++;
								//cities_viewed[cities_viewed.length] = self.listings[index].city_id;
							listings_viewed[listings_viewed.length] = self.listings[index].id;
							// spinner for image loading, start hidden
							var ele = $('div#listings ul#listings');
							$('div#listings ul#listings').append('<li class="listing listing-collapsed" style="display:none;" listing-id="'+self.listings[index].id+'">'+
								'<div class="spin-wrap"><div class="spinner sphere">'+
				              '</div></div>'+
				              'Loading listing: '+(typeof self.listings[index].title != 'undefined' && self.listings[index].title ? self.listings[index].title : "ID:"+self.listings[index].id.toString())+'</li>'
				            );
				            self.listingViewcount++;

	           			 	// display collaped listing, prepare for and begin image load
							$('ul#listings [listing-id='+self.listings[index].id+']').fadeIn(250,function(){
								self = quizProfile;
								
								var listing = self.listings[ self.get.listing_index_by_id( $(this).attr('listing-id') ) ];
								var li = $(this);
								// $('.spinner', li).addClass('sphere').fadeIn(250, function(){});
								//$('.spinner', li).addClass('sphere');

								if (typeof listing.image_path == 'undefined')
									listing.image_path = '_blank.jpg';
								if (listing.image_path.substr(0, 4) != 'http')
									filepath = ah_local.tp+'/_img/_listings/845x350/'+listing.image_path;
		 						else filepath = listing.image_path;
								listing.html_image = new Image();
								listing.html_image.onerror = function(){
									self.listingViewcount--;
									$('ul#listings [listing-id='+listing.id+']').fadeOut({
										duration: 250,
										queue: true,
										complete:function(){
											$(this).finish().height(50).html('<p style="line-height: 50px;margin: 0 1em;"><strong>Error:</strong> Unable to load listing image.</p>').fadeIn(250);
										}
									});
								}
								listing.html_image.onload = function(){
									$('.spinner', li).removeClass('sphere').fadeOut(250, function(){});
									$('ul#listings [listing-id='+listing.id+']').fadeOut({
										duration: 250,
										queue: true,
										complete:function(){
											self = quizProfile;
											$(this).finish().removeClass('listing-collapsed')
															.html(self.get.listing_html( listing.id ) )
															.fadeIn(250, function() { self.listingViewcount--; });
															$.fn.capitalise = function() {
																return this.each(function() {
																		var $this = $(this),
																				text = $this.text(),
																				split = text.split(' '),
																				res = [],
																				i,
																				len,
																				component;
																		for (i = 0, len = split.length; i < len; i++) {
																				component = split[i];
																				res.push(component.substring(0, 1).toUpperCase());
																				res.push(component.substring(1).toLowerCase());
																				res.push(" "); // put space back in
																		}
																		$this.text(res.join(''));
																});
															};
															$('#quiz-results ul#listings>li.listing .titlecenter .title').capitalise();

											var ele = $('ul#listings [listing-id='+listing.id+']');
											var custom_menu = ele.find('ul.custom-menu');
											$( custom_menu, "li").click(function(){
												var id = custom_menu.attr('listing-id');
												quizProfile.goToListing(id, true);
												custom_menu.hide();
											});

											ele.contextmenu(function(e) {
												console.log( "entered contextmenu");
												e.preventDefault();
												$('.custom-menu').hide();
												custom_menu.finish().toggle(100).
												    // In the right position (the mouse)
												    css({
												        top: e.offsetY + "px",
												        // top: "-180px",
												        left: e.offsetX + "px"
												    });
											})
										}
									});
								}
								// load the image
								listing.html_image.src = filepath;
							});
						}
						else max_index++;
						index++;
					}

					self.get.setListingTop(pageNumber, listhub_viewed, cities_viewed, listings_viewed);
				}

				this.doHash();
			},

			setListingTop: function(pageNumber, listhub_viewed, cities_viewed, listings_viewed) {
				self = quizProfile;
				if (self.listingViewcount) {
					window.setTimeout(function() {
						self.get.setListingTop(pageNumber, listhub_viewed, cities_viewed, listings_viewed);
					}, 100);
					return;
				}

				console.log("setListingTop being serviced for pageNumber:"+pageNumber);

				if (self.scrollData[self.primary].scrollTop && self.justOpened) {
					var ele = $('div#listings ul');
					ele.scrollTop(self.scrollData[self.primary].scrollTop);
					self.listingPageNumber = self.scrollData[self.primary].scrollPage;
					self.defaults.perPage[self.primary].for_first_page = 7;
				}
				else {
					// self.scrollData[self.primary].scrollTop = $('#listings').scrollTop();
					self.scrollData[self.primary].scrollPage = pageNumber;
					setCookie("QuizListingPos", pageNumber, 2);
					// setCookie('QuizListingTop', self.scrollData[self.primary].scrollTop, 2);
					if (ah_local.ip != "1")
						lh('submit', 'SEARCH_DISPLAY', listhub_viewed);

					self.DB({query: 'update-analytics',
						 data: {cities: cities_viewed,
						 		listings: listings_viewed},
						 error: function(d) {
						 	console.log("update-analytics - got an error:"+d);
						 }});
				}

				self.justOpened = false;
			},

			showCities: function(pageNumber, append, startFromIndex) {
				self = quizProfile;
				if (self.citiesViewcount &&
					typeof append == 'undefined') {
					if (pageNumber)
						self.cityPageNumber--;
					console.log("showCities has citiesViewcount:"+self.citiesViewcount+", returning..");
					return;
				}

				if (self.city_sorted && self.city_sorted.length > 0){
					if (typeof pageNumber == 'undefined')
						pageNumber = 0;

					var primary = 'city';
					per_page = pageNumber === 0 ? self.defaults.perPage[primary].for_first_page : self.defaults.perPage[primary].per_page;
					if (typeof append == 'undefined' ||
						typeof startFromIndex == 'undefined') {
						index = pageNumber > 0 ? (pageNumber-1) * per_page + self.defaults.perPage[primary].for_first_page : pageNumber * per_page;
						max_index = index + per_page;
					}
					else {
						console.log("showCities - append is true, startIndex:"+startFromIndex+", perPage:"+per_page);
						index = startFromIndex;
						max_index = per_page;
					}

					if (self.defaults.perPage[primary].for_first_page != 7) {
						self.justOpened = true;
						$('ul#cities').scrollTop(self.scrollData[primary].scrollTop);
					}

					var justOpened = self.justOpened;

					if (index >= self.city_sorted.length ) {
						console.log("showCities - index:"+index+" is at or above city_sorted length:"+self.city_sorted.length);
						self.cityPageNumber--;
						return;
					}

					// if (self.scrollData[self.primary].scrollPage) {
					// 	self.cityPageNumber = pageNumber = self.scrollData[self.primary].scrollPage;
					// 	self.defaults.perPage[self.primary].for_first_page = 7;
					// }
					// setCookie("QuizCityPos", pageNumber, 2);

					// var listhub_viewed = [];
					var listings_viewed = [];
					var cities_viewed = {};
					// self.citiesViewcount = 0;
					while ( index <= self.city_sorted.length && index < max_index ){
						if (self.city_sorted[index]) {
							self.citiesViewcount++;
							console.log("showCities, index:"+index+" increment self.citiesViewcount to "+self.citiesViewcount);
							if ( typeof cities_viewed[self.city_sorted[index].id] == 'undefined' ||
								 cities_viewed[self.city_sorted[index].id] == null)
								cities_viewed[self.city_sorted[index].id] = 1;
							else
								cities_viewed[self.city_sorted[index].id]++;
								//cities_viewed[cities_viewed.length] = self.listings[index].city_id;
							// spinner for image loading, start hidden
							self.city_sorted[index].index = index;

							var city = self.get.city_by_id(self.city_sorted[index].id);
							var city_id = self.city_sorted[index].id;
							$('ul#cities').append('<li class="city city-collapsed" style="display:none;" city-id="'+city_id+'">'+
								'<div class="spin-wrap"><div class="spinner sphere">'+
				              '</div></div>'+
				              'Loading city: '+city.city+', '+city.state+'</li>'
				            );

				            // display collaped listing, prepare for and begin image load
				            var li = $('ul#cities li[city-id='+city_id+']');
							li.fadeIn(250,function(){
								self = quizProfile;
								
								var city_id = $(this).attr('city-id');
								var city = self.get.city_by_id( city_id );
								// var li = $(this);
								// $('.spinner', li).addClass('sphere').fadeIn(250, function(){});
								//$('.spinner', li).addClass('sphere');

								if (typeof city.image == 'undefined')
									city.image = '_blank.jpg';
								if (city.image.substr(0, 4) != 'http')
									filepath = ah_local.tp+'/_img/_cities/845x350/'+city.image;
		 						else filepath = city.image;
								city.html_image = new Image();
								city.html_image.onerror = function(){
									self.citiesViewcount--;
									console.log("city.html_image.onerror, decrement self.citiesViewcount to "+self.citiesViewcount);
									$('#cities [city-id='+city.id+']').fadeOut({
										duration: 250,
										queue: true,
										complete:function(){
											$(this).finish().height(50).html('<p style="line-height: 50px;margin: 0 1em;"><strong>Error:</strong> Unable to load city image.</p>').fadeIn(250);
										}
									});
								}
								city.html_image.onload = function(){
									$('.spinner', li).removeClass('sphere').fadeOut(50, function(){});
									$('#cities [city-id='+city_id+']').fadeOut({
										duration: 250,
										queue: true,
										complete:function(){
											$(this).finish().removeClass('city-collapsed').html( quizProfile.get.city_html( city_id, index ) ).fadeIn({duration:100, complete: function() {
												self.citiesViewcount--;
												console.log("city.html_image.onload, decrement self.citiesViewcount to "+self.citiesViewcount);
												$('.rateit').rateit();

												var city_sorted = self.get.city_sorted_by_id(city_id);
												for(var i in city_sorted.listings) 
													self.get.showSubListing(city_sorted.listings[i]);
												var ele = $('button#seeAll[city-id='+city_id+']');
												ele.on('click', function() {
													self.switchToListing(city_id);
												})
											}});
										}
									});
								}
								// load the image
								city.html_image.src = filepath;
							});
						}
						else max_index++;
						index++;
					}

					self.get.setCitiesTop(justOpened, pageNumber, cities_viewed, listings_viewed);

				}
				else if (self.cityPageNumber)
					self.cityPageNumber--;	

				this.doHash();
			},

			setCitiesTop: function(justOpened, pageNumber, cities_viewed, listings_viewed) {
				self = quizProfile;
				if (self.citiesViewcount) {
					window.setTimeout(function() {
						self.get.setCitiesTop(justOpened, pageNumber, cities_viewed, listings_viewed);
					}, 100);
					return;
				}

				var primary = 'city';
				console.log("setCitiesTop being serviced for pageNumber:"+pageNumber+", top:"+self.scrollData[primary].scrollTop);

				if (self.scrollData[primary].scrollTop && justOpened) {
					$('ul#cities').scrollTop(self.scrollData[primary].scrollTop);
					self.cityPageNumber = self.scrollData[primary].scrollPage;
					self.defaults.perPage[primary].for_first_page = 7;
					console.log("showCities justOpened, scrollTop is "+self.scrollData[primary].scrollTop);
				}
				else {
					// self.scrollData[self.primary].scrollTop = $('#cities').scrollTop();
					self.scrollData[primary].scrollPage = pageNumber;
					setCookie("QuizCityPos", pageNumber, 2);
					// setCookie('QuizCityTop', self.scrollData[self.primary].scrollTop, 2);
					self.DB({query: 'update-analytics',
						 data: {cities: cities_viewed,
						 		listings: listings_viewed},
						 error: function(d) {
						 	console.log("update-analytics - got an error:"+d);
						 }});
				}

				self.justOpened = false;
			},

			showSubListing: function(listing) {
				// display collaped listing, prepare for and begin image load
				var ele = $('li.city[city-id='+listing.city_id+'] #sub-listings li[listing-id='+listing.id+']');
				// $('#sub-listings li[listing-id='+listing.id+']').fadeIn(250,function(){
				ele.fadeIn(250,function(){
					self = quizProfile;
					// var listing = self.listings[ self.get.listing_index_by_id( $(this).attr('listing-id') ) ];
					var li = $(this);
					// $('.spinner', li).addClass('sphere').fadeIn(250, function(){});
					//$('.spinner', li).addClass('sphere');

					// if (typeof listing.image_path == 'undefined')
					// 	listing.image_path = '_blank.jpg';
					// if (listing.image_path.substr(0, 4) != 'http')
					// 	filepath = ah_local.tp+'/_img/_listings/845x350/'+listing.image_path;
					// 	else filepath = listing.image_path;
					var filepath = listing.image_path.substr(0, 4) != 'http'  ? ah_local.tp+'/_img/_listings/210x120/'+ listing.image_path 
																			  : listing.image_path;
					listing.html_image = new Image();
					listing.html_image.onerror = function(){
						// $('#sub-listings li[listing-id='+listing.id+']').fadeOut({
						ele.fadeOut({
							duration: 250,
							queue: true,
							complete:function(){
								$(this).finish().height(50).html('<p style="line-height: 50px;margin: 0 1em;"><strong>Error:</strong> Unable to load listing image.</p>').fadeIn(250);
							}
						});
					}
					listing.html_image.onload = function(){
						$('.spinner', li).removeClass('sphere').fadeOut(250, function(){});
						// $('#sub-listings li[listing-id='+listing.id+']').fadeOut({
						ele.fadeOut({
							duration: 250,
							queue: true,
							complete:function(){
								$(this).finish().removeClass('sub-listing-collapsed')
										.html( self.get.subListing_html( listing ) )
										.fadeIn(250);

								var a = $(ele, 'a');
								// a.on('mouseup', function(e) {
								// 	var e = e || window.event;
								// 	switch (e.which) {
								// 	    case 1: console.log('left up'); break;
								// 	    case 2: console.log('middle up'); break;
								// 	    case 3: console.log('right up'); break; 
								// 	  }
								// });

								// a.on('mousedown', function(e) {
								// 	var e = e || window.event;
								// 	switch (e.which) {
								// 	    case 1: console.log('left down'); break;
								// 	    case 2: console.log('middle down'); break;
								// 	    case 3: console.log('right down'); break; 
								// 	  }
								// })

								var custom_menu = ele.find('ul.custom-menu');
								$( custom_menu, "li").click(function(){
									var id = custom_menu.attr('listing-id');
									quizProfile.goToListing(id, true);
									custom_menu.hide();
								});

								a.contextmenu(function(e) {
									console.log( "entered contextmenu");
									e.preventDefault();
									$('.custom-menu').hide();
									custom_menu.finish().toggle(100).
									    // In the right position (the mouse)
									    css({
									        // top: e.pageY + "px",
									        top: "30px"
									        // left: e.pageX + "px"
									    });
								})
							}
						});
					}
					// load the image
					listing.html_image.src = filepath;
				});
			},

			doHash: function() {
				switch(hash) {
					case 'saveProfile':
						self.saveProfile();
						break;
					case 'selectMyProfile':
						self.saveProfile(self.selectMyProfile);
						break;
					case 'mobileselectMyProfile':
						self.saveProfile(self.mobileselectMyProfile);
						break;
					case 'selectCities':
						self.popup.cities();
						break;
				}
				hash = "";
			},

			getPartialResults: function() {
				self = quizProfile;
				if (self.ignorePartialResults)
					return;
				self.requestingPartialResults = true;
				theData = { //sessionID: sessionToUse, // this will be added by this.DB()
							quizId: quiz_id }; // should be -1 or actual row index of QuizActivity
				quizProfile.DB({
					query: 'partial-quiz-run',
					data: theData,
					error: function(d){
						self.requestingPartialResults = false;
						if (typeof d != 'undefined' &&
							d != null) {
							if (typeof d == 'string') {
								// if ( d.indexOf('No results') != -1 || 
								// 	 d.indexOf('No activity found for session') != -1 ) {
										ahtb.open({
											height: 150,
												width: 480,
												title: 'Quiz Results',
												html:'<p>A problem:'+d+'</p>',
												buttons: [
													{text: 'Take Quiz', action:function(){
														ahtb.close();
													}}
												]
										});
									return;
								// }
							}
							else if (typeof d == 'object') {
								ahtb.alert("Unknown failure, please refresh page: "+JSON.stringify(d));
								return;
							}
						}
						ahtb.alert("Unknown failure, please refresh page");
					},
					done: function(d){
						self.requestingPartialResults = false;
						if (typeof d == 'string') {
							ahtb.open({
									height: 150,
										width: 480,
										title: 'Quiz Results',
										html:'<p>A problem:'+d+'</p>',
										buttons: [
											{text: 'Take Quiz', action:function(){
												ahtb.close();
											}}
										]
								});
							return;
						}
						else
							self.processPartial(d);
					}
				})
			},

			/**
			 * queries DB for last result set and parses listings, tags, price; adds listings to map
			 * @param  {object} newQuery    pass new tag array, location array, and / or price array
			 * @param  {bool} reparseTags 	parse and display tags in tag editor window
			 */
			results: function(newQuery, reparseTags){
				quizProfile.ignorePartialResults = true;
				if ( quizProfile.requestingPartialResults) {
					window.setTimeout( function() {
						quizProfile.get.results(newQuery, reparseTags);
					}, 200);
					return;
				}

				if (reparseTags !== false) reparseTags = true;
				setCookie("QuizCompleted", quiz_id, 2);
				setCookie("QuizId", quiz_id, 2);
				setCookie("QuizSession", ah_local.sessionID, 2);
				setCookie("ACTIVE_SESSION_ID", ah_local.activeSessionID, 2);
				console.log("quiz-results - getting results for quizId:"+quiz_id+", session:"+ah_local.sessionID+", activeSessionID:"+ah_local.activeSessionID);
				
				var sessionToUse = ah_local.sessionID;
				var activeSessionID = ah_local.activeSessionID;
				
				var title = newQuery ? '<strong>New Search. Sorting through '+listHubFeedSize.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+' listings...</strong>' : 'Loading quiz results...';
				$('.results-error-overlay').hide();
				$('.results-loading-overlay').show();
				$('.results-loading-overlay').css('opacity', '1');
						$('ul#listings').empty();
						theData = { //sessionID: sessionToUse, // this will be added by this.DB()
									quizId: quiz_id }; // should be -1 or actual row index of QuizActivity
						// update data and clear up existing profile cache..
						if (newQuery) {
							for (var i in newQuery)
								theData[i] = newQuery[i];
							setCookie('ProfileData', '', 1);
							if (profiles) 
								profiles = null;
						}

						quizProfile.DB({
							query: 'new-result-from-query',
							data: theData,
							error: function(d){
								if (typeof d != 'undefined' &&
									d != null) {
									if (typeof d == 'string') {
										if ( d.indexOf('No results') != -1 || 
											 d.indexOf('No activity found for session') != -1 ) {
												ahtb.open({
													height: 165,
														width: 480,
														title: 'Quiz Results',
														html:'<p>You have not yet taken the quiz, please take the quiz before trying to view results.</p>',
														buttons: [
															{text: 'Take Quiz', action:function(){
																ahtb.close();
																setTimeout( function(){ ah.openModal('find-a-home'); }, 250 );
															}}
														]
												});
											return;
										}
									}
									else if (typeof d == 'object') {
										ahtb.alert("Unknown failure, please refresh page: "+JSON.stringify(d));
										return;
									}
								}
								ahtb.alert("Unknown failure, please refresh page");
							},
							done: function(d){
								ah_local.sessionID = sessionToUse; // let's restore it, since ahtb call may have reverted it back.
								ah_local.activeSessionID = activeSessionID; // let's restore it, since ahtb call may have reverted it back.
								self = quizProfile;
								if (newQuery)
									self.updateResultsAnalytics();

								if (typeof d == 'string') {
									ahtb.open({
										height: 165,
											width: 480,
											title: 'Quiz Results',
											html:'<p>You have not yet taken the quiz, please take the quiz before trying to view results.</p>',
											buttons: [
												{text: 'Take Quiz', action:function(){
													ahtb.close();
													setTimeout( function(){ ah.openModal('find-a-home'); }, 250 );
												}}
											]
									});
								}
								else {
									self.processResults(d, reparseTags);	
									if (getCookie('QuizViewSelector') == 'map') {
										self.map.hidden = true;
										quizProfile.map.show();
									}	
								}	
							}
						});
			}
		}
		this.fixRange = function(price) {
			if (price == -1)
				return price;

			if (price == (lastPrice-1))
				return price;

			if (price >= 8000000) {
				var mil = Math.round(price/1000000);
				price = (price % 2000000) == 0 ? price : mil * 1000000;
			}
			else if (price >= 3000000) {
				var fifth = Math.round(price / 500000);
				price = (price % 500000) == 0 ? price : fifth * 500000;
			}
			else {
				//var twohun = Math.round(price / 200000);
				//price = (price % 200000) == 0 ? price : twohun * 200000;
				price = (price / 10000) * 10000; // strip the one
			}
			return price;
		}
		
		this.makeToggle = function(i, tag) {
			var h = '<span class="fh-switch" tag-index="'+i+'">'+
					   	'<input type="checkbox" id="switch-id" name="myswitch" tag-id="'+tag.id+'" '+(tag.flag == 1 ? 'checked' : '')+'>'+
					   	'<label for="switch-id"></label>'+
					   	'<span class="fh-switch-knob"></span>'+
					'</span>';
			return h;
		}

		this.fixCityTagOrder = function(d) {
			for(var i in d.cities) {
				arr = [];
				for(var j in d.cities[i].tags)
					arr.push( {'tag': j,
							   'score': d.cities[i].tags[j]} );
				arr.sort(function(a, b) {
			        return b.score - a.score; //  sorts in desc order, do a - b for asc
			    });

			    // taglist = {};
			    // for(var k in arr) {
			    // 	var tag = arr[k].tag;
			    // 	taglist.tag = arr[k].score;
			    // }

			    d.cities[i].tags = arr;
			}
		}

		this.cleanTags = function() {
			self = quizProfile;
			for (var i in self.query.tags) {
				if ( typeof self.query.tags[i].removed != 'undefined' )
					delete self.query.tags[i].removed;
				if ( typeof self.query.tags[i].origFlag != 'undefined' )
					delete self.query.tags[i].origFlag;
				if ( typeof self.query.tags[i].origPercent != 'undefined' )
					delete self.query.tags[i].origPercent;
			}
		}

		this.processPartial = function(d) {
			self = quizProfile;
			var existingCount = self.city_sorted.length;
			for(var i in d.city_sorted) {
				if (i < existingCount)
					continue;
				// d.city_sorted[i].index = i;
				self.city_sorted[self.city_sorted.length] = d.city_sorted[i];
			}

			// self.query = d;
			self.query.cities = length(d.cities) ? d.cities : self.query.cities;

			var allowPartialQuizResults = typeof d.allowPartialQuizResults == 'undefined' ? 0 : d.allowPartialQuizResults;

			self.storeResults('results'+d.index, d);

			self.fixCityTagOrder(d);
			// self.cleanTags();

			self.map.updateMarkers(existingCount); // this will add new markers
			self.map.setMapBounds();

			console.log("processPartial - cityPageNumber:"+self.cityPageNumber+", existingCount:"+existingCount+', city_sorted:'+self.city_sorted.length+", firstPageCount:"+self.defaults.perPage['city'].for_first_page);

			var ele = $('div#cities ul#cities');
	        var scrollTop = ele.scrollTop();
			if ( self.cityPageNumber == 0 &&
				 existingCount < self.defaults.perPage['city'].for_first_page &&
				 self.city_sorted.length > existingCount ) {
				self.get.showCities(self.cityPageNumber, 1, existingCount);
				if ( self.cityBottom ) {
					ele.scrollTop(scrollTop+100);
	        		quizProfile.cityBottom = false;
				}
			}

			var innerHeight = ele.innerHeight();
	        var scrollHeight = ele.context.scrollingElement.scrollHeight;
	        console.log("processPartial - scrollTop:"+scrollTop+", innerHeight:"+innerHeight+", scrollHeight:"+scrollHeight+", cityBottom:"+quizProfile.cityBottom);
	        if ( quizProfile.cityBottom ) {
	        	quizProfile.get.page(++self.cityPageNumber);
	        	ele.scrollTop(scrollTop+100);
	        	quizProfile.cityBottom = false;
	        }

			if ( allowPartialQuizResults &&
				!quizProfile.ignorePartialResults ) {
				quizProfile.get.getPartialResults();
			}
			else
				$('div#cities #loading').hide();
		}

		this.processResults = function(d, reparseTags) {
			// ahtb.close();
			if (reparseTags !== false) reparseTags = true;
			self = quizProfile;
			self.query = d;
			tagsEdited = false;
			var displayingWarning = false;
			returningFromListing = getCookie('VisitingListing') != '';
			deleteCookie('VisitingListing');
			/* Loading Overlay Hide */
			timeoutID = window.setTimeout(delayopacity, 1000);
			function delayopacity() {
				$('.results-loading-overlay').css('opacity', '0');
			}
			timeoutID = window.setTimeout(delayhide, 1500);
			function delayhide() {
				$('.results-loading-overlay').hide();
			}
			
			// self.storeResults('results', d);
			self.storeResults('results'+d.index, d);
			
			if (!returningFromListing)
				self.resetCookies();

			var allowPartialQuizResults = typeof d.allowPartialQuizResults == 'undefined' ? 0 : d.allowPartialQuizResults;

			if (!returningFromListing &&
				!allowPartialQuizResults &&
				 (isMobile == false ||
				  isMobile === 'false')) {
				if (typeof d.showFatalMsg != 'undefined' &&
						d.showFatalMsg ) {
					var h = '<div id="intro-box" >'+
								'<div class="close" ></div>'+
								'<div class="top"><img src="'+ah_local.tp+'/_img/lifestyled-logo-med.png" /></div>'+
	           				'<div class="main" style="height: 100%;padding: 0;"><p id="welcome" style="padding: .75em 0 .15em;font-family:Perpetua;font-size: 2.3em;letter-spacing: .05em;">There was a problem</p>' +
									'<p class="subtitle" style="width: 75%;margin: 0 auto!important;text-transform: none;font-size: .9em;">'+d.failureMsg+'</p>' +
									'<button id="retakeQuiz" style="display: block;width: 38%!important;height: auto;padding: .4em 0;margin: .5em auto;" >Retake Quiz</button>' +
	          		 		 	'</div>'+
	          		 		 '</div>';
					ahtb.open({html: h,
							  width: 500,
							  height: 210 + ((d.failureMsg.length / 40)*40),
							  opened: function() {
								$('#intro-box .close').on('click', function() {
				                    ahtb.close();
				                  })
								$('#intro-box #retakeQuiz').on('click', function() {
				                    ahtb.close();
				                    window.location = ah_local.wp+'/quiz/#sq=0';
				                  })
							  }});
					displayingWarning = true;
				}
				else if (typeof d.showFailureMsg != 'undefined' &&
						d.showFailureMsg ) {
					var h = '<div id="intro-box" >'+
								'<div class="close" ></div>'+
								'<div class="top"><img src="'+ah_local.tp+'/_img/lifestyled-logo-med.png" /></div>'+
	           				'<div class="main" style="height: 100%;padding: 0;"><p id="welcome" style="padding: .75em 0 .15em;font-family:Perpetua;font-size: 2.3em;letter-spacing: .05em;">Here Are Your Results!</p>' +
									'<p class="subtitle" style="width: 75%;margin: 0 auto!important;text-transform: none;font-size: .9em;">'+d.failureMsg+'</p>' +
									'<button id="view" style="display: block;width: 38%!important;height: auto;padding: .4em 0;margin: .5em auto;" >View Homes</button>' +
	          		 		 	'</div>'+
	          		 		 '</div>';
					ahtb.open({html: h,
							  width: 500,
							  height: 210 + ((d.failureMsg.length / 40)*40),
							  opened: function() {
								$('#intro-box .close').on('click', function() {
				                    ahtb.close();
				                  })
								$('#intro-box #view').on('click', function() {
				                    ahtb.close();
				                  })
							  }});
					displayingWarning = true;
				}
				else if (typeof d.showWarning != 'undefined' &&
					d.showWarning  &&
					d.admitted &&
					quizProfile.showHaveExcessQuizResults == true) {
					var h = '<div id="intro-box" >'+
								'<div class="close" ></div>'+
								'<div class="top"><img src="'+ah_local.tp+'/_img/lifestyled-logo-med.png" /></div>'+
	           				'<div class="main" style="height: 222px;padding: 0;"><p id="welcome" style="padding: .75em 0 .15em;font-family:Perpetua;font-size: 2.3em;letter-spacing: .05em;">Here Are Your Results!</p>' +
									'<p class="subtitle" style="width: 75%;margin: 0 auto!important;text-transform: none;font-size: .9em;">Your quiz selection found '+d.total_listings_found+' listings, <span style="font-weight:600;color:#333;">the top '+d.admitted+' listings are shown.</span>  Narrow down the search by adding more tags.</p>' +
									'<button id="beginSearch" style="display: block;width: 38%!important;height: auto;padding: .4em 0;margin: .5em auto;" >View Homes</button>' +
			            			'<input type="checkbox" id="noshow" /><span id="no-show">&nbsp;'+"Don't show me this again" +'</span>' +
	          		 		 	'</div>'+
	          		 		 '</div>';
					ahtb.open({html: h,
							  width: 500,
							  height: 300,
								hideTitle: true,
							  opened: function() {
							  	$('input#noshow').on('change', function() {
							  		quizProfile.showHaveExcessQuizResults = !$(this).prop('checked');
							  		console.log("showHaveExcessQuizResults is "+quizProfile.showHaveExcessQuizResults);
							  		setCookie('ShowHaveExcessQuizResults', quizProfile.showHaveExcessQuizResults, 30);
							  	})
								$('#intro-box .close').on('click', function() {
				                    ahtb.close();
				                  })
								$('#intro-box #beginSearch').on('click', function() {
				                    ahtb.close();
				                  })
							  }})
					displayingWarning = true;
				}
			}

			self.fixCityTagOrder(d);
			self.cleanTags();
			d.quiz == 1 || d.quiz == 5 ? $('.quiz-tags .location-mod').hide() : $('.quiz-tags .location-mod').show(), $('.quiz-tags .location-mod option[value="'+d.distance+'"]').prop('selected', true), $('.quiz-tags .price-slider').css('margin-top','.5em');
			setCookie("QuizCompleted", d.index, 2);
			setCookie("QuizId", d.index, 2);
			quiz_id = d.index;
			// clear map
	    	self.map.clearMarkers();

			// parse listings & add to map
			self.listings = [];
			self.city_sorted = [];
			if (d.listings != null && length(d.listings)) {
				$('ul#listings').empty();
				self.primary = 'listings';
				$('.view-selector .largedesktop').hide();
				$('.view-selector .smalldesktop').hide();
				$('.view-selector .largemobile').hide();
				$('.view-selector .smallmobile').hide();
				$('.view-selector li.city').hide();
				$('.view-selector li.listings').show();
				//$('.view-selector li.list').addClass('active');
				$('.view-selector li.listings').addClass('leftmost');
				$('.view-selector li.listings').click();

				quizProfile.activeCityId = parseInt(Object.keys(d.cities)[0]);

				for (var i in d.listings) {
					d.listings[i].price = parseInt(d.listings[i].price);
					if (typeof d.listings[i].image_path == 'undefined' ||
						d.listings[i].image_path.length == 0) {
						d.listings[i].image_path = '_blank.jpg'; // default..
						if (d.listings[i].images && d.listings[i].images.length > 0 && typeof d.listings[i].images[0].file != 'undefined' && d.listings[i].images[0].file.length) 
							d.listings[i].image_path = d.listings[i].images[0].file;
					}
						
					self.listings[i] = d.listings[i];
					self.map.createMarker( self.listings[i], 'listings' );
				}
			}
			else if (d.city_sorted && length(d.city_sorted)) {
				$('ul#cities').empty();
				self.primary = 'city';
				// self.city_sorted = d.city_sorted;
				for (var i in d.city_sorted) {
					self.city_sorted[self.city_sorted.length] = d.city_sorted[i];
					self.map.createMarker( self.get.city_by_id(d.city_sorted[i].id), 'city');
				}
			}
			else { // uh oh, empty results..
				$('ul#cities').empty();
				$('ul#listings').empty();
				self.primary = 'city';
			}

			if (returningFromListing) {
				var storedCitySorted = self.retrieveResults('city_sorted'+self.query.index);
			 	if (storedCitySorted)
			 		self.city_sorted = storedCitySorted.results;
				if (getCookie("PrimaryList").length) {
					 var primary = getCookie("PrimaryList");
					 if (primary == 'listings') {
					 	var city_sorted = self.get.city_sorted_by_id(self.activeCityId);
					 	if ( city_sorted != null )
					 		self.primary = primary;
					 	else if ( d.city_sorted && length(d.city_sorted) ) {
					 		self.primary = 'city';
					 		setCookie("PrimaryList", 'city', 2);
					 	}
					 }
					 else if ( !(d.city_sorted && length(d.city_sorted)) ) {
					 	self.primary = 'listings';
					 	setCookie("PrimaryList", 'listings', 2);
					 }
				}
			}

			self.map.setMapBounds();

			
			// if (self.price_slider_init)
			// 	$( ".price-slider #slider-range" ).slider("destroy");
			self.price_slider_init = true;

			// self.query.price[1] = self.query.price[1] == -1 || self.query.price[1] >= theRange[1] ? theRange[1] : self.query.price[1];
			self.query.price[1] = self.query.price[1] == theRange[1] ? theRange[1] - 1 : self.query.price[1];


	    	self.query.price[0] = self.fixRange(self.query.price[0]);
	    	self.query.price[1] = self.fixRange(self.query.price[1]);
			var maxKey = self.query.price[1] == -1 ? 20000000 : self.query.price[1];
			$('.selectBoxmin ul#min li[value="'+self.query.price[0]+'"]').prop('selected', true);
			$('.selectBoxmax ul#max li[value="'+maxKey+'"]').prop('selected', true);
			$('.selectBoxmin .selected').html(priceOptionmin[self.query.price[0]]);			
			var maxPrice =priceOptionmax[maxKey];
			$('.selectBoxmax .selected').html(maxPrice);
		
			$('select#min option[value="'+self.query.price[0]+'"]').prop('selected', true);
			$('select#max option[value="'+maxKey+'"]').prop('selected', true);

			homeOptions = self.query.homeOptions;
			self.query.homeOptions.beds = typeof self.query.homeOptions.beds != 'undefined' ? self.query.homeOptions.beds : 0;
			$('.selectBed ul li[value="'+self.query.homeOptions.beds+'"]').prop('selected', true);
			// var beds = BedsOptions[self.query.homeOptions.beds]+(self.query.homeOptions.beds ? '&nbsp;'+(self.query.homeOptions.beds != 1 ? 'beds' : 'bed') : '');
			// var value = parseFloat(self.query.homeOptions.beds);
			// var fraction = (value % Math.floor(value));
			// var base = Math.floor(value);
			// var beds = (self.query.homeOptions.beds == 0 ? 'ALL' : base.toString()+(fraction == 0 ? '' : (fraction == 0.3 ? '.5' : '+')))+'&nbsp;'+(value == 1 ? 'bed' : 'beds');
			var beds = homeOptions.beds != 0 ? homeOptions.beds : 'Any';
			$('.selectBed .selected').html(beds+' beds');
			var ele = $('select#bed option[value="'+self.query.homeOptions.beds+'"]');
			ele.prop('selected', true);

			self.query.homeOptions.baths = typeof self.query.homeOptions.baths != 'undefined' ? self.query.homeOptions.baths : 0;
			$('.selectBath ul li[value="'+self.query.homeOptions.baths+'"]').prop('selected', true);
			// value = parseFloat(self.query.homeOptions.baths);
			// fraction = (value % Math.floor(value));
			// base = Math.floor(value);
			// var baths = (self.query.homeOptions.baths == 0 ? 'ALL' : self.query.homeOptions.baths.toString())+(self.query.homeOptions.baths == 4 ? '+' : '')+(self.query.homeOptions.baths ? '&nbsp;'+(self.query.homeOptions.baths != 1 ? 'baths' : 'baths') : '');
			// var baths = (self.query.homeOptions.baths == 0 ? 'ALL' : base.toString()+(fraction == 0 ? '' : (fraction == 0.3 ? '.5' : '+')))+'&nbsp;'+(value == 1 ? 'bath' : 'baths');
			var baths = homeOptions.baths != 0 ? homeOptions.baths : 'Any';
			$('.selectBath .selected').html(baths+' baths');
			ele = $('select#bath option[value="'+self.query.homeOptions.baths+'"]');
			ele.prop('selected', true);
	    			    // display price
		    // if ( self.query.price[1] == -1 || self.query.price[1] == theRange[1]) var theMax =  "&infin;";
		    // else var theMax =  "$" + self.query.price[1].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		    // $( "#amount" ).html("$"+self.query.price[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" - "+theMax);

		    $("button#undo").prop('disabled', true);
			$("button#apply").prop('disabled', true);
		    // tags
		    if (reparseTags){
		    	self.myTagList = '';
		    	$('ul.tags-list').empty();
			    for (var i in self.query.tags) if (self.query.tags[i]){
			    	self.query.tags[i].percent = parseFloat(self.query.tags[i].percent);
			    	var t = self.query.tags[i];
			    	t.flag = typeof t.flag == 'undefined' ? 0 : 
			    			 typeof t.flag == 'string' ? (t.flag == 'true' ? 1 : 0) :
			    			 t.flag;
			    	self.myTagList += (self.myTagList.length ? "," : '')+t.id;

						var h = '<li class="tag" tag-index="'+i+'" tag-id="'+t.id+'">'+
								'<a class="remove entypo-cancel-squared"></a> '+
								t.tag+' '+
								self.makeToggle(i, t) +
								//'<a flagged="0" class="flag entypo-flag'+(t.flag == 1 ? ' flagged' : '')+'"></a>'+
							'</li>'+
							'<li class="percent" tag-index="'+i+'" tag-id="'+t.id+'">'+
								// '<div class="percent-bar"><div class="percent-bar-fill" style="width:'+t.percent+'%"></div></div>'+
								'<div class="percent-bar"><div class="percent-bar-fill" style="width:100%"></div></div>'+
								//'<div class="split"></div>'+
							'</li>';
						$('ul.tags-list').append(h);

						var enableControls = isMobile ? '.mobile-nav' : '.right-col';
						var disableControls = isMobile ? '.right-col' : '.mobile-nav';
						$(enableControls).prop('disabled', false);
						$(disableControls).prop('disabled', true);
						var parent = '.tag-editor';
						
						$(parent+' .tags-list span.fh-switch[tag-index='+i+']').on('click',function(){
							// var checked = $(this).prop('checked');
							var id = $(this).attr('tag-index');
							var ele = $(this).find('input#switch-id');
							var checked = ele.prop('checked'); // get value from clicked-on element
							// set counterpart
							var switchContainers = $('.tag-editor .tags-list span.fh-switch[tag-index='+id+']');
							var switches = switchContainers.find('input#switch-id');
							switches.prop('checked', !checked);

							console.log("clicked on "+id+", checked is now "+(!checked));
							if (typeof self.query.tags[id].origFlag == 'undefined')
								self.query.tags[id].origFlag = typeof self.query.tags[id].flag != 'undefined' ? self.query.tags[id].flag : 0;
							self.query.tags[id].flag = !checked ? 1 : 0;
							$("button#undo").prop('disabled', false);
							$("button#apply").prop('disabled', false);
						})

						// tag actions
						$(parent+' .tags-list [tag-index='+i+'] a').on('click',function(){
							self = quizProfile;
							var id = $(this).parent().attr('tag-id');
							if ( $(this).hasClass('remove') ){ // remove tag
								// $('li[tag-id='+id+']').fadeOut(250,function(){ $(this).remove() }); // remove from DOM
								$('li[tag-id='+id+']').fadeOut(250,function(){ $(this).prop('disabled', true) }); // remove from DOM
								found = null;
								for (var j in self.query.tags) if (self.query.tags[j].id == id) {
									self.query.tags[j].removed = true;
									found = j;
									la(	AnalyticsType.ANALYTICS_TYPE_CLICK,
					          			'RemoveTag', 
					          			id);
									ga('send', {
									  'hitType': 'event',          // Required.
									  'eventCategory': 'button',   // Required.
									  'eventAction': 'click',      // Required.
									  'eventLabel': 'RemoveTag'
									});
									$("button#undo").prop('disabled', false);
									$("button#apply").prop('disabled', false);
									break;
								}
								// if (found !== null) self.query.tags.splice(found, 1);
							} else if ( $(this).hasClass('flag') ) { // change tag 'flagged'
								for (var j in self.query.tags)
									if (self.query.tags[j].id == id){
										if ( $(this).hasClass('flagged') ) {
											$(this).removeClass('flagged');
											self.query.tags[j].flag = 0;
											la(	AnalyticsType.ANALYTICS_TYPE_CLICK,
							          			'DeFlagTag', 
							          			id,
							          			self.query.tags[j].tag);
											ga('send', {
											  'hitType': 'event',          // Required.
											  'eventCategory': 'button',   // Required.
											  'eventAction': 'click',      // Required.
											  'eventLabel': 'DeFlagTag'
											});
										} else {
											$(this).addClass('flagged');
											self.query.tags[j].flag = 1;
											la(	AnalyticsType.ANALYTICS_TYPE_CLICK,
							          			'FlagTag', 
							          			id,
							          			self.query.tags[j].tag);
											ga('send', {
											  'hitType': 'event',          // Required.
											  'eventCategory': 'button',   // Required.
											  'eventAction': 'click',      // Required.
											  'eventLabel': 'FlagTag'
											});
										}
									}
									$("button#undo").prop('disabled', false);
									$("button#apply").prop('disabled', false);
							}
							//self.resetCookies();
							//self.get.results({ tags: self.query.tags }, false);
						});

						$(parent+' .tags-list [tag-index='+i+'] .percent-bar-fill').slider({
							range: "min",
						    min: 0,
						    max: 100,
						    step: 5,
						    value: t.percent,
						    stop: function( event, ui ) {
							    var id = $(this).parent().parent().attr('tag-id');
							  	var newPercent = parseFloat($(this).attr('percent'));
							  	self = quizProfile;

							  	for (var j in self.query.tags)
							  		if (self.query.tags[j].id == id && self.query.tags[j].percent != newPercent){
							  			if (typeof self.query.tags[j].origPercent == 'undefined')
							  				self.query.tags[j].origPercent = self.query.tags[j].percent;
							  			self.query.tags[j].percent = newPercent;
							  			la(	AnalyticsType.ANALYTICS_TYPE_SLIDER,
							          			'SlideTag', 
							          			newPercent,
							          			self.query.tags[j].tag);
							  			ga('send', {
										  'hitType': 'event',          // Required.
										  'eventCategory': 'button',   // Required.
										  'eventAction': 'slide',      // Required.
										  'eventLabel': 'SlideTag'
										});
							  			self.resetCookies();
										//var isMobile = $('.mobile-identifier').css('display') != 'none';
										// if (!isMobile)
							  	// 		self.get.results({ tags: self.query.tags }, false);
										tagsEdited = true;
										$("button#undo").prop('disabled', false);
										$("button#apply").prop('disabled', false);
							  			break;
							  		}
							},
							slide: function( event, ui) {
								$(this).attr('percent', ui.value);
								if(ui.value <= 20){
						          $(this).removeClass('green');
						          $(this).addClass('red');
						        
						        }
						        else if (ui.value <= 50) {
						            $(this).removeClass('red');
						            $(this).removeClass('blue');
						          	$(this).addClass('green');
						        }
						        else {
						            $(this).removeClass('green');
						          	$(this).addClass('blue');
						        }
						        console.log("Value:"+ui.value);
								}
						});
						if (t.percent <= 20)
							$('.tags-list [tag-index='+i+'] .percent-bar-fill').addClass('red');
						else if (t.percent <= 50)
							$('.tags-list [tag-index='+i+'] .percent-bar-fill').addClass('green');
						else
							$('.tags-list [tag-index='+i+'] .percent-bar-fill').addClass('blue');

						$('.tags-list [tag-index='+i+'] .percent-bar-fill').slider().draggable();
					}
					setCookie('quizTags', self.query.tags.length? JSON.stringify(self.query.tags) : '', 2);
					// h = '<div id="actions"><button id="undo" disabled >Undo</button><button id="apply" disabled >Apply Changes</button></div>';
					// $('ul.tags-list').after(h);

					//var checkMobile = $('.mobile-identifier').css('display') != 'none';
					//if (!isMobile &&
					//	!checkMobile)
					$('div#actions').show();


					$('button#undo').off().on('click', function() {
						for (var j in self.query.tags) {
							var id = self.query.tags[j].id;
							if (typeof self.query.tags[j].removed != 'undefined' &&
								self.query.tags[j].removed) {
								$('li[tag-id='+id+']').fadeIn(250,function(){ 
									$(this).prop('disabled', false) ;
									self.query.tags[j].removed = false;
									la(	AnalyticsType.ANALYTICS_TYPE_CLICK,
					          			'RestoreTag', 
					          			id);
								}); // show in DOM
							}
							if (typeof self.query.tags[j].origFlag != 'undefined') {
								// self.query.tags[j].flag = 0;
								// $('li[tag-id='+id+'] a').removeClass('flagged');
								var ele = $('.tags-list span.fh-switch[tag-index='+j+']').find("input#switch-id");
								ele.prop('checked', self.query.tags[j].origFlag);
								self.query.tags[j].tag = self.query.tags[j].origFlag;
								la(	AnalyticsType.ANALYTICS_TYPE_CLICK,
							          			'ResetFlagTag', 
							          			id,
							          			self.query.tags[j].tag);
							}
							if (typeof self.query.tags[j].origPercent != 'undefined') {
							  	self.query.tags[j].percent = self.query.tags[j].origPercent;
							  	la(	AnalyticsType.ANALYTICS_TYPE_SLIDER,
							          			'SlideTag', 
							          			self.query.tags[j].percent,
							          			self.query.tags[j].tag);
							  	var ele = $('.tags-list [tag-index='+j+'] .percent-bar-fill');
							  	if(self.query.tags[j].percent <= 20){
						          ele.removeClass('green');
						          ele.removeClass('blue');
						          ele.addClass('red');
						        
						        }
						        else if (self.query.tags[j].percent <= 50) {
						            ele.removeClass('red');
						            ele.removeClass('blue');
						          	ele.addClass('green');
						        }
						        else {
						            ele.removeClass('green');
						            ele.removeClass('red');
						          	ele.addClass('blue');
						        }
							  	ele.slider('value', self.query.tags[j].percent);
							}
						}
						$("button#undo").prop('disabled', true);
						$("button#apply").prop('disabled', true);
					})

					$('button#apply').off().on('click', function() {
						if (isMobile){
							activePulldown = -1;
							$('.mobile-nav li.edit-tags .dropdown').toggle();
							$('.mobile-nav .centerarrow').click();
							$('.mobile-nav li a.mlink .collapselink').css('display','none');
							$('.mobile-nav li a.mlink .expandlink').css('display','inline-block');
						}
						var tagList = [];
						for (var j in self.query.tags) {
							if (typeof self.query.tags[j].removed == 'undefined' ||
								!self.query.tags[j].removed)
								tagList.push(self.query.tags[j]);
						}
						self.resetCookies();
						self.get.results({ tags: tagList }, false);
					});
		    }


		    $('#miles option[value="'+self.query.distance+'"]').prop('selected', true);
			
		 //    $('.view-selector .backtocity').on('click', function() {
			// 		quizProfile.primary = 'city';
			// 		quizProfile.map.hide();
			// 		setCookie('QuizViewSelector', 'city');
			// 	$('.view-selector li.map').removeClass( 'active' );
			// 	$('.view-selector li.city').addClass( 'active' );
			// 	$('.view-selector-bg .slide').addClass( 'left' );
			// 	$('.view-selector-bg .slide').removeClass( 'right' );
			// });
			// $('.view-selector .backtolistings').on('click', function() {
			// 		timeoutID = window.setTimeout(delaylistingscheck, 850);
			// 		quizProfile.primary = 'listings';
			// 		quizProfile.map.hide();
			// 		setCookie('QuizViewSelector', 'list');
			// 	$('.view-selector li.map').removeClass( 'active' );
			// 	$('.view-selector li.city').addClass( 'active' );
			// 	$('.view-selector-bg .slide').addClass( 'left' );
			// 	$('.view-selector-bg .slide').removeClass( 'right' );
			// });

		    // get first page
		    if (returningFromListing) {
			    var listSet = ['city','listings'];
			    for(var i in listSet) {
			    	self.scrollData[listSet[i]].scrollPage = listSet[i] == 'listings' ? getCookie("QuizListingPos") : getCookie("QuizCityPos");
			    	self.scrollData[listSet[i]].scrollPage = self.scrollData[listSet[i]].scrollPage.length ? parseInt(self.scrollData[listSet[i]].scrollPage) : 0;
			    	self.scrollData[listSet[i]].scrollTop = listSet[i] == 'listings' ? getCookie("QuizListingTop") : getCookie("QuizCityTop");
			    	self.scrollData[listSet[i]].scrollTop = self.scrollData[listSet[i]].scrollTop.length ? parseInt(self.scrollData[listSet[i]].scrollTop) : 0;
			    	self.defaults.perPage[listSet[i]].for_first_page = self.scrollData[listSet[i]].scrollPage ? self.defaults.perPage[listSet[i]].for_first_page + ((self.scrollData[listSet[i]].scrollPage)*self.defaults.perPage[listSet[i]].per_page) : 7;
			    }
			    console.log("returningFromListing - scrollTop for city:"+self.scrollData['city'].scrollTop+', listings:'+self.scrollData['listings'].scrollTop);
			    console.log("returningFromListing - scrollPage for city:"+self.scrollData['city'].scrollPage+', listings:'+self.scrollData['listings'].scrollPage);
			}
			else
				self.cityPageNumber = self.listingPageNumber = self.citiesViewcount = 0;
		
			if (self.primary == 'listings' &&
				returningFromListing &&
				self.listings.length == 0) {
				self.switchToListing();
				self.get.showCities(0);
			}
			else {
				$('.view-selector li.map').removeClass( 'active' );
				$('.view-selector li.city').addClass( 'active' );
				$('.view-selector-bg .slide').addClass( 'left' );
				$('.view-selector-bg .slide').removeClass( 'right' );
	    		self.get.page(0);

		    	// if (self.scrollData[self.primary].scrollPage) {
		    	// 	self.scrollData[self.primary].scrollPage = 0;
		    	// 	self.scrollData[self.primary].scrollTop = self.primary == 'listings' ? parseInt(getCookie('QuizListingTop')) : parseInt(getCookie('QuizCityTop'));
		    	// 	console.log("QuizListingTop retrieved: "+self.scrollData[self.primary].scrollTop);
		    	// 	var timeout = 250; //self.defaults.listings_for_first_page * 50;
		    	// 	window.setTimeout(self.setTop, timeout);
		    	// 	// self.scrollTop = getCookie('QuizListingTop');
		    	// 	// $('#listings').scrollTop(self.scrollTop);
		    	// 	//$('#listings').scrollTop((self.defaults.listings_for_first_page-self.defaults.listings_per_page)*350);
		    	// }
		    }

			if (self.listings && 
				length(self.listings) == 0 &&
				self.city_sorted &&
				length(self.city_sorted) == 0 &&
				!displayingWarning &&
				!allowPartialQuizResults) {
				// ahtb.alert("Empty quiz results returned, please retry with different settings.",
				// 			{height: 180,
				// 			 width: 600});
				$('.results-error-overlay').show();
				$('.view-selector').hide();
			}
			// else if (!displayingWarning)
				//ahtb.close();

			if (d.admitted) {
				var inactiveOne = quizProfile.primary == 'listings' ? 'cities' : 'listings';
				$('div#'+inactiveOne).animate(
						{ left: "100%" },
						{ queue: false, duration: 100, done:function(){
							$('div#'+inactiveOne).hide();
							var activeOne = quizProfile.primary == 'listings' ? 'listings' : 'cities';
							$('div#'+activeOne).show().animate({left: '0%'},
															  { queue: false, duration: 100, done: function() {
															  	if ( $('div#'+activeOne).html().length == 0 )
															  		quizProfile.switchToListing();
															  }});
						}}
					);
			}

			// callback for the city and list ul's
			window.setTimeout(function() {
				$('ul#cities').off().on('scroll', function() {
					if (returningFromListing) {
						console.log("ul#cities scroll called when returningFromListing is true");
						returningFromListing = false;
					}
					self = quizProfile;
			        self.scrollData[self.primary].scrollTop = $(this).scrollTop();
			        setCookie('QuizCityTop', self.scrollData[self.primary].scrollTop, 2);
			        var innerHeight = $(this).innerHeight();
			        var scrollTop = $(this).scrollTop();
			        var scrollHeight = this.scrollHeight;
			        // console.log("scrollTop:"+scrollTop+", innerHeight:"+innerHeight+", this.scrollHeight:"+scrollHeight);
			        if( (scrollTop + innerHeight) >= (scrollHeight-5)) {
			        	console.log("cityBottom is true");
			        	self.cityBottom = true;
			        }
			        else
			        	self.cityBottom = false;

			        if( (scrollTop + innerHeight) >= (scrollHeight - 230) && 
			        	self.city_sorted.length > $(this).children().length ) // &&
			        	//!self.doingQuery)
			          	self.get.page(++self.cityPageNumber);
				});

				$('ul#listings').off().on('scroll', function() {
					if (returningFromListing) {
						console.log("ul#listings scroll called when returningFromListing is true");
						returningFromListing = false;
					}
					self = quizProfile;
			        self.scrollData[self.primary].scrollTop = $(this).scrollTop();
			        setCookie('QuizListingTop', self.scrollData[self.primary].scrollTop, 2);
			        var innerHeight = $(this).innerHeight();
			        if( ($(this).scrollTop() + innerHeight) >= (this.scrollHeight - 230) && 
			        	self.listings.length > $(this).children().length ) //&&
			        	//!self.doingQuery)
			          	self.get.page(++self.listingPageNumber);
				});
			}, 1000);

			quizProfile.ignorePartialResults = false;
			if ( allowPartialQuizResults ) {
				$('div#cities #loading').show();
				quizProfile.get.getPartialResults();
			}

		}
		this.setTop = function() {
			self = quizProfile;
			var height = $('ul#listings')[0].scrollHeight;
			console.log("setTop - height: "+height);
			
			height = Math.min(height, self.scrollData[self.primary].scrollTop);
			
			$('ul#'+self.primary).scrollTop(height);
			console.log("setTop at: "+height);
		}
		
		this.makeTagSelector = function() {
			  self = quizProfile;
				tagsEdited = false;
				var h = '<div id="tag-window">'+
						'<h2 class="title">Modify Your Search Tags</h2>'+
						'<div class="top"><span class="left">Categories</span><span class="right">Click to Add Tags</span></div>'+
						'<ul class="tag-categories">';
						for (var i in tags_categories) if (tags_categories[i].tags.length > 0) {
							h+= '<li class="tag-category" category="'+tags_categories[i].category.replace(' ','-')+'"><span class="title">'+tags_categories[i].category+'</span><span class="entypo-record"></span></li>';
						}
					h+= '</ul>'+
					'<div class="tag-definition"><h5></h5><span></span></div><div class="list-wrap">';
					for (var i in tags_categories) if (tags_categories[i].tags.length > 0) {
						h+= '<ul class="tags-list" id="'+tags_categories[i].category.replace(' ','-')+'" category="'+tags_categories[i].category.replace(' ','-')+'">';
						for (var j in tags_categories[i].tags){
							var tag_used = false;
							for (var k in self.query.tags)
								if (self.query.tags[k].tag == tags_categories[i].tags[j].tag){
									tag_used = true;
									h+= '<li class="tag'+(tag_used ? ' active' : '')+'" tag="'+tags_categories[i].tags[j].tag+'" tag-id="'+tags_categories[i].tags[j].id+'"><span class="checkmark"><img class="mobile" src="'+ah_local.tp+'/_img/_sellers/check-mobile.png" /><img class="desktop" src="'+ah_local.tp+'/_img/_sellers/check.png" /></span>'+tags_categories[i].tags[j].tag+'</li>';
									break;
								}
							if (!tag_used)
								h+= '<li class="tag'+(tag_used ? ' active' : '')+'" tag="'+tags_categories[i].tags[j].tag+'" tag-id="'+tags_categories[i].tags[j].id+'"><span class="checkmark"><img class="mobile" src="'+ah_local.tp+'/_img/_sellers/check-mobile.png" /><img class="desktop" src="'+ah_local.tp+'/_img/_sellers/check.png" /></span>'+tags_categories[i].tags[j].tag+'</li>';
						}
						h+= '</ul>';
					}
					h+= '</div>';
				h+= '</div>';
			  return h;
		}
		
		this.setTagSelectorHandlers = function() {
						$('ul.tag-categories li').each(function(){
							$(this).hover(function(){
								for (var i in tags_categories) if (tags_categories[i].category.replace(' ','-') == $(this).attr('category')){
									var theCategory = $(this).attr('category');
									var theDesciption = tags_categories[i].description;
									if ($('.tag-definition h5').html() !== theCategory+' - ') $('.tag-definition').fadeOut(75,function(){
										$(this).children('h5').html(theCategory+' - ');
										$(this).children('span').html(theDesciption && theDesciption.length > 0 ? theDesciption : 'This tag category has no description.');
										$(this).fadeIn(75);
									});
									break;
								}
							},function(){});
							$(this).on('click',function(){
								$('.tag-category, .tags-list').removeClass('active');
								$('[category='+$(this).attr('category')+']').addClass('active');
							})
						});
						$('#tag-window .tags-list li.tag').each(function(){
							$(this).hover(function(){
								for (var i in tags_categories) if (tags_categories[i].tags.length > 0) for (var j in tags_categories[i].tags)
									if (tags_categories[i].tags[j].tag == $(this).attr('tag')){
										var theTag = $(this).attr('tag');
										var theDesciption = tags_categories[i].tags[j].description;
										if ($('.tag-definition h5').html() !== theTag+' - ') $('.tag-definition').fadeOut(75,function(){
											$(this).children('h5').html(theTag+' - ');
											$(this).children('span').html(theDesciption && theDesciption.length > 0 ? theDesciption : 'This tag has no description.');
											$(this).fadeIn(75);
										});
										break;
									}
							},function(){});
							$(this).on('click',function(){
								var mode = 0;
								if ($(this).hasClass('active')) $(this).removeClass('active');
								else {
									$(this).addClass('active');
									mode = 1;
								}
								tagsEdited = true;
								var id = $(this).attr('tag-id');
								var tag = $(this).attr('tag');
								la(	AnalyticsType.ANALYTICS_TYPE_CLICK,
				          			mode ? 'SelectTag' : 'DeselectTag', 
				          			id,
				          			tag);
								ga('send', {
									  'hitType': 'event',          // Required.
									  'eventCategory': 'button',   // Required.
									  'eventAction': 'click',      // Required.
									  'eventLabel': mode ? 'SelectTag' : 'DeselectTag'
									});
							})
						});
						$('.tag-categories li[category='+self.defaults.tag_category.replace(' ','-')+']').click();
		}
		
		this.saveTagSelection = function() {
							self = quizProfile;
							$('.tag-category, .tags-list').removeClass('active');
							var tags = [];
							$('.tags-list li').each(function(){
								if ($(this).hasClass('active'))
									tags.push( parseInt($(this).attr('tag-id')) );
							});

							for (var i in tags){
								found = false;
								for (var j in self.query.tags) if (self.query.tags[j].id == tags[i]){ tags[i] = self.query.tags[j]; found = true; break; }
								if (!found) for (var j in tags_categories) if (tags_categories[j].tags.length > 0)
									for (var k in tags_categories[j].tags) if (tags_categories[j].tags[k].id == tags[i]) {
										tags[i] = tags_categories[j].tags[k];
										tags[i].percent = 100;
										tags[i].flag = 0;
										break;
									}
							}

							// Send results
							self.resetCookies();
							if ( (self.query.quiz == 1 || self.query.quiz == 5) &&
								 tags.length == 0 )
								ahtb.alert("There must be at least one tag for nationwide searches.",
										{width: 450, height: 150});
							else
								self.get.results({ tags: tags.length ? tags : "notags" });
		}
		
		
		
		
		
		
		
		
		
		this.mobilecityselector = function() {
			  self = quizProfile;
				
				var h = '<div id="cities-popup">'+
					'<h2>Sort By Area</h2>'+
					'<ul class="nearby-cities">';
				for (var i in self.query.cities){
					var $row = self.query.cities[i];
					h+= '<li class="city'+( self.query.location.length < 1 || $.inArray(parseInt(i), self.query.location) > -1 ? ' active' : '')+'" city-id="'+i+'">'+
						'<span class="city-toggle '+( self.query.location.length < 1 || $.inArray(parseInt(i), self.query.location) > -1 ? 'entypo-cancel-circled' : 'entypo-plus-circled')+'"></span>'+
						'<div class="inactive">Hidden From Results</div>'+
						'<div class="meta">'+$row.city+', '+$row.state+'</div>'+
					'</li>';
				}
				h+= '</ul></div>';
			buttons: [
						{
							text: 'Save',
							action:function(){
								var cities = [];
								var inactive = 0;
								$('#cities-popup li.city').each(function(){
									if ( $(this).hasClass('active') )
										cities.push( parseInt($(this).attr('city-id')) );
								 	else
										inactive++;
								});
								if (cities.length == 0 && self.query.location.length > 0) {
									self.resetCookies();
									self.get.results({ location: [] }, false);
								}
								else if (inactive > 0 && cities.length > 0) {
									var changed = false;
									if (cities.length !== self.query.location.length)
										changed = true;
									for (var i in cities)
										if (!$.inArray( cities[i], self.query.location ))
											changed = true;
									for (var i in self.query.location)
										if (!$.inArray( self.query.location[i], cities ))
											changed = true;
									if (changed) {
										self.resetCookies();
										self.get.results({ location: cities,
															distance: 0 }, false);
									}
								}
								ahtb.close();
							}
						},
						{ text: 'Cancel', action:function(){ ahtb.close(); } }
					]
			return h;
		}
		
		this.setmobilecityselector = function() {
						$('#cities-popup li.city').each(function(){
							if (typeof self.query.cities[ $(this).attr('city-id') ].image != 'undefined') theImage = self.query.cities[ $(this).attr('city-id') ].image;
							else theImage = '_blank.jpg';
							self.query.cities[ $(this).attr('city-id') ].el_img = new Image();
							$(this).prepend('<img src="'+ah_local.tp + '/_img/_cities/290x180/' + theImage+'" />');
							self.query.cities[ $(this).attr('city-id') ].el_img.src = ah_local.tp + '/_img/_cities/290x180/' + theImage;
							$(this).on('click',function(){
								if ($(this).hasClass('active')){
									$(this).removeClass('active');
									$(this).children('.city-toggle').removeClass('entypo-cancel-circled').addClass('entypo-plus-circled');
								} else {
									$(this).addClass('active');
									$(this).children('.city-toggle').removeClass('entypo-plus-circled').addClass('entypo-cancel-circled');
								}
							});
						});
		}
		
		this.popup = {
			/**
			 * opens the tags popup
			 */
			tags: function(){
				self = quizProfile;
				var h = self.makeTagSelector();
				ahtb.open({
					html: h,
					hideTitle: true,
					height: 480, width: 1050,
					buttons: [
						{ text:'Save', action:function(){
							self.saveTagSelection();
							ahtb.close();
						}},
						{ text:'Cancel', action:function(){ ahtb.close(); }}
					],
					opened: function(){
						$('.tb-buttons').css({ 'position': 'absolute', 'bottom': '1.5em', 'width' : '100%' });
						quizProfile.setTagSelectorHandlers();
					}
				});
			},
			/**
			 * opens the city selector popup
			 */
			cities: function(){
				self = quizProfile;
				if (ahtb.opened) {
					window.setTimeout(function() {
						self.popup.cities();
					}, 200);
				}
				else { // wait just a bit more, just in case...
					window.setTimeout(function() {
						self.popup.reallyCities();
					}, 200);
				}
			},
			// open up the cities after making sure ahtb is closed
			reallyCities: function(){
				self = quizProfile;
				
				var h = '<div id="cities-popup">'+
					'<h2>Sort By Area</h2>'+
					'<ul class="nearby-cities">';
				for (var i in self.query.cities){
					var $row = self.query.cities[i];
					h+= '<li class="city'+( self.query.location.length < 1 || $.inArray(parseInt(i), self.query.location) > -1 ? ' active' : '')+'" city-id="'+i+'">'+
						'<span class="city-toggle '+( self.query.location.length < 1 || $.inArray(parseInt(i), self.query.location) > -1 ? 'entypo-cancel-circled' : 'entypo-plus-circled')+'"></span>'+
						'<div class="inactive">Hidden From Results</div>'+
						'<div class="meta">'+$row.city+', '+$row.state+'</div>'+
					'</li>';
				}
				h+= '</ul></div>';
				ahtb.open({
					hideTitle: true,
					height: 650, width: 1160,
					html: h,
					buttonsClass: 'alignright',
					buttons: [
						{
							text: 'Save',
							action:function(){
								var cities = [];
								var inactive = 0;
								$('#cities-popup li.city').each(function(){
									if ( $(this).hasClass('active') )
										cities.push( parseInt($(this).attr('city-id')) );
								 	else
										inactive++;
								});
								if (cities.length == 0 && self.query.location.length > 0) {
									self.resetCookies();
									self.get.results({ location: [] }, false);
								}
								else if (inactive > 0 && cities.length > 0) {
									var changed = false;
									if (cities.length !== self.query.location.length)
										changed = true;
									for (var i in cities)
										if (!$.inArray( cities[i], self.query.location ))
											changed = true;
									for (var i in self.query.location)
										if (!$.inArray( self.query.location[i], cities ))
											changed = true;
									if (changed) {
										self.resetCookies();
										self.get.results({ location: cities,
															distance: 0 }, false);
									}
								}
								ahtb.close();
							}
						},
						{ text: 'Cancel', action:function(){ ahtb.close(); } }
					],
					opened: function(){
						//$('#TB_window').css({ 'background' : 'rgba(0,0,0,.6)' });
						$('#cities-popup li.city').each(function(){
							if (typeof self.query.cities[ $(this).attr('city-id') ].image != 'undefined') theImage = self.query.cities[ $(this).attr('city-id') ].image;
							else theImage = '_blank.jpg';
							self.query.cities[ $(this).attr('city-id') ].el_img = new Image();
							$(this).prepend('<img src="'+ah_local.tp + '/_img/_cities/290x180/' + theImage+'" />');
							self.query.cities[ $(this).attr('city-id') ].el_img.src = ah_local.tp + '/_img/_cities/290x180/' + theImage;
							$(this).on('click',function(){
								if ($(this).hasClass('active')){
									$(this).removeClass('active');
									$(this).children('.city-toggle').removeClass('entypo-cancel-circled').addClass('entypo-plus-circled');
								} else {
									$(this).addClass('active');
									$(this).children('.city-toggle').removeClass('entypo-plus-circled').addClass('entypo-cancel-circled');
								}
							});
						});
					}
				})
			}
		}
		this.map = new function(){
			this.colors = ['00cdff', '33c2ff', '66c2ff', '99ccff', 'cce0ff', 'd4dff0'];
			this.hidden = true;
			this.maps = {'city': null,
						 'listings': null};
			this.show = function(){
				if (this.hidden){
					this.hidden = false;
					//$('.view-selector a.map').parent().addClass('active');
					//$('.view-selector a.list').parent().removeClass('active');
					//$('.view-selector a.city').parent().removeClass('active');
					$('.view-selector li.map').addClass( 'active' );
					$('.view-selector li.city').removeClass( 'active' );
					$('.view-selector-bg .slide').removeClass( 'left' );
					$('.view-selector-bg .slide').addClass( 'right' );

					var activeOne = quizProfile.primary == 'listings' ? 'listings' : 'cities';
					var inactiveOne = quizProfile.primary == 'listings' ? 'cities' : 'listings';
					$('div#'+activeOne).animate(
						{ left: "100%" },
						{ queue: false, duration: 500, done:function(){
							$(this).hide();
							$('div#'+inactiveOne).hide();
							var activeMap = quizProfile.primary == 'listings' ? 'listing-map' : 'city-map';
							var inActiveMap = quizProfile.primary == 'listings' ? 'city-map' : 'listing-map';
							$('div#'+inActiveMap).hide();
							$('div#'+activeMap).show().animate({left: '0%'},{ queue: false, duration: 500, done: function() {
								quizProfile.map.maps[quizProfile.primary].map.setZoom(3);
								quizProfile.map.setMapBounds();
								if ( quizProfile.map.maps[quizProfile.primary].bounds.getNorthEast().equals(quizProfile.map.maps[quizProfile.primary].bounds.getSouthWest()) )
									quizProfile.map.maps[quizProfile.primary].map.setZoom(15);
								else if (quizProfile.city_sorted && length(quizProfile.city_sorted) &&
										quizProfile.primary == 'listings')
									quizProfile.map.maps[quizProfile.primary].map.setZoom( 12 );
								else {
									if ( quizProfile.map.maps[quizProfile.primary].map.getZoom() < 4 )
										quizProfile.map.maps[quizProfile.primary].map.setZoom( 4 );
									// else
									// 	google.maps.event.trigger(quizProfile.map.maps[quizProfile.primary].map, 'resize');
								}
							} });
						}}
					);
				}
			}
			this.hide = function(){
				if (!this.hidden){
					this.hidden = true;
					var activeOne = quizProfile.primary == 'listings' ? 'listings' : 'cities';
					var inactiveOne = quizProfile.primary == 'listings' ? 'cities' : 'listings';
					//$('.view-selector a.list').parent().addClass('active');
					//$('.view-selector a.'+inactiveOne).parent().removeClass('active');
					//$('.view-selector a.map').parent().removeClass('active');
					var activeOne = quizProfile.primary == 'listings' ? 'listing-map' : 'city-map';
					var inactiveOne = quizProfile.primary == 'listings' ? 'city-map' : 'listing-map';
					$('div#'+activeOne).animate(
						{ left: "100%" },
						{ queue: false, duration: 500, done:function(){
							$(this).hide();
							$('div#'+inactiveOne).hide();
							activeOne = quizProfile.primary == 'listings' ? 'listings' : 'cities';
							$('div#'+activeOne).show().animate({left: '0%'},{ queue: false, duration: 500 });
						}}
					);
				}
				else {
					var inactiveOne = quizProfile.primary == 'listings' ? 'cities' : 'listings';
					$('div#'+inactiveOne).animate(
							{ left: "100%" },
							{ queue: false, duration: 500, done:function(){
								$('div#'+inactiveOne).hide();
								var activeOne = quizProfile.primary == 'listings' ? 'listings' : 'cities';
								$('div#'+activeOne).show().animate({left: '0%'},
																  { queue: false, duration: 500, done: function() {
																  	if ( $('div#'+activeOne+' ul').html().length == 0 ) {
																  		// get first page
																  		var self = quizProfile;
																	    self.scrollData[self.primary].scrollPage = self.primary == 'listings' ? getCookie("QuizListingPos") : getCookie("QuizCityPos");
																	    self.scrollData[self.primary].scrollPage = self.scrollData[self.primary].scrollPage != "" ? parseInt(self.scrollData[self.primary].scrollPage) : 0;
																	    self.defaults.perPage[self.primary].for_first_page = self.scrollData[self.primary].scrollPage ? self.defaults.perPage[self.primary].for_first_page + ((self.scrollData[self.primary].scrollPage)*self.defaults.perPage[self.primary].per_page) : 7;

																  		if (activeOne == 'listings') {
																  			if (!self.doingSwitchToListing)
																  				self.switchToListing();
																  		}
																  		else
																  			self.get.showCities(0);
																  	}
																  	else {
																  		var primary = 'city';
																  		var scrollTop = $('ul#cities').scrollTop();
																  		if (scrollTop != quizProfile.scrollData[primary].scrollTop)
																  			$('ul#cities').scrollTop(quizProfile.scrollData[primary].scrollTop)
																  	}
																  }});
							}}
						);
				}
			}
			this.init = function(){
				if (typeof google != 'undefined'){
					this.maps['city'] =  {
											bounds: new google.maps.LatLngBounds(),
										  markers: [],
										  infowindows: [],
										  map: new google.maps.Map(document.getElementById('city-map'), {center:new google.maps.LatLng(39.226851, -120.081859),zoom:12,scrollwheel:true})
										 };
					this.maps['listings'] = 
										 {
											bounds: new google.maps.LatLngBounds(),
										  markers: [],
										  infowindows: [],
										  map: new google.maps.Map(document.getElementById('listing-map'), {center:new google.maps.LatLng(39.226851, -120.081859),zoom:12,scrollwheel:true})
										 };
				}
			}
			this.createMarker = function(element, what){
				if (typeof google != 'undefined' && element.lat && element.lng){
					var latlng = new google.maps.LatLng(parseFloat(element.lat), parseFloat(element.lng));

					this.maps[what].bounds.extend(latlng);

					switch(what) {
						case 'listings':
							this.maps[what].infowindows[element.id] = new google.maps.InfoWindow({
								      content: '<ul class="infowindow-content">'+
								      (element.images && element.images.length > 0 ? '<li class="image"><img style="max-width:100%;" src="'+(element.image_path.substr(0, 7) == 'http://' ? element.image_path : ah_local.tp+'/_img/_listings/80x80/'+element.image_path)+'" /></li>' : '')+
								      	'<li class="meta">'+
									      	'<h5 class="title">'+(element.title ? element.title : 'Listing '+element.id)+'</h5>'+
									      	(element.price ? '<span class="price">$'+element.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</span>' : '')+
									      	'<a href="javascript:quizProfile.goToListing('+element.id+')">View Listing &raquo;</a>'+
								      	'</li>'+
								      '</ul>'
								  });					

						  	var base = typeof element.percent_average == 'undefined' || element.percent_average == null ? 0 : element.percent_average;
						  	var iconNumber = Math.floor(base / 20);
						  	iconNumber = iconNumber <= 3 ? iconNumber : 4;

							this.maps[what].markers[element.id] = new google.maps.Marker({
								title: element.title ? element.title : 'Listing '+this.maps[what].markers.length,
					            position: latlng,
					            map: this.maps[what].map,
					            icon: ah_local.tp+'/_img/page-quiz-results/map-markers/pin'+iconNumber+'.png'
						        });
					        google.maps.event.addListener(this.maps[what].markers[element.id], 'click', function() {
					        	for (var i in quizProfile.map.maps[what].infowindows)
					        		if (i != element.id) quizProfile.map.maps[what].infowindows[i].close();
					        	quizProfile.map.maps[what].infowindows[element.id].open(quizProfile.map.maps[what].map, quizProfile.map.maps[what].markers[element.id]);
					        });
						break;

						case 'city':
							var city = element; //quizProfile.query.cities[element.id];
							element = quizProfile.get.city_sorted_by_id(element.id);
							this.maps[what].infowindows[element.id] = new google.maps.InfoWindow({
								      content: '<ul class="infowindow-content">'+
								      '<li class="image"><img style="max-width:100%;" src="'+(city.image.substr(0, 7) == 'http://' ? city.image : ah_local.tp+'/_img/_cities/100x70/'+city.image)+'" /></li>'+
								      	'<li class="meta">'+
									      	'<h5 class="title">'+city.city+", "+city.state+'</h5>'+
									      	// add more stuff here later
									      	'<a href="javascript:quizProfile.switchToListing('+element.id+')">View Listings &raquo;</a>'+
								      	'</li>'+
								      '</ul>'
								  });					

						  	var base = typeof element.listings == 'undefined' || element.listings == null ? 0 : length(element.listings);
						  	var iconNumber = Math.floor(base / 20);
						  	iconNumber = iconNumber <= 3 ? iconNumber : 4;

							this.maps[what].markers[element.id] = new google.maps.Marker({
								title: city.city+", "+city.state,
					            position: latlng,
					            map: this.maps[what].map,
					            icon: ah_local.tp+'/_img/page-quiz-results/map-markers/pin'+iconNumber+'.png'
						        });
					        google.maps.event.addListener(this.maps[what].markers[element.id], 'click', function() {
					        	for (var i in quizProfile.map.maps[what].infowindows)
					        		if (i != element.id) quizProfile.map.maps[what].infowindows[i].close();
					        	quizProfile.map.maps[what].infowindows[element.id].open(quizProfile.map.maps[what].map, quizProfile.map.maps[what].markers[element.id]);
					        });
						break;
					}
				}
			}
			this.clearMarkers = function(){
				if (typeof google != 'undefined'){
					for (var i in this.maps[quizProfile.primary].markers) this.maps[quizProfile.primary].markers[i].setMap(null);
					this.maps[quizProfile.primary].markers = [];
					this.maps[quizProfile.primary].infowindows = [];
					this.maps[quizProfile.primary].bounds = new google.maps.LatLngBounds();
				}
			}
			this.setMapBounds = function(){ 
				if (typeof google != 'undefined' && !this.maps[quizProfile.primary].bounds.isEmpty()) {
					google.maps.event.trigger(quizProfile.map.maps[quizProfile.primary].map, 'resize');
				 	this.maps[quizProfile.primary].map.fitBounds(this.maps[quizProfile.primary].bounds); 
				 }
			}
			this.updateMarkers = function(existingCount) {
				self = quizProfile;
				for (var i in self.city_sorted) {
					if (i < existingCount)
						continue;
					self.map.createMarker( self.get.city_by_id(self.city_sorted[i].id), 'city');
				}
			}
		}
		this.DB = function(xx){
			if (xx != null){
				if (xx.data == null) xx.data = {};
				if (typeof xx.data.sessionID == 'undefined' ||
					xx.data.sessionID == null)
					// xx.data.sessionID = ah_local.sessionID;
					xx.data.sessionID = ah_local.activeSessionID;
				quizProfile.doingQuery = true;
				$.post(ah_local.tp+'/_pages/ajax-quiz.php',
					{ query: xx.query,
					  data: xx.data },
					function(){}, 'JSON')
				.done(function(d){
					quizProfile.doingQuery = false;
			        if (d.status != 'OK') {
			        	la(AnalyticsType.ANALYTICS_TYPE_ERROR,
			        	  'post',
			        	  0,
			        	  xx.query+": error - data:"+(d.data ? d.data : "No data"));
			        	xx.error ? xx.error (d.data) : ahtb.alert('There was a problem.<br/><small>'+d.data+'</small>');
			        }
			        else {
			        	if (typeof xx.retried != 'undefined')
			        		la(AnalyticsType.ANALYTICS_TYPE_ERROR_RECOVERY,
				        	  'post',
				        	  0,
				        	  xx.query+": recovered at "+xx.retried);
			        	if (xx.done != null)
				        	xx.done(d.data);
				        else console.log(d.data);
				    }
			      })
				.fail(function(d){
					quizProfile.doingQuery = false;
					if (d && d.status && d.status != 'OK') {
						la(AnalyticsType.ANALYTICS_TYPE_ERROR,
			        	  'post',
			        	  0,
			        	  xx.query+": failure - data:"+(d.data ? d.data : "No data"));
			        	xx.error ? xx.error (d.data) : ahtb.alert('There was a problem.<br/><small>'+d.data+'</small>');
			        }
			        else {
			        	if ( xx.query != 'update-analytics' &&
			        		 xx.query != 'stop-partial-run' &&
			        		 xx.query != 'partial-quiz-run')
			        		ahtb.alert("There was a problem, sorry!");
			        	if (xx.query == 'partial-quiz-run') {
			        		if (typeof xx.retried == 'undefined') 
				        		xx.retried = 1;
				        	else
				        		xx.retried++;

				        	if (xx.retried <= 3)
				        		quizProfile.DB(xx);
			        	}
			        	la(AnalyticsType.ANALYTICS_TYPE_ERROR,
			        	  'post',
			        	  0,
			        	  xx.query+": failure");
			        }
				});
			}
		}
	}
});
