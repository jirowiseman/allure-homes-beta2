var selected = [0,0,0];
var savedInfo = false;

function ajax(d){
	if(typeof d.query !== 'undefined'){
		var h = [];
		if (typeof d.data === 'undefined') h.headers = {query:d.query}; else h.headers = {query:d.query,data:d.data};
		if (typeof d.done === 'undefined') h.done = function(){}; else h.done = d.done;
		// if (typeof d.fail === 'undefined') h.fail = function(){}; else h.fail = d.fail;
		if (typeof d.error !== 'undefined') h.error = d.error;
		if (typeof d.before === 'undefined') h.before = function(){}; else h.before = d.before;
		$.post(ah_local.tp+'/_sellers/_ajax.php', h.headers, function(){h.before()},'json')
			.done(function(x){
				if (x.status == 'OK') h.done(x.data);
				else if (h.error) h.error(x.data);
				else ahtb.close(function(){ahtb.alert(x.data, 'Database Error')});
			})
			.fail(function(x){
				if (h.fail) h.fail(x);
				else ahtb.alert("Database failure, please retry.", {height: 150});
			});
	} else ahtb.alert('Query was undefined');
}

function setupAutocomplete(element) {
	element.autocomplete({
    source: cities,
    select: function(e, ui){ 
      if (ui.item.value != null) {
        element.attr('city_id', ui.item.id); 
        element.attr('city_name', ui.item.label); 
        cityName = ui.item.label;
        element.change();
      }
    }
  }).on('focus',function(){
    if ($(this).val() == 'ENTER CITY') {
      $(this).val(''); $(this).removeClass('inactive');
    }
  }).on('keypress', function(e){
    var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
    if(keynum == 13) {//13 is the enter char code
      e.preventDefault();
    }
    $(this).attr('city_id', 0);
    $(this).attr('city_name', '');	
  });
}

function reallyReserve() {
	testAgentMatchList.item.splice(0, testAgentMatchList.item.length); // remove any items
	var ele = $('#information #lifestyles select');
	ele.each(function() {
 		var which = parseInt($(this).attr('for'));
		var val = selected[which];
		if (val != 0) {
			var item = {};
	 		item.location = testCityId;
	 		item.locationStr = testCityName;
	 		item.specialty = amTagList[val].id;
	 		item.specialtyStr = amTagList[val].tag;
	 		testAgentMatchList.item[testAgentMatchList.item.length] = item;
	 		console.log("AM specialty is "+val+' for:'+which);
	 	}
 	})

 	if (testAgentMatchList.item.length == 0) {
 		ahtb.push();
	    window.setTimeout(function() {
 			ahtb.alert("Please pick at least one lifestyle.", 
 				{height: 150,
                 buttons: [
                      {text: 'OK', action: function() {
                        ahtb.pop();
                      }}]});
 		}, 150);
 		return;
 	}
 	else {
 		savedInfo = true;
 		ahtb.close();
 	}
}

function pickedCity(wasPushed, city_id, city) {
	$('#information #user-data .service_areas').attr('city_id', city_id); 
    $('#information #user-data .service_areas').attr('city_name', city); 
	$('#information #user-data .service_areas').val(city);
	testCityName = city;
	testCityId = city_id;
	if (wasPushed)
		ahtb.pop();

	window.setTimeout(function() {
		reallyReserve();
	}, 350);
}

function getCityId(id, city) {
	$.post(ah_local.tp+'/_pages/ajax-quiz.php',
        { query:'find-city',
          data: { city: city,
          		  distance: 0 }
        },
        function(){}, 'JSON')
		.done(function(d){
			if (d &&
                d.data &&
                d.data.city &&
                (typeof d.data.city == 'object' ||
                d.data.city.length) ){
                if (d.status == 'OK' &&
                    (typeof d.data.city == 'object' || d.data.city.length) ) {							                        
                    console.log(JSON.stringify(d.data));

                	if (d.data.allState == 'true' || d.data.allState == true || d.data.allState == 1) {// doing statewide
                        ahtb.alert("Please enter a city name along with the state.", {height: 150});
                        return;
                    }

                    var len = length(d.data.city); //(typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
                	// var len = (typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
					if (len > 1) {
						if (d.data.allState == 'false' || d.data.allState == false) {
	                        var h = '<p>Please pick one of the cities:</p>' +
	                                  '<ul class="cities">';
	                        for(var i in d.data.city) {
	                        	var location = d.data.city[i].city+', '+d.data.city[i].state;
	                          	h += '<li><a href="javascript:pickedCity('+1+','+d.data.city[i].id+",'"+location+"'"+');" >'+location+'</a></li>';
	                        }
	                        h += '</ul>';
	                        ahtb.push();
	                        window.setTimeout(function() {
	                        	ahtb.open(
		                          { title: 'Cities List',
		                            width: 380,
		                            height: (150 + (len * 25)) < 680 ? (150 + (len * 25)) : 680,
		                            html: h,
		                            buttons: [
		                                  {text: 'Cancel', action: function() {
		                                    ahtb.pop();
		                                  }}],
		                            opened: function() {
		                            }
		                          })
	                        }, 150);
	                        
	                    }
	                    else {
	                    	ahtb.push();
	                    	window.setTimeout(function() {
	                    		ahtb.alert("Only one city/state combination accepted.", 
	                    			{height: 150,
	                    			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		ahtb.pop();
			 						 	}}]});
	                    	}, 150);
                          	
                        }
                    } // only one!
                    else {
                    	pickedCity(0, d.data.city[0].id, d.data.city[0].city+', '+d.data.city[0].state);
                    }
                }
                else {
                	ahtb.push();
                	window.setTimeout(function() {
                		ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                  			{title: 'City Match', 
	                  			 height:180, 
	                  			 width:600,
	                  			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		ahtb.pop();
			 						 	}}]});
                	}, 150);
                }
            }
            else {
            	ahtb.push();
            	window.setTimeout(function() {
	                ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                  			{title: 'City Match', 
	                  			 height:180, 
	                  			 width:600,
	                  			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		ahtb.pop();
			 						 	}}]});
	            }, 150);
            }
        })
        .fail(function(d){
        	ahtb.push();
            window.setTimeout(function() {
	        	ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                  			{title: 'City Match', 
	                  			 height:180, 
	                  			 width:600,
	                  			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		ahtb.pop();
			 						 	}}]});
	        }, 150);
        });
}


function agentMatchDetail() {
	var agent_match_detail = 
		'<div id="agent-reservation">' +
			'<div id="left">' +
				'<span id="title1">Be A Lifestyled Agent</br></span>' +
				'<span id="title2">Act Now, Exclusively Available to only 3 Agents Per Lifestyle</br></span>' +
				'<span id="title3">Dominate Your Areas of Expertise By Being An Exclusive Agent in Your Area.</br></span>' +	
				'<div id="main-graphics">' +
					'<div class="step1">' +
						'<img src="'+ah_local.tp+'/_img/page-agent-reservation/step1.png" />' +
						'<div class="steptext">' +
							'<span class="number">1</span>' +
							'<span class="title">Stand out from your peers</span>' +
							'<p class="sub">Tell us what lifestyles you&#39;re an expert in</p>' +
						'</div>' +
					'</div>' +
					'<div class="step2">' +
						'<img src="'+ah_local.tp+'/_img/page-agent-reservation/step2.png" />' +
						'<div class="steptext">' +
							'<span class="number">2</span>' +
							'<span class="title">Work with your dream clients</span>' +
							'<p class="sub">We match you to interested buyers looking for homes with those lifestyles</p>' +
						'</div>' +
					'</div>' +
					'<div class="step3">' +
						'<img src="'+ah_local.tp+'/_img/page-agent-reservation/step3.png" />' +
						'<div class="steptext">' +
							'<span class="number">3</span>' +
							'<span class="title">Stand out from your peers</span>' +
							'<p class="sub">You represent buyers on homes that are a miniumum of $800K</p>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>' +
			'<span class="toptext"><span style="color:#333;margin-right:10px;font-size:.9em">Regularly</span><span style="color:#2289df;font-size:1.6em">$149.99/</span><span style="color:#2289df;font-size:.8em">Month</span></span>' +
			'<div id="information">' +	
				'<div id="message">' +	
					'<span id="line1">GET IN NOW!</span>' +	
					'<span id="line2">Early Adopter Price</span>' +	
					'<span id="line3">$99 / Month</span>' +	
					'<span id="line4">No payment required at this time</span>' +	
				'</div>' +				
				'<table id="user-data">' +
					'<tbody>' +
						'<tr>' +
							'<td><label>City</label></td><td><input type="text" class="service_areas" placeholder="City" value="'+service_area+'" /></td>' +
						'</tr>' +
					'</tbody>' +
				'</table>' +
				'<div id="lifestyles">' +
						'<select class="field_expertise" style="margin:20px auto 7px" for="0" >';
						var h = '';
						var index = meta && meta.item.length ? meta.item[0].specialty : -1;
						for(var id in amTagList)
							h += '<option value="'+id+'" '+(index != -1 && index == amTagList[id].id ? 'selected' : '')+' >'+amTagList[id].tag+'</option>';
					h+= '</select>';
		agent_match_detail += h;
  agent_match_detail += '<select class="field_expertise" for="1" >';
  						h = '';
  						index = meta && meta.item.length > 1 ? meta.item[1].specialty : -1;
						for(var id in amTagList)
							h += '<option value="'+id+'" '+(index != -1 && index == amTagList[id].id ? 'selected' : '')+' >'+amTagList[id].tag+'</option>';
					h+= '</select>';
		agent_match_detail += h;
  agent_match_detail += '<select class="field_expertise" style="margin:7px auto 20px" for="2" >';
  						h = '';
  						index = meta && meta.item.length > 2 ? meta.item[2].specialty : -1;
						for(var id in amTagList)
							h += '<option value="'+id+'" '+(index != -1 && index == amTagList[id].id ? 'selected' : '')+' >'+amTagList[id].tag+'</option>';
					h+= '</select>';
		agent_match_detail += h;
agent_match_detail += 
				'</div>' +		
			'</div>' +
			'<div>' +
				'<button id="reserve">Save Information</button>&nbsp;<button id="done">Done</button>' +
			'</div>' +
		'</div>';

	ahtb.open({
		html: agent_match_detail,
		width: 1200,
		height: 625,
		hideSubmit: true,
		hideTitle: true,
		opened: function() {
			setupAutocomplete($('#information #user-data .service_areas'));

			$('#information #user-data .service_areas').attr('city_id', testCityId); 
    		$('#information #user-data .service_areas').attr('city_name', testCityName); 
			$('#information #user-data .service_areas').val(testCityName);

			$('#information #lifestyles select').on('change', function() {
				var item = parseInt($(this).attr('for'));
				var val = parseInt($(this,' option:selected').val());
				console.log("option for "+item+", val: "+val);
				selected[item] = val;				
			});

			$('#information #lifestyles select[for="0"] option[value="'+selected[0]+'"]').prop('selected', true);
			$('#information #lifestyles select[for="1"] option[value="'+selected[1]+'"]').prop('selected', true);
			$('#information #lifestyles select[for="2"] option[value="'+selected[2]+'"]').prop('selected', true);

			$('#agent-reservation #done').on('click', function(e) {
				e.preventDefault();
				testCityId = $('#information #user-data .service_areas').attr('city_id');
				testCityName = $('#information #user-data .service_areas').attr('city_name');
				if (savedInfo == false &&
					testCityId != 0 &&
					testCityName.length &&
					(selected[0] || selected[1] || selected[2])) {
					ahtb.alert("Do you want to save your information?",
								{height: 150,
								 width: 450,
								 buttons:[
		 						 	{text:'OK', action: function() {
		 						 		reallyReserve();
		 						 		ahtb.close();
		 						 	}},
		 						 	{text:'No Thanks', action: function() {
		 						 		ahtb.close();
		 						 	}}]});
					return;

				}
				ahtb.close();
			})

			$('#agent-reservation #reserve').on('click', function(e) {
				var city = $('#information #user-data .service_areas').attr('city_id');
				testCityName = $('#information #user-data .service_areas').attr('city_name');
			 	testCityId = city? parseInt(city) : 0;
			 	if (!testCityId ||
			 		 testCityName.length == 0) {
			 		testCityName = $('#information #user-data .service_areas').val();
			 		if (testCityName.length == 0) {
			 			msg = "Please pick your city.";
			 			ahtb.push();
			 			window.setTimeout(function() {
			 				ahtb.open({html: msg, 
			 						   width: 400,
			 						   height: 120,
			 						   buttons:[
			 						 	{text:'OK', action: function() {
			 						 		ahtb.pop();
			 						 	}}]});
			 			}, 250);
						return;
			 		}
			 		else {
			 			getCityId(testCityId, testCityName);
			 			return;
			 		}
			 	}
			 	reallyReserve();		 	
			});
		}
	})
}

jQuery(document).ready(function($){
	var msg = 	'Lifestyledlistings.com wants you to <strong>capture leads and receive referral fees</strong> by using the PORTAL feature of our website.  ' +
				'This user friendly tool allows you to <strong>attract and manage interested buyers</strong>.  '+
				'Once a buyer enters the site through your PORTAL <strong>they will remain tied to you*.' +
				'</strong>  The benefit for you is you get a highly developed, lifestyle site that captures leads whom will remain with you <strong>' +
				'whether you refer them out or close an escrow.</strong><br/><br/> Share your link through your social medias, email contacts, advertisements, '+
				'add it to your business cards and email signatures. Spread the word and <strong>keep your business growing faster than ever!</strong>  ' +
				'Call us, and our specialists can help you work your social media sites to show your portal URL to your clients!' +
				'<span style="font-size:.9em;width:90%;text-align:center;display:block;margin-top:35px">*Disclaimer: Lifestyled Listings is legally required ' +
				'to give users the ability to detach from your PORTAL if they choose to.</span>';

	//$('#message').html(msg);
	$('[name=mobile], [name=phone]').mask('(999) 000-0000');

	$('#define-portal #portal').val(testPortal);
	$('#marker').css('color', 'lightgreen');
	$('#marker').hide();
	$('#message #base-url').text('LifestyledListings.com/');
	$('#message #portal').keyup(function(e){
		e.preventDefault();
		$('#marker').hide();
	})

	$('#check-it').on('click', function() {
		var val = $('#message #portal').val();
		if (val.length == 0) {
			ahtb.alert("Please enter a portal name.", 
						{height: 150,
						buttons: [{text: "OK", action: function() {
							ahtb.close();
						}}]});
			return;
		}
		ajax({
			query: 'check-portal-name',
			data: val,
			done: function(d) {
    			$('#marker').show();
    			testPortal = d;
    			$('#portal').val(d);
				// ahtb.alert("Congratulations, this portal name is available!", {height: 150, width: 450})
			},
			error: function(d) {
				ahtb.alert("The portal name "+val+" is already in use.</br>Please try a different name.", {height: 180, width: 450});
				$('#portal').val('');
				$('#marker').hide();
			}
		});
	});

	$('#information .reserveportal').on('click', function() {
		agentMatchDetail();
	})

	$('button#reserve').on('click', function() {
		if (testPortal.length == 0) {
			ahtb.alert('Please enter your desired portal name to reserve it.', {height: 170});
			return;
		}

		var first = $('#user-data #first_name').val();
		var last = $('#user-data #last_name').val();
		var phone = $('#user-data #phone').val().replace(/[^0-9]/g, '');
		var email = $('#user-data #email').val();
		var msg = '';
		if (first.length == 0)
			msg += "first name";
		if (last.length == 0)
			msg += (msg.length ? ", ": "") + "last name";
		if (phone.length == 0)
			msg += (msg.length ? ", ": "") + "phone";
		if (email.length == 0)
			msg += (msg.length ? " and ": "") + "email";

		if (msg.length) {
			msg = "Please enter "+msg+" so we can contact you.";
			ahtb.alert(msg, {height: 170});
			return;
		}
		
		ahtb.open({html: "Reserving..", 
				   hideSubmit: true});
		try{
		    __adroll.record_user({"adroll_segments": "c0156c72"})    
		} catch( err ) {
			if (typeof err == 'object' && typeof err != null && typeof err.message != 'undefined')
				console.log("Adroll exception - "+err.message);
			else
				console.log("Adroll exception of some sort");
			// ahtb.alert("AdRoll exception!", {height: 150});
			// return;
		}

		ajax({
			query: 'reserve-agent-product',
			data: {id: seller.id,
				   what: SellerFlags.SELLER_IS_PREMIUM_LEVEL_1,
				   lifestyles: testAgentMatchList,
				   portal: testPortal,
				   first: first,
				   last: last,
				   phone: phone,
				   email: email },
			done: function(d) {
				console.log("reserve-agent-product - "+d);
				var msg = "Your portal, "+testPortal+", is now reserved.  Please expect a call from us soon.  Please contact us via our Feedback!";
				if (email.length &&
					email.toLowerCase().indexOf('aol') != -1) {
					isAOL = true;
					msg +=  "<br/><br/>AOL users, please add support@lifetyledlistings.com to your whitelist, otherwise AOL may bounce any email from us.";
				}
				ahtb.alert(msg,
						{height: (isAOL ? 260 : 200),
						 width: 600});							
    		},
			error: function(d) {
				console.log("We're very sorry, had trouble reserving your portal. "+d);
				ahtb.alert("We're very sorry, had trouble reserving your portal. "+d, {height: 150, width: 450});
			}
		});
	})
})