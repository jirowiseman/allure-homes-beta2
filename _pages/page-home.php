<?php
require_once(__DIR__.'/../_classes/Listings.class.php'); global $Listings;
require_once(__DIR__.'/../_classes/ListingsTags.class.php');
require_once(__DIR__.'/../_classes/CitiesTags.class.php');
require_once(__DIR__.'/../_classes/Tags.class.php');
require_once(__DIR__.'/../_classes/Options.class.php'); global $Options; $Options = new AH\Options();
require_once(__DIR__.'/../_classes/Utility.class.php');

function updateMostViewed() {
	global $Options;
	global $Listings;
	$sql = 'SELECT a.id, b.clicks, b.viewed FROM '.$Listings->getTableName().' AS a ';
	$sql.= 'INNER JOIN '.$Listings->getTableName('listings-viewed').' AS b ';
	$sql.= 'WHERE b.listing_id = a.id ';
	$sql.= 'ORDER BY b.viewed DESC';

	$list = $Listings->rawQuery($sql);
	// usort($list, function($a, $b) {
	// 	if ($a->clicks != $b->clicks)
	// 		return $b->clicks - $a->clicks;
	// 	else
	// 		return strtotime($b->viewed) - strtotime($a->viewed);
	// });

	
	$updates = [];
	$i = 0;
	foreach($list as $id) {
		$updates[] = $id->id;
		if ($i++ == 100)
			break;
	}
	$Options->set([(object)['where'=>['opt'=>'ListingsMostViewed'],
							'fields'=>['value'=>json_encode($updates)]]]);
}

$cassie = isset($_COOKIE['CassiopeiaDirective']) ? $_COOKIE['CassiopeiaDirective'] : '';

global $isMobile;
$wpId = wp_get_current_user()->ID;

$portalRow = get_query_var('id'); //$_COOKIE['WAIT_PORTAL_ROW'];
if (empty($portalRow))
  $portalRow = 0;

$opt = $Options->get((object)['where'=>['opt'=>'ShowSideBarList']]);
$showSideBarList = !empty($opt) ? json_decode($opt[0]->value) : [8];
$showSideBar = in_array($wpId, $showSideBarList) && !$isMobile;

if ($showSideBar) {
  global $Listings;
  $Listings = new AH\Listings();
  updateMostViewed();
  
  $Tags = new AH\Tags();
  // most viewed listings
  $opt = $Options->get((object)['where'=>['opt'=>'ListingsMostViewed']]);
  $mostViewed = !empty($opt) ? json_decode($opt[0]->value) : [];
  $showMostViewedIds = [];
  $maxViewed = count($mostViewed) > 80 ? 80 : count($mostViewed);
  if (!empty($mostViewed)) {
    // $sizeOfIds = count($mostViewed);
    // for($i = 0; $i < $maxViewed; $i++) {
    //   while(true) {
    //     $index = rand() % $sizeOfIds;
    //     if (!in_array($mostViewed[$index], $showMostViewedIds)) {
    //       	$showMostViewedIds[] = $mostViewed[$index];
    //       	break;
    //     }
    //     if (count($showMostViewedIds) == $sizeOfIds)
    //     	break;
    //   }
    // }
    $showMostViewedIds = array_merge($mostViewed);
    $mostViewed = $Listings->get((object)['where'=>['id'=>$showMostViewedIds],
                                          'what'=>['id','title','images','city','state','price','active']]);

    $temp = [];
    foreach($mostViewed as $l)
      $temp[$l->id] = $l;
    unset($mostViewed);
    $mostViewed = $temp;

    $temp = [];
    foreach($showMostViewedIds as $id) {
      $listing = $mostViewed[$id];
      $sql = "SELECT DISTINCT a.id, a.icon, a.description FROM ".$Tags->getTablename()." AS a ";
      $sql .="INNER JOIN ".$Tags->getTablename('listings-tags')." AS b ON b.listing_id = ".$id." AND b.tag_id = a.id ";
      $sql .="INNER JOIN ".$Tags->getTablename('cities-tags')." AS c ON c.tag_id = b.tag_id ";
      $sql .="WHERE a.type = 1 AND a.icon IS NOT NULL AND (c.score >= 7 OR c.score = -1)";
      $listing->tags = $Tags->rawQuery($sql);
      $temp[$listing->id] = $listing;
    }
    $mostViewed = $temp;
  }
}

if ($_SERVER['SERVER_NAME'] == 'localhost') $local = true; // So I can work locally without pulling remote scripts
else $local = false;

global $thisPage; 
$opt = $Options->get((object)['where'=>['opt'=>'VideoList']]);
// $videoId = 166059782;
$videoList = new stdClass();
$videoList->home = 166059782;
if (!empty($opt)) {
  $videoList = json_decode($opt[0]->value);
  foreach($videoList as $page=>$video) {
    if ( strpos(strtolower($page), $thisPage) === 0) {
      $videoList->$page = $video;
    }
  }
}

//PredefinedQuizListHome
global $agent; // portal agent from header.php
$opt = $Options->get((object)['where'=>['opt'=>'PredefinedQuizListHome']]);
$predefinedQuizList = empty($opt) ? [] : json_decode($opt[0]->value);
$predefinedQuizOptions = null;
if (!empty($predefinedQuizList) ) {
	if (!empty($agent)) foreach($predefinedQuizList as $item) {
		if ($item->agentID == $agent->author_id) {
			$predefinedQuizOptions = $item;
			break;
		}
		unset($item);
	}
	// if we didn't find a match, see if there is a global one
	if (empty($predefinedQuizOptions)) foreach($predefinedQuizList as $item) {
		if ($item->agentID == -1) {
			$predefinedQuizOptions = $item;
			break;
		}
		unset($item);
	}
}

?>
<script type="text/javascript">
var myPortalRow = '<?php echo $portalRow; ?>';
var hasNoShowBenefits = getCookie("NoShowBenefits");
var local = <?php echo $local ? 1 : 0; ?>;
var showSideBar = <?php echo $showSideBar ? 1 : 0 ?>;
var videoList = <?php echo json_encode($videoList); ?>;

jQuery(document).ready(function($){
	 // ---- Load High Resolution Images for Retina Displays ---- //
	function hdAgents() {
		$('#lifestyled-agents .bottom .agent-slider .slide1 .image img').replaceWith( '<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/agent1-large.jpg"/>' );
		$('#lifestyled-agents .bottom .agent-slider .slide2 .image img').replaceWith( '<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/agent2-large.jpg"/>' );
		$('#lifestyled-agents .bottom .agent-slider .slide3 .image img').replaceWith( '<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/agent3-large.jpg"/>' );
		$('#lifestyled-agents .bottom .agent-slider .slide4 .image img').replaceWith( '<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/agent4-large.jpg"/>' );
	}
	if (isRetina){
		$('.home-wrap #lifestyled-agents .top img').replaceWith( '<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/lifestyled-bg-large.jpg"/>' );
		hdAgents();
	}
	if (isMobile){
		hdAgents();
	}
	// ---- Animated Loadins ---- //
	timeoutID = window.setTimeout(delaythree, 500);
	function delaythree() {
		$('#home-top .belowsearch .three-image .text').addClass('animate');
		$('#home-top .belowsearch .three-image .smalltext').addClass('animate');
	}
	$('#home-top .mainsearch .opening').addClass('animate');
	
	(function($) {
		$.fn.visible = function(partial) {
				var $t            = $(this),
						$w            = $(window),
						viewTop       = $w.scrollTop(),
						viewBottom    = viewTop + $w.height(),
						_top          = $t.offset().top,
						_bottom       = _top + $t.height(),
						compareTop    = partial === true ? _bottom : _top,
						compareBottom = partial === true ? _top : _bottom;

			return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
		}; 
	})(jQuery);
	var win = $(window);
	
	var allMods = $('.home-wrap #home-bottom .home-bottom-bg');
	allMods.each(function(i, el) {
		var el = $('.home-wrap #home-bottom .home-bottom-bg');
		if (el.visible(true)) {
			$('.home-wrap .home-bottom-bg').addClass('active');
		} 
	});
	win.scroll(function(event) {
		allMods.each(function(i, el) {
			var el = $('.home-wrap #home-bottom .home-bottom-bg');
			if (el.visible(true)) {
				$('.home-wrap .home-bottom-bg').addClass('animate');
			} 
		});
	});
	
	var textboxLoadin = $('.home-wrap #home-mid-slider');
	textboxLoadin.each(function(i, el) {
		var el = $('.home-wrap #home-mid-slider');
		if (el.visible(true)) {
			$('#home-mid-slider .slider-wrapper .slide1 .textbox').addClass('animate');
		}
	});
	win.scroll(function(event) {
		textboxLoadin.each(function(i, el) {
			var el = $('.home-wrap #home-mid-slider');
			if (el.visible(true)) {
				$('#home-mid-slider .slider-wrapper .slide1 .textbox').addClass('animate');
			}
		});
	});
	

	// ---- Info Slider ---- //
	function activeInfo1() {
		$('#home-mid-slider .slider-wrapper').addClass('slide1active');
		$('#home-mid-slider .slider-wrapper').removeClass('slide2active');
		$('#home-mid-slider .slider-wrapper').removeClass('slide3active');
		$('.mid-slider-buttons .button1').addClass('active');
		$('.mid-slider-buttons .button2').removeClass('active');
		$('.mid-slider-buttons .button3').removeClass('active');
	}
	function activeInfo2() {
		$('#home-mid-slider .slider-wrapper').removeClass('slide1active');
		$('#home-mid-slider .slider-wrapper').addClass('slide2active');
		$('#home-mid-slider .slider-wrapper').removeClass('slide3active');
		$('.mid-slider-buttons .button1').removeClass('active');
		$('.mid-slider-buttons .button2').addClass('active');
		$('.mid-slider-buttons .button3').removeClass('active');
	}
	function activeInfo3() {
		$('#home-mid-slider .slider-wrapper').removeClass('slide1active');
		$('#home-mid-slider .slider-wrapper').removeClass('slide2active');
		$('#home-mid-slider .slider-wrapper').addClass('slide3active');
		$('.mid-slider-buttons .button1').removeClass('active');
		$('.mid-slider-buttons .button2').removeClass('active');
		$('.mid-slider-buttons .button3').addClass('active');
	}
	
	if (isMobile){
		var slideDiv1 = $('#home-mid-slider .slider-wrapper .slide1');
		var slideDiv2 = $('#home-mid-slider .slider-wrapper .slide2');
		var slideDiv3 = $('#home-mid-slider .slider-wrapper .slide3');
		Hammer(slideDiv1[0]).on('swipeleft', function(event) {
			activeInfo2();
		});
		Hammer(slideDiv2[0]).on('swiperight', function(event) {
			activeInfo1();
		});
		Hammer(slideDiv2[0]).on('swipeleft', function(event) {
			activeInfo3();
		});
		Hammer(slideDiv3[0]).on('swiperight', function(event) {
			activeInfo2();
		});
	}
	
	$('.mid-slider-buttons .button1').on('click', function() {
		activeInfo1();
	});
	$('.mid-slider-buttons .button2').on('click', function() {
		activeInfo2();
	});
	$('.mid-slider-buttons .button3').on('click', function() {
		activeInfo3();
	});
	
	// ---- Agent Slider ---- //
	function nextAgent() {
	$('.bottom .agent-slider li.active').removeClass('temp');
		$('.bottom .agent-slider li.active').removeClass('active')
		.next().add('.bottom .agent-slider li:first').last().addClass('temp');
		timeoutID = window.setTimeout(delayopacity, 250);
		function delayopacity() {
			$('.bottom .agent-slider li.temp').addClass('active');
			$('.bottom .agent-slider li.temp').removeClass('temp');
		}
		$('.bottom .buttons li.active').removeClass('active')
		.next().add('.bottom .buttons li:first').last().addClass('active');
	}
	function prevAgent() {
	$('.bottom .agent-slider li.active').removeClass('temp');
		$('.bottom .agent-slider li.active').removeClass('active')
		.prev().add('.bottom .agent-slider li:first').last().addClass('temp');
		timeoutID = window.setTimeout(delayopacity, 250);
		function delayopacity() {
			$('.bottom .agent-slider li.temp').addClass('active');
			$('.bottom .agent-slider li.temp').removeClass('temp');
		}
		$('.bottom .buttons li.active').removeClass('active')
		.prev().add('.bottom .buttons li:first').last().addClass('active');
	}
	function activeAgent1() {
		clearInterval(interval);
		$('.bottom .agent-slider li.active').removeClass('active');
		timeoutID = window.setTimeout(delayopacity, 250);
		function delayopacity() {
			$('.bottom .agent-slider .slide1').addClass('active');
		}
		$('.bottom .buttons li.active').removeClass('active');
		$('.bottom .button1').addClass('active');
	}
	function activeAgent2() {
		clearInterval(interval);
		$('.bottom .agent-slider li.active').removeClass('active');
		timeoutID = window.setTimeout(delayopacity, 250);
		function delayopacity() {
			$('.bottom .agent-slider .slide2').addClass('active');
		}
		$('.bottom .buttons li.active').removeClass('active');
		$('.bottom .button2').addClass('active');
	}
	function activeAgent3() {
		clearInterval(interval);
		$('.bottom .agent-slider li.active').removeClass('active');
		timeoutID = window.setTimeout(delayopacity, 250);
		function delayopacity() {
			$('.bottom .agent-slider .slide3').addClass('active');
		}
		$('.bottom .buttons li.active').removeClass('active');
		$('.bottom .button3').addClass('active');
	}
	function activeAgent4() {
		clearInterval(interval);
		$('.bottom .agent-slider li.active').removeClass('active');
		timeoutID = window.setTimeout(delayopacity, 250);
		function delayopacity() {
			$('.bottom .agent-slider .slide4').addClass('active');
		}
		$('.bottom .buttons li.active').removeClass('active');
		$('.bottom .button4').addClass('active');
	}

	var startAgents = function(){
		nextAgent();
	}
	var interval = setInterval(startAgents, 10000);
	if (isMobile){
		var slideAgent = $('.bottom .agent-slider');
		Hammer(slideAgent[0]).on('swipeleft', function(event) {
			clearInterval(interval);
			nextAgent();
		});
		Hammer(slideAgent[0]).on('swiperight', function(event) {
			clearInterval(interval);
			prevAgent();
		});
	}
	$('.bottom .button1').on('click', function() {
		activeAgent1();
	});
	$('.bottom .button2').on('click', function() {
		activeAgent2();
	});
	$('.bottom .button3').on('click', function() {
		activeAgent3();
	});
	$('.bottom .button4').on('click', function() {
		activeAgent4();
	});
});
</script>

<div id="overlay-bg" style="display: none;">
	<div class="spin-wrap"><div class="spinner sphere"></div></div>
</div>

<div class="home-wrap">
  <section id="home-top">
    <div id="most-viewed" <?php echo !$showSideBar ? 'style="display: none;"' : ''; ?> >
      <span class="title">Most Viewed Listings</span>
      <ul id="most-viewed">
      <?php
        if ($showSideBar) 
          foreach ($mostViewed as $l){
            ?>
            <li class="listing" listing-id="<?php echo $l->id; ?>" style="min-height: 200px; width: 300px;" >
              <?php
              $file = '';
              if (is_array($l->images) && count($l->images) > 0)
                foreach($l->images as $img) {
                  if (isset($img->file)) {
                  	if (substr($img->file, 0, 4) == 'http')
                  		$file = $img->file;
                  	elseif (file_exists(__DIR__.'/../_img/_listings/845x350/'.$img->file))
                  		$file = get_bloginfo('template_directory').'/_img/_listings/845x350/'.$img->file;
                    if (!empty($file))
                    	break;
                  }
                }
              ?>
              <a href="<?php echo get_home_url()."/listing/$l->id"; ?>" >
                <span alt="US Lifestyled Homes" class="grayscale mobileimages" style="background:url(<?php echo $file; ?>) no-repeat; background-size: cover; display:block; height: 200px"/>
                <div class="listing-info">
                  <!--<h3><?php echo $l->title; ?></h3>-->
                  <span class="price"><?php echo empty($l->price) ? 'LifeStyle' : '$'.number_format($l->price); ?></span>
                  <span class="location"><?php
                    if ($l->city && $l->city != -1) {
                      echo $l->city;
                      if ($l->state && $l->state != -1) echo ', ';
                    } if ($l->state && $l->state != -1) echo $l->state;
                  ?></span>
                  <span> <?php
                    if (!empty($l->tags)) {
                      echo '<table class="tag-icons"><tr>';
                      foreach($l->tags as $tag) { // '.(!empty($l->tags) ? json_encode($l->tags) : '0').'
                        echo '<td><a href="javascript:explainIcons('.(!empty($l->tags) ? preg_replace('/\"/',"'",json_encode($l->tags)) : '0').');"><img src="'.get_template_directory_uri().'/_img/tag_icons/'.$tag->icon.'"/></a></td>';
                      }
                      echo '</tr></table>';
                    }
                  ?></span>
                  <!--<a href="#" listing-id="<?php echo $l->id; ?>">View Listing</a>-->
                </div><!-- .listing-info -->
              </a>
            </li><!-- .listing -->
            <?php
          }
      ?>
      </ul>
    </div> <!-- end most-viewed sidebar -->

		<div class="mainsearch">
			<div class="mainsearch-bg"></div>
			<div class="opening">
				<div class="title">Search by Lifestyle</div>
				<div class="subtext">Discover amazing locations and homes that match your lifestyle perfectly</div>
				<a href="#videoDiv" class="video" id="home"><span class="entypo-right-dir"></span></a>
				<a class="start" href="<?php bloginfo('wpurl'); ?>/quiz/#sq=0">Find your home!</a>
			</div>
		</div>
		<div class="belowsearch">
			<div class="three-image">
				<?php if (!$predefinedQuizOptions ||
						  !$predefinedQuizOptions->showPredefinedQuiz ||
						   empty($predefinedQuizOptions->home->quiz_list)) : ?>
				<div class="image1"><span class="mobilegray"></span><span class="text">Great homes</span></div>
				<div class="image2"><span class="mobilegray"></span><span class="text">In amazing locations</span><span class="smalltext">Amazing locations</span></div>
				<div class="image3"><span class="mobilegray"></span><span class="text">Discovered in minutes</span></div>
				<?php else:
				if ($predefinedQuizOptions->showPredefinedQuiz) {
					if (!empty($predefinedQuizOptions->home->intro)) {
						echo '<div class="intro">';
						AH\outputDynamicHtml($predefinedQuizOptions->home->intro);
						echo '</div>';
					}
					$quizList = '';
					$quizIndex = 1;
					foreach($predefinedQuizOptions->home->quiz_list as $i=>$quiz) {
						$quizHref = get_home_url().($quiz->quizMode == 0 ? "/quiz-results/R-$quiz->quizID" : "/$quiz->quizSEO");
						$class = "image".($quizIndex++);
						$quizList .= '<div class="'.$class.'" style="background:url('.get_template_directory_uri()."/".$predefinedQuizOptions->imgPath.'/'.$predefinedQuizOptions->home->slideSize."/$quiz->img".') no-repeat; background-size: cover;">';
						$quizList .=	'<a href="javascript:jumpToPredefinedQuiz('."'".$quizHref."'".');" class="mobilegray">';
						$quizList .=		'<img class="icon" src="'.get_template_directory_uri()."/_img/tag_icons/".$quiz->icon.'" />';
						$quizList .=		'<span class="text">'.$quiz->title.'</span>';
						$quizList .=	'</a>';
						$quizList .= '</div>';
					}
					echo $quizList;
				}
				endif;
				?>
			</div>
			<div class="process">What is our process?</div>
		</div>
  </section>
	<div class="videoDiv" id="home">
		<button class="closeVid"><span class="line1"></span><span class="line2"></span></button>
		<span class="video"></span>
	</div>
	<div class="mid-slider-buttons">
		<span class="button1 active"></span>
		<span class="button2"></span>
		<span class="button3"></span>
	</div>
	<section id="home-mid-slider">
		<div class="slider-wrapper slide1active">
			<div class="slide1">
				<div class="mobiletextbox">
					<span class="title">Make a custom search</span>
					<div class="mobile-image"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/slide1.jpg"/></div>
					<div class="text">
						<p>Take our fun and fast lifestyle quiz and choose what activities, city features, amenities, and more you want in or near your dream home!</p>
						<a class="start" href="<?php bloginfo('wpurl'); ?>/quiz/#sq=0">Start your quiz!</a>
					</div>
				</div>
				<div class="textbox">
					<span class="title">Make a custom search</span>
					<p>Take our fun and fast lifestyle quiz and choose what activities, city features, amenities, and more you want in or near your dream home!</p>
					<a class="start" href="<?php bloginfo('wpurl'); ?>/quiz/#sq=0">Start your quiz!</a>
				</div>
			</div>
			<div class="slide2">
				<div class="mobiletextbox">
					<span class="title">Discover new locations and homes!</span>
					<div class="mobile-image"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/slide2.jpg"/></div>
					<div class="text">
						<p>See the best cities for your lifestyle and homes in those cities ordered by the closest fit to what you asked for.</p>
						<a class="start" href="<?php bloginfo('wpurl'); ?>/quiz/#sq=0">Start your quiz!</a>
					</div>
				</div>
				<div class="textbox">
					<span class="title">Discover new locations and homes!</span>
					<p>See the best cities for your lifestyle and homes in those cities ordered by the closest fit to what you asked for.</p>
					<a class="start" href="<?php bloginfo('wpurl'); ?>/quiz/#sq=0">Start your quiz!</a>
				</div>
			</div>
			<div class="slide3">
				<div class="mobiletextbox">
					<span class="title">Find the perfect agent</span>
					<div class="mobile-image"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/slide3.jpg"/></div>
					<div class="text">
						<p>Find real estate agents like never before. We match you to agents who are experts in the lifestyle or property type you're looking for.</p>
						<a class="start" href="<?php bloginfo('wpurl'); ?>/quiz/#sq=0">Start your quiz!</a>
					</div>
				</div>
				<div class="textbox">
					<span class="title">Find the perfect agent</span>
					<p>Find real estate agents like never before. We match you to agents who are experts in the lifestyle or property type you're looking for.</p>
					<a class="start" href="<?php bloginfo('wpurl'); ?>/quiz/#sq=0">Start your quiz!</a>
				</div>
			</div>
		</div>
  </section>
	<div class="slider-bottom"><p>We are proud to work with agents from</p></div>
  <section id="lifestyled-agents">
		<img class="agency-large" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/agency-large.jpg"/>
		<img class="agency-small" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/agency-small.jpg"/>
		<div class="top">
			<div class="info-box">
				<span class="title">What are Lifestyled <span>Agents?</span></span>
				<p>Agents who love and specialize in the lifestyles you are looking to buy into. See them featured on listings on our site.</p>
			</div>
			<span class="agent-banner"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/lifestyled-bg.jpg"/></span>
		</div>
		<div class="bottom">
			<ul class="buttons">
				<li class="button1 active"></li>
				<li class="button2"></li>
				<li class="button3"></li>
				<li class="button4"></li>
			</ul>
			<ul class="agent-slider">
				<li class="slide1 active">
					<div class="mobile-agent-title"><span class="title">Meet Mike Jansen</span></div>
					<div class="image"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/agent1.jpg"/></div>
					<div class="agent-box">
						<span class="title">Meet Mike Jansen</span>
						<p>As an owner of Premier Seaside Group, the official affiliate of Christie's International, Mike and his team dedicate more than 70% of their business to dealing with inter coastal and beach front homes for the most discerning clients from around the world. His knowledge of the Pinellas County Coastline from land and sea is unmatched, making him the go to agent for any buyer that enjoys sailing, fishing, or relaxing on the beach!</p>
						<a class="signup" href="<?php bloginfo('wpurl'); ?>/agent-purchase-lifestyle">Sign up as an agent <span class="entypo-right-dir"></span></a>
					</div>
				</li>
				<li class="slide2">
					<div class="mobile-agent-title"><span class="title">Meet Nicole Beauchamp</span></div>
					<div class="image"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/agent2.jpg"/></div>
					<div class="agent-box">
						<span class="title">Meet Nicole Beauchamp</span>
						<p>Attending one of the most musical oriented high school's in the city gave Nikki a love for music and put her on the path to be a classically trained pianist. As a lifetime Manhattanite and condo resident for over 10 years, she understands fully the impact that owning a condo of your own has on your lifestyled. Her creative and adverturous nature has connected her deeply with the pulse of Manhattan making her invaluable.</p>
						<a class="signup" href="<?php bloginfo('wpurl'); ?>/agent-purchase-lifestyle">Sign up as an agent <span class="entypo-right-dir"></span></a>
					</div>
				</li>
				<li class="slide3">
					<div class="mobile-agent-title"><span class="title">Meet Mark Frye</span></div>
					<div class="image"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/agent3.jpg"/></div>
					<div class="agent-box">
						<span class="title">Meet Mark Frye</span>
						<p>Mark is an avid boater that works hard to enjoy the lake as much as possible. Competitive in his younger years, he was ranked #1 in the 1996 Slalom Course for men under 21 in Michigan. Before Mark became a Realtor in 2001, he was a Golf Caddie for 3 years and built a respect for the game, and the people that love it. That experience gives him the attention to detail that buyers searching for their dream golf home value most.</p>
						<a class="signup" href="<?php bloginfo('wpurl'); ?>/agent-purchase-lifestyle">Sign up as an agent <span class="entypo-right-dir"></span></a>
					</div>
				</li>
				<li class="slide4">
					<div class="mobile-agent-title"><span class="title">Meet Megan Gebhardt</span></div>
					<div class="image"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/agent4.jpg"/></div>
					<div class="agent-box">
						<span class="title">Meet Megan Gebhardt</span>
						<p>As a lifelong equestrian, Megan gets it. She trains, breeds and rides and is familiar with all disciplines from performance to trail. Any agent can put together a list of properties that are zoned for horses, but only a true horse person will know what to look for when it comes to finding the right equestrian property.</p>
						<a class="signup" href="<?php bloginfo('wpurl'); ?>/agent-purchase-lifestyle">Sign up as an agent <span class="entypo-right-dir"></span></a>
					</div>
				</li>
			</ul>
		</div>
	</section>
	<section id="home-bottom">
		<span class="mobile-title">Lifestyled Listings is a <span class="mobilestyling">one stop shop</span></span>
		<div class="home-bottom-bg"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/iphone-large.jpg"/></div>
		<div class="content">
			<div class="top">
				<span class="title">Lifestyled Listings is a <span class="mobilestyling">one stop shop</span></span>
				<p>With Lifestyled Listings we help you find the best places to live and the best places to retire. We've designed our home search to lead you through the full home buying process. It starts with discovering</p>
			</div>
			<div class="cities">
				<span class="title">
					<span class="image">
						<span>Cities</span>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/cities.jpg"/>
					</span>
					<p>Over 27,000 Cities to choose from</p>
				</span>
				<p>Finding the right location to buy your dream home can be an intimidating process, but we're here to help. Just tell us what you want and we'll show you some great cities for whatever you're interested in. Looking for a thriving wine, golf and boating scene? We've got you covered.</p>
			</div>
			<div class="listings">
				<span class="title">
					<span class="image">
						<span>Homes</span>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/listings.jpg"/>
					</span>
					<p>1.6+ Million listings updated daily</p>
				</span>
				<p>We rank our massive listing database according to how well each one matches the features you've selected. Save time and energy when your results are ordered for you and easily compare how far your money goes in each of the markets you're interested in. Easily change the importance of each home feature or flag them as "must haves" to really get a custom viewing experience.</p>
			</div>
			<div class="agents">
				<span class="title">
					<span class="image">
						<span>Agents</span>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/agents.jpg"/>
					</span>
					<p>Over 430,000 active real estate agents</p>
				</span>
				<p>With so many agents to choose from and a constant focus on marketing, it can be hard to find the agent that best suits your needs. We tailor our agent matches to your search so you only see the agents that have experience and knowledge about the things that YOU are interested in. Read their detailed descriptions to understand what makes them so uniquely suited to your needs.</p>
			</div>
		</div>
	</section>
	<div class="calltoaction">
		<p>Experience it for yourself!</p>
		<a class="start" href="<?php bloginfo('wpurl'); ?>/quiz/#sq=0">Find your home!</a>
	</div>
</div><!-- .home-wrap -->