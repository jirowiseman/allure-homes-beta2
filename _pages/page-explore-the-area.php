<?php
global $wp_query;
require_once(__DIR__.'/../_classes/Listings.class.php');
$Listings = new AH\Listings();
$listing_id = intval(get_query_var('id'));

// Get listing title & geoinfo
$Listing = $Listings->rawQuery("SELECT a.id, a.title, a.city_id, b.lat, b.lng, b.address FROM {$Listings->getTableName($Listings->table)} as a LEFT JOIN {$Listings->getTableName('listings-geoinfo')} as b ON a.id = b.listing_id WHERE a.id = {$Listings->prepare('%d', $listing_id)}");

if ( !isset($Listing) ) :
    ?><h1>Unable to find listing.</h1><?php
else :

$Listing = array_pop($Listing);
$badAddress = ( !isset($Listing->lat) || !isset($Listing->lng) || ( intval($Listing->lat) === -1 && intval($Listing->lng) === -1 ) );
if ( $badAddress ) {
	require_once(__DIR__.'/../_classes/Cities.class.php'); $Cities = new AH\Cities();
	$city = $Cities->get((object)['where'=>['id'=>$Listing->city_id]]);
	if ( !empty($city) &&
	     intval($city[0]->lng) != -1 &&
	     intval($city[0]->lat) != -1 ) {
		$Listing->lng = floatval($city[0]->lng);
		$Listing->lat = floatval($city[0]->lat);
	}
}

if ( !isset($Listing->lat) || !isset($Listing->lng) || ( intval($Listing->lat) === -1 && intval($Listing->lng) === -1 ) ) :
    ?><h1>Google maps could not locate this address.</h1><?php
else :

// get the categories
require_once(__DIR__.'/../_classes/PointsCategories.class.php');
$PointsCategories = new AH\PointsCategories();
$PointsCategories = $PointsCategories->get((object)[ 'where' => [ 'parent' => 0 ] ]);

// parse categories
$places = [];
foreach ($PointsCategories as $row){
    $places[$row->id] = (object)[
        'category' => $row->category,
        'points' => [],
    ];
}

// look for POIs within range of listing->lat & listing->lng
require_once(__DIR__.'/../_classes/Points.class.php');
$Points = new AH\Points();
$range = [ .72, 1 ]; // lat, lng radius (in degrees)
$points_list = $Points->rawQuery("SELECT a.id, a.name, a.address, a.lat, a.lng, b.category_id FROM {$Points->getTableName($Points->table)} as a LEFT JOIN {$Points->getTableName('points-taxonomy')} as b ON a.id = b.point_id WHERE lat > {$Points->prepare('%f', $Listing->lat - $range[0])} AND lat < {$Points->prepare('%f', $Listing->lat + $range[0])} AND lng > {$Points->prepare('%f', $Listing->lng - $range[1])} AND lng < {$Points->prepare('%f', $Listing->lng + $range[1])}");

// Parse results and calculate distance
require_once(__DIR__.'/../_classes/GoogleLocation.class.php');
$google = new AH\GoogleLocation();
if (!empty( $points_list ))
    foreach ($points_list as $row){
        $row->category_id = intval($row->category_id);
        $row->id = intval($row->id);
        $places[$row->category_id]->points[$row->id] = (object)[
            'id' => $row->id,
            'lat' => floatval($row->lat),
            'lng' => floatval($row->lng),
            'name' => $row->name,
            'address' => $row->address,
            'distance' => $google->getDistance($row->lat, $row->lng, $Listing->lat, $Listing->lng)
        ];
    }

$x = [];
foreach ($places as $row)
    $x[$row->category] = $row->points;
$places = $x;

?>
<style>
.info-content{color: black;font-size: 0.9em;}
.info-content h1 {font-size: 1.3em; line-height: 1em; margin: 0;}
.info-content div { height: 2.2em; margin: 0 0 0.2em; }
.info-content p {line-height: 1.2em; margin: 0 0 0.2em; }
.pager { clear: both; display: block; overflow: auto; position: relative; }
.pager .next { float: right; }
.pager .prev { float: left; }
</style>
<script type="text/javascript">
	var explore_info = {
		lat: <?php echo $Listing->lat; ?>,
		lng: <?php echo $Listing->lng; ?>,
		places: <?php echo json_encode($places); ?>,
        id: <?php echo $Listing->id; ?>,
	}
</script>
<div id="page-explore-the-area">
	<header>
        <section class="sub-nav">
            <div class="left">
                <!--<a href="javascript:window.history.go(-2);">My Search</a> <span class="entypo-right-open-big"></span>-->
                <a href="javascript:window.history.back();">Listing <?php echo $listing_id; ?></a> <span class="entypo-right-open-big"></span>
                <a href="<?php bloginfo('wpurl'); ?>/explore-the-area/<?php echo $listing_id; ?>">Explore the Area</a>
            </div>
            <div class="right">
                <a href="#">Speak with Listing Agent</a>
                <!--<a href="#">Save This Listing</a>-->
            </div>
        </section>
        <section class="banner">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-explore-the-area/explore-general.jpg" />
            <!--<ul>
                <li class="intro">At a Glance</li>
                <li class="map">Lifestyle Map</li>
                <li class="area-guides">Area Guides</li>
            </ul>-->
        </section>
	</header>
    <div class="content-wrap">
        <!--<section id="view-intro" class="intro">
            <div class="left">
                <img src="<?php bloginfo('template_directory'); ?>/_img/page-explore-the-area/temp/title_intro.png" />
                <p>The most beautiful west coast city that reaches incredible heights. Its hills steep, its bridges esteemed, and its vistas incomparable, this energetic bohemian outpost packs a lot into its 7x7 mile environs. San Francisco's neighborhoods and the locals that love them each tell their own story. Take the time to speak with them and you'll learn a lot, you're likely to find a resident that spends the extra time to help you get where you want to go, and then suggests a spot they know that you just might like even better.</p>
            </div>
            <div class="right">

            </div>
        </section>-->
        <section id="view-map" class="map">
            <div id="places-wrap">
                <ul id="places-menu">
                    <li class="attractions" category="attractions"><img src="<?php bloginfo('template_directory'); ?>/_img/page-explore-the-area/category_icons/attractions.png" /><a>Attractions</a></li>
                    <li class="golf" category="golf"><img src="<?php bloginfo('template_directory'); ?>/_img/page-explore-the-area/category_icons/golf.png" /><a>Golf Courses</a></li>
                    <li class="outdoors" category="outdoors"><img src="<?php bloginfo('template_directory'); ?>/_img/page-explore-the-area/category_icons/outdoors.png" /><a>Outdoors</a></li>
                    <li class="nightlife" category="nightlife"><img src="<?php bloginfo('template_directory'); ?>/_img/page-explore-the-area/category_icons/nightlife.png" /><a>Nightlife</a></li>
                    <li class="dining" category="dining"><img src="<?php bloginfo('template_directory'); ?>/_img/page-explore-the-area/category_icons/dining.png" /><a>Dining</a></li>
                    <li class="shopping" category="shopping"><img src="<?php bloginfo('template_directory'); ?>/_img/page-explore-the-area/category_icons/shopping.png" /><a>Shopping</a></li>
                    <li class="arts" category="arts"><img src="<?php bloginfo('template_directory'); ?>/_img/page-explore-the-area/category_icons/arts.png" /><a>Arts</a></li>
                </ul>
            </div>
            <div id="map-wrap">
                <div id="map-canvas"></div>
                <!--<ul id="places-list" class="scrollbar">
                    <li class="attractions"><ul><li class="title">Attractions - <span class="count"></span></li></ul></li>
                    <li class="golf"><ul><li class="title">Golf Courses - <span class="count"></span></li></ul></li>
                    <li class="outdoors"><ul><li class="title">Outdoors - <span class="count"></span></li></ul></li>
                    <li class="nightlife"><ul><li class="title">Nightlife - <span class="count"></span></li></ul></li>
                    <li class="dining"><ul><li class="title">Dining - <span class="count"></span></li></ul></li>
                    <li class="shopping"><ul><li class="title">Shopping - <span class="count"></span></li></ul></li>
                    <li class="arts"><ul><li class="title">Arts - <span class="count"></span></li></ul></li>
                </ul>-->
            </div>
        </section>
        <!--<section id="view-area-guides" class="area-guides">
            <img src="<?php bloginfo('template_directory'); ?>/_img/page-explore-the-area/temp/area-guides.jpg" />
        </section>-->
    </div>
    
</div>
<?php endif; endif; ?>
