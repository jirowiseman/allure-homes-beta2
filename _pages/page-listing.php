<?php
$listing_id = get_query_var('id');
$extra = get_query_var('extra');
$extra2 = empty(get_query_var('extra2')) ? 0 : get_query_var('extra2');
if (strpos($extra2, 'F-') !== false)
	$extra2 = 0;
global $havePortal; // from header.php
if (strpos(get_home_url(), 'local') !== false)
	$specialAgents = [];
elseif (strpos(get_home_url(), 'alpha') !== false)
	$specialAgents = [];
elseif (strpos(get_home_url(), 'mobile') !== false)
	$specialAgents = [];
else
	$specialAgents = [5,8,381]; // just Kendall, Tom, Bennett excluded

if ( empty($listing_id) ) :
	?>
	<h2>Query not set.</h2>
	<?php
else :
	require_once(__DIR__.'/../_classes/Utility.class.php');
	require_once(__DIR__.'/../_classes/Listings.class.php'); global $Listings; $Listings = new AH\Listings(1);
	require_once(__DIR__.'/../_classes/ListingsGeoinfo.class.php'); global $ListingsGeoinfo; $ListingsGeoinfo = new AH\ListingsGeoinfo();
	require_once(__DIR__.'./../_classes/PortalUsersMgr.class.php'); global $PortalUsersMgr; $PortalUsersMgr = new AH\PortalUsersMgr();
	require_once(__DIR__.'./../_classes/ListingsViewed.class.php'); global $ListingsViewed; $ListingsViewed = new AH\ListingsViewed();

	global $timezone_adjust; // from Utility
	global $myTagList;
	$myTagList = '';

	$Listings->log("listing page: id$listing_id, extra:".(!empty($extra) ? $extra : "N/A").", extra2:".(!empty($extra2) ? $extra2 : "N/A"));

	function userIP() {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		    $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = $_SERVER['REMOTE_ADDR'];
		}

		if ( ($pos = strpos($ip, ',')) !== false)
			$ip = substr($ip, 0, $pos);
		
		return $ip;
	}

	function getCitiesTags(&$listing) {
		require_once(__DIR__.'/../_classes/Cities.class.php'); $Cities = new AH\Cities(1);
		$sql = 'SELECT c.id, c.tag, c.type, b.score FROM `'.$Cities->getTableName().'` AS a ';
		$sql .= 'INNER JOIN '.$Cities->getTableName('cities-tags').' AS b ON a.id = b.city_id AND (b.score >= 7 OR b.score = -1) ';
		$sql .= 'INNER JOIN '.$Cities->getTableName('tags').' AS c ON c.id = b.tag_id ';
		if (!empty($listing->city_id) &&
			intval($listing->city_id) != -1)
			$sql .= 'WHERE a.id = '.$listing->city_id;
		else
			$sql .= 'WHERE a.city = "'.$listing->city.'" AND a.state = "'.$listing->state.'"';
		$Cities->log("getCitiesTags - sql: $sql");

		$cityTags = $Cities->rawQuery($sql);

		$listing->cityTags = array();
		if (!empty($cityTags)) foreach($cityTags as $tag) {
			if ($tag->score == -1)
				$tag->score = 10;
			$listing->cityTags[$tag->id] = $tag;
		}
	}

	function updateUserVisitations(&$meta, &$visits, $userId, $ip, $quizId) {
		global $myTagList;
		global $Listings;
		global $timezone_adjust;

		$gotOne = false;
		$gotDate = false;
		$needUpdate = false;
		$stamp = date("Y-m-d", time() + ($timezone_adjust*3600));

		$Listings->log("updateUserVisitations - entered, user:$userId for $ip and quizId:$quizId");
		// $visit == array of user IDs.  Each of those are arrays of IPs, Each IP is an array of dates
		if (count($visits->users)) 
			foreach($visits->users as $id=>&$ips) {
				$id = intval($id);
				if ($id == $userId) {
					foreach($ips as $addr=>&$theDates) {
						$theDates = (array)$theDates;
						if ($addr == $ip) {						
							foreach($theDates as $date=>$data) {
								if ( $date == $stamp) {
									$gotDate = true;
									foreach($data as $oneSet) {
										if ( $oneSet->quizId == $quizId &&
											 AH\stringArrayIsSame($oneset->tagList, $myTagList)) {
											$gotOne = true;
											$oneSet->count++;
											$Listings->log("updateUserVisitations - user:$userId, already have for $ip, date:$stamp, tagList:$myTagList");
											break;
										}
										unset($tagList);
									}
								}
								unset($date, $data);
							}

							if (!$gotDate) {
								$theDates[] = [$stamp=>['tagList'=>$myTagList,
														'quizId' =>$quizId,
														'count'=>1]];
								$Listings->log("updateUserVisitations - user:$userId, new date added for $ip, date:$stamp, tagList:$myTagList");
								$gotOne = true;
								$needUpdate = true;
							}
							elseif (!$gotOne) { // then got date, but not this tagList
								$theDates[$stamp] = (array)$theDates[$stamp];
								$theDates[$stamp][] = ['tagList'=>$myTagList,
														'quizId' =>$quizId,
														'count'=>1];
								$Listings->log("updateUserVisitations - user:$userId, adding new tags for $ip, date:$stamp, tagList:$myTagList");
								$gotOne = true;
								$needUpdate = true;
							}
						}
						unset($theDates);
					}
					if (!$gotOne) { // then same user, but different IP
						$ips = (array)$ips;
						$ips[$ip] = [$stamp=>[['tagList'=>$myTagList,
												'quizId' =>$quizId,
												'count'=>1]]];
						$ips = (object)$ips;
						$Listings->log("updateUserVisitations - user:$userId, adding new $ip, date:$stamp, tagList:$myTagList");
						$gotOne = true;
						$needUpdate = true;
					}
					break;
				}
				unset($ips);
			}

		if (!$gotOne) { // fresh visit
			$users = (array)$visits->users;
			$users[$userId] = (object)[$ip=>[$stamp=>[['tagList'=>$myTagList,
														'quizId' =>$quizId,
														'count'=>1]]]];
			$visits->users = (object)$users;
			$Listings->log("updateUserVisitations - new user:$userId, adding new $ip, date:$stamp, tagList:$myTagList");
			$needUpdate = true;
		}
		return $needUpdate;
	}

	function portVisitations(&$meta, &$visits, $userId, $portalUserID) {
		global $Listings;
		global $timezone_adjust;

		$gotOne = false;
		$needUpdate = false;
		$stamp = date("Y-m-d", time() + ($timezone_adjust*3600));
		$Listings->log("portVisitations - entered, user:$userId for portalUserID:$portalUserID");
		// $visit == array of user IDs.  Each of those are arrays of IPs, Each IP is an array of dates
		if (count($visits->users)) {
			foreach($visits->users as $id=>&$ips) {
				$id = intval($id);
				if ($id == $userId) {
					// move these over to $visits->portalUsers
					foreach($ips as $addr=>$theDates) {
						foreach($theDates as $date=>$data) {
							// $this->log("portVisitations - for user:$userId to portalUser:$portalUserID, ip:$addr on $date", 2);
							updatePortalUserVisitations($meta, $visits, $portalUserID, $addr, 0, $date, $data);
							// $this->getClass('PortalUsersMgr')->updatePageVisitations($portalUserID, 'home', $addr, $date);
							unset($date);
						}
						unset($theDates);
					}
					$needUpdate = true;
					if (gettype($visits->users) == 'array')
						unset($visits->users[$id]);
					else
						unset($visits->users->$id);
					break;
				}
				unset($ips);
			}
		}
		return $needUpdate;
	}

	function updatePortalUserVisitations(&$meta, &$visits, $userId, $ip, $quizId = 0, $fixedStamp = null, $fixedTagList = null) {
		global $Listings;
		global $myTagList;
		global $timezone_adjust;

		$gotOne = false;
		$gotDate = false;
		$needUpdate = false;
		$stamp = !$fixedStamp ? date("Y-m-d", time() + ($timezone_adjust*3600)) : $fixedStamp;
		$checkTagList = !$fixedTagList ? $myTagList : $fixedTagList;
		$Listings->log("updateUserVisitations - entered, user:$userId for $ip");
		// $visit == array of user IDs.  Each of those are arrays of IPs, Each IP is an array of dates
		if (count($visits->portalUsers)) 
			foreach($visits->portalUsers as $id=>&$ips) {
				$id = intval($id);
				if ($id == $userId) {
					foreach($ips as $addr=>&$theDates) {
						$theDates = (array)$theDates;
						if ($addr == $ip) {						
							foreach($theDates as $date=>&$data) {
								if ( $date == $stamp) {
									$gotDate = true;
									foreach($data as $i=>&$oneSet) {
										if ( gettype($oneSet) == 'string') {
											if ( AH\stringArrayIsSame($oneSet, $checkTagList) ) {
												$data[$i] = ['tags'=>$checkTagList,
															 'quizId'=>$quizId,
															 'count'=>2];
												$gotOne = true;
												$needUpdate = true;
												break;
											}
										}
										else {
											$oneSet = (object)$oneSet;
											if ( AH\stringArrayIsSame($oneSet->tags, $checkTagList) &&
											 	$oneSet->quizId == $quizId) {
												$gotOne = true;
												$needUpdate = true;
												$oneSet->count++;
												$Listings->log("updatePortalUserVisitations - user:$userId, already have for $ip, date:$stamp, tagList:$myTagList");
												break;
											}
										}
										unset($oneSet);
									}
								}
								unset($date, $data);
							}

							if (!$gotDate) {
								$theDates[] = [$stamp=>['tags'=>$checkTagList,
														'quizId'=>$quizId,
														'count'=>1]];
								$Listings->log("updatePortalUserVisitations - user:$userId, new date added for $ip, date:$stamp, tagList:$myTagList");
								$gotOne = true;
								$needUpdate = true;
							}
							elseif (!$gotOne) { // then got date, but not this tagList
								$theDates[$stamp] = (array)$theDates[$stamp];
								$theDates[$stamp][] = ['tags'=>$checkTagList,
														'quizId'=>$quizId,
														'count'=>1];
								$theDates[$stamp] = (object)$theDates[$stamp];
								$Listings->log("updatePortalUserVisitations - user:$userId, adding new tags for $ip, date:$stamp, tagList:$myTagList");
								$gotOne = true;
								$needUpdate = true;
							}
						}
						unset($theDates);
					}
					if (!$gotOne) { // then same user, but different IP
						$ips = (array)$ips;
						$ips[$ip] = [$stamp=>[['tags'=>$checkTagList,
														'quizId'=>$quizId,
														'count'=>1]]];
						$ips = (object)$ips;
						$Listings->log("updatePortalUserVisitations - user:$userId, adding new $ip, date:$stamp, tagList:$myTagList");
						$gotOne = true;
						$needUpdate = true;
					}
					break;
				}
				unset($ips);
			}

		if (!$gotOne) { // fresh visit
			$users = (array)$visits->portalUsers;
			$users[$userId] = (object)[$ip=>[$stamp=>[['tags'=>$checkTagList,
														'quizId'=>$quizId,
														'count'=>1]]]];
			$visits->portalUsers = (object)$users;
			$Listings->log("updatePortalUserVisitations - new user:$userId, adding new $ip, date:$stamp, tagList:$myTagList");
			$needUpdate = true;
		}
		return $needUpdate;
	}

	function trackViewer($listing, $userId, $portalUserID, $quizId) {
		global $Listings;
		global $PortalUsersMgr;
		global $myTagList;
		global $ListingsViewed;
		global $timezone_adjust;

		$myTagList = isset($_COOKIE['TagsForListing']) && !empty($_COOKIE['TagsForListing']) ? $_COOKIE['TagsForListing'] : '0';
		$Listings->log("trackViewer - entered, listing:$listing->id, user:$userId, portalUserID:$portalUserID, quizId:".(!empty($quizId) ? $quizId : "N/A"));
		if (!empty($listing)) {
			// update the seller meta data with userId and IP and date
			$ip = userIP();
			$meta = array();
			$visits = null;
			if (!empty($listing->meta)) foreach($listing->meta as $data) {
				if ($data->action != LISTING_VISITATIONS)
					$meta[] = $data;
				else {
					$visits = $data;
					// $visits->users = json_decode($visits->users, true);
				}
			}
			$newVisit = false;
			if (!$visits) {
				$visits = new \stdClass();
				$visits->action = LISTING_VISITATIONS;
				$visits->users = array();
				$visits->portalUsers = array();
				$newVisit = true;
			}
			elseif (!isset($visits->portalUsers))
				$visits->portalUsers = [];

			if ($userId) {
				if ($portalUserID) {
					portVisitations($meta, $visits, $userId, $portalUserID);
					$needUpdate = updatePortalUserVisitations($meta, $visits, $portalUserID, $ip, $quizId);
				}
				else
					$needUpdate = updateUserVisitations($meta, $visits, $userId, $ip, $quizId);
			}
			elseif ($portalUserID)
				$needUpdate = updatePortalUserVisitations($meta, $visits, $portalUserID, $ip, $quizId);
			else
				$needUpdate = updateUserVisitations($meta, $visits, $userId, $ip, $quizId);
			
			if ($needUpdate) {
				// $visits->users = json_encode($visits->users);
				$meta[] = $visits;
				$Listings->set([(object)['fields'=>['meta'=>$meta],
										'where'=>['id'=>$listing->id]]]);
			}

			// update ListingsViewed
			$listingsViewed = $ListingsViewed->get((object)['where'=>['listing_id'=>$listing->id]]);
			$thisViewer = new \stdClass();
			$thisViewer->userId = $userId;
			$thisViewer->portalUserID = $portalUserID;
			$thisViewer->ip = $ip;
			$thisViewer->date = date("Y-m-d H:i:s", time() + ($timezone_adjust*3600));

			if (empty($listingsViewed)) {
				$viewer = new \stdClass();
				$viewer->action = LISTINGS_VIEWER_LIST;
				$viewer->list = [];
				$viewer->list[] = $thisViewer;
				$ListingsViewed->add((object)['listing_id'=>$listing->id,
											  'clicks'=>1,
											  'meta'=>[$viewer]]);
			}
			else {
				$viewer = array_pop($listingsViewed);
				$viewers = null;
				$metas = [];
				foreach($viewer->meta as $meta) {
					if ($meta->action == LISTINGS_VIEWER_LIST)
						$viewers = $meta;
					else
						$metas[] = $meta;
				}
				if (!$viewers) {
					$viewers = new \stdClass();
					$viewers->action = LISTINGS_VIEWER_LIST;
					$viewers->list = [];
				}
				$viewers->list[] = $thisViewer;
				$metas[] = $viewers;
				$ListingsViewed->set([(object)['where'=>['id'=>$viewer->id],
											   'fields'=>['meta'=>$metas,
											   			  'clicks'=>($viewer->clicks + 1)]]]);
			}
		}		
	}

	$isAdmin = 0;
	$track = 0;
	$trackId = 0;
	$tag = 0;
	$sampleView = false;
	$newTab = false;
	$isCityGeo = false;
	$fromQuiz = false;

	// $browser = AH\getBrowser();
	global $browser;

	require_once(__DIR__.'./../_classes/SessionMgr.class.php'); $SessionMgr = new AH\SessionMgr();
	$session_id = 0;
	$session = null;
	$sessionID = $SessionMgr->getCurrentSession($session_id, $session);
	$portalUser = 0;
	$agentID = 0;
	global $ALR; 
	$ALR->getSessionData($session, $portalUser, $agentID);
	$agentViewPortalUserTraffic = false;

	if (strpos($listing_id, "-") != false) {
		$parts = explode("-", $listing_id);
		$listing_id = $parts[1];
		
		if (strpos($parts[0], "S") !== false) { // sample view
			$tag = substr($parts[0], 1);
			$sampleView = true;
			$newTab = true;
		}
		elseif (strpos($parts[0], "N") !== false) { // new tab view
			$newTab = true;
		}
		elseif (strpos($parts[0], "Q") !== false) { // new tab view
			$fromQuiz = true;
		}
		elseif (strpos($parts[0], "A") !== false) { // new tab view
			$agentViewPortalUserTraffic = true;
		}
		elseif (strpos($parts[0], "T") !== false) {
			$trackId = substr($parts[0], 1);
			require_once(__DIR__.'/../_classes/EmailTracker.class.php'); $et = new AH\EmailTracker();
			$track = $et->get((object)['where'=>['id'=>$trackId]]);
			if (!empty($track)) {
				$track = array_pop($track);
			}
		}
		elseif (strpos($parts[0][0], "L") !== false) { // from social media sharing link
			$referer = isset($_SERVER["HTTP_REFERER"]) ? (!empty($_SERVER["HTTP_REFERER"]) ? strtolower($_SERVER["HTTP_REFERER"]) : "") : '';
			$date = date("Y-m-d H:i:s", time() + ($timezone_adjust*3600));
			global $browser;
			require_once(__DIR__.'./../_classes/Analytics.class.php'); $Analytics = new AH\Analytics();
			$from = '';
			$ch = $parts[0][1];
			if (strlen($parts[0]) > 1) switch($ch) {
				case 'F': $from = "Facebook"; break;
				case 'I': $from = "LinkedIn"; break;
				case 'G': $from = "Google+"; break;
				case 'T': $from = "Twitter"; break;
				case 'M': $from = "Email"; break;
				case 'C': $from = "Contact"; break;
			}
			$Analytics->add(['type'=>ANALYTICS_TYPE_EVENT,
							 'origin'=>'listing',
							 'session_id'=>$session_id,
							 'referer'=>$referer,
							 'what'=>'sharing',
							 'value_str'=>$from,
							 'value_int'=>$listing_id,
							 'added'=>$date,
							 'browser'=>$browser['shortname'],
							 'connection'=>$browser,
							 'ip'=>userIP()]);
		}
		elseif ($parts[0][0] == '1') // admin view
			$isAdmin = intval($parts[0]);
	}

	$totalImageCount = 0;
	$userId = wp_get_current_user()->ID;
	$x = $Listings->get((object)[ 'where'=>[ 'id'=>$listing_id ] ]);
	$first_image = '';
	$portalDirect = $havePortal && $userId && !in_array($userId, $specialAgents) ? "P-$userId" : '';
	if (empty($x)) :
		?>
		<h2>Oops!  This listing is no longer in our database.<br/><br/>Click <a href="<?php echo get_home_url(); ?>/quiz/#sq=0">here</a> to search for cool homes!</h2>
		<?php
	else :
		$x = array_pop( $x );
		$baths = floor($x->baths);
		$baths += floor( (fmod(floatval($x->baths), 1)*1000)+0.1 ) > 0 ? 0.5 : 0;
		if (!$agentViewPortalUserTraffic)
			trackViewer($x, $userId, $portalUser, $extra2);

		$Listings->log("begin image processing for listing:$listing_id");
		getCitiesTags($x);

		if (!empty($x->images)) foreach($x->images as $i=>&$img) {
			$img = (object)$img;
			$url = '';
			if (isset($img->discard)) 
				if (!isset($img->file))
					$img->file = $img->discard;
			if (isset($img->file)) { $img->file = AH\removeslashes($img->file); $url = $img->file; }
			if (isset($img->url)) $img->url = AH\removeslashes($img->url); 

			if ( empty($url) )
				$Listings->log("image:$i - file:".(!empty($url) ? $url : "empty").", url:".(isset($img->url) ? $img->url : "n/a"));
			if ( (empty($url) ||
				  (substr($url, 0, 4 ) != 'http' &&
				   (!file_exists(__DIR__.'/../_img/_listings/uploaded/'.$url) ||
				    !file_exists(__DIR__.'/../_img/_listings/900x500/'.$url)))) ) {
				if (isset($img->url)) 
					$img->file = $img->url;
				else {
					unset($x->images[$i]);
					unset($img);
					continue;
				}
			}

			$totalImageCount++;

			if (empty($first_image) &&
				isset($img->file)) {
				if (substr($img->file, 0, 4 ) != 'http') {
					if (file_exists(__DIR__.'/../_img/_listings/900x500/'.$img->file))
						$first_image = get_template_directory_uri().'/_img/_listings/900x500/'.$img->file;
				}
				else
					$first_image = $img->file;
			}
			unset($img);
		}
		$x->about = AH\removeslashes($x->about);

		$sql = "UPDATE ".$Listings->getTableName($Listings->table)." SET `clicks` = `clicks` + 1 WHERE `id` = ".$x->id;
		$Listings->rawQuery($sql);

		require_once(__DIR__.'/../_classes/Cities.class.php'); $Cities = new AH\Cities();
		$city_id = $x->city_id;
		if (empty($city_id) ||
			intval($city_id) == -1) {
			$city_id = $Cities->get((object)[ 'what' => ['id','lng','lat'], 'where'=>[ 'city'=>$x->city,
																			'state'=>$x->state ] ]);
			if (!empty($city_id))
				$x->city_id = $city_id = $city_id[0]->id;
		}
		if (!empty($x->city_id)) {
			$sql = "UPDATE ".$Cities->getTableName($Cities->table)." SET `clicks` = `clicks` + 1 WHERE `id` = ".$x->city_id;
			$Cities->rawQuery($sql);
		}

		require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new AH\Sellers(1);
		require_once(__DIR__.'/../_classes/ListingsTags.class.php'); $ListingsTags = new AH\ListingsTags();
		require_once(__DIR__.'/../_classes/Tags.class.php'); $Tags = new AH\Tags();

		$ListingsTags = $ListingsTags->getGroup((object)[ 'field'=>'tag_id', 'where'=>[ 'listing_id'=>$x->id ] ]);
		// add incoming sample tag id into the mix if it's not in there already
		if ($tag &&
			!in_array($tag, $ListingsTags))
			$ListingsTags[] = $tag;

		$x->tags = $Tags->get((object)[ 'what' => ['id','tag','type'], 'where'=>[ 'id'=>$ListingsTags ] ]);
		$gotCityTags = false;
		$tagList = [];
		if (!empty($x->tags)) foreach($x->tags as $tag) {
			$tagList[$tag->id] = $tag;
			// if ($tag->type == 1) {
			// 	$gotCityTags = true;
			// }
		}
		unset($x->tags);
		if (isset($x->cityTags) &&
			count($x->cityTags)) {
			$gotCityTags = true;
			foreach($x->cityTags as $tag)
				if (!array_key_exists($tag->id , $tagList) )
					$tagList[$tag->id] = $tag;

			unset($x->cityTags);
		}
		$x->tags = $tagList;

		if (!$gotCityTags &&
			!empty($x->city_id)) {
			require_once(__DIR__.'/../_classes/CitiesTags.class.php'); $CitiesTags = new AH\CitiesTags();
			$city_tags = $CitiesTags->get((object)[ 'what'=>['tag_id','score'], 'where' => ['city_id' => $city_id] ]);
			if (!empty($city_tags)) {
				$goodTags = [];
				foreach($city_tags as $tag) {
					if (gettype($tag->score) == 'string')
						$tag->score = intval($tag->score);
					if ($tag->score >= 7 || $tag->score == -1)
						$goodTags[] = $tag->tag_id;
				}
				if (!empty($goodTags)) {
					$city_tags = $Tags->get((object)[ 'what' => ['id','tag','type'], 'where'=>[ 'id'=>$goodTags ] ]);
					if (!empty($city_tags))
						if (!empty($x->tags))
							$x->tags = array_merge($x->tags, $city_tags);
						else
							$x->tags = $city_tags;
				}
			}
		}
		unset($Tags, $ListingsTags);

		if ($x->author_has_account) $query = (object) [ 'where'=>[ 'author_id'=>$x->author ] ];
		else $query = (object) [ 'where'=>[ 'id'=>$x->author ] ];
		$agent = $Sellers->get($query);
		if (!empty($agent)) {
			$agent[0]->phone = AH\fixPhone($agent[0]->phone);
			$agent = array_pop($agent);
			$info = !empty($agent->author_id) ? get_userdata($agent->author_id) : null;
			$nickname = !empty($info) && !empty($info->nickname) ? $info->nickname : (!empty($info) && !empty($info->user_nicename) ? $info->user_nicename : 'unknown');
			$agent->nickname = stripos($nickname, 'unknown') === false ? $nickname : '';
			$agentPage = get_home_url().'/agent/'.(!empty($agent->nickname) ? $agent->nickname : $agent->author_id);
			// $Sellers->log("listing agent into:".print_r($agent, true));
		}

		$images = [];
		if (!empty($x->images)) foreach ($x->images as &$image) {
			if (isset($image->discard)) 
				if (!isset($image->file))
					$image->file = $image->discard;
			if (isset($image->title)) unset($image->title);
			if (isset($image->desc) && $image->desc == 'blank') unset($image->desc);
			$images[] = $image;
		}

		$videoStr = null;
		if (!empty($x->video)) {
			if (gettype($x->video) == 'array')
				$videoStr = isset($x->video[0]->file) && !empty($x->video[0]->file) ? $x->video[0]->file : null;
			elseif (gettype($x->video) == 'object')
				$videoStr = isset($x->video->file) && !empty($x->video->file) ? $x->video->file : null;
			elseif (gettype($x->video) == 'string') {
				$videoStr = $x->video;
				if (strpos($videoStr, "ttp:") !== false) // no url supported directly
					$videoStr = null;
			}
		}

		if (!empty($videoStr))
		 	if ( ($pos = strpos($videoStr, "watch?v=")) !== false )
		 		$videoStr = substr($videoStr, $pos);
		 	elseif ( ($pos = strpos($videoStr, "v=")) !== false )
		 		$videoStr = "watch?".substr($videoStr, $pos);
		 	else
		 		$videoStr = "watch?v=".$videoStr;

		$video = !empty($videoStr) && AH\validYouTubeURL("www.youtube.com/".$videoStr) ? $videoStr : null;
		$youtubeVideoStr = $video ? "https://www.youtube.com/embed/".substr($videoStr, strpos($videoStr, '=')+1 )."?autoplay=1" : '';
		$Listings->log("listing:$listing_id has videoStr:$videoStr, video:$video, x:".(!empty($x->video) ? print_r($x->video, true) : "N/A"));

		$geoInfo = $ListingsGeoinfo->get((object)['where'=>['listing_id'=>$listing_id]]);
		if (empty($geoInfo) ||
			$geoInfo[0]->lng == -1) {
			if ($x->active > AH_ACTIVE) {
				require_once(__DIR__.'/../_classes/GoogleLocation.class.php'); $GoogleLocation = new AH\GoogleLocation();
				$google = null;
				$hadEmptyGeoCode = !empty($geoInfo);
				$geoInfo = null;
				if ($Listings->geocodeListing($GoogleLocation, $x, $google, $Cities)) {
					$geoInfo = (object)['lng'=>$x->result->query->lng,
										'lat'=>$x->result->query->lat];
					$Listings->log("For $listing_id, with active:$x->active, found lng:".$x->result->query->lng.", lat:".$x->result->query->lat);
					if ($hadEmptyGeoCode)
						$ListingsGeoinfo->set([(object)['where'=>['listing_id'=>$listing_id],
														'fields'=>['lng'=>$x->result->query->lng,
																   'lat'=>$x->result->query->lat,
													   			   'address'=>$x->result->query->address]]]);
					else
						$ListingsGeoinfo->add((object)['listing_id'=>$listing_id,
													   'lng'=>$x->result->query->lng,
													   'lat'=>$x->result->query->lat,
													   'address'=>$x->result->query->address]);
				}

			}
			if ( !$geoInfo &&
				 !empty($x->city_id) ) {
				$geoInfo = $Cities->get((object)[ 'what' => ['id','lng','lat'], 'where'=>[ 'id' => $x->city_id ]]);
				$isCityGeo = !empty($geoInfo) && $geoInfo[0]->lng != -1;
				$geoInfo = !empty($geoInfo) ? array_pop($geoInfo) : null;
			}
		}
		else
			$geoInfo = array_pop($geoInfo);

		$Listings->log("geocode for $listing_id:".(!empty($geoInfo) ? print_r($geoInfo, true) : "N/A").", portalUser:$portalUser" );
		if (!$portalUser &&
			$agentID &&
			$userId) {
			$portalUser = $PortalUsersMgr->addPortalUserFromWP($session_id, $userId, 'listing', $agentID);
			$Listings->log("called addPortalUserFromWP for user:$userId, and got portalUser with id:".$portalUser);
		}

		if ($portalUser) {
			$out = $PortalUsersMgr->recordPortalUserListing($portalUser, intval($listing_id));
			$Listings->log("recordPortalUserListing returned:".print_r($out, true));
		}
		
		$listing = (object)[
			'id' => $listing_id,
			'street_address' => $x->street_address,
			'city_id' => $x->city_id,
			'city' => $x->city,
			'state' => $x->state,
			'country' => $x->country,
			'price' => $x->price,
			'beds' => $x->beds,
			'baths' => $x->baths,
			'lotsize_std' => $x->lotsize_std,
			'lotsize' => $x->lotsize,
			'interior_std' => $x->interior_std,
			'interior' => $x->interior,
			'tags'=> $x->tags,
			'flags'=> $x->flags,
			'error' => $x->error,
			'author_has_account' => $x->author_has_account,
			'author' => $x->author,
			'listhub_key' => $x->listhub_key,
			'active' => $x->active,
			'url' => $x->url,
			'disclaimer' => $x->disclaimer,
			'video' => $video,
			'title' => $x->title,
			'lng' => !empty($geoInfo) ? $geoInfo->lng : -1,
			'lat' => !empty($geoInfo) ? $geoInfo->lat : -1,
			'listing_status' => !empty($x->listing_status) ? $x->listing_status : 'Active'
		];

		// $listingAgentDetail = '<h4>Listing Agent Detail</h4>';
		// if (!empty($agent)) {
		// 	$listingAgentDetail .= '<div id="agent-detail">';
		// 	if (!empty($agent->phone))
		// 		$listingAgentDetail .= '<span class="phone">'.$agent->phone.'</span><a href="tel:'.$agent->phone.'" class="phone mobile">'.$agent->phone.'</a>';

		// 	if (!empty($agent->first_name) ||
		// 	 	!empty($agent->last_name)) {
		// 		$listingAgentDetail .= !empty($agent->phone) ? '<span class="desktopspace">&nbsp;&nbsp;</span><span class="mobilespace"><br/></span>' : '';
		// 		$listingAgentDetail .= '<a href="'.$x->url.'" target="_blank">'."$agent->first_name $agent->last_name".'</a>';
		// 	}
		// 	$listingAgentDetail .= '<br/><br/></div>';
		// }
		// $listingAgentDetail .= !empty($x->disclaimer) ? $x->disclaimer : '';

		// portal agent
		$isPortal = true;
		$portalAgent = $ALR->get('portal-agent');
		if ($portalAgent == "0")
			$isPortal = false;

		$premierAgents = $Sellers->premierAgents($x->tags, $x->city_id, $portalAgent, $x->id, $sampleView, $userId, intval($extra2) );
		$Listings->log("listing page for listing:$listing->id - premierAgents:".(!empty($premierAgents) ? print_r($premierAgents, true) : "none"));
		if (!empty($premierAgents)) {
			$Listings->log("listing page found ".count($premierAgents)." premierAgents");
			foreach($premierAgents as &$pagent) {
				$tags = [];
				foreach($pagent->tags as $tag)
					$tags[] = $tag;
				unset($pagent->tags);
				$pagent->tags = $tags;
				$Listings->log("Agent:$pagent->id, tags:".print_r($pagent->tags, true));
			}
		}

		if (empty($x->about)) {
                        $x->about = "Listing was not supplied with any descriptive text.  Please contact ";
			if ($isPortal)
				$x->about .= "the portal agent, $portalAgent->first_name $portalAgent->last_name, for assistance.";
			elseif (!empty($premierAgents))
				$x->about .= "our LifeStyled Agent".(count($premierAgents) > 1 ? 's' : '')." for assistance.";
			else
				$x->about .= "the listing agent for additional information.";
		}

		// }
		// else
		// 	$premierAgent = [$premierAgent];

		if (!empty($track)) {
			$today = date('Y-m-d H:i:s', time() - (7*3600));
			$track->listhub_key = $listing->listhub_key;
			$meta = new \stdClass();
			$meta->action = ET_LISTING_VIEWED;
			$meta->date = $today;
			$meta->listing = $listing_id;
			if ( !isset($track->meta) || 
				 empty($track->meta) )
				$track->meta = [$meta]; 
			else
				$track->meta[] = $meta;
			$et->set([(object)['where'=>['id'=>$track->id],
							   'fields'=>['meta'=>$track->meta]]]);
			require_once(__DIR__.'/../_classes/SellersEmailDb.class.php'); $SellersEmailDb = new AH\SellersEmailDb();
			if ( $SellersEmailDb->exists((object)['seller_id'=>$track->agent_id])) {
				$sellerEmailDb = $SellersEmailDb->get((object)['where'=>['seller_id'=>$track->agent_id]]);
				if (!empty($sellerEmailDb)) {
					$fields = [];
					if ( ($sellerEmailDb[0]->flags & RESPONDED_TO_VIEW_LISTING) == 0) {
						$sellerEmailDb[0]->flags |= RESPONDED_TO_VIEW_LISTING;
						$fields['flags'] = $sellerEmailDb[0]->flags;
					}

					$respondMeta = new \stdClass();
					$respondMeta->action = VIEWED_LISTING;
					$respondMeta->date = $today;
					$respondMeta->emailTrackerId = $track->id;
					$respondMeta->listing = $listing_id;
					if (empty($sellerEmailDb[0]->meta))
						$sellerEmailDb[0]->meta = [$respondMeta];
					else {
						$sellerEmailDb[0]->meta = (array)$sellerEmailDb[0]->meta; // force to array
						$sellerEmailDb[0]->meta[] = $respondMeta;
					}
					$fields['meta'] = $sellerEmailDb[0]->meta;

					$SellersEmailDb->set([(object)['where'=>['id'=>$sellerEmailDb[0]->id],
												   'fields'=>$fields]]);
					unset($fields, $sellerEmailDb);
				}
			}
			require_once(__DIR__.'/../_classes/Email.class.php'); $Email = new AH\Email();
			$msg = "Seller - id:$agent->id, $agent->first_name $agent->last_name, looked at their listing:$listing_id";
			$Email->sendMail(SUPPORT_EMAIL, 'Agent viewed listing', $msg,
							'Tracking views',
							"Seller - id:$agent->id viewed listing:$listing_id");
		}

		$haveAgentMatchLogo = file_exists(__DIR__.'/../_img/_sellers/agent-match-logo.png');

		$emailBody = 'Go check out this listing at Lifestyled Listings!  Copy and paste into browser: '.get_home_url().'/listing/LM-'.$listing_id.(!empty($portalDirect) ? '/'.$portalDirect : '');
		$emailSubject = ('Check out this listing at '.$listing->street_address);
		$emailHref = "mailto:?subject=$emailSubject&body=$emailBody";

		$Listings->log("listing page - about to create xml for listing:$listing->id");
?>
<script type="text/javascript">
	var gallery_images = <?php echo json_encode($images); ?>;
	var listing_info = <?php echo json_encode($listing); ?>;
	var seller = <?php echo json_encode($agent); ?>;
	var listingAgent = <?php echo json_encode($agent); ?>;
 	var premierAgents = <?php echo $premierAgents != "0" ? json_encode($premierAgents) : "0"; ?>;
	var portalAgent = <?php echo $portalAgent != "0" ? json_encode($portalAgent) : "0"; ?>;
	var isPortal = <?php echo $isPortal ? 1 : 0; ?>;
	var emailTracker = <?php echo empty($track) ? 0 : json_encode($track); ?>;
	var etId = <?php echo $trackId; ?>;
	var haveAgentMatchLogo = <?php echo $haveAgentMatchLogo ? 1 : 0; ?>;
	var userID = '<?php echo $userId; ?>';
	var isCityGeo = <?php echo $isCityGeo ? 1 : 0; ?>;
	var command = '<?php echo !isset($extra) || empty($extra) ? "none" : $extra; ?>';
		
	jQuery(document).ready(function($){
		
		// ---- Animated Loadins ---- //
		(function($) {
		$.fn.visible = function(partial) {
				var $t            = $(this),
						$w            = $(window),
						viewTop       = $w.scrollTop(),
						viewBottom    = viewTop + $w.height(),
						_top          = $t.offset().top,
						_bottom       = _top + $t.height(),
						compareTop    = partial === true ? _bottom : _top,
						compareBottom = partial === true ? _top : _bottom;

			return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
		}; 

		loadListHub();

		window.setTimeout( function() {
			if (ah_local.ip != "1" &&
				ah_local.ip != "2130706433") {// 127.0.0.1
				if (typeof lh == 'function')
					lh('submit', 'DETAIL_PAGE_VIEWED', {lkey:'<?php echo $x->listhub_key; ?>'});
				else
					console.log("lh not callable");
			}
		}, 1000);
		


	})(jQuery);
	var win = $(window);
	
	var allMods = $('.listing .listing-info .premier-agents #banner');
	allMods.each(function(i, el) {
		var el = $('.listing .listing-info .premier-agents #banner');
		if (el.visible(true)) {
			$('.premier-agents #banner .imageDiv .image1 ,.premier-agents #banner .imageDiv .image2 ,.premier-agents #banner .imageDiv .image3 ,.premier-agents #banner .imageDiv .image4').addClass('animate');
			$('.premier-agents #banner .logoDiv').addClass('animate');
		} 
	});
	win.scroll(function(event) {
		allMods.each(function(i, el) {
			var el = $('.listing .listing-info .premier-agents #banner');
			if (el.visible(true)) {
				$('.premier-agents #banner .imageDiv .image1 ,.premier-agents #banner .imageDiv .image2 ,.premier-agents #banner .imageDiv .image3 ,.premier-agents #banner .imageDiv .image4').addClass('animate');
				$('.premier-agents #banner .logoDiv').addClass('animate');
			} 
		});
	});
		
		// ---- Click Functions ---- //
		$('.mobile-listing-meta .seemore').on('click', function() {
			$('.mobile-listing-meta .abouttext').show();
			$('.mobile-listing-meta .abouttexthidden').hide();
			$('.mobile-listing-meta .seemore').hide();
			$('.mobile-listing-meta .seeless').show();
		});
		$('.mobile-listing-meta .seeless').on('click', function() {
			$('.mobile-listing-meta .abouttext').hide();
			$('.mobile-listing-meta .abouttexthidden').show();
			$('.mobile-listing-meta .seeless').hide();
			$('.mobile-listing-meta .seemore').show();
		});
		
		if (history.length == 1) {
			$newTab = true;
			$('.main-container .main .listing a.exit-page').attr('href', 'javascript:window.close();');
			$('.main-container .main .listing a.exit-page').html('<p><span class="entypo-left-open-big"></span> &nbsp;Close Window</p>');
			$('body.listing .mobile-back-button').hide();
		}
	});
</script>

<div id="agent-verify">
</div>

<div id="overlay-bg" style="display: none;">
	<div class="spin-wrap"><div class="spinner sphere"></div></div>
</div>

<div class="listing">
	<div class="videoDiv" id="listing">
		<button class="closeVid"><span class="line1"></span><span class="line2"></span></button>
		<span class="video">
			<iframe src="<?php echo $youtubeVideoStr; ?>" width="100%" height="100%" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</span>
	</div>
	<?php if (!$newTab && $isAdmin == 0 && empty($track) ): ?>
	<a href="javascript:window.history.back();" class="exit-page">
		<span class="desktop"><span class="entypo-left-open-big" style="font-size:.6em;left:102px;position:absolute;"></span><p>Return to <?php echo $fromQuiz ? 'Quiz' : 'Results'; ?></p></span>
		<span class="mobile"><p><span class="entypo-left-open-big" style="font-size:.6em;left:102px;position:absolute;"></span> Return to <?php echo $fromQuiz ? 'Quiz' : 'Results'; ?></p></span>
	</a>
	<?php else: ?>
		<?php if ($isAdmin) : ?>
			<a href="javascript:window.history.go(-2);" class="exit-page"><p><span class="entypo-left-open-big"></span> &nbsp;Close Window</p></a>
		<?php else: ?>
			<a href="javascript:window.close();" class="exit-page"><p><span class="entypo-left-open-big"></span> &nbsp;Close Window</p></a>
		<?php endif; ?>
	<?php endif; ?>
	<div class="mobile-listing-title">
		<?php
			$listingHTML = '<div class="mobileinfo">';
			if ($isAdmin ||
				$x->flags & LISTING_PERMIT_ADDRESS )
				$listingHTML .= '<div class="street_address notranslate" colspan="2">'.$x->street_address.'</div><div class="slash">/</div>';
			else
				$listingHTML .= '<div class="street_address" colspan="2">'.'Address hidden'.'</div><div class="slash">/</div>';

			$listingHTML .= '<div class="city notranslate">';
				if ($x->city != '' && $x->city != -1)
					$listingHTML .= $x->city.', ';
				if ($x->state != '' && $x->state != -1)
					$listingHTML .= $x->state;
			$listingHTML .= '</div>';
			$listingHTML .= '</div>';
			echo $listingHTML;
		?>
	</div>
	<section>
		<a class="gallery-full"><span class="subtitle">See all <?php echo $totalImageCount; ?> images</span> &nbsp;<span class="entypo-right-open-big"></span></a>
		<div class="open-gallery"></div>
		<div class="bxslider-nav">
            <span class="nav left"></span>
            <span class="nav right"></span>
        </div>
		<ul class="listing-gallery" style="opacity: 0;">
			<?php $count = 0;
				if ($x->images) foreach ($x->images as $i=>$img){
				if ($count >= 6) break;
				if (isset($img->file) && substr($img->file, 0, 4 ) == 'http') {
				?><li class="listing-image"><img src="<?php echo $img->file; ?>" /></li><?php
					// if ($count == 0)
					// 	$first = $img->file;
					$count++;
				}
				elseif (isset($img->discard))
					continue;
				else {
				?><li class="listing-image"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/_listings/900x500/<?php echo $img->file; ?>" /></li><?php
				// if ($count == 0)
				// 	$first = get_bloginfo('stylesheet_directory','raw').'/_img/_listings/900x500/'.$img->file;
				$count++;
				}
			} ?>
		</ul>
		<div class="listing-meta" >
			<?php
			$listingHTML = '<table>';
			//$listingHTML .= '<tr><td class="title" colspan="2">'.strlen($x->title) > 40 ? substr($x->title, 0, 40) : $x->title.'</td></tr>';
			if ($isAdmin ||
				$x->flags & LISTING_PERMIT_ADDRESS )
				$listingHTML .= '<tr><td class="street_address notranslate">'.$x->street_address.'</td></tr>';
			else
				$listingHTML .= '<tr><td class="street_address">'.'Address hidden'.'</td></tr>';

			$listingHTML .= '<tr><td class="city-state notranslate">';
				if ($x->city != '' && $x->city != -1)
					$listingHTML .= $x->city.', ';
				if ($x->state != '' && $x->state != -1)
					$listingHTML .= $x->state;
				if (!empty($x->country) && $x->country != 'US') $listingHTML .= ' '.$x->country;
			$listingHTML .= '</td></tr>';

			$listingHTML .= '<tr><td id="price">$'.number_format($x->price).'</td></tr>';


			$listingHTML .= '<tr><td>'.$x->beds.' beds <span class="slash">/</span> ';
			$x->baths = explode('.', $x->baths);
			$listingHTML .= intval($x->baths[0]).'.'.(!empty($x->baths[1]) ? '5' : '0').' baths</td></tr>';

			if ($x->interior > 0){
				if ($x->interior_std == 'meters') $x->interior_std = 'm<sup>2</sup>';
				else if ($x->interior_std == 'acres') $x->interior_std = 'acres';
				else $x->interior_std = 'ft<sup>2</sup>';
				$listingHTML .= '<tr><td>'.number_format($x->interior).' '.$x->interior_std.' home</td></tr>';
			}

			if ($x->lotsize > 0){
				if ($x->lotsize_std == 'meters') $x->lotsize_std = 'm<sup>2</sup>';
				else if ($x->lotsize_std == 'acres') $x->lotsize_std = 'acres';
				else $x->lotsize_std = 'ft<sup>2</sup>';
				$listingHTML .= '<tr><td>'.number_format($x->lotsize).' '.$x->lotsize_std.' lot</td></tr>';
			}
			$listingHTML .= '<tr><td>'.$listing->listing_status.'</td></tr>';
			$listingHTML .= '</table>';
			echo $listingHTML;
			?>
			<div id="shareDiv">
				<span class="title">Share this listing</span>
				<div class="icons">
					<a id="facebook-share" href="javascript:shareSocialNetwork('FB',1,'https://www.facebook.com/sharer/sharer.php?u=<?php echo get_home_url().'/listing/LF-'.$listing_id.(!empty($portalDirect) ? '/'.$portalDirect : ''); ?>')" target="_blank"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/facebook.png'; ?>" /></a>
					<a id="linkedin-share" href="javascript:shareLinkedIn({	comment:'Hi friends check out this listing!', title:'Another listing from LifeStyled Listings', description:'<?php echo (!empty($listing->title) ? $listing->title : $listing->street_address.', '.$listing->city).(!empty($listing->price) ? ', $'.$listing->price : '').(!empty($listing->beds) ? ', Beds:'.$listing->beds : '').(!empty($baths) ? ", Baths:".number_format($baths, 1) : ''); ?>', url:'<?php echo get_home_url().'/listing/LI-'.$listing_id.(!empty($portalDirect) ? '/'.$portalDirect : ''); ?>', img:'<?php echo $first_image; ?>', where: 1});" target="_blank"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/linkedin.png'; ?>" /></a>
					<a id="googleplus-share" href="javascript:shareSocialNetwork('GP',1,'https://plus.google.com/share?url=<?php echo get_home_url().'/listing/LG-'.$listing_id.(!empty($portalDirect) ? '/'.$portalDirect : ''); ?>')" target="_blank"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/googleplus.png'; ?>" /></a>
					<a id="twitter-share" href="javascript:shareSocialNetwork('TW',1,'https://twitter.com/share?url=<?php echo get_home_url().'/listing/LT-'.$listing_id.(!empty($portalDirect) ? '/'.$portalDirect : ''); ?>')" target="_blank"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/twitter.png'; ?>" /></a>
					<!-- <a id="email-share" href="mailto:%20?subject=<?php echo $emailSubject; ?>&body=<?php echo $emailBody; ?>"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/email.png'; ?>" /></a> -->
					<a id="email-share" href="javascript:shareSocialNetwork('EM',1,'<?php echo $emailHref; ?>');"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/email.png'; ?>" /></a>
				</div>
				<a id="client-share" href="javascript:;">Share with client</a>
			</div>
			
		</div>
		<div class="mobile-listing-meta" >
			<?php
			$listingHTML = '<div class="mobileinfo">';
			$listingHTML .= '<div class="price">$'.number_format($x->price).'</div>';
			$listingHTML .= '<div class="bottominfo"><p>'.$x->beds.' bed</p><span class="slash">/</span>';
			$listingHTML .= '<p>'.intval($x->baths[0]).' / '.(!empty($x->baths[1]) ? intval($x->baths[1]) : 0).' bath</p>';
			if ($x->interior > 0){
				if ($x->interior_std == 'meters') $x->interior_std = '<span class="smalltext">m<span class="tinytext">2</span></span>';
				else if ($x->interior_std == 'acres') $x->interior_std = 'acres';
				else $x->interior_std = '<span class="smalltext">sqft</span>';
				$listingHTML .= '<div style="display:inline-block"><span class="slash">/</span><p>'.number_format($x->interior).' '.$x->interior_std.'</p></div>';
			}
			if ($x->lotsize > 0){
				if ($x->lotsize_std == 'meters') $x->lotsize_std = '<span class="smalltext">m<span class="tinytext">2</span></span>';
				else if ($x->lotsize_std == 'acres') $x->lotsize_std = '<span class="smalltext">acres</span>';
				else $x->lotsize_std = '<span class="smalltext">sqft</span>';
				$listingHTML .= '<div style="display:inline-block"><span class="slash">/</span><p>'.number_format($x->lotsize).' '.$x->lotsize_std.'</p></div>';
			}
			$listingHTML .= '<span class="slash">/</span>'.$listing->listing_status;
			$listingHTML .= '</div>';
			echo $listingHTML;
			?>
			<div id="shareDiv">
				<a id="facebook-share" href="javascript:shareSocialNetwork('FB',1,'https://www.facebook.com/sharer/sharer.php?u=<?php echo get_home_url().'/listing/LF-'.$listing_id.(!empty($portalDirect) ? '/'.$portalDirect : ''); ?>')" target="_blank"><img src="<?php echo get_template_directory_uri().'/_img/_promo/facebook.png'; ?>" /></a>
				<a id="linkedin-share" href="javascript:shareLinkedIn({	comment:'Hi friends check out this listing!', title:'Another listing from LifeStyled Listings', description:'<?php echo (!empty($listing->title) ? $listing->title : $listing->street_address.', '.$listing->city).(!empty($listing->price) ? ', $'.$listing->price : '').(!empty($listing->beds) ? ', Beds:'.$listing->beds : '').(!empty($baths) ? ", Baths:".number_format($baths, 1) : ''); ?>', url:'<?php echo get_home_url().'/listing/LI-'.$listing_id.(!empty($portalDirect) ? '/'.$portalDirect : ''); ?>', img:'<?php echo $first_image; ?>', where: 1});" target="_blank"><img src="<?php echo get_template_directory_uri().'/_img/_promo/linkedin.png'; ?>" /></a>
				<a id="googleplus-share" href="javascript:shareSocialNetwork('GP',1,'https://plus.google.com/share?url=<?php echo get_home_url().'/listing/LG-'.$listing_id.(!empty($portalDirect) ? '/'.$portalDirect : ''); ?>')" target="_blank"><img src="<?php echo get_template_directory_uri().'/_img/_promo/googleplus.png'; ?>" /></a>
				<a id="twitter-share" href="javascript:shareSocialNetwork('TW',1,'https://twitter.com/share?url=<?php echo get_home_url().'/listing/LT-'.$listing_id.(!empty($portalDirect) ? '/'.$portalDirect : ''); ?>')" target="_blank"><img src="<?php echo get_template_directory_uri().'/_img/_promo/twitter.png'; ?>" /></a>
				<!-- <a id="email-share" href="mailto:%20?subject=<?php echo $emailSubject; ?>&body=<?php echo $emailBody; ?>"><img src="<?php echo get_template_directory_uri().'/_img/_promo/email.png'; ?>" /></a> -->
				<a id="email-share" href="javascript:shareSocialNetwork('EM',1,'<?php echo $emailHref; ?>');"><img src="<?php echo get_template_directory_uri().'/_img/_promo/email.png'; ?>" /></a>
				<a id="client-share" href="javascript:;">Share with client</a>
				<a id="video-view" href="#videoPosition"><span class="video" id="listing" style="background: none; display: <?php echo empty($listing->video) ? "none;" : "inline;"?>">Watch Video<span class="entypo-play" style="font-size: .8em;margin-left: 8px;"></span></span></a>
			</div>
			<a href="javascript:listings.gotoExploreMap(2)">
				<div class="explore">
					<div id="container" style="display:none;">
						<div id="mobile-listing-map"></div>
						<button>Show bigger map</button>
					</div>
				</div>
			</a>
			<div class="seemore">Read More<span class="entypo-down-open-big"></span></div><div class="abouttexthidden"><span><?php echo $x->about; ?></span></div><div class="abouttext" style="display: none;"><span><?php echo $x->about; ?></span><div class="seeless" style="display: none;"><span class="entypo-up-open-big"></span>Collapse</div></div>
		<script type="text/javascript">
			$.fn.capitalise = function() {
				return this.each(function() {
					var $this = $(this),
							text = $this.text(),
							split = text.split(' '),
							res = [],
							i,
							len,
							component;
					for (i = 0, len = split.length; i < len; i++) {
							component = split[i];
							res.push(component.substring(0, 1).toUpperCase());
							res.push(component.substring(1).toLowerCase());
							res.push(" "); // put space back in
					}
					$this.text(res.join(''));
				});
			};
			$('.listing>section .listing-meta>table .street_address').capitalise();
			$('.listing>section .mobile-listing-meta .mobileinfo .street_address').capitalise();
			$('.listing .mobile-listing-title .mobileinfo .street_address').capitalise();
		</script>
	</section>
	<?php
	if ( current_user_can('add_users') &&
		 current_user_can('manage_options') && // then pretty damn sure this is being called by admin
		 $isAdmin == 1 ) : // then came from admin's Developer/listing section
	?>
	<section class="adminControl"></section>
	<?php endif; ?>
	<div class="listing-info" id="videoPosition">
		<div class="listing-about">
			<div class="discover-this-listing"><span class="title">Read about</span>&nbsp;<span class="subtitle1">this listing</span>
				<a id="video-view" href="#videoPosition"><span class="video" id="listing" style="background: none; display: <?php echo empty($listing->video) ? "none;" : "inline;"?>">Watch the video<span class="entypo-play" style="font-size: .8em;margin-left: 8px;"></span></span></a>
			</div>
			
			<!--<div class="favorite">
				Favorite this home
				<a href="#">
					<img class="bottom" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/favorite.jpg" style="cursor:pointer" />
					<img class="top" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/favorite-on.jpg" style="cursor:pointer" />
				</a>
			</div>-->
			<div class="text scrollbar">
				<?php echo $x->about; ?>
			</div>
		</div>
		<span id="mobile-disclaimer">Powered by Google Maps</span>
		<div class="mapIt" >
			<div class="explore">
				<div class="geo-failed"><p>Google Maps could not find any geo info for this listing or city.</p></div>
			</div>
			<div id="container">
				<div id="listing-map"></div>
				<button id="go-explore">Explore the Area</button>
			</div>
			<span id="disclaimer">Powered by Google Maps</span>
		</div>
		<?php if ($isPortal) : ?>
		<div class="premier-agent">
			<span class="mobile-container">
				<a class="agent-img" href="javascript:agentProfile('<?php echo get_home_url().'/agent/'.(empty($portalAgent->nickname) ? $portalAgent->author_id : $portalAgent->nickname); ?>');"><img src="<?php bloginfo('template_directory'); ?>/_img/_authors/250x250/<?php echo $portalAgent->photo ? $portalAgent->photo : '_blank.jpg' ; ?>" /></a>
				<div class="meta">
					<h4 class="name notranslate"><?php echo $portalAgent->first_name.' '.$portalAgent->last_name; ?></h4>
					<span class="company notranslate"><?php echo $portalAgent->company; ?></span>
					<!-- <span class="phone"><?php echo strlen($portalAgent->phone) ? $portalAgent->phone : ''; ?></span> -->
					<div id="tip">
						<img src="<?php echo get_template_directory_uri().'/_img/page-listings/down-arrow.png'; ?>" /><span>&nbsp;Need some guidance?</span>
					</div>
					<a href="javascript:;" class="contact-button" for="<?php echo $portalAgent->id; ?>">Contact Me</a>
					<a href="javascript:agentProfile('<?php echo get_home_url().'/agent/'.(empty($portalAgent->nickname) ? $portalAgent->author_id : $portalAgent->nickname); ?>');" class="go-profile" for="<?php echo $portalAgent->id; ?>">View Profile</a>
				</div>
			</span>
			<div id="intro">
				<h3>Your trusted guide</h3>
				<p>I'm here to help you in any way you may need, whether that's a second opinion or any general real estate questions.  Feel free to reach out to me!</p>
			</div>
		</div>
		<?php endif; ?>
	 	<?php if ($premierAgents != "0") : ?>
		<div class="premier-agents">
			<div id="banner">
				<div class="imageDiv">
					<span class="image1"><img src="<?php echo get_template_directory_uri().'/_img/page-listings/lifestyled-image-1.jpg'; ?>" /></span>
					<span class="image2"><img src="<?php echo get_template_directory_uri().'/_img/page-listings/lifestyled-image-2.jpg'; ?>" /></span>
					<span class="image3"><img src="<?php echo get_template_directory_uri().'/_img/page-listings/lifestyled-image-3.jpg'; ?>" /></span>
					<span class="image4"><img src="<?php echo get_template_directory_uri().'/_img/page-listings/lifestyled-image-4.jpg'; ?>" /></span>
				</div>
				<div class="logoDiv">
					<div class="title"><span class="life notranslate">Life</span><span class="lifestyledsmall notranslate">styled</span> <span class="agents notranslate">Agents</span></div>
					<span class="subtitle">Specialized real estate agents matched to you</span>
				</div>
			</div>
			<!--<div id="header" style="display: inline;">
				<h1 class="premier-agent-label"><span class="line1"></span><p class="title">Agents</p><span class="line2"></span></h1>
				<div class="sortlanguage"><label id="languageLabel" style="display: none;">Sort by language: </label><select name="languages" id="languages" style="display: none;"></select></div>
				<span id="agents-header">Local Agents that are experts in your lifestyle</span>
			</div>
			<div id="mobileheader" style="display: inline;">
				<h1 class="premier-agent-label"></span><p class="title">Lifestyled Agents</p></span></h1>-->
				<!--<div class="sortlanguage"><label id="languageLabel" style="display: none;">Sort by language: </label><select name="languages" id="languages" style="display: none;"></select></div>-->
				<!--<span id="agents-header">Meet some real estate experts that share and love your lifestyle.</span>
			</div>-->
	 		<div class="agent-list" style="overflow: hidden; height: 100%;"><ul class="premier-agents-list" style="display: inline; overflow: auto;" /></div>
		</div>
	 	<?php endif; ?>
		<div class="more-agents" style="display: none;">
			<button id="more">See more agents&nbsp;&nbsp;<span class="entypo-right-open-big"></span></button>
		</div>
		<div class="listing-agent">
			<div id="info">
				<?php if (!empty($agent) && !empty($agent->author_id))
					echo '<a href="javascript:agentProfile('."'".$agentPage."'".');" target="_blank">'; ?>
				<img src="<?php bloginfo('template_directory'); ?>/_img/_authors/<?php  echo !$isPortal ? '250x250/' :  '250x250/'; ?><?php echo $agent && $agent->photo ? $agent->photo : '_blank.jpg' ; ?>" />
				<?php if (!empty($agent) && !empty($agent->author_id))
					echo '</a>'; ?>
				<h3>Listing Agent</h3>
				<div class="meta">
					<h4 class="name notranslate"><?php echo ($agent && $agent->first_name ? $agent->first_name : '').' '.($agent && $agent->last_name ? $agent->last_name : ''); ?></h4>
					<span class="company notranslate"><?php echo $agent && $agent->company ? $agent->company : ''; ?></span>
					<span class="contact">
						<?php if (!empty($agent))
						echo '<a href="javascript:;" class="contact-button" for="'.$agent->id.'">Email Me</a>';
						?>
						<?php if (!empty($agent) && !empty($agent->office_email))
						echo '<span class="slash">/</span><a href="javascript:;" class="contact-button" for="'.$agent->id.'" where="office">Email Office</a>';
						?>
						<span class="phone"><?php echo $agent && $agent->phone ? '<span class="slash">/</span>'.$agent->phone : ''; ?></span>
					</span>
					<?php if (!empty($listing->url)) : ?>
					<span class="listing-source"><a href="<?php echo $listing->url; ?>" target="_blank">Listing source</a></span>
					<?php endif; ?>
				</div>
			</div>
		</div> 
		<div class="tags">
			<div class="titleListings">Home features</div>
			<ul class="showTagsListings">
				<?php
					if (!empty($x->tags)){
						sort($x->tags);
						foreach ($x->tags as $tag) if ($tag->type == 0 ) echo '<li><span>'.$tag->tag.'</span></li>';
					}
				?>
			</ul>
			<div class="titleCity">Location qualities</div>
			<ul class="showTagsCity">
				<?php
					if (!empty($x->tags)){
						sort($x->tags);
						foreach ($x->tags as $tag) if ($tag->type == 1 ) echo '<li><span>'.$tag->tag.'</span></li>';
					}
				?>
			</ul>
		</div>
	</div>
</div>
<?php endif; endif; ?>
