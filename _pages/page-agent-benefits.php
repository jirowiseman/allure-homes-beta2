<?php
require_once(__DIR__.'/../_classes/Options.class.php'); $Options = new AH\Options();

$home_start_opt = get_query_var('id');

if ($_SERVER['SERVER_NAME'] == 'localhost') $local = true; // So I can work locally without pulling remote scripts
else $local = false;

global $thisPage;	
$opt = $Options->get((object)['where'=>['opt'=>'VideoList']]);
$video1 = 'agent-benefits-portal';
$video2 = 'agent-benefits-agent-match';
// $video1 = 131675206;
// $video2 = 141358615;
$videoList = new stdClass();
$videoList->$video1 = 131675206;
$videoList->$video2 = 141358615;
if (!empty($opt)) {
	$videoList = json_decode($opt[0]->value);
	foreach($videoList as $page=>$video) {
		if ( strpos(strtolower($page), $thisPage) === 0) {
      		$videoList->$page = $video;
    	}
	}
}
?>
<script type="text/javascript">
var startOption = '<?php echo !empty($home_start_opt) ? $home_start_opt : 0; ?>';
var videoList = <?php echo json_encode($videoList); ?>;
</script>

<div class="videoDiv" id="<?php echo $video2; ?>">
  <button class="closeVid"><span class="line1"></span><span class="line2"></span></button>
  <!-- <a id="closeVid" href="javascript:reg.closeFrame();">[X]</a> -->
  <span class="video"></span>
	  <!-- <iframe src="//player.vimeo.com/video/<?php echo $video2; ?>" width="100%" height="100%" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> 
  </span> -->
</div>

<div class="videoDiv" id="<?php echo $video1; ?>">
  <button class="closeVid"><span class="line1"></span><span class="line2"></span></button>
  <!-- <a id="closeVid" href="javascript:reg.closeFrame();">[X]</a> -->
  <!-- <iframe src="//player.vimeo.com/video/131676770" width="900" height="600" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen> -->
  <span class="video"></span>
  	<!-- <iframe src="//player.vimeo.com/video/<?php echo $video1; ?>" width="100%" height="100%" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> 
  </span> -->
</div>

<section id="intro">
	<div class="data" id="intro-div">
		<div id="content">
			<span id="title">List with <span class="notranslate">LifeStyled Listings</span><span style="font-size:.6em;position:absolute;margin-top:-5px">&nbsp;&#8482;</span>&nbsp;&nbsp;&nbsp; for free</span>
			<span id="main-content">Prominently placed listing agent, upload high resolution images, get worldwide reach</span>
			<button id="register">Register</button>
		</div>
	</div>
</section>
<section id="agent-match">
	<div class="data" id="agent-match-div" style="height: 650px">
		<div id="content">
			<span id="title">Do you have what it takes to work with Interested Buyers?</span>
			<span id="main-content">Dominate your area of expertise by becoming an exclusive <span class="notranslate">LifeStyled&#8482; Agent</span> in your area</span>
			<span id="footer"><span style="margin-top: -10px; width: 90%; font-size: .9em; line-height: 1.1em; text-shadow: 0px 0px 15px rgba(255,255,255,1);">Exclusively available only to 3 agents per lifestyle</span><span>
			<button class="video" id="<?php echo $video2; ?>" style="background: none; border: 1px solid;">Learn More</button>&nbsp;<button id="claim">Claim Your Spot</button>
		</div>
	</div>
</section>
<section id="portal">
	<div class="data" id="portal-div" style="height: 650px">
		<div id="content">
			<span id="title">Generate Referrals While You Sleep</span>
			<span id="main-content">Use your social and personal networks to actively get you paid referral fees</span>
			<span id="footer">You&#39;ve worked hard to build your network, now let us put it to work for you<span>
			<button class="video" id="<?php echo $video1; ?>" style="background: none; border: 1px solid;">Learn More</button>&nbsp;<button id="claim">Get Your Portal</button>
		</div>
	</div>
</section>