<div id="page-contact">
	<div class="contact-padding">
		<div class="main-contact">
			<div class="top">
					<span class="title">Contact Us</span>
					<p>Before contacting us, please visit our <a href="<?php echo get_site_url().'/faq'; ?>">FAQ</a> section</p>
			</div>
			<div class="line"></div>
			<!--<div class="live-chat">
					<span class="title">Live Chat</span>
					<p>Coming Soon to Lifestyled Listings, live chat with our customer<br/>service to help with any needs you have</p>
			</div>-->
			<div class="email">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-contact/mail.png" />
					<span class="email-contact">General Questions<br/><a href="mailto:Info@lifestyledlistings.com">Info@lifestyledlistings.com</a></span>
					<span class="email-contact2">Marketing Inqueries<br/><a href="mailto:John@lifestyledlistings.com">John@lifestyledlistings.com</a></span>
					<p style="font-style:italic;font-size:.8em;padding-top:1em">*All emails will be answered within 2 business days</p>
			</div>
			<p class="mid">You may also reach us by calling during our business hours</p>
			<div class="phone">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-contact/phone.png" />
					<span class="phone-contact">Monday - Friday &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9am - 5pm (PST)<br/>831.508.8821</span>
					<p>To mail us, please send to 877 Cedar St. #150, Santa Cruz, CA 95060</p>
			</div>
		</div>
	</div>
</div>