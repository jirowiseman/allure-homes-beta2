<?php
require_once(__DIR__.'/../_classes/Options.class.php'); $Options = new AH\Options();
require_once(__DIR__.'/../_classes/PortalUsersMgr.class.php'); $PortalUsersMgr = new AH\PortalUsersMgr();
require_once(__DIR__.'/../_classes/Utility.class.php'); 

global $browser;
global $isMobile;

	$redirect = $redirectPage = !empty(get_query_var('id')) ? get_query_var('id') : '';
	$extra = !empty(get_query_var('extra')) ? get_query_var('extra') : null;
	$extra2 = !empty(get_query_var('extra2')) ? get_query_var('extra2') : null;

	$PortalUsersMgr->log("redirect:$redirect, extra:$extra, extra2:$extra2");

	if (!empty($redirect)) {
		if ($extra !== null)
			$redirect .= "/$extra";
		if ($extra2 !== null)
			$redirect .= "/$extra2";
	}

	$goButtonText = empty($redirectPage) || $redirectPage == 'quiz' ? 'Start My Lifestyle Quiz' :
					($redirectPage == 'quiz-results' ? 'View results' :
					($redirectPage == 'listing' ? 'View listing' : 'Go to home'));


	$agentText = "I am here to assist you to find the best home in this area for you.  I listen to your needs and help guide you in knowing the area and its abundant resources.";
    $agentTextClean = "I am here to assist you to find the best home in this area for you.  I listen to your needs and help guide you in knowing the area and its abundant resources.";
    $agentTextShort = "I am here to assist you to find the best home in this area for you.  I listen to your needs and help guide you in knowing the area and its abunda...";
    $agentTextShorter = "I am here to assist you to find the best home in this area for you.  I listen to your needs and help guide you...";
	global $ALR;
	$isPortal = true;
	$portalAgent = $ALR->get('portal-agent');
	if ($portalAgent == "0")
		$isPortal = false;
	else {
		foreach($portalAgent->meta as $meta)
			if ($meta->action == SELLER_PORTAL_MESSAGE) {
                $agentTextClean = $meta->message;
				$agentText = AH\removeslashes($meta->message);
                if ((strlen($agentTextClean) > 110) && (strlen($agentTextClean) <= 145)) {
                  $agentTextShorter = substr($agentText, 0, 90).'...';
                  $agentTextShort = $agentText;
                }
                else if (strlen($agentTextClean) > 145) {
                  $agentTextShorter = substr($agentText, 0, 90).'...';
                  $agentTextShort = substr($agentText, 0, 145).'...';
                }
                else {
                  $agentTextShorter = $agentText;
                  $agentTextShort = $agentText;
                }
				break;
			}
	}


	if (!$isPortal) {
		if (headers_sent())
			echo '<script type="text/javascript"> window.location = "'.get_home_url().'"; </script>';
		else {
			header('Location: '.get_home_url());
			header("Connection: close");
		}
		return;
	}

	$opt = $Options->get((object)['where'=>['opt'=>'PortalLandingAgentOptions']]);
	$agentOptionsList = empty($opt) ? [] : json_decode($opt[0]->value);
	$agentOptions = null;
	if (!empty($agentOptionsList)) {
		foreach($agentOptionsList as $item) 
			if ($item->agentID == $portalAgent->author_id) {
				$agentOptions = $item;
				break;
			}

		if (empty($agentOptions)) foreach($agentOptionsList as $item) 
			if ($item->agentID == -1) { // global
				$agentOptions = $item;
				break;
			}
	}

	$defaultStringSet = ['data0'=>(object)['type'=>'span',
										  'id'=>'title',
										  'html'=>"Retiring<span class=&quot;dot&quot;>|</span>Relocating<span class=&quot;dot&quot;>|</span>Second home?"],
						 'data1'=>(object)['type'=>'span',
								 		  'id'=>'subtext',
								 		  'html'=>"Live somewhere with better lifestyle"],
						 'data2'=>(object)['type'=>'span',
								 		  'id'=>'subtitle',
								 		  'html'=>"Find Cities &amp; Homes that match your lifestyle perfectly"]
	];

	$opt = $Options->get((object)['where'=>['opt'=>'PortalLandingStrings']]);
	$portalLandingStringsList = empty($opt) ? [] : json_decode($opt[0]->value);
	$portalLandingStrings = null;
	if (!empty($portalLandingStringsList)) {
		foreach($portalLandingStringsList as $item) 
			if ($item->agentID == $portalAgent->author_id) {
				$portalLandingStrings = $item;
				break;
			}

		if (empty($portalLandingStrings)) foreach($portalLandingStringsList as $item) 
			if ($item->agentID == -1) { // global
				$portalLandingStrings = $item;
				break;
			}
	}
	if (empty($portalLandingStrings))
		$portalLandingStrings = $defaultStringSet; // default

	// PortalLandingSlides
	$opt = $Options->get((object)['where'=>['opt'=>'PortalLandingSlides']]);
	$portalLandingSlides = empty($opt) ? [] : json_decode($opt[0]->value);
	$desktopSlides = '';
	$mobileSlides = '';

	if (!empty($portalLandingSlides)) {
		foreach($portalLandingSlides as $type=>$set) {
			$slides = '';
			foreach($set as $slide)
				$slides .= '<li class="'.$type.' slide"><img src="'.get_template_directory_uri()."/_img/page-portal-landing/$type-slides/$slide".'" /></li>';
			if ($type == 'desktop')
				$desktopSlides = $slides;
			else
				$mobileSlides = $slides;
		}
	}

	$imgPath = get_template_directory_uri()."/_img/page-portal-landing/";
	$photo = get_template_directory_uri()."/_img/_authors/250x250/".(!empty($portalAgent->photo) ? $portalAgent->photo : '_blank.jpg');
	$city = file_exists($imgPath."city.png") ? $imgPath."city.png" : $imgPath."_blank.jpg";
	$home = file_exists($imgPath."home.png") ? $imgPath."home.png" : $imgPath."_blank.jpg";
	$agent = file_exists($imgPath."agent.png") ? $imgPath."agent.png" : $imgPath."_blank.jpg";
	$nameMode = !empty($agentOptions) && isset($agentOptions->nameMode) ? ($agentOptions->nameMode != PORTAL_LANDING_NOT_USED ? PORTAL_LANDING_MUST_HAVE : $agentOptions->nameMode) : PORTAL_LANDING_MUST_HAVE;
	$phoneMode = !empty($agentOptions) ? $agentOptions->phoneMode : PORTAL_LANDING_NOT_USED;
	// $emailMode = !empty($agentOptions) ? ($phoneMode == PORTAL_LANDING_USE_AS_PASSWORD ? PORTAL_LANDING_MUST_HAVE : $agentOptions->emailMode) : PORTAL_LANDING_MUST_HAVE;
	$emailMode = !empty($agentOptions) ? $agentOptions->emailMode : PORTAL_LANDING_MUST_HAVE;
	// 0 - not used
	// 1 - optional
	// 2 - must have
	// 3 - as password
	$phonePlaceHolder = $phoneMode == PORTAL_LANDING_NOT_USED ? '' :
						($phoneMode == PORTAL_LANDING_OPTIONAL ||
						 $phoneMode == PORTAL_LANDING_MUST_HAVE ? 'Enter Phone' : 'Enter Phone as Password');

	$form = wp_login_form( array( 	'echo' => false, 
				'redirect' => site_url().(!empty($redirectPage) ? '/'.$redirectPage : "/quiz/#sq=0"),
				'label_username' => __( 'Username:' ), 
				'label_password' => __( 'Password:' ), 
				'form_id' => 'login-form', 
				'remember' => false) );
	$form = explode('</form>', $form);
	$form = str_replace("\n","", $form[0]);
	$form = str_replace("\t","", $form);
	$form = str_replace("\r","", $form);
	$lostPasswordUrl = wp_lostpassword_url();

	global $thisPage; 
	$opt = $Options->get((object)['where'=>['opt'=>'VideoList']]);
	$videoId = 166059782;
	if (!empty($opt)) {
	  $videoList = json_decode($opt[0]->value);
	  foreach($videoList as $page=>$video) {
	    if (strtolower($page) == $thisPage) {
	      $videoId = $video;
	      break;
	    }
	  }
	}

?>
<script type="text/javascript">
    var agentTextCleanLen = <?php echo strlen($agentTextClean); ?>;
    var agentTextClean = <?php echo json_encode(['agentMsg'=>$agentTextClean]); ?>;
	var portalAgent = <?php echo $portalAgent != "0" ? json_encode($portalAgent) : "0"; ?>;
	var isPortal = <?php echo $isPortal ? 1 : 0; ?>;
	var redirectPage = '<?php echo $redirect; ?>';
	var nameMode = <?php echo $nameMode ? $nameMode : 0; ?>;
	var phoneMode = <?php echo $phoneMode ? $phoneMode : 0; ?>;
	var emailMode = <?php echo $emailMode ? $emailMode : 0; ?>;
	var login_form = '<?php echo trim($form); ?>';
	var lostPasswordUrl = '<?php echo $lostPasswordUrl; ?>';
    var videoList = <?php echo json_encode($videoList); ?>;

jQuery(document).ready(function($){
	
	if (isRetina){
		$('.portal-landing .gallery .desktop-wrapper .laptop img').replaceWith( '<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/laptop-retina.png"/>' );
		$('.portal-landing #how-to-steps .step1 img').replaceWith( '<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/ipad.png"/>' );
		$('.portal-landing #how-to-steps .step2 img').replaceWith( '<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/iphone-map.png"/>' );
	}
	
	// ---- Animated Loadins ---- //	
	/*(function($) {
		$.fn.visible = function(partial) {
				var $t            = $(this),
						$w            = $(window),
						viewTop       = $w.scrollTop(),
						viewBottom    = viewTop + $w.height(),
						_top          = $t.offset().top,
						_bottom       = _top + $t.height(),
						compareTop    = partial === true ? _bottom : _top,
						compareBottom = partial === true ? _top : _bottom;

			return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
		}; 
	})(jQuery);

	var win = $(window);
	win.scroll(function(event) {
		// allMods.each(function(i, el) {
			var el = $('.portal-landing #home-bottom .home-bottom-bg');
			if (el.visible(true)) {
				el.addClass('animate');
			} 
		// });
	});
	

	// ---- Info Slider ---- //
	function activeInfo1() {
		$('#home-mid-slider .slider-wrapper').addClass('slide1active');
		$('#home-mid-slider .slider-wrapper').removeClass('slide2active');
		$('#home-mid-slider .slider-wrapper').removeClass('slide3active');
		$('.mid-slider-buttons .button1').addClass('active');
		$('.mid-slider-buttons .button2').removeClass('active');
		$('.mid-slider-buttons .button3').removeClass('active');
	}
	function activeInfo2() {
		$('#home-mid-slider .slider-wrapper').removeClass('slide1active');
		$('#home-mid-slider .slider-wrapper').addClass('slide2active');
		$('#home-mid-slider .slider-wrapper').removeClass('slide3active');
		$('.mid-slider-buttons .button1').removeClass('active');
		$('.mid-slider-buttons .button2').addClass('active');
		$('.mid-slider-buttons .button3').removeClass('active');
	}
	function activeInfo3() {
		$('#home-mid-slider .slider-wrapper').removeClass('slide1active');
		$('#home-mid-slider .slider-wrapper').removeClass('slide2active');
		$('#home-mid-slider .slider-wrapper').addClass('slide3active');
		$('.mid-slider-buttons .button1').removeClass('active');
		$('.mid-slider-buttons .button2').removeClass('active');
		$('.mid-slider-buttons .button3').addClass('active');
	}
	
	if (isMobile){
		var slideDiv1 = $('#home-mid-slider .slider-wrapper .slide1');
		var slideDiv2 = $('#home-mid-slider .slider-wrapper .slide2');
		var slideDiv3 = $('#home-mid-slider .slider-wrapper .slide3');
		Hammer(slideDiv1[0]).on('swipeleft', function(event) {
			activeInfo2();
		});
		Hammer(slideDiv2[0]).on('swiperight', function(event) {
			activeInfo1();
		});
		Hammer(slideDiv2[0]).on('swipeleft', function(event) {
			activeInfo3();
		});
		Hammer(slideDiv3[0]).on('swiperight', function(event) {
			activeInfo2();
		});
	}
	
	$('.mid-slider-buttons .button1').on('click', function() {
		activeInfo1();
	});
	$('.mid-slider-buttons .button2').on('click', function() {
		activeInfo2();
	});
	$('.mid-slider-buttons .button3').on('click', function() {
		activeInfo3();
	});*/
  
    var len = agentTextCleanLen;
    if (len > 145){
      $('.portal-landing #home-top #agent #agent-intro #agent-sig').hide();
      $('.portal-landing #home-top #agent #agent-intro #agent-readmore').show();
    }
  
    $('.portal-landing #home-top #agent #agent-intro #agent-readmore').on('click', function() {
      $('.portal-landing #home-top #agent #agent-intro #agent-readmore').hide();
      $('.portal-landing #home-top #agent #agent-intro #agent-readless').show();
      $('.portal-landing #home-top #agent #agent-intro #agent-text.short').hide();
      $('.portal-landing #home-top #agent #agent-intro #agent-text.long').show();
      $('.portal-landing #home-top #agent #agent-intro #agent-sig').show();
      return false;
    });
    $('.portal-landing #home-top #agent #agent-intro #agent-readless').on('click', function() {
      $('.portal-landing #home-top #agent #agent-intro #agent-readmore').show();
      $('.portal-landing #home-top #agent #agent-intro #agent-readless').hide();
      $('.portal-landing #home-top #agent #agent-intro #agent-text.short').show();
      $('.portal-landing #home-top #agent #agent-intro #agent-text.long').hide();
      $('.portal-landing #home-top #agent #agent-intro #agent-sig').hide();
      return false;
    });
})

jQuery(window).load(function () {
	var ele = $('.top-panel');
	if (ele.length) {
		ele.css('min-height', '');
		ele.css('max-height', '');
		ele.removeClass('fullscreen-height');
		var height = ele.innerHeight();
		ele.css('min-height', height+'px');
		ele.css('max-height', height+'px');
		ele.addClass('fullscreen-height');
	}
})

</script>

<div id="overlay-bg" style="display: none;">
	<div class="spin-wrap"><div class="spinner sphere"></div></div>
</div>

<div class="portal-landing">
	<div class="logged-in-already-with-fb">
		<div class="blackout"></div>
		<div class="redirect-content">
			<div class="redirect-header">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/social_media/facebook-icon.png" id="fb_login"></img><span>Connected with facebook</span>
			</div>
			<span class="entypo-link"></span>
			<div class="redirect-message">
				<span></span>
			</div>
			<div class="redirect-buttons">
				<button id="redirect"></button>
				<button id="cancel">Cancel</button>
			</div>
		</div>
	</div>
    <div class="global-signin-popup">
      <div class="blackout"></div>
      <div class="signin-content">
				<div class="close">x</div>
        <div class="signin-header"><span style="font-weight:400;">Welcome back!</span> Please sign in</div>
				<div id="fbDiv" style="display:none;">
						<div id="fb-root"></div>
						<fb:login-button scope="public_profile,email" onlogin="checkLoginState();" data-size="xlarge">Log in with Facebook</fb:login-button>
						<a href="#" class="fb-fakebutton" onclick="fbLogin();">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/social_media/facebook-icon.png" id="fb_login"></img>Continue with facebook
						</a>
						<div class="or-wrapper">
							<span class="line1"></span>
							<span class="line2"></span>
							<span id="or">OR</span>
						</div>
				</div>
        <div class="signin-wrapper">
          <form name="login-form" id="login-form" action="<?php echo site_url().'/wp-login.php'; ?>" method="post"><p class="login-username">
          <input type="text" name="log" id="user_login" class="input" value="" placeholder="Username">
          <input type="password" name="pwd" id="user_pass" class="input" value="" placeholder="Password">
          <input type="submit" name="wp-submit" id="wp-submit" class="button-primary" value="Log In">
          <input type="hidden" name="redirect_to" value="<?php echo site_url().(!empty($redirect) ? '/'.$redirect : "/quiz/#sq=0"); ?>">
          <p class="forgot-password"><a href="'+lostPasswordUrl+'" title="Forgot your password?">Forgot your password?</a></p>
        </div>
			</div>
    </div>
	<div class="home-top-wrapper">
		<section id="home-top">
			<div class="top-panel">
				<div id="agent">
					<div class="agent-photo-wrapper"><img class="agent-photo" src="<?php echo $photo; ?>" /></div>
					<div id="agent-intro">
						<div id="agent-text">
							<div class="desktop-agent-text">
								<span id="agent-text" class="short"><span style="font-style:italic; margin-right:4px; margin-left:-4px;">"</span><?php echo $agentTextShort; ?><span style="font-style:italic; margin-left:-1px; margin-right:10px;">"</span></span>
							</div>
							<div class="mobile-agent-text">
								<span id="agent-text" class="short"><span style="font-style:italic; margin-right:4px; margin-left:-4px;">"</span><?php echo $agentTextShorter; ?><span style="font-style:italic; margin-left:-1px; margin-right:10px;">"</span></span>
							</div>
												<span id="agent-text" class="long" style="display:none;"><span style="font-style:italic; margin-right:4px; margin-left:-4px;">"</span><?php echo $agentText; ?><span style="font-style:italic; margin-left:-1px; margin-right:10px;">"</span></span>
												<span id="agent-sig">- <?php echo $portalAgent->first_name.' '.$portalAgent->last_name; ?></span>
												<span id="agent-readmore" style="display:none;">Read more</span>
												<span id="agent-readless" style="display:none;">Read less</span>
						</div>
					</div>
					<button class="agent-contact">Contact me</button>
				</div>
				<div class="home-top-content">
					<div id="intro" class="desktop">
					<?php 
						if ( isset($portalLandingStrings) && !empty($portalLandingStrings) )
							AH\outputDynamicHtml($portalLandingStrings);
					?>
					</div>
					<div id="userdata" group="one">
							<input type="text" id="one" class="name portalUserInfo" placeholder="Your Name" style="display: none;"></input>
							<input type="text" id="one" class="email portalUserInfo" placeholder="Your Email" style="display: none;"></input>
							<input type="text" id="one" class="phone portalUserInfo" placeholder="<?php echo $phonePlaceHolder ?>" style="display: none;"></input>
							<a class="start" id="one" href="javascript:portal_user_register.start(1);" style="display: none;"><?php echo $goButtonText; ?></a>
					</div>
					<!--<div class="arrow-down"><span class="entypo-down-open-big"></span></div>-->
					<div class="gallery">
						<div class="desktop-wrapper">
							<div class="laptop"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/laptop.png"/></div>
							<ul class="slide-gallery desktop" style="display:none;">
								<?php echo $desktopSlides; ?>
							</ul>
						</div>
						<div class="mobile-wrapper">
							<div class="phone"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/pixel-phone.png"/></div>
							<ul class="slide-gallery mobile">
								<?php echo $mobileSlides; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="videoDiv">
		<button class="closeVid"><span class="line1"></span><span class="line2"></span></button>
		<span class="video">
	      <iframe src="//player.vimeo.com/video/<?php echo $videoId; ?>" width="100%" height="100%" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	    </span>
	</div>
	<!--<div class="mid-slider-buttons">
		<span class="button1 active"></span>
		<span class="button2"></span>
		<span class="button3"></span>
	</div>
	<section id="home-mid-slider">
		<div class="slider-wrapper slide1active">
			<div class="slide1">
				<div class="mobiletextbox">					
					<span class="title">Search by your own</span>
					<span class="subtitle">unique lifestyle!</span>
					<a href="#videoDiv" class="video"><span class="entypo-right-dir"></span></a>
					<div class="mobile-image"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/slide1.jpg"/></div>
					<p class="main-text">It all starts with you telling us what you love! Just select the images that match your lifestyle needs and instantly get matched to a list of your top cities, homes, and even professionals all completely tailored to you.</p>
					<div class="three-steps">
						<div class="tri-numbers">
							<span class="number" id="one">1</span><span class="line"></span>
							<span class="number" id="two">2</span><span class="line"></span>
							<span class="number" id="three">3</span>
						</div>
						<div class="tri-images">
							<img class="one" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/three-steps-1.jpg"/>
							<img class="two" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/three-steps-2.jpg"/>
							<img class="three" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/three-steps-3.jpg"/>
						</div>
						<div class="tri-text">
							<p>Activities</p>
							<p>Environment</p>
							<p>Home features</p>
						</div>
					</div>
				</div>
				<div class="textbox">
					<div class="textbox-wrapper">
						<span class="title">Search by your own</span>
						<span class="subtitle">unique lifestyle!</span>
						<a href="#videoDiv" class="video"><span class="entypo-right-dir"></span></a>
						<p class="main-text">It all starts with you telling us what you love! Just select the images that match your lifestyle needs and instantly get matched to a list of your top cities, homes, and even professionals all completely tailored to you.</p>
						<div class="three-steps">
							<div class="tri-numbers">
								<span class="number" id="one">1</span><span class="line"></span>
								<span class="number" id="two">2</span><span class="line"></span>
								<span class="number" id="three">3</span>
							</div>
							<div class="tri-images">
								<img class="one" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/three-steps-1.jpg"/>
								<img class="two" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/three-steps-2.jpg"/>
								<img class="three" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/three-steps-3.jpg"/>
							</div>
							<div class="tri-text">
								<p>Activities</p>
								<p>Environment</p>
								<p>Home features</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide2">
				<div class="mobiletextbox">
					<span class="title">Discover new locations and homes!</span>
					<div class="mobile-image"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/slide2.jpg"/></div>
					<div class="text">
						<p>See the best cities for your lifestyle and homes in those cities ordered by the closest fit to what you asked for.</p>
						<a class="start" href="<?php bloginfo('wpurl'); ?>/quiz/#sq=0">Start your quiz!</a>
					</div>
				</div>
				<div class="textbox">
					<div class="textbox-wrapper">
						<span class="title">Discover new locations and homes!</span>
						<p>See the best cities for your lifestyle and homes in those cities ordered by the closest fit to what you asked for.</p>
					</div>
				</div>
			</div>
			<div class="slide3">
				<div class="mobiletextbox">
					<span class="title">Find the perfect agent</span>
					<div class="mobile-image"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/slide3.jpg"/></div>
					<div class="text">
						<p>Find real estate agents like never before. We match you to agents who are experts in the lifestyle or property type you're looking for.</p>
						<a class="start" href="<?php bloginfo('wpurl'); ?>/quiz/#sq=0">Start your quiz!</a>
					</div>
				</div>
				<div class="textbox">
					<div class="textbox-wrapper">
						<span class="title">Find the perfect agent</span>
						<p>Find real estate agents like never before. We match you to agents who are experts in the lifestyle or property type you're looking for.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="mid-bottom">
		<div id="agent-mid">
			<img src="<?php echo $photo; ?>" />
			<div id="description">
				<span id="name"><span class="portal-color"><?php echo $portalAgent->first_name.' '.$portalAgent->last_name; ?></span><span class="after-name"> is your trusted guide</span></span>
				
				<span id="subtitle">When searching in new territory you need a friend who can watch your back to make sure you make the best decisions.</span>
				<div id="agent-text">
					<span id="bigquote" class="start entypo-quote"></span>
					<span id="agent-text"><?php echo $agentText; ?></span>
					<span id="bigquote" class="end entypo-quote"></span>
					<span id="agent-sig">-<?php echo $portalAgent->first_name.' '.$portalAgent->last_name; ?></span>
				</div>
			</div>
		</div>
	</section>
	<div class="mobile-agent-quote">
		<span id="bigquote" class="start entypo-quote"></span>
		<span id="agent-text"><?php echo $agentText; ?></span>
		<span id="bigquote" class="end entypo-quote"></span>
		<span id="agent-sig">-<?php echo $portalAgent->first_name.' '.$portalAgent->last_name; ?></span>
	</div>
	<section id="home-bottom">
		<span class="mobile-title">Lifestyled Listings is a <span class="mobilestyling">one stop shop</span></span>
		<div class="home-bottom-bg"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/iphone-large.jpg"/></div>
		<div class="content">
			<div class="top">
				<span class="title">Lifestyled Listings is a <span class="mobilestyling">one stop shop</span></span>
				<p>With Lifestyled Listings we help you find the best places to live and the best places to retire. We've designed our home search to lead you through the full home buying process. It starts with discovering</p>
			</div>
			<div class="cities">
				<span class="title">
					<span class="image">
						<span>Cities</span>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/cities.jpg"/>
					</span>
					<p>Over 17,000 Cities to choose from</p>
				</span>
				<p>Finding the right location to buy your dream home can be an intimidating process, but we're here to help. Just tell us what you want and we'll show you some great cities for whatever you're interested in. Looking for a thriving wine, golf and boating scene? We've got you covered.</p>
			</div>
			<div class="listings">
				<span class="title">
					<span class="image">
						<span>Homes</span>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/listings.jpg"/>
					</span>
					<p>1.6+ Million listings updated daily</p>
				</span>
				<p>We rank our massive listing database according to how well each one matches the features you've selected. Save time and energy when your results are ordered for you and easily compare how far your money goes in each of the markets you're interested in. Easily change the importance of each home feature or flag them as "must haves" to really get a custom viewing experience.</p>
			</div>
			<div class="agents">
				<span class="title">
					<span class="image">
						<span>Agents</span>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-home/agents.jpg"/>
					</span>
					<p>Over 430,000 active real estate agents</p>
				</span>
				<p>With so many agents to choose from and a constant focus on marketing, it can be hard to find the agent that best suits your needs. We tailor our agent matches to your search so you only see the agents that have experience and knowledge about the things that YOU are interested in. Read their detailed descriptions to understand what makes them so uniquely suited to your needs.</p>
			</div>
		</div>
	</section>-->
	<section id="three-steps">
		<div class="header">
			<div class="title"> Most search sites help you find a nice home. <span class="breakpoint"><br/></span>We help you find an amazing life.</div>
			<div class="line"></div>
			<div class="subtitle"> Discover amazing locations you've never heard of and <span class="breakpoint"><br/></span>homes you're sure to love with one simple search.</div>
		</div>
		<div class="steps">
			<div class="step1">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/step1-icon.jpg"/>
				<div class="step-title">Amazing <span>Destinations</span></div>
				<p>We analyze all your lifestyle needs to instantly match you to thousands of cities &amp; communities around the U.S.</p>
			</div>
			<div class="step2">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/step2-icon.jpg"/>
				<div class="step-title">Perfect <span>Homes</span></div>
				<p>We match you with homes based on the things you love, from our inventory of 1.6M real estate listings nationwide</p>
			</div>
			<div class="step3">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/step3-icon.jpg"/>
				<div class="step-title">Matched to you <span>Instantly</span></div>
				<p>We combined everything you need to discover locations and homes all in one search</p>
			</div>
		</div>
	</section>
	<div class="divider">
		<div class="title">The latest homes from</div>
		<img class="desktop" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/logos.jpg"/>
        <img class="mobile" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/logos-small.jpg"/>
	</div>
	<section id="how-to-steps">
		<div class="step1">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/ipad.png"/>
			<div class="textbox">
				<div class="title">Personalized searching <span class="breakpoint"><br/></span>based on what you love</div>
				<div class="line"></div>
				<p>You start with a quick 10 question lifestyle quiz that points you right to the best places for you.</p>
			</div>
		</div>
		<div class="step2">
			<div class="background-color"></div>
			<img class="desktop" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/iphone-map.png"/>
			<div class="textbox">
				<div class="title">Search within a radius or <span class="breakpoint"><br/></span>anywhere in the U.S.</div>
				<div class="line"></div>
				<p>No matter where you want to search we can help you see more options than you thought possible!</p>
			</div>
			<img class="mobile" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/iphone-map.png"/>
		</div>
		<div class="step3">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-portal-landing/wallet.png"/>
			<div class="textbox">
				<div class="title">Your dream home may be <span class="breakpoint"><br/></span>closer than you think</div>
				<div class="line"></div>
				<p>Whatever your budget is, we can find you a home in a city you'll love.</p>
			</div>
		</div>
	</section>
	<div class="calltoaction">
		<p> This may be the best thing <span class="breakpoint"><br/></span>you've ever done.</p>
		<div id="userdata" group="two">
			<input type="text" id="two" class="name portalUserInfo" placeholder="Your Name" style="display: none;"></input>
			<input type="text" id="two" class="email portalUserInfo" placeholder="Your Email" style="display: none;"></input>
			<input type="text" id="two" class="phone portalUserInfo" placeholder="<?php echo $phonePlaceHolder ?>" style="display: none;"></input>
			<a class="start" id="two" href="javascript:portal_user_register.start(2);" style="display: none;"><?php echo $goButtonText; ?></a>
			<span class="warning hidden">*Please enter your info.</span>
		</div>
	</div>
</div>