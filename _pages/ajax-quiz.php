<?php
namespace AH;
require_once(__DIR__.'/../_classes/ActivityHelper.class.php');
require_once(__DIR__.'/../_classes/QuizQuery.class.php'); 
require_once(__DIR__.'/../_classes/Utility.class.php'); 
require_once(__DIR__.'/../_classes/States.php'); 
require_once(__DIR__.'/../../../../wp-load.php');

global $listingKeyMap;
$listingKeyMap = [	"price"	=>"p",
					"beds"	=>"br",
					"baths"	=>"b",
					"id"	=>'i',
					"city_id"=>'ci',
					"percent"=>'pt',
					"percent_average"=>'pa',
					"image_path"=>'ip',
					"street_address"=>'sa',
					"title"=>'t',
					"country"=>'c',
					"interior"=>'in',
					"interior_std"=>'ins',
					"lotsize"=>'l',
					"lotsize_std"=>'ls',
					"flags"=>'f',
					"active"=>'a',
					"first_image"=>'fi',
					"list_image"=>'li',
					"lng"=>"lg",
					"lat"=>"lt",
					"tags"=>'tg'];

class AJAX_Quiz extends ActivityHelper {
	private $defaults = array(
		'show_inactive_listings' => true,
		'show_parser_listings' => false,
		'imgPath' => '/../_img/_listings/845x350'
	);

	public function __construct(){
		//$this->qq = new QuizQuery();
		$in = parent::__construct();

		$this->log = new Log(__DIR__.'/../_classes/_logs/ajax-quiz.log') ;
		$this->dumpJson = false;
		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>"QuizResultsDumpJson"]]);
		if (!empty($opt)) $this->dumpJson = is_true($opt[0]->value);
		$this->defaults['imgPath'] = realpath(__DIR__.'/../_img/_listings/845x350');

		try {
			if (empty($in->query)) throw new \Exception (__FILE__.':('.__LINE__.') - No query sent from ip:'.$this->userIP().", broswer: ".print_r(getBrowser(), true));	
			$this->log("ajax-quiz - for ".$in->query);		
			switch ($in->query){
					// called from quiz-results::processResuts()
					case 'record-portal-user-quiz':
						if (empty($in->data)) throw new \Exception('No data sent.');					
						// if (!isset($in->data['portalUser'])) throw new \Exception('No portalUser sent.');
						if (empty($in->data['quiz_id'])) throw new \Exception('No quiz_id sent.');

						$agentID = isset($in->data['agentID']) ? intval($in->data['agentID']) : 0;
						$portalUserID = isset($in->data['portalUser']) ? intval($in->data['portalUser']) : 0;
						$sessionID = isset($in->data['sessionID']) ? intval($in->data['sessionID']) : 0;
						$quiz_id = intval($in->data['quiz_id']);

						if (!empty($sessionID)) {
							$session = $this->getClass('Sessions')->get((object)['where'=>['id'=>$sessionID]]);
							if (!empty($session)) {
								$fields = [];
								if( $session[0]->quiz_id != $quiz_id) 
									$fields['quiz_id'] = $quiz_id;
								if (count($fields))
									$this->getClass('Sessions')->set([(object)['where'=>['id'=>$sessionID],
																			   'fields'=>$fields]]);
								$this->log("record-portal-user-quiz ".(count($fields) ? 'updated' : 'did not update')." the quiz_id of session:$sessionID");
								if ($portalUserID && $agentID) {
									if ($this->getClass('PortalUsersMgr')->updateSession($portalUserID, $agentID, $sessionID))
										$this->log("record-portal-user-quiz updated portalUserID:$portalUserID and agentID:$agentID for session:$sessionID");
								}
							}
						}

						if (!empty($portalUserID)) {
							$this->log("record-portal-user-quiz for portalUserID:$portalUserID, quiz_id:$quiz_id");
							try {
								$out = $this->getClass('PortalUsersMgr')->recordPortalUserQuiz($portalUserID, $quiz_id);
								$this->log("record-portal-user-quiz got back:".print_r($out, true));
							}
							catch(\Exception $e) {
								$this->log("record-portal-user-quiz got exception:".$e->getMessage());
								$out = new Out('fail', "Exception:".$e->getMessage());
							}
						}
						else
							$out = new Out('OK', 'No portalUser sent.');
						break;

					/* called from profile selection popup */
					case 'find-listing':
						global $usStates;
						if (!isset($in->data['address'])) throw new \Exception('No address data provided.');
						$address = trim($in->data['address']);
						$this->log("find-listing - address:$address");
						$pieces = explode(',', $address);
						$street_address = $pieces[0];
						$city = '';
						$state = '';
						if (count($pieces) > 1) {							
							$zone = explode(' ', $pieces[1]); // could be city and/or state
							$len = count($zone);
							if ($len) { // maybe have state?
								if ( strlen($zone[$len-1]) == 2) {
									if (in_array(strtoupper($zone[$len-1]), $usStates))
										$state = strtoupper($zone[$len-1]);
								}
								if (!empty($state)) 
									array_pop($zone);
								if (count($zone)) // still have something
									$city = trim( implode(' ',$zone) );
							}
						}

						$q = new \stdClass;
						$q->likeback = ['street_address'=>$street_address];
						if (!empty($city))
							$q->like2 = ['city'=>$city];
						if (!empty($state))
							$q->likeonlycase = ['state'=>$state];
						$q->what = ['id', 'street_address','city','state'];
						//$q->where = ['tier'=>1];
						$q->or = ['active'=>[1, 2, 4, 6]];

						$listings = $this->getClass('Listings', 1)->get($q);
						$out = new Out(!empty($listings) ? 'OK' : 'fail', $listings);
						break;

					case 'delete-profile':
						if (empty($in->data['sessionID'])) throw new \Exception('No session id provided.');
						if (!isset($in->data['info'])) throw new \Exception('No quiz data provided.');
						$profile = (object)$in->data['info'];

						$user = $this->getClass('Register')->getUserData(-1);
						// $s = $this->getClass('Register')->getSessions();
						$ses = $this->getClass('Sessions')->get((object)['where'=>['id'=>$in->data['sessionID']]]);
						if (empty($ses))
							throw new \Exception("Failed to find sessino with id:".$in->data['sessionID']);

						// get all sessions with the same id, can have different ip's
						$s = $this->getClass('Sessions')->get((object)['where'=>['session_id'=>$ses[0]->session_id]]);
						if (!empty($s) &&
							$user->status == 'OK') {
							
							foreach($s as $j=>$ses) {
								$data = (array)$ses->value;
								$needUpdate = false;
								$values = [];
								if (isset($data)) {
									foreach($data as $i=>$info) {
										$append = true;
										if ($info->type == SESSION_PROFILE_NAME) {
											if ($info->quizType == $profile->quizType &&
												$info->quizId == $profile->quizId &&
												$info->name == $profile->name) {
												if (!isset($info->owner) ||
													$info->owner == $user->data->user_id) {
													$needUpdate = true;
													$append = false;
												}
											}
										}
										if ($append)
											$values[] = $info;
										unset($i, $info);
									}
									if ($needUpdate) {
										$sessions = $this->getClass('Sessions');
										$sessions->set([(object)['where'=>['id'=>$ses->id],
																 'fields'=>['value'=>$values]]]);
									}
								}
								unset($values,$ses);
							}
						}
						$out = new Out('OK', $this->getProfiles($in->data['sessionID']));
						break;
					/* called from quiz-results */
					case 'update-city-listings':
						if (empty($in->data['sessionID'])) throw new \Exception('No session id provided.');
						if (!isset($in->data['quizId'])) throw new \Exception('No quiz id provided.'); // can be -1 or the actual row index from QuizActivity
						if (!isset($in->data['city_id'])) throw new \Exception('No city_id provided.');

						$city_id = $in->data['city_id'];
						$quizId = intval($in->data['quizId']);
						$activity = $this->getActivityBySessionID($in->data['sessionID'], $quizId); // this is 'id' from Sessions table now, not 'session_id' of QuizActivity!!
						if (empty($activity)) throw new \Exception('No activity found for session id.');
						$activity = array_pop($activity);

						$size = count($activity->data);
						if ($quizId == -1 ||
							$quizId >= $size)
							$last_quiz = array_pop($activity->data); // pop the last quiz
						else
							$last_quiz = $activity->data[$quizId];
							
						if (!isset($last_quiz->results)) throw new \Exception('no results');
						$r = array_pop($last_quiz->results); // pop the last result set
						if (empty($r)) throw new \Exception('no results');
						if (empty($r->city_sorted)) throw new \Exception('no cities sorted');
						if (!isset($r->city_sorted->$city_id)) throw new \Exception("city: $city_id not found in results");

						// $r->city_sorted = (array)$r->city_sorted; // just to be sure
						$city = $r->city_sorted->$city_id;
						$city->listings = (array)$city->listings; // just to be sure
						$listing_ids_to_get = array_keys($city->listings);
						$startId = isset($in->data['start_id']) ? intval($in->data['start_id']) : 0;
						$maxUpdateListingsPerCall = 200;
						$maxListingsPerCity = 200;

						$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'MaxUpdateListingsPerCall']]);
						if (!empty($opt))
							$maxUpdateListingsPerCall = intval($opt[0]->value);
						$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'MaxListingsPerCity']]);
						if (!empty($opt))
							$maxListingsPerCity = intval($opt[0]->value);

						$len = count($listing_ids_to_get);
						if ($len > ($startId + $maxUpdateListingsPerCall)) {
							$this->log("update-city-listings - key length:$len, startId:$startId, maxUpdateListingsPerCall:$maxUpdateListingsPerCall, truncating list");
							$listing_ids_to_get = array_slice($listing_ids_to_get, $startId, $maxUpdateListingsPerCall);
						}

						$query = (object) array(
							'where'=>array('id'=>$listing_ids_to_get),
							'what'=>array('id','active','city_id','title','street_address','country','interior','interior_std','lotsize','lotsize_std','flags')
						);

						// get listing geoinfo
						$Geoinfo = $this->getClass('ListingsGeoinfo')->get((object)array(
							'where'=>array('listing_id'=>$listing_ids_to_get),
							'what'=>array('id','listing_id','lat','lng'),
						));

						$Listings = $this->getClass('Listings')->get($query);
						// remove extra data from output
						if (empty($Listings)) return new Out('fail', "No listings in result for quizId:$quizId, cityId:$city_id");
						$this->log("getLastResult - Retrieved ".count($Listings)." listings from DB");

						if (!empty( $Geoinfo )) {
							$temp = array();
							foreach($Geoinfo as $geo) $temp[$geo->listing_id] = $geo;
							$Geoinfo = $temp;
							unset($temp);
						}

						if (!empty( $Listings )) {
							$temp = array();
							foreach($Listings as $l) $temp[$l->id] = $l;
							$Listings = $temp;
							unset($temp);
						}

						$results = [];
						foreach($Listings as &$listing) {
							$geo = isset($Geoinfo[$listing->id]) ? $Geoinfo[$listing->id] : null;	
							if ($geo != null &&
								$geo->lat != -1){
								$listing->lat = $geo->lat;
								$listing->lng = $geo->lng;
							}	
							else {
								// $r->cities = (array)$r->cities; // just to be sure
								$listing->lat = $r->cities->$city_id->lat;
								$listing->lng = $r->cities->$city_id->lng;
							}
						}

						$results = [];
						$index = 0;
						foreach($city->listings as $db_listing) { // maintain order, since these are sorted by percent/percent_average
							if ($index++ < $startId)
								continue;

							if ($index > ($startId+$maxUpdateListingsPerCall))
								break;
							
							$newListing = [];
							global $listingKeyMap;
							// get data from quiz result's listing list
							foreach($db_listing as $key=>$value) {
								$newListing[ $listingKeyMap[$key] ] = $value;
								unset($value);
							}
							// remap keys to abbreviated form from extra data we need for a full listing data set
							if ( isset($Listings[$db_listing->id]) ) foreach($Listings[$db_listing->id] as $key=>$value) {
								$newListing[ $listingKeyMap[$key] ] = $value;
								unset($value);
							}
							$results[] = $newListing; // disassociate
							unset($db_listing);
						}

						$out = new Out('OK', ['results'=>$results,
											  'nextId'=>$startId+$maxUpdateListingsPerCall,
											  'id_length'=>$len]);

						break;

					case 'update-results-analytics':
						if (empty($in->data['sessionID'])) throw new \Exception('No session id provided.');
						if (!isset($in->data['quizId'])) throw new \Exception('No quiz id provided.');

						$this->exitNow("update-results-analytics is running");

						$this->log("update-results-analytics - released connection");

						// $quizId = intval($in->data['quizId']);
						$quizId = (int)$in->data['quizId'];
						$activity = $this->getActivityBySessionID($in->data['sessionID'], $quizId); // this is 'id' from Sessions table now, not 'session_id' of QuizActivity!!
						if (empty($activity)) throw new \Exception('No activity found for session id.');
						$activity = array_pop($activity);

						$size = count($activity->data);
						if ($quizId == -1 ||
							$quizId >= $size)
							$last_quiz = array_pop($activity->data); // pop the last quiz
						else
							$last_quiz = $activity->data[$quizId];
							
						if (!isset($last_quiz->results)) throw new \Exception('no results');
						$r = array_pop($last_quiz->results); // pop the last result set
						if (empty($r)) throw new \Exception('no results');

						$qq = $this->getClass('QuizQuery');
						$ListingsTags = $this->getClass('ListingsTags');
						$CitiesTags = $this->getClass('CitiesTags');
						$time = microtime(true);
						// use external counter, had one failure where $i wasn't starting at zero
						$sql = '';
						$listingCount = 0;
						if (isset($r->listings) &&
							!empty($r->listings)) foreach($r->listings as $row) {
							$i = 0;
							$sql = '(';
							if (!empty($row->tags)) foreach ($row->tags as $v){
								$sql .= ($i>0 ? ' OR ': '' ) . "`tag_id` = $v";
								unset($v);
								$i++;
							}
							$sql.= ') AND `listing_id` = '.$row->id;
							if ($i != 0)
								$ListingsTags->rawQuery("UPDATE ".$ListingsTags->getTableName($ListingsTags->table)." SET `clicks` = `clicks` + 1 WHERE $sql");
							unset($row);
							$listingCount++;
						}

						$qq->add((object)array('session_id'=>$in->data['sessionID'],
											   'type'=>'post-listing-metric',
											   'data'=>json_encode(['memory usage'=>memory_get_usage(),
											   						'listing count'=>$listingCount])));

						// use external counter, had one failure where $i wasn't starting at zero: cities is an associative array!
						$cityCount = 0;
						if (!empty($r->cities)) foreach($r->cities as $row) {
							if (empty($row) ||
								empty($row->tags))
								continue;
							$i = 0;
							$sql = '(';
							foreach( array_keys((array)$row->tags) as $v) {
								$sql .= ($i>0 ? ' OR ': '' ) . "`tag_id` = $v";
								unset($v);
								$i++;
							}
							$sql.= ') AND `city_id` = '.$row->id;
							if ($i != 0)
								$CitiesTags->rawQuery("UPDATE ".$CitiesTags->getTableName($CitiesTags->table)." SET `clicks` = `clicks` + 1 WHERE $sql");
							$cityCount++;
							unset($row);
						}

						$qq->add((object)array('session_id'=>$in->data['sessionID'],
											   'type'=>'post-cities-metric',
											   'data'=>json_encode(['memory usage'=>memory_get_usage(),
											   						'city count'=>$cityCount])));

						$out = new Out('OK', "Updated for results - ".$listingCount." listings and ".$cityCount." cities metrics.");

						die;
						// break;
					case 'update-analytics':
						if (!isset($in->data)) throw new \Exception('Data not provided.');
						// if (!isset($in->data['cities'])) throw new \Exception('No city ids provided.');
						// if (!isset($in->data['listings'])) throw new \Exception('No city ids provided.');

						$this->exitNow("update-analytics is running");
						
						$this->log("update-analytics - released connection");


						$cities = isset($in->data['cities']) ? $in->data['cities'] : [];
						$c = $this->getClass('Cities');
					
						$cityCount = 0;
						if (!empty($cities)) foreach ($cities as $city=>$v){
							$x = 0;
							if ($city > 0) {
								$x = $c->rawQuery("UPDATE ".$c->getTableName($c->table)." SET `impressions` = `impressions` + $v WHERE `id` = $city");
								if (!$x)
									$this->log("update-analytics - failed for city:$city - $v");
								else
									$cityCount++;
							}
							unset($v, $city);
						}

						// do listings
						$listings = isset($in->data['listings']) ? $in->data['listings'] : [];
						$this->log("update-analytics - entered for ".count($listings)." listings, ".count($cities)." cities");
						$l = $this->getClass('Listings');
						$sql = "UPDATE ".$l->getTableName($l->table)." SET `impressions` = `impressions` + 1 WHERE (";
						// use external counter, had one failure where $i wasn't starting at zero
						$i = 0;
						if (!empty($listings)) foreach ($listings as $v){
							if ($v != -1)
								$sql .= ($i>0 ? ' OR ': '' ) . "`id` = $v";
							unset($v);
							$i++;
						}
						$sql.= ')';
						$y = 1; 
						if (count($listings))
							$y = $l->rawQuery($sql);

						$this->log("update-analytics - completed cityCount:".$cityCount.", y:".(count($listings) ? print_r($y, true) : '0'));

						$out = new Out( $cityCount && $y ? 'OK' : 'fail', $x ? "Updated ".count($cities)." city and ".count($listings)." listings impressions analytics" : "Failed to update cities impressions analytics");
						die;
						// break;
					case 'get-profiles':
						if (!isset($in->data)) throw new \Exception('City data not provided.');
						if (!isset($in->data['sessionID'])) throw new \Exception('No session id provided.');
						$sessionId = $in->data['sessionID'];
						$this->log("get-profiles entered");
						$profiles = $this->getProfiles($sessionId);
						$out = new Out($profiles == false ? 'fail' : 'OK', $profiles == false ? "Did not find session with id: ".$sessionId : $profiles);
						$this->log("get-profiles exiting");
						break;
					/* called from quiz-results */
					case 'save-profile':
						if (!isset($in->data)) throw new \Exception('City data not provided.');
						if (!isset($in->data['sessionID'])) throw new \Exception('No session id provided.');
						if (!isset($in->data['profile_name'])) throw new \Exception('Profile name not provided.');
						if (!isset($in->data['quizId'])) throw new \Exception('Quiz Id not provided.'); // this is now the row index from QuizActivity

						$quizId = intval($in->data['quizId']); // this is 'id' from QuizActivity table, not nth data element from QA
						$sessionId = $in->data['sessionID']; // this is 'id' from Sessions table now, not 'session_id' of QuizActivity!!
						$name = $in->data['profile_name'];
						$user = wp_get_current_user();

						$gotOne = false;
						$activeSes = null;
						$out = null;
						// $sessions = $this->getClass('Register')->getSessions();
						$this->getClass('Register');
						$sessionForID = $this->getClass('Sessions')->get((object)['where'=>['id'=>$sessionId]]); // this is 'id' from Sessions table now, not 'session_id' of QuizActivity!!
						if (empty($sessionForID)) {
							$out = new Out('fail', "No session found with id:".$sessionId);
							break;
						}
						$sessionForID = array_pop($sessionForID);

						$sessions = $this->getClass('Sessions')->get((object)['where'=>['session_id'=>$sessionForID->session_id]]); // get all session rows with same session_id
						if (empty($sessions)) {
							$out = new Out('fail', "No sessions found!!");
							break;
						}
					
						$activity = $this->getActivityBySession($sessionForID); // use the main session's quiz_id to find the active QuizActivity
						if (empty($activity)) {
							// then check to see if the quizId (row index of QuizActivity exists)
							$activity = $this->getClass('QuizActivity')->get((object)['where'=>['id'=>$quizId]]);
							if (empty($activity))
								throw new Exception('No activity found for session id:'.$sessionId);
							else
								$this->log("save-profile found QuizActivity using row index:$quizId, and not from session id:$sessionId");
						}
						$activity = array_pop($activity);

						$size = count($activity->data);
						$last_quiz = $size ? $activity->data[$size-1] : null; // pop the last quiz
						// $quizId = $quizId == -1 ? ($size ? $size - 1 : null) : $quizId;
						// if ($quizId == -1 ||
						// 	$quizId >= $size)
						// 	$last_quiz = $activity->data[$size-1]; // pop the last quiz
						// elseif ($quizId !== null)
						// 	$last_quiz = $activity->data[$quizId];	
												
						$this->log('save-profile - id:'.$sessionId.', session count: '.count($sessions).' last_quiz:'.!empty($last_quiz).', quizId:'.$quizId.", userId:".(!empty($user) ? $user->ID : "N/A"));
						if ($last_quiz !== null) {
							// if (isset($s[0]->user_id) &&
							// 	$s[0]->user_id != 0) { // then grab all the sessions owned by this user
							// 	$s = $this->getClass('Sessions')->get((object)array('where'=>array('user_id'=>$s[0]->user_id)));
							// }
							foreach($sessions as $ses) {
								if ($out && $out->status == 'fail')
									break;
								$values = [];
								
								$sameQuiz = null;
								$sameQuizIndex = null;
								$needUpdate = false;
								if (isset($ses->value)) {
									$data = (array)$ses->value;
									foreach($data as $i=>$info) {
										$saveIt = true;
										if ($info->type == SESSION_PROFILE_NAME) {
											if ($info->name == $name) {
												if ($info->quizId != $quizId) {
													$sameQuizIndex = $i;
													$sameQuiz = $info;
													$saveIt = false;
													$needUpdate = true;
													$this->log('save-profile - id:'.$sessionId.", found same name:$name, different quizId - existing:$info->quizId, new:$quizId");
												}
												else {
													$sameQuiz = $info;
													$this->log('save-profile - id:'.$sessionId.", found same name:$name, same quizId - $quizId");
												}
											}
											
										}
										if ($saveIt)
											$values[] = $info;
										unset($i, $info);
									}
								}

								if ($sameQuiz === null) {
									$info = new \stdClass();
									$info->type = SESSION_PROFILE_NAME;
									$info->owner = $user->ID;
									$info->name = $name;
									$info->quizId = $quizId;
									$info->session_id = $sessionForID->session_id;
									$info->activeSessionID = $sessionForID->id;
									$info->quizType = $last_quiz->quiz;
									$info->locations = isset($last_quiz->location) ? $last_quiz->location : [];
									$info->distance = isset($last_quiz->distance) ? $last_quiz->distance : 0;
									$info->state = $last_quiz->state;
									$values[] = $info;
									$needUpdate = true;
									$this->log('save-profile - id:'.$sessionId.", created new SESSION_PROFILE_NAME with name:$name, quizId:$quizId, type:$last_quiz->quiz");
								}
								else if ($needUpdate) {
									$data[$sameQuizIndex]->owner = $user->ID;
									$data[$sameQuizIndex]->name = $name;
									$data[$sameQuizIndex]->quizId = $quizId;
									$data[$sameQuizIndex]->session_id = $sessionForID->session_id;
									$data[$sameQuizIndex]->activeSessionID = $sessionForID->id;
									$data[$sameQuizIndex]->quizType = $last_quiz->quiz;
									$data[$sameQuizIndex]->locations = $last_quiz->location;
									$data[$sameQuizIndex]->distance = $last_quiz->distance;
									$data[$sameQuizIndex]->state = $last_quiz->state;
									$values = $data;
									$this->log('save-profile - id:'.$sessionId.", updated existing SESSION_PROFILE_NAME with name:$name, quizId:$quizId, type:$last_quiz->quiz");
								}									
								
								if ($needUpdate) {
									$this->log('save-profile - about to update ses:'.$ses->id);
									$this->getClass('Sessions')->set([(object)[
											'where'=>['id'=>$ses->id],
											'fields'=>[ 'value'=>$values ]]]);
								}
								else
									$this->log('save-profile - no need to update ses:'.$ses->id);
								unset($values, $ses);
							}
							$profiles = $this->getProfiles($sessionId);
							$this->log('save-profile - returning '.count($profiles).' profiles');
							$out = new Out('OK', $profiles);
						}
						else
							$out = new Out('fail', "Did not find session with id: $sessionId, quiz id:$quizId");
						break;
					/* called from home page or ahjs */
					case 'find-city':
						if (empty($in->data)) throw new \Exception('City data not provided.');
						if (empty($in->data['city'])) throw new \Exception('City info not provided.');
						$city = $in->data['city'];
						// $distance = isset($in->data['distance']) ? intval($in->data['distance']) : 0;
						$distance = isset($in->data['distance']) ? (int)$in->data['distance'] : 0;
						$state = '';
						global $usStates;
						if (strpos($city, ",") !== false) {
							$city = explode(',', $city);
							$state = strtoupper( trim($city[1]) );
							$city = $city[0];
							if (!in_array($state, $usStates))
								if (array_key_exists ( ucwords(trim($city[1])), $usStates ))
									$state = $usStates[ucwords(trim($city[1]))];
						}

						$city = formatName($city);
						$cities = null;
						$Cities = $this->getClass('Cities');
						$q = new \stdClass();
						$doingStateWide = false;

						if (empty($state)) {
							if (in_array(strtoupper($city), $usStates))
								$city = strtoupper($city);
							elseif (array_key_exists(ucwords($city), $usStates)) // convert it to 2-char state for processing below
								$city = $usStates[ucwords($city)];
						}

						if (empty($state) &&
							strlen($city) == 2) {// this must actually be a state only!!

							$state = strtoupper($city);
							$q->where = array('state'=>$state);
							$x = $Cities->get($q);
							$doingStateWide = !empty($x);
						}
						else {
							if (!empty($state)) $q->where = array('state' => $state);
							isset($q->where) ? $q->where['city'] = $city : $q->where = array('city'=>$city);
							$x = $Cities->get($q);
						}

						if ($doingStateWide) {
							$out = new Out('OK', (object)['city'=>[$state],
												   		  'allState'=> $doingStateWide]);
							break;
						}

						require_once(__DIR__.'/../_classes/GoogleLocation.class.php');
						$GoogleLocation = new GoogleLocation();
						if (!empty($x)) {
							foreach($x as $i=>$city) {
								if (!$doingStateWide) {
									if ( (empty($city->lat)  && empty($city->lng)) ||
										 ($city->lat == -1 && $city->lng == -1) ||
										 ($city->lat == '-1' && $city->lng == '-1') ) {
										$result = $google = null;
										$lng = $lat = -1;
										$city->lat = null; $city->lng = null;
										if ($Cities->geocodeCity($GoogleLocation, $city, $lng, $lat, $result, $google)) {
											unset($q->where);
											$city->lng = $lng;
											$city->lat = $lat;
											$q->where = array('id'=>$city->id);
											$q->fields = array('lng'=>$city->lng,
																'lat'=>$city->lat);
											$a = array($q);
											$Cities->set($a);
										}
										else
											unset($x[$i]);
									}
								}
								else if ( ($city->lat == null && $city->lng == null) ||
										 ($city->lat == -1 && $city->lng == -1) ||
										 ($city->lat == '-1' && $city->lng == '-1') ) {
									unset($x[$i]);
								}
							}
							$cities = !empty($x) ? $x : null;
						}
						else { // go find it
							if (!empty($state) &&
								!$doingStateWide ) {
								$city = (object)array('city'=>$city,
													  'state'=>$state,
													  'country'=>'US',
													  'lng'=>0,
													  'lat'=>0);
								$lng = $lat = 0;
								if ($Cities->geocodeCity($GoogleLocation, $city, $lng, $lat, $result, $google)) { 
									// wow!  found one!
									$city->lng = $lng;
									$city->lat = $lat;
									$x = $Cities->add($city);
									if ($x) {
										$city->id = $x;
										$out = new Out('OK', array($city));
									}
									$cities = [$city];
								}
							}
						}
						if (!$cities)
							$out = new Out('fail', "Could not find : ".$in->data['city']." Please check spelling.");
						else {
							if (!$doingStateWide) {
								$Listings = $this->getClass('Listings');
								$count = 0;
								$type = 'COUNT(*)';
								// if $distance == 0, set it to six.  Much faster searh than using city/state
								$distance = $distance ? $distance : 6; 
								$q = new \stdClass();
								if ($distance == 0) {
									foreach($cities as $i=>$city) {
										!empty($state) ? $q->where = array('state'=>$state,
																			'city'=>$cities[$i]->city) : $q->where = array('city'=>$cities[$i]->city);
										$q->where['active'] = 1;
										$count = $Listings->count($q);
										// $count = !empty($count) ? (is_int($count) ? $count : (is_numeric($count) ? intval($count) : (is_array($count) ? $count[0]->$type : 0))) : 0;
										$count = !empty($count) ? (is_int($count) ? $count : (is_numeric($count) ? (int)$count : (is_array($count) ? $count[0]->$type : 0))) : 0;
										if (!$count)
											unset($cities->$i);
									}
								}
								else {
									require_once(__DIR__.'/../_classes/DistanceScale.php');
									foreach($cities as $i=>$city) {
										// $lat = (int)(abs( is_float($city->lat) ? $city->lat : floatval($city->lat) ) / 10);
										$lat = (int)(abs( is_float($city->lat) ? $city->lat : (float)$city->lat ) / 10);
										$deltaLng = $distance / $DistanceScale[$lat][0];
										$deltaLat = $distance / $DistanceScale[$lat][1]; // latitude remains pretty constant at about 60miles/degree
										
										$lg = $this->getClass("ListingsGeoinfo");
										$sql = "SELECT COUNT(*) FROM ".$Listings->getTableName($Listings->table)." AS a INNER JOIN ".$lg->getTableName($lg->table)." AS b ON b.listing_id = a.id 
												WHERE b.lng >= ".($city->lng - $deltaLng)." AND b.lng <= ".($city->lng + $deltaLng)." 
												AND b.lat >= ".($city->lat - $deltaLat)." AND b.lat <= ".($city->lat + $deltaLat)." AND a.active = 1";
										$count = $Listings->rawQuery($sql);
										// $count = !empty($count) ? (is_int($count) ? $count : (is_numeric($count) ? intval($count) : (is_array($count) ? $count[0]->$type : 0))) : 0;
										$count = !empty($count) ? (is_int($count) ? $count : (is_numeric($count) ? (int)$count : (is_array($count) ? $count[0]->$type : 0))) : 0;
										if (!$count)
											unset($cities->$i);
									}
								}
							}
							if (!empty($cities)) {
								usort($cities,function($a,$b){ return strcmp($a->city, $b->city); });
								$out = new Out('OK', (object)['city'=>$cities,
													  		  'allState'=> $doingStateWide]);
							}
							else if ( (isset($in->data['distance']) ? intval($in->data['distance']) : 0) == 0)
								$out = new Out('fail', "Currently, ".$city->city.", ".$city->state." has no listings designated.  Try expanding the search.");
							else 
								$out = new Out('fail', "There are no listings within $distance miles of ".$city->city.", ".$city->state.($distance < 120 ? " Try expanding the search." : " Try searching for all homes."));
						}
						break;

				/* Called from quiz */
					case 'get-status':
						if (empty($in->data['sessionID'])) {
							$this->log("get-status - no sssionID");
							throw new \Exception('No session id provided.');
						}
						$newSession = '';
						$newSessionId = 0;
						$q = $this->getActivityBySessionID($in->data['sessionID'], 0, $newSession, $newSessionId);
						if (empty($q)) {
							$this->log("get-status no quiz for {$in->data['sessionID']}");
							$out = new Out('OK', array('response'=>'null'));
							//$out = new Out('OK', array('quiz'=>-1));
							break;
						}
						else {
							$q = array_pop($q); 
							if (empty($q)) {
								$this->log("get-status failed to get quiz for {$in->data['sessionID']}");
								throw new \Exception('Empty activity set.');
							}
							$this->log('get-status found:'.$q->status.' for session: '.$in->data['sessionID']);
							switch($q->status){
								case 'processing': 
								case 'done': 
									$data = ['response'=>$q->status];
									if (!empty($newSession) &&
										!empty($newSessionId))
										$data['sessionData'] = ['session_id'=>$newSessionId,
														   		'session'=>$newSession];	 
									$out = new Out('OK', $data); 
									break;
								case 'started':
									$a = $q->data[(count($q->data) > 0 ? count($q->data)-1 : 0)];

									$qDB = $this->getClass('QuizQuestions')->get( (object) array('orderby'=>'orderby', 
																								 'where'=>array('quiz'=>array($a->quiz,2) ),
																								 'bitand'=>['enabled'=>1]) );
									foreach ($qDB as $question)
										// if (intval($question->enabled) == 1)
										if ((int)$question->enabled == 1)
											$questions[] = $question;

									// usort($questions, function($a,$b){ return intval($a->orderby)-intval($b->orderby); });
									usort($questions, function($a,$b){ return (int)$a->orderby - (int)$b->orderby; });
									unset($id);
									$act_index = count($a->actions) > 0 ? count($a->actions)-1 : 0;
									foreach ($questions as $i=>&$question) {
										if ( (is_array($a->actions) && isset($a->actions[$act_index]->next) && $question->id == $a->actions[$act_index]->next->id) ||
											 (!is_array($a->actions) && isset($a->actions->$act_index->next) && $question->id == $a->actions->$act_index->next->id) )
											{ $id = $i; break; }
									}
									$id = (isset($id) ? $id : 0);
									$data = ['response'=>($id > 0 ? 'started' : 'new'),
											 'quizPosition'=>['question_id'=>$id,'quiz'=>$a->quiz]];
									if (!empty($newSession) &&
										!empty($newSessionId))
										$data['sessionData'] = ['session_id'=>$newSessionId,
														   		'session'=>$newSession];	 
									$out = new Out('OK', $data); // quiz value here is what type of quiz we are doing, not quizId...
									break;
								default: $out = new Out('OK', array('response'=>'null')); break;
							}
						}
						break;
					case 'get-quiz':
						if (empty($in->data['quiz']) && $in->data['quiz'] !== '0') throw new \Exception('No quiz provided.');
						$Questions = (array) $this->getClass('QuizQuestions', 1)->get((object) array('orderby'=>'orderby', 
																								  'where'=>array('quiz'=>array($in->data['quiz'],2)),
																								  'bitand'=>['enabled'=>1] ));
						if (empty($Questions)) throw new \Exception('No questions returned');
						$Slides = $this->getClass('QuizSlides')->get((object)array('orderby'=>'question_id'));
						$SlideTags = $this->getClass('QuizTags')->get((object)[ 'orderby' => 'slide_id']);
						$Tags = $this->getClass('Tags')->get();

						$this->buildQuestions($Questions, $Slides, $SlideTags, $Tags);

						$this->log("get-quiz called for quiz:{$in->data['quiz']}");
						
						$out = new Out('OK', $Questions);
						break;

					case 'save-all-actions':
						// save-quiz-action must have been called with 'start' to create an instance of QuizActivity already for this sessionID
						if (empty($in->data['stack']) && $in->data['stack'] !== '0') {
							$this->log('save-all-actions - No stack provided.');
							throw new \Exception('No stack provided.');
						}
						if (empty($in->data['sessionID'])) { // this is the row index to Sessions table
							$this->log('save-all-actions - No session id provided.');
							throw new \Exception('No session id provided.');
						}
						// $presetProcessing = isset($in->data['presetProcessing']) ? intval($in->data['presetProcessing']) : 0;
						$presetProcessing = isset($in->data['presetProcessing']) ? (int)$in->data['presetProcessing'] : 0;
						$filter = isset($in->data['filter']) ? (int)$in->data['filter'] : 0;

						$stack = $in->data['stack'];
						if (count($stack) == 0) {
							$this->log('save-all-actions - Empty stack provided.');
							throw new \Exception('Empty stack provided.');
						}
						$price = isset($in->data['price']) ? $in->data['price'] : [500000, -1];
						$homeOptions = isset($in->data['homeOptions']) ? $in->data['homeOptions'] : ['beds'=>0, 'baths'=>0];

						$this->log('save-all-actions - entered session:'.$in->data['sessionID'].", presetProcessing:$presetProcessing, stack of ".count($stack));

						foreach($stack as $quiz)
							$quizType = $quiz['quiz'] == (int)$quiz['quiz'] % 2;// even == city, odd == nationwide or statewide
							// $quizType = $quiz['quiz'] == intval($quiz['quiz']) % 2;// even == city, odd == nationwide or statewide

						$Questions = (array) $this->getClass('QuizQuestions')->get((object)[ 'orderby'=>'orderby', 
																							 'where' => [ 'quiz' => [$quizType,2] ],
																							 'bitand' => ['enabled'=>1] ]);
						if (empty($Questions)) throw new \Exception('No questions returned');
						$Slides = $this->getClass('QuizSlides')->get((object)[ 'orderby' => 'question_id'] );
						$SlideTags = $this->getClass('QuizTags')->get((object)[ 'orderby' => 'slide_id'] );
						$Tags = $this->getClass('Tags')->get();

						// sessionID is now the 'id' column of Sessions table
						$a = $this->getActivityBySessionID($in->data['sessionID']);
						if ( !empty($a) ){
							$a = array_pop($a);
							if (empty($a)) {
								$this->log('save-all-actions - Empty activity set.');
								throw new \Exception('Empty activity set.');
							}
							$a->data = (array) $a->data;
							
							$this->buildQuestions($Questions, $Slides, $SlideTags, $Tags);

							$index = count($a->data) > 0 ? count($a->data)-1 : 0;
							$x = (array) $a->data[$index]->actions;
							$this->log('save-all-actions -  starting out with '.count($x)." actions for activity");
							$gotDone = false;
							foreach($stack as $quiz) {	
								$quiz = (object)$quiz;	
								if ($quiz->action == 'done')
									$gotDone = true;					
								$y = array('time'=>$quiz->time, 'action'=>$quiz->action);
								if (isset($quiz->selected)) $y['selected'] = &$quiz->selected;
								if (isset($quiz->next)) foreach ($Questions as &$q) if ($q->id == $quiz->next) { $y['next'] = $q; break; }
								//array_push($x, $y);	
								$x[] = $y;							
								$this->log("time:".$y['time'].", action:".$y['action'].", selected:".(isset($y['selected']) ? print_r($y['selected'], true) : "N/A") );
								unset($y, $quiz);
							}
							$a->data[$index]->updated = date("Y-m-d H:i:s");
							$a->data[$index]->actions = $x;
							$a->data[$index]->price = $price;
							$a->data[$index]->homeOptions = $homeOptions;
							$a->data[$index]->filter = $filter;

							if ( !$this->QuizActivity->set(array( (object) array(
										'fields'=>[	'data'=>$a->data,
													'status'=>($gotDone ? ($presetProcessing ? 'pending' : 'done'): 'started')],
										'where'=>array(	'id'=>$a->id,
														'session_id'=>$in->data['sessionID'])
									) )) ) {
								$this->log('save-all-actions - Unable to save quiz action.');
								throw new \Exception('Unable to save quiz action.');
							}
							$this->log('save-all-actions - completed session:'.$in->data['sessionID'].", for quiz_id:$a->id");
							$out = new Out('OK');
						}
						else {
							$this->log('save-all-actions - No activity found.');
							throw new \Exception('No activity found for sessionID:'.$in->data['sessionID']);
						}
						break;

					case 'run-quiz':
						if (empty($in->data['quiz']) && $in->data['quiz'] !== '0') throw new \Exception('No quiz provided.');
						if (empty($in->data['sessionID'])) throw new \Exception('No session id provided.');

						$this->log('run-quiz - entered session:'.$in->data['sessionID']);

						$session_id = $in->data['sessionID'];
						$ses = $this->getClass('Sessions')->get((object)['where'=>['id'=>$session_id]]);
						if (empty($ses)) {
							$this->log("run-quiz - Failed to find session id:$session_id");
							$out = new Out('fail', "Failed to find session id:$session_id");
							break;
						}
						$ses = array_pop($ses);
						$quiz_id = intval($in->data['quiz']);
						if ($quiz_id == -1) {
							if (empty($ses->quiz_id)) {
								$this->log("run-quiz - Cannot find any quiz for session id:$session_id");
								$out = new Out('fail', 'Cannot find any quiz for session');
								break;
							}
							$quiz_id = $ses->quiz_id;
						}

						$activity = $this->getClass('QuizActivity')->get((object)['where'=>['id'=>$quiz_id]] ); //[0];
						if (empty($activity)) {
							$this->log("run-quiz - Cannot find quiz with id:$quiz_id for session id:$session_id");
							$out = new Out('fail', "Cannot find quiz with id:$quiz_id");
							break;
						}

						$this->exitNow("processing quiz for session:".$in->data['sessionID']);
						// close current session
						if (session_id()) session_write_close();
						
						// sleep(1); // maybe help?

						// $session_id = intval($in->data['sessionID']);
						$session_id = (int)$in->data['sessionID'];
						$this->log("Calling Quiz::parseQuizActions for session:".$in->data['sessionID']);
						$results = $this->getClass('Quiz')->parseQuizActions($session_id == -1 ? null : $session_id, $in->data['quiz']); // 'quiz' should be -1 here always...
						$this->log("Quiz::parseQuizActions completed for session:".$in->data['sessionID'].", quizId:".$in->data['quiz']);
						// $out = new Out('OK', $results);						

						$this->log('run-quiz - completed session:'.$in->data['sessionID']);
						die;
						// break;

					case 'save-quiz-action':
						if (empty($in->data['quiz']) && $in->data['quiz'] !== '0') throw new \Exception('No quiz provided.');
						if (empty($in->data['sessionID'])) throw new \Exception('No session id provided.');

						// $this->log('save-quiz-action - '.$in->data['action'].', entered session:'.$in->data['sessionID']);
						$this->log('save-quiz-action - ');
						foreach($in->data as $index=>$value)
							$this->log(" $index: ".(is_array($value) ? print_r($value, true) : $value));

						$ses = $this->getClass('Sessions')->get((object)['where'=>['id'=>$in->data['sessionID']]]);
						if (empty($ses))
							throw new \Exception("Failed to find session with id:".$in->data['sessionID']);
						$ses = array_pop($ses);

						// $quizType = $in->data['quiz'] == intval($in->data['quiz']) % 2; // even == city, odd == nationwide or statewide
						$quizType = $in->data['quiz'] == (int)$in->data['quiz'] % 2; // even == city, odd == nationwide or statewide

						if ($in->data['action'] != 'done') {
							$Questions = (array) $this->getClass('QuizQuestions')->get((object)[ 'orderby'=>'orderby', 
																								 'where' => [ 'quiz' => [$quizType,2] ],
																								 'bitand' => ['enabled'=>1] ]);
							if (empty($Questions)) throw new \Exception('No questions returned');
							$Slides = $this->getClass('QuizSlides')->get((object)[ 'orderby' => 'question_id'] );
							$SlideTags = $this->getClass('QuizTags')->get((object)[ 'orderby' => 'slide_id'] );
							$Tags = $this->getClass('Tags')->get();
						}
						
						$a = $this->getActivityBySessionID($in->data['sessionID']);
						$filter = isset($in->data['filter']) ? (int)$in->data['filter'] : 0;
						// if 'start', then always create a new QuizActivity from now on, so that the QA doesn't
						// become larger and larger...
						if ( !empty($a) &&
							 $in->data['action'] != 'start' ) {
							$this->log('save-quiz-action - start get activity at '.date("Y-m-d H:i:s"));
							
							// if (count($a) < 1) throw new \Exception('No activity found.');
							$a = array_pop($a);
							if (empty($a)) throw new \Exception('Empty activity set.');
							$a->data = (array) $a->data;
							if (isset($in->data['next']) ) 
								$this->buildQuestions($Questions, $Slides, $SlideTags, $Tags);
							$this->log('save-quiz-action - end get activity session:'.$in->data['sessionID']);
							switch($in->data['action']){
								// case 'start':
								// 	$a->data[] = [
								// 		'quiz' => $in->data['quiz'],
								// 		'created' => date("Y-m-d H:i:s"),
								// 		'updated' => date("Y-m-d H:i:s"),
								// 		'location'=> isset($in->data['location']) ? (is_array($in->data['location']) ? $in->data['location'] : [$in->data['location']]) : null,
								// 		'distance'=> isset($in->data['distance']) ? $in->data['distance'] : 0,
								// 		'actions' => [
								// 			[ 'time'=>date("Y-m-d H:i:s"), 'start' => 1 ],
								// 			[ 'time'=>date("Y-m-d H:i:s"), 'get' => $Questions[0] ]
								// 		],
								// 		'state' => isset($in->data['state']) && $in->data['quiz'] == '5' ? $in->data['state'] : ''
								// 	];
								// 	if ( !$this->getClass('QuizActivity')->set([ (object) [ 'fields'=> [ 'status'=>'started', 'data'=>$a->data ], 
								// 															'where' => [ 'session_id'=>$in->data['sessionID'] ] ]]) )
								// 		throw new \Exception('Unable to save quiz action.');
								// 	$this->log("save-quiz-action - @1 type: {$in->data['quiz']}, location:".(isset($in->data['location']) ? (is_array($in->data['location']) ? print_r($in->data['location'], true) : $in->data['location']) : "N/A").", distance:".(isset($in->data['distance']) ? $in->data['distance'] : 0).", state:".(isset($in->data['state']) && $in->data['quiz'] == '5' ? $in->data['state'] : 'N/A') );
								// 	$out = new Out('OK');
								// 	break;
								default:
									$index = count($a->data) > 0 ? count($a->data)-1 : 0; // should be count of 1 always now..
									$x = (array) $a->data[$index]->actions;
									$y = array('time'=>date("Y-m-d H:i:s"), 'action'=>&$in->data['action']);
									if (isset($in->data['selected'])) $y['selected'] = &$in->data['selected'];
									if (isset($in->data['next'])) foreach ($Questions as &$q) if ($q->id == $in->data['next']) { $y['next'] = $q; break; }
									array_push($x, $y);
									$a->data[$index]->updated = date("Y-m-d H:i:s");
									$a->data[$index]->actions = $x;
									$a->data[$index]->filter = $filter;

									$this->log('save-quiz-action - start save activity session:'.$in->data['sessionID']);

									if ( !$this->QuizActivity->set(array( (object) array(
										'fields'=>array('data'=>$a->data,
														'status'=>$in->data['action']=='done'?'done':'started'),
										'where'=>array(	'id'=>$a->id,
														'session_id'=>$in->data['sessionID'])
									) )) ) throw new \Exception('Unable to save quiz action.');

									$this->log("time:".$y['time'].", action:".$y['action'].", selected:".(isset($y['selected']) ? print_r($y['selected'], true) : "N/A") );
									$this->log('save-quiz-action - '.$in->data['action'].', completed session:'.$in->data['sessionID']);

									if ($in->data['action'] == 'done') {
										$this->exitNow("processing quiz for session:".$in->data['sessionID']);
										// $results = $this->getClass('Quiz')->parseQuizActions($in->data['sessionID']);
										$this->log("Calling Quiz::parseQuizActions for session:".$in->data['sessionID']);
										$results = $this->getClass('Quiz')->parseQuizActions($ses->id, $a->id);
										$this->log("Quiz::parseQuizActions completed for session:$ses->id, quiz:$a->id");
										$out = new Out('OK', $results);
									}
									else
										$out = new Out('OK');
									break;
							}
						} else {
							if ($in->data['action'] != 'start') throw new \Exception('no quiz created, must start one first');

							$portalAgent = isset($in->data['portalAgent']) ? $in->data['portalAgent'] : 0;
							$portalUser = isset($in->data['portalUser']) ? $in->data['portalUser'] : 0;
							$wpUser = isset($in->data['wpUser']) ? $in->data['wpUser'] : 0;

							if (!empty($wpUser)) {
								if (empty($portalAgent)) { // then check if current user is a portal agent...
									$user = get_userdata( intval($wpUser) );
									if (!empty($user)) {
										if (strpos($user->nickname, 'unknown') === false &&
											$user->nickname == $user->user_nicename) {
											$portalAgent = $user->ID;
											$this->log("save-quiz-action - setting portalAgent to $portalAgent, with portal:$user->nickname");
										}
									}
								}
							}

							$this->buildQuestions($Questions, $Slides, $SlideTags, $Tags);
							if (!($row = $this->getClass('QuizActivity')->add((object) array(
																							'session_id' => $ses->id,
																							'status' => 'started',
																							'portal_agent_id' => $portalAgent,
																							'portal_user_id' => $portalUser,
																							'wp_user_id' => $wpUser,
																							'data' => array(array(
																								'quiz'=>$in->data['quiz'],
																								'created'=>date("Y-m-d H:i:s"),
																								'updated'=>date("Y-m-d H:i:s"),
																								'location'=> isset($in->data['location']) ? (is_array($in->data['location']) ? $in->data['location'] : [$in->data['location']]) : null,
																								// 'location'=>isset($in->data['location']) ? $in->data['location'] : null,
																								'distance'=>isset($in->data['distance']) ? $in->data['distance'] : 0,
																								'actions'=>[
																										[ 'time'=>date("Y-m-d H:i:s"), 'start' => 1 ],
																										[ 'time'=>date("Y-m-d H:i:s"), 'get' => $Questions[0] ]
																								],
																								'state' => isset($in->data['state']) && $in->data['quiz'] == '5' ? $in->data['state'] : '' 
																							))
																						))) ) throw new \Exception('unable to save quiz action.');
							// update Session row with the new quiz_id value..
							// only one activity set per QuizActivity now...
							// $this->getClass('Sessions')->set([(object)['where'=>['id'=>$ses->id],
							// 										   'fields'=>['quiz_id'=>$row]]]);
							$this->log("save-quiz-action - @2 row:$row, type: {$in->data['quiz']}, location:".(isset($in->data['location']) ? (is_array($in->data['location']) ? print_r($in->data['location'], true) : $in->data['location']) : "N/A").", distance:".(isset($in->data['distance']) ? $in->data['distance'] : 0).", state:".(isset($in->data['state']) && $in->data['quiz'] == '5' ? $in->data['state'] : 'N/A') );
							$out = new Out('OK', $Questions[0]->id);
						}
						break;
				/* Called from quiz-results */
				/* deprecated */
					case 'get-last-result':
						// $out = new Out('OK', $this->getLastResult($in->data['sessionID'], -1));
						$quizId = isset($in->data['quiz']) ? intval($in->data['quiz']) : 0;
						$out = $this->getLastResult($in->data['sessionID'], $quizId);
						break;
					case 'stop-partial-run':
						if (empty($in->data['sessionID'])) throw new \Exception('No session id provided.');
						if (!isset($in->data['quizId'])) throw new \Exception('No quiz id provided.');
						$quizId = $in->data['quizId'];
						$this->exitNow("setting quiz:$quizId to done");

						while (true) {
							$activity = $this->getActivityBySessionID($in->data['sessionID'], $quizId);
							if (empty($activity)) {
								$this->log("stop-partial-run failed to find quiz:$quizId");
								die;
							}
							switch ($activity[0]->status) {
								case 'processing':
									usleep(1000);
									break;
								case 'partial':
									$this->getClass('QuizActivity')->set([(object)['where'=>['id'=>$activity[0]->id],
																	   			   'fields'=>['status'=>'done']]]);
									die;
								case 'done':
									die;
							}
						}
						break;

					case 'partial-quiz-run':
						if (empty($in->data['sessionID'])) throw new \Exception('No session id provided.');
						if (!isset($in->data['quizId'])) throw new \Exception('No quiz id provided.');
						$quizId = $in->data['quizId'];

						$this->log("partial-quiz-run for quizID:$quizId entered at ".date("Y-m-d H:i:s"));

						$activity = $this->getActivityBySessionID($in->data['sessionID'], $quizId);
						if (empty($activity)) {
							if ($quizId == -1)
								throw new Exception('No activity found for session id:'.$in->data['sessionID']);
							else 
								$activity = $this->getClass('QuizActivity')->get((object)['where'=>['id'=>$quizId]]);
							if (empty($activity))
								throw new Exception('No activity found for quiz id:'.$quizId);
						}
						$activity = array_pop($activity);
					
						$size = count($activity->data);
						if (!$size)
							throw new Exception('No previous results found for quiz id:'.$quizId);

						$last_quiz = array_pop($activity->data); // pop the last quiz
						$last_results = array_pop($last_quiz->results); // pop the last result set
						if ( isset($last_results->allowPartialQuizResults) &&
							 empty($last_results->allowPartialQuizResults)) {
							// put the activity back together...
							$last_quiz->results[] = $last_results;
							$activity->data[] = $last_quiz;
							$out = $this->getLastResult($in->data['sessionID'], $quizId, -1, $activity);
							break;
						}

						if ($activity->status != 'processing')
							$this->getClass('QuizActivity')->set([(object)['where'=>['id'=>$activity->id],
																		   'fields'=>['status'=>'processing']]]);

						// $last_results->tags = (array)$last_results->tags;
						// $last_results->price = (array)$last_results->price;
						// $last_results->homeOptions = (array)$last_results->homeOptions;
						// $last_results->cities = (array)$last_results->cities;
						// $last_results->city_sorted = (array)$last_results->city_sorted;

						$new_results = $this->getClass('Quiz')->parseAndReturnResultSet( $last_results );
						$this->log("partial-quiz-run for quizID:$quizId returned from parseAndReturnResultSet() at ".date("Y-m-d H:i:s"));
						$last_quiz->results[] = $new_results;
						$activity->data[] = $last_quiz;
						$status = $new_results->allowPartialQuizResults ? 'partial' : 'done'; // if partial, fe will need to call runParial()
						$this->getClass('QuizActivity')->set([(object)['where'=>['id'=>$activity->id],
																	   'fields'=>['status'=>$status,
																	   			  'data'=>$activity->data]]]);
						$out = $this->getLastResult($in->data['sessionID'], $quizId, -1, $activity);

						$this->log("partial-quiz-run for quizID:$quizId exit at ".date("Y-m-d H:i:s"));
						break;

					case 'new-result-from-query':
						// excepts tag array, sessionID, location array (array of city ids), and / or price array [min, max]
						// omit tag, location, or price and it will fetch from previous quiz result
						if (empty($in->data['sessionID'])) throw new \Exception('No session id provided.');
						if (!isset($in->data['quizId'])) throw new \Exception('No quiz id provided.');

						// $quizId = intval($in->data['quizId']);
						$quizId = (int)$in->data['quizId'];
						$new_query = false;
						foreach (['tags', 'location', 'price', 'distance', 'homeOptions','state','quiz'] as $field){
							if ( isset($in->data[$field]) ){
								$new_query = true;
							}
						}

						$this->log("new-result-from-query - sessionID:{$in->data['sessionID']}, quizId:$quizId, newQuery:$new_query, data:".print_r($in->data, true));
						if ($new_query){
							$activity = $this->getActivityBySessionID($in->data['sessionID'], $quizId);
							if (empty($activity)) {
								if ($quizId == -1)
									throw new Exception('No activity found for session id:'.$in->data['sessionID']);
								else 
									$activity = $this->getClass('QuizActivity')->get((object)['where'=>['id'=>$quizId]]);
								if (empty($activity))
									throw new Exception('No activity found for quiz id:'.$quizId);
							}
							$activity = array_pop($activity);
						
							$size = count($activity->data);
							if (!$size)
								throw new Exception('No previous results found for quiz id:'.$quizId);

							$last_quiz = $activity->data[ $size-1]; // pop the last quiz
							// if ($quizId == -1 ||
							// 	$quizId >= $size)
							// 	$last_quiz = $activity->data[$size-1]; // pop the last quiz
							// else
							// 	$last_quiz = $activity->data[$quizId];								

							$last_results = array_pop($last_quiz->results); // pop the last result set
							$last_quiz->results[] = $last_results; // re-add the popped results

							$new_results = clone $last_results;
							$new_quiz = clone $last_quiz;
							$new_results->quiz = $last_quiz->quiz;

							foreach (['tags', 'location', 'price', 'distance', 'sessionID','homeOptions','state','quiz','filter'] as $field)
								if ( isset($in->data[$field]) ){
									if ($field == 'tags' &&
										$in->data[$field] == 'notags')
										$new_results->$field = [];
									else
										$new_results->$field = $in->data[$field];
								}

							if ( intval($new_results->quiz) == 1 ) { // nationwide
								if ( isset($new_results->state) ) unset($new_results->state);
								if ( isset($new_results->location) ) unset($new_results->location);
								if ( isset($new_results->distance) ) unset($new_results->distance);
							}
							elseif ( intval($new_results->quiz) == 0 ) { // city
								if ( isset($new_results->state) ) unset($new_results->state);
							}
							elseif ( intval($new_results->quiz) == 5 ) { // state
								if ( isset($new_results->location) ) unset($new_results->location);
								if ( isset($new_results->distance) ) unset($new_results->distance);
							}

							$this->log("new-result-from-query - existing filter:".(isset($last_quiz->filter) ? $last_quiz->filter : 0).", incoming:".(isset($in->data['filter']) ? $in->data['filter'] : 0));
							if ( !isset($in->data['filter']) ) {
								$new_results->filter = isset($last_quiz->filter) ? $last_quiz->filter : 0;
							}

							if ( isset($new_results->allowPartialQuizResults) ) unset($new_results->allowPartialQuizResults);
							if ( isset($new_results->startCityCountUsedForListings) ) unset($new_results->startCityCountUsedForListings);
							if ( isset($new_results->currentMaxCityCountUsedForListings) ) unset($new_results->currentMaxCityCountUsedForListings);

							$new_results->tags = (array)$new_results->tags;
							$new_results->price = (array)$new_results->price;
							$new_results->homeOptions = (array)$new_results->homeOptions;
							$new_results->cities = (array)$new_results->cities;
							$new_results->city_sorted = (array)$new_results->city_sorted;

							// account for a city having muliple entries in Cities table for one reason or another.
							// So if the distance is zero, we won't find all the duplicate cities involved, so go
							// query for duplicate cities and expande the location array accordingly
							$cityLoc = [];
							// $quiz = gettype($new_results->quiz) == 'string' ? intval($new_results->quiz) : $new_results->quiz;
							$quiz = gettype($new_results->quiz) == 'string' ? (int)$new_results->quiz : $new_results->quiz;
							if (($quiz % 2) == 0 &&
								isset($new_results->location) &&
								empty($new_results->distance) ) {
								$loc = is_array($new_results->location) ? $new_results->location : [$new_results->location];
								foreach($loc as $id) {
									$city = $this->getClass('Cities')->get((object)['where'=>['id'=>$id]]);
									$cities = $this->getClass('Cities')->get((object)['where'=>['city'=>$city[0]->city,
																								'state'=>$city[0]->state]]);
									foreach($cities as $city)
										$cityLoc[$city->id] = $city->city;
								}
								$new_results->location = array_keys($cityLoc);
							}
							$this->log("new-result-from-query - running quiz type:$new_results->quiz");
							$new_quiz->quiz =$new_results->quiz;
							$new_results = $this->getClass('Quiz')->parseAndReturnResultSet( $new_results );
							$this->log("new-result-from-query - ".$new_results->admitted." listings from parseAndReturnResultSet".", filter:".$new_results->filter);
							$new_quiz->filter = $new_results->filter;
							$new_quiz->results = [$new_results];
							$new_quiz->created = date("Y-m-d H:i:s");
							$new_quiz->updated = date("Y-m-d H:i:s");
							// $activity->data[] = $new_quiz;
							// $quizId = -1; // get the last one from getLastResult()

							$ses = $this->getClass('Sessions')->get((object)['where'=>['id'=>$in->data['sessionID']]]);
							$ses = array_pop($ses);

							// check to see if we are going to re-use the owners or re-set them to incoming request's
							$portalAgent = isset($in->data['portalAgent']) && !empty($in->data['portalAgent']) ? $in->data['portalAgent'] : $activity->portal_agent_id;
							$portalUser = isset($in->data['portalUser']) && !empty($in->data['portalUser']) ? $in->data['portalUser'] : $activity->portal_user_id;
							$wpUser = isset($in->data['wpUser']) && !empty($in->data['wpUser']) ? $in->data['wpUser'] : $activity->wp_user_id;

							if (!($row = $this->getClass('QuizActivity')->add((object) array(
																							'session_id' => $ses->id,
																							'status' => 'done',
																							'portal_agent_id' => $portalAgent,
																							'portal_user_id' => $portalUser,
																							'wp_user_id' => $wpUser,
																							'data' => [$new_quiz]
																						))) ) throw new \Exception('unable to save quiz action.');
							$quizId = $row;
							// if ($quizId == -1 ||
							// 	$quizId >= $size)
							// 	$activity->data[] = $last_quiz;
							// else
							// 	$activity->data[$quizId] = $last_quiz;

							// $result = $this->getClass('QuizActivity')->set([(object) [
							// 	'where'  => [ 'session_id' => $in->data['sessionID'] ],
							// 	'fields' => [ 'data' => $activity->data,
							// 				  'status' => 'done' ]
							// ]]);
							// if (!$result) throw new \Exception('Unable to update quiz activity.');
						}

						// $out = new Out('OK', $this->getLastResult($in->data['sessionID'], $quizId));
						$out = $this->getLastResult($in->data['sessionID'], $quizId);
						break;
				default: throw new \Exception(__FILE__.':('.__LINE__.') - invalid query: '.$in->query); break;
			}
			if ($out) {
				if ($this->dumpJson) {
					$json = json_encode($out);
					$this->log("json result: $json");
					echo $json;
				}
				else
					echo json_encode($out);
			}
			
			// $this->qq->add((object)array('type' => 'Quiz',
			// 				  		'data' => 'echo out: '.json_encode($out)));
		} catch (\Exception $e) { return parseException($e, true); }
	}
	private function buildQuestions(&$Questions, &$Slides, &$SlideTags, &$Tags) {
		$cQ = count($Questions);
		for ($qI = 0; $qI < $cQ; $qI++) {
			$Questions[$qI]->slides = [];
			$sQ = count($Slides);
			$gotQuestionMatch = false;
			for($sI = 0; $sI < $sQ; $sI++) if ($Slides[$sI]->question_id != $Questions[$qI]->id && !$gotQuestionMatch) continue; else
				if ($Slides[$sI]->question_id == $Questions[$qI]->id) {
					$gotQuestionMatch = true;
					$Slides[$sI]->tags = [];
					$stQ = count($SlideTags);
					$tQ = count($Tags);
					$gotSlideTag = false;
					for($stI = 0; $stI < $stQ; $stI++) if ($SlideTags[$stI]->slide_id != $Slides[$sI]->id && !$gotSlideTag) continue; else
						if ($SlideTags[$stI]->slide_id == $Slides[$sI]->id) {
							$gotSlideTag = true;
							for($tI = 0; $tI < $tQ; $tI++) {
								$Tags[$tI] = (object)$Tags[$tI];
								 if ($Tags[$tI]->id == $SlideTags[$stI]->tag_id) {
									$Slides[$sI]->tags[] = $Tags[$tI];
									break;
								}
							}
						}
						else if ($gotSlideTag)
							break;
					$Questions[$qI]->slides[] = $Slides[$sI];
				}
				else if ($gotQuestionMatch)
					break;

			usort($Questions[$qI]->slides, function($a, $b) {
				return $a->orderby - $b->orderby;
			});
		}
	}

	// session_id is the 'id' column of Sessions table!!
	private function getLastResult($session_id, $quizId, $city_id = -1, $activity = null) {
		try {// gets the last result + listing info
			require_once(__DIR__.'/../_classes/GoogleLocation.class.php'); $Google = new GoogleLocation();
			$this->log("getLastResult - quizId: $quizId, session: $session_id, activity:".(empty($activity) ? "not " : '')."supplied");
			if (!$activity) {
				$activity = $this->getActivityBySessionID($session_id, $quizId); // this is 'id' from Sessions table now, not 'session_id' of QuizActivity!!
				if (empty($activity)) return new Out('fail', "No activity found for session id:$session_id.");
				$activity = array_pop($activity);

				if ($activity->status != 'done' &&
					$activity->status != 'partial') {
					if ($activity->status == 'pending' &&
						(time() - strtotime($activity->updated)) > 20 ) {
						$this->log("getLastResult - quizId: $quizId, is still pending after 20 secs!"); 
						$out = $this->doForcedRerun($session_id, $quizId);
						if ($out->status == 'fail')
							return $out;
						elseif ( gettype($out->data) == 'string' ) // then it's OK with the activity->status
							return $out;
						else { // then it's OK, get the current activity
							$activity = $this->getActivityBySessionID($session_id, $quizId);
							if (empty($activity)) return new Out('fail', "No activity found for session id:$session_id after rerun quiz.");
							$activity = array_pop($activity);
						}
							
					}
					else {
						$this->log("getLastResult - status:".$activity->status.", quizId: $quizId, session: $session_id");
						return new Out('OK', $activity->status);
					}
				}
			}

			// $size = count($activity->data);
			// if ($quizId == -1 ||
			// 	$quizId >= $size)
			// 	$last_quiz = array_pop($activity->data); // pop the last quiz
			// else
			// 	$last_quiz = $activity->data[$quizId];
				
			$last_quiz = array_pop($activity->data); // pop the last quiz
			if (!isset($last_quiz->results) ||
				 empty($last_quiz->results) ||
				 empty($last_quiz->results[0])) { 
				$reason = !isset($last_quiz->results) ? 'results array not set' :
						  (empty($last_quiz->results) ? 'results array is empty' : 'results object is empty');
				$this->log("getLastResult - quizId: $quizId, $reason!"); 
				$out = $this->doForcedRerun($session_id, $quizId);
				if ($out->status == 'fail')
					return $out;
				elseif ( gettype($out->data) == 'string' ) // then it's OK with the activity->status
					return $out;
				else
					$results = $out->data; // it's OK with results
			}
			else
				$results = array_pop($last_quiz->results); // pop the last result set

			$haveActiveParallelProcessing = $this->haveActiveParallelProcessing();
			$listingCount = 0;
			$imgPath = realpath(__DIR__.'/../_img/_listings/845x350');
			// setup listing ids to call from DB
			$listing_ids_to_get = array();
			if (isset($results->listings) &&
				!empty($results->listings)){
				foreach ($results->listings as $i=>$listing) {
					$listing_ids_to_get[$i] = $listing->id;
					unset($listing);
				}
				
				$this->log("getLastResult - initial listing count: ".count((array)$results->listings).", listing_ids_to_get:".count($listing_ids_to_get).", haveActiveParallelProcessing:".($haveActiveParallelProcessing ? 'yes' : 'no'));

				// get listings from database
				$query = (object) array(
					'where'=>array('id'=>$listing_ids_to_get),
					'what'=>array('id','active','first_image','list_image','price','title','street_address','city','state','country','interior','interior_std','lotsize','lotsize_std','beds','baths','city_id','flags','listhub_key'),
				);
				$this->defaults['show_inactive_listings'] ? null : $query->where['active'] = 1;

				// get listing geoinfo
				$Geoinfo = $this->getClass('ListingsGeoinfo')->get((object)array(
					'where'=>array('listing_id'=>$listing_ids_to_get),
					'what'=>array('id','listing_id','lat','lng'),
				));

				$Listings = $this->getClass('Listings')->get($query);
				// remove extra data from output
				if (empty($Listings)) return new Out('fail', "No listings in result for quizId:$quizId");
				$this->log("getLastResult - Retrieved ".count($Listings)." listings from DB");

				// double check that the listings' cities are in the $results->cities (won't be if the city wasn't tagged)
				if (!isset($results->cities) || empty($results->cities))
					$results->cities = new \stdClass();
				else
					$results->cities = (object) $results->cities;

				foreach ($results->listings as &$listing)
					if ($listing->city_id > 0 &&
						!property_exists($results->cities, $listing->city_id)) {
						$id = $listing->city_id;
						$city = (object)array('tags' => null,
												'id' => $id,
												'percent' => 0);
						$results->cities->$id = $city;
					}

				if (!empty( $results->cities )) {
					$temp = array();
					$cities_from_db = $this->getClass('Cities')->get((object)[ 'what' => [ 'id', 'image', 'city', 'state', 'count', 'lng', 'lat' ], 'where' => [ 'id' => array_keys((array) $results->cities) ] ]);
					foreach($cities_from_db as $city) $temp[$city->id] = $city;
					$cities_from_db = $temp;
					unset($temp);
				}

				if (!empty( $Geoinfo )) {
					$temp = array();
					foreach($Geoinfo as $geo) $temp[$geo->listing_id] = $geo;
					$Geoinfo = $temp;
					unset($temp);
				}

				if (!empty( $Listings )) {
					$temp = array();
					foreach($Listings as $l) $temp[$l->id] = $l;
					$Listings = $temp;
					unset($temp);
				}

				$fieldTypes = ['first_image']; // can be an array of non-homegenous updates, some row can have one or more fields
				$keyType = 'id'; // what column to trigger on
				$elements = [];
				$y = [];
				// $this->log("getLastResult - before foreach loop to gather results, count:".count((array)$results->listings));
				// grab listing data from DB and combine with results data
				foreach ($results->listings as $i => &$listing){
					$geo = isset($Geoinfo[$listing->id]) ? $Geoinfo[$listing->id] : null;	
					if ($geo != null &&
						$geo->lat != -1){
						$listing->lat = $geo->lat;
						$listing->lng = $geo->lng;
					}			

					$a = null;
					$haveFile = false;
					// grab only first image
					$db_listing = isset($Listings[$listing->id]) ? $Listings[$listing->id] : null;
					// foreach ($Listings as &$db_listing) if (!empty($db_listing->id) && $db_listing->id == $listing->id) {
					if ($db_listing != null) {
						if (isset($listing->image_path)  && !empty($listing->image_path)) {
							if ( (substr($listing->image_path, 0, 4 ) != 'http' &&
								  file_exists($imgPath.'/'.$listing->image_path)) ||
								 substr($listing->image_path, 0, 4 ) == 'http' ) {
									$haveFile = true;
									$db_listing->image_path = $listing->image_path;
									$a = &$db_listing;
									$this->log("Listing:$listing->id has image_path:$listing->image_path");
							}				
						}

						if (!$haveFile) {
							// $this->log("getLastResult - begin looking for valid image for $listing->id");
							if ( isset($db_listing->list_image) &&
								 ($haveFile = !empty($db_listing->list_image)) ) {
								if ( (substr($db_listing->list_image->file, 0, 4 ) != 'http' &&
									  file_exists($imgPath.'/'.$db_listing->list_image->file)) ) {
									$db_listing->image_path = $db_listing->list_image->file;
									$a = &$db_listing;
								}
								else
									$haveFile = false;
							}

							if ( isset($db_listing->first_image) &&
								 ($haveFile = !empty($db_listing->first_image)) ) {
								if ( (substr($db_listing->first_image->file, 0, 4 ) != 'http' &&
									  file_exists($imgPath.'/'.$db_listing->first_image->file)) ||
									  substr($db_listing->first_image->file, 0, 4 ) == 'http') {
									$db_listing->image_path = $db_listing->first_image->file;
									$a = &$db_listing;
								}
								else
									$haveFile = false;
								
							}
							
							// if we have to, go get the whole images stack and get the first one...
							if (!$haveFile) {
								$temp = $this->getClass('Listings')->get((object)['where'=>['id'=>$listing->id],
																				  'what'=>['images']]);
								if (!empty($temp) &&
									!empty($temp[0]->images)) {
									foreach($temp[0]->images as $i=>$img) {
										if (!empty($img->file)) { // can be a discard
											$isHttp = substr($img->file, 0, 4 ) == 'http';
											if ($isHttp) {
												$db_listing->images = array($img);
												$haveFile = true;
											}
											elseif ($haveActiveParallelProcessing) {
												if (isset($img->url) &&
													!empty($img->url)) {
													$img->file = $img->url;
													$haveFile = true;
												}
											}
											elseif (!file_exists($imgPath.'/'.$img->file)) {
												if (isset($img->url)) {
													$img->file = $img->url; // revert to url...
													$db_listing->images = array($img);
													$haveFile = true;
												}
											}
											else {
												$db_listing->images = array($img);
												$haveFile = true;
											}

											if ($haveFile) {
												$db_listing->image_path = $img->file;
												if (!isset($temp[0]->first_image) ||
													empty($temp[0]->first_image) ||
													$temp[0]->first_image->file != $db_listing->image_path)
													$elements[] = (object)['key'=>$listing->id,
											   	  							'fields'=>['first_image'=>$img]];
											   	$a = &$db_listing;
												break;
											}

										}
										unset($img);
									}
								}
							}
							
							// if ($haveFile) {// else they must have been all discards
							// 	if ( ($file = $this->getFirstImage($db_listing, $haveActiveParallelProcessing)) ) {
							// 		$db_listing->image_path = $file;
							// 		$a = &$db_listing;
							// 	}
							// }
						}
						// if ( ($i % 50) == 0 )
						// 	$this->log("getLastResult - done looking for valid image, haveFile:".($haveFile ? 'found' : 'no image').", index:".($i+1));
					}
					else
						$this->log("Cannot find listing with id:$listing->id");

					if ($haveFile &&
						isset($a)){
						// $this->log("getLastResult - update geo for $listing->id");
						foreach ($listing as $key=>&$value) if ($key != 'image_path') $a->$key = $value;
						$listing = $a;
						if ( !isset($listing->lat) ||
							 $listing->lat == -1 ) {// then no listing geoCode exists or it's failed
							$city = isset($cities_from_db[$listing->city_id]) ? $cities_from_db[$listing->city_id] : null;
							if ($city != null &&
								(isset($city->lng) &&
								 $city->lng != -1)) { // pass long city's geocoding
								$listing->lat = $city->lat;
								$listing->lng = $city->lng;
							}
							elseif ($listing->city_id <= 0 &&
									!empty($listing->city) &&
									!empty($listing->state)) {
								$city = $this->getClass('Cities')->get((object)['where'=>['city'=>$listing->city,
																						  'state'=>$listing->state]]);
								if (!empty($city) ) {
									$listing->city_id = $city[0]->id;
									if ($city[0]->lng != -1 &&
										$city[0]->lat != -1 &&
										(!isset($listing->lng) ||
										 $listhub->lng == -1) ) {
										$listing->lat = $city[0]->lat;
										$listing->lng = $city[0]->lng;	
									}
									$this->getClass('Listings')->set([(object)['where'=>['id'=>$listing->id],
																				'fields'=>['city_id'=>$city[0]->id]]]);
								}
							}
						}
						$y[] = $a;
						unset($a);
						// $this->log("getLastResult - done updating geo for $listing->id");
					}
					else 
						unset($listing);
				}
				unset($Listings, $listing, $listing_id);

				if (count($elements)) {
					$this->log("getLastResult - About to setMultiple for $keyType for ".count($elements)." elements for first_image");
					$x = $this->getClass('Listings')->setMultiple($keyType, $fieldTypes, $elements);
				}
				unset($elements, $fieldTypes);


				$this->log("getLastResult - y list count:".count($y));
				$x = [];
				foreach ($y as &$listing){
					// unset extra data
					if (!empty($listing->images)) foreach ($listing->images as &$image){
						if (isset($image->title) && $image->title == 'N/A') unset($image->title);
						if (isset($image->desc) && $image->desc == 'blank') unset($image->desc);
						if (isset($image->file)) $image->file = removeslashes($image->file);
						if (isset($image->url)) $image->url = removeslashes($image->url);
					}

					if (isset($listing->price)) $x[] = $listing;
					else $this->log("Listing:$listing->id has no price!");
					unset($listing);
				}
				$results->listings = $x;
				$listingCount = count($results->listings);
				unset($x);
				unset($y);

				if (empty($results->listings)) $results->listings = [];
				$this->log("getLastResult - result set count:$listingCount, real count:".count($results->listings).", listings:".print_r($results->listings, true));
			}
			elseif (isset($results->city_sorted)) {
				$temp = [];
				global $listingKeyMap;

				foreach($results->city_sorted as $id=>$city) {
					$listings = [];
					$count = 0;

					if ( (gettype($results->cities) == 'object' &&
						 !isset($results->cities->$id)) ||
						 (gettype($results->cities) == 'array' &&
						 !isset($results->cities[$id])) ) {
						$this->log("getLastResult - cityId:$id, not found in r->cities, ignored");
						unset($city, $listings);
						continue;
					}

					$city->total = count((array)$city->listings);

					foreach($city->listings as $listing) {
						if (isset($listing->tags)) unset($listing->tags);
						if (isset($listing->city_id)) unset($listing->city_id);

						if (substr($listing->image_path, 0, 4 ) != 'http' &&
							!$haveActiveParallelProcessing &&
							!file_exists($imgPath.'/'.$listing->image_path)) {
							$this->log("getLastResult - failed to locate $listing->image_path for listing:$listing->id");
							unset($listing);
							$city->total--;
							continue;
						}

						$newListing = [];
						// remap kyes to abbreviated form
						foreach($listing as $key=>$value)
							$newListing[ $listingKeyMap[$key] ] = $value;
						$listings[] = $newListing; // disassociate
						unset($listing, $newListing);
						if (++$count == QUIZ_INIT_MAX_LISTINGS_PER_CITY) break;
					}

					unset($city->listings);
					$city->listings = $listings;
					if ($city->total)
						$temp[] = $city; // disassociate
					$listingCount += count($listings);
					unset($city,$listings);
				}
				$cityKeys = array_keys((array)$results->city_sorted);
				unset($results->city_sorted);
				$results->city_sorted = $temp;

				// take only cities that matter, discard rest
				$temp = [];
				foreach($cityKeys as $keys)
					$temp[$keys] = gettype($results->cities) == 'object' ? $results->cities->$keys : $results->cities[$keys];
				unset($results->cities);
				$results->cities = $temp;

				// lighten load
				unset($results->time);


				// $temp = [];
				// foreach($results->cities as $id=>$data)
				// 	$temp[] = $data; // disassociate
				// unset($results->cities);
				// $results->cities = $temp;

				$this->log("getLastResult - city_sorted count:".count((array)$results->city_sorted));
				$this->log("getLastResult - gather city images, count:".count((array)$results->cities));
				// $i = 0;
				// foreach($results->city_sorted as $id=>&$city_group) {
				// 	if ($i == DEFAULT_INITIAL_CITY_GROUP)
				// 		break;
				// 	if ( !is_true($city_group->haveAllListings) ) {
				// 		$listing_ids = [];
				// 		foreach($city_group->listings as $listing) {
				// 			if (!isset($listing->city)) $listing_ids[] = $listing->id;
				// 			unset($listing);
				// 		}

				// 		$listings = $this->getClass('Listings')->get((object)['where'=>['id'=>$listing_ids],
				// 															  'what'=>['id','images','first_image']]);
				// 		unset($listing_ids);
				// 		foreach($listings as $listing)
				// 	}
				// 	$i++;
				// }
			}

			if (!isset($results->city_sorted)) { // then had to be old style quiz or show-all city search
				$this->log("getLastResult - gather city images, count:".count($results->cities));
				if (!empty( $results->cities )){
					//$cities_from_db = $this->getClass('Cities')->get((object)[ 'what' => [ 'id', 'image', 'city', 'state', 'count' ], 'where' => [ 'id' => array_keys((array) $results->cities) ] ]);
					foreach ($results->cities as &$row){
						$row_db = isset($cities_from_db[$row->id]) ? $cities_from_db[$row->id] : null;
						// foreach ($cities_from_db as &$row_db){
						if ($row_db != null){
							foreach ($row_db as $row_db_field => &$row_db_value) if ($row_db_field != 'id') {
								if ($row_db_field == 'image')
									$row->$row_db_field = $row_db_value ? $row_db_value : '_blank.jpg';
								else
									$row->$row_db_field = $row_db_value;
								unset($row_db_value);
							}
					// 		unset($row_db);
						}
						// }
					}
				}
				else
					$this->log("getLastResult - results->cities is empty!");
			}

			// $results->price[0] = intval($results->price[0]);
			// $results->price[1] = intval($results->price[1]);
			$results->price[0] = (int)$results->price[0];
			$results->price[1] = (int)$results->price[1];

			if (!empty($results->location))
				foreach($results->location as &$row)
					$row = (int)$row;
					// $row = intval($row);

			if (true) // unset extra data before output
				unset($results->updated, $results->max, $results->total, $results->city_total);

			$results->quiz = $last_quiz->quiz;
			// $results->index = $quizId == -1 ? $size-1 : $quizId;
			$results->index = $activity->id; // this is now the row index of QuizActivity table

			// grab Tags for cities so we have the icon data
			$this->attachCityTags($results);

			$this->log("getLastResult - quizId: $quizId, session: $session_id - returning results with ".$listingCount." listings.");
			return new Out('OK', $results);
		} catch (\Exception $e) { return parseException($e); }
	}

	private function getFirstImage($listing, $haveActiveParallelProcessing) {
		$haveFile = false;
		if ( isset($listing->first_image) &&
			($haveFile = !empty($listing->first_image)) )
			return $listing->first_image;
		else {
			if (!empty($listing->images)) {
				foreach($listing->images as $i=>$img) {
					if (!empty($img->file)) { // can be a discard
						$isHttp = substr($img->file, 0, 4 ) == 'http';
						if ($isHttp) {
							return $img->file;
						}
						elseif ($haveActiveParallelProcessing) {
							if (isset($img->url) &&
								!empty($img->url)) {
								return $img->url;
							}
						}
						elseif (!file_exists($this->defaults['imgPath'].'/'.$img->file)) {
							if (isset($img->url)) {
								return $img->url; // revert to url...
							}
						}
						else {
							return $this->options['imgPath'].'/'.$img->file;
						}
					}
					unset($img);
				}
			}
			return null;
		}					
	}

	private function attachCityTags(&$results) {
		// $this->qq->add((object)array('type' => 'attachCityTags',
		// 					   'data' => 'entered'));
		$temp = array();
		$Tag = $this->getClass('Tags');
		$q = new \stdClass();
		$q->what = array('id', 'description', 'icon','tag');
		$q->where = array('type' => 1,
						  'icon' => 'notnull');
		// $this->qq->add((object)array('type' => 'attachCityTags',
		// 					   'data' => 'before Tag->get icon data'));
		$temp = array();
		$x = $Tag->get($q);
		// $this->qq->add((object)array('type' => 'attachCityTags',
		// 					   'data' => 'result -'.(empty($x) ? "N/A" : count($x)).' icon data'));
		$temp = array();
		if (!empty($x))
			foreach($x as $icon) {
				if ($icon->id == 167) // freshwater boating
					$icon->tag = 'boating';
				$temp[$icon->id] = array('desc'=> $icon->description,
										'icon' => $icon->icon,
										'tag' => ucwords($icon->tag));
			}
		// $this->qq->add((object)array('type' => 'attachCityTags',
		// 					   'data' => 'exiting attachCityTags with '.count($temp).' icon data'));
		$results->cityTags = $temp;
	}

	private function profileInList($profiles, $info) {
		foreach($profiles as $prof) { // just check for row index of QuizActivity now...
			if ($prof->quizId == $info->quizId ) //&&
				// $prof->session_id == $info->session_id)
				return true;
		}
		return false;
	}

	// session_id is the 'id' column of the Sessions table
	public function getProfiles($sessionId) {
		if (empty($sessionId))
			return false;

		$profiles = [];
		$sessionForID = $this->getClass('Sessions')->get((object)array('where'=>array('id'=>$sessionId)));
		if (!empty($sessionForID))
			$sessionForID = array_pop($sessionForID);
		else
			throw new \Exception("Failed to find session with id: $sessionId");

		$s = $this->getClass('SessionMgr')->getSessions(); // all sessions based on userId
		$this->log("getProfiles - has ".count($s)." sessions to work with");
		$user = $this->getClass('Register')->getUserData(-1); // valid regiestered user?
		$this->log("getProfiles - id:$sessionId, session_id:$sessionForID->session_id, userId:".($user->status == 'OK' ? $user->data->user_id : 'N/A'));


		$lastQuiz = null;
		try {
			$lastQuiz = $this->getLastQuiz($sessionId);
			$this->log("getProfiles - getLastQuiz() returned ".(!empty($lastQuiz) ? $lastQuiz->index : "nothing"));
			// NOTE: below check not needed anymore since each QuizActivity should only have one result/activity
			// if ($lastQuiz &&
			// 	$lastQuiz->index != ($lastQuiz->size-1)) // then this quiz was not the last one, but some previous result
			// 	$lastQuiz = null;
		}
		catch( \Execption $e) {
			// oops, no last quiz!
			// parseException($e); 
			$this->log("Exception from getLastQuiz: ".$e->getMessage());
		}

		// if we have lastQuiz set, make sure it didn't come from a user owned session if the userid == 0
		if (!empty($lastQuiz) &&
			!empty($s) &&
			$user->status != 'OK') {
			foreach($s as $ses) {
				// if ($ses->session_id == $sessionId &&
				if ($ses->id == $sessionId &&
					$ses->user_id != 0) {
					if ( $ses->ip_dot == $this->getClass('SessionMgr')->userIP() )
						break; // then same session on same ip, probably just logged out
					$lastQuiz = null;
					break;
				}
			}
		}
		
		$haveLastQuizProfiled = false;
		if (!empty($s) &&
			$user->status == 'OK') {
			// then grab all the sessions owned by this user
			// if (isset($s[0]->user_id) &&
			// 	$s[0]->user_id != 0) { 
			// 	$s = $this->getClass('Sessions')->get((object)array('where'=>array('user_id'=>$s[0]->user_id)));
			// }
			foreach($s as $j=>$ses) {
				$data = $ses->value;
				if (isset($data)) {
					$data = (array)$data;
					$msg = "session: $j has ".count($data)." elements";
					$this->log($msg);
					foreach($data as $i=>$info) {
						if ($info->type == SESSION_PROFILE_NAME) {
							if (isset($info->owner) &&
								$info->owner != $user->data->user_id) {
								$this->log("Different owner, expecting:$info->owner, but current user is {$user->data->user_id}, session: $j index: $i, quizType: ".$info->quizType.", name:".$info->name.", id:".$info->quizId);
								continue;
							}
							$msg = "session: $j index: $i, quizType: ".$info->quizType.", name:".$info->name.", id:".$info->quizId;
							$this->log($msg);
							if ( ($info->quizType % 2) != 1 &&
								!is_array($info->locations))
								$info->locations = [$info->locations];

							if ( ($info->quizType % 2) != 1 &&
								!empty($info->locations) &&
								count($info->locations)) // convert to city-name
								foreach($info->locations as $k=>$loc) {
									if (!empty($loc)) {
										$c = $this->getClass('Cities')->get((object)['where'=>['id'=>$loc]]);
										$info->locations[$k] = !empty($c) ? $c[0]->city.','.$c[0]->state : "N/A";
									}
									unset($k, $loc);
								}

							if ($lastQuiz != null &&
								// $ses->session_id == $sessionId &&
								$ses->id == $sessionId &&
								$info->quizId == $lastQuiz->index)
								$haveLastQuizProfiled = true;


							$info->session_id = $ses->session_id;
							$info->activeSessionID = $ses->id;
							if (!$this->profileInList($profiles, $info)) {
								$msg = "session: $j index: $i, added to profiles - quizType: ".$info->quizType.", name:".$info->name.", id:".$info->quizId;
								$this->log($msg);
								$profiles[] = $info;
							}
							else {
								$msg = "session: $j index: $i, duplicate - not added - quizType: ".$info->quizType.", name:".$info->name.", id:".$info->quizId;
								$this->log($msg);
							}
						}
						else {
							$msg = "session:$ses->session_id, session index: $j meta index: $i, type:$info->type";
							$this->log($msg);
						}
						unset($info);
					}
				}
				unset($ses);
			}
		}
		
		$this->log("getProfiles - user status:$user->status, haveLastQuizProfiled:".($haveLastQuizProfiled ? "true" : "false").", lastQuiz:".(!empty($lastQuiz) ? $lastQuiz->index: "n/a"));

		if (!$haveLastQuizProfiled &&
			!empty($lastQuiz) ) {
			$info = new \stdClass();
			$info->type = SESSION_PROFILE_NAME;
			$info->owner = $user->data->user_id;
			$info->name = "View Last Search";
			$info->quizId = $lastQuiz->index;
			$info->session_id = $sessionForID->session_id;
			$info->activeSessionID = $sessionForID->id;
			$info->quizType = $lastQuiz->quiz;
			$info->locations = isset($lastQuiz->location) ? $lastQuiz->location : [];
			$info->distance = isset($lastQuiz->distance) ? $lastQuiz->distance : 0;
			// $info->session_id = $sessionForID->session_id;
			// $info->id = $sessionId;
			// $last = [];
			// $last[] = $info;
			// $profiles = array_merge($last, $profiles);
			$profiles[] = $info;
		}
		return count($profiles) ? $profiles : false;
	}

	private function haveActiveParallelProcessing() {
		$Options = $this->getClass('Options');
		$x = $Options->get((object)['or'=>['opt'=>['activePP','activeIP','activeIPCustom']]]);
		if (empty($x))
			$x = $Options->get((object)['where'=>['opt'=>'activeParseToDb']]);
		return !empty($x);
	}

	protected function doForcedRerun($session_id, $quizId) {
		$ran = $this->getClass('Quiz')->parseQuizActions($session_id, $quizId);
		if (!$ran) {// bool returned
			$this->log("doForcedRerun - failed to rerun quiz:$quizId for session:$session_id");
			return new Out('fail', "Empty results for quizId:$quizId, failed to rerun quiz"); 
		}
		else { // get the last result set so far
			$this->log("doForcedRerun - forced rerun quiz:$quizId, going to retrieve the last quiz search results");
			$activity = $this->getActivityBySessionID($session_id, $quizId); // this is 'id' from Sessions table now, not 'session_id' of QuizActivity!!
			if (empty($activity)) return new Out('fail', "No activity found for session id:$session_id after forced rerun quiz:$quizId");
			$activity = array_pop($activity);

			if ($activity->status != 'done' &&
				$activity->status != 'partial') {
				$this->log("doForcedRerun - after forced rerun, status:".$activity->status.", quizId: $quizId, session: $session_id");
				return new Out('OK', $activity->status);
			}

			$last_quiz = array_pop($activity->data); // pop the last quiz
			if (!isset($last_quiz->results)) { 
				$this->log("doForcedRerun - quizId: $quizId, results array not set after forced rerun!"); 
				return new Out('fail', "No results not set quizId:$quizId after forced rerun"); 
			}
			$results = array_pop($last_quiz->results); // pop the last result set
			$this->log("doForcedRerun - after forced rerun, got results for quiz:$quizId");
		}
		return new Out('OK', $results);
	}
}

new AJAX_Quiz();
