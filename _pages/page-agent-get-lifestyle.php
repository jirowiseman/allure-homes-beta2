<?php
global $ALR; 
require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new AH\Sellers(1);
require_once(__DIR__.'/../_classes/Reservations.class.php'); $Reservations = new AH\Reservations;
require_once(__DIR__.'/../_classes/Options.class.php'); $Options = new AH\Options;
require_once(__DIR__.'/../_classes/Cities.class.php'); $Cities = new AH\Cities;
require_once(__DIR__.'/../_classes/States.php');
global $usStates;
global $ALR;

$amTags = $Sellers->getAMTagList();
// $intro = new \stdClass();
// $intro->id= 0;
// $intro->tag = "Choose LifeStyle";
// array_unshift($amTags, $intro);

$seller_id = get_query_var('id');
$agent = !empty(get_query_var('extra')) ? get_query_var('extra') : 0;
$Sellers->log("page-agent-get-lifestyle - seller_id:".(!empty($seller_id) ? $seller_id : "N/A").", agent:".(!empty($agent) ? $agent : "N/A"));
$seller = null;
$track = null;
$trackId = 0;
$discount = 0;
$coupon = "Lifestyle10Ok";
$discount = 89;
$tag = 0;
$promo = '';

if (strpos($seller_id, "-") != false) {
	$parts = explode("-", $seller_id);
	
	if (strpos($parts[0], "T") === false &&
		strpos($parts[0], "C") === false &&
		strpos($parts[0], "L") === false) {
		$isAdmin = intval($parts[0]);
		$Sellers->log("page-agent-get-lifestyle - isAdmin: ".$parts[0]);
	}
	else if (strpos($parts[0], "T") !== false) {
		$seller_id = $parts[1];
		$trackId = substr($parts[0], 1);
		$Sellers->log("page-agent-get-lifestyle - track: ".$parts[0].", id: $trackId");
		require_once(__DIR__.'/../_classes/EmailTracker.class.php'); $et = new AH\EmailTracker(1);
		$track = $et->get((object)['where'=>['id'=>$trackId]]);
		$et->log("page-agent-get-lifestyle - trackId:$trackId, got ".(!empty($track) ? 'one' : 'none'));
		if (!empty($track)) {
			$track = array_pop($track);
			$et->log("page-agent-get-lifestyle - track: ".print_r($track, true));
			switch(intval($track->type)) {
				case lifestyle_PROMO_1:
					$opt = $Options->get((object)['where'=>['opt'=>'LIFESTYLE_PROMO_1']]);
					$et->log("page-agent-get-lifestyle - opt: ".print_r($opt, true));
					if (empty($opt)) {
						$discount = 29.00;
						$coupon = "lifestyle10OK";
					}
					else {
						$opt = json_decode($opt[0]->value);
						$discount = $opt->discount;
						$coupon = $opt->coupon;
						if (isset($opt->tag))
							$tag = $opt->tag;
					}
					break;
			}
		}
	}
	else if (strpos($parts[0], "L") !== false) { // lifestyle
		$tag = $parts[1];
	}
	else if (strpos($parts[0], "C") !== false) { // coupon
		$promo = $seller_id;
		$seller_id = $agent;

		// $extraInfo = '';
		// $orig = '';
		// if ( ($pos = strpos($$parts[1], '?')) !== false ) {
		// 	$orig = $$parts[1];
		// 	$$parts[1] = substr($$parts[1], 0, $pos);
		// 	$extraInfo = substr($orig, $pos);
		// }
		// if (!empty($extraInfo)) {
		// 	$extraInfo = subsbr($extraInfo, 1);
		// 	$extraInfo - explode('=', $extraInfo);
		// 	switch($extraInfo[0]) {
		// 		case 'agent': 
		// 			$seller_id = $extraInfo[1]; break;
		// 	}
		// }
		$opt = $Options->get((object)['where'=>['opt'=>$parts[1]]]);
		$Sellers->log("page-agent-get-lifestyle - extraInfo:$orig".(!empty($extraInfo) ? print_r($extraInfo, true) : "N/A").", opt: ".print_r($opt, true));
		if (empty($opt)) {
			$discount = 89.00;
			$coupon = "Lifestyle10OK";
		}
		else {
			$opt = json_decode($opt[0]->value);
			$discount = $opt->discount;
			$coupon = $opt->coupon;
			if (isset($opt->tag))
				$tag = $opt->tag;
		}
	}
}
else {
	$opt = $Options->get((object)['where'=>['opt'=>'DefaultLifestyleCoupon']]);
	if (!empty($opt)) {
		$Sellers->log("page-agent-get-lifestyle - opt: ".print_r($opt, true));
		$opt = json_decode($opt[0]->value);
		$discount = $opt->discount;
		$coupon = $opt->coupon;
	}
	else
		$Sellers->log("page-agent-get-lifestyle - no DefaultLifestyleCoupon found, using $coupon");
}

$showSimplifiedAgentLifestylePages = 0;
$opt = $Options->get((object)['where'=>['opt'=>'ShowSimplifiedAgentLifestylePages']]);
if (!empty($opt))
	$showSimplifiedAgentLifestylePages = AH\is_true($opt[0]->value);


$isFree = $discount == 99;

if ( !empty($seller_id) ) {
	$seller = $Sellers->get((object)['where'=>['id'=>$seller_id]]);
	if (!empty($seller)) {
		$seller = $seller[0];
	}
}
	
if (empty($seller)) {
	if (is_user_logged_in()) {
		$seller = $ALR->get('seller');	 
		if (empty($seller)) {
			$user = wp_get_current_user();
			if ($user && !is_wp_error( $user ) && gettype($user) == 'object' && property_exists($user, 'ID') && $user->ID) {
				$seller = new \stdClass();
				$seller->id = 0;
				$user = (object)$user;
				$seller->first_name = $user->user_firstname;
				$seller->last_name = $user->user_lastname;
				$seller->email = $user->user_email;
				$seller->state = '';
			}
		}
	}
}
	
if (empty($seller)) {
	$seller = new \stdClass();
	$seller->id = 0;
	$seller->first_name = '';
	$seller->last_name = '';
	$seller->email = '';
	$seller->state = '';
}

$seller->lifestyle = ''; // default value

$i = 0; $states = '';
foreach($usStates as $long=>$state) {
	$states .= '<option value="'.$state.'">'."$long - ($state)".'</option>';
	$i++;
}

$lifestyleOptions = '';
foreach($amTags as $tag_data) {
	$lifestyleOptions .= '<li value="'.$tag_data->id.'">'."$tag_data->tag".'</li>';
}

$allStates = [];
foreach($usStates as $long=>$state) {
	$allStates[] = (object)['label'=>$long,
							'value'=>$long,
							'id'=>$state];
	$allStates[] = (object)['label'=>$state,
							'value'=>$state,
							'id'=>$state];
}

$service_area = '';
$metaOrder = null;
$city_id = 0;
if (!empty($seller)) {
	if (isset($seller->service_areas) &&
		!empty($seller->service_areas)) {
		$areas = explode(":", $seller->service_areas);
		$areas = explode(',', $areas[0]);
		if (count($areas) == 2) {
			$city = trim($areas[0]);
			$state = trim($areas[1]);
			$service_area = $city.', '.$state;
			$city = $Cities->get((object)['where'=>['city'=>$city,
													'state'=>$state]]);
			if (!empty($city))
				$city_id = $city[0]->id;
		}
	}
	elseif (isset($seller->city) &&
			isset($seller->state) &&
			!empty($seller->city) &&
			!empty($seller->state)) {
		$service_area = $seller->city.", ".$seller->state;
		$city = $Cities->get((object)['where'=>['city'=>$seller->city,
												'state'=>$seller->state]]);
		if (!empty($city))
			$city_id = $city[0]->id;
	}

	$reservation = !empty($seller->id) ?  $Reservations->get((object)['where'=>['email'=>$seller->email]]) : null;
	if (!empty($reservation)) {
		if  (!empty($reservation[0]->meta)) foreach($reservation[0]->meta as $data) 
			if ($data->action == ORDER_AGENT_MATCH) {
				$metaOrder = $data;
				break;
			}
		if (isset($reservation[0]->lifestyle) &&
			!empty($reservation[0]->lifestyle))
			$seller->lifestyle = $reservation[0]->lifestyle;
	}
}

$sellerEmail = !empty($seller) && isset($seller->email) && !empty($seller->email) ? str_replace('"','', str_replace("'",'',$seller->email)) : '';

$Sellers->log("page-agent-get-lifestyle - sellerEmail: $sellerEmail, coupon:$coupon, discount:$discount");
?>

<script type="text/javascript">
var amTagList = <?php echo json_encode($amTags); ?>;
var seller = <?php echo json_encode($seller); ?>;
var service_area = '<?php echo $service_area; ?>';
var seller_city_id = <?php echo $city_id; ?>;
var metaOrder = <?php echo $metaOrder ? json_encode($metaOrder) : 0 ?>;
var et = '<?php echo !empty($track) ? $track->track_id : 0 ?>';
var key = <?php echo $trackId; ?>;
var tag = <?php echo $tag ? $tag : 0; ?>;
var isFree = <?php echo $isFree ? 1 : 0; ?>;
var showSimplifiedAgentLifestylePages = <?php echo $showSimplifiedAgentLifestylePages ? 1 : 0; ?>;
var type = <?php echo !empty($track) ? $track->type : 0 ?>;
var discount = <?php echo $discount; ?>;
var coupon = '<?php echo $coupon; ?>';
var promo = '<?php echo $promo; ?>';
var sellerEmail = "<?php echo $sellerEmail; ?>";
var allStates = <?php echo json_encode($allStates); ?>;
// console.log("discount:"+discount+", coupon:"+coupon);
</script>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('.mobile .more').on('click', function() {
			$('.mobile .bottomtext').slideToggle('fast');
			$('.mobile .more').hide();
			$('.mobile .less').show();
		});
		$('.mobile .less').on('click', function() {
			$('.mobile .bottomtext').slideToggle('fast');
			$('.mobile .less').hide();
			$('.mobile .more').show();
		});
		$('.top .more').on('click', function() {
			$('.top #information').slideToggle('fast');
			$('.top .more').hide();
			$('.top .less').show();
		});
		$('.top .less').on('click', function() {
			$('.top #information').slideToggle('fast');
			$('.top .less').hide();
			$('.top .more').show();
		});

		$('.lifestyleinput .urlinput').on('click', function() {
			event.preventDefault(); 
			$('.lifestyleinput #lifestyle').focus(); 
		});
		$('.lifestyleinput #lifestyle').keypress(function(e){
	      if(e.keyCode==13)
	      $('.lifestyleinput #check-it').click();
	    });
		$('.lifestyleinputmobile .urlinput').on('click', function() {
			event.preventDefault(); 
			$('.lifestyleinputmobile #lifestyle').focus(); 
		});
		$('.lifestyleinputmobile #lifestyle').keypress(function(e){
      if(e.keyCode==13)
      $('.lifestyleinputmobile #check-it').click();
    });
		
	});
</script>
<div class="videoDiv" id="lifestyle">
  <button class="closeVid">Close</button>
  <!-- <a id="closeVid" href="javascript:reg.closeFrame();">[X]</a> -->
  <iframe src="//player.vimeo.com/video/131675206" width="900" height="600" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen>
  </iframe>
</div>

<div id="agent-verify">
</div>

<div id="agent-lifestyle-reservation">
	<div class="top">
		<div class="title">It's simple, just choose your expertise and City</div>
		<!--<div class="subtitle">Start making money while you sleep</div>-->
		<div class="lifestyleinput">
			<div class="lifestyletags">
				<span class='selectArrow'></span>
				<span class='selected'>Select Lifestyle</span>
				<ul id="tags" style="display: none">
					<?php echo $lifestyleOptions; ?>
				</ul>
			</div>
			<input autocomplete="off" autocorrect="off" spellcheck="false" id="city" type="text" placeholder="Enter City Name" />
			<button id="check-it">Get Started!</button>
		</div>
		<div class="hiddendiv" style="display:block;"></div>
		<!--<div class="more" style="display:none;">Show More<div class="entypo-down-open-mini"></div></div>
		<div class="less" style="display:none;">Show Less<div class="entypo-up-open-mini"></div></div>-->
		<div id="information" style="display:none;">
			<span class="title">Great! To claim your spot please sign up with us below</span>
			<div id="user-data">
				<textarea id="description" for="'+index+'" <?php echo $showSimplifiedAgentLifestylePages ? 'style="display:none;"' : ''; ?> placeholder="Please enter detailed information that showcases your strength for this selection (at least 300 characters).  You need to be knowledgable about and experienced enough to answer any question about this specialty, WITHOUT searching on the internet for it.  We will be calling all candidates to vet them, to make sure our Agent Match specialists are who they claim to be.  For example: Why do you consider yourself an expert/specialist?  How long have you done this?  Do you have any specialized certifcations or won awards?  What makes you stand out from the rest, in your area, that will make a potential buyer want to talk to you about this?"></textarea>
				<div id="counter" <?php echo $showSimplifiedAgentLifestylePages ? 'style="display:none;"' : ''; ?> >
					<label>characters left:</label>&nbsp;<label id="chCount">300</label>
				</div>
				<div class="left">
					<div class="names">
						<input type="text" id="first_name" name="first_name" placeholder="First Name" value="<?php echo (!empty($seller) && isset($seller->first_name) && !empty($seller->first_name) ? $seller->first_name : ''); ?>" />
						<input type="text" id="last_name" name="last_name" placeholder="Last Name" value="<?php echo (!empty($seller) && isset($seller->last_name) && !empty($seller->last_name) ? $seller->last_name : ''); ?>" />
					</div>
					<input type="text" id="phone" name="phone" placeholder="Mobile Phone" value="<?php echo (!empty($seller) && isset($seller->phone) && !empty($seller->phone) ? $seller->phone : ''); ?>" />
					<input type="text" id="email" name="email" placeholder="*Email - name@example.com" value="<?php echo (!empty($seller) && isset($seller->email) && !empty($seller->email) ? $seller->email : ''); ?>" />
					<input type="text" id="login-id" name="login-id" placeholder="Login name - Defaults to Email (optional)" value="" />
				</div>
				<div class="right">
					<input type="text" id="breID" placeholder="RE License Number" value="" />
					<input type="text" id="states" placeholder="State" value="" />
					<!--<div class="states"> 
						<span id="states">State</span>&nbsp;
						<select name="States" id="states">
							<option value="-1" selected="selected">Select one</option>
							<?php echo $states; ?>
						</select>
					</div> -->
					<input type="password" id="password" placeholder="Password (At least 8 characters)" value="" />
					<input type="password" id="verify" placeholder="Verify Password" value="" /><span id="marker" class="entypo-attention"></span>
					<input type="text" id="invitation_code" placeholder="Invite Code (optional)" style="display: <?php echo $isFree ? 'none' : 'block'; ?>"/>
				</div>
			</div>
			<div class="reserve-now">
				<div class="reserve-now">
					<button id="buyIt">Claim My Spot</button>
					<span id="disclaimer" style="display: none;">*Please expect a call from LifeStyled Listings to discuss this lifestyle and perhaps additional cities and lifestyles.<span>
				</div>
			</div>
			<span class="reservelifestyle" style="display: none;">Learn more about being a <a id="agent-match" href="javascript:;">LifeStyled&#8482; agent</a> in your area!</span>
			<div id="warn-email-usage">
				<span>* Please use the email you associate with your MLS listings, so we can match you with any of your listings that may be on our site!</span>
				<div id="opt-in">
					<input type="checkbox" id="opt-in"/>
					<span>I would like to receive emails with helpful guides and updates related to my account.</span>
				</div>
			</div>
		</div>
		<div class="whatislifestyle">
			<div class="left">
				<iframe src="https://player.vimeo.com/video/133201928" style="padding-left: 12%;" width="600" height="334" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
			<div class="right">
				<span class="title"><span style="color:#F9A054">What</span> is Lifestyled Agent?</span>
				<p>A Lifestyled Agent is an expert in one or more lifestyles that our home buyers are looking for. Agents can choose from 30 lifestyles such as equestrian, golfing, sailing or even property types like gated communities, ranches, condos and more. Lifestyled agents have more to offer their clients than a real estate license!</p>
				<p>And perhaps best of all - it's <?php echo $isFree ? 'free!' : "$".$discount." off!"; ?></p>
			</div>
		</div>
	</div>
	<div class="mid">
		<div class="left">
			<span class="title"><span style="color:#F9A054">Why</span> become a Lifestyled Agent?</span>
			<p>Because you have knowledge, insight and passion of certain lifestyles that can truly help our buyers.</p>
			<p class="mobilespace">Studies like NAR's Buyer and Seller Profile and Better Homes and Gardens survey show that buyers are most concerned with trust, communication, and insight into the local lifestyle. <span style="font-weight:400">Sharing a common passion or being an expert in the client's needs is the perfect foundation for building a strong relationship of trust, understanding, and value to the consumer.</span></p>
		</div>
		<div class="divider">
			<div class="line1"></div>
			<span>+</span>
			<div class="line2"></div>
		</div>
		<div class="right">
			<span class="title"><span style="color:#F9A054">What's</span> in it for you?</span>
			<p>How about being the preferred agent that shares the lifestyle with the buyer?</p>
			<p class="mobilespace" style="font-weight:400">If you are looking for a new home, wouldn't you rather work with an agent that shares a passion with you, or is an expert about the particular property type you’re searching for?  By matching you to a specific buyer interests and needs we recommend you as the best agent for them.</p>
		</div>
	</div>
	<div class="circle">
		<span class="title">Why are we so different?</span>
		<p>You'll get the opportunity to work with buyers that are carefully matched to your exact lifestyle. This will bring agents and buyers closer than ever before.</p>
		<p style="width: 60%; margin: 20px auto 0;">Lifestyled Agents are agents that make a difference.</p>
	</div>
	<div class="bottom"></div>
	<div class="calltoaction">
		<a href="#"><span class="entypo-up-open-mini"></span> Sound Good? Sign up for free now! <span class="entypo-up-open-mini"></span></a>
	</div>
</div>
