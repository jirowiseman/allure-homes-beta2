<?php

require_once(__DIR__.'/../_classes/Options.class.php'); $Options = new AH\Options();
global $thisPage;   
$opt = $Options->get((object)['where'=>['opt'=>'VideoList']]);
$videoId = 166059782;
if (!empty($opt)) {
    $videoList = json_decode($opt[0]->value);
    foreach($videoList as $page=>$video) {
        if (strtolower($page) == $thisPage) {
            $videoId = $video;
            break;
        }
    }
}

?>

<div id="page-about">
  	<div class="top">
				<!--<div class="title">
					<span class="title1">Taking you</span>
					<span class="title2">from what you know, to</span>
					<span class="title3">What is</span>
					<span class="title4">Possible</span>
				</div>-->
				<p class="headertext">We believe that when a buyer is matched with the right location, the right home, and the right agent their experience is exceptional.</p>
        <div class="text">
            <p>Every year more than 3.1 million households relocate further than 50 miles from their current residence, and in 2014 second home purchases accounted for more than 20% of all residential  sales. For those who are relocating we wanted to create the best possible experience when they start their search online. The starting point is giving buyers a search process that mimics the way that agents help their own buyer clients, the end is connecting them with you.</p>
            <p>It was designed and conceived by a hollywood experience builders in collaboration with real estate professionals to give home buyers a fresh and awesome new way to find everything they need to make great real estate decisions. The <span class="notranslate">LifeStyled Listings</span> style is fun and motivating, it comes across like a dream building exercise, but surprises you when you see how obtainable your results are.</p>
					<p><span class="notranslate">LifeStyled Listings</span> addresses those homebuyers driven by specific lifestyle needs or aspirations. However, even primary home buyers can't help but wonder where is the best place for me to live? They have a natural drive to escape their day to day lifestyle and to explore and discover what’s available on the market.</p>
        </div>
		<div class="video">
			<iframe src="https://player.vimeo.com/video/<?php echo $videoId; ?>" width="900" height="550" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			<a href="<?php bloginfo('wpurl'); ?>/quiz/#sq=0">Start Our Quiz</a>
		</div>
    </div>
    <!--<div class="team">
        <div class="left">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-about/ryan.png"/>
            <div class="text">
                <span class="title">Ryan Buckholdt</span>
                <span class="subtitle">Co-Founder</span>
                <div class="line"></div>
                <p>Ryan is all about the numbers. With 15 years in the mortgage lending business and a healthy sense of competition, Ryan weighs seeks out ways to gain an edge on the competition. Whether it’s a single transaction or a multi-million dollar company, if your source of value comes from hurting one of the parties involved you are doomed to fail. Lifestyled was born under this philosophy and stays the course that empowers agents and their clients simultaneously.</p>
            </div>
        </div>
        <div class="right">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-about/bernice.png"/>
            <div class="text">
                <span class="title">Bernice Wong</span>
                <span class="subtitle">Co-Founder</span>
                <div class="line"></div>
                <p>Bernice is our guiding compass when it comes to serving agents. As a top performing real estate agent for over 30 years, she has seen it all and knows what stands the test of time in the industry and what doesn’t. You can thank Bernice for the company motto “You can’t be a winner by creating losers”. Whether it’s a single transaction or a multi-million dollar company, if your source of value comes from hurting one of the parties involved you are doomed to fail. Lifestyled was born under this philosophy and stays the course that empowers agents and their clients simultaneously. On the weekends you can find BB relaxing in her dream home in the foothills of Capitola, chasing down her heritage in China, or travelling to an industry event for one of the dozen organizations she is active in.</p>
            </div>
        </div>
        <div class="left">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-about/john.png"/>
            <div class="text">
                <span class="title">John Spagnola</span>
                <span class="subtitle">Creative Director</span>
                <div class="line"></div>
                <p>John’s favorite way to start a sentence is “What if we…” As the creative director of Lifestyled his job is to push the envelope when it comes to design and function. John caught the creative bug at the ripe age of 12 when he picked up the French Horn. Classical music took him all over the country performing with orchestras and feeling the power of what is possible when you aim the collective will of creative people in a single direction. John did just that when he co-founded a successful production and branding company, Bishop Lyons, in 2009. The company quickly grew and gained a reputation for bringing the highest standards in post production under his creative guidance. You can find him on the weekends poolside, exercising, or enjoying the beach.</p>
            </div>
        </div>
        <div class="right">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-about/jiro.png"/>
            <div class="text">
                <span class="title">Jiro Wiseman</span>
                <span class="subtitle">Creative Director</span>
                <div class="line"></div>
                <p>Jiro’s job is to keep the team working efficiently and sorting the ideas with potential from the pipedreams. Nicknamed “The Dream Squasher”, he spends his time diving into market research, competitive analysis, and crunching numbers so that Allure can choose where to direct its efforts wisely. Jiro earned a B.A Business Management and Economics from the University of California at Santa Cruz, completed 2 years of post-baccalaureate study in Psychology, and is currently working towards a Masters in Urban and Regional Planning with a certificate in Commercial and Residential Development at San Jose State University. On the weekends you probably can’t find him because he’s never doing the same thing twice.</p>
            </div>
        </div>
        <div class="left">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-about/tomshani.png"/>
            <div class="text">
                <span class="title">Tom Shani</span>
                <span class="subtitle">Lead Programmer</span>
                <div class="line"></div>
                <p>It’s rare to find a 26 year old with 16 years of programming under his belt, but that’s exactly what Tom is. A recent graduate of the prestigious Berklee College of Music, he used his love of music to fuel his exploration of programming languages. Tom built Lifestyled from the ground up while allowing us the technical space to explore one-of-a-kind functionality. On the weekends you can find him building the most off the wall gadgets you’ve ever seen that combine music with avant garde performance.</p>
            </div>
        </div>
        <div class="right">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-about/kendall.png"/>
            <div class="text">
                <span class="title">Kendall Lamkin</span>
                <span class="subtitle">Lead Designer</span>
                <div class="line"></div>
                <p>As lead designer for Allure, Kendall is responsible for the look and feel of Lifestyled. As a violinist since age 4, Kendall has spent most of his life surrounded by music and perfecting his creative side. Prior to Lifestyled he worked with his business partner John Spagnola to co-found Bishop Lyons, in 2009. On the weekends you can find him playing basketball or composing music for his own enjoyment on his custom built computer.</p>
            </div>
        </div>
        <div class="left">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-about/tomtong.png"/>
            <div class="text">
                <span class="title">Tom Tong</span>
                <span class="subtitle">Programmer</span>
                <div class="line"></div>
                <p>“Double T” joined Allure hungry for a new creative outlet. Having explored the extent of software engineering in complex control systems for heavy machinery in the semiconductor industry, Tom was ready to change gears and enter web based, social interfacing development. Tom’s biggest achievements, besides raising his two children, are completing the world's first fully operational Factory Automation software 300mm reticle tool for KLA-Tencor running at Global Foundries in New York, and designing and implementing the upgrade to the National Missile Defense pre-launch vehicle checkout system, protecting all of us from intercontinental ballistic missiles. You can find him on the weekends taking his kids to French, Kung-Fu, violin, and Mandarin lessons or cruising  in his classic Porche Cabriolet.</p>
            </div>
        </div>
    </div>-->
</div>