<?php
global $thisPage;

require_once(__DIR__.'./../_classes/Tags.class.php'); $Tags = new AH\Tags(); $Tags = $Tags->get(array('sortby'=>'tag'));
require_once(__DIR__.'./../_classes/TagsCategories.class.php'); $TagsCategories = new AH\TagsCategories(); $TagsCategories = $TagsCategories->get();
require_once(__DIR__.'./../_classes/TagsTaxonomy.class.php'); $TagsTaxonomy = new AH\TagsTaxonomy(); $TagsTaxonomy = $TagsTaxonomy->get();
require_once(__DIR__.'./../_classes/SessionMgr.class.php'); $SessionMgr = new AH\SessionMgr();
require_once(__DIR__.'./../_classes/Options.class.php'); $Options = new AH\Options();
require_once(__DIR__.'./../_classes/Logger.class.php'); $Logger = new AH\Logger(1, $thisPage);
require_once(__DIR__.'./../_classes/States.php');

$SessionMgr->log("entered page-quiz-results.php");


function getLastQuiz($session_id) {
	require_once(__DIR__.'./../_classes/QuizActivity.class.php'); $QuizActivity = new AH\QuizActivity();
	// $session_id = $_COOKIE['PHPSESSID'];
	try {
		if (empty($session_id) ) throw new \Exception('No session id provided.');
		$activity = $QuizActivity->get( (object) array('where'=>array('session_id'=>$session_id)) );
		if (empty($activity)) { return false; }
		else {
			$activity = array_pop($activity);
			$size = count($activity->data);

			$a = $activity->data;
			$index = count($a) > 0 ? count($a)-1 : 0;
			$b = &$a[$index];
			$newData = [];
			while ($index >= 0 && !isset($b->results)){
				$index--;
				$b = &$a[$index];
			}
			if (empty($b->results)) return false;		
			// remove all non-result data from the activity..
			for($i = 0; $i <= $index; $i++)
				$newData[] = $a[$i];
			$QuizActivity->set([(object)['where'=>['session_id'=>$session_id],
										'fields'=>['status'=>'done',
												   'data'=>$newData]]]);					
			return $index;
		}
	} catch (\Exception $e) { 
		parseException($e); 
		return false;
	}
}

function validQuizId($index, $session_id) {
	require_once(__DIR__.'./../_classes/QuizActivity.class.php'); $QuizActivity = new AH\QuizActivity();
	//$session_id = $_COOKIE['PHPSESSID'];
	try {
		if (empty($session_id)) throw new \Exception('No session id provided.');
		$activity = $QuizActivity->get( (object) array('where'=>array('session_id'=>$session_id)) );
		if (empty($activity)) { return false; }
		else {
			$activity = array_pop($activity);
			$size = count($activity->data);

			if ($activity->status != 'done')
				return false;

			$a = $activity->data;
			if ( $index >= $size )
				return false;

			$b = &$a[$index];
			if (empty($b->results)) return false;							
			return true;
		}
	} catch (\Exception $e) { 
		parseException($e); 
		return false;
	}
}

foreach ($TagsCategories as &$category){
	$category->tags = array();
	foreach ($TagsTaxonomy as $tax)
		if ($tax->category_id == $category->id)
			foreach ($Tags as $tag) 
				if ($tag->id == $tax->tag_id && $tag->type <= 1){ $category->tags[] = $tag; break; }
	usort($category->tags,function($a,$b){ return strcmp($a->tag, $b->tag); });
}
usort($TagsCategories,function($a,$b){ return strcmp($a->category, $b->category); });
$input = get_query_var('id');
$extra = get_query_var('extra');
$id = 0;
$sessionID = $SessionMgr->getCurrentSession($id);
$quizId = -1;
$hash = '';
global $timezone_adjust; // from Utility

$Logger->log("id:$input, extra:$extra");

// check if this is a compound input <quizID>:<sessionID>
if (empty($input)) {
	require_once(__DIR__.'./../_classes/Sessions.class.php'); $Sessions = new AH\Sessions();
	$ses = $Sessions->get((object)['where'=>['id'=>$id]]);
	if (!empty($ses) && 
		!empty($ses[0]->quiz_id)) {
		$input = $ses[0]->quiz_id;
		$hash = 'Reentry';
	}
}
elseif (strpos($input, ":")) {
	$input = explode(":", $input);
	$input = $input[0]; // this would be the quizId, row indes from QuizActivity
}
elseif (strpos($input, "-") != false) {
		$parts = explode("-", $input);
		$input = $parts[1];
		
		if (strpos($parts[0][0], "R") !== false) { // run quiz
			$hash = "RunQuiz";
			$referer = isset($_SERVER["HTTP_REFERER"]) ? (!empty($_SERVER["HTTP_REFERER"]) ? strtolower($_SERVER["HTTP_REFERER"]) : "") : '';
			$date = date("Y-m-d H:i:s", time() + ($timezone_adjust*3600));
			global $browser;
			require_once(__DIR__.'./../_classes/Analytics.class.php'); $Analytics = new AH\Analytics();
			$from = '';
			if (strlen($parts[0]) > 1) switch($parts[0][1]) {
				case 'F': $from = "Facebook"; break;
				case 'I': $from = "LinkedIn"; break;
				case 'G': $from = "Google+"; break;
				case 'T': $from = "Twitter"; break;
				case 'M': $from = "Email"; break;
				case 'C': $from = "Contact"; break;
			}
			$Analytics->add(['type'=>ANALYTICS_TYPE_EVENT,
							 'origin'=>'quiz-results',
							 'session_id'=>$id,
							 'referer'=>$referer,
							 'what'=>'sharing',
							 'value_str'=>$from,
							 'value_int'=>$input,
							 'added'=>$date,
							 'browser'=>$browser['shortname'],
							 'connection'=>$browser,
							 'ip'=>$SessionMgr->userIP()]);
		}
		elseif (strpos($parts[0][0], "L") !== false) { // reload from listing
			$hash = 'Reload';
		}
}

is_numeric($input) ? $quizId = $input : $hash = $input;
$incomingQuizId = intval($quizId); // save it, else may get forced to -1

if (isset($quizId) && empty($quizId) && $quizId !== 0 && $quizId !== '0')
	$quizId = $incomingQuizId != -1 ? $incomingQuizId : (isset($_COOKIE['QuizCompleted']) ? $_COOKIE['QuizCompleted'] : -1);
else if ($hash !== 'RunQuiz' &&
		 $hash !== 'Reload' &&
	     $hash !== 'Reentry') {
	$quizId = $incomingQuizId != -1 ? $incomingQuizId : -1;
	if ($incomingQuizId != -1) $hash = 'Reload';
}

$gotBadQuizId = false;

$opt = $Options->get((object)['where'=>['opt'=>'NO_ASK_ALLOW_MIN_PRICE']]);
$noAskAllowMinPrice = !empty($opt) ? intval($opt[0]->value) : 800000;
$opt = $Options->get((object)['where'=>['opt'=>'ListHubFeedSize']]);
$listHubFeedSize = !empty($opt) ? intval($opt[0]->value) : 215000;
$opt = $Options->get((object)['where'=>['opt'=>'PrefetchImagesIntoBuffer']]);
$prefetchImagesIntoBuffer = !empty($opt) ? intval($opt[0]->value) : 0;

$priceList = null;
$priceListMax = null;
$lastPrice = 20000000;
if ($noAskAllowMinPrice < 800000) {
	$base = $noAskAllowMinPrice / 1000;
	$firstPrice = $noAskAllowMinPrice+1;
	$priceList = (object)[$noAskAllowMinPrice=>"Min Price",
						  $firstPrice=>'$'.number_format($base).'K'];
	
	for( $start = $noAskAllowMinPrice+50000; $start <= 800000; $start += 50000)
		$priceList->$start = '$'.number_format($start/1000).'K';

	if ($start > 800000) {// oops skipped 
		$start = 800000;
		$priceList->$start = '$'.number_format($start/1000).'K';
	}

	$count = 0;
	for( $start = 1000000; $start < 2000000; $start += 200000, $count++)
		$priceList->$start = '$'.number_format($start/1000000, $count == 0 ? 0 : 1).' Million';

	$count = 0;
	for( $start = 2000000; $start < 5000000; $start += 500000, $count++)
		$priceList->$start = '$'.number_format($start/1000000, $count % 2).' Million';

	for( $start = 5000000; $start < 10000000; $start += 1000000)
		$priceList->$start = '$'.number_format($start/1000000).' Million';

	for( $start = 10000000; $start <= 16000000; $start += 2000000)
		$priceList->$start = '$'.number_format($start/1000000).' Million';

	// max price list
	$base = $noAskAllowMinPrice + 100000;
	$priceListMax = (object)[];
	
	for( $start = $base; $start <= 800000; $start += 50000)
		$priceListMax->$start = '$'.number_format($start/1000).'K';

	if ($start > 800000) {// oops skipped 
		$start = 800000;
		$priceListMax->$start = '$'.number_format($start/1000).'K';
	}

	$count = 0;
	for( $start = 1000000; $start < 2000000; $start += 200000, $count++)
		$priceListMax->$start = '$'.number_format($start/1000000, $count == 0 ? 0 : 1).' Million';

	$count = 0;
	for( $start = 2000000; $start < 5000000; $start += 500000, $count++)
		$priceListMax->$start = '$'.number_format($start/1000000, $count % 2).' Million';

	for( $start = 5000000; $start < 10000000; $start += 1000000)
		$priceListMax->$start = '$'.number_format($start/1000000).' Million';

	for( $start = 10000000; $start <= 16000000; $start += 2000000)
		$priceListMax->$start = '$'.number_format($start/1000000).' Million';

	$start = $lastPrice-1;
	$priceListMax->$start = '$'.number_format($lastPrice/1000000).' Million';
	$priceListMax->$lastPrice = 'Max Price';
}

$emailBody = 'Go check out this search results at Lifestyled Listings!  Copy and paste into browser: '.get_home_url().'/'.$thisPage.'/RM-%quiz-id%';
$emailSubject = 'Check out this search I did at Lifestyled Listings!';
$emailHref = "mailto:?subject=$emailSubject&body=$emailBody";

$SessionMgr->log("page-quiz-results: outputting page data, extra:$extra");
global $all_tag_city;

$user = null;
if (is_user_logged_in() )
	$user = wp_get_current_user();

$opt = $Options->get((object)['where'=>['opt'=>'QuizOptions']]);
$quizOptionList = empty($opt) ? [] : json_decode($opt[0]->value);
$quizOptions = null;
if (!empty($quizOptionList) ) {
	if (!empty($user)) foreach($quizOptionList as $item) {
		if ($item->agentID == $user->ID) {
			$quizOptions = $item;
			break;
		}
		unset($item);
	}
	// if we didn't find a match, see if there is a global one
	if (empty($quizOptions)) foreach($quizOptionList as $item) {
		if ($item->agentID == -1) {
			$quizOptions = $item;
			break;
		}
		unset($item);
	}
}

global $ALR;
$portalAgent = $ALR->get('portal-agent');
$photo = get_template_directory_uri()."/_img/_authors/250x250/".(!empty($portalAgent) && !empty($portalAgent->photo) ? $portalAgent->photo : '_blank.jpg');
$agentText = "I am here to assist you to find the best home in this area for you.  I listen to your needs and help guide you in knowing the area and it's abundant resources.";
if (!empty($portalAgent)) foreach($portalAgent->meta as $meta)
	if ($meta->action == SELLER_PORTAL_MESSAGE) {
		$agentText = AH\removeslashes($meta->message);
		break;
	}

$opt = $Options->get((object)['where'=>['opt'=>'QuizResultsAgentOptions']]);
$agentOptionsList = empty($opt) ? [] : json_decode($opt[0]->value);
$agentOptions = null;
if (!empty($agentOptionsList)) {
	if (!empty($portalAgent)) foreach($agentOptionsList as $item) 
		if (!empty($portalAgent) &&
			$item->agentID == $portalAgent->author_id) {
			$agentOptions = $item;
			break;
		}

	if (empty($agentOptions)) foreach($agentOptionsList as $item) 
		if ($item->agentID == -1) { // global
			$agentOptions = $item;
			break;
		}
}
$nameMode = PORTAL_LANDING_MUST_HAVE;
$phoneMode = !empty($agentOptions) ? $agentOptions->phoneMode : PORTAL_LANDING_NOT_USED;
$emailMode = !empty($agentOptions) ? $agentOptions->emailMode : PORTAL_LANDING_MUST_HAVE;
$queryMode = !empty($agentOptions) && isset($agentOptions->queryMode) ? $agentOptions->queryMode : PORTAL_LANDING_NOT_USED;
$fbMode = !empty($agentOptions) && isset($agentOptions->fbMode) ? $agentOptions->fbMode : PORTAL_LANDING_NOT_USED;
$markerMode = !empty($agentOptions) && isset($agentOptions->markerMode) ? $agentOptions->markerMode : PORTAL_LANDING_NOT_USED;
$textMessageOnEmailCapture = !empty($agentOptions) && isset($agentOptions->textMessageOnEmailCapture) ? $agentOptions->textMessageOnEmailCapture : PORTAL_LANDING_NOT_USED;
$forcePhoneAsPasswordIfUsed = !empty($agentOptions) && isset($agentOptions->forcePhoneAsPasswordIfUsed) ? $agentOptions->forcePhoneAsPasswordIfUsed : PORTAL_LANDING_NOT_USED;
if ($queryMode != PORTAL_LANDING_NOT_USED &&
	$emailMode < PORTAL_LANDING_MUST_HAVE) // force email
	$emailMode = PORTAL_LANDING_MUST_HAVE;
// 0 - not used
// 1 - optional
// 2 - must have
// 3 - as password
$emailPlaceHolder = $emailMode == PORTAL_LANDING_NOT_USED ? '' :
					($emailMode == PORTAL_LANDING_OPTIONAL ? 'Enter Email (optional)' : 
					 ($emailMode == PORTAL_LANDING_MUST_HAVE ? 'Enter Email' : 'Enter Email as Password'));
$phonePlaceHolder = $phoneMode == PORTAL_LANDING_NOT_USED ? '' :
					($phoneMode == PORTAL_LANDING_OPTIONAL ? 'Phone # as password (optional)' : 
					 ($phoneMode == PORTAL_LANDING_MUST_HAVE ? 'Enter Phone Number' : 'Phone # as password'));


$opt = $Options->get((object)['where'=>['opt'=>'QuizResultsPortalUserCaptureOptions']]);
$quizResultsPortalUserCaptureOptions = empty($opt) ? [] : json_decode($opt[0]->value);
$captureOptions = null;
if (!empty($quizResultsPortalUserCaptureOptions) ) {
	if (!empty($portalAgent)) foreach($quizResultsPortalUserCaptureOptions as $item) {
		if (!empty($portalAgent) &&
			$item->agentID == $portalAgent->author_id) {
			$captureOptions = $item;
			break;
		}
		unset($item);
	}
	// if we didn't find a match, see if there is a global one
	if (empty($captureOptions)) foreach($quizResultsPortalUserCaptureOptions as $item) {
		if ($item->agentID == -1) {
			$captureOptions = $item;
			break;
		}
		unset($item);
	}
}

if (empty($captureOptions)) {
	$captureOptions = (object)[	'agentID'=>-1,
								'byPassCount'=>(object)['quizLoad'=>0,
														'viewListing'=>0],
								'quizLoad'=>(object)['captureMode'=>CAPTURE_NONE],
								'cityScroll'=>(object)[ 'page'=>1,
														'captureMode'=>CAPTURE_NONE],
								'listingScroll'=>(object)['page'=>1,
														  'captureMode'=>CAPTURE_NONE],
								'viewListing'=>(object)['captureMode'=>CAPTURE_NONE] ];
}

$form = wp_login_form( array( 	'echo' => false, 
								'redirect' => site_url(). "/quiz-results/-1",
								'label_username' => __( 'Username:' ), 
								'label_password' => __( 'Password:' ), 
								'form_id' => 'login-form', 
								'remember' => false) );
$form = explode('</form>', $form);
$form = str_replace("\n","", $form[0]);
$form = str_replace("\t","", $form);
$form = str_replace("\r","", $form);
$lostPasswordUrl = wp_lostpassword_url();

?>
<script type="text/javascript">
	var quiz_id = <?php echo $quizId; ?>;
	var incomingQuizId = <?php echo $incomingQuizId; ?>;
	var hash = '<?php echo empty($hash) ? 0 : $hash; ?>'; 
	setCookie("QuizCompleted", quiz_id, 2);
	console.log("quiz-results.php set quiz_id to "+quiz_id);
	tags_categories = <?php echo json_encode($TagsCategories); ?>;
	ah_local.sessionID = '<?php echo $sessionID; ?>';
	var activePulldown = -1;
	var quizProfile = null;
	var tagsEdited = false;
	var noAskAllowMinPrice = <?php echo $noAskAllowMinPrice; ?>;
	var priceList = <?php echo json_encode($priceList); ?>;
	var priceListMax = <?php echo json_encode($priceListMax); ?>;
	var lastPrice = <?php echo $lastPrice; ?>;
	var listHubFeedSize = <?php echo $listHubFeedSize; ?>;
	var quizPagePer = <?php echo QUIZ_PAGE_PER; ?>;
	var all_tag_city = <?php echo json_encode($all_tag_city); ?>;
	var captureOptions = <?php echo json_encode($captureOptions); ?>;
	var nameMode = <?php echo $nameMode ? $nameMode : 0; ?>;
	var phoneMode = <?php echo $phoneMode ? $phoneMode : 0; ?>;
	var emailMode = <?php echo $emailMode ? $emailMode : 0; ?>;
	var queryMode = <?php echo $queryMode ? $queryMode : 0; ?>;
	var fbMode = <?php echo $fbMode ? $fbMode : 0; ?>;
    var markerMode = <?php echo $markerMode ? $markerMode : 0; ?>;
    var textMessageOnEmailCapture = <?php echo $textMessageOnEmailCapture ? $textMessageOnEmailCapture : 0; ?>;
    var forcePhoneAsPasswordIfUsed = <?php echo $forcePhoneAsPasswordIfUsed ? $forcePhoneAsPasswordIfUsed : 0; ?>;
	var command = '<?php echo !isset($extra) || empty($extra) ? "none" : $extra; ?>';
	var prefetchImagesIntoBuffer = <?php echo $prefetchImagesIntoBuffer ? 1 : 0 ?>;
	var havePortalOwner  = <?php echo !empty($portalAgent) ? 1 : 0 ?>;
	var portalAgent = <?php echo $portalAgent != "0" ? json_encode($portalAgent) : "0"; ?>;
	var login_form = '<?php echo trim($form); ?>';
	var lostPasswordUrl = '<?php echo $lostPasswordUrl; ?>';
</script>
<script type="text/javascript">
	
	function setupMobileTagSelector() {
		if (quizProfile == null ||
			  typeof quizProfile.query == 'undefined') {
			window.setTimeout(function() {
				setupMobileTagSelector();
			}, 200);
			return;
		}
		var h = quizProfile.makeTagSelector();
		$('.dropdown .add-tags .add-tags-content').html(h);
		quizProfile.setTagSelectorHandlers();
	}
	
//	function mobileselectMyprofilesetup() {
//		if (quizProfile == null ||
//			  typeof quizProfile.query == 'undefined') {
//			window.setTimeout(function() {
//				mobileselectMyprofilesetup();
//			}, 200);
//			return;
//		}
//		var h = quizProfile.mobileselectMyprofile();
//		$('.dropdown .searchcollapse').html(h);
//		quizProfile.mobileselectMyprofile();
//	}
	
	function mobilecitysetup() {
		if (quizProfile == null ||
			  typeof quizProfile.query == 'undefined') {
			window.setTimeout(function() {
				mobilecitysetup();
			}, 200);
			return;
		}
		var h = quizProfile.mobilecityselector();
		$('.dropdown .my-cities .citiescollapse').html(h);
		quizProfile.setmobilecityselector();
	}
	
	jQuery(document).ready(function($){
		$('.mobile-nav li.edit-tags a.mlink').on('click', function() {
			if ($('.mobile-nav li.edit-tags a.mlink').hasClass( 'active' )) {
				$('.mobile-nav li a.mlink').removeClass( 'active' );
				$('.mobile-nav ul.resultsnav .blank').hide();
			}
			else {
				$('.mobile-nav li a.mlink').removeClass( 'active' );
				$('.mobile-nav li.edit-tags a.mlink').addClass( 'active' );
				$('.dropdown .add-tags .add-tags-content').empty();
				$('.mobile-nav ul.resultsnav .blank').show();
			}
			var val = $('.mobile-nav li.edit-tags').attr('id');
			if (activePulldown != -1)
				$('.mobile-nav li[id="'+activePulldown+'"] .dropdown').slideToggle('fast');
			if (activePulldown != val) {
				$('.mobile-nav li[id="'+val+'"] .dropdown').slideToggle('fast');
				activePulldown = val;
				// setupMobileTagSelector();
			}
			else {
				activePulldown = -1;
			}
		});
		$('.mobile-nav li.add-tags-main a.mlink').on('click', function() {
			if ($('.mobile-nav li.add-tags-main a.mlink').hasClass( 'active' )) {
				$('.mobile-nav li a.mlink').removeClass( 'active' );
				$('.dropdown .add-tags .add-tags-content').empty();
				$('.mobile-nav ul.resultsnav .blank').hide();
			}
			else {
				$('.mobile-nav li a.mlink').removeClass( 'active' );
				$('.mobile-nav li.add-tags-main a.mlink').addClass( 'active' );
				$('.mobile-nav ul.resultsnav .blank').show();
			}
			var val = $('.mobile-nav li.add-tags-main').attr('id');
			if (activePulldown != -1)
				$('.mobile-nav li[id="'+activePulldown+'"] .dropdown').slideToggle('fast');
			if (activePulldown != val) {
				$('.mobile-nav li[id="'+val+'"] .dropdown').slideToggle('fast');
				activePulldown = val;
				setupMobileTagSelector();
			}
			else {
				activePulldown = -1;
			}
		});
		$('.mobile-nav li.change-area a.mlink').on('click', function() {
			if ($('.mobile-nav li.change-area a.mlink').hasClass( 'active' )) {
				$('.mobile-nav li a.mlink').removeClass( 'active' );
				$('.mobile-nav ul.resultsnav .blank').hide();
			}
			else {
				$('.mobile-nav li a.mlink').removeClass( 'active' );
				$('.mobile-nav li.change-area a.mlink').addClass( 'active' );
				$('.dropdown .add-tags .add-tags-content').empty();
				$('.mobile-nav ul.resultsnav .blank').show();
			}
			var val = $('.mobile-nav li.change-area').attr('id');
			if (activePulldown != -1)
				$('.mobile-nav li[id="'+activePulldown+'"] .dropdown').slideToggle('fast');
			if (activePulldown != val) {
				$('.mobile-nav li[id="'+val+'"] .dropdown').slideToggle('fast');
				activePulldown = val;
			}
			else
				activePulldown = -1;
		});
		$('.mobile-nav li.option a.mlink').on('click', function() {
			if ($('.mobile-nav li.option a.mlink').hasClass( 'active' )) {
				$('.mobile-nav li a.mlink').removeClass( 'active' );
			}
			else {
				$('.mobile-nav li a.mlink').removeClass( 'active' );
				$('.mobile-nav li.option a.mlink').addClass( 'active' );
				$('.dropdown .add-tags .add-tags-content').empty();
			}
			var val = $('.mobile-nav li.option').attr('id');
			if (activePulldown != -1)
				$('.mobile-nav li[id="'+activePulldown+'"] .dropdown').slideToggle('fast');
			if (activePulldown != val) {
				$('.mobile-nav li[id="'+val+'"] .dropdown').slideToggle('fast');
				activePulldown = val;
			}
			else
				activePulldown = -1;
		});
		$('.mobile-nav li.add-tags-main button').on('click', function() {
				quizProfile.saveTagSelection();
				activePulldown = -1;
				$('.mobile-nav li .dropdown').hide();
				$('.mobile-nav .centerarrow').click();
				$('.mobile-nav li a.mlink').removeClass( 'active' );
				$('.dropdown .add-tags .add-tags-content').empty();
				$('.mobile-nav ul.resultsnav .blank').hide();
		});
		$('.mobile-nav li.change-area button').on('click', function() {
				activePulldown = -1;
				$('.mobile-nav li .dropdown').hide();
				$('.mobile-nav .centerarrow').click();
				$('.mobile-nav li a.mlink').removeClass( 'active' );
				$('.dropdown .add-tags .add-tags-content').empty();
				$('.mobile-nav ul.resultsnav .blank').hide();
		});
	})
</script>
<script type="text/javascript">
		jQuery(document).ready(function($){
			$('.mobile-nav .centerarrow').on('click', function() {
				transitionEnd = 'transitionend webkitTransitionEnd otransitionend MSTransitionEnd';
				if ($('.resultsnav').hasClass('animatedown')){
					$('#quiz-results article>section.left-col').css('-webkit-overflow-scrolling','touch');
					$('.mobile-nav li a.mlink').removeClass( 'active' );
					$('.mobile-nav ul.resultsnav .blank').hide();
					activePulldown = -1;
					$('.mobile-nav li .dropdown').hide();
					$('.resultsnav li').removeClass( 'animatein' );
					$('.centerarrow .arrow').removeClass( 'active' );
					$('.centerarrow .arrow .left').removeClass( 'active' );
					$('.centerarrow .arrow .right').removeClass( 'active' );
					timeoutID = window.setTimeout(delayanimateup, 500);
					function delayanimateup() {
						$('.resultsnav').addClass( 'animateup' );
						$('.resultsnav').removeClass( 'animatedown' );
					}
					timeoutID = window.setTimeout(delayanimatehide, 1500);
					function delayanimatehide() {
						$('.resultsnav').removeClass( 'animateup' );
					}
				}
				else {
					$('#quiz-results article>section.left-col').css('-webkit-overflow-scrolling','auto');
					$('.resultsnav').addClass( 'animatedown' );
					$('.resultsnav').removeClass( 'animateup' );
					$('.centerarrow .arrow').addClass( 'active' );
					$('.centerarrow .arrow .left').addClass( 'active' );
					$('.centerarrow .arrow .right').addClass( 'active' );
					timeoutID = window.setTimeout(delayanimatedown, 200);
					function delayanimatedown() {
						$('.resultsnav li').addClass( 'animatein' );
					}
				}
			});
			$('.expand').on('click', function() {
				$('.collapse').slideToggle('fast');
				$('.expand').hide();
				$('.collapsehref').show();
			});
			$('.collapsehref').on('click', function() {
				$('.collapse').slideToggle('fast');
				$('.collapsehref').hide();
				$('.expand').show();
			});
			$('.saveexpand').on('click', function() {
				var saveisDropped = $('.savecollapse').css('display') != 'none';
				if (ah_local.author_id == '0') {	
					return;
					console.log("user wants to register!");
					self.login(redirect, ah_local.wp+'/quiz-results/saveProfile', 'Log in now to save this search.');
				}
				else {
					if (getCookie("QuizNeedSaveProfile") == '1' &&
							!saveisDropped) {
						$('.savecollapse').show();
						$('.searchcollapse').hide();
						$('.newcollapse').hide();
					}
					else
						$('.savecollapse').hide();
				}
			});
			$('.searchexpand').on('click', function() {
				var searchisDropped = $('.searchcollapse').css('display') != 'none';
					if (searchisDropped)
						$('.searchcollapse').hide();
					if ((!searchisDropped) && (ah_local.author_id !== '0'))
						$('.mobile-nav li.more .dropdown').slideToggle('fast');
						activePulldown = -1;
						$('.searchcollapse').hide();
						$('.savecollapse').hide();
						$('.newcollapse').hide();
			});
			$('.newexpand').on('click', function() {
				var newisDropped = $('.newcollapse').css('display') != 'none';
				if (ah_local.author_id == '0') {
					return;
					console.log("user wants to register!");
					self.login(redirect, ah_local.wp+'/quiz-results/saveProfile', 'Log in now to save this search.');
				}
				else {
					if (newisDropped)
						$('.newcollapse').hide();
					if (!newisDropped)
						ah.openModal('find-a-home');
						$('.mobile-nav li.more .dropdown').slideToggle('fast');
						activePulldown = -1;
						$('.newcollapse').hide();
						$('.searchcollapse').hide();
						$('.savecollapse').hide();
				}
			});
			$('.citiesexpand').on('click', function() {
				$('.citiescollapse').slideToggle('fast');
				$('.citiesexpand').hide();
				$('.citiescollapsehref').show();
			});
			$('.citiescollapsehref').on('click', function() {
				$('.citiescollapse').slideToggle('fast');
				$('.citiescollapsehref').hide();
				$('.citiesexpand').show();
			});
			$('.view-selector li.city').on('click', function() {
              checkCitySlider();
			});
			$('.view-selector li.map').on('click', function() {
              checkMapSlider();
			});
			$('.view-selector li.listings').on('click', function() {
              checkListingsSlider();
			});
            
			$('#quiz-results a').on('click', function() {
				timeoutID = window.setTimeout(checkResultsLoad, 750);
			});
			
				/*var handleMove = function (e) {
				var scrollable = false;
				var items = $(e.target).parents();
				$(items).each(function(i,o) {
						if($(o).hasClass("results-scroll")) {
								scrollable = true;
						}
						else if($(o).hasClass("resultsnav")) {
								scrollable = true;
						}
				});
				if(!scrollable)
						e.preventDefault();
				};
				document.addEventListener('touchmove', handleMove, true);*/
			
		});
	
</script>

<div id="agent-verify">
</div>

<div id="imageBuffer" style="display: none;"></div>
<div id="imageBuffer2" style="display: none;"></div>

<?php if (!empty($portalAgent)) : ?>
<div id="confirm-skip" style="display: none;">
	<div class="confirm-popup">
		<div class="title">Are you sure?</div>
		<p>Get full access every time you visit us by entering your phone number (used as password) to create a proper account.</p>
		<div class="options">
			<button class="back-to-signup">Enter password</button>
			<a class="skip-signup">Skip for now</a>
		</div>
	</div>
</div>
<div id="portal-user-capture" style="display: none;">
	<div id="portalAgentCapture">
		<a class="goBack" href="javascript:controller.prepPortalUserCapture();"><span class="entypo-left-open-big"></span></a>
		<button class="closeDiv"><span class="line1"></span><span class="line2"></span></button>
		<div id="agent">
			<img class="desktop-agent" src="<?php echo $photo; ?>" />
			<div id="subintro">
				<span id="title"><span class="portal-color">Sign in with</span></span>
				<span id="subtext"><?php echo $portalAgent->first_name; ?> <?php echo $portalAgent->last_name; ?></span>
			</div>
		</div>
        <div id="pitch">
            <div class="first-pitch-wrapper">
              <div id="first"><span id="first-pitch">Get Full Access</span></div>
              <div id="second"><span id="second-pitch">Save custom searches &amp; view all 1.6+ million listings</span></div>
            </div>
            <div class="second-pitch-wrapper">
              <div id="first"><span id="first-pitch">One more thing</span></div>
              <div id="second"><span id="second-pitch">and you're ready to go</span></div>
            </div>
		</div>
        <div id="userdata" group="one">
			<div id="fbDiv" style="display: none;">
				<div id="fb-root"></div>
				<fb:login-button scope="public_profile,email" onlogin="checkLoginState();" data-size="xlarge">Continue with Facebook</fb:login-button>
				<!-- <a href="#" onclick="fbLogin();"><img src="<?php echo get_bloginfo('template_directory').'/_img/social_media/fb_login.png';?>" id="fb_login"></img></a> -->
				<a href="#" class="fb-fakebutton" onclick="fbLogin();">
                  <img src="<?php echo get_bloginfo('template_directory').'/_img/social_media/facebook-icon.png';?>" id="fb_login"></img>Continue with facebook
                </a>
                <div class="or-wrapper">
                  <span class="line1"></span>
                  <span class="line2"></span>
                  <span id="or">OR</span>
                </div>
			</div>
			<div class="input" id="name">
                <span id="marker" class="status-icon entypo-attention"></span>
				<input type="text" id="one" class="name portalUserInfo" placeholder="Your Name"></input>
			</div>
			<div class="input" id="email">
                <span id="marker" class="status-icon entypo-attention"></span>
				<input type="text" id="one" class="email portalUserInfo" placeholder="<?php echo $emailPlaceHolder ?>"></input>
			</div>
			<div class="input" id="phone">
                <span id="marker" class="status-icon entypo-attention"></span>
				<input type="text" id="one" class="phone portalUserInfo" placeholder="<?php echo $phonePlaceHolder ?>" style="display: none;"></input>
			</div>
			<a class="start" id="one" href="javascript:portal_user_register.start(1, controller.captureEvent);">Sign Up</a>
			<a class="next" id="one" href="javascript:controller.nextStepPortalUserCapture();">Continue with Email</a>
			<span class="warning hidden">*Please enter your info.</span>
			<span id="go-login">Already signed up? <a href="javascript:controller.loginSimple();">Log in</a></span>
		</div>
		<div id="disclaimer">
			<span id="no-like">Don't worry! We don't like spam either.</span>
			<span id="terms">By registering with our site you agree to the website <a href="<?php echo get_home_url()."/terms"; ?>">terms</a>. We protect your personal privacy and security</span>
			<a id="privacy" href="javascript:controller.showPrivacyPolicy();">View privacy policy</a>
		</div>
	</div>
	<div id="portalAgentCaptureLeadIn" style="display: none;">
		<button class="closeDiv"><span class="line1"></span><span class="line2"></span></button>
		<div id="leadInContent">
			<span id="title"></span>
			<span id="message"></span>
		</div>
		<button class="openCapture">Let's go!</button>
	</div>
</div>
<?php endif; ?>

<div id="overlay-bg" style="display: none;">
	<div class="spin-wrap"><div class="spinner sphere"></div></div>
</div>

<div id="quiz-results">
  <div class="global-signin-popup">
      <div class="blackout"></div>
        <div class="signin-content">
          <div class="close">x</div>
          <div class="signin-header"><span style="font-weight:400;">Welcome back!</span> Please sign in</div>
          <!--<div id="fbDiv">
            <div id="fb-root"></div>
            <fb:login-button scope="public_profile,email" onlogin="checkLoginState();" data-size="xlarge">Log in with Facebook</fb:login-button>
            <a href="#" class="fb-fakebutton" onclick="fbLogin();">
              <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/social_media/facebook-icon.png" id="fb_login"></img>Continue with facebook
            </a>
            <div class="or-wrapper">
              <span class="line1"></span>
              <span class="line2"></span>
              <span id="or">OR</span>
            </div>
          </div>-->
        <div class="signin-wrapper">
          <input type="text" name="log" id="user_login" class="input" value="" placeholder="Username">
          <input type="password" name="pwd" id="user_pass" class="input" value="" placeholder="Password">
          <input type="submit" name="wp-submit" id="wp-submit" class="button-primary" value="Log In">
          <input type="hidden" name="redirect_to" value="http://mobile.lifestyledlistings.com/quiz/#sq=0">
          <p class="forgot-password"><a href="'+lostPasswordUrl+'" title="Forgot your password?">Forgot your password?</a></p>
        </div>
      </div>
    </div>
  <main>
	<article>
		<div class="mobile-nav">
			<div class="mobile-nav-header">
				<div class="backbutton">
					<div class="view-selector">
						<div class="largemobile">
							<a class="backtocity" style="display:none;"><span class="entypo-left-open-big"></span> Back to Cities</a>
							<a class="backtolistings" style="display:none;"><span class="entypo-left-open-big"></span> Back to Listings</a>
						</div>
						<div class="smallmobile">
							<a class="backtocity" style="display:none;"><span class="entypo-left-open-big"></span> Back</a>
							<a class="backtolistings" style="display:none;"><span class="entypo-left-open-big"></span> Back</a>
						</div>
					</div>
				</div>
				<div class="centerarrow"><span class="title">Edit Search</span><div class="arrow"><span class="click"><span class="left"></span><span class="right"></span></span></div></div>
				<ul class="view-selector">
					<li class="city leftmost active"><a class="city">City View</a></li>
					<li class="listings" style="display: none;"><a class="list">List View</a></li>
					<li class="map"><a class="map">Map View</a></li>
				</ul>
			</div>
			<ul class="resultsnav">
				<li class="social">
					<div id="shareDiv">
						<a id="facebook-share" href="javascript:shareSocialNetwork('FB_Q',1,'https://www.facebook.com/sharer/sharer.php?u=<?php echo get_home_url().'/'.$thisPage.'/RF-%quiz-id%'; ?>')" target="_blank"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/facebook.png'; ?>" /></a>
						<a id="linkedin-share" href="javascript:shareLinkedIn({type:'LI_Q', comment:'Hi friends check out this search result!', title:'Great listings found at LifeStyled Listings', description:'Find great homes using your lifestyle and favorite things in life!', url:'<?php echo get_home_url().'/'.$thisPage.'/RI-%quiz-id%'; ?>', img:'<?php echo get_template_directory_uri().'/_img/backgrounds/header-fb.jpg'; ?>', where: 1});" target="_blank"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/linkedin.png'; ?>" /></a> 
						<a id="googleplus-share" href="javascript:shareSocialNetwork('GP_Q',1,'https://plus.google.com/share?url=<?php echo get_home_url().'/'.$thisPage.'/RG-%quiz-id%'; ?>')" target="_blank"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/googleplus.png'; ?>" /></a>
						<a id="twitter-share" href="javascript:shareSocialNetwork('TW_Q',1,'https://twitter.com/share?url=<?php echo get_home_url().'/'.$thisPage.'/RT-%quiz-id%'; ?>')" target="_blank"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/twitter.png'; ?>" /></a>
						<a id="email-share" href="javascript:shareSocialNetwork('EM_Q',1,'<?php echo $emailHref; ?>');"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/email.png'; ?>" /></a>
					</div>
				</li>
				<li class="more" id="1">
					<a class="save-profile">Save Search</a>
					<a class="my-profiles">My Searches</a>
					<a class="new-profile">New Search</a>
				</li>
				<li class="option" id="2">
					<a class="mlink">Price and Home Options<span class="expandlink"> <span class="entypo-down-open-big"></span></span><span class="collapselink"> <span class="entypo-up-open-big"></span></span></a>
					<div class="dropdown" style="display: none;">
						<div class="quiz-tags">
							<div class="price-slider">
								<div style="margin:0 auto; text-align:center">
									<select name="min" class="price" id="min"></select>
									<select name="max" class="price" id="max" style="margin-right:0!important"></select>
								</div>
							</div>
							<div class="home-options">
								<div style="margin:0 auto; text-align:center">
									<select name="bed" class="option" id="bed"></select>
									<select name="bath" class="option" id="bath" style="margin-right:0!important"></select>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="change-area" id="3">
					<a class="mlink">Change Search Area<span class="expandlink"> <span class="entypo-down-open-big"></span></span><span class="collapselink"> <span class="entypo-up-open-big"></span></span></a>
					<div class="dropdown" style="display: none;">
						<div class="quiz-select">
							<span class="title">Where do you want to look?</span>
							<div class="chooseQuiz"> <!-- even == city, odd == nationwide or statewide -->
								<span class="fullsearch"><a href="javascript:;" class="choose selected" quiz="<?php echo QUIZ_NATION; ?>"><span class="circle active"></span>Anywhere in the U.S.</a></span>
								<span class="specificsearch"><a href="javascript:;" class="choose" quiz="<?php echo QUIZ_LOCALIZED; ?>"><span class="circle"></span><span id="title">Near a City or State</span></a></span>
							</div>
							<div class="specific">
								<div class="autocomplete mobile">
									<input type="text" class="inactive" placeholder="Enter City or State"></input>
								</div>
								<div class='searchexact selectDistance mod'>
									<span class='selected'></span>
									<span class='selectArrow'></span>
									<ul id="distance" class="distance"></ul>
								</div>
								<?php if ($quizOptions && $quizOptions->showListingFilter) : ?>
								<div id="median-range" class="mobile" style="display: none">
									<!-- <span id="msg">Listings filter</span> -->
									<div id="radio-item"  style="display: none"><input type="checkbox" name="no-limit" value="0" checked><span>No median price limit</span></div>
									<div id="radio-item"><input type="checkbox" name="commercial-ok" value="1"><span>Include commercial</span></div>
									<div id="radio-item"><input type="checkbox" name="rental-ok" value="2"><span>Include rental</span></div>
									<div id="radio-item"><input type="checkbox" name="rental-only" value="3"><span>Only rentals</span></div>
								</div>
								<?php endif; ?>
							</div>
							<div id="control">
								<button id="runQuiz">Save and Apply</button>
							</div>
			  		</div>
					</div>
				</li>
				<li class="add-tags-main" id="4">
					<a class="mlink">Add Tags<span class="expandlink"> <span class="entypo-down-open-big"></span></span><span class="collapselink"> <span class="entypo-up-open-big"></span></span></a>
					<div class="dropdown" style="display: none;">
						<button class="saveandclose">Save and Apply</button>
						<div class="add-tags">
							<div class="add-tags-content"></div>
						</div>
					</div>
				</li>
				<li class="edit-tags" id="5">
					<a class="mlink">Edit Tags<span class="expandlink"> <span class="entypo-down-open-big"></span></span><span class="collapselink"> <span class="entypo-up-open-big"></span></span></a>
					<div class="dropdown" style="display: none;">
						<!--<button class="saveandclose">Save and Apply</button>-->
						<div class="editable-tags">
							<span class="title-tags">Edit and refine your tags below.</span>
							<!--<span class="subtitle"><span class="questioncircle"><span>?</span></span> Explain to me how this works.</span>-->
							<div class="quiz-tags">
								<div class="tags-wrap tag-editor">
									<div class="main-header">
										<div class="top">
											<span class="fh-switch" style="position: absolute;margin-left: 5px;">
												<input type="checkbox" id="switch-id" name="myswitch" checked />
												<label for="switch-id" style="cursor:default;"></label>
												<span class="fh-switch-knob"></span>
											</span>
											<span class="sub">Toggle Your &quot;Must Haves&quot;</span>
										</div>
									</div>
									<ul class="tags-list scrollbar scrollbar-left"></ul>
									<div id="actions">
										<button id="undo" disabled >Undo</button>
										<button id="apply" disabled >Apply Changes</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</li>
				<div class="blank"></div>
			</ul>
		</div>
		<div class="mobile-modal-wrapper">
			<div class="mobile-tags-modal">
				<div class="header">
					<span class="fh-switch" style="position: absolute; pointer-events: none; margin-left: 39px;">
						<input type="checkbox" id="switch-id" name="myswitch" checked="">
						<label for="switch-id" style="cursor:default;"></label>
						<span class="fh-switch-knob"></span>
					</span>
					<span style="margin-left: 48px;"> Switch On</span></div>
				<div class="sub-header">in the <em>Edit Tags</em> section</div>
				<div class="sub-header">to <span style="font-weight:700;">only</span> see results with</div>
				<div class="sub-header">your "must have" criteria</div>
				<button class="close">Got it!</button>
			</div>
		</div>
		<div class="mobile-modal-bg"></div>
		<section class="left-col quiz-listings">
			<div class="results-loading-overlay">
				<p>Loading Your Results...</p>
				<div class="spin-wrap">
					<div class="spinner">
						<div class="cube1"></div>
						<div class="cube2"></div>
					</div>
				</div>
			</div>
			<div class="results-error-overlay">
				<p>No listings were found using current search preferences.  Please adjust your price range/search area, untoggle "Must Haves", and/or change tags and retry</p>
				<div id="right-arrow"><span class="entypo-right-open-mini"></span><span class="entypo-right-open-mini" style="left: 6px;"></span></div>
			</div>
			<ul class="view-selector">
				<div class="largedesktop">
					<a class="backtocity" style="display:none;"><span class="entypo-left-open-big"></span> Back to Cities</a>
					<a class="backtolistings" style="display:none;"><span class="entypo-left-open-big"></span> Back to Listings</a>
				</div>
				<div class="smalldesktop">
					<a class="backtocity" style="display:none;"><span class="entypo-left-open-big"></span> Back</a>
					<a class="backtolistings" style="display:none;"><span class="entypo-left-open-big"></span> Back</a>
				</div>
				<li class="city leftmost active"><a class="city"><span style="display: block; margin: 2px 0 0 0;">City View</span></a></li>
				<li class="listings" style="display: none;"><a class="list"><span style="display: block; margin: 2px 0 0 0;">List View</span></a></li>
				<li class="map"><a class="map"><span style="display: block; margin: 2px 0 0 0;">Map View</span></a></li>
				<div id="shareDiv">
					<a id="facebook-share" href="javascript:shareSocialNetwork('FB_Q',1,'https://www.facebook.com/sharer/sharer.php?u=<?php echo get_home_url().'/'.$thisPage.'/RF-%quiz-id%'; ?>')" target="_blank"><img style="vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/facebook-small.png'; ?>" /></a>
					<a id="linkedin-share" href="javascript:shareLinkedIn({type:'LI_Q', comment:'Hi friends check out this search result!', title:'Great listings found at LifeStyled Listings', description:'Find great homes using your lifestyle and favorite things in life!', url:'<?php echo get_home_url().'/'.$thisPage.'/RI-%quiz-id%'; ?>', img:'<?php echo get_template_directory_uri().'/_img/backgrounds/header-fb.jpg'; ?>', where: 1});" target="_blank"><img style="vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/linkedin-small.png'; ?>" /></a> 
					<a id="googleplus-share" href="javascript:shareSocialNetwork('GP_Q',1,'https://plus.google.com/share?url=<?php echo get_home_url().'/'.$thisPage.'/RG-%quiz-id%'; ?>')" target="_blank"><img style="vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/googleplus-small.png'; ?>" /></a>
					<a id="twitter-share" href="javascript:shareSocialNetwork('TW_Q',1,'https://twitter.com/share?url=<?php echo get_home_url().'/'.$thisPage.'/RT-%quiz-id%'; ?>')" target="_blank"><img style="vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/twitter-small.png'; ?>" /></a>
					<a id="email-share" href="javascript:shareSocialNetwork('EM_Q',1,'<?php echo $emailHref; ?>');"><img style="vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/email-small.png'; ?>" /></a>
				</div>
			</ul>
			<div class="view-selector-bg">
				<div class="slide left"></div>
			</div>
			<div id="cities">
				<div class="results-scroll" id="citywrap">
					<ul id="cities" class="scrollbar"></ul>
					<span id="loading" class="mobile" style="display:none;">Loading more cities...</span>
					<div class="mobile-space"></div>
				</div>
				<span id="loading" class="desktop" style="display:none;">Loading more cities...</span>
			</div>
			<div id="listings" style="display:none;">
				<div class="citynamebanner">
					<!--<table class="tag-icons"></table>-->
					<span class="cityname notranslate"></span>
					<span class="score">Lifestyle Match: <span></span></span>
				</div>
				<div class="results-scroll" id="listingswrap">
					<ul id="listings" class="scrollbar"></ul>
					<span id="loading" class="mobile" style="display:none;">Loading more listings...</span>
					<div class="mobile-space"></div>
				</div>
				<span id="loading" class="desktop" style="display:none;">Loading more listings...</span>
			</div>
			<div id="city-map" style="display:none;"></div>
			<div id="listing-map" style="display:none;"></div>
		</section>
		<section class="right-col">
			<ul class="profile-tiles">
				<li class="save-profile"><a href="javascript:;">Save Search</a></li>
				<li class="my-profiles"><a href="javascript:;">My Searches</a></li>
				<li class="new-profile"><a href="javascript:;">New Search</a></li>
			</ul>
			<div id="shareDiv" class="tiny">
				<a id="facebook-share" href="javascript:shareSocialNetwork('FB_Q',1,'https://www.facebook.com/sharer/sharer.php?u=<?php echo get_home_url().'/'.$thisPage.'/RF-%quiz-id%'; ?>')" target="_blank"><img style="width:34px; height:34px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/facebook.png'; ?>" /></a>
				<a id="linkedin-share" href="javascript:shareLinkedIn({type:'LI_Q', comment:'Hi friends check out this search result!', title:'Great listings found at LifeStyled Listings', description:'Find great homes using your lifestyle and favorite things in life!', url:'<?php echo get_home_url().'/'.$thisPage.'/RI-%quiz-id%'; ?>', img:'<?php echo get_template_directory_uri().'/_img/backgrounds/header-fb.jpg'; ?>', where: 1});" target="_blank"><img style="width:34px; height:34px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/linkedin.png'; ?>" /></a> 
				<a id="googleplus-share" href="javascript:shareSocialNetwork('GP_Q',1,'https://plus.google.com/share?url=<?php echo get_home_url().'/'.$thisPage.'/RG-%quiz-id%'; ?>')" target="_blank"><img style="width:34px; height:34px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/googleplus.png'; ?>" /></a>
				<a id="twitter-share" href="javascript:shareSocialNetwork('TW_Q',1,'https://twitter.com/share?url=<?php echo get_home_url().'/'.$thisPage.'/RT-%quiz-id%'; ?>')" target="_blank"><img style="width:34px; height:34px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/twitter.png'; ?>" /></a>
				<a id="email-share" href="javascript:shareSocialNetwork('EM_Q',1,'<?php echo $emailHref; ?>');"><img style="width:34px; height:34px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/email.png'; ?>" /></a>
			</div>
      <div class="quiz-tags">
				<div class="selectors">
					<div class="location-range" > <!-- relative;top: 20px;"> -->
						<h3>Selected city: </h3>
						<div class='selectDistance'>
							<span class='selected'></span>
							<span class='selectArrow'></span>
							<ul id="distance" class="distance"></ul>
						</div>
					</div>
					<button id="set-search-area">Change Search Area</button>
					<div class="quiz-select" style="display:none;">
						<span class="title">Where do you want to look?</span>
 						<div class="chooseQuiz"> <!-- even == city, odd == nationwide or statewide -->
							<span class="fullsearch"><a href="javascript:;" class="choose selected" quiz="<?php echo QUIZ_NATION; ?>"><span class="circle active"></span>Anywhere in the U.S.</a></span>
							<span class="specificsearch"><a href="javascript:;" class="choose" quiz="<?php echo QUIZ_LOCALIZED; ?>"><span class="circle"></span><span id="title">Near a City or State</span></a></span>
						</div>
						<div class="specific">
							<div class="autocomplete desktop" style="margin: -3px 0 11px 5px;">
								<input type="text" class="inactive" placeholder="Enter City or State"></input>
							</div>
							<div class='searchexact selectDistance mod' style="left: 8px; top: -3px;">
								<span class='selected' style="width: 90px;padding:.4em 2em .4em 1em;"></span>
								<span class='selectArrow' style="margin-top: 7px;"></span>
								<ul id="distance" class="distance" style="top: 29px;"></ul>
							</div>
							<?php if ($quizOptions && $quizOptions->showListingFilter) : ?>
							<div id="median-range" style="display: none">
								<!-- <span id="msg">Listings filter</span> -->
								<div id="radio-item"  style="display: none"><input type="checkbox" name="no-limit" value="0" checked><span>No median price limit</span></div>
								<div id="radio-item"><input type="checkbox" name="commercial-ok" value="1"><span>Include commercial</span></div>
								<div id="radio-item"><input type="checkbox" name="rental-ok" value="2"><span>Include rental</span></div>
								<div id="radio-item"><input type="checkbox" name="rental-only" value="3"><span>Only rentals</span></div>
							</div>
							<?php endif; ?>
						</div>
						<div id="control">
							<button id="runQuiz">Update my area</button>
							<button id="cancelChangeSearchArea">Cancel</button>
						</div>
					</div>
					<div class="price-slider">
						<div class="minmaxprice">
							<div class='selectBoxmin priceList'>
								<span class='selected'></span>
								<span class='selectArrow'></span>
								<ul id="min" class="prices"></ul>
							</div>
							<div class='selectBoxmax priceList'>
								<span class='selected'></span>
								<span class='selectArrow'></span>
								<ul id="max" class="prices"></ul>
							</div>
						</div>
						<div class="bedbath">
							<div class='selectBed homeOptions'>
								<span class='selected'>BEDS</span>
								<span class='selectArrow'></span>
								<ul class="options" id="beds" class="home"></ul>
							</div>
							<div class='selectBath homeOptions'>
								<span class='selected'>BATHS</span>
								<span class='selectArrow'></span>
								<ul class="options" id="baths" class="home"></ul>
							</div>	
						</div>
					</div>
				</div>
				<div class="lifestyle-profile"><span><strong>Your</strong> Lifestyle Profile</span></div>
				<div class="tags-bg">
					<div class="tags-header">
						<span class="title-tags">Edit and refine your tags below.</span>
						<div class="tags-modal-wrap-1">
							<div class="tags-modal-wrap-2">
								<div class="tags-modal">
									<div class="header">
										<span class="fh-switch" style="position: absolute; pointer-events: none; margin-left: 39px;">
											<input type="checkbox" id="switch-id" name="myswitch" checked="">
											<label for="switch-id" style="cursor:default;"></label>
											<span class="fh-switch-knob"></span>
										</span>
										<span style="margin-left: 48px;"> Switch On</span></div>
									<div class="sub-header">to <span style="font-weight:700;">only</span> see results with</div>
									<div class="sub-header">your "must have" criteria</div>
									<button class="close">Got it!</button>
								</div>
							</div>
						</div>
					</div>
                    <div id="actions">
                        <div class="add-tags"><span class="entypo-plus-circled" style="margin-right: 2px;"></span> Add More Things</div>
                        <div class="enforce">
                            <button id="apply" disabled >Apply Changes</button>
                            <button id="undo" disabled >Undo</button>
                        </div>
                    </div>
					<div class="tags-wrap tag-editor">
						<div class="main-header">
							<!--<div class="add-tags"><span class="entypo-plus-circled"></span> Add More Tags</div>-->
							<div class="top">
								<span class="fh-switch" style="position: absolute;margin-left: 5px;pointer-events: none;">
										<input type="checkbox" id="switch-id" name="myswitch" checked />
										<label for="switch-id" style="cursor:default;"></label>
										<span class="fh-switch-knob"></span>
								</span>
								<span class="sub">Toggle Your &quot;Must Haves&quot;</span>
							</div>
						</div>
						<ul class="tags-list scrollbar scrollbar-left"></ul>
					</div>
				</div>
			</div>
		</section>
	</article>
</main></div>
