<?php
global $ALR; 
require_once(__DIR__.'/_classes/Sellers.class.php'); 
global $s;
$s = new AH\Sellers(1);
require_once(__DIR__.'/_classes/Options.class.php');
require_once(__DIR__.'/_classes/Utility.class.php');
$Options = new AH\Options();
$x = $Options->get((object)['where'=>['opt'=>'SystemMaintenance']]);
$SystemMaintenance = !empty($x) ? intval($x[0]->value) : 0;

$x = $Options->get((object)['where'=>['opt'=>'BypassAnalytics']]);
$BypassAnalytics = !empty($x) ? intval($x[0]->value) : 0;

// live site
// FB - app secret: 51806a7df74f5ca97d5451165620a8f3
// FB - app ID: 629501310545215

// alpha
// FB - app secret: 2c68acaae28a17db788386b0895b2c52
// FB - app ID: 1779986508910477

// mobile
// FB - app secret: 7508127a6277a09958c1089b87214bf0
// FB - app ID: 315304152157084

// local
// FB - app secret: 86f14db654936041cc815a07c551de0b
// FB - app ID: 1181119758597729

// engr
// FB - app secret: 4a5ef6abca9608ccbd4dc0bea8fc411c
// FB - app ID: 616307271864753 

global $wpId;
global $browser;
$browser = AH\getBrowser();
global $isMobile;

$fbId = strpos(get_home_url(), 'local') !== false ? '1181119758597729' :
		(strpos(get_home_url(), 'alpha') !== false ? '1779986508910477' :
		(strpos(get_home_url(), 'mobile') !== false ? '315304152157084' :
		(strpos(get_home_url(), 'engr') !== false  || strpos(get_home_url(), '63.249.66.66') !== false ? '616307271864753' :
																									     '629501310545215')));
// $isMobile = $browser['shortname'] == 'IEMobile' ||
// 			$browser['shortname'] == 'iPhone' ||
// 			$browser['shortname'] == 'iPad' ||
// 			$browser['shortname'] == 'Android';

function forceLogin($userId) {
	global $s;
	$seller = $s->get((object)['where'=>['author_id'=>$userId]]);
	if (empty($seller)) {
		$s->log("forceLogin failed to find seller with id:$userId");
		return;
	}

	wp_logout(); // just in case...
	wp_set_current_user(0);
	wp_set_auth_cookie(0);

	$seller = array_pop($seller);
	$user = get_userdata($seller->author_id);
	$magic = get_user_meta($user->ID, 'user_key', true);
	$i = 0;
	$testPasswd = '';
	if (!empty($magic)) {
		$s->log("forceLogin - Found user_key for sellerId:$seller->id, userID:$user->ID - ".print_r($magic, true));
		foreach($magic as $value)
			$testPasswd .= chr( $value ^ ($i++ + ord('.')));
	}
	else {
		if (!empty($seller->meta)) {
			// $seller->meta = json_decode($seller->meta);
			foreach($seller->meta as $meta) {
				if ($meta->action == SELLER_KEY) {
					$s->log("Found SELLER_KEY for $seller->id, meta:$meta->key");
					$key = json_decode($meta->key);
					$len = count($key); // strlen($meta->key);
					for($i = 0; $i < $len; $i++)
						$testPasswd .= chr( $key[$i] ^ ($i + ord('.')) );
				}
			}
		}
	}
	
	$msg = '<h2>Failed to retrieve seller password.</h2>';
	$msg .= '<h1>Seller with author id: '.$seller->author_id.', could not be logged in.<br/>Please contact administrator</h1>';
	if (empty($testPasswd)) {
		$s->log("Failed seller: ".print_r($seller, true));
		$xml = '<!DOCTYPE html>';
		$xml .= '<html><title>Message</title><body>'.$msg.'</body>';
		$xml .= '</html>';
		echo $xml;
		die;
	}								
	
	$s->log($testPasswd." ".$user->user_login);
	$creds = array();
	$creds['user_login'] = $user->user_login;
	$creds['user_password'] = $testPasswd;
	$creds['remember'] = true;
	$user = wp_signon( $creds, false );
	if ( is_wp_error($user) ) {
		$s->log("Failed seller: ".print_r($seller, true));
		$msg = '<h2>Failed to login seller.</h2>';
		$msg .= '<h1>Seller with author id: '.$seller->author_id.', failed to login with error: '.$user->get_error_message().'.<br/>Please contact administrator</h1>';
		$xml = '<!DOCTYPE html>';
		$xml .= '<html><title>Message</title><body>'.$msg.'</body>';
		$xml .= '</html>';
		echo $xml;
		die;
	}
	
	$user = wp_set_current_user($user->ID, $user->user_login);
	$s->log("UserId: $user->ID after wp_set_current_user()");
	// $user = wp_get_current_user();
	wp_set_auth_cookie( $user->ID );
	do_action('wp_login', $user->user_login, $user);

	$wpId = wp_get_current_user()->ID;

	$s->log("forceLogin - Seller:$seller->id, with userID:$seller->author_id is logged in with $testPasswd as {$creds['user_login']}, actual user is $wpId, is logged in:".is_user_logged_in());
}

// $ch_nickname = isset($_GET['ch_nickname']) ? $_GET['ch_nickname'] : (isset($_POST['ch_nickname']) ? $_POST['ch_nickname'] : '');
$input_var = get_query_var('id');
$s->log("header - input:$input_var");
if (!empty($input_var)) {
	if (strpos($input_var, 'Z-') === 0) {
		$pos = strpos($input_var, "_");
		$userId = intval(substr($input_var, 2, $pos));
		$hash = '#'.substr($input_var, $pos+1);
		forceLogin($userId);
		// $user = get_userdata($userId);
		// $user = wp_set_current_user($user->ID, $user->user_login);
		// $s->log("seller - input:$input_var, using $userId, hash: $hash, got UserId: $user->ID after wp_set_current_user()");
		// // $user = wp_get_current_user();
		// wp_set_auth_cookie( $user->ID );
		// do_action('wp_login', $user->user_login, $user);
		global $current_user;
		get_currentuserinfo();
		$s->log("seller - input:$input_var, using $userId, hash: $hash, got login:$current_user->user_login, id:$current_user->ID after forceLogin()");
	}
	elseif (strpos($input_var, 'L-') === 0 ||   // L = login
			strpos($input_var, 'U-') === 0) {   // U = user
		$userId = intval(substr($input_var, 2));
		$wpId = wp_get_current_user()->ID;
		if ($wpId != $userId) {
			$s->log("UserId:$userId is not the same as logged in user:$wpId");
			forceLogin($userId);
			// $user = get_userdata($userId);
			// $user = wp_set_current_user($user->ID, $user->user_login);
			global $current_user;
			get_currentuserinfo();
			$s->log("seller - input:$input_var, using $userId, login:$current_user->user_login, id:$current_user->ID, after forceLogin()");
		}
		// $user = get_userdata($userId);
	}
	elseif (strpos($input_var, 'T-') === 0) { // T for transaction
		$s->log("header is logging out user");
		wp_logout(); // just in case...
		wp_set_current_user(0);
		wp_set_auth_cookie(0);
	}
}
$seller = null; 
global $agent;
global $showAgentInfo;
// $agent = isset($_COOKIE['CassiopeiaDirective']) ? $ALR->get('portal-agent') : 0;
$agent = $ALR->get('portal-agent');
$cassie = isset($_COOKIE['CassiopeiaDirective']) ? $_COOKIE['CassiopeiaDirective'] : '';
$showAgentInfo = !empty($agent) && !empty($agent->nickname) && !empty($agent->photo) && !empty($agent->first_name)&& !empty($agent->last_name);

if (is_user_logged_in()) $seller = $ALR->get('seller');	 
// 0 - show all non-registered
// 1 = show all, registered user
// 2 = registered agent already but no options
// 3 = registered has AgentMatch, no Portal
// 4 = registered has Portal, not AgentMatch
// 5 = registered has both or Lifetime
$showBenefits = empty($seller) && !is_user_logged_in() ? 0 : 
				( is_user_logged_in() && empty($seller) ? 1 :
				( empty($seller->author_id) || $seller->flags == 0 ? 2 : 
				( ($seller->flags & SELLER_IS_PREMIUM_LEVEL_1) == 0 ? ( ($seller->flags & (SELLER_IS_PREMIUM_LEVEL_2| SELLER_IS_LIFETIME)) == 0 ? 2 : 3) : 
				( ($seller->flags & (SELLER_IS_PREMIUM_LEVEL_2 | SELLER_IS_LIFETIME)) == 0 ? 4 : 5) )  )); 	

$user = null;
if (is_user_logged_in() )
	$user = wp_get_current_user();

global $thisPage;	
$thisPage = get_queried_object()->post_name;
$imgUrl = '';
$imgDesc = 'The most advanced search engine for lifestyled properties and second homes. Search by your lifestyle and find the best places to live with homes and agents that match you.';
$imgTitle = '';
$imgListingPageUrl = '';
$pageParts = explode('-',$thisPage);
$title = 'LifeStyled Listings: Search Homes By Lifestyle';
//foreach($pageParts as $part)
//	$title .= (strlen($title) ? ' ' : '').ucfirst($part);
//$title .= strlen( trim($title) ) > 1 ? ' Page' : 'Home Page';

$extra = null;
$seo = '';
switch($thisPage) {
	case 'listing':
	case 'listing-new':
		$listing_id = get_query_var('id');
		$extra = get_query_var('extra');
		if ( !empty($extra) &&
			 $extra == 'seo') {
			$seo = isset($_COOKIE['RedirectURL']) && !empty($_COOKIE['RedirectURL']) ? $_COOKIE['RedirectURL'] : 
				   isset($_SESSION['RedirectURL']) && !empty($_SESSION['RedirectURL']) ?  : '';
			if (!empty($seo)) {
				require_once(__DIR__.'/_classes/SiteMapper.class.php'); $SiteMapper = new AH\SiteMapper(1);
				$imgTitle = $SiteMapper->getDescription($seo, $thisPage);
			}
		}
		if (!empty($listing_id)) {
			require_once(__DIR__.'/_classes/Listings.class.php'); $Listings = new AH\Listings();
			$isAdmin = 0;
			$track = 0;
			$trackId = 0;
			if (strpos($listing_id, "-") != false) {
				$parts = explode("-", $listing_id);
				$listing_id = $parts[1];
				
				if (strpos($parts[0], "T") === false)
					$isAdmin = intval($parts[0]);
				else {
					$trackId = substr($parts[0], 1);
					require_once(__DIR__.'/_classes/EmailTracker.class.php'); $et = new AH\EmailTracker();
					$track = $et->get((object)['where'=>['id'=>$trackId]]);
					if (!empty($track)) {
						$track = array_pop($track);
					}
				}
			}
			$x = $Listings->get((object)[ 'where'=>[ 'id'=>$listing_id ] ]);
			if (!empty($x)) {
				$listing = array_pop($x);
				if (!empty($listing->images)) foreach($listing->images as $img) {
					if (isset($img->file)) {
						if (strpos($img->file, "http") === false )
							$imgUrl = get_template_directory_uri().'/_img/_listings/900x500/'.$img->file;
						else
							$imgUrl = $img->file;
						break;
					}
				}
				$listing->baths = floatval($listing->baths);
				$baths = floor($listing->baths) + (fmod($listing->baths, 1.0) != 0.0 ? ".5" : '');
				$imgTitle .= "$listing->city, $listing->state - $".preg_replace('/\B(?=(\d{3})+(?!\d))/', ",", $listing->price).", Beds:$listing->beds, Baths:$baths";
				if (!empty($listing->street_address))
					$imgTitle .= " located on $listing->street_address.";
				$imgDesc = $listing->about;
				$imgListingPageUrl = get_home_url()."/listing/$listing_id";
				$title = "$listing->street_address, $listing->city, $listing->state | Lifestyled Listings";
			}
		}
		break; // case listing

	case 'quiz-results':
	case 'quiz-results-new':
		$extra = get_query_var('extra');
		$imgUrl =  get_template_directory_uri().'/_img/backgrounds/quiz-results-og.jpg';
		if ( isset($extra) && !empty($extra) &&
			 $extra == 'seo') {
			$seo = isset($_COOKIE['RedirectURL']) && !empty($_COOKIE['RedirectURL']) ? $_COOKIE['RedirectURL'] : 
				   isset($_SESSION['RedirectURL']) && !empty($_SESSION['RedirectURL']) ?  : '';
			if (!empty($seo)) {
				require_once(__DIR__.'/_classes/SiteMapper.class.php'); $SiteMapper = new AH\SiteMapper(1);
				require_once(__DIR__.'/_classes/States.php');
				$imgTitle = $SiteMapper->getDescription($seo, $thisPage);
				$param = $SiteMapper->getParmeters($seo, $thisPage);
				$newTitle = isset($param->cityName) && !empty($param->cityName) ? $param->cityName.", ".$param->state : 
						    (isset($param->state) ? AH\getLongName($param->state) : "Nation wide") ;
				$newTitle.= " for ".(isset($param->tagDescString) ? $param->tagDescString : $param->optName);
				$newTitle.= isset($param->fields) && isset($param->fields['price']) ? ", priced from {$param->fields['price'][0]} to {$param->fields['price'][1]}" : '';
				$title = "LifeStyled Listings: Search Homes By Lifestyle | $newTitle";
			}
		}
		else
			$imgTitle = 'Check out the locations and homes I got matched to!';
		$imgDesc = 'Lifestyled Listings matches you to the best locations and homes by your unique lifestyle. Check out what your friends got matched to and start a custom search for yourself.';
		$imgListingPageUrl = get_home_url()."/$thisPage";
	break;
	case 'sellers':
		$title = "Sellers Dashboard | Lifestyled Listings";
	break;
	case 'about':
		$imgTitle = "What we are about LifeStyled Listings, the lifestyled home search engine";
		$imgDesc = "Lifestyle is about choice, lifestyle is unique, lifestyle is about taking what you expect to a new level. Lifestyled is exactly that. We give you the ability to search for homes based on what it is you desire, not the limited list of places you already know. We show you all the locations and homes that meet your unique needs, wherever they exist.";
		$imgListingPageUrl = get_home_url()."/$thisPage";
		$title = "Discover Your Dream Home, Second Homes, Retirement Communities & Amazing Locations | Lifestyled Listings";
	break;
	case 'faq':
		$imgTitle = "Find out more about LifeStyled Listings, the lifestyled home search engine";
		$imgDesc = "Answers to questions you may have about our site.";
		$imgListingPageUrl = get_home_url()."/$thisPage";
		$title = "Frequently Asked Questions | Lifestyled Listings";
	break;
	case 'agent-benefits':
		$imgTitle = "Benefits afforded to agents from LifeStyled Listings, the lifestyled home search engine";
		$imgDesc = "Find out what agents can get from using our site.";
		$imgListingPageUrl = get_home_url()."/$thisPage";
		$title = "Real Estate Agent Benefits | Lifestyled Listings";
	break;
	case 'agent-purchase-lifestyle':
	case 'agent-get-lifestyle':
		$imgTitle = "Reserve your position as a lifestyled agent of LifeStyled Listings, the lifestyled home search engine";
		$imgDesc = "Specialized and exclusive agents gets the best clients, be one of the select few to qualify!";
		$imgListingPageUrl = get_home_url()."/$thisPage";
		$title = "Lifestyled Agent: Find New Buyers Who Connect With Your Lifestyle | Lifestyled Listings";
	break;
	case 'agent-purchase-portal':
	case 'agent-get-portal':
		$imgTitle = "Reserve your portal to capture your clients using LifeStyled Listings, the lifestyled home search engine";
		$imgDesc = "Create your own portal to our site, locking in your clients and be visible nationwide!";
		$imgListingPageUrl = get_home_url()."/$thisPage";
		$title = "Agent Portal: Connect Buyers & Get Referrals | Lifestyled Listings";
	break;
	case 'contact':
		$imgTitle = "Reach out to LifeStyled Listings, the lifestyled home search engine";
		$imgDesc = "Find how to get a hold of us.";
		$imgListingPageUrl = get_home_url()."/$thisPage";
	break;
	case 'register':
		$imgTitle = "Join us at LifeStyled Listings, the lifestyled home search engine";
		$imgDesc = "Register to save your profiles as user, or upload HD images and listings if you're an agent.";
		$imgListingPageUrl = get_home_url()."/$thisPage";
	break;
	case 'terms':
		$imgTitle = "The legal stuff for LifeStyled Listings, the lifestyled home search engine";
		$imgDesc = "Find out your rights and ours here.";
		$imgListingPageUrl = get_home_url()."/$thisPage";
		$title = "Terms & Conditions | Lifestyled Listings";
	break;
}

$wpId = $user ? $user->ID : 0;
$portalOwner = $wpId ? get_userdata($wpId, 'nickname') : null; // see if current logged in user owns a portal

global $nickname;
global $havePortal;
$nickname = !empty($portalOwner) && isset($portalOwner->nickname) && !empty($portalOwner->nickname) ? $portalOwner->nickname : 'unknown';
$nicename = !empty($portalOwner) && isset($portalOwner->user_nicename) && !empty($portalOwner->user_nicename) ? $portalOwner->user_nicename : 'unknown';
// this is for sellers page usage
$havePortal = ($nickname != 'unknown' && stripos($nicename, 'unknown') === false) ? PORTAL_VALID : PORTAL_NOT_EXIST;
$referer = isset($_SERVER["HTTP_REFERER"]) ? (!empty($_SERVER["HTTP_REFERER"]) ? strtolower($_SERVER["HTTP_REFERER"]) : "") : '';

$defaultFindAHomeString = "Start Search";

$opt = $Options->get((object)['where'=>['opt'=>'HomePageOptions']]);
$homePageOptions = !empty($opt) ? json_decode($opt[0]->value) : (object)['showAgentBenefits'=>1];

$opt = $Options->get((object)['where'=>['opt'=>'SiteMasterOption']]);
$siteMasterOption = null;
$siteMasterOptionList =  !empty($opt) ? json_decode($opt[0]->value) : [];
if (!empty($siteMasterOptionList)) {
	if (!empty($user)) foreach($siteMasterOptionList as $item) {
		if ($item->agentID == $user->ID) {
			$siteMasterOption = $item;
			break;
		}
		unset($item);
	}

	if (!$siteMasterOption &&
		!empty($agent)) foreach($siteMasterOptionList as $item) {
		if ($item->agentID == $agent->author_id) {
			$siteMasterOption = $item;
			break;
		}
		unset($item);
	}

	// if we didn't find a match, see if there is a global one
	if (empty($siteMasterOption)) foreach($siteMasterOptionList as $item) {
		if ($item->agentID == -1) {
			$siteMasterOption = $item;
			break;
		}
		unset($item);
	}
}
if (!$siteMasterOption)
	$siteMasterOption = (object)['agentID'=>-1,
								 'showGoogleTranslate'=>0];

global $showAgentBenefits;
$showAgentBenefits = $homePageOptions->showAgentBenefits;

$mobilePages = [
	'home',
	'quiz',
	'agent-benefits',
	'agent-purchase-portal',
	'agent-purchase-lifestyle',
	'agent-basic-lifestyle-info',
	'contact',
	'quiz-results',
	'quiz-results-new',
	'register',
	'about',
	'listing',
	'listing-new',
	'sellers',
	'agent',
	'terms',
	'faq',
	'portal-landing',
	'portal-landing-one',
	'portal-landing-two'
];
$disableZoomPages = [
	'home',
	'quiz',
	'quiz-results',
	'quiz-results-new',
	'listing',
	'listing-new',
	'sellers',
	'agent',
	'faq',
	'portal-landing',
	'portal-landing-one',
	'portal-landing-two'
];
//$sidebar = get_sidebar('widgets');
?>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="<?php echo in_array($thisPage, $mobilePages) ? 'initial-scale=1' : 'width=1440' ?><?php echo in_array($thisPage, $disableZoomPages) ? ', user-scalable=no, width=device-width' : '' ?>">
	<meta name="keywords" content="Lifestyled Listings, Lifestyle, unique properties, estates, lifestyled agents, perfect real estate, real estate, best places to live, homes, 2nd homes, second homes, lifestyle properties">
	<meta name="robots" content="index, nofollow">
	<?php if (!empty($imgListingPageUrl)) : ?>
	<meta property="og:url" content="<?php echo $imgListingPageUrl; ?>" />
 	<?php endif; ?>
	<?php if (!empty($imgUrl)) : ?>
	<meta property="og:image" content="<?php echo $imgUrl; ?>" />
 	<?php endif; ?>
 	<?php if (!empty($imgDesc)) : ?>
 	<meta property="og:description" content="<?php echo str_replace('"', "'", $imgDesc); ?>" />
 	<?php endif; ?>
	<meta property="og:image:type" content="image/jpeg">
	<meta property="og:image:width" content="700">
	<meta property="og:image:height" content="368">
	<meta property="og:title" content="<?php echo empty($imgTitle) ? "Lifestyled Listings. Search for amazing homes by your Lifestyle." : $imgTitle; ?>" >
	<link rel="shortcut icon" href="http://lifestyledlistings.com/wp-content/themes/allure/_img/lifestyled_favicon.png">
	<link rel="image_src" href="http://lifestyledlistings.com/wp-content/themes/allure/_img/backgrounds/header-1.jpg" />
	<title><?php echo $title; ?></title>

	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@lifestyledlistings">
	<meta name="twitter:creator" content="@LifeStyledListings">
	<meta name="twitter:title" content="<?php echo empty($imgTitle) ? "Lifestyled Listings. Search for amazing homes by your Lifestyle." : $imgTitle; ?>">
	<meta name="twitter:description" content="<?php echo str_replace('"', "'", $imgDesc); ?>">
	<meta name="twitter:image" content="<?php echo (!empty($imgUrl) ? $imgUrl : 'http://lifestyledlistings.com/wp-content/themes/allure/_img/backgrounds/header-1.jpg'); ?> ">
	<?php 
	if ($wpId == 8 ) 
		echo '<meta name="google-site-verification" content="f6xUla4et81un0DKa0DY-l_iztS0ZPF__lSxWJq5y6g" />';
	?>
	<?php
		if ( $browser['name'] == 'Twitterbot' ||
			(!empty($referer) &&
			  strpos($referer, "twitterbot") !== false) )
			die;
		wp_head();
		require_once(__DIR__.'/_classes/States.php');
		global $usStates;
		// require_once(__DIR__.'/_classes/Sellers.class.php');
	?>
	<script type="text/javascript">
		var states = '';
		<?php $i = 0; foreach($usStates as $state) { ?>
	  		states += '<option value="<?php echo $state; $i++ ?>"><?php echo $state; ?></option>';
	  	<?php } ?>
	  	var isSeller = <?php echo !empty($seller) ? 1 : 0; ?>;
	  	var showBenefits = <?php echo $showBenefits; ?>;
	  	var havePortal = <?php echo $havePortal ? $havePortal : 0 ?>;
	  	var nickname = '<?php echo ($havePortal == PORTAL_VALID || $havePortal == PORTAL_EXPIRING) ? $nickname : "<Your name>"; ?>';
	</script>
	<!-- devel key: 75jkho46gu2f73, secret: M5P9wue0w8z4Dmoz
	 	 live key:  7557gvsweqivxa, secret: srRtk3V1GoxFp5H4 -->
	<script type="text/javascript" src="//platform.linkedin.com/in.js">
	    api_key: <?php echo (strpos(get_home_url(), 'localhost') !== false ? '75jkho46gu2f73' : '7557gvsweqivxa').PHP_EOL; ?>
	    lang:en_US
	 	authorize: true
    	onLoad: onLinkedInLoad
	</script>
</head>
<body <?php body_class($thisPage); ?>>
<script>
var thisPage = '<?php echo $thisPage; ?>';
var SystemMaintenance = <?php echo $SystemMaintenance; ?>;
var BypassAnalytics = <?php echo $BypassAnalytics; ?>;
var Referer = '<?php echo isset($_SERVER["HTTP_REFERER"]) ? (!empty($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : "empty") : "not set"; ?>';
var Browser = <?php echo json_encode($browser); ?>;
var agent = <?php echo !empty($agent) ? json_encode($agent) : 0; ?>;
var showAgentInfo = <?php echo !empty($showAgentInfo) ? 1 : 0 ?>;
var isMobile = <?php echo $isMobile ? 'true' : 'false'; ?>;
var sellerId = <?php echo !empty($seller) ? $seller->id : 0 ?>;
var wpId = <?php echo !empty($wpId) ? $wpId : 0 ?>;
var fbId = '<?php echo !empty($fbId) ? $fbId : 0 ?>';
var cassie = '<?php echo $cassie; ?>';
var siteMasterOption = <?php echo json_encode($siteMasterOption); ?>;
/*var sidebar = '<?php echo $sidebar; ?>';*/
</script>
	
<script type="text/javascript">
	jQuery(document).ready(function($){

		isRetina = $('section.isretina-identifier').css('display') != 'none';
		isMobile = $('section.ismobile-identifier').css('display') != 'none';
		var checkLangChangeCount = 0;

		function reparentWidgets(oldParent, newParent) {
			var children = oldParent.children();
			children.each(function() {
				var ele = $(this).detach();
				newParent.append(ele);
			})
		}

		function hideSideBar() {
			var sideBarOwner = '#mobile-header-nav ';
			var isLi = $(sideBarOwner+'div#sidebar-widgets.sidebar').parent().is('li');
			isLi ? $(sideBarOwner+'div#sidebar-widgets.sidebar').parent().hide() : $(sideBarOwner+'div#sidebar-widgets.sidebar').hide();

			sideBarOwner = '#header-nav ';
			isLi = $(sideBarOwner+'div#sidebar-widgets.sidebar').parent().is('li');
			isLi ? $(sideBarOwner+'div#sidebar-widgets.sidebar').parent().hide() : $(sideBarOwner+'div#sidebar-widgets.sidebar').hide();
		}

		function checkLangChange(force) {
			checkLangChangeCount++;
			if (typeof force == 'undefined' &&
				checkLangChangeCount > 12) {// stop after 1 min
				hideSideBar();
				$('nav#header-nav .nav-container ul>li').css('width', '32%');
				setCookie('ShowTranslateButton', 'no', 7);
				console.log("checkLangChangeCount("+checkLangChangeCount+") is over limit, hiding sidebar");
				return;
			}

			if (typeof force != 'undefined') // reset counter in case it's taken longer than 5 secs to translate page after clicking on anchor
				checkLangChangeCount = 0;

			var ele = $('.home-options-progress .mobilequestion');
			var question = !ele.length ? 'Question' : ele.html();
			var ele2 = $('#header-options .about');
			var about = !ele2.length ? 'About Us' : ele2.html();
			var translated = (ele.length && question.indexOf('Question') == -1) ||
							 (ele2.length && about.indexOf('About') == -1);
			console.log("checkLangChange - about:"+about+", question:"+question+", translated:"+translated);
	 		if (translated) {
	 			hideSideBar();
	 			$('nav#header-nav .nav-container ul>li').css('width', '32%');
	 			console.log("already translated, hiding sidebar");
	 		}
	 		else window.setTimeout(function() {
			 	checkLangChange();
			}, 5000);
		}

		function resetGoogleTranslateHtml() {
			var ele = $('div#google_translate_element a.goog-te-menu-value span').first();
			ele.html('Translate');

			ele = $('div#google_translate_element .goog-te-gadget-icon');
			ele.css('background-image', 'url('+ah_local.tp+'/_img/google_search-min.png)');
			ele.css('background-position', '0 0');
			ele.css('background-size', 'cover');
			$('div#google_translate_element a').show();

			console.log("enterd resetGoogleTranslateHtml, ele.length:"+ele.length);

			console.log("calling checkLangChange");
			checkLangChange();

			$('div#google_translate_element a').on('click', function() {
			 	window.setTimeout(function() {
			 		console.log("calling checkLangChange");
				 	checkLangChange(true);
				}, 5000);
			})

			if (isMobile) {
				var liveNav = $('#header-nav #sidebar-widgets');
				var mobileNav = $('#mobile-header-nav #sidebar-widgets');
				reparentWidgets(liveNav, mobileNav);
			}
		}

		function waitForGoogleTranslateHtml() {
			var showButton = getCookie("ShowTranslateButton");
			if (!siteMasterOption.showGoogleTranslate ||
				(showButton.length && showButton == 'no')) {
				hideSideBar();
				console.log("siteMasterOption.showGoogleTranslate("+siteMasterOption.showGoogleTranslate+"), showButton("+showButton+"), hiding sidebar");
				return;
			}

			$('nav#header-nav .nav-container ul>li').css('width', '24%');
			$('nav#header-nav div.header-login.portal-landing').css('width', '15%');

			var ele = $('div#google_translate_element a.goog-te-menu-value');
			var done = false;
			if (ele.length) {
				ele = $('div#google_translate_element a.goog-te-menu-value span').first();
				if (ele.length) {
					done = true;
				}
			}

			if (!done)
				window.setTimeout(function() {
					waitForGoogleTranslateHtml();
				}, 100);
			else
				window.setTimeout(function() {
					resetGoogleTranslateHtml();
				}, 650);
		}

		waitForGoogleTranslateHtml();

		$(window).on('resize', function(e) {
			var previous = isMobile;
			isMobile = $('section.ismobile-identifier').css('display') != 'none';
			if (previous != isMobile) {
				var liveNav = $('#header-nav #sidebar-widgets');
				var mobileNav = $('#mobile-header-nav #sidebar-widgets');
				if (false == previous)
					reparentWidgets(liveNav, mobileNav);
				else
					reparentWidgets(mobileNav, liveNav);
			}
		});

		$('.menulinesclick').on('click', function() {
			transitionEnd = 'transitionend webkitTransitionEnd otransitionend MSTransitionEnd';
			if ($('.dropmenu').hasClass('animatedown')){
				$('.main-overlay').fadeOut(750);
				$('#main-page-wrapper').removeClass('scrollable');
				$('#mobile-header-nav .dropmenu ul li').removeClass( 'animatein' );
				timeoutID = window.setTimeout(delayanimateup, 500);
				function delayanimateup() {
					$('#mobile-header-nav .dropmenu').addClass( 'animateup' );
					$('#mobile-header-nav .dropmenu').removeClass( 'animatedown' );
				}
				timeoutID = window.setTimeout(delayanimatehide1, 1250);
				function delayanimatehide1() {
					$('#mobile-header-nav .dropmenu').removeClass( 'animateup' );
				}
				$('.menulines .linediv').removeClass( 'active' );
				$('.menulines .linediv .line1').removeClass( 'active' );
				$('.menulines .linediv .line2').removeClass( 'active' );
				$('.menulines .linediv .line3').removeClass( 'active' );
			}
			else {
				$('.main-overlay').fadeIn(750);
				$('#main-page-wrapper').addClass('scrollable');
				timeoutID = window.setTimeout(delayanimatedown, 400);
				function delayanimatedown() {
					$('#mobile-header-nav .dropmenu ul li').addClass( 'animatein' );
				}
				$('#mobile-header-nav .dropmenu').addClass( 'animatedown' );
				$('#mobile-header-nav .dropmenu').removeClass( 'animateup' );
				$('.menulines .linediv').addClass( 'active' );
				$('.menulines .linediv .line1').addClass( 'active' );
				$('.menulines .linediv .line2').addClass( 'active' );
				$('.menulines .linediv .line3').addClass( 'active' );
			}
		});
	});
</script>

<!--[if lt IE 7 ]> <section class="ie6 ie-identifier" style="display: none;"></section> <![endif]-->
<!--[if IE 7 ]>    <section class="ie7 ie-identifier" style="display: none;"></section> <![endif]-->
<!--[if IE 8 ]>    <section class="ie8 ie-identifier" style="display: none;"></section> <![endif]-->
<!--[if IE 9 ]>    <section class="ie9 ie-identifier" style="display: none;"></section> <![endif]-->
<!--[if (gt IE 9)|!(IE)]> <section class="ie-identifier" style="display: none;"></section> <![endif]-->

<!--Media Query Section--> <section class="isretina-identifier" style="display: none;"></section> <!--<![endif]-->
<!--Media Query Section--> <section class="ismobile-identifier" style="display: none;"></section> <!--<![endif]-->
<!--Media Query Section--> <section class="sellers-mobile-identifier" style="display: none;"></section> <!--<![endif]-->
<!--Media Query Section--> <section class="mobile-identifier" style="display: none;"></section> <!--<![endif]-->
<!--Media Query Section--> <section class="mobile-nav-identifier" style="display: none;"></section> <!--<![endif]-->
<?php if ( strpos($thisPage,'portal-landing') !== false ) : ?>
<div id="main-page-wrapper" data-role="page">
		<nav data-role="header" id="header-nav">
			<div class="nav-container">
				<div class="agent-info" <?php  echo (!$showAgentInfo ? 'style="display: none;"' : ''); ?> > 
					<a class="agent-profile" href="javascript:;" ><img src="" class="agent-picture" /></a> 
					<span>Courtesy of: </span>
					<a class="name notranslate" href="javascript:;" ></a>
					<a href="javascript:goToAgency();" class="agency-logo"></a>
				</div>
				<div class="logoDiv">
						<a href="<?php bloginfo('wpurl'); ?>" class="logo"><span class="life notranslate">Life</span><span class="notranslate">styled</span><span class="lifestyledsmall notranslate">Listings</span></a>
				</div>
				<div class="agent-logoDiv" style="display: none;">
						<a href="<?php bloginfo('wpurl'); ?>" class="logo"><span class="life notranslate">Life</span><span class="notranslate">styled</span><span class="lifestyledsmall notranslate">Listings</span></a>
				</div>
				<div class="header-login portal-landing">
					<?php get_sidebar( 'widgets' ); ?>	
					<div class="info-solo" style="right: 15px;">
						<!--<fb:login-button scope="public_profile,email" onlogin="checkLoginState();" data-size="large" style="display: none;">Connect with Facebook</fb:login-button>
						&nbsp;<span id="fb-or" style="display: none;">or</span>&nbsp;-->
						<a href="javascript:portal_landing.login();" title="Log In" class="login">Log in<span class="entypo-right-open-big" style="margin-left: 10px; position: relative; font-size: .65em; top: -1.5px; font-weight: 900;"></span></a>
					</div>
				</div>
			</div>
		</nav>

		<nav data-role="header" id="mobile-header-nav">
	      	<div class="nav-container">
						<div class="topsection">
							<div class="logoDiv">
								<a href="javascript:portal_landing.login();" title="Log In" class="login" id="login-portal">Log in<span class="entypo-right-open-big" style="margin-left: 8px; position: relative; font-size: .65em; top: -1.5px; font-weight: 900;"></span></a>
								<span class="logo">
									<span class="life notranslate">Life</span><span class="notranslate">styled</span><span class="lifestyledsmall notranslate">Listings</span>
								</span>
							</div>
							<?php get_sidebar( 'widgets_mobile' ); ?>
						</div>
			</div>
		</nav>

<?php else: ?>

<div class="loading-overlay">
	<p>Loading Your Quiz...</p>
	<div class="spin-wrap">
		<div class="spinner">
			<div class="cube1"></div>
			<div class="cube2"></div>
		</div>
	</div>
</div>

<div id="main-page-wrapper" data-role="page">
		<nav data-role="header" id="header-nav">
			<div class="nav-container">
				<div class="agent-info" <?php  echo (!$showAgentInfo ? 'style="display: none;"' : ''); ?> > 
					<a class="agent-profile" href="javascript:;" ><img src="" class="agent-picture" /></a> 
					<span>Courtesy of: </span>
					<a class="name notranslate" href="javascript:;" ></a>
					<a href="javascript:goToAgency();" class="agency-logo"></a>
				</div>
				<div class="logoDiv">
						<a href="<?php bloginfo('wpurl'); ?>" class="logo"><span class="life notranslate">Life</span><span class="notranslate">styled</span><span class="lifestyledsmall notranslate">Listings</span></a>
				</div>
				<div class="agent-logoDiv" style="display: none;">
						<a href="<?php bloginfo('wpurl'); ?>" class="logo"><span class="life notranslate">Life</span><span class="notranslate">styled</span><span class="lifestyledsmall notranslate">Listings</span></a>
				</div>
			  	<div class='feedbackDiv'> <a class="feedback" href="javascript:;">Feedback</a></div>
	        	<div class="header-login">
	        		<?php if (is_user_logged_in()) : ?>
						<?php if ($seller) : ?>
									<div class="info">
										<span>Welcome, <a class="update-profile name notranslate" href="javascript:;"><?php echo $seller->first_name; ?></a></span>
										<?php if ($user->user_login == 'gtvracer') : ?>
											<a href="<?php bloginfo('wpurl'); ?>/wp-admin">Admin</a> &nbsp;
										<?php endif; ?>
										<a href="javascript:logout('<?php echo wp_logout_url(get_bloginfo('wpurl')); ?>');" title="Log Out" style="color:#28adcd;font-weight:100;">Log Out</a>
									</div>
									<a class="update-profile" href="javascript:;"><img src="<?php echo get_bloginfo('template_directory').'/_img/_authors/55x55/'.($seller->photo ? $seller->photo : '_blank.jpg') ?>" class="profile-picture" /></a>
						<?php else : ?>
									<div class="info-buyer">
										<span>Welcome&nbsp;</span><span class="notranslate"><?php echo (wp_get_current_user()->ID != 0 ?  " ".wp_get_current_user()->first_name : ''); ?></span><span>!</span>
										<?php if ($user->user_login == 'gtvracer') : ?>
											<a href="<?php bloginfo('wpurl'); ?>/wp-admin">Admin</a> &nbsp;
										<?php endif; ?>
										<span id="buyer-extra" style="margin-top:0;">
											<a href="javascript:logout('<?php echo wp_logout_url(get_bloginfo('wpurl')); ?>');" title="Log Out">Log Out</a>&nbsp;|&nbsp;
											<a href="javascript:void(0);" onclick="register();" title="Register" style="text-transform:capitalize;margin-left:-5px;">Agent Signup</a>
										</span>
									</div>
						<?php endif; ?>
					<?php else : ?>
						<div class="info-solo">
							<?php if ($thisPage == 'quiz-results' ||
									  $thisPage == 'listing' ||
									  $thisPage == 'explore-the-area-new') : ?>
							<a href="javascript:controller.login();" title="Log In">Log in / Register <span class="entypo-right-dir"></span></a>
							<?php else: ?>
							<a href="javascript:signInRegister();" title="Log In">Log in / Register <span class="entypo-right-dir"></span></a>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			    <ul id="header-options">
			    	<?php if (!is_page('new-listing')) : ?>
						<li><a class="find-a-home" href="javascript:;"><?php echo $defaultFindAHomeString; ?></a></li>
						<?php if ($seller) : ?>
						<li><a class="sell-a-home" href="<?php bloginfo('wpurl'); ?>/sellers">Agent Dashboard</a></li>
						<?php else : ?>
<!-- 						<li><a class="sell-a-home" href="javascript:void(0);" onclick="register();" >Agent Dashboard</a></li> -->
							<?php if ($showAgentBenefits) : ?>
							<li><a class="sell-a-home" href="javascript:showAgentBenefits('<?php echo $showBenefits; ?>');" >Agent Benefits</a></li>
							<?php endif; ?>
						<?php endif; ?>
						<li class="mobilecontact"><a class="about" href="<?php bloginfo('wpurl'); ?>/about">About Us</a></li>
						<li><?php get_sidebar( 'widgets' ); ?></li>
					<?php else : ?>
						<li><a class="mysearch" href="<?php bloginfo('wpurl'); ?>/quiz-results">My search</a></li>
						<li class="mobilecontact"><a class="about" href="<?php bloginfo('wpurl'); ?>/about">About us</a></li>
						<?php if ($showAgentBenefits) : ?>
						<li class="mobileabout"><a class="benefits" href="javascript:showAgentBenefits('<?php echo $showBenefits; ?>');">Agent benefits</a></li>
						<?php endif; ?>
						<li><?php get_sidebar( 'widgets' ); ?></li>
					<?php endif; ?>


					<?php if (!is_user_logged_in()): ?>
					<li class="mobilesignin"><a href="javascript:signInRegister();">Sign In</a></li>
					<?php else: ?>
					<li class="mobilelogout"><a href="javascript:logout('<?php echo wp_logout_url(get_bloginfo('wpurl')); ?>');" title="Log Out">Log Out</a></li>
					<?php endif; ?>
			    </ul>
	      	</div>
  		</nav>
	
		<nav data-role="header" id="mobile-header-nav">
	      	<div class="nav-container">
						<div class="topsection">
							<div class="logoDiv">
								<span class="logo">
									<span class="life notranslate">Life</span><span class="notranslate">styled</span><span class="lifestyledsmall notranslate">Listings</span>
								</span>
							</div>
							<?php get_sidebar( 'widgets_mobile' ); ?>

							<div class="menulinesclick"></div>
							<div class="menulines">
								<div class="linediv">
									<div class="innerdiv">
										<span class="line1"></span>
										<span class="line2"></span>
										<span class="line3"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="dropmenu">
	        			<div class="header-login">
								<div class='feedbackDiv'> <a class="feedback" href="javascript:;">Feedback</a></div>
							<div class="agent-info" <?php  echo (!$showAgentInfo ? 'style="display: none;"' : ''); ?> > 
								<a class="agent-profile" href="javascript:;" ><img src="" class="agent-picture" /></a> 
								<span class="tinymobile" style="display:none;">Sponsored:</span>
								<span class="main">Courtesy of: </span>
								<a class="name notranslate" href="javascript:;" ></a></span>
							</div>
			 		<?php if (is_user_logged_in()): ?>
						<?php if ($seller) : ?>
									<div class="info">
										<a class="update-profile" href="javascript:;"><img src="<?php echo get_bloginfo('template_directory').'/_img/_authors/55x55/'.($seller->photo ? $seller->photo : '_blank.jpg') ?>" class="profile-picture" /></a>
										<span>Welcome, <a class="update-profile name notranslate" href="javascript:;"><?php echo $seller->first_name; ?></a></span>
										<?php if ($user->user_login == 'gtvracer') : ?>
											<a href="<?php bloginfo('wpurl'); ?>/wp-admin">Admin</a> |&nbsp;
										<?php endif; ?>
										<!-- <a href="<?php echo wp_logout_url(get_bloginfo('wpurl')); ?>" title="Log Out">Log Out</a> -->
										<a href="javascript:logout('<?php echo wp_logout_url(get_bloginfo('wpurl')); ?>');" title="Log Out">Log Out</a>
									</div>
						<?php else : ?>
									<div class="info-buyer">
										<span>Welcome&nbsp;</span><span class="notranslate"><?php echo (wp_get_current_user()->ID != 0 ?  " ".wp_get_current_user()->first_name : ''); ?>!<a class="update-profile name" href="javascript:;"></a></span>
										<?php if ($user->user_login == 'gtvracer') : ?>
											<a href="<?php bloginfo('wpurl'); ?>/wp-admin">Admin</a> |&nbsp;
										<?php endif; ?>
										<span id="buyer-extra">
											<a href="javascript:logout('<?php echo wp_logout_url(get_bloginfo('wpurl')); ?>');" title="Log Out">Log Out</a>&nbsp;|&nbsp;
											<a href="javascript:void(0);" onclick="register();" title="Register" style="text-transform:capitalize;margin-left:-5px;">Agent Signup</a>
										</span>
									</div>
						<?php endif; ?>
					<?php else : ?>
						<div class="info-solo">
							<a href="javascript:signInRegister();" title="Log In">
								<span class="entypo-user"></span>
								Log in / Register <span class="entypo-right-dir"></span>
							</a>
						</div>
					<?php endif; ?>
				</div>
			    <ul id="header-options">
					<li><a href="<?php bloginfo('wpurl'); ?>">Home</a></li>
					<?php if (is_user_logged_in() && $seller) : ?>
						<li><a class="sell-a-home" href="<?php bloginfo('wpurl'); ?>/sellers">Agent Dashboard</a></li>
					<?php endif; ?>
					<li><a class="find-a-home" href="javascript:;"><?php echo $defaultFindAHomeString; ?></a></li>
					<?php if (!is_user_logged_in() && $showAgentBenefits): ?>
						<li><a class="benefits" href="javascript:showAgentBenefits('<?php echo $showBenefits; ?>');">Agent Benefits</a></li>
					<?php endif; ?>
					<li><a class="about" href="<?php bloginfo('wpurl'); ?>/about">About Us</a></li>
					<li><a class="about" href="<?php bloginfo('wpurl'); ?>/contact">Contact</a></li>
				</ul>
	      	</div>
  		</nav>


	<!-- <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/_js/jquery.gray.min.js"></script> -->
    <div class="popup-block-modal">
      <p>Please enable popups for this site on your browser.</p>
      <button class="close">Got it!</button>
    </div>
	<div class="main-overlay"></div>
	<div class="imageGalleryDiv"></div>
	<div class="mobile-back-button">
		<a href="javascript:window.history.back();" class="exit-page"><span class="entypo-left-open-big"></span></a>
	</div>
<?php endif; ?>

	<div role="main" class="ui-content main-container">
		<div class="main wrapper clearfix">
