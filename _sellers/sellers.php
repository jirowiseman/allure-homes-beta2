<?php
require_once(__DIR__.'/../_classes/Listings.class.php');
require_once(__DIR__.'/../_classes/Sellers.class.php');
require_once(__DIR__.'/../_classes/SessionMgr.class.php');
require_once(__DIR__.'/../_classes/QuizActivity.class.php');
require_once(__DIR__.'/../_classes/PortalUsers.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php');
require_once(__DIR__.'/../_classes/Tags.class.php'); $Tags = new AH\Tags();
require_once(__DIR__.'/../_classes/IpList.class.php'); $IpList = new AH\IpList(1);
require_once(__DIR__.'/../_classes/ListingsTags.class.php'); global $ListingsTags; $ListingsTags = new AH\ListingsTags();
require_once(__DIR__.'/../_classes/Cities.class.php'); global $Cities; $Cities = new AH\Cities(1);
require_once(__DIR__.'/../_classes/Options.class.php'); global $Options; $Options = new AH\Options();
require_once(__DIR__.'/../_classes/Associations.class.php'); global $Associations; $Associations = new AH\Associations();
require_once(__DIR__.'/../_classes/States.php');

function getCitiesTags(&$listing) {
	global $Cities;
	$sql = 'SELECT c.id, c.tag, c.type, b.score FROM `'.$Cities->getTableName().'` AS a ';
	$sql .= 'INNER JOIN '.$Cities->getTableName('cities-tags').' AS b ON a.id = b.city_id AND (b.score >= 7 OR b.score = -1) ';
	$sql .= 'INNER JOIN '.$Cities->getTableName('tags').' AS c ON c.id = b.tag_id ';
	if (!empty($listing->city_id) &&
		intval($listing->city_id) != -1)
		$sql .= 'WHERE a.id = '.$listing->city_id;
	else
		$sql .= 'WHERE a.city = "'.$listing->city.'" AND a.state = "'.$listing->state.'"';
	$Cities->log("getCitiesTags - sql: $sql");

	$cityTags = $Cities->rawQuery($sql);

	$listing->cityTags = array();
	if (!empty($cityTags)) foreach($cityTags as $tag) {
		if ($tag->score == -1)
			$tag->score = 10;
		$listing->cityTags[$tag->id] = $tag;
		unset($tag);
	}
}

global $woocommerce;
$itemsInCart = 0;
if (isset($woocommerce) &&
	!empty($woocommerce)) {
	$wc_cart = &$woocommerce->cart;
	$itemsInCart = count($wc_cart->get_cart());
}

global $usStates;
global $agent; // from header.php, this is the portal agent

$q = new stdClass(); 
$s = new AH\Sellers(1);
$input_var = get_query_var('id');

$dumpVisitationMetaData = false;
$opt = $Options->get((object)['where'=>['opt'=>'DumpVisitationMetaData']]);
if (!empty($opt)) 
	$dumpVisitationMetaData = intval($opt[0]->value);

// PremiumPortalMode
$premiumPortalMode = 0;
$opt = $Options->get((object)['where'=>['opt'=>'PremiumPortalMode']]);
if (!empty($opt)) 
	$premiumPortalMode = intval($opt[0]->value);

$discount = 0;
$coupon = '';
$discountLA = 0;
$couponLA = '';
$maxFreeLA = 10;
$inviteCode = [];

$portalPriceGrade = GRADE_2;
$portalDefaultPrice = 59;
//PortalProductPricing
$opt = $Options->get((object)['where'=>['opt'=>'PortalProductPricing']]);
if (!empty($opt)) {
	$opt = json_decode($opt[0]->value);
	$portalDefaultPrice = $opt[$portalPriceGrade]->monthly;
}

$opt = $Options->get((object)['where'=>['opt'=>'DefaultPortalCoupon']]);
if (!empty($opt)) {
	// $s->log("seller page - opt: ".print_r($opt, true));
	$opt = json_decode($opt[0]->value);
	$discount = $opt->discount;
	$coupon = $opt->coupon;
}
else
	$s->log("seller page - no DefaultPortalCoupon found.");
// portal
$isFree = $discount == $portalDefaultPrice;

$opt = $Options->get((object)['where'=>['opt'=>'DefaultLifestyleCoupon']]);
if (!empty($opt)) {
	// $s->log("seller page - opt: ".print_r($opt, true));
	$opt = json_decode($opt[0]->value);
	$discountLA = $opt->discount;
	$couponLA = $opt->coupon;
}
else
	$s->log("seller page - no DefaultLifestyleCoupon found.");
// portal
$isFreeLA = $discountLA == 99;

$opt = $Options->get((object)['where'=>['opt'=>'MaxFreeLifestyle']]);
if (!empty($opt)) {
	// $s->log("seller page - opt: ".print_r($opt, true));
	$maxFreeLA = intval($opt[0]->value);
}
else
	$s->log("seller page - no MaxFreeLifestyle found.");

$opt = $Options->get((object)['where'=>['opt'=>'ShowSiteEntryStatsInActivityPage']]);
$showSiteEntryStatsInActivityPage = !empty($opt) ? intval($opt[0]->value) : 0;

$associations = $Associations->get((object)['what'=>'*']);

$hash = '';
$clearBrowserUrl = false;
if (!empty($input_var)) {
	if ($input_var[0] == '#')
		$hash = $input_var;
	else if (strpos($input_var, 'Z-') === 0) {
		$pos = strpos($input_var, "_");
		$userId = intval(substr($input_var, 2, $pos));
		$hash = '#'.substr($input_var, $pos+1);
		// $user = get_userdata($userId);
		// $user = wp_set_current_user($user->ID, $user->user_login);
		// $s->log("seller - input:$input_var, using $userId, hash: $hash, got UserId: $user->ID after wp_set_current_user()");
		// // $user = wp_get_current_user();
		// wp_set_auth_cookie( $user->ID );
		// do_action('wp_login', $user->user_login, $user);
	}
	else if (strpos($input_var, 'L-') === 0)
		$clearBrowserUrl = true;
}

$wpId = wp_get_current_user()->ID;
$user = get_userdata($wpId, 'nickname');

$nickname = !empty($user) && isset($user->nickname) && !empty($user->nickname) ? $user->nickname : 'unknown';
$nicename = !empty($user) && isset($user->user_nicename) && !empty($user->user_nicename) ? $user->user_nicename : 'unknown';
$havePortal = ($nickname != 'unknown' && stripos($nicename, 'unknown') === false) ? PORTAL_VALID : PORTAL_NOT_EXIST;
$reservedPortalName = '';
$portalMsg = '';
$s->log("Seller page entered for userID:$wpId, ID:".(!empty($user) ? $user->ID : 0).", nickname:$nickname, nicename:$nicename, havePortal:$havePortal");

$q->where = array('author_id'=>$wpId);
$x = $s->get($q);
$fromListhub = 0;
$hasBoughtLifestyle = 0;
$visitorNames = ['Anonymous']; // first one, with index 0 is unknown always
$visitorEmails = ['Unknown']; // first one, with index 0 is unknown always
$visitorPhones = ['Unknown']; // first one, with index 0 is unknown always
$Listings = new AH\Listings();
$PortalUsers = new AH\PortalUsers();
$QuizActivity = new AH\QuizActivity(1);
$portalUsersList = [];
$portalUsersOffset = 0;

$visitationCities = [];
$haveLifestyle = false;

$opt = $Options->get((object)['where'=>['opt'=>'PortalLandingAgentOptions']]);
$agentOptionsList = empty($opt) ? [] : json_decode($opt[0]->value);
$agentOptions = null;
if (!empty($agentOptionsList)) {
	foreach($agentOptionsList as $item) 
		if ($item->agentID == $wpId) {
			$agentOptions = $item;
			break;
		}

	if (empty($agentOptions)) foreach($agentOptionsList as $item) 
		if ($item->agentID == -1) { // global
			$agentOptions = $item;
			break;
		}
}

if (!$agentOptions) {
	$agentOptions = new \stdClass();
	$agentOptions->agentID = $wpId;
	$agentOptions->nameMode = PORTAL_LANDING_MUST_HAVE;
	$agentOptions->phoneMode = PORTAL_LANDING_NOT_USED;
	$agentOptions->emailMode = PORTAL_LANDING_OPTIONAL;
}

$opt = $Options->get((object)['where'=>['opt'=>'QuizResultsPortalUserCaptureOptions']]);
$quizResultsPortalUserCaptureOptions = empty($opt) ? [] : json_decode($opt[0]->value);
$captureOptions = null;
if (!empty($quizResultsPortalUserCaptureOptions) ) {
	foreach($quizResultsPortalUserCaptureOptions as $item) {
		if ($item->agentID == $wpId) {
			$captureOptions = $item;
			break;
		}
		unset($item);
	}
	// if we didn't find a match, see if there is a global one
	if (empty($captureOptions)) foreach($quizResultsPortalUserCaptureOptions as $item) {
		if ($item->agentID == -1) {
			$captureOptions = $item;
			break;
		}
		unset($item);
	}
}

if (empty($captureOptions)) {
	$captureOptions = (object)[	'agentID'=>$wpId,
								'quizLoad'=>(object)['captureMode'=>CAPTURE_NONE],
								'cityScroll'=>(object)[ 'page'=>1,
														'captureMode'=>CAPTURE_OPTIONAL],
								'listingScroll'=>(object)['page'=>1,
														  'captureMode'=>CAPTURE_NONE],
								'viewListing'=>(object)['captureMode'=>CAPTURE_MUST] ];
}

$opt = $Options->get((object)['where'=>['opt'=>'SellerLeadCaptureOption']]);
$sellerLeadCaptureOption = empty($opt) ? [] : json_decode($opt[0]->value);
$doLeadCaptureSetup = !empty($sellerLeadCaptureOption) && isset($sellerLeadCaptureOption->showLeadCapture) ? $sellerLeadCaptureOption->showLeadCapture : 0;

$sellerLeadCaptureDivs = '';
$sellerLeadCaptureDivTitleList = [];
global $portalUserCaptureMode;
$portalUserCaptureMode = ['Not Used'=>0,
						  'Optional'=>1,
						  'Hard Stop'=>2];



function getCaptureModeOptions($val) {
	global $portalUserCaptureMode;
	$portalUserCaptureModeOptions = '';
	foreach($portalUserCaptureMode as $id=>$value) {
		$portalUserCaptureModeOptions .= '<option value="'.$value.'" '.($value == $val ? "selected" : '').'>'.$id.'</option>';
		unset($value);
	}
	return $portalUserCaptureModeOptions;
}


if (!empty($sellerLeadCaptureOption)) {
	$s->log("sellerLeadCaptureOption:".print_r($sellerLeadCaptureOption, true));
	foreach($sellerLeadCaptureOption->questions as $title=>$content) {
		$sellerLeadCaptureDivTitleList[] = $title;
		$h = '<div class="capture-slide" id="'.$title.'">';
		$h.=	'<div class="text">';
		$h.=		'<p>'.$content->text.'</p>';
		$h.=	'</div>';
		$h.=	'<span class="line"></span>';
		$h.=	'<div class="controls">';
		foreach($content->buttons as $id=>$context) {
			$h.=	'<button id="'.$context->id.'" what="'.$id.'" onclick="'.$context->onclick.'">'.$context->html.'</button>';	
			unset($context);
		}
		$h.=	'</div>';
		$h.= '</div>';
		$sellerLeadCaptureDivs .= $h;
		unset($content);
	}

	$sellerLeadCaptureDivs .= '<div class="capture-slide" id="capture_options">';
	foreach($sellerLeadCaptureOption->capture_options as $id=>$content) {
		if ($id == 'text') {
			$sellerLeadCaptureDivs .= 	'<div class="text">';
			$sellerLeadCaptureDivs .=		'<span>'.$content.'</span>';
			$sellerLeadCaptureDivs .=	'</div>';	
		}
		elseif ( $id != 'buttons' &&
				(gettype($content) == 'object' ||
				 gettype($content) == 'array') ) {
			$sellerLeadCaptureDivs .= 	'<div class="trigger-option" id="'.$id.'"><span id="title">'.$id.':</span>';
			foreach($content as $what=>$value) {
				switch($what) {
					case 'mode':
						$sellerLeadCaptureDivs .= '<span id="subtitle">'.$what.'</span><select id="'.$id.'">'.getCaptureModeOptions($value).'</select>';
						break;
					case 'delay':
					case 'pageNumber':
						$sellerLeadCaptureDivs .= '<span id="subtitle">'.$what.'</span><input type="text" id="'.$id.'" what="'.$what.'" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="'.$value.'"></input>';
						break;
				}
			}
			$sellerLeadCaptureDivs .=	'</div>';
		}
		else {
			$sellerLeadCaptureDivs.=	'<span class="line"></span>';
			$sellerLeadCaptureDivs.=	'<div class="controls">';
			foreach($content as $id=>$context) {
				$sellerLeadCaptureDivs.=	'<button id="'.$context->id.'" what="'.$id.'" onclick="'.$context->onclick.'">'.$context->html.'</button>';	
				unset($context);
			}
			$sellerLeadCaptureDivs.=	'</div>';
		}
		unset($content);
	}
	$sellerLeadCaptureDivs .= '</div>';
}

$user = null;
if (is_user_logged_in() )
	$user = wp_get_current_user();

$opt = $Options->get((object)['where'=>['opt'=>'AgentPageOption']]);
$agentPageOptionList = empty($opt) ? [] : json_decode($opt[0]->value);
$agentPageOption = null;
if (!empty($agentPageOptionList) ) {
	if (!empty($user)) foreach($agentPageOptionList as $item) {
		if ($item->agentID == $user->ID) {
			$agentPageOption = $item;
			break;
		}
		unset($item);
	}
	// if we didn't find a match, see if there is a global one
	if (empty($agentPageOptionList)) foreach($agentPageOptionList as $item) {
		if ($item->agentID == -1) {
			$agentPageOption = $item;
			break;
		}
		unset($item);
	}
}

if (empty($agentPageOption)) {
	$agentPageOption = new \stdClass();
	$agentPageOption->showFindCompanyButton = 0;
	$agentPageOption->allowAdvancedAgentCompanyEdits = 0;
	$agentPageOption->showExtendedCompanyFields = 0;
	$agentPageOption->showCompanyMorePulldownTab = 0;
	$agentPageOption->displayStatsPagePhoneNumbers = 1;
}

$seller_association = !empty($x) ? $x[0]->association : 0;
$doingAdvancedCompanyEdits = $agentPageOption->allowAdvancedAgentCompanyEdits;

$companySelectionList = '';
$detailColumnClass = $agentPageOption->showCompanyMorePulldownTab ? 'third' : 'second';
if (!empty($associations)) {
	$s->log("associations:".print_r($associations, true));
	$h = '<div id="title"><span>Select Company</div>';
	$h.= '<button class="closeDiv"><span class="line1"></span><span class="line2"></span></button>';
	$h.= '<div id="column-header">';
		$h.= '<div class="first"><span>Logo</span></div>';
		if ($agentPageOption->showCompanyMorePulldownTab)
			$h.= '<div class="second"><span>Details</span></div>';
		if ($doingAdvancedCompanyEdits)
			$h.= '<div class="'.$detailColumnClass.'"><span>Option</span></div>';
	$h.= '</div>';
	$h.= '<div id="companies"><ul>';
	foreach($associations as $company) {
		$h.= '<li for="'.$company->id.'">';
			$h.= '<div class="container" id="company" for="'.$company->id.'" >';
				$h .= '<div class="first">';
					$h.= '<a href="javascript:controller.selectCompany('.$company->id.');">';
						$h.= '<img src="'.get_template_directory_uri()."/_img/_agency/$company->logo".'"></img>';
					$h.= '</a>';
				$h.= '</div>';
				// $h .= '<div class="second" id="identification">';
				// 	$h.= '<a href="javascript:controller.selectCompany('.$company->id.');">';
				// 		$h.= '<span id="name">'.$company->company.'</span>';
				// 		$h.= '<span id="address">'.$company->street_address.'</span>';
				// 		$h.= '<span id="locality">'.$company->city.', '.$company->state.' '.$company->zip.'</span>';
				// 	$h.= '</a>';
				// $h .= '</div>';
				if ($agentPageOption->showCompanyMorePulldownTab) {
					$h .= '<div class="second" id="more"><a href="javascript:controller.showCompanyDetail('.$company->id.');">More<span class="entypo-down-open-big"/></a></div>';
					$h .= '<div class="second hidden" id="closeDetail"><a href="javascript:controller.closeCompanyDetail('.$company->id.');" >Close<span class="entypo-up-open-big"/></a></div>';
				}
				if ($doingAdvancedCompanyEdits) {
					if ($company->id == $seller_association)
					$h .= '<div class="'.$detailColumnClass.'" id="editDetail"><a href="javascript:controller.editCompanyDetail('.$company->id.');" >Edit</a></div>';
					else
					$h .= '<div class="'.$detailColumnClass.'" id="editDetail"></div>';
				}
				$h .= '<div class="details" for="'.$company->id.'" style="display: none;">';
					$h.= '<span class="detail" id="name">Name: '.$company->company.'</span>';
					if (!empty($company->street_address))
						$h.= '<span class="detail" id="address">Address: '.$company->street_address.'</span>';
					if (!empty($company->city))
						$h.= '<span class="detail" id="locality">Locality: '.$company->city.', '.$company->state.' '.$company->zip.'</span>';
					if (!empty($company->contact))
						$h.= '<span class="detail">Contact: '.(!empty($company->contact) ? $company->contact : 'N/A').'</span>';
					if (!empty($company->contact_email))
						$h.= '<span class="detail">Email: '.(!empty($company->contact_email) ? $company->contact_email : 'N/A').'</span>';
					if (!empty($company->phone))
						$h.= '<span class="detail">Phone1: '.(!empty($company->phone) ? AH\fixPhone($company->phone) : 'N/A').'</span>';
					if (!empty($company->phone2))
						$h.= '<span class="detail">Phone2: '.(!empty($company->phone2) ? AH\fixPhone($company->phone2) : 'N/A').'</span>';
					if (!empty($company->url))
						$h.= '<span class="detail">WebSite: '.(!empty($company->url) ? $company->url : 'N/A').'</span>';
				$h .= '</div>';
			$h.= '</div>';
		$h.= '</li>';
	}
	$h.= '</ul></div>';
	$h.= '<div id="controls">';
		$h.= '<button id="select" disabled >Accept</button>';
		$h.= '<button id="create">Create</button>';
		$h.= '<span id="error" class="hidden"></span>';
	$h.= '</div>';
	$companySelectionList = $h;
}


if (!empty($x)) {
	$haveLifestyle = $x[0]->flags & SELLER_IS_PREMIUM_LEVEL_2;
	$x[0]->phone = !empty($x[0]->phone) ? AH\fixPhone($x[0]->phone) : (!empty($x[0]->mobile) ? AH\fixPhone($x[0]->mobile) : '');

	if ($dumpVisitationMetaData)
		$s->log("got seller:".print_r($x[0], true));
	else
		$s->log("got seller:".$x[0]->id);

	// get all agent's listings now
	$listings = $Listings->get((object)['where'=>['author'=>$wpId,
											 // 'listhub_key'=>'notnull',
										'author_has_account'=>1]]);
	
	// for each listing, get the general users' data to get the portalUsersOffset correct
	// deal with the portal Users and clicks later
	// setup visitorNames, visitorEmails, visitationCities
	if (!empty($listings)) foreach($listings as $listing) {
		if (!empty($listing->listhub_key)) {
			$fromListhub++;
		}
		// $clicks = 0;
		if (!empty($listing->meta)) foreach($listing->meta as $meta) {
			if ($meta->action == LISTING_VISITATIONS) {
				$meta->users = (array)$meta->users;
				if (!empty($meta->users)) foreach($meta->users as $id=>$ips) {
					if (!empty($id)) {
						$id = intval($id);
						// $portalUsersOffset = $id > $portalUsersOffset ? $id : $portalUsersOffset; // get the offset updated
						if (!isset($visitorNames[$id])) {// then it's not zero
							$visitor = $s->get((object)['where'=>['author_id'=>$id]]);
							$email = $first_name = $last_name = '';
							$phone = 0;
							if (!empty($visitor)) {
								$first_name = $visitor[0]->first_name;
								$last_name = $visitor[0]->last_name;
								$email = $visitor[0]->email;
								$phone = !empty($visitor[0]->mobile) ? AH\fixPhone($visitor[0]->mobile) : AH\fixPhone($visitor[0]->phone);
								$break = false;
								if (!empty($visitor[0]->meta)) foreach($visitor[0]->meta as $submeta) {
									if ($submeta->action == SELLER_PROFILE_DATA &&
										!empty($submeta->contact_email) ) {
										$email = $submeta->contact_email;
										$break = true;
									}
									unset($submeta);
									if ($break) break;
								}
							}
							else {
								if ( ($user_info = get_userdata($id)) !== false) {
									$first_name = $user_info->first_name;
									$last_name = $user_info->last_name;
									$email = $user_info->user_email;
								}
							}

							$visitorNames[$id] = !empty($first_name) ? $first_name.(!empty($last_name) ? ' '.$last_name : '') : 'Anonymous';
							$visitorEmails[$id] = $email;
							$visitorPhones[$id] = $phone;
							unset($visitor);
						}
					}
					foreach($ips as $addr=>$theDates) {
						// foreach($theDates as $date=>$data) {
						// 	$clicks++;
						// 	unset($data);
						// }
						// unset($theDates);
						if (!in_array($addr, $visitationCities))
							$visitationCities[] = $addr;
					}
					unset($ips);
				} // foreach($meta->users as $id=>$ips) 
			} //if ($meta->action == LISTING_VISITATIONS)
		} // foreach($listing->meta as $meta) 
	}// foreach($listings as $listing)

	$x[0]->fromListhub = $fromListhub > 0;
	if (!empty($x[0]->meta)) {
		reset($x[0]->meta);
		foreach($x[0]->meta as $i=>&$meta) {
			// $s->log("meta $i:".print_r($meta, true));
			// $s->log("meta $i:$meta->action, SELLER_VISITATIONS:".SELLER_VISITATIONS);
			// $meta->action = intval($meta->action);
			// $s->log("meta $i:$meta->action, type:".gettype($meta->action)." SELLER_VISITATIONS:".SELLER_VISITATIONS.", type:".gettype(SELLER_VISITATIONS));
			// $s->log("got meta - action: $meta->action");
			switch ($meta->action) {
				case SELLER_AGENT_ORDER:
					$laInviteUsedCount = 0;
					foreach($meta->item as $item) {
						if ($item->type == ORDER_AGENT_MATCH) {
							if ( $item->mode == ORDER_BOUGHT &&
							     $item->order_id == DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM &&
							     $item->cart_item_key == $couponLA)
								$laInviteUsedCount++;
						}
					}
					if ($laInviteUsedCount >= $maxFreeLA) {
						$s->log("Lifestyle coupon:$couponLA is used up with $laInviteUsedCount uses for sellerId:".$x[0]->id);
						$couponLA = '';
						$discountLA = 0;
						$isFreeLA = false;
					}
					break;
				case SELLER_NICKNAME:
					$SessionMgr = new AH\SessionMgr;
					$out = $SessionMgr->checkPortalName($meta->nickname);
					if ($out->status == 'OK') {
						$reservedPortalName = $meta->nickname;
						if (isset($meta->flags) &&
							!empty($meta->flags))
							$havePortal = $meta->flags & IC_BOUGHT_PORTAL ? PORTAL_VALID : ($meta->flags & IC_REMOVED ? PORTAL_REMOVED : ($meta->flags & IC_EXPIRED ? PORTAL_EXPIRED : PORTAL_NOT_EXIST));
					}
					else {
						$portalMsg = $out->data;
						if (isset($meta->flags) && $meta->flags & (IC_WITHIN_1 | IC_WITHIN_7 | IC_WITHIN_14 | IC_WITHIN_30))
							$havePortal = PORTAL_EXPIRING;
					}
					break;
				case SELLER_VISITATIONS:
					if ($dumpVisitationMetaData)
						$s->log("Got SELLER_VISITATIONS:".print_r($meta, true));
					$meta->userActivity = [];
					$meta->portalUserActivity = [];
					if (isset($meta->users)) {
						$meta->users = (array)$meta->users;
						if (!empty($meta->users)) {
							foreach($meta->users as $id=>&$data) {
								$id = intval($id);
								$portalUsersOffset = $id > $portalUsersOffset ? $id : $portalUsersOffset; // get the offset updated
								$newUserActivity = new \stdClass();
								$newUserActivity->id = $id;
								$newUserActivity->ips= json_decode( json_encode($data) ); // makes a perfect copy of [ip=>[dates,...],ip2=>[dates,...],...]

								if (!empty($id) &&  // then it's not zero
									!isset($visitorNames[$id])) { // and name isn't recorded yet
									$visitor = $s->get((object)['where'=>['author_id'=>$id]]);
									$email = $first_name = $last_name = '';
									$phone = 0;
									if (!empty($visitor)) {
										$first_name = $visitor[0]->first_name;
										$last_name = $visitor[0]->last_name;
										$email = $visitor[0]->email;
										$phone = !empty($visitor[0]->mobile) ? AH\fixPhone($visitor[0]->mobile) : AH\fixPhone($visitor[0]->phone);
										$gotOne = false;
										if (!empty($visitor[0]->meta)) foreach($visitor[0]->meta as $submeta) {
											if ($submeta->action == SELLER_PROFILE_DATA &&
												!empty($submeta->contact_email) ) {
												$email = $submeta->contact_email;
												$gotOne = true;
											}
											unset($submeta);
											if ($gotOne) break;
										}
									}
									else {
										if ( ($user_info = get_userdata($id)) !== false) {
											$first_name = $user_info->first_name;
											$last_name = $user_info->last_name;
											$email = $user_info->user_email;
										}
									}

									$s->log("Fetching regular User:$id, got ".$first_name);
									$visitorNames[$id] = !empty($first_name) ? $first_name.' '.(!empty($last_name) ? $last_name : '') : 'Anonymous';
									$visitorEmails[$id] = !empty($email) ? $email : 'Unknown';
									$visitorPhones[$id] = $phone;
									unset($visitor);
								}
								$entries = 0;
								foreach($data as $ip=>$theDates) {
									$entries += count($theDates);
									unset($theDates);
									if (!in_array($ip, $visitationCities))
										$visitationCities[] = $ip;
								}

								$newUserActivity->entries = $entries;
								$meta->userActivity[$id] = $newUserActivity;
								unset($data);
							}
						}
					}

					if (isset($meta->portalUsers)) {
						$meta->portalUsers = (array)$meta->portalUsers;
						if (!empty($meta->portalUsers)) foreach($meta->portalUsers as $id=>$data) {
							$pid = intval($id) + $portalUsersOffset;
							if (!isset($visitorNames[$pid])) {// then it's not zero
								$visitor = $PortalUsers->get((object)['where'=>['id'=>$id]]);
								if (!empty($visitor)) {
									$s->log("Fetching portalUser:$id, got ".$visitor[0]->first_name);
									$entries = 0;
									$newUserActivity = new \stdClass();
									$newUserActivity->id = $id;
									$newUserActivity->pid = $pid;
									$newUserActivity->ips= json_decode( json_encode($data) ); // makes a perfect copy of [ip=>[dates,...],ip2=>[dates,...],...]

									$newUserActivity->dates = [];
									$newUserActivity->entries = 0;

									$quizMeta = null;
									$listingMeta = null;
									$siteMeta = null;
									$summaryMeta = null;
									$hiddenMeta = null;
									$phone = AH\fixPhone($visitor[0]->phone);
									if (!empty($visitor[0]->meta)) {
										foreach($visitor[0]->meta as &$pMeta) {
											switch ($pMeta->action) {
												case QUIZS_TAKEN: $quizMeta = $pMeta; break;
												case LISTINGS_VIEWED: $listingMeta = $pMeta; break;
												case SITE_ENTRIES: $siteMeta = $pMeta; break;
												case DAILY_SUMMARY: $summaryMeta = $pMeta; break;
												case HIDDEN_AGENTS: $hiddenMeta = $pMeta; break;
											}
											unset($pMeta);
										}

										$hiddenMetaHasMe = false;
										if ($hiddenMeta) {
											if (!empty($hiddenMeta->list)) foreach($hiddenMeta->list as $hider)
												if ($hider == $wpId) {
													// force PORTAL_USER_HIDDEN onto this portal user for this agent
													$hiddenMetaHasMe = true;
													break;
												}
										}
										if ($hiddenMetaHasMe)
											$visitor[0]->flags |= PORTAL_USER_HIDDEN; // set it
										else
											$visitor[0]->flags &= ~PORTAL_USER_HIDDEN; // clear it


										if ($siteMeta) {
											if (!empty($siteMeta->list)) {
												// $activity = new \stdClass();
												// $activity->type = $siteMeta->action;
												// $activity->dates = [];

												foreach($siteMeta->list as $page=>$data) {													
													// $activity->ips = json_decode( json_encode($data) ); // makes a perfect copy of [ip=>[oneSet,...],ip2=>[oneSet,...],...]
													
													foreach($data as $ip=>$theDates) {
														foreach($theDates as $oneSet) {
															// $newUserActivity->entries += intval($oneSet->count); // add up number of times this quiz was run
															if (!isset($newUserActivity->dates[$oneSet->date]))
																$newUserActivity->dates[$oneSet->date] = (object)['quiz'=>[],
																												  'listing'=>[],
																												  'visit'=>[]];
															if (!isset($newUserActivity->dates[$oneSet->date]->visit[$page]))
																$newUserActivity->dates[$oneSet->date]->visit[$page] = [];
															$newUserActivity->dates[$oneSet->date]->visit[$page][] = $ip; // chance of same page by same portal user on different IP on same day
															$newUserActivity->entries++;
															unset($oneSet);
														}
														unset($theDates);
														if (!in_array($ip, $visitationCities))
															$visitationCities[] = $ip;
													}
													unset($data);
												}
											}
										}

										if ($quizMeta) {
											if (!empty($quizMeta->list)) {
												// $activity = new \stdClass();
												// $activity->type = $quizmeta->action;
												// $activity->dates = [];

												foreach($quizMeta->list as $quizId=>$data) {
													$out = $QuizActivity->getDetails($quizId);
													foreach($data as $ip=>$theDates) {
														foreach($theDates as $oneSet) {
															// $newUserDisplay->entries += intval($oneSet->count); // add up number of times this quiz was run
															if ( ($spacePos = strpos($oneSet->date, ' ')) !== false)
																$oneSet->date = substr($oneSet->date, 0, $spacePos);
															if (!isset($newUserActivity->dates[$oneSet->date]))
																$newUserActivity->dates[$oneSet->date] = (object)['quiz'=>[],
																												  'listing'=>[],
																												  'visit'=>[]];
															if (!isset($newUserActivity->dates[$oneSet->date]->quiz[$quizId])) {
																$newUserActivity->dates[$oneSet->date]->quiz[$quizId] = $out->status == 'OK' ? $out->data : []; // this is an array from QuizActivity
																$newUserActivity->dates[$oneSet->date]->quiz[$quizId]['ip'] = $ip;  // add to array
																$newUserActivity->dates[$oneSet->date]->quiz[$quizId]['listings'] = []; // add to array
															}
															$newUserActivity->entries++;
															unset($oneSet);
														}
														unset($theDates);
														if (!in_array($ip, $visitationCities))
															$visitationCities[] = $ip;
													}
													if ($out->status == 'OK')
														$quizMeta->list->$quizId->details = $out->data;
													else
														$s->log("Failed to get details for quiz:$quizId");

													unset($data, $out);
													// $newUserDisplay->activity[] = $activity;
												}
											}
										}
										
										if ($listingMeta) {
											if (!empty($listingMeta->list)) {
												$countIt = true;
												foreach($listingMeta->list as $listingId=>$data) {
													$listingId = intval($listingId);
													$out = $Listings->get((object)['where'=>['id'=>$listingId],
																				    'what'=>['price','title','street_address','city','state','beds','baths','city_id','meta']]);
													if (!empty($out)) {
														getCitiesTags($out[0]);
														$listingsTags = $ListingsTags->getGroup((object)[ 'field'=>'tag_id', 'where'=>[ 'listing_id'=>$listingId ] ]);
														$tags = $Tags->get((object)[ 'what' => ['id','tag','type'], 'where'=>[ 'id'=>$listingsTags ] ]);
														$tagList = [];
														if (!empty($tags)) foreach($tags as $tag) {
															$tagList[$tag->id] = $tag;
															unset($tag);
														}
														if (!empty($out[0]->cityTags)) foreach($out[0]->cityTags as $tag) {
															if (!array_key_exists($tag->id , $tagList) )
																$tagList[$tag->id] = $tag;
															unset($tag);
														}
														$out[0]->tags = $tagList; // every tag associated with this listing, including city tags
														$details = $out[0];
														$details->quiz = [];
														$foundLISTING_VISITATIONS = false;
														if (!empty($out[0]->meta)) foreach($out[0]->meta as $lmeta) {
															if ($lmeta->action == LISTING_VISITATIONS) {
																$foundLISTING_VISITATIONS = true;
																if (!empty($lmeta->portalUsers)) foreach($lmeta->portalUsers as $uid=>$ips) {
																	$uid = intval($uid);
																	// if portalUser id in Seller::SELLER_VISITATIONS is the same as the 
																	//    portalUser id in Listing::LISTING_VISITATIONS, then remember quizId(s) taken
																	/**
																	 * 
																	    [{"action":16,
																			"users":
																				[],
																			"portalUsers":
																				{"17":
																					{"127.0.0.1":
																						{"2016-06-16":
																							{"0":{"tags":"42,145,169,66,49,75,50","quizId":518,"count":3}}}
																					}
																				}
																		}]
																		
																		OR

																		[{"action":16,
																			"users":
																				[],
																			"portalUsers":
																				{"15":
																					{"127.0.0.1":
																						{"2016-06-12":
																							["0","163,165,143,33,36,166,158,129,128"]}},
																				"16":
																					{"127.0.0.1":
																						{"2016-06-14":
																							["163,165,143,33,36,166,158,129,128"]}}
																				}
																		}]

																		OR

																		[{"action":16,
																			"users":
																				{"8":
																					{"107.202.64.174":
																						{"2016-01-05":
																							["0"]}},
																				"0":
																					{"66.249.79.113":
																						{"2016-04-06":
																							["0"]},
																					"66.249.79.186":
																						{"2016-04-12":
																							["0"],
																					"0":
																						{"2016-04-14":
																							["0"]}
																					}}
																				}
																		}]
																	*/

																	if ($uid == $id) { // this group is the one generated by the PortalUser in the seller's meta
																		foreach($ips as $addr=>$theDates) { // {"ip": {"date":[tagString, {"tags":tagString,"quizId":#id,"count":#}, ...], "date2":[tagString, tagString,...], ...}}
																			foreach($theDates as $date=>$data) {
																				// find matching date from newUserActivity pile
																				if (!isset($newUserActivity->dates[$date]))
																					$newUserActivity->dates[$date] = (object)['quiz'=>[],
																															  'listing'=>[],
																															  'visit'=>[]];
																				// if (!isset($newUserActivity->dates[$date]->listing[$listingId]))
																				// 	$newUserActivity->dates[$date]->listing[$listingId] = [];
																				// now look through the
																				foreach($data as $i=>$oneSet) { // oneSet can be either a tagString, or object: {"tags":tagString,"quizId":#id,"count":#}
																					if ( gettype($oneSet) != 'string' ) {
																						if ( isset($oneSet->quizId) &&
																						 	!empty($oneSet->quizId) ) {
																							$oneSet->quizId = intval($oneSet->quizId);
																							// older way, of gathering data, keeping it just in case...
																							if (!in_array($oneSet->quizId, $details->quiz)) {
																								$details->quiz[] = $oneSet->quizId;
																							}
																							// try to match this $oneSet->quizId to any exiting
																							// quizzes in $newUserDisplay->datees[$date].  Use exact $date for the particular $quizId
																							// so that the display data is associated on per day per quiz
																							$found = false;
																							if (!empty($newUserActivity->dates[$date]->quiz)) foreach($newUserActivity->dates[$date]->quiz as $quizId=>&$qDetails) {
																								if ($quizId == $oneSet->quizId) {
																									if (!isset($qDetails['listings']))
																										$qDetails['listings'] = [];
																									$qDetails['listings'][$listingId] = $out[0]; // this is an object of listing data
																									$qDetails['listings'][$listingId]->searchTags = $oneSet->tags;
																									$qDetails['listings'][$listingId]->ip = $addr;
																									$newUserActivity->entries++;
																									$found = true;
																								}
																								unset($qDetails);
																							}

																							// must be listing viewed directly, not from quiz that was recorded?
																							if (!$found) {
																								$s->log("Failed to find quiz:$oneSet->quizId for LISTING_VISITATIONS in listing:$listingId from quizMeta, creating a new entry");
																								$qDetails = $QuizActivity->getDetails($oneSet->quizId);
																								$newUserActivity->dates[$date]->quiz[$oneSet->quizId] = $qDetails->data; // this is an array from QuizActivity
																								$newUserActivity->dates[$date]->quiz[$oneSet->quizId]['ip'] = $addr; // add to array
																								$newUserActivity->dates[$date]->quiz[$oneSet->quizId]['listings'] = []; // add to array
																								$newUserActivity->dates[$date]->quiz[$oneSet->quizId]['listings'][$listingId] = $out[0]; // this is an object of listing data
																								$newUserActivity->dates[$date]->quiz[$oneSet->quizId]['listings'][$listingId]->searchTags = $oneSet->tags; // this is a tagString from $_COOKIE['TagsForListing']
																								$newUserActivity->dates[$date]->quiz[$oneSet->quizId]['listings'][$listingId]->ip = $addr;
																								$newUserActivity->entries++;
																								unset($qDetails);
																							}

																						}																						
																					}
																					else { // it's a string of numbers, i.e: "9, 46, 123, 79"
																						if (!isset($newUserActivity->dates[$date]->listing[$listingId]))
																							$newUserActivity->dates[$date]->listing[$listingId] = (object)['detail'=>$out[0], // this is an object of listing data
																																						   'ip'=>$addr,
																																						   'searchTags'=>[]]; // we can have multiple viewing of same listing on same day with multiple tag lists
																						$newUserActivity->dates[$date]->listing[$listingId]->searchTags[] = $oneSet; // this is a tagString from $_COOKIE['TagsForListing']
																						$newUserActivity->entries++;
																					} // end else if ( gettype($oneSet) != 'string' ) 
																					unset($oneSet);
																				} // end foreach($data as $i=>$oneSet)
																				unset($data);
																			} // end foreach($theDates as $date=>$data)
																			unset($theDates);
																			if (!in_array($addr, $visitationCities))
																				$visitationCities[] = $addr;
																		} // end foreach($ips as $addr=>$theDates)
																	} // end if ($uid == $id)
																	unset($ips);
																} // end foreach($lmeta->portalUsers as $uid=>$ips) 
															} // end if ($lmeta->action == LISTING_VISITATIONS)
															unset($lmeta);
														} // end foreach($out[0]->meta as $lmeta)

														// uh, oh, if not $foundLISTING_VISITATIONS, then revert to using the PortalUser's $listingMeta data
														if (!$foundLISTING_VISITATIONS) {
															foreach($data as $ip=>$theDates) {
																foreach($theDates as $oneSet) {
																	if (!isset($newUserActivity->dates[$oneSet->date]))
																		$newUserActivity->dates[$oneSet->date] = (object)['quiz'=>[],
																														  'listing'=>[],
																														  'visit'=>[]];
																	if (!isset($newUserActivity->dates[$oneSet->date]->listing[$listingId]))
																		$newUserActivity->dates[$oneSet->date]->listing[$listingId] = (object)['detail'=>$out[0], // this is an object of listing data
																																			   'ip'=>$ip,
																																			   'searchTags'=>[]]; // we can have multiple viewing of same listing on same day with multiple tag lists
																	$newUserActivity->entries++;
																	unset($oneSet);
																}
																unset($theDates);
																if (!in_array($ip, $visitationCities))
																	$visitationCities[] = $ip;
															}
														} // end if (!$foundLISTING_VISITATIONS)
														$listingMeta->list->$listingId->details = $details;
														unset($listingsTags, $tags, $details);
													} // end if (!empty($out))
													else
														$s->log("Failed to get details for listing:$listingId");
													unset($data, $out);
												} // end foreach($listingMeta->list as $listingId=>$data)
											} // end if (!empty($listingMeta->list))
										} // end if ($listingMeta) 

										if ($summaryMeta) {
											usort($summaryMeta->list, function($a, $b) {
												return strtotime($b->date) - strtotime($a->date);
											});

										}
									} // end if (!empty($visitor[0]->meta))
									$newUserActivity->dates = (array)$newUserActivity->dates;
									$newUserActivity->summary = $summaryMeta;
									// $s->log("before ksort:".print_r($newUserActivity, true));
									// array_reverse($newUserActivity->dates, true);
									krsort($newUserActivity->dates, SORT_NATURAL);
									// $s->log("after krsort:".print_r($newUserActivity, true));
									$visitor[0]->activity = $newUserActivity; // save the activity structure
									$visitor[0]->last_name = !empty($visitor[0]->last_name) ? $visitor[0]->last_name : '';
									$visitor[0]->phone = !empty($visitor[0]->phone) ? $phone : $visitor[0]->phone;
									$portalUsersList[$id] = $visitor[0];
									$visitorNames[$pid] = !empty($visitor) ? (!empty($visitor[0]->first_name) ? $visitor[0]->first_name.(!empty($visitor[0]->last_name) ? ' '.$visitor[0]->last_name : '') : 'Anonymous') : 'Anonymous';
									$visitorEmails[$pid] = !empty($visitor) && !empty($visitor[0]->email) ? $visitor[0]->email : 'Unknown';
									$visitorPhones[$pid] = $phone;
								} // end if (!empty($visitor))
								unset($visitor);
							} // end if (!isset($visitorNames[$pid]))
							unset($data);
						} // end foreach($meta->portalUsers as $id=>$data) 
					} // end if (isset($meta->portalUsers))
					break;
				case SELLER_AGENT_ORDER:
					$meta->item = (array)$meta->item;
					foreach($meta->item as $item) {
						$item = (object)$item;
						if ($item->type == ORDER_AGENT_MATCH &&
							$item->mode == ORDER_BOUGHT) {
							$hasBoughtLifestyle = 1;
						}
						unset($item);
						if ($hasBoughtLifestyle) break;
					}
					break;

				case SELLER_INVITE_CODE:
					$inviteCode = $this->getClass('Invitations')->get((object)['where'=>['code'=>$meta->key]]);
					if (!empty($inviteCode))
						$inviteCode = array_pop($inviteCode);
					else
						$inviteCode = [];
					break;
			} // end switch ($meta->action)
			unset($meta);
		}
	} // end if (!empty($x[0]->meta))

	if (!empty($portalUsersList)) {
		// ok, lets sort this based on $portalUsersList[id]->activity->entries
		// uasort($portalUsersList, function($a, $b) {
		// 	return $b->activity->entries - $a->activity->entries; // should sort in descending order
		// });

		// sort based on most recent activity
		uasort($portalUsersList, function($a, $b){
			$keyB = array_keys($b->activity->dates);
			$keyA = array_keys($a->activity->dates);
			$timeB = !empty($keyB) && isset($keyB[0]) ? strtotime($keyB[0]) : (!empty($keyA) && isset($keyA[0]) ? strtotime($keyA[0]) : 0);
			$timeA = !empty($keyA) && isset($keyA[0]) ? strtotime($keyA[0]) : 0;
			if ($timeB != $timeA)
				return $timeB - $timeA;
			else
				return $b->activity->entries - $a->activity->entries; // should sort in descending order
		});

		$tempList = [];
		foreach($portalUsersList as $portalUser)
			$tempList[] = $portalUser;

		unset($portalUsersList);
		$portalUsersList = $tempList;
		unset($tempList);

		if ($dumpVisitationMetaData)
			$s->log("portalUsersList: ".print_r($portalUsersList, true));
	}

	// $listings = $Listings->get((object)['where'=>['author'=>$wpId,
	// 										 // 'listhub_key'=>'notnull',
	// 									   'author_has_account'=>1]]);
	
	if (!empty($listings)) foreach($listings as $listing) {
		// if (!empty($listing->listhub_key)) {
		// 	$fromListhub++;
		// }
		$clicks = 0;
		if (!empty($listing->meta)) foreach($listing->meta as $meta) {
			if ($meta->action == LISTING_VISITATIONS) {
				$meta->users = (array)$meta->users;
				if (!empty($meta->users)) foreach($meta->users as $id=>$ips) {
					foreach($ips as $addr=>$theDates) {
						$clicks = count($theDates);
						unset($theDates);
					}
					unset($ips);
				}

				if (isset($meta->portalUsers)) {
					$meta->portalUsers = (array)$meta->portalUsers;
					if (!empty($meta->portalUsers)) foreach($meta->portalUsers as $id=>$ips) {
						$pid = intval($id) + $portalUsersOffset;
						$visitor = $newUserActivity = null;
						if (!isset($visitorNames[$pid])) {// then it's not zero, and wasn't in the SELLER_VISITATION list of portal Users
							$visitor = $PortalUsers->get((object)['where'=>['id'=>$id]]);
							if (!empty($visitor)) {
								$s->log("Fetching listingId:$listing->id, portalUser:$id, got ".$visitor[0]->first_name);
								$visitor[0]->last_name = !empty($visitor[0]->last_name) ? $visitor[0]->last_name : '';
								$visitorNames[$pid] = !empty($visitor) ? (!empty($visitor[0]->first_name) ? $visitor[0]->first_name.' '.(!empty($visitor[0]->last_name) ? ' '.$visitor[0]->last_name : '') : 'Anonymous') : 'Anonymous';
								$visitorEmails[$pid] = !empty($visitor) && !empty($visitor[0]->email) ? $visitor[0]->email : 'Unknown';
								$visitorPhones[$pid] = AH\fixPhone($visitor[0]->phone);
								unset($visitor);
							}
						}
						foreach($ips as $addr=>$theDates) {
							foreach($theDates as $date=>$data) {
								foreach($data as $oneSet) {
									if (gettype($oneSet) != 'string') {
										$oneSet = (object)$oneSet;
										$clicks += $oneSet->count;
									}
									else
										$clicks++;
									unset($oneSet);
								}
								unset($data);
							}
							unset($theDates);
							if (!in_array($addr, $visitationCities))
								$visitationCities[] = $addr;
						}
						unset($ips);
					}
				}
			}
			unset($meta);
		}
		if (!isset($listing->clicks) ||
		 	 $listing->clicks != $clicks)
			$Listings->set([(object)['where'=>['id'=>$listing->id],
									 'fields'=>['clicks'=>$clicks]]]);
		unset($listing);
	}
	unset($listings);

	if (!empty($visitationCities)) {
		$ipCities = [];
		foreach($visitationCities as $ip) {
			$city = $IpList->get((object)['where'=>['ip'=>$ip]]);
			if (!empty($city)) {
				$ipCities[$ip] = $city[0]->city.(!empty($city[0]->state) ? ", ".$city[0]->state : '');
				unset($city);
			}
			else {
				$city = $IpList->getCity($ip);
				if (!empty($city))
					$ipCities[$ip] = $city['city'].(!empty($city['state']) ? ", ".$city['state'] : '');
				else
					$ipCities[$ip] = $ip;
			}
			unset($ip);
		}
		unset($visitationCities);
		$visitationCities = $ipCities;
	}
}

if ($havePortal == PORTAL_NOT_EXIST &&
	empty($reservedPortalName) &&
	!empty($x)) { // then no SELLER_NICKNAME, so check if there's a reservation with a portal
	require_once(__DIR__.'/../_classes/Reservations.class.php'); $Reservations = new AH\Reservations();
	$res = $Reservations->get((object)['where'=>['email'=>$x[0]->email]]);
	if (!empty($res) &&
		!empty($res[0]->portal)) {
		$SessionMgr = new AH\SessionMgr;
		$out = $SessionMgr->checkPortalName($res[0]->portal);
		if ($out->status == 'OK') {
			$reservedPortalName = $res[0]->portal;
			$havePortal = PORTAL_RESERVED;
			$s->log("Reservation:{$res[0]->id} has a valid portal:{$res[0]->portal}");
		}
		else {
			$havePortal = PORTAL_INVALID;
			$portalMsg = $out->data;
			$s->log("Reservation:{$res[0]->id} has an invalid portal:{$res[0]->portal}, why: $out->data");
		}
	}

}

$x = ($wpId == 0)  ? "0" :
     (current_user_can("create_users") ? (!empty($x) ?  json_encode($x[0]) :  "2") :
	 (!empty($x) ?  json_encode($x[0]) :  "1")); 

$amTags = $s->getAMTagList();
$lifestyleOptions = '';
foreach($amTags as $tag_data) {
	$lifestyleOptions .= '<option value="'.$tag_data->id.'">'."$tag_data->tag".'</option>';
}
// $browser = AH\getBrowser();
global $browser;

$Tags = new AH\Tags();
$allTags = $Tags->get();
$y = [];
foreach($allTags as $tag) {
	$y[$tag->id] = $tag;
	unset($tag);
}

$allTags = $y;
unset($y);

global $thisPage; 
$opt = $Options->get((object)['where'=>['opt'=>'VideoList']]);
$videoList = new stdClass();
if (!empty($opt)) {
  $videos = json_decode($opt[0]->value);
  foreach($videos as $page=>$video) {
    if ( strpos(strtolower($page), $thisPage) === 0) {
      $videoList->$page = $video;
    }
  }
}

/**
Add code here to find out what this seller should see in the dashboard cards.
User the seller::flags to see if they have any products, portal and/or lifestyled agent
Depending of that, specific xml file will be read in and parsed to populate a <ul> to be attached by calling $('.page-home').html().
This <ul> structure will be passed as a Javascript variable: var dashboardCards;
*/

$havePortal = $havePortal == PORTAL_VALID || $havePortal == PORTAL_EXPIRING;

$xmlFile = 'basicDashboard.xml';
if ( $havePortal && !$haveLifestyle)
	$xmlFile = 'havePortal.xml';
elseif (!$havePortal && $haveLifestyle)
	$xmlFile = 'haveLifestyle.xml';
elseif ($havePortal && $haveLifestyle)
	$xmlFile = 'haveAll.xml';

$xmlFile = __DIR__."/_templates/$xmlFile";
$dashboardCards = AH\readDashCardFile($xmlFile);

 ?>
<script type="text/javascript">
var validSeller = <?php echo $x; ?>;
var portalMsg = '<?php echo $portalMsg; ?>';
var videoList = <?php echo json_encode($videoList); ?>;
/* NOTE: the following two are now in header.php
//var havePortal = <?php echo $havePortal ?>;
//var nickname = '<?php echo ($havePortal == PORTAL_VALID || $havePortal == PORTAL_EXPIRING) ? $nickname : "<Your name>"; ?>';
*/
var reservedPortalName = '<?php echo $reservedPortalName; ?>';
var amTagList = <?php echo json_encode($amTags); ?>;
var allTags = <?php echo json_encode($allTags); ?>;
var clearBrowserUrl = <?php echo $clearBrowserUrl ? 1 : 0; ?>;
var browser = <?php echo json_encode( $browser ); ?>;
var visitorNames = <?php echo json_encode( $visitorNames ); ?>;
var visitorEmails = <?php echo json_encode( $visitorEmails ); ?>;
var visitorPhones = <?php echo json_encode( $visitorPhones ); ?>;
var visitationCities = <?php echo json_encode( $visitationCities ); ?>;
var portalUsersList = <?php echo json_encode( $portalUsersList ); ?>;
var portalUsersOffset = <?php echo $portalUsersOffset ? $portalUsersOffset : 0; ?>;
var agentOptions = <?php echo json_encode( $agentOptions ); ?>;
var captureOptions = <?php echo json_encode( $captureOptions ); ?>;
var fromListhub = <?php echo $fromListhub ? 1 : 0; ?>;
var hasBoughtLifestyle = <?php echo $hasBoughtLifestyle ? 1 : 0; ?>;
var isFree = <?php echo $isFree ? 1 : 0; ?>;
var isFreeLA = <?php echo $isFreeLA ? 1 : 0; ?>;
var discount = <?php echo $discount ? $discount : 0; ?>;
var coupon = '<?php echo $coupon; ?>';
var discountLA = <?php echo $discountLA ? $discountLA : 0; ?>;
var couponLA = '<?php echo $couponLA; ?>';
var maxFreeLA = <?php echo $maxFreeLA ? $maxFreeLA : 0; ?>;
var usStates = <?php echo json_encode( $usStates ); ?>;
var dashboardCards = <?php echo json_encode($dashboardCards); ?>;
var lifestyleOptions = '<?php echo $lifestyleOptions; ?>';
var showSiteEntryStatsInActivityPage = <?php echo $showSiteEntryStatsInActivityPage ? 1 : 0; ?>;
var sellerLeadCaptureDivs = <?php echo json_encode(['divs'=>$sellerLeadCaptureDivs]); ?>;
var sellerLeadCaptureDivTitleList = <?php echo json_encode($sellerLeadCaptureDivTitleList); ?>;
var doLeadCaptureSetup = <?php echo $doLeadCaptureSetup ? 1 : 0; ?>;
var portalDefaultPrice = <?php echo $portalDefaultPrice; ?>;
var inviteCode = <?php echo json_encode($inviteCode); ?>;
var premiumPortalMode = <?php echo $premiumPortalMode ? 1 : 0; ?>;
var associations = <?php echo json_encode($associations); ?>;
var agentPageOption = <?php echo json_encode($agentPageOption); ?>;
//<?php if ($clearBrowserUrl) : ?>
//window.location.hash = '<?php echo $hash; ?>';
//<?php else: ?>
//window.history.pushState('pure','Title',ah_local.wp+'/sellers');
//<?php endif; ?>
</script>

<?php
$x = $Options->get((object)['where'=>['opt'=>'PortalAgentProductID']]);
$productIdPortalAgent = !empty($x) ? intval($x[0]->value) : 0;
$wooPortalAgent = '';
if ($productIdPortalAgent &&
	isset($woocommerce) &&
	!empty($woocommerce)) {
	$wooPortalAgent = do_shortcode( '[add_to_cart id="'.$productIdPortalAgent.'"]' ); 
	$wooPortalAgent = str_replace("\n","", $wooPortalAgent); 
}

$x = $Options->get((object)['where'=>['opt'=>'AgentMatchProductPricing']]);
$productPricing = !empty($x) ? json_decode($x[0]->value) : null;

?>
<script type="text/javascript">
var productPricing = <?php echo json_encode($productPricing); ?>;
var ecommercePortalAgent = '<div class="ecommerce_PortalAgent">';
ecommercePortalAgent += '<?php	echo $wooPortalAgent; ?>' + '</div>';
var productIdPortalAgent = '<?php echo $productIdPortalAgent; ?>';
</script>	


<?php 
$x = $Options->get((object)['where'=>['opt'=>'AgentMatchProductID']]);
$productIdAgentMatch = !empty($x) ? intval($x[0]->value) : 0;
$wooAgentMatch = '';
if ($productIdAgentMatch &&
	isset($woocommerce) &&
	!empty($woocommerce)) {
	$wooAgentMatch = do_shortcode( '[add_to_cart id="'.$productIdAgentMatch.'"]' ); 
	$wooAgentMatch = str_replace("\n","", $wooAgentMatch); 
}
?>
<script type="text/javascript">
var ecommerceAgentMatch = '<div class="ecommerce_AgentMatch">';
ecommerceAgentMatch += '<?php	echo $wooAgentMatch; ?>' + '</div>';
var productIdAgentMatch = '<?php echo $productIdAgentMatch; ?>';
</script>	

<script type="text/javascript">
	jQuery(document).ready(function($){
		
		// ---- Seller's Menu Clicks ---- //
		$('.mobilemenutop .mobile-menu-title').on('click', function() {
			$('.mobilemenutop .centerarrow .arrow .click').click();
		});
		
		$('.mobilemenutop .centerarrow .arrow .click').on('click', function() {
			transitionEnd = 'transitionend webkitTransitionEnd otransitionend MSTransitionEnd';
			if ($('.mobile-profile-menu .profile-menu').hasClass('animatedown')){
				sellerMenuClose();
			}
			else {
				sellerMenuOpen();
			}
		});
		$('.mobile-profile-menu .profile-menu li a').on('click', function() {
				sellerMenuClose();
		});
		
		// ---- Page Transitions ---- //
		$('.mainmenu .profile-menu li a').on('click', function() {
			sellerTransitionStart();
		});
		
	});
</script>


<div id="overlay-bg" class="leadCaptureDivs" style="display: none;">
	<?php echo $sellerLeadCaptureDivs; ?>
</div>

<div id="overlay-bg" class="companiesList" style="display: none;">
	<div class="spin-wrap" style="display: none;"><div class="spinner sphere"></div></div>
	<div id="companiesDiv">
	<?php echo $companySelectionList; ?>
	</div>
	<div id="createCompanyDiv" style="display: none;">
		<div id="title">
			<span>Enter values for your company</span>
		</div>
		<div id="name">
			<span id="label">Name</span><input type="text" name="name"></input>
		</div>
		<?php if ($agentPageOption->showExtendedCompanyFields) : ?>
		<div id="address">
			<span id="label">Street Address</span><input type="text" name="street_address"></input>
		</div>
		<div id="city">
			<span id="label">City and State</span><input type="text" name="city" placeholder="eg: San Francisco, CA" value=""></input>
		</div>
		<div id="zip">
			<span id="label">ZIP</span><input type="number" name="zip"></input>
		</div>
		<div id="contact">
			<span id="label">Contact</span><input type="text" name="contact"></input>
		</div>
		<div id="email">
			<span id="label">Email</span><input type="email" name="contact_email"></input>
		</div>
		<div id="phone1">
			<span id="label">Phone 1</span><input type="text" name="phone1"></input>
		</div>
		<div id="phone2">
			<span id="label">Phone 2</span><input type="text" name="phone2"></input>
		</div>
		<div id="url">
			<span id="label">Website</span><input type="url" name="url"></input>
		</div>
		<div id="type">
			<span id="desc">Type of company:</span>
			<div id="radio"><span id="title">Brokerage </span><input type="radio" name="type" what="brokerage" value="1" /></div>
			<div id="radio"><span id="title">Company </span><input type="radio" name="type" what="company" value="2" /></div>
			<div id="radio"><span id="title">Non Profit </span><input type="radio" name="type" what="nonprofit" value="3" /></div>
		</div>
	    <?php endif; ?>
		<div id="logo" class="logo">
		</div>	
		<div id="controls">
			<button id="ok">Done</button>
			<button id="exit">Cancel</button>
			<span id="error" class="hidden"></span>
		</div>
	</div>
	<div id="editLogo" style="display: none;">
		<div id="title" class="hidden">
			<span id="loadingMessage">Loading logo image...</span>
		</div>
		<div id="editor" class="hidden">
			<div id="title"><span id="title">Please adjust the cropping for your photo, then click save.</span></div>
			<img id="editImage" src="" />
		</div>
		<div id="controls">
			<button id="save">Save</button>
			<button id="exit">Cancel</button>
			<span id="error" class="hidden"></span>
		</div>
	</div>
</div>

<div class="global-signin-popup">
      <div class="blackout"></div>
      <div class="signin-content" style="height: auto;">
		<div class="close">x</div>
        <div class="signin-header"><span style="font-weight:400;">Welcome back!</span> Please sign in</div>
        <div class="signin-wrapper">
          <form name="login-form" id="login-form" action="<?php echo site_url().'/wp-login.php'; ?>" method="post">
          	<p class="login-username">
          		<input type="text" name="log" id="user_login" class="input" value="" placeholder="Username">
          		<input type="password" name="pwd" id="user_pass" class="input" value="" placeholder="Password">
          		<input type="submit" name="wp-submit" id="wp-submit" class="button-primary" value="Log In">
          		<input type="hidden" name="redirect_to" value="<?php echo site_url()."/sellers"; ?>">
          		<p class="forgot-password"><a href="'+lostPasswordUrl+'" title="Forgot your password?">Forgot your password?</a></p>
          	</p>
          </form>
          <!-- <a href="<?php echo site_url(); ?>" style="margin: auto 37%;">Go to Home</a> -->
		</div>
    </div>
</div>

<section id="seller-admin">
	<div class="videoDiv">
		<button class="closeVid"><span class="line1"></span><span class="line2"></span></button>
		<span class="video">
    </span>
	</div>
	<div class="container">
			<aside class="sidebar">
				<div class="mainmenu">
					<div class="mobilemenutop">
                      <div class="show-nav-modal">
                        <p>This is your agent navigation bar! Use it to get to all of your content such as listings, profile, and account info.</p>
                        <button class="close">Got it!</button>
                      </div>
                      <span class="mobile-menu-title"><?php echo $havePortal ? '<span style="color:#50e3c2; font-weight:400;">Premium</span> Menu' : 'Seller\'s Menu'; ?></span>
                      <div class="centerarrow">
                        <div class="arrow">
                          <span class="click"><span class="left"></span><span class="right"></span></span>
                        </div>
                      </div>
                    </div>
						<?php if ($havePortal) : ?>
							<div class="premium-agent">
                              <span class="main-text"><a href="javascript:seller.getPage('home');"><span>Premium</span> Agent Dashboard</a></span>
                            </div>
							<div class="premium-agent-bg"></div>
							<ul class="profile-menu desktop premium">
                            <div class="show-nav-modal">
                              <p>This is your agent navigation bar! Use it to get to all of your content such as listings, profile, and account info.</p>
                              <button class="close">Got it!</button>
                            </div>
							<li class="menu-item" id="prem-lis" data-page="stats"><a href="javascript:seller.getPage('stats');">Stats</a></li>
							<li class="menu-item" id="prem-lis" data-page="portal"><a href="javascript:seller.getPage('portal');">Tools</a></li>
							<div class="prem-bg"></div>
						<?php else: ?>
							<ul class="profile-menu desktop">
                            <div class="show-nav-modal">
                              <p>This is your agent navigation bar! Use it to get to all of your content such as listings, profile, and account info.</p>
                              <button class="close">Got it!</button>
                            </div>
							<li class="menu-item" data-page="home"><a href="javascript:seller.getPage('home');">Dashboard</a></li>
							<li class="menu-item" data-page="stats"><a href="javascript:seller.getPage('stats');">Stats</a></li>
							<li class="menu-item" data-page="portal"><a href="javascript:seller.getPage('portal');">Client Portal</a></li>
						<?php endif; ?>
						<li class="menu-item" data-page="profile"><a href="javascript:seller.getPage('profile');">My Profile</a></li>
						<!-- <li class="menu-item"><a href="<?php bloginfo('wpurl'); ?>/new-listing">Add Listing</a></li> -->
						<li class="menu-item" data-page="listings"><a href="javascript:seller.getPage('listings');">My Listings</a></li>
						<li class="menu-item" data-page="agent_match"><a href="javascript:seller.getPage('agent_match_reserve');">LifeStyled Agent</a></li>					
						<li class="menu-item" data-page="account"><a href="javascript:seller.getPage('account');">My Account</a></li>
					<?php if ($itemsInCart)	: ?>
						<li class="menu-item" data-page="cart"><a href="javascript:seller.getPage('cart');">My Cart</a></li>		
					<?php endif; ?>
					</ul>
					<div class="mobile-profile-menu">
						<ul class="profile-menu">
							<li class="menu-item" data-page="home"><a href="javascript:seller.getPage('home');">Dashboard</a></li>
							<li class="menu-item" data-page="stats"><a href="javascript:seller.getPage('stats');"><?php echo $havePortal ? '<span id="premium"><span class="premium-color">Premium</span>&nbsp;</span>Stats' : 'Stats'; ?></a></li>
							<li class="menu-item" data-page="portal"><a href="javascript:seller.getPage('portal');"><?php echo $havePortal ? '<span id="premium"><span class="premium-color">Premium</span>&nbsp;</span>Tools' : 'Agent Portal'; ?></a></li>
							<li class="menu-item" data-page="profile"><a href="javascript:seller.getPage('profile');">My Profile</a></li>
							<li class="menu-item" data-page="listings"><a href="javascript:seller.getPage('listings');">My Listings</a></li>
							<li class="menu-item" data-page="agent_match"><a href="javascript:seller.getPage('agent_match_reserve');">LifeStyled Agent</a></li>					
							<li class="menu-item" data-page="account"><a href="javascript:seller.getPage('account');">My Account</a></li>
						<?php if ($itemsInCart)	: ?>
							<li class="menu-item" data-page="cart"><a href="javascript:seller.getPage('cart');">My Cart</a></li>		
						<?php endif; ?>
						</ul>
					</div>
				</div>
			</aside>
            <div class="mobile-modal-bg"></div>
			<div class="loading-overlay-sellers">
				<p>Loading...</p>
				<div class="spin-wrap">
					<div class="spinner">
						<div class="cube1"></div>
						<div class="cube2"></div>
					</div>
				</div>
			</div>
			<div id="details">
				<a href="javascript:;"></a>
				<div id="description">
				</div>
				<button id="details-done">Done</button>
			</div>
			<div id="portal-user-activity">
				<div id="id-block">
					<span id="first-name"></span><span id="last-name"></span>
					<a id="mailto" href="javascript:;"></a>
				</div>
				<div id="activities">
					<table id="main">
						<thead>
							<th id="main">Quizzes taken</th><th id="main">Listings viewed</th><th id="main">Site Entry Points</th>
						</thead>
						<tbody>
							<tr>
								<td id="main"><div id="quizzes">
										<table>
											<thead>
												<th>ID</th><th>Location</th><th>Date/Count</th>
											</thead>
											<tbody>
											</tbody>
										</table>
								</div></td>
								<td id="main"><div id="listings">
										<table>
											<thead>
												<th>ID</th><th>Location</th><th>Date/Count</th>
											</thead>
											<tbody>
											</tbody>
										</table>
								</div></td>
								<td id="main"><div id="entries">
										<table>
											<thead>
												<th>Page</th><th>Location</th><th>Date/Count</th>
											</thead>
											<tbody>
											</tbody>
										</table>
								</div></td>
							<tr>
						</tbody>
					</table>
				</div>
				<button id="portal-user-activity-done">Done</button>
			</div>
			<section id="seller-content" class="content"></section>
	</div>
	<div class="clearfix"></div>
</section>
