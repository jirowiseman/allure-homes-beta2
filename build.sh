#!/bin/bash
# requires node, less & less-plugin-clean-css

rm -rf _css/_sellers/*.css && printf "Erasing /_css/_sellers/*.css\n" &&
rm -rf _css/_compiled/*.css && printf "Erasing /_css/_compiled/*.css\n\n" &&

ls _css/_req/*.less | while read file
do
    ( css="_css/_compiled/"${file:10} &&
    css=${css:0:$(( ${#css} - 5 ))}".css" &&
    lessc --clean-css "$file" "$css" &&
    printf $file" -> "$css"\n" ) &
done

ls _css/_pages/*.less | while read file
do
    ( css="_css/_compiled/"${file:12} &&
    css=${css:0:$(( ${#css} - 5 ))}".css" &&
    lessc --clean-css "$file" "$css" &&
    printf $file" -> "$css"\n" ) &
done

ls _css/_sellers/*.less | while read file
do
    ( css=${file:0:$(( ${#file} - 5 ))}".css" &&
    lessc --clean-css "$file" "$css" &&
    printf $file" -> "$css"\n" ) &
done

ls _css/_admin/*.less | while read file
do
    ( css="_css/_compiled/admin-"${file:12} &&
    css=${css:0:$(( ${#css} - 5 ))}".css" &&
    lessc --clean-css "$file" "$css" &&
    printf $file" -> "$css"\n" ) &
done

ls _css/style.less | while read file
do
    ( css="_css/_compiled/style.css" &&
    lessc --clean-css "$file" "$css" &&
    printf $file" -> "$css"\n" ) &
done