<?php
require_once(__DIR__.'/../_classes/Listings.class.php'); $Listings = new AH\Listings(1);
require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new AH\Sellers(1); 

// $Sellers = $Sellers->get((object)[ 'what' => ['id','author_id','first_name','last_name'] ]);
$sql = "SELECT a.id, a.author_id,a.first_name,a.last_name FROM {$Sellers->getTableName()} AS a ";
$sql.= "INNER JOIN {$Sellers->getTableName('sellers-email-db')} AS b ";
$sql.= "ON b.seller_id = a.id ";
$sql.= "WHERE b.flags & (1 << ".(AH_ACTIVE + 20)." | 1 << ".(AH_WAITING + 20).")";
$Sellers->log("listing page - sql:$sql");

$Sellers = $Sellers->rawQuery($sql);
global $wpdb;
if (!isset($_GET['page_number'])) $_GET['page_number'] = 0;
// $x = $Listings->get((object)[ 'what'=>['id','title','author','author_has_account'], 
							  // 'or'=>['active'=>AH_ACTIVE,
							  // 	     'active'=>AH_WAITING],
							  // 'orderby'=>'id', 
							  // 'page'=>$_GET['page_number'], 'limit'=>30 ]);
$sql = "SELECT `id`,`title`,`author`,`author_has_account` FROM {$Listings->getTableName()} ";
$sql.= "WHERE `active` = ".AH_ACTIVE." OR `active` = ".AH_WAITING." ";
$sql.= "ORDER BY `id` ";
$sql.= "LIMIT ".($_GET['page_number']*30).", 30";
$x = $Listings->rawQuery($sql);

$countActive = intval($wpdb->get_var("SELECT COUNT(*) FROM {$Listings->getTableName()} WHERE `active` = ".AH_ACTIVE));
$countWaiting = intval($wpdb->get_var("SELECT COUNT(*) FROM {$Listings->getTableName()} WHERE `active` = ".AH_WAITING));
?>
<script type="text/javascript">ah_local = {
	tp: '<?php echo get_template_directory_uri(); ?>',
	wp: '<?php echo get_home_url(); ?>',
	authors: <?php echo json_encode($Sellers); ?>
}</script>
<style>
.column-action{ width: 170px; }
.column-author{ width: 160px; }
</style>
<p style="right:2em; position: absolute;">
	<a href="<?php echo get_home_url().'/wp-admin/admin.php?page=listings&page_number='.(intval($_GET['page_number'])-1 >= 0 ? intval($_GET['page_number'])-1 : 0); ?>">&laquo; Prev</a> | 
	<a href="<?php echo get_home_url().'/wp-admin/admin.php?page=listings&page_number='.(intval($_GET['page_number'])+1); ?>">Next &raquo;</a>
</p>
<p>Total Active Listings:<?php echo $countActive; ?> and Waiting Listings: <?php echo $countWaiting?> | <a href="<?php bloginfo('wpurl'); ?>/sellers/new-listing/">Create New Listing</a></p>
<table class="widefat">
	<thead>
		<tr>
			<th scope="col" class="manage-column column-action">Action</th>
			<th scope="col" class="manage-column column-author">Author</th>
			<th scope="col" class="manage-column column-listing-id">ID</th>
			<th scope="col" class="manage-column column-listing">Listing</th>
			<th scope="col" class="manage-column column-delete">Delete</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th scope="col" class="manage-column column-action">Action</th>
			<th scope="col" class="manage-column column-author">Author</th>
			<th scope="col" class="manage-column column-listing-id">ID</th>
			<th scope="col" class="manage-column column-listing">Listing</th>
			<th scope="col" class="manage-column column-delete">Delete</th>
		</tr>
	</tfoot>
	<tbody>
	<?php
		foreach ($x as  $i => $l){
			echo '<tr';
			if ($i % 2 === 1) echo ' class="alternate"';
			echo '>';
			?>
				<td><a href="javascript:listingAdmin.editAuthor(<?php echo $l->id; ?>, <?php echo $l->author; ?>, <?php echo $l->author_has_account; ?>);">Edit Author</a> | <a href="<?php bloginfo('wpurl'); ?>/new-listing/#listingID=<?php echo $l->id; ?>" target="_blank">Edit Listing</a>
				</td>
				<td><?php
					if ($l->author > 0) {
						$found = false;
						foreach ($Sellers as $a) {
							if ($l->author_has_account &&
								$a->author_id == $l->author) {
									echo $a->first_name.' '.$a->last_name;
									$found = true;
									break;
								}
							elseif (!$l->author_has_account &&
									 $a->id == $l->author) {
								echo $a->first_name.' '.$a->last_name;
								$found = true;
								break;
							}
						} 
						if (!$found)
							echo '(no author)';
					}
					else echo '(no author)';
					?>
				</td>
				<td><?php echo $l->id; ?></td>
				<td><a href="<?php bloginfo('url'); ?>/listing/<?php echo $l->id; ?>" target="_blank"><?php echo $l->title; ?></a></td>
				<td><a href="javascript:listingAdmin.deleteListing(<?php echo $l->id; ?>)">Delete</a></td>
			</tr>
			<?php
		}
	?>
	</tbody>
</table>
