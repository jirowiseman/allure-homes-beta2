var cityAdmin;
jQuery(document).ready(function($) {
	cityAdmin = {
		init: function() {
			current_query = null;
			current_page_number = 0;
			current_fields = {};
			$('.page-menu .prev').hide();
			this.citiesDB = {};
			var $query = {
				page: current_page_number
			};
			if (current_query) $query.query = current_query;
			this.citiesGet({
				data: $query,
				done: function(x) {
					cityAdmin.citiesReceived(x);

					$('#city-search').on('keyup', function() {
						if ($.active && typeof this.currentQuery != 'undefined') this.currentQuery.abort();
						$('#city-search').css('background-color', 'rgba(34,167,240,.05)');
						$('#cities tbody').empty();
						theData = {
							page: 0,
							fields: {
								city: $(this).val()
							}
						};
						cityAdmin.citiesGet({
							ahtb: false,
							data: theData,
							error: function(x) {
								$('#city-search').css('background-color', 'rgba(231,76,60,.05)');
								$('.page-menu a.next:visible, .page-menu a.prev:visible').hide(0.1);
							},
							done: function(x) {
								$('#city-search').css('background-color', 'white');
								if (cityAdmin.isParsing) setTimeout(function() {
									cityAdmin.citiesReceived(x);
								}, 500);
								else cityAdmin.citiesReceived(x);
							}
						});
					});
					$('#filter-state').change(function() {
						$('#cities tbody').empty();
						var theState = $(this).val() === 0 ? '' : $(this).val();
						cityAdmin.citiesGet({
							data: {
								page: 0,
								fields: {
									state: theState
								}
							},
							done: function(x) {
								cityAdmin.citiesReceived(x);
							}
						});
					});
					$('.city-menu a').on('click', function() {
						switch ($(this).attr('data-query')) {
							// case 'city-tag-xls':
							// 	ahtb.open({
							// 		title: 'Upload City Tag XLS',
							// 		html: '&nbsp;', buttons: [],
							// 		height: 200, width: 600,
							// 		opened: function(){
							// 			$('#tb-submit').prepend($('<form id="dropzone" class="dropzone"></form>'));
							// 			var dzxls = new Dropzone("#dropzone", {
							// 				// acceptedFiles:'*',
							// 				url: ah_local.tp+"/_admin/ajax_upload_xls.php",
							// 				headers: {'xlstype':"city-tag"},
							// 				maxFiles: 1,
							// 				accept: function(file, done) { ahtb.resize(450, 350); done(); }
							// 			});
							// 			dzxls.on("success", function(file, responseText){
							// 				x = $.parseJSON(responseText);
							// 				console.log(x);
							// 			});
							// 			dzxls.on('maxfilesexceeded', function(){ ahtb.alert('Please upload only one file at once.'); });
							// 		}
							// 	});
							// 	break;
							default: console.error('Invalid query: ' + $(this).attr('data-query'));
							break;
						}
					});

					$('.page-menu a').on('click', function() {
						var thePage = null;
						if ($(this).hasClass('next')) thePage = current_page_number + 1;
						else if ($(this).hasClass('prev')) thePage = current_page_number - 1;

						if (thePage !== null) {
							$('#cities tbody').empty();
							cityAdmin.citiesGet({
								done: function(x) {
									cityAdmin.citiesReceived(x);
								},
								data: {
									page: thePage
								}
							});
						}
					});
				}
			});
		},
		cleanCities: function() {
			$.ajax({
				type: "POST",
				url: ah_local.tp + '/_classes/Image.class.php',
				data: {
					query: 'clean',
					data: 'cities'
				},
				success: function(d) {
					console.log(d);
				},
				dataType: "JSON"
			});
		},
		DBNoPopup: function(x) {
			current_query = x.data.query ? x.data.query : current_query;
			current_page_number = typeof x.data.page != 'undefined' ? parseInt(x.data.page) : current_page_number;
			if (x.data.fields)
				for (var i in x.data.fields)
					current_fields[i] = x.data.fields[i];
			cityAdmin.DB({
				query: 'get-cities',
				data: {
					query: current_query,
					fields: current_fields,
					page: current_page_number,
				},
				error: function(d) {
					if (typeof x.error === 'function')
						x.error(d);
					else
						console.error(d);
				},
				done: function(d) {
					if (current_page_number < 1)
						$('.page-menu .prev').hide();
					else
						$('.page-menu .prev').show();
					window.location.hash = (current_query ? '&query=' + current_query : '') + '&page_num=' + (current_page_number + 1);
					if (typeof x.done === 'function')
						x.done(d);
				}
			});
		},
		citiesGet: function(x) {
			if (x && x.data) {
				if (typeof x.ahtb == 'undefined' || x.ahtb == true) {
					ahtb.loading('Getting cities...', {
						height: 90,
						opened: function() {
							current_query = x.data.query ? x.data.query : current_query;
							current_page_number = typeof x.data.page != 'undefined' ? parseInt(x.data.page) : current_page_number;
							if (x.data.fields)
								for (var i in x.data.fields)
									current_fields[i] = x.data.fields[i];
							cityAdmin.DB({
								query: 'get-cities',
								data: {
									query: current_query,
									fields: current_fields,
									page: current_page_number,
								},
								error: function(d) {
									if (typeof x.error === 'function')
										x.error(d);
									else
										console.error(d);
									ahtb.close();
								},
								done: function(d) {
									if (current_page_number < 1)
										$('.page-menu .prev').hide();
									else
										$('.page-menu .prev').show();
									window.location.hash = (current_query ? '&query=' + current_query : '') + '&page_num=' + (current_page_number + 1);
									if (typeof x.done === 'function')
										x.done(d);
									ahtb.close();
								}
							});
						}
					});
				}
				else
					cityAdmin.DBNoPopup(x);
			}
		},
		citiesReceived: function(x) {
			cityAdmin.isParsing = true;
			$('.page-menu a.next:hidden').show(0.1);
			for (var i in x) cityAdmin.parseCity(x[i]);
			$('#cities tbody tr').each(function(index, ele) {
				if (index % 2 === 1) $(this).addClass('alternate');
				else $(this).removeClass('alternate');
			});
			cityAdmin.isParsing = false;
		},
		parseCity: function(x) {
			// console.log(x);
			this.citiesDB[x.id] = x;
			if (typeof x.lat != 'undefined') x.lat = parseFloat(x.lat);
			if (typeof x.lng != 'undefined') x.lng = parseFloat(x.lng);
			if (typeof x.id != 'undefined') x.id = parseInt(x.id);

			// order of tags with a score
			var tag_order = [42, 123, 163, 18, 164, 6, 66, 49, 145, 72, 143, 25, 40, 165, 167, 10, 169, 172];

			// tags without a score
			var non_score_tags = {
				27: 'small town',
				16: 'metropolitan',
				29: 'urban',
				19: 'ocean',
				1: 'alpine',
				4: 'countryside',
				31: 'wine country',
				166: 'lake',
				159: 'city outskirts',
				24: 'rural',
				104: 'airport within 15 miles',
				142: 'airport within 40 miles',
				105: 'airport within 80 miles',
				115: 'summer 71-85',
				114: 'summer 60-70',
				116: 'summer 86+',
				33: 'winter <40',
				32: 'winter 40-70',
				34: 'winter >70'
			};
			var h = '<tr class="city" city="' + x.id + '" city-name="' + (!x.city ? null : x.city) + (!x.state ? null : ', ' + x.state) + '" image="' + (!x.image ? null : x.image) + '">' +
				'<td><a href="javascript:cityAdmin.editImage(' + x.id + ');">' +
				'<img src="' + ah_local.tp + '/_img/_cities/15x15/' + (!x.image ? '_blank.jpg' : x.image) + '" />' +
				'</a></td>' +
				'<td>' + x.count + '</td>' +
				'<td>' + (!x.city ? null : x.city) + (!x.state ? null : ', ' + x.state) + '</td>';
			for (var order_index in tag_order)
				h += '<td class="tag-with-score" city-id="' + x.id + '" tag-id="' + tag_order[order_index] + '" tag-score="' + (x.tags && x.tags[tag_order[order_index]] && x.tags[tag_order[order_index]].score ? x.tags[tag_order[order_index]].score : 0) + '">' +
				'<a>' + (x.tags && x.tags[tag_order[order_index]] && x.tags[tag_order[order_index]].score ? x.tags[tag_order[order_index]].score : '-') + '</a>' +
				'</td>';
			h += '<td class="other-tags">';
			var otherTags = [];
			for (var x_tags_index in x.tags)
				if ($.inArray(parseInt(x_tags_index), tag_order) < 0) {
					otherTags.push(parseInt(x_tags_index));
					h += '<span tag-id="' + x_tags_index + '">' + x.tags[x_tags_index].tag + '</span>, ';
				}
			h += '</td>';
			h += '</tr>';
			// console.log( 'id:', x.id, 'length:',$('[city='+x.id+']').length );
			if ($('[city=' + x.id + ']').length > 0)
				$('[city=' + x.id + ']').replaceWith(h);
			else
				$('#cities tbody').append(h);
			h = null;
			$('[city=' + x.id + '] .other-tags').on('click', function() {
				var h = '';
				var a = [{
					'airport within 15 miles': 104,
					'airport within 40 miles': 142,
					'airport within 80 miles': 105
				}, {
					'summer 71-85': 115,
					'summer 60-70': 114,
					'summer 86+': 116
				}, {
					'winter <40': 33,
					'winter 40-70': 32,
					'winter >70': 34
				}, ];
				for (var ai in a) {
					h += '<p>';
					for (var aii in a[ai])
						h += '<input type="radio" name="tags_' + ai + '" value="' + a[ai][aii] + '" ' + ($.inArray(a[ai][aii], otherTags) >= 0 ? ' checked' : '') + ' />' + aii + '&nbsp;&nbsp&nbsp;&nbsp';
					h += '</p>';
				}
				a = {
					'small town': 27,
					'metropolitan': 16,
					'urban': 29,
					'ocean': 19,
					'alpine': 1,
					'countryside': 4,
					'wine country': 31,
					'lake': 166,
					'city outskirts': 159,
					'rural': 24,
				};
				h += '<ul class="other-tags">';
				for (var aj in a)
					h += '<li><input type="checkbox" name="tags" value="' + a[aj] + '" ' + ($.inArray(a[aj], otherTags) >= 0 ? ' checked' : '') + '/>' + aj + '</li>';
				h += '</ul>';
				var cityID = x.id;
				ahtb.open({
					title: 'Edit Tags for ' + x.city,
					html: h,
					width: 650,
					height: 275,
					buttons: [{
						text: 'Save',
						action: function() {
							var checked = [];
							$('#tb-submit input:checked').each(function() {
								checked.push(parseInt($(this).val()));
							});
							var toAdd = [],
								toRemove = [];
							for (var oTi in otherTags)
								if ($.inArray(otherTags[oTi], checked) < 0) toRemove.push({
									city_id: x.id,
									tag_id: otherTags[oTi]
								});
							for (var ci in checked)
								if ($.inArray(checked[ci], otherTags) < 0) toAdd.push({
									city_id: x.id,
									tag_id: checked[ci],
									score: -1
								});
							ahtb.open({
								html: '<p>Saving...</p>',
								height: 85,
								width: 200,
								buttons: [],
								opened: function() {
									cityAdmin.DB({
										query: 'edit-city-tags',
										data: {
											add: toAdd,
											'delete': toRemove
										},
										done: function(d) {
											var c = cityAdmin.citiesDB[x.id];
											for (var tRi in toRemove) delete c.tags[toRemove[tRi].tag_id];
											for (var tAi in toAdd) {
												if ( typeof non_score_tags[toAdd[tAi].tag_id] == 'undefined') {
													console.log("non_score_tags[toAdd[tAi].tag_id] is undefined.");
													for(var i in d.addTags) {
														if (d.addTags[i].tag_id == toAdd[tAi].tag_id) {
															console.log("adding to non_score_tags - id:"+toAdd[tAi].tag_id+", tag:"+d.addTags[i].tag);
															non_score_tags[toAdd[tAi].tag_id] = d.addTags[i].tag;
															break;
														}
													}
												}
												if( typeof c.tags == 'undefined') {
													console.log("tags array for "+c.city+" is undefined, creating one");
													c.tags = [];
												}
												c.tags[toAdd[tAi].tag_id] = {
													score: -1,
													tag: non_score_tags[toAdd[tAi].tag_id]
												};
											}
											cityAdmin.parseCity(c);
											ahtb.close();
										},
										error: function(d) {
											var msg = 'Something failed...';
											if (typeof d == 'string')
												msg = d;
											ahtb.alert(msg, {height: 150 });
										}
									});
								}
							});
						}
					}, {
						text: 'Cancel',
						action: function() {
							ahtb.close();
						}
					}],
				});
			});
			$('[city=' + x.id + '] td.tag-with-score a').each(function(index, element) {
				$(this).on('click', function() {
					var tag_id = parseInt($(this).parent().attr('tag-id')),
						city_id = parseInt($(this).parent().attr('city-id')),
						score = parseInt($(this).parent().attr('tag-score'));
					console.log("edit tag:"+tag_id+", city:"+city_id+", score:"+score);
					ahtb.open({
						title: 'Edit ' + $('th:nth-child(' + (index + 4) + ')').html() + ' score for ' + x.city + ', ' + x.state,
						html: '<p class="value">New Score: <span>' + score + '</span></p><div class="slider"></div>',
						height: 175,
						width: 450,
						opened: function() {
							$('#tb-submit .slider').slider({
								value: score,
								min: 0,
								max: 10,
								slide: function(event, ui) {
									$('#tb-submit .value span').html(ui.value);
								}
							});
						},
						buttons: [{
							text: 'Save',
							action: function() {
								var newScore = parseInt($('#tb-submit .value span').html());
								console.log("saving tag:"+tag_id+", city:"+city_id+", score:"+newScore);
								ahtb.loading('Saving...', {
									opened: function() {
										if (newScore === score) ahtb.close();
										else {
											cityAdmin.DB({
												query: 'update-city-tag',
												data: {
													city_id: city_id,
													tag_id: tag_id,
													score: newScore
												},
												done: function(d) {
													console.log("returned from saving tag:"+tag_id+", city:"+city_id+", score:"+newScore);
													if (typeof cityAdmin.citiesDB[city_id] == 'undefined') {
														ahtb.alert("saving tag has city_id:"+city_id+" as undefined");
														return;
													}

													if (typeof cityAdmin.citiesDB[city_id].tags == 'undefined') {// create new one
														console.log("create new tags array for city:"+city_id);
														cityAdmin.citiesDB[city_id].tags = [];
													}

													if (typeof cityAdmin.citiesDB[city_id].tags[tag_id] == 'undefined') {
														if ( typeof non_score_tags[tag_id] == 'undefined') {
															var msg = "undefined non_score_tags["+tag_id+"]";
															if (d) {
																msg += ", setting it to "+d.tag;
																non_score_tags[tag_id] = d.tag;
															}
															console.log(msg)
														}
														cityAdmin.citiesDB[city_id].tags[tag_id] = {
															tag: non_score_tags[tag_id],
															score: newScore
														};
													}
													else
														cityAdmin.citiesDB[city_id].tags[tag_id].score = newScore;
													cityAdmin.parseCity(cityAdmin.citiesDB[city_id]);
													ahtb.close();
												},
												error: function(d) {
													var msg = 'Something failed...';
													if (typeof d == 'string')
														msg = d;
													ahtb.alert(msg, {height: 150 });
												}
											});
										}
									}
								});
							}
						}, {
							text: 'Cancel',
							action: function() {
								ahtb.close();
							}
						}],
					});
				});
			});
		},
		editImage: function(cityID) {
			oldImg = $('[city=' + cityID + ']').attr('image');
			if (oldImg == 'null') oldImg = null;

			var x = null;
			x = (oldImg === null) ? {
				height: 250,
				html: '<p>Loading..</p>',
				opened: function() {
					$('#tb-submit').children('p').remove();
					$('#tb-submit').prepend('<p>Preferred minimum: 300x300px</p><form id="dropzone" class="dropzone"></form>');
					var dz = new Dropzone("#dropzone", {
						acceptedFiles: 'image/*',
						url: ah_local.tp + "/_classes/Image.class.php",
						headers: {
							'image-dir': "_cities/uploaded/"
						},
						maxFiles: 1,
						accept: function(file, done) {
							ahtb.resize(400, 375);
							done();
						}
					});
					dz.on("success", function(file, responseText) {
						x = $.parseJSON(responseText);
						if (x.status == 'OK') {
							ahtb.open({
								width: 200,
								height: 85,
								html: '<p>Saving...</p>',
								opened: function() {
									cityAdmin.DB({
										query: 'update-city',
										data: {
											id: cityID,
											fields: {
												image: x.data[0]
											}
										},
										done: function() {
											$('[city="' + cityID + '"]').attr('image', x.data[0]);
											$('[city="' + cityID + '"] img').attr('src', ah_local.tp + '/_img/_cities/15x15/' + x.data[0]);
											cityAdmin.editImage(cityID);
										}
									});
								}
							});
						} else console.log(x);
					});
					dz.on('maxfilesexceeded', function() {
						ahtb.alert('Please upload only one file at once.', {
							closed: function() {
								cityAdmin.editImage(cityID);
							}
						});
					});
				}
			} : {
				height: 400,
				html: '<img src="' + ah_local.tp + '/_img/_cities/300x300/' + oldImg + '" />',
				buttons: [{
					text: 'Delete Image',
					action: function() {
						ahtb.open({
							height: 85,
							width: 200,
							html: '<p>Deleting...</p>',
							opened: function() {
								cityAdmin.DB({
									query: 'update-city',
									data: {
										id: cityID,
										fields: {
											image: 'NULL'
										}
									},
									done: function(xx) {
										$('[city="' + cityID + '"]').attr('image', 'null');
										$('[city="' + cityID + '"] img').attr('src', ah_local.tp + '/_img/_cities/15x15/_blank.jpg');
										cityAdmin.editImage(cityID);
									}
								});
							}
						});
					}
				}, {
					text: 'Cancel',
					action: function() {
						ahtb.close();
					}
				}],
			};

			ahtb.open({
				title: 'Image for ' + $('[city=' + cityID + ']').attr('city-name'),
				width: 400,
				height: x.height,
				buttons: (x.buttons ? x.buttons : []),
				html: x.html,
				open: function() {
					if (typeof x.open !== 'undefined') x.open();
				},
				opened: function() {
					if (typeof x.opened !== 'undefined') x.opened();
				}
			});
		},
		DB: function(xx) {
			if (xx) {
				if (!xx.query || xx.query === '') console.log('DB: invalid request');
				else {
					cityAdmin.currentQuery = $.ajax({
						url: ah_local.tp + '/_admin/ajax.php',
						cache: false,
						dataType: "json",
						data: {
							query: xx.query,
							data: xx.data
						},
						type: "POST"
					}).done(function(x) {
						if (x.status == 'OK' && typeof xx.done === 'function') xx.done(x.data);
						else if (x.status != 'OK' && typeof xx.error === 'function') xx.error(x.data);
						else console.log(x);
						cityAdmin.currentQuery = null;
					});
				}
			}
		}
	};
	cityAdmin.init();
});