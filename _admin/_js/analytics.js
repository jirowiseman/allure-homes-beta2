if (typeof deleteCookie != 'function')
	window.deleteCookie = function (what) {
		var name = what + "=";
		document.cookie = name+"; expires=Thu, 01 Jan 1970 00:00:01 GMT"+ "; path=/";
	}

if (typeof setCookie != 'function')
	window.setCookie = function stCookie(cname, cvalue, exdays) {
	    var d = new Date();
	    d.setTime(d.getTime() + (exdays*24*60*60*1000));
	    var expires = "expires="+d.toUTCString();
	    deleteCookie(cname);
	    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
	}

if (typeof getCookie != 'function')
	window.getCookie = function (cname) {
	    var name = cname + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0; i<ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1);
	        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
	    }
	    return "";
	}

if (typeof checkCookie != 'function')
	window.checkCookie = function (what) {
	    var user = getCookie(what);
	    if (user != "") {
	        return true;
	    } else {
	        return false;
	    }
	}

if (typeof length != 'function')
	window.length = function (item) {
		if( Array.isArray(item) ) 
			return item.length;

		if (typeof item != 'object' || item == null)
			return item != null ? 1 : 0;

			// Detecting IE
	    var oldIE;
	    if ($('section.ie-identifier').is('.ie6, .ie7, .ie8')) {
	        oldIE = true;
	    }

	    if (oldIE) {
	        // Here's your JS for IE..
	        var count = 0;
			for (var i in item) {
			    if (item.hasOwnProperty(i)) 
			        count++;
			    }
			return count;
	    } else {
	        // ..And here's the full-fat code for everyone else
	        return Object.keys( item ).length;
	    }			
	}

if ( typeof getTime != 'function')
	window.getTime = function(date) {
		var currentdate = typeof date == 'undefined' ? new Date() : date; 
		// var datetime = (currentdate.getMonth()+1)  + "/" 
	 //                + currentdate.getDate() + "/"
	 //                + currentdate.getFullYear() + " "  
	 //                + currentdate.getHours() + ":"  
	 //                + currentdate.getMinutes(); // + ":";
	                // + currentdate.getSeconds();

	    var datetime = currentdate.getFullYear() + "-" 
	    			+ (currentdate.getMonth()+1)  + "-" 
	    			+ currentdate.getDate() + " "
	    			+ currentdate.getHours() + ":"  
	    			+ currentdate.getMinutes()+ ":"
	                + currentdate.getSeconds();
	    return datetime;
	}

jQuery(document).ready(function($){
	
	function exportDropToggle() {
        $('#tableDiv #export .export-drop').slideToggle();
        if ($('#tableDiv #export .export span').hasClass('entypo-down-open-big')) {
          $('#tableDiv #export .export span').removeClass('entypo-down-open-big');
          $('#tableDiv #export .export span').addClass('entypo-up-open-big');
        }
        else {
          $('#tableDiv #export .export span').removeClass('entypo-up-open-big');
          $('#tableDiv #export .export span').addClass('entypo-down-open-big');
        }
    }

	analytics = new function() {

		this.tasks = [];
		this.dbBusy = false;
		this.lastQuery = '';
		this.lastQueryResult = null;
		this.cachingVisitAllPages = 0;

		this.init = function() {
			var options = '';
			for(var i in eventTypes)
				options += "<option value='"+i+"'>"+eventTypes[i]+'</option>';
			$('#events').html(options);

			options = '';
			for(var i in originTypes)
				options += "<option value='"+originTypes[i]+"'>"+originTypes[i]+'</option>';
			$('#origins').html(options);

			options = '';
			for(var i in whatTypes)
				options += "<option value='"+whatTypes[i]+"'>"+whatTypes[i]+'</option>';
			$('#whats').html(options);

	        var isIE = browser.shortname.indexOf('MSIE') != -1 && document.body.createControlRange;
            var isEdge =  browser.shortname == 'Edge';
            var isFirefox = browser.shortname == 'Firefox';
            var h = '';
            if (!isFirefox && !isEdge && !isIE) {
				h+= '<div id="export" class="hidden">'+
						'<a href="javascript:;" class="export">Export <span class="entypo-down-open-big"></span></a>' +
						'<div class="export-drop">' +
							'<a id="print" href="javascript:analytics.print();">Print</a>' +
							'<a id="print" href="javascript:analytics.print(true);">Save As CSV</a>' +
						'</div>'+
					'</div>';
				$('#tableDiv table').before(h);                
			}

			this.setHandlers();

			for(var i in visitPages) 
				this[visitPages[i]] = null;
		}

		this.setHandlers = function() {
			$('#customSQLs button').on('click', function() {
				var id = $(this).attr('id');
				if (analytics[id] == null)
					analytics.DB({query:id,
								  data: {
								  	from: $('#date_from').val(),
							  		to: $('#date_to').val()
								  },
							  	  done: analytics.display
							  	});
				else {
					analytics.lastQuery = id;
					analytics.display(analytics[id]);
				}
			})

			$('#tableDiv #export a.export').on('click', function() {
                exportDropToggle();
            });
            $('#tableDiv #export .export-drop a').on('click', function() {
                exportDropToggle();
            });

            $('#date_from').appendDtpicker({
				"onShow": function(handler){
					// window.alert('Picker is shown!');
				},
				"onHide": function(handler){
					// window.alert('Picker is hidden!');
				}
			});
			var date = new Date();
			var dst = (isDst ? (3600*1000) : 0);
			var lastMonth = new Date(date.getTime() - (1000 * 60 * 60 * 24 * 30) - dst);
			var yesterday = new Date(date.getTime() - (1000 * 60 * 60 * 24 * 1));
			$('#date_from').handleDtpicker('setDate', yesterday);
			$('#date_from').val( getTime(yesterday) );

			console.log("lastMonth:"+lastMonth);
			console.log("yesterday:"+yesterday);

			$('#date_to').appendDtpicker({
				"onShow": function(handler){
					// window.alert('Picker is shown!');
				},
				"onHide": function(handler){
					// window.alert('Picker is hidden!');
				}
			});

			$('button#resetToNow').on('click', function() {
				var now = new Date();
				$('#date_to').handleDtpicker('setDate', now);
				$('#date_to').val( getTime(now) );
			})
		}

		this.submitRequest = function() {			
			var ele = jQuery('#date_from');
			var t1 = ele.val();
			var t2 = $('#date_to').val();
			var events = '';
			$('#events option:selected').each(function() {
				events += (events.length ? ',' : '') + $(this).attr('value');
			})
			var origins = '';
			$('#origins option:selected').each(function() {
				origins += (origins.length ? ',' : '') + $(this).attr('value');
			})
			var whats = '';
			$('#whats option:selected').each(function() {
				whats += (whats.length ? ',' : '') + $(this).attr('value');
			})
			var retrieve = '';
			$('#getWhat').find('input[type=checkbox]').each(function() {
				if ($(this).prop('checked'))
					retrieve += (retrieve.length ? ',' : '') + $(this).attr('id');

			})
			ahtb.alert("From:"+t1+", To:"+t2+" for events:"+events+", origins:"+origins+", whats:"+whats+", retrieve:"+retrieve);

			analytics.DB({query: 'general-SQL',
						  data: {events: events,
						  		 origins: origins,
						  		 whats: whats,
						  		 retrieve: retrieve,
						  		 from: $('#date_from').val(),
						  		 to: $('#date_to').val()
						  		},
						  done: analytics.display,
						 });
		}

		this.print = function(asCsv) {
			var asCsv = typeof asCsv == 'undefined' ? false : asCsv;
			if (!asCsv) {
		        window.setTimeout(function() {
		        	var originalContents = document.body.innerHTML;
			        var data = 	'<section id="selections">'+
			        				'<div class="tableDiv">'+
			        					'<table>'+
			        						'<thead>';
			        			data += analytics.getTableHeader();
									data += '</thead>'+
											'<tbody>';
								data += analytics.getTableBody();
									data += '</tbody>'+
										'</table>'+
			        				'</div>'+
			        			'</section>';
		        	var currentBody =  $('html')[0].outerHTML;
				    document.body.innerHTML = data;
				    window.print();
	                setTimeout(statsEvents, 100);
	                function statsEvents() {
	                  //document.body.innerHTML = originalContents;
	                  document.body.innerHTML = currentBody;
	                  analytics.setHandlers();
	                }
		        }, 500);
			}
			else {
				var csv = analytics.getTableHeader(true)+analytics.getTableBody(true);
				saveAs( new Blob([csv], 
				  				 {type: "plain/text;charset=" + document.characterSet} ),
				  				 analytics.lastQuery+".csv");	
			}

	        return true;
		}

		this.getTableHeader = function(asCsv) {
			if (!analytics.lastQueryResult)
				return '';

			var head = '<th><span>index</span></th>';
			var csv = 'index';
			for(var i in analytics.lastQueryResult.keys) {
				head += '<th><span>'+analytics.lastQueryResult.keys[i]+'</span></th>';
				csv += ','+analytics.lastQueryResult.keys[i]
			}
			csv += '\n\r';

			return typeof asCsv != 'undefined' && asCsv ? csv : head;
		}

		this.getTableBody = function(asCsv) {
			if (!analytics.lastQueryResult)
				return '';

			var body = '';
			var row = 0;
			var csv = '';
			for(var j in analytics.lastQueryResult.results) {
				var index = row+1;
				body += '<tr><td'+((row%2) ? ' class="darken"' : '')+'><span>'+index+'</span></td>';
				csv += index.toString();
				for(var i in analytics.lastQueryResult.keys) {
					body += '<td'+((row%2) ? ' class="darken"' : '')+'><span>'+analytics.lastQueryResult.results[j][analytics.lastQueryResult.keys[i]]+'</span></td>';
					csv += ','+analytics.lastQueryResult.results[j][analytics.lastQueryResult.keys[i]];
				}
				body += '</tr>';
				csv += '\n\r';
				row++;
			}

			return typeof asCsv != 'undefined' && asCsv ? csv : body;
		}

		this.display = function(d) {
			analytics.lastQueryResult = null;
			if (typeof d != 'undefined') {
				// console.log( JSON.stringify(d));
				analytics.lastQueryResult = d;
				$('#tableDiv table thead').empty();
				$('#tableDiv table tbody').empty();
				$('#tableDiv table thead').html(analytics.getTableHeader());
				$('#tableDiv table tbody').html(analytics.getTableBody());

				switch(analytics.lastQuery) {
					case 'visitAllSql':
					case 'visitAllSqlWithSessionId':
						$('#visitPages').show();
						for(var i in visitPages)
							if (visitPages[i] != 'pageInitialEntrySql')
								$('#visitPages #'+visitPages[i]).removeClass('hidden');

						analytics.visitAll = d;
						analytics.cacheVisitAllPages();
						break;

					case 'pageInitialEntrySql':
						$('#visitPages').show();
						$('#visitPages #pageInitialEntrySql').removeClass('hidden');
						analytics.initialEntry = d;
						analytics['pageInitialEntrySql'] = d;
						break;

					case 'portalUsage':
						$('#visitPages').show();
						$('#visitPages #portalUsage').removeClass('hidden');
						analytics.initialEntry = d;
						analytics['portalUsage'] = d;
						break;
				}
				 $('#tableDiv #export').removeClass('hidden');
			}
		}

		this.cacheIt = function(d, what) {
			analytics[what] = d;
			analytics.cachingVisitAllPages--;
		}

		this.cacheVisitAllPages = function() {
			for(var i in visitPages)
				if (visitPages[i] != 'initialEntry') {
					analytics.cachingVisitAllPages++;
					analytics.DB({query: visitPages[i],
								  data: {
								  	from: $('#date_from').val(),
							  		to: $('#date_to').val()
								  },
							  	  done: analytics.cacheIt
							  	})
				}
		}

		this.DB = function(xx){
			if (analytics.dbBusy) {
				analytics.tasks.push(xx);			
				return;
			}
			analytics.reallyDB(xx);
		}
		this.reallyDB = function(xx) {
			if (xx != null){
				if (xx.data == null) xx.data = {};
				// xx.data.sessionID = ah_local.sessionID;
				analytics.dbBusy++;
				if (!analytics.cachingVisitAllPages)
					analytics.lastQuery = xx.query;
				$('div#overlay-bg').show();
				var call = $.ajax({
					type: "POST",
					dataType: "json",
					url: ah_local.tp+'/_admin/ajax_analytics.php',
					data: { query: xx.query, data: xx.data },
					async: true,
					error: function($xhr, $status, $error) {
						var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
						analytics.dbBusy--;
						ahtb.alert("Failed:"+xx.query+", msg: "+msg);
			    		console.log("Got error back for run-quiz: "+msg);
			    		$('div#overlay-bg').hide();
					},
					success: function(d) {
						analytics.dbBusy--;
						$('div#overlay-bg').hide();
						console.log("returned from "+xx.query);
				        if (d && d.status != 'OK') xx.error == null ? ahtb.alert('There was a problem.<br/><small>'+d.data+'</small>') : xx.error(d.data);
				        else if (d && xx.done != null) xx.done(d.data, xx.query);
				        else console.log(d);
				        if (analytics.tasks.length) {
				        	console.log("Analytics has "+analytics.tasks.length+" tasks left to do.");
				        	analytics.DB(analytics.tasks.shift());
				        }
					}
				});
				console.log("Made ajax call to "+xx.query);
			}
		}

	}

	
	analytics.init();
})
