var quizAdmin;
jQuery(document).ready(function($){
quizAdmin = new function(){
	this.ajaxURL = ah_local.tp+"/_admin/ajax.php";
	this.init = function(){
		ahtb.loading('Getting quiz questions from database...',
			{ height: 50, opened: function(){ quizAdmin.DB({ query: 'get-quiz-questions', done: function(d){ 
				quizAdmin.parse('quiz-questions',d); 
				ahtb.close(); 
				for (var i in quizAdmin.order) quizAdmin.print('quiz-question', quizAdmin.qDB[quizAdmin.order[i]]);
				$('#questions tr:nth-child(2n)').addClass('alternate');
				if ($('#questions tr:nth-child(2n-1)').hasClass('alternate')) $('#questions tr:nth-child(2n-1)').removeClass('alternate');

				$('.add-blank-question').on('click',function(){
					ahtb.loading('Creating blank question...',{opened:function(){
						quizAdmin.DB({ query: 'add-question', done: function(d){location.reload()}})
					}});
				});

				$('#questions > tbody').sortable({update: function(event, ui) {
					$('#questions tr:nth-child(2n)').addClass('alternate');
					if ($('#questions tr:nth-child(2n-1)').hasClass('alternate')) $('#questions tr:nth-child(2n-1)').removeClass('alternate');
					var x = [];
					$(this).children().each(function(index){ if (quizAdmin.qDB[$(this).attr('data-qid')].orderby != index) x.push( {question_id: $(this).attr('data-qid'), fields: {orderby: index}} ) });
					ahtb.loading('Saving...',{opened:function(){ quizAdmin.updateQuestions(x); }});
			  }});
			}}); }
		});
	}
	this.edit = function(field, question_id, slide_id){
		question = this.qDB[question_id];
		if (field) switch (field){
			case 'description': case 'prompt':
				ahtb.open({
					title: 'Edit '+field, width: 200, height: 110,
					html: '<p><input type="text" name="'+field+'" id="tb-input" value="'+(question[field] == null ? '' : question[field])+'" /></p>',
					buttons: [{text:'Save', action:function(){
						var val = $.trim($('#tb-submit input').val());
						ahtb.edit({html: '<p>Saving '+field+'...</p>', height: 75, hideClose: true, opened: function(){
							val = val.length < 1 ? null : val;
							if ( question[field] == val ) ahtb.close();
							else { 
								var x = {}; x[field] = val;
								$('tr[data-qid='+question.id+']').html('<td colspan="4">Saving...</td>');
								quizAdmin.updateQuestion(question.id, x);
								quizAdmin.qDB[question_id][field] = val;
								quizAdmin.print('quiz-question', quizAdmin.qDB[question.id]);
							}
						}});
					}}]
				});
				break;
			case 'slide':
				var slide = null;
				for (var i in question.slides) if (question.slides[i].id == slide_id){ slide = question.slides[i]; break; }
				if (!slide) console.error('no slide found');
				else {
					var formattedTags = '';
					for (var j in slide.tags)
						formattedTags+= '<li><a href="javascript:;" tag-id="'+slide.tags[j].id+'" tag-name="'+slide.tags[j].tag+'" slide-id="'+slide.id+'">'+slide.tags[j].tag+'</a></li>';
					ahtb.open({
						title: 'Edit Slide',
						height: 325, width: 600,
						html: '<div style="clear:both;overflow:auto;margin:1em 0 0;"><div style="float:left;clear:left;width: 45%;">'+
							'<label for="title">Title:</label><br/><input type="text" id="edit-slide-title" name="title" value="'+(slide.title==null ? '' : slide.title)+'" />'+
							'<label for="tags">Tags: <small>(click to remove)</small></label><br/><ul id="slide-tags">'+formattedTags+'</ul></p><p><a class="add-tag" question-id="'+question.id+'" slide-id="'+slide.id+'" href="javascript:;">(add tag)</a>'+
						'</div><div style="float:right;clear:right;">'+
							'<img src="'+ah_local.tp+'/_img/_quiz/280x175/'+(slide.file == null ? '_blank.jpg' : slide.file)+'" style="text-align:center;" /><br/>'+
							'<a class="upload-image" href="javascript:;" slide-id="'+slide.id+'">Upload New Image</a> | '+
							'<a class="delete-image" href="javascript:;" slide-id="'+slide.id+'">Delete Image</a>'+
						'</div></div>',
						buttons: [
							{text: 'Save', action:function(){ 
								quizAdmin.updateSlides([{ slide_id: slide_id, fields: {title:$.trim($('#edit-slide-title').val())} }]);
								for (var i in quizAdmin.qDB[question_id].slides) if (quizAdmin.qDB[question_id].slides[i].id == slide_id){
									quizAdmin.qDB[question_id].slides[i].title = $.trim($('#edit-slide-title').val());
									break;
								}
								quizAdmin.print('quiz-question', quizAdmin.qDB[question_id]);
								ahtb.close();
							}},
							{text:'Delete Slide', action:function(){ ahtb.open({
								height: 125, width: 300,
								title: 'Delete slide?',
								html: '<p>Are you sure you want to delete this slide?</p>',
								buttons: [
									{text: 'Delete Slide', action:function(){
										quizAdmin.DB({ query: 'delete-slide', data: {question_id: question_id, slide_id: slide_id }, done: function(d){ 
											quizAdmin.parse('quiz-questions',d);
											quizAdmin.print('quiz-question', quizAdmin.qDB[question_id]);
											ahtb.close();
										}});
									}},
									{text: 'Cancel', action:function(){quizAdmin.edit('slide', question_id, slide_id)}}
								]
								}); }}
						],
						opened: function(){
							$('#tb-submit .upload-image').on('click',function(){
								ahtb.open({
									height: 350, width: 500, title: 'Upload New Image', html: ' ', 
									buttons: [{text:'Cancel',action:function(){quizAdmin.edit('slide', question_id, slide_id)}}],
									opened: function(){
										$('#tb-submit').replaceWith($('<div id="tb-submit"><form id="dropzone" class="dropzone" style="display:block;margin:1em 0 .5em;width:500px;height:250px;"></form>'+$('#tb-submit').html()+'</div>'));
										var dz = new Dropzone("#dropzone", {
											acceptedFiles:'image/*', url:ah_local.tp+"/_classes/Image.class.php",
											headers:{"image-dir":"_quiz/uploaded/","gen_sizes":1}, maxFiles: 1
										}).on("success", function(file, responseText){
											var d = $.parseJSON(responseText);
											if (d.status == 'OK'){
												ahtb.loading('Saving image...',{opened:function(){
													quizAdmin.updateSlides([{ slide_id: slide_id, fields: {file:d.data[0]} }]);
													for (var i in quizAdmin.qDB[question_id].slides) if (quizAdmin.qDB[question_id].slides[i].id == slide_id){
														quizAdmin.qDB[question_id].slides[i].file = d.data[0];
														break;
													}
													quizAdmin.print('quiz-question', quizAdmin.qDB[question_id]);
													quizAdmin.edit('slide', question_id, slide_id);
												}});
											} else console.log(d);
										});
									}
								});
							});
							$('#tb-submit .delete-image').on('click',function(){
								ahtb.open({
									height: 125, width: 300, title: 'Delete Image?', html: '<p>Are you sure you want to delete this image?</p>', buttons:[
										{text:'Delete Image',action:function(){ 
											ahtb.loading('Deleting image...',{opened:function(){
												quizAdmin.updateSlides([{ slide_id: slide_id, fields: {file:''} }]);
												for (var i in quizAdmin.qDB[question_id].slides) if (quizAdmin.qDB[question_id].slides[i].id == slide_id){
													quizAdmin.qDB[question_id].slides[i].file = null;
													break;
												}
												quizAdmin.print('quiz-question', quizAdmin.qDB[question_id]);
												quizAdmin.edit('slide', question_id, slide_id);
											}});
										}},
										{text: 'Cancel', action:function(){quizAdmin.edit('slide', question_id, slide_id)}}
									]
								});
							});
							$('#tb-submit .add-tag').on('click', function(){ quizAdmin.edit('add-tag', question_id, slide_id) });
							$('#tb-submit #slide-tags a').on('click', function(){
								var tag_name = $(this).attr('tag-name');
								var tag_id = $(this).attr('tag-id');
								ahtb.open({
									height: 125, width: 500,
									title: 'Delete tag?',
									html: '<p>Are you sure you want to remove tag "'+tag_name+'" from this slide?</p>',
									buttons: [
										{text: 'Remove Tag', action:function(){
											quizAdmin.DB({ query: 'remove-tag-from-slide', data: {question_id: question_id, slide_id: slide_id, tag_id: tag_id }, done: function(d){ 
												quizAdmin.parse('quiz-questions',d);
												quizAdmin.edit('slide', question_id, slide_id);
											}});
										}},
										{text: 'Cancel', action:function(){quizAdmin.edit('slide', question_id, slide_id)}}
									]
								});
							})
						}
					});
				}
				break;
			case 'add-tag':
				ahtb.edit({
					title: 'Add Tag', height: 120, width: 250,
					html: '<div style="margin-top:1em"><input type="text" style="position: relative" id="add-tag-input" value="" autocomplete="on" /><ul class="menu"></ul>',
					buttons: [
						{text: 'Add', action: function(){
								var val = $('#add-tag-input').val();
								var found = false;
								for (var i in tag_database) if (val == tag_database[i].tag){found = tag_database[i]; break;}
								if (found === false) // make sure tag exists in DB
									ahtb.edit({ height: 120, width: 300, html: '<p>Invalid tag, please choose one from the list.</p>', buttons: [{text:'OK',action:function(){ quizAdmin.edit('add-tag', question_id, slide_id); }}] });
								else for (var i in quizAdmin.qDB[question_id].slides) if (quizAdmin.qDB[question_id].slides[i].id == slide_id){ // search the slides
									var tags = quizAdmin.qDB[question_id].slides[i].tags;
									var hasTag = false;
									for (var j in tags) if (found.tag == tags[j]){ hasTag = true; break; } 
									if (hasTag) // tag already in slide
										ahtb.edit({ height: 120, width: 400, html: '<p>Tag "'+found.tag+'" already used in this slide.</p>', buttons: [{text:'OK',action:function(){ quizAdmin.edit('add-tag', question_id, slide_id); }}] }); 
									else // add the tag to the slide
										quizAdmin.DB({ query: 'add-tag-to-slide', data: {question_id: question_id, slide_id: slide_id, tag_id: found.id }, done: function(d){ 
											quizAdmin.parse('quiz-questions',d);
											quizAdmin.print('quiz-question', quizAdmin.qDB[question_id]);
											quizAdmin.edit('slide', question_id, slide_id);
										}});
									break;
								}
							}
						},
						{text: 'Back', action:function(){quizAdmin.edit('slide', question_id, slide_id)} }
					],
					opened: function(){
						var tags = [];
						for (var i in tag_database) tags.push(tag_database[i].tag);
						$('#add-tag-input').autocomplete({appendTo: '#tb-submit .menu', source: tags });
					}
				});
				break;
		}
	}
	this.parse = function(type, data){
		if (type) switch(type){
			case 'quiz-questions':
				if (data){
					// console.log();
					if (quizAdmin.qDB == null) { quizAdmin.qDB = {}; }
					if (quizAdmin.order == null) { quizAdmin.order = {}; }
					var toUpdate = {questions:[], slides:[]};
					for (var i in data){
						var ints = ['type','enabled','multi','multi_num','weight','quiz','section','orderby'];
						var strings = ['description','prompt'];
						for (var j in ints) if (data[i][ints[j]] !== null) data[i][ints[j]] = parseInt(data[i][ints[j]]);
						for (var j in strings) if (data[i][strings[j]] !== null) {
							data[i][strings[j]] = data[i][strings[j]].trim();
							if (data[i][strings[j]] < 1) data[i][strings[j]] = null;
						}
						if (data[i].orderby === null || isNaN(data[i].orderby)){
							var o = 0;
							for (var j in data) if (data[j].orderby >= o) o = parseInt(data[j].orderby)+1;
							data[i].orderby = o;

							toUpdate.questions.push({ question_id: data[i].id, fields:{orderby: o} });
						}

						var x = [];
						if (data[i].slides) for (var j = 0; j < data[i].slides.length; j++) for (var k in data[i].slides){
							if (!data[i].slides[k].file || !data[i].slides[k].file.length) data[i].slides[k].file = null;
							if (data[i].slides[k].orderby === null || isNaN(data[i].slides[k].orderby)){
								var o = 0;
								for (var l in data[i].slides)
									if (data[i].slides[l].orderby >= o) o = parseInt(data[i].slides[l].orderby)+1;
								toUpdate.slides.push({ slide_id: data[i].slides[k].id, fields: {orderby: isNaN(o) ? data[i].slides.length : o} });
							}
							else if (data[i].slides[k].orderby > data[i].slides.length-1)
								toUpdate.slides.push({ slide_id: data[i].slides[k].id, fields: {orderby: data[i].slides.length-1} });
							if (data[i].slides[k].orderby == j) x[j] = data[i].slides[k];
						}

						data[i].slides = x;

						quizAdmin.qDB[data[i].id] = data[i];
						quizAdmin.order[data[i].orderby] = data[i].id;
					}
					if (toUpdate.slides.length > 0) quizAdmin.updateSlides(toUpdate.slides);
					if (toUpdate.questions.length > 0) quizAdmin.updateQuestions(toUpdate.questions);
					// console.log(toUpdate);
					for (var j in quizAdmin.qDB){
						var x = [];
						for (var k = 0; k <= quizAdmin.qDB[j].slides.length; k++)
							if (quizAdmin.qDB[j].slides[k] && quizAdmin.qDB[j].slides[k].orderby == k) 
								x[k] = quizAdmin.qDB[j].slides[k];
						quizAdmin.qDB[j].slides = x;
					}
					$('#questions tr:nth-child(2n)').addClass('alternate');
					if ($('#questions tr:nth-child(2n-1)').hasClass('alternate')) $('#questions tr:nth-child(2n-1)').removeClass('alternate');
				} else console.log('quizAdmin.parse - no questions recieved');
				break;
			default: console.error('unable to parse: '+type); break;
		}
	}
	this.print = function(type, data){
		if (type) switch(type){
			case 'quiz-question':
				if (data){
					var q = data;
					var h = 
						'<td class="column-enabled">'+
							'<input data-qid="'+q.id+'" data-field="enabled" class="question-enabled" type="checkbox"'+( (q.enabled & 1) ? ' checked' : null)+' />'+
							'<a href="javascript:;">(delete)</a>'+
						'</td>'+
						'<td class="column-predefined">'+
							// consider that this may not have to be radio button, but also a checkbox if we allow multiple predefined questions to exist.
							'<input data-qid="'+q.id+'" data-field="enabled" class="question-predefined" type="radio"'+( (q.enabled & 2) ? ' checked' : null)+' />'+
						'</td>'+
						'<td class="column-slides"><ul class="slide-list" data-qid="'+q.id+'">';
						if (q.slides == null || q.slides.length < 1) h+= '(none)';
						else {
							if (q.type == 3) q.slides.reverse();
							for (var j = 0; j < q.slides.length; j++){
								var slide = null;
								for (var k in q.slides) if (q.slides[k].orderby == j) { slide = q.slides[k]; break; }
								if (slide != null){
									var tags_print = 'Tags: '; for (var k in slide.tags) tags_print+= slide.tags[k].tag+', ';
									h+= '<li class="slide-image" question-id="'+q.id+'" slide-id="'+slide.id+'" slide-orderby="'+slide.orderby+'">'+
										'<a href="javascript:;" title="'+tags_print+'">'+
										'<img src="'+ah_local.tp+'/_img/_quiz/25x50/'+(slide.file == null ? '_blank.jpg' : slide.file)+'" />'+
										'</a>'+
									'</li>';
								}
							}	
						}			
						h+= '</ul></td>'+
						'<td class="column-add-slide"><a href="javascript:;">(add new)</a><br/># of slides: '+ (q.slides == null ? 0 : q.slides.length)+
							'<select class="question-type" data-field="type" data-qid="'+q.id+'">'+
								'<option value="null" disabled>(type)</option>'+
								'<option value="12"'+(q.type == 12 ? ' selected' : '')+'>12</option>'+
								'<option value="6"'+(q.type == 6 ? ' selected': '')+'>6</option>'+
								'<option value="3"'+(q.type == 3 ? ' selected': '')+'>3</option>'+
							'</select>'+
						'</td>'+
						'<td class="column-prompt">'+
							'<span class="prompt"><a class="edit-prompt" data-qid="'+q.id+'" href="javascript:quizAdmin.edit('+"'prompt'"+', '+q.id+');">'+(q.prompt == null ? '(edit prompt)' : q.prompt)+'</a></span><br/>'+
							'<span class="description"><a class="edit-description" data-qid="'+q.id+'" href="javascript:quizAdmin.edit('+"'description'"+', '+q.id+');">'+(q.description == null ? '(edit description)' : q.description)+'</a></span>'+
						'</td>'+
						'<td class="column-multi"><input data-qid="'+q.id+'" data-field="multi" class="question-multi" type="checkbox"'+(q.multi == 1 ? ' checked' : null)+' /><select data-qid="'+q.id+'" data-field="multi_num" class="question-multi-num">';
						if (q.slides != null) for (var j = 0; j < q.slides.length+1; j++) {
							h+= '<option value="'+j+'"';
							if ((typeof q.multi_num == 'undefined' && j == 0) || q.multi_num == j || (q.multi_num == null && j == 0)) h+= ' selected';
							j == 0 ? h+= ' disabled>(none)' : h+= '>'+j;
							h+= '</option>';
						}
						h+= '</select></td>'+
						'<td class="quiz-selector"><select data-qid="'+q.id+'" data-field="quiz" class="question-quiz">'+
							'<option value="0"'+(q.quiz == 0 ? ' selected' : '')+'>Home Only</option>'+
							'<option value="1"'+(q.quiz == 1 ? ' selected' : '')+'>General</option>'+
							'<option value="2"'+(q.quiz == 2 ? ' selected' : '')+'>Both</option>'+
						'</select></td>'+
						'<td class="column-weight"><select data-qid="'+q.id+'" data-field="weight" class="question-weight">';
						for (var w=1; w<=99; w++) h+= '<option value="'+w+'"'+(q.weight == w ? ' selected' : null)+'>'+w+'</option>';
						h+= '</select></td>'+
						'<td class="column-section"><select data-qid="'+q.id+'" data-field="section" class="question-section">'+
							'<option value="null" disabled'+(q.section == null ? ' selected' : '')+'>(none)</option>';
							var sections = ['Environment','Geography','Home Type','Home Features'];
							for (var i in sections) h+= '<option value="'+i+'"'+(q.section == i ? ' selected' : '')+'>'+sections[i]+'</option>';
						h+= '</select></td>';

					if ( typeof $('tr[data-qid='+q.id+']').html() == 'undefined' ) $('#questions tbody').append('<tr data-qid="'+q.id+'">'+h+'</tr>');
					else $('tr[data-qid='+q.id+']').html(h);

					$('.slide-list[data-qid='+q.id+']').sortable({update: function(event, ui) {
						var x = [];
						$(this).children().each(function(index){
							if ($(this).attr('slide-orderby') != index){
								x.push( {slide_id: $(this).attr('slide-id'), fields: {orderby: index} } );
								$(this).attr('slide-orderby', index);
							}
						});
						// console.log(x);
						ahtb.loading('Saving...',{opened:function(){ quizAdmin.updateSlides(x,function(){ahtb.close()}); }});
				  }});
				  $('.slide-list[data-qid='+q.id+'] a').click(function(){ quizAdmin.edit('slide', $(this).parent().attr('question-id'), $(this).parent().attr('slide-id')) });
					$('[data-qid='+q.id+'] .column-add-slide a').on('click',function(){
						var id = $(this).parent().parent().attr('data-qid');
						ahtb.loading('Adding slide...',{opened:function(){ quizAdmin.DB({
							query:'add-slide', data:{question_id: id, orderby: quizAdmin.qDB[id].slides.length }, done:function(d){ quizAdmin.parse('quiz-questions',d); quizAdmin.print('quiz-question', quizAdmin.qDB[id]); ahtb.close(); }
						})}})
					});
					$('.question-type[data-qid='+q.id+'], .question-quiz[data-qid='+q.id+'], .question-multi-num[data-qid='+q.id+'], .question-section[data-qid='+q.id+'], .question-weight[data-qid='+q.id+']').change(function(){ 
						var id = $(this).attr('data-qid');
						var data = {};
						data[$(this).attr('data-field')] = parseInt($(this).val())
						ahtb.loading('Saving...',{opened:function(){ quizAdmin.updateQuestion(id, data) }});
					});
					$('.question-enabled[data-qid='+q.id+'], .question-multi[data-qid='+q.id+']').click(function(){
						var id = $(this).attr('data-qid');
						var data = {};
						data[$(this).attr('data-field')] = $(this).is(':checked')?1:0;
						ahtb.loading('Saving...',{opened:function(){ quizAdmin.updateQuestion(id, data) }});
					});
					$('.question-predefined[data-qid='+q.id+']').click(function(){
						var id = $(this).attr('data-qid');
						var checked = $(this).is(':checked')?1:0;
						console.log('prefined clicked id:'+id+', checked:'+checked);
						// consider that this may not have to be radio button, but also a checkbox if we allow multiple predefined questions to exist.
						for (var i in quizAdmin.order) {
							var enabled = quizAdmin.qDB[quizAdmin.order[i]].enabled;
							var qId = quizAdmin.qDB[quizAdmin.order[i]].id;
							var update = false;
							var data = {};
							if (qId == id) {
								if (enabled & 2)
									console.log("qId:"+qId+", enabled:"+enabled+", already set for predefined");
								else {
									update = true;
									enabled |= 2;
									console.log("qId:"+qId+", enabled:"+enabled+", now set for predefined");
								}
							}
							else {
								if (enabled & 2) {
									update = true;
									enabled &= ~2;
									console.log("qId:"+qId+", enabled:"+enabled+", now unset for predefined");
									$('.question-predefined[data-qid='+qId+']').prop('checked', false);
								}
							}
							if (update) {
								data[$(this).attr('data-field')] = enabled;
								console.log("updating "+qId+" with enabled:"+enabled);
								quizAdmin.updateQuestion(qId, data);
							}
						}
					})
					$('[data-qid='+q.id+'] .column-enabled a').on('click',function(){
						var id = $(this).parent().parent().attr('data-qid');
						ahtb.open({title: 'Delete Question', height: 100, width: 150, html: '<p>Are you sure you want to delete this question?</p>', buttons:[
							{text: 'Delete Question', action:function(){ 
								ahtb.loading('Deleting...',{opened:function(){quizAdmin.DB({ query: 'delete-question', data: { question_id: id }, done: function(d){
									quizAdmin.qDB[id] = null;
									$('tr[data-qid='+id+']').slideUp(250,function(){$(this).remove()});
									// quizAdmin.parse('quiz-questions',d); //commented out because unneeded, for now
									ahtb.close();
								}})}});
							}},
							{text: 'Cancel', action:function(){ahtb.close()}},
						]});
					});
				}
				break;
			default: console.error('unable to print: '+type); break;
		}
	}
	this.updateQuestions = function(data){this.DB({ query:'update-questions', data:data, done:function(d){quizAdmin.parse('quiz-questions',d);ahtb.close()} })}
	this.updateQuestion = function(question_id, data){this.updateQuestions([{ question_id: question_id, fields: data }])}
	this.updateSlides = function(data, callback){this.DB({query: 'update-slides', data: data, done: function(d){ if (callback) callback(d); } })}
	this.DB = function(x){
		if (this.dbQueue == null) this.dbQueue = [];
		if (this.dbRunning == null) this.dbRunning = false;
		if (x){
			if (this.dbRunning) { x.id = this.dbQueue.length; this.dbQueue.push(x); }
			else {
				this.dbRunning = true;
				$.post(quizAdmin.ajaxURL, {query: x.query, data: (x.data?x.data:null)}, function(){}, 'json').done(function(d){
					quizAdmin.DBdone();
					if (d.status != 'OK') { ahtb.alert(d.data, 'Error', {height: 60+(16*Math.round(d.data.length/75)) }); console.log(d); }
					else if (x.done) x.done(d.data);
					else console.log(d);
				});
			}
		}
	}
	this.DBdone = function(){
		this.dbRunning = false;
		this.DB(quizAdmin.dbQueue.shift());
	}
}
quizAdmin.init();
});