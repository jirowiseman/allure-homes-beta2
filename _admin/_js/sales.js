if (typeof deleteCookie != 'function')
	window.deleteCookie = function (what) {
		var name = what + "=";
		document.cookie = name+"; expires=Thu, 01 Jan 1970 00:00:01 GMT"+ "; path=/";
	}

if (typeof setCookie != 'function')
	window.setCookie = function (cname, cvalue, exdays) {
	    var d = new Date();
	    d.setTime(d.getTime() + (exdays*24*60*60*1000));
	    var expires = "expires="+d.toUTCString();
	    deleteCookie(cname);
	    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
	}

if (typeof getCookie != 'function')
	window.getCookie = function (cname) {
	    var name = cname + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0; i<ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1);
	        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
	    }
	    return "";
	}

if (typeof checkCookie != 'function')
	window.checkCookie = function (what) {
	    var user = getCookie(what);
	    if (user != "") {
	        return true;
	    } else {
	        return false;
	    }
	}

var testCityName = '';
var ZIP_MAPTYPE_ID = 'ziphybrid';
var ZIP_OPACITY = 0.3;
var tskey = "cfa3314f70" ;

var CityTagState = {
	TAGS_NEED_ALL: 0,
	TAGS_HAVE_SOME: 1,
	TAGS_HAVE_ALL: 2,
	TAGS_ALL_LISTINGS: 3
}

var TableType = {
	ALL_AGENTS: 0,
	CITY_AGENTS: 1,
	TAGGED_AGENTS: 2
}

if (typeof SellerMetaFlags == 'undefined')
	var SellerMetaFlags = {
		SELLER_IMPROVEMENT_TASKS: 1,
		SELLER_KEY: 2,
		SELLER_BRE: 4,
		SELLER_INVITE_CODE: 8,
		SELLER_VISITATIONS: 16,
		SELLER_PROFILE_DATA: 32,
		SELLER_AGENT_MATCH_DATA: 64,
		SELLER_MODIFIED_PROFILE_DATA: 128,
		SELLER_NICKNAME: 256,
		SELLER_NEW_ORDER: 512,
		SELLER_AGENT_ORDER: 1024,
		SELLER_AGENT_MATCH_STATUS: 2048,
		SELLER_NOTES_FROM_RESERVATION: 4096
	}

if (typeof AgentOrderMode == 'undefined')
	var AgentOrderMode = {
		ORDER_IDLE: 0,
		ORDER_BUYING: 1,
		ORDER_BOUGHT: 2,
		ORDER_PORTAL: 10,
		ORDER_AGENT_MATCH: 20,
		ORDER_SIGN_UP: 30
	}

if (typeof ReservationMetaFlags == 'undefined')
	var ReservationMetaFlags = {
		RESERVATION_EXTRA_SELLER_DATA: (1 << 20)
	}

if (typeof AgentType == 'undefined')
	var AgentType = {
		AGENT_LISTHUB: 1,
		AGENT_INVITE: 2,
		AGENT_ORGANIC: 3
	}

var GatheredTypes = {
	Cities: 'locations',
	Listings: 'listings',
	Agents: 'agents'
}

var zohoToLLCKey_AccessSeller = 'ielwo3820ldioqlaie8okd1830wilszohollc88';


jQuery(document).ready(function($){

if (typeof length != 'function')
	window.length = function (item) {
		if( Array.isArray(item) ) 
			return item.length;

		if (typeof item != 'object' || item == null)
			return item != null ? 1 : 0;

			// Detecting IE
	    var oldIE;
	    if ($('section.ie-identifier').is('.ie6, .ie7, .ie8')) {
	        oldIE = true;
	    }

	    if (oldIE) {
	        // Here's your JS for IE..
	        var count = 0;
			for (var i in item) {
			    if (item.hasOwnProperty(i)) 
			        count++;
			    }
			return count;
	    } else {
	        // ..And here's the full-fat code for everyone else
	        return Object.keys( item ).length;
	    }			
}

	$('.spin-wrap').hide();
	$('.topOption').hide();

	var cities = [];
	var cityName = '';
	var j = 0;
	for (var i in ah_local.cities)
		if (typeof ah_local.cities[i] != 'undefined' &&
			typeof ah_local.cities[i].label != 'undefined' &&
			ah_local.cities[i].label != null &&
			ah_local.cities[i].label != '' &&
			typeof ah_local.cities[i].id != 'undefined' &&
			ah_local.cities[i].id != null &&
			ah_local.cities[i].id != 0) 
	    cities[j++] = {label: ah_local.cities[i].label, value: ah_local.cities[i].label, id: ah_local.cities[i].id};

	salesAdmin = new function(){
		this.ajaxSalesUrl = ah_local.tp+"/_admin/ajax_sales.php";
		this.cities = [];
		this.salesInfo = null;
		this.currentResvIndex = -1;
		this.originalAmData = null;
		this.currentResvOrigLSCount = 0;
		this.currentResvLocation = null;
		this.currentResvTagType = null;
		this.gatheredData = null;
		this.gatheredDataMarkers = null;
		this.gatheredDataInfoWindows = null;
		this.gatheredListingMarkers = null;
		this.gatheredListingInfoWindows = null;
		this.currentZone = null;
		this.gatheredZoneRectangle = null;
		this.currentZoneRectangle = null;
		this.hotkey = 0;
		this.mousePt = null;
		//var cityName = '';
		for (var i in ah_local.cities) {
		    this.cities[i] = ah_local.cities[i];
		    this.cities[i].index = i;
		}

		// adding tags for one city
		this.currentGatheredCity = null;

		// adding tags for one listing
		this.currentGatheredListing = null;

		// active tags for one or all gathered citeis
		this.activeTags = [];
		this.activeCategory = null;
		this.activeMode = 'location'; // or 'locations' for multiple cities, 'listing' for one listing, 'listings' for multiple listings


		this.init = function() {
			// this.getStatusData();
			this.finalize();
		}

		this.submitCRMContact = function(type) {
			var lead_id = $('.crm_id[name='+type+']').val();
			var seller_id = $('.seller_id[name='+type+']').val();
			var email = $('.email[name='+type+']').val();
			if (lead_id.length == 0 ||
				seller_id.length == 0 ||
				email.length == 0) {
				ahtb.alert("Please enter all three values");
				return;
			}
			console.log("submitCRMContact for "+type+", lead_id:"+lead_id+", seller_id:"+seller_id+", email:"+email);
			var query = type == 'leads' ? 'access-seller' : 'access-seller-contact';
			var call = ah_local.tp+"/_admin/ajax_sales.php?query="+query+"&leadID="+lead_id+"&sellerID="+seller_id+"&email="+email+"&key=ielwo3820ldioqlaie8okd1830wilszohollc88&fromLocal=true";
			$.ajax({
					url: call,
					dataType: 'JSON',
					type: 'POST',
					error: function($xhr, $status, $error){
						var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
						ahtb.open({ title: 'You Have Encountered an Error', 
									height: 150, 
									html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
								});
						
					},					
				  	success: function(data){
					  	if (data == null || typeof data == 'undefined' || typeof data.status == 'undefined') 
					  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
					  	else if (data.status != 'OK') {
					  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, width: 200, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
					  	}
					  	else {
					  		window.location = data.data;
					  	}
					}
			});
			return false;
		}

		this.createDataSet = function() {
			var x = {query: 'create-lifestyle-status-file',
					 done: function(d) {
					 	ahtb.open({
					 			 html: d+" Retrieving data..",
					 			 width: 450,
					 			 height: 150,
					 			 opened: function() {
					 			 	salesAdmin.getStatusData();
					 			 }
					 			})
					 },
					 error: function(d) {
		 			 	ahtb.alert(d,
		 			 			{width: 350, 
		 			 			 height: 150});
		 			 }};
			salesAdmin.DB(x);
		}

		this.getStatusData = function() {
			ahtb.open({	html: "Loading status file...",
						width: 350,
						height: 150,
						opened: function() {
							var x = {query: 'get-lifestyle-status-file',
									done: function(d) {
										reservationCollation = [];
										var data = d.data.split('||');
										for(var i in data) {
											var row = JSON.parse(data[i]);
											reservationCollation[row.city_id] = row;
										}

										salesAdmin.getMapCenter();
									},
									error: function(d) {
										ahtb.alert(d+" Creating data..",
												{width:450,
												 height: 150});
										salesAdmin.createDataSet();
									}}
							salesAdmin.DB(x);
						}})
		}

		this.getMapCenter = function() {
			ahtb.open({	html: "Loading map file...",
						width: 350,
						height: 150,
						opened: function() {
							var x = {query: 'get-map-center-file',
									done: function(d) {
										var data = d.data;
										mapCenter = JSON.parse(data);

										salesAdmin.getAgentData();
									},
									error: function(d) {
										ahtb.alert(d+" Creating data..",
												{width:450,
												 height: 150});
										salesAdmin.createDataSet();
									}}
							salesAdmin.DB(x);
						}})
		}

		this.getAgentData = function() {
			ahtb.open({	html: "Loading agent data...",
						width: 350,
						height: 150,
						opened: function() {
							var x = {query: 'get-agent-data-file',
									done: function(d) {
										var data = d.data.split('||');
										for(var i in data)
											activeAgents[activeAgents.length] = JSON.parse(data[i]);

										salesAdmin.getReservationsData();
									},
									error: function(d) {
										ahtb.alert(d+" Creating data..",
												{width:450,
												 height: 150});
										salesAdmin.createDataSet();
									}}
							salesAdmin.DB(x);
						}})
		}

		this.getReservationsData = function() {
			ahtb.open({	html: "Loading reservations data...",
						width: 350,
						height: 150,
						opened: function() {
							var x = {query: 'get-reservations-data-file',
									done: function(d) {
										var data = d.data.split('||');
										for(var i in data)
											reservations[reservations.length] = JSON.parse(data[i]);

										ahtb.close();
										salesAdmin.finalize();
									},
									error: function(d) {
										ahtb.alert(d+" Creating data..",
												{width:450,
												 height: 150});
										salesAdmin.createDataSet();
									}}
							salesAdmin.DB(x);
						}})
		}

		this.updateTags = function() {
			ahtb.open({html: "Updating City tags...",
					   width: 400,
					   height: 150,
					   opened: function () {
					   		var x = {query:'update-city-tags',
					    			done: function(d) {
					    				reservationCollation = [];
					    				var data = d.data.split('||');
										for(var i in data) {
											var row = JSON.parse(data[i]);
											if (typeof row != 'undefined' &&
												typeof row.city_id != 'undefined')
												reservationCollation[row.city_id] = row;
										}
					    				console.log("updated "+d.count+" cities");
					    				if (d.count)
						    				ahtb.alert("Updated "+d.count+" cities",
						    						{width: 400,
						    						height: 150});
						    			else
						    				ahtb.close();
						    			salesAdmin.finalize();
					    			},
					    			error: function(d) {
					    				ahtb.alert("Failed updateTags",
					    						{width: 400,
					    						height: 150})
					    				salesAdmin.finalize();
					    			}};
					    	salesAdmin.DB(x);
					   }
			})
		}


		this.finalize = function() {
			console.log("salesAdmin init entered");
			salesAdmin.salesInfo  = null;
			salesAdmin.list.lists = [];
			salesAdmin.map.init();
			salesAdmin.map.createMarkers();
			salesAdmin.map.setMapBounds();

			salesAdmin.list.init();

			$('#optionWrapper input[value=Submit]').prop('disabled', true);
			$('#optionWrapper .optionActive').on('click', function() {
				var clicked = $(this).prop('checked');
				$('#optionWrapper input[value=Submit]').prop('disabled', !clicked);
				if (!clicked) {
					salesAdmin.salesInfo  = null;
				 	salesAdmin.list.lists = [];
					salesAdmin.list.showTable(10+TableType.ALL_AGENTS); // 10 indicated fromNav
				 	if (salesAdmin.map.hidden)
						salesAdmin.map.showQuick();
				 	salesAdmin.map.init();
					salesAdmin.map.createMarkers();
					salesAdmin.map.setMapBounds();
					google.maps.event.trigger(salesAdmin.map.map, 'resize');
				}
			})

			salesAdmin.setupAutocomplete($('#optionWrapper #addressOption .city'));
			$('#optionWrapper #addressOption #submitAddress').on('click', function() {
				var city = $('#optionWrapper #addressOption .city').attr('city_id');
				var index = $('#optionWrapper #addressOption .city').attr('city_index');
				testCityId = city? parseInt(city) : 0;
			 	if (!testCityId) {
			 		 if (testCityName.length == 0) {
				 		testCityName = $('#optionWrapper #addressOption .city').val();
				 		if (testCityName.length == 0) {
				 			msg = "Please pick your city.";
							ahtb.alert(msg, {height: 170});
							return;
				 		}
				 		else {
				 			salesAdmin.getCityId(testCityId, testCityName);
				 			return;
				 		}
			 		}
			 	}
			 	else {
			 		salesAdmin.list.location = testCityId;
			 		salesAdmin.cityIndex = index;
			 		testCityName = $('#optionWrapper #addressOption .city').val();
			 		salesAdmin.list.showTable( 20+TableType.CITY_AGENTS, 0, testCityId ); // 20 indicates fromOptionSubmit
			 		if (typeof reservationCollation[testCityId] == 'undefined') {
			 			salesAdmin.map.map.setCenter(new google.maps.LatLng(reservationCollation.cities[salesAdmin.list.location].lat, reservationCollation.cities[salesAdmin.list.location].lng));
						salesAdmin.map.map.setZoom(14);
			 		}
			 		else {
						salesAdmin.map.map.setCenter(new google.maps.LatLng(salesAdmin.cities[salesAdmin.cityIndex].lat, salesAdmin.cities[salesAdmin.cityIndex].lng));
						salesAdmin.map.map.setZoom(14);
					}
			 	}
			})

			$('#optionWrapper #sellerOption #submitName').on('click', function() {
				var first = $('input#firstName').val();
				var last = $('input#lastName').val();
				var email = $('input#email').val();

				if (first.length == 0 &&
					last.length == 0 &&
					email.length == 0) {
					ahtb.alert("Please enter at least one piece of information",
							{width: 400,
							height: 150});
					return;
				}

				var x = {query: 'get-sales-info',
						 data: {first: first,
						 	    last: last,
						 		email: email},
						 done:function(d) {
						 	salesAdmin.salesInfo = d;

						 	salesAdmin.list.lists = [];
						 	salesAdmin.list.showTable(20+TableType.ALL_AGENTS); // 20 indicates fromOptionSubmit
						 	if (salesAdmin.map.hidden)
						 		salesAdmin.map.showQuick();
						 	salesAdmin.map.init();
							salesAdmin.map.createMarkers();
							salesAdmin.map.setMapBounds();

						 },
						 error: function(d) {
						 	ahtb.alert("Failed to get sales data for first:"+first+", last:"+last+", email:"+email,
										{width:450,
										 height: 150});
						 	salesAdmin.salesInfo  = null;
						 }};
				salesAdmin.DB(x);
			})

			$('ul.selector#view li a').on('click',function(){
		    	if ($(this).hasClass('list')) {
		    		console.log("view left click");
		    		$('div#zoomInstructions').removeClass('active');
		    		salesAdmin.map.hide();
		    		$('#zipCodeDiv').hide();
		    		setCookie('ReservationViewSelector', 'list');
		    	}
		    	else if ($(this).hasClass('map')) {
		    		console.log("view right click");
		    		$('div#zoomInstructions').addClass('active');
		    		salesAdmin.map.show();
		    		$('#zipCodeDiv').show();
		    		setCookie('ReservationViewSelector', 'map');
		    	}
		    });

		    $('#zipCodes').on('click', function() {
		    	var checked = $(this).prop('checked');
		    	salesAdmin.map.imageMapType.set('opacity', checked ? ZIP_OPACITY : 0); 
		    })
		    $('#kml').on('click', function() {
		    	var checked = $(this).prop('checked');
		    	salesAdmin.map.kmlLayer.setMap(checked ? salesAdmin.map.map : null); 
		    })
		    $('updateTags').on('click', function() {
		    	salesAdmin.updateTags();
		    })

		    $('ul.selector#list li a').prop('disabled', true);
		    $('ul.selector#list li a').on('click',function(){
		    	if ($(this).hasClass('left')) {
		    		// salesAdmin.map.hide();
		    		// setCookie('ReservationDataViewSelector', 'list');
		    		console.log("data left click");
		    		if(salesAdmin.list.index > 0) {
		    			switch(salesAdmin.list.index) {
		    				case 2:
		    					salesAdmin.list.showTable( 10+salesAdmin.list.index-1, salesAdmin.list.reservation, salesAdmin.list.location );
		    					break;
		    				case 1:
		    					salesAdmin.list.showTable( 10+salesAdmin.list.index-1 );
		    					break;
		    			}
		    		}
		    	}
		    	else if ($(this).hasClass('right')) {
		    		// salesAdmin.map.show();
		    		// setCookie('ReservationDataViewSelector', 'map');
		    		console.log("data right click");
		    		if(salesAdmin.list.index < 2) {
		    			switch(salesAdmin.list.index) {
		    				case 0:
		    					salesAdmin.list.showTable( 10+salesAdmin.list.index+1, salesAdmin.list.reservation, salesAdmin.list.location );
		    					break;
		    				case 1:
		    					salesAdmin.list.showTable( 10+salesAdmin.list.index+1, salesAdmin.list.location, salesAdmin.list.specialty );
		    					break;
		    			}
		    		}
		    	}
		    });

			$('div#reservation-map').animate(
						{ left: "100%" },
						{ queue: false, duration: 500, done:function(){
							$(this).hide();
							$('div#reservations-list-div').show().animate({left: '0%'},{ queue: false, duration: 500 });
						}}
					);

			$('div#tag-selections button#accept').on('click', function() {
				salesAdmin.saveTagSelection();
				// $('div#tag-selections').removeClass('active');
			})

			$('div#tag-selections button#cancel').on('click', function() {
				$('div#tag-selections').removeClass('active');
			})

			$('div.context-menu button').on('click', function() {
				$('div.context-menu').hide();
			})
		}

		this.setReservationMeta = function(dataPack) {
			var index = salesAdmin.currentResvIndex;
			var location = salesAdmin.currentResvLocation;
			var tagType = salesAdmin.currentResvTagType;
			var res = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.reservations != 'undefined' ? salesAdmin.salesInfo.reservations : reservations;
			var collated = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.reservationCollation != 'undefined' ? salesAdmin.salesInfo.reservationCollation : reservationCollation;

			if (!location &&
				!tagType)
				dataPack.reservation = res[index];
			else
				dataPack.reservation = res[collated[location].tags[tagType].reservations[index]];
			// dataPack.bre = null;
			// dataPack.profileData = null;
			// dataPack.amData = null;
			// dataPack.extraData = null;
			// var currentdate = new Date(); 
			// var currentYear = parseInt(currentdate.getFullYear());

			var reservation = dataPack.reservation;
			for(var i in reservation.meta) {
				if (reservation.meta[i].action == SellerMetaFlags.SELLER_BRE)
					dataPack.bre = reservation.meta[i];
				else if (reservation.meta[i].action == SellerMetaFlags.SELLER_PROFILE_DATA)
					dataPack.profileData = reservation.meta[i];
				else if (reservation.meta[i].action == AgentOrderMode.ORDER_AGENT_MATCH)
					dataPack.amData = reservation.meta[i];
				else if (reservation.meta[i].action == ReservationMetaFlags.RESERVATION_EXTRA_SELLER_DATA)
					dataPack.extraData = reservation.meta[i];
				else if (reservation.meta[i].action == SellerMetaFlags.SELLER_NOTES_FROM_RESERVATION)
					dataPack.notes = reservation.meta[i];
			}
		}

		this.matchDescWithProfile = function(id, force) {
			// var index = salesAdmin.currentResvIndex;
			// var res = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.reservations != 'undefined' ? salesAdmin.salesInfo.reservations : reservations;
			// var reservation = res[index];
			var textarea = $('.define-agent-match-container #define-agent-match #description[for="'+id+'"]');
			var dataPack = {bre: null,
							profileData: null,
							amData: null,
							extraData: null,
							notes: null,
							reservation: null
							};

			salesAdmin.setReservationMeta(dataPack);
			var bre = dataPack.bre;
			var profileData = dataPack.profileData;
			var amData = dataPack.amData;
			var extraData = dataPack.extraData;
			// var currentdate = new Date(); 
			// var currentYear = parseInt(currentdate.getFullYear());

			// for(var i in reservation.meta) {
			// 	if (reservation.meta[i].action == SellerMetaFlags.SELLER_BRE)
			// 		bre = reservation.meta[i];
			// 	else if (reservation.meta[i].action == SellerMetaFlags.SELLER_PROFILE_DATA)
			// 		profileData = reservation.meta[i];
			// 	else if (reservation.meta[i].action == AgentOrderMode.ORDER_AGENT_MATCH)
			// 		amData = reservation.meta[i];
			// 	else if (reservation.meta[i].action == ReservationMetaFlags.RESERVATION_EXTRA_SELLER_DATA)
			// 		extraData = reservation.meta[i];
			// }

			if (!amData) {
				console.log("matchDescWithProfile - Failed to find amData!");
				return;
			}

			var item = amData.item[id];
			var desc = item.desc;
			var forceIt = typeof force != 'undefined' ? force : false;
			textarea.val(desc.replace(/\\/g,""));

			if (parseInt(item.location) == 0) {
				if (extraData &&
					extraData.city.length &&
					extraData.state.length) {
					var city = extraData.city+", "+extraData.state;
					salesAdmin.getCity(id, city);
				}	
			}
			else {
				var city = salesAdmin.matchCity(item.location);
				$('.define-agent-match-container #define-agent-match .service_areas[for="'+id+'"]').val(city);
				item['locationStr'] = city;
				if (!isNaN(item.specialty) && parseInt(item.specialty) != 0)
					item['specialtyStr'] = amTagList[item.specialty].tag;
			}
		}

		this.agentMatchSection = function(index, item) {
			item.specialty = parseInt(item.specialty);
			var desc = item.desc;
			
			var h = '<div id="define-agent-match">' +
						'<span class="define-agent-match-span1">' +
							// '<input type="checkbox" id="mode" for="'+index+'" '+(item.mode == AgentOrderMode.ORDER_BUYING? "checked " : '')+(item.mode == AgentOrderMode.ORDER_BOUGHT ? 'style="display: none;"': '')+' /><span id="buy" '+(item.mode == AgentOrderMode.ORDER_BOUGHT ? 'style="display: none;"': '')+'>Buy</span>' +
							'<label for="service_areas">Expertise Service Area</label><input type="text" class="service_areas" for="'+index+'" placeholder="eg: San Francisco, CA" style="color:#444;height:30px;padding:0 .25em" value="'+(item.location ? salesAdmin.matchCity(item.location) : '')+'" />' +
						'</span>' +
						'<span class="define-agent-match-span2">' +
							'<label for="field_expertise">Field of Expertise</label>'+
							'<select class="field_expertise" for="'+index+'" >';
						for(var i in amTagList) {
							h += '<option value="'+i+'" '+(item.specialty ? (item.specialty == i ? 'selected="selected"' : '') : '')+'>'+amTagList[i].tag+'</option>';
							if (item.specialty && item.specialty == i) item.specialtyStr = amTagList[i].tag;
						}
					h+= '</select>' +
						'</span>' +
						// '<button for="'+index+'" id="update" '+(item.mode == AgentOrderMode.ORDER_BOUGHT ? '' : 'style="display:none;"')+'>Update Description</button>'+
						'<textarea id="description" for="'+index+'" placeholder="Please enter detailed information that showcases your strength for this selection.\n\nYou need to be knowledgable about and experienced enough to answer any question about this specialty, WITHOUT searching on the internet for it.  We will be calling all candidates to vet them, to make sure our Agent Match specialists are who they claim to be.\n\nFor example:\nWhy do you consider yourself an expert/specialist?\nHow long have you done this?\nDo you have any specialized certifcations or won awards?\nWhat makes you stand out from the rest, in your area, that will make a potential buyer want to talk to you about this?">'+desc+'</textarea>' +
					'</div>';
			return h;
		}

		this.add_callbacks = function() {
			// var index = salesAdmin.currentResvIndex;
			// var res = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.reservations != 'undefined' ? salesAdmin.salesInfo.reservations : reservations;
			// var reservation = res[index];
			var dataPack = {bre: null,
							profileData: null,
							amData: null,
							extraData: null,
							notes: null,
							reservation: null
							};

			salesAdmin.setReservationMeta(dataPack);
			var bre = dataPack.bre;
			var profileData = dataPack.profileData;
			var amData = dataPack.amData;
			var extraData = dataPack.extraData;
			// var currentdate = new Date(); 
			// var currentYear = parseInt(currentdate.getFullYear());

			// for(var i in reservation.meta) {
			// 	if (reservation.meta[i].action == SellerMetaFlags.SELLER_BRE)
			// 		bre = reservation.meta[i];
			// 	else if (reservation.meta[i].action == SellerMetaFlags.SELLER_PROFILE_DATA)
			// 		profileData = reservation.meta[i];
			// 	else if (reservation.meta[i].action == AgentOrderMode.ORDER_AGENT_MATCH)
			// 		amData = reservation.meta[i];
			// 	else if (reservation.meta[i].action == ReservationMetaFlags.RESERVATION_EXTRA_SELLER_DATA)
			// 		extraData = reservation.meta[i];
			// }

			if (amData) for(var j in amData.item)
				salesAdmin.add_callback(j, amData);
		}

		this.add_callback = function(id, amData) {
			$('.define-agent-match-container #define-agent-match .service_areas[for="'+id+'"]').on('change', function() {
				var ele = $(this);
				var item = $(this).attr('for');
				var val = $(this).attr('city_id');
				var cityName = $(this).attr('city_name');
				var city = $(this).val();
				console.log("AM service area is "+city+' or '+cityName+', val:'+val+', for:'+item);
				if ((typeof val == 'undefined' ||
					 val == '0') &&
					city.length)
					salesAdmin.list.getCityId(item, city);
				else if (typeof val != 'undefined' &&
						 val != '0' &&
						 typeof cityName == 'string' &&
						 cityName.length) {
					amData.item[item].location = val;
					amData.item[item].locationStr = cityName;
				}
			});
			
			$('.define-agent-match-container #define-agent-match .field_expertise[for="'+id+'"]').on('change', function() {
				var item = $(this).attr('for');
				var val = $(this,'option:selected').val();
				console.log("AM specialty is "+val+' for:'+item);
				amData.item[item].specialty = parseInt(val);
				amData.item[item].specialtyStr = amTagList[val].tag;
				salesAdmin.matchDescWithProfile(item, true);
			});
		}

		this.add_another = function() {
			// var index = salesAdmin.currentResvIndex;
			// var res = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.reservations != 'undefined' ? salesAdmin.salesInfo.reservations : reservations;
			// var reservation = res[index];
			var dataPack = {bre: null,
							profileData: null,
							amData: null,
							extraData: null,
							notes: null,
							reservation: null
							};

			salesAdmin.setReservationMeta(dataPack);
			var bre = dataPack.bre;
			var profileData = dataPack.profileData;
			var amData = dataPack.amData;
			var extraData = dataPack.extraData;
			// var currentdate = new Date(); 
			// var currentYear = parseInt(currentdate.getFullYear());

			// for(var i in reservation.meta) {
			// 	if (reservation.meta[i].action == SellerMetaFlags.SELLER_BRE)
			// 		bre = reservation.meta[i];
			// 	else if (reservation.meta[i].action == SellerMetaFlags.SELLER_PROFILE_DATA)
			// 		profileData = reservation.meta[i];
			// 	else if (reservation.meta[i].action == AgentOrderMode.ORDER_AGENT_MATCH)
			// 		amData = reservation.meta[i];
			// 	else if (reservation.meta[i].action == ReservationMetaFlags.RESERVATION_EXTRA_SELLER_DATA)
			// 		extraData = reservation.meta[i];
			// }
			var newItem = {
				// mode: AgentOrderMode.ORDER_BUYING,
				// type: AgentOrderMode.ORDER_AGENT_MATCH,
				desc: '',
				location: 0,
				locationStr: '',
				specialty: '6',
				specialtyStr: 'equestrian',
				// cart_item_key: 0,
				// order_id: 0,
				// subscriptionType: seller.subscriptionType,
				// priceLevel: seller.priceLevel
			}
			var id = 0;
			if (amData) {
				id = amData.item.length;
				amData.item[id] = newItem;
			}
			else {
				amData = {
					action: AgentOrderMode.ORDER_AGENT_MATCH,
					item: [newItem]
				}
				reservation.meta[reservation.meta.length] = amData;
			}
			$('.agent-match-div .maincontent .define-agent-match-container').append(salesAdmin.agentMatchSection(id, newItem));					
			salesAdmin.add_callback(id, amData);

			var element = $('.define-agent-match-container #define-agent-match .service_areas[for="'+id+'"]');
			salesAdmin.list.setupAutocomplete(element);
			salesAdmin.matchDescWithProfile(id);				

			var scrollTop = amData.item.length > 2 ? (amData.item.length - 2) * $('.define-agent-match-container #define-agent-match').outerHeight(true) : 0;
			$('.define-agent-match-container').scrollTop(scrollTop);
			console.log("add_another - scrollTop:"+scrollTop);
		}

		this.matchCity = function(id) {
			for(var i in cities)
				if (cities[i].id == id)
					return cities[i].label;

			console.log("matchCity could not find id:"+id);
			return '';
		}
		
		this.updateCity = function(type1, cityId, tagId, fromAgent) {
			var collated = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.reservationCollation != 'undefined' ? salesAdmin.salesInfo.reservationCollation : reservationCollation;
			var h = '<div id="updateCity">'+
						'<span>'+collated[cityId].locationStr+' - '+collated[cityId].tags[tagId].specialtyStr+'</span>'+
						'<div id="scoring">'+
							'<label>Slide to a new score:</label>&nbsp;'+
							'<span id="scoreValue">'+collated[cityId].tags[tagId].score+'</span>&nbsp;<div id="scoreSlider" />'+
						'</div>' +
						'<div id="controls">'+
							'<button id="ok">OK</button>'+
						'</div>'+
					'</div>';
			var score = collated[cityId].tags[tagId].score;
			ahtb.open({	html: h,
					   	width: 450,
					 	height: 155,
					 	hideSubmit: true,
					 	opened: function() {
					 		$('#ok').on('click', function() {
					 			ahtb.close();
					 		})
					 		$( "#scoreSlider" ).slider({
						      	range: false,
						      	min: 1,
						      	max: 10,
						      	value: score,
						      	stop: function( event, ui ) {							      	
						        	score = ui.value;
						        	$('#scoreValue').html(ui.value);
						      	},
						      	slide: function( event, ui) {
						      		$('#scoreValue').html(ui.value);
						      	}
						    });
					 	},
					 	closed: function() {
					 		if (score != collated[cityId].tags[tagId].score) {
					 			$.post(ah_local.tp+'/_admin/ajax_sales.php',
						                { query:'update-city-tag', 
						                  data: { city_id: cityId,
						                  		  tag_id: tagId,
						                  		  score: score }
						                },
						                function(){}, 'JSON')
										.done(function(d){
											if (d &&
												d.status) {
												// ahtb.alert("Updating "+collated[cityId].locationStr+" returned:"+d.status+" for tag:"+collated[cityId].tags[tagId].specialtyStr,
												// 	{width: 450, height: 150});
												if (d.status == 'OK') {
													collated[cityId].tags[tagId].score = score;
													delete salesAdmin.list.lists[salesAdmin.list.index];
													if (type1 != -1) // would be the agent id
														salesAdmin.list.showTable(salesAdmin.list.index, type1, cityId, fromAgent ? 'agent' : false);
													else
														salesAdmin.list.showTable(	salesAdmin.list.index, 
																					salesAdmin.list.index == 1 ? (fromAgent ? salesAdmin.list.agent : salesAdmin.list.reservation) : cityId, 
																					salesAdmin.list.index == 1 ? cityId : tagId, 
																					salesAdmin.list.index == 1 ? 'agent' : false);
													salesAdmin.map.createMarker(collated[cityId], cityId);
												}
												else
													ahtb.alert("Failed to update "+collated[cityId].locationStr,
															{width: 450, height: 150});
											}
										})
										.fail(function(d){
											ahtb.alert("Failed to update "+collated[cityId].locationStr,
												{width: 450, height: 150});
										});
					 		}
					 }})
		}

		this.list = new function() {
			this.index = 0;
			this.reservation = 0;
			this.agent = 0;
			this.location = 0;
			this.specialty = 0;
			this.lists = [];

			this.init = function() {
				this.showTable(this.index);
			}

			this.showTable = function(index, type, type2, fromAgent) {
				var h = this.createTable(index, type, type2, fromAgent);
				$('#reservations-list-div').html(this.lists[this.index].html);	
				switch(this.index) {
					case TableType.ALL_AGENTS:
					case TableType.TAGGED_AGENTS:
						$('#reservations-list-div .details.left').scrollTop(this.lists[this.index].offsetLeft);
						$('#reservations-list-div .details.right').scrollTop(this.lists[this.index].offsetRight);
						break;
					case TableType.CITY_AGENTS:
						$('#reservations-list-div .details').scrollTop(this.lists[this.index].offset);
						break;
				}
				
			}

			this.showCityStatus = function(index, type, type2, fromAgent) {
				var h = this.createTable(index, type, type2, fromAgent);
				content = 	'<div id="ahtb-list-div" class="popup">'+this.lists[this.index].html+'</div>'+
							'<div id="messageDiv">'+
								'<span id="message"></span>'+
							'</div>';
				ahtb.open({html: content,
						   width: 1280,
							height: 400});
			}

			this.setupAutocomplete = function(element) {
				element.autocomplete({
			    source: cities,
			    select: function(e, ui){ 
			      if (ui.item.value != null) {
			        element.attr('city_id', ui.item.id); 
			        element.attr('city_name', ui.item.label); 
			        cityName = ui.item.label;
			        element.change();
			      }
			    }
			  }).on('focus',function(){
			    if ($(this).val() == 'ENTER CITY') {
			      $(this).val(''); $(this).removeClass('inactive');
			    }
			  }).on('keypress', function(e){
			    var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
			    if(keynum == 13) {//13 is the enter char code
			      e.preventDefault();
			    }
			  });
			}

			this.pickedCity = function(id, city_id, city) {
				var index = salesAdmin.currentResvIndex;
				var res = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.reservations != 'undefined' ? salesAdmin.salesInfo.reservations : reservations;
				var reservation = res[index];
				var bre = null;
				var profileData = null;
				var amData = null;
				var extraData = null;
				// var currentdate = new Date(); 
				// var currentYear = parseInt(currentdate.getFullYear());

				for(var i in reservation.meta) {
					if (reservation.meta[i].action == SellerMetaFlags.SELLER_BRE)
						bre = reservation.meta[i];
					else if (reservation.meta[i].action == SellerMetaFlags.SELLER_PROFILE_DATA)
						profileData = reservation.meta[i];
					else if (reservation.meta[i].action == AgentOrderMode.ORDER_AGENT_MATCH)
						amData = reservation.meta[i];
					else if (reservation.meta[i].action == ReservationMetaFlags.RESERVATION_EXTRA_SELLER_DATA)
						extraData = reservation.meta[i];
				}
				amData.item[id].location = city_id;
				amData.item[id].locationStr = city;
				$('#define-agent-match input.service_areas[for="'+id+'"]').val(city);
				// ahtb.close();
			}

			this.getCityId = function(id, city) {
				$.post(ah_local.tp+'/_pages/ajax-quiz.php',
	                { query:'find-city',
	                  data: { city: city,
	                  		  distance: 0 }
	                },
	                function(){}, 'JSON')
					.done(function(d){
						if (d &&
	                        d.data &&
	                        d.data.city &&
	                        (typeof d.data.city == 'object' ||
	                        d.data.city.length) ){
		                    if (d.status == 'OK' &&
		                        (typeof d.data.city == 'object' || d.data.city.length) ) {							                        
		                        console.log(JSON.stringify(d.data));

		                    	if (d.data.allState == 'true' || d.data.allState == true || d.data.allState == 1) {// doing statewide
		                    		ahtb.push();
			                        ahtb.open({html:"Please enter a city name along with the state.", 
			                        			width: 450,
			                        			height: 110,
			                        			buttons: [
				                                  {text: 'OK', action: function() {
				                                    ahtb.close(ahtb.pop);
				                                  }}]});
			                        return;
			                    }

			                    var len = length(d.data.city); //(typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
		                    	// var len = (typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
								if (len > 1) {
									if (d.data.allState == 'false' || d.data.allState == false) {
				                        var h = '<p>Please pick one of the cities:</p>' +
				                                  '<ul class="cities">';
				                        for(var i in d.data.city) {
				                        	var location = d.data.city[i].city+', '+d.data.city[i].state;
				                          	h += '<li><a href="javascript:salesAdmin.list.pickedCity('+id+','+d.data.city[i].id+",'"+location+"'"+');" >'+location+'</a></li>';
				                        }
				                        h += '</ul>';
				                        ahtb.push();
				                        ahtb.open(
				                          { title: 'Cities List',
				                            width: 380,
				                            height: (150 + (len * 25)) < 680 ? (150 + (len * 25)) : 680,
				                            html: h,
				                            buttons: [
				                                  {text: 'Cancel', action: function() {
				                                    ahtb.close(ahtb.pop);
				                                  }}],
				                            opened: function() {
				                            }
				                          })
				                    }
				                    else {
				                    	ahtb.push();
			                          	ahtb.open({html:"Only one city/state combination accepted.", 
			                          			   width: 400,
			                          			   height: 150,
				                          		   buttons: [
					                                  {text: 'OK', action: function() {
					                                    ahtb.close(ahtb.pop);
					                                  }}]});
			                        }
			                    } // only one!
			                    else {
			                    	salesAdmin.list.pickedCity(id, d.data.city[0].id, d.data.city[0].city+', '+d.data.city[0].state);
			                    	if (salesAdmin.matchCity(d.data.city[0].id).length == 0) {
			                    		var label = d.data.city[0].city+', '+d.data.city[0].state;
			                    		cities[cities.length] = {label: label, value: label, id: d.data.city[0].id};
			                    	}
			                    }
		                    }
		                    else {
		                      ahtb.push();
		                      ahtb.open({html:d && d.data ? d.data : 'Failed find a matching city id for '+city, 
		                      			title: 'City Match', 
		                      			 height:120, 
		                      			 width:600,
		                      			 buttons: [
				                                  {text: 'OK', action: function() {
				                                    ahtb.close(ahtb.pop);
				                                  }}]});
		                    }
		                }
		                else {
		                	ahtb.push();
		                    ahtb.open({html: d && d.data ? d.data : 'Failed find a matching city id for '+city, 
		                      			title: 'City Match', 
		                      			height:120, 
		                      			width:600,
		                      			buttons: [
				                                  {text: 'OK', action: function() {
				                                    ahtb.close(ahtb.pop);
				                                  }}]});
		                }
	                })
	                .fail(function(d){
	                	ahtb.push();
	                	ahtb.open({html:d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                      			title: 'City Match', 
	                      			height:120, 
	                      		 	width:600,
	                      			buttons: [
			                                  {text: 'OK', action: function() {
			                                    ahtb.close(ahtb.pop);
			                                  }}]});
	                });
			}

			this.gatherReservationData = function(writeBack) {
				if (salesAdmin.currentResvIndex === -1)
					return;

				writeBack = typeof writeBack == 'undefined' || writeBack == false ? false : true;

				var dataPack = {bre: null,
								profileData: null,
								amData: null,
								extraData: null,
								notes: null,
								reservation: null
								};

				salesAdmin.setReservationMeta(dataPack);
				var reservation = dataPack.reservation;
				var bre = dataPack.bre;
				var profileData = dataPack.profileData;
				var amData = dataPack.amData;
				var extraData = dataPack.extraData;
				var currentdate = new Date(); 
				var currentYear = parseInt(currentdate.getFullYear());

				var inputs = ['first_name', 'last_name', 'email', 'phone', 'portal', 'contact_email', 'mobile', 'company', 'website', 'street_address', 'city', 'state', 'zip', 'country', 'BRE', 'preferredName', 'year', 'inArea', 'sold', 'about'];
				var data = {};
				var fields = {};
				var needAmData = false;
				var needProfileData = false;
				var needBre = false;
				var needExtraData = false;

				if (amData) {
					if (salesAdmin.currentResvOrigLSCount != amData.item.length)
						needAmData = true;

					for(var i in amData.item) {
						data['desc'+i] = $('.define-agent-match-container #define-agent-match #description[for="'+i+'"]').val();
						if (data['desc'+i] != amData.item[i].desc) {
							needAmData = true;
							amData.item[i].desc = data['desc'+i];
						}
						// now compare current location and specialty with original (if it exists)
						if (i < salesAdmin.currentResvOrigLSCount &&
							salesAdmin.originalAmData) {
							if (salesAdmin.originalAmData.item[i].location != amData.item[i].location ||
								salesAdmin.originalAmData.item[i].specialty != amData.item[i].specialty)
								needAmData = true;
						}
					}
				}

				for(var i in inputs) {
					data[inputs[i]] = $('[name='+inputs[i]+']').val();
					switch(inputs[i]) {
						// main reservation fields
						case 'first_name':
						case 'last_name':
						case 'email':
						case 'phone':
						case 'portal':
							if (inputs[i] == 'phone')
								data[inputs[i]] = $('[name='+inputs[i]+']').val().replace(/[^0-9]/g, '');
							if (reservation[inputs[i]] != data[inputs[i]] &&
								!(reservation[inputs[i]] == null && data[inputs[i]] == '') ) {
								reservation[inputs[i]] = data[inputs[i]];
								fields[inputs[i]] = data[inputs[i]];
							}
							break;
						// profile data
						case 'contact_email':
						case 'preferredName':
						case 'year':
						case 'inArea':
						case 'sold':
							if (!profileData) {
								profileData = {	action: SellerMetaFlags.SELLER_PROFILE_DATA,
												contact_email: '',
												first_name: '',
												last_name: '',
												year: currentYear,
												inArea: 1,
												sold: 1 };
							}
							if (inputs[i] == 'preferredName' &&
								data[inputs[i]] != '') {
								var names = data[inputs[i]].split(' ');
								if (profileData['first_name'] != names[0]) {
									profileData['first_name'] = names[0];
									needProfileData = true;
								}

								if (names.length > 1) {
									names.shift();
									name = names.join(' ');
									if (profileData['last_name'] != name) {
										profileData['last_name'] = name;
										needProfileData = true;
									}
								}
							}
							else if (inputs[i] == 'inArea') {
								var years = currentYear - parseInt(data[inputs[i]])  + 1;
								if (profileData[inputs[i]] != years) {
									profileData[inputs[i]] = years;
									needProfileData = true;
								}
							}
							else if (profileData[inputs[i]] != data[inputs[i]] &&
								data[inputs[i]] != '') {
								profileData[inputs[i]] = data[inputs[i]];
								needProfileData = true;
							}
							break;

						// bre
						case 'BRE':
							if (!bre) {
								bre = {	action: SellerMetaFlags.SELLER_BRE,
										BRE: '',
										state: data['state'] != '' ? data['state'] : (amData ? amData.item[0].locationStr.split(",")[1].trim() : ''),
										userMode: reservation['seller_id'] ? AgentType.AGENT_LISTHUB : AgentType.AGENT_ORGANIC,
										needVerify: reservation['seller_id'] ? 0 : 1
									  }
							}
							if (bre[inputs[i]] != data[inputs[i]] &&
								data[inputs[i]] != '') {
								bre[inputs[i]] = data[inputs[i]];
								needBre = true;
							}
							break;

						// extra data
						case 'company':
						case 'website':
						case 'mobile':
						case 'street_address':
						case 'city':
						case 'state':
						case 'zip':
						case 'country':
						case 'about':
							if (inputs[i] == 'mobile')
								data[inputs[i]] = $('[name='+inputs[i]+']').val().replace(/[^0-9]/g, '');
							if (!extraData) {
								extraData = {action: ReservationMetaFlags.RESERVATION_EXTRA_SELLER_DATA,
											 company: '',
											 website: '',
											 street_address: '',
											 city: '',
											 state: '',
											 zip: '',
											 country: '',
											 about: ''
											}
							}
							if (extraData[inputs[i]] != data[inputs[i]] &&
								data[inputs[i]] != '') {
								extraData[inputs[i]] = data[inputs[i]];
								needExtraData = true;
							}
							break;
					}
				}

				reservation.meta = null;
				var metas = [bre, profileData, extraData];
				if (amData)
					metas[metas.length] = amData;
				if (dataPack.notes)
					metas[metas.length] = dataPack.notes;
				reservation.meta = metas;

				var saveMeta = 	needAmData || 
								needBre ||
								needProfileData ||
								needExtraData;
				if (writeBack &&
					(Object.keys(fields).length ||
					 saveMeta)) {
					if (saveMeta)
						fields.meta = metas;
					var x = {query: 'save-reservation',
							data: { id: reservation.id,
									fields: fields},
							done: function(d) {
								console.log(typeof d == 'string' ? d : JSON.stringify(d));
							},
							error: function(d) {
								if (salesAdmin.map.hidden)
									window.setTimeout(function() {
										ahtb.alert(typeof d != 'undefined' && d ? (typeof d == 'string' ? d : JSON.stringify(d)) : "Database failure!!", {width: 450, height: 150});
									}, 250);
								else
					        		window.setTimeout(function() {
					        			$('#messageDiv #message').html(typeof d != 'undefined' && d ? (typeof d == 'string' ? d : JSON.stringify(d)) : "Database failure!!");
					        		}, 1000); 
							}};
					salesAdmin.DB(x);
				}
				
				salesAdmin.currentResvIndex = -1;
				salesAdmin.currentResvLocation = null;
				salesAdmin.currentResvTagType = null;
			}

			this.editReservation = function(index, location, tag) {
				salesAdmin.currentResvIndex = index;
				salesAdmin.currentResvLocation = typeof location == 'undefined' ? null : location;
				salesAdmin.currentResvTagType = typeof tag == 'undefined' ? null : tag;
				salesAdmin.currentResvOrigLSCount = 0;
				salesAdmin.originalAmData = null;

				// var res = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.reservations != 'undefined' ? salesAdmin.salesInfo.reservations : reservations;
				// var collated = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.reservationCollation != 'undefined' ? salesAdmin.salesInfo.reservationCollation : reservationCollation;
				// var reservation = res[index];
				
				var currentdate = new Date(); 
				var currentYear = parseInt(currentdate.getFullYear());

				var dataPack = {bre: null,
								profileData: null,
								amData: null,
								extraData: null,
								notes: null,
								reservation: null
							};

				salesAdmin.setReservationMeta(dataPack);
				var reservation = dataPack.reservation;
				var bre = dataPack.bre;
				var profileData = dataPack.profileData;
				var amData = dataPack.amData;
				var extraData = dataPack.extraData;

				if (amData) {
					salesAdmin.currentResvOrigLSCount = amData.item.length;
					salesAdmin.originalAmData = JSON.parse(JSON.stringify(amData)); // makes copy of it
				}

				var h = '<div id="headerDiv">'+
							'<table id="header">'+
								'<tbody>'+
									'<tr>'+
										'<td id="cell-label"><label>First</label></td><td id="cell-input"><input type="text" name="first_name" value="'+reservation.first_name+'" /></td>'+
									'</tr>'+
									'<tr>'+
										'<td id="cell-label"><label>Last</label></td><td id="cell-input"><input type="text" name="last_name" value="'+reservation.last_name+'" /></td>'+
									'</tr>'+
									'<tr>'+
										'<td id="cell-label"><label>Email</label></td><td id="cell-input"><input type="text" name="email" value="'+reservation.email+'" /></td>'+
									'</tr>'+
									'<tr>'+
										'<td id="cell-label"><label>Secondary Email</label></td><td id="cell-input"><input type="text" name="contact_email" value="'+(profileData && profileData.contact_email ? profileData.contact_email : '')+'" /></td>'+
									'</tr>'+
									'<tr>'+
										'<td id="cell-label"><label>Phone</label></td><td id="cell-input"><input type="text" name="phone" value="'+reservation.phone+'" /></td>'+
									'</tr>'+
									'<tr>'+
										'<td id="cell-label"><label>Mobile</label></td><td id="cell-input"><input type="text" name="mobile" value="'+(extraData ? extraData.mobile : '')+'" /></td>'+
									'</tr>'+
								'</tbody>'+
							'</table>'+
						'</div>';

				h += 	'<div id="extraDataDiv">'+
							'<table id="extraData">'+
								'<tbody>'+
									'<tr>'+
										'<td><label>Company</label></td><td><input type="text" name="company" value="'+(extraData ? extraData.company : '')+'" /></td>'+
										'<td><label>Website</label></td><td><input type="text" name="website" value="'+(extraData ? extraData.website : '')+'" /></td>'+
									'</tr>'+
									'<tr>'+
										'<td><label>Street</label></td><td><input type="text" name="street_address" value="'+(extraData ? extraData.street_address : '')+'" /></td>'+
										'<td><label>City</label></td><td><input type="text" name="city" value="'+(extraData ? extraData.city : '')+'" /></td>'+
									'</tr>'+
									'<tr>'+
										'<td><label>State</label></td><td><input type="text" name="state" value="'+(extraData ? extraData.state : '')+'" /></td>'+
										'<td><label>Zip</label></td><td><input type="text" name="zip" value="'+(extraData ? extraData.zip : '')+'" /></td>'+
									'</tr>'+
									'<tr>'+
										'<td><label>Country</label></td><td><input type="text" name="country" value="'+(extraData ? extraData.country : '')+'" /></td>'+
										'<td><label><strong>Portal</strong></label></td><td><input type="text" name="portal" value="'+(typeof reservation.portal != 'undefined' && reservation.portal != null ? reservation.portal : '')+'" /></td>'+
									'</tr>'+
								'</tbody>'+
							'</table>'+
						'</div>';

				h += 	'<div id="profileDiv">'+
							'<table id="profileData">'+
								'<tbody>'+
									'<tr>'+
										'<td><label>BRE</label></td><td><input type="text" name="BRE" value="'+(bre ? bre.BRE : '')+'" /></td>'+
									'</tr>'+
									'<tr>'+
										'<td><label>Preferred Display Name</label></td><td><input type="text" name="preferredName" value="'+(profileData ? (typeof profileData.first_name != 'undefined' && profileData.first_name.length ? profileData.first_name : '')+(typeof profileData.last_name != 'undefined' && profileData.last_name.length ? (typeof profileData.first_name != 'undefined' && profileData.first_name.length ? ' ' : '')+profileData.last_name : '') : '')+'" /></td>'+
									'</tr>'+
									'<tr>'+
										'<td><label>Year Licensed</label></td><td><input type="text" name="year" value="'+(profileData ? (profileData.year ? profileData.year : '') : '')+'" /></td>'+
									'</tr>'+
									'<tr>'+
										'<td><label>Years lived in Service Area</label></td><td><input type="text" name="inArea" value="'+(profileData ? currentYear - parseInt(profileData.inArea) + 1 : 1)+'" /></td>'+
									'</tr>'+
									'<tr>'+
										'<td><label>Transactions/year</label></td><td><input type="text" name="sold" value="'+(profileData && parseInt(profileData.sold) ? profileData.sold : 1)+'" /></td>'+
									'</tr>'+
								'</tbody>'+
							'</table>'+
						'</div>';

				h += 	'<span class="line"></span>'+
						'<div id="aboutDiv">'+
							'<table id="extraData">'+
								'<tbody>'+
									'<tr>'+
										'<td id="aboutLabel"><label>About</label></td>'+
										'<td id="aboutText"><textarea id="about" name="about" placeholder="Please enter information about the seller that&#39;ll go into their About section of their profile.">'+(extraData ? extraData.about : '')+'</textarea></td>' +
									'</tr>'+
								'</tbody>'+
							'</table>'+
						'</div>';

				h +=	'<span class="line"></span>'+
						'<div class="agent-match-div">' +
							'<div class="maincontent">' +
								'<div id="headers"></div>' +
								'<div class="define-agent-match-container">' + '</div>'  +
								'<div id="more">' +
									'<a href="javascript:;" class="another entypo-down-circled"><span> Add Another</span></a>' +
								'</div>' +
							'</div>' +
						'</div>';

				if (!salesAdmin.map.hidden) {
					$('#messageDiv #message').html(""); // clear out
					ahtb.clear(); // just in case
					ahtb.push();
				}	

				ahtb.open({html: h,
						   width: 1150,
						   height: 450 + (10 * 30),
						   buttons:[{text:'Save to DB', action:function() {
						   		salesAdmin.list.gatherReservationData(true);
						   		ahtb.close( (!salesAdmin.map.hidden ? ahtb.pop : null) );
						   }},
						   {text:'Cancel', action:function() {
						   		ahtb.close( (!salesAdmin.map.hidden ? ahtb.pop : null) );
						   }}],
						   closed: function() {
						   		console.log("Reservation edit popup is closed");
						   		salesAdmin.list.gatherReservationData();						   		
						   },
						   opened: function() {
						   		$('[name=mobile], [name=phone]').mask('(999) 000-0000');
						   		$('#extraDataDiv tr td:nth-child(odd)').each(function() {
						   			$(this).css('width', '10%');
						   		})
						  
						   		$('#extraDataDiv tr td:nth-child(even)').each(function() {
						   			$(this).css('width', '40%');
						   		})

						   		$('td#aboutLabel').css('width','10%');
						   		$('td#aboutText').css('width','90%');

						   		var id = 0;
						   		if (amData) {
						   			for(var j in amData.item) {
						   				if (amData.item[j].location) {
											if (salesAdmin.matchCity(amData.item[j].location).length == 0) {
												cities[cities.length] = {label: amData.item[j].locationStr, value: amData.item[j].locationStr, id: parseInt(amData.item[j].location) };
											}
										}
										$('.agent-match-div .maincontent .define-agent-match-container').append(salesAdmin.agentMatchSection(j, amData.item[j]));
										salesAdmin.list.setupAutocomplete($('.define-agent-match-container #define-agent-match input.service_areas[for="'+j+'"]'));
										salesAdmin.matchDescWithProfile(j);
										id++;
						   			}
						   		}
						   		if (id == 0)
									salesAdmin.add_another();
								else
									salesAdmin.add_callbacks();

								$('.maincontent #more a.another').on('click', function() {
									salesAdmin.add_another();
								})
						   }});
			}

			this.createTable = function(index, type, type2, fromAgent) {
				var h = '';
				var fromNav = index > 10 && index < 20;
				var fromOptionSubmit = index >= 20;
				index = index % 10;
				fromAgent = typeof fromAgent == 'undefined' ? false : fromAgent == 'agent' ? true : false;

				var res = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.reservations != 'undefined' ? salesAdmin.salesInfo.reservations : reservations;
				var agents = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.activeAgents != 'undefined' ? salesAdmin.salesInfo.activeAgents : activeAgents;
				var collated = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.reservationCollation != 'undefined' ? salesAdmin.salesInfo.reservationCollation : reservationCollation;

				switch(index) {
					case TableType.ALL_AGENTS:
						if (typeof this.lists[index] == 'undefined') {
							h = '<div id="all-data-div">'+
									'<div id="all-reservations-div" class="details left">'+
										'<table id="outer"><tbody>'+
											'<tr>'+
												'<td><span>Reservations</span></td>'+
											'</tr>'+
											'<tr><td>'+
												'<table class="all-reservations">' +
													'<thead>' +
														'<tr>' +
															'<th scope="col" class="manage-column column-res-id"><a href="javascript:salesAdmin.sortId()" target="_blank">ID</a></th>' +
															'<th scope="col" class="manage-column column-res-seller-id"><a href="javascript:salesAdmin.sortId(1)" target="_blank">Seller ID</a></th>' +
															'<th scope="col" class="manage-column column-res-first"><a href="javascript:salesAdmin.sortFirst()" target="_blank">First</a></th>' +
															'<th scope="col" class="manage-column column-res-last"><a href="javascript:salesAdmin.sortLast()" target="_blank">Last</a></th>' +
															'<th scope="col" class="manage-column column-phone">Phone</th>' +
															'<th scope="col" class="manage-column column-specialty">City/Specialty</th>' +
														'</tr>' +
													'</thead>' +
													'<tbody>';
													for (var i in res) {
													h+=	'<tr>' +
															'<td>'+res[i].id+'</td>'+
															'<td>'+res[i].seller_id+'</td>'+
															'<td><a href="javascript:salesAdmin.list.editReservation('+i+');">'+res[i].first_name+'</a></td>'+
															'<td>'+res[i].last_name+'</td>'+
															'<td>'+res[i].phone+'</td>'+
															'<td>'+
																'<table id="specialty" style="margin:auto;">'+
																	'<tbody>';
																	for(var j in res[i].city) {
																	h+=	'<tr>' +
																			'<td><a href="javascript:salesAdmin.list.showTable('+TableType.CITY_AGENTS+','+i+','+j+');">'+res[i].city[j].locationStr+'</a></td>' +
																			'<td>'+
																				'<div id="specialties">';
																			for(var k in res[i].city[j].tags) {
																				h+=	'<span>'+res[i].city[j].tags[k].specialtyStr+'</span>';
																				h+= (k+1) != res[i].city[j].tags.length ? '<br/>' : '';
																			}
																			h+=	'</div>'+
																			'</td>'+
																		'</tr>';
																	}
																h+=	'</tbody>'+
																'</table>'+
															'</td>'+
														'</tr>';
													}
												h+= '</tbody>' +
												'</table>'+	
											'</td></tr>'+
										'</tbody></table>'+
									'</div>';
								h+= '<div id="all-agents-div" class="details right">'+
										'<table id="outer"><tbody>'+
											'<tr>'+
												'<td><span>Lifestyled Agents</span></td>'+
											'</tr>'+
											'<tr><td>'+
												'<table class="all-agents">' +
													'<thead>' +
														'<tr>' +
															'<th scope="col" class="manage-column column-res-id"><a href="javascript:salesAdmin.sortId()" target="_blank">ID</a></th>' +
															'<th scope="col" class="manage-column column-res-author-id"><a href="javascript:salesAdmin.sortId(1)" target="_blank">Seller ID</a></th>' +
															'<th scope="col" class="manage-column column-res-first"><a href="javascript:salesAdmin.sortFirst()" target="_blank">First</a></th>' +
															'<th scope="col" class="manage-column column-res-last"><a href="javascript:salesAdmin.sortLast()" target="_blank">Last</a></th>' +
															'<th scope="col" class="manage-column column-phone">Mobile</th>' +
															'<th scope="col" class="manage-column column-specialty">City/Specialty</th>' +
														'</tr>' +
													'</thead>' +
													'<tbody>';
													for (var i in agents) {
													h+=	'<tr>' +
															'<td>'+agents[i].id+'</td>'+
															'<td>'+agents[i].author_id+'</td>'+
															'<td>'+agents[i].first_name+'</td>'+
															'<td>'+agents[i].last_name+'</td>'+
															'<td>'+agents[i].mobile+'</td>'+
															'<td>'+
																'<table id="specialty" style="margin:auto;">'+
																	'<thead>'+
																		'<th scope="col" class="manage-column column-specialty-city">'+
																		'<th scope="col" class="manage-column column-specialty-tag">'+
																	'</thead>'+
																	'<tbody>';
																	for(var j in agents[i].city_tags) {
																	h+=	'<tr>' +
																			'<td><a href="javascript:salesAdmin.list.showTable('+TableType.CITY_AGENTS+','+i+','+j+",'agent'"+');">'+agents[i].city_tags[j].city+'</a></td>' +
																			'<td>'+
																				'<div id="specialties">';
																			for(var k in agents[i].city_tags[j].tags) {
																				h+=	'<span>'+agents[i].city_tags[j].tags[k].tag+'</span>';
																				h+= (k+1) != agents[i].city_tags[j].tags.length ? '<br/>' : '';
																			}
																			h+=	'</div>'+
																			'</td>'+
																		'</tr>';
																	}
																h+=	'</tbody>'+
																'</table>'+
															'</td>'+
														'</tr>';
													}
												h+=	'</tbody>'+
												'</table>'+
											'</td></tr>'+
										'</tbody></table>'+
									'</div>';
							h+= '</div>';
							this.lists[index] = {html: h,
												offsetLeft: 0,
												offsetRight: 0}
							$('ul.selector#list li a').prop('disabled', true);
						}
						else 
							$('ul.selector#list li a.right').prop('disabled', typeof this.lists[index+1] == 'undefined');
						break;
					case TableType.CITY_AGENTS:
						console.log("createTable - index:1, type:"+type+", type2:"+type2+", fromAgent:"+fromAgent);
						if (typeof this.lists[index] == 'undefined' ||
							fromOptionSubmit ||
							(!fromNav &&
							  ((!fromAgent &&
							   	 this.lists[index].html.indexOf(res[type].city[type2].locationStr) == -1) ||
							   (fromAgent &&
							   	 this.lists[index].html.indexOf(agents[type].city_tags[type2].city) == -1)) ) ) {
							if (typeof collated[type2] == 'undefined') {
								var msg = !fromOptionSubmit ? "City: "+(fromAgent ? agents[type].city_tags[type2].city : res[type].city[type2].locationStr)+" is not in the collated table data" :
															  "City: "+testCityName+" does not have any data";
								ahtb.alert(msg,
											{height: 150, width: 400} );
								if (fromOptionSubmit) {
									this.index = index;
									salesAdmin.map.show();
								}
								return;
							}
							if (!fromOptionSubmit) {
								if (!fromAgent)
									this.reservation = type;
								else
									this.agent = type;
							}
							this.location = type2;
							h = '<div id="lifestyles-div" class="details">'+
									'<span>City - '+collated[type2].locationStr+'</span>'+
									'<table class="city-specialties">' +
										'<thead>' +
											'<tr>' +
												'<th scope="col" class="manage-column column-res-tag">Tag</th>' +
												'<th scope="col" class="manage-column column-res-score">Score</th>' +
												'<th scope="col" class="manage-column column-res-count">Reservations</th>' +
												'<th scope="col" class="manage-column column-am-count">Agents</th>' +
											'</tr>' +
										'</thead>' +
										'<tbody>';
										for(var i in collated[type2].tags) {
										h+=	'<tr '+(typeof collated[type2].tags[i].score != 'undefined' && collated[type2].tags[i].score != null && parseInt(collated[type2].tags[i].score) == 1 ? 'style="background-color: red;"' : '' )+'>'+
												'<td><a href="javascript:salesAdmin.list.showTable('+TableType.TAGGED_AGENTS+','+type2+','+i+');" >'+collated[type2].tags[i].specialtyStr+'</a></td>';
											if (typeof collated[type2].tags[i].score != 'undefined' && collated[type2].tags[i].score != null) // && parseInt(collated[type2].tags[i].score) == 1)
											h+= '<td><a href="javascript:salesAdmin.updateCity('+type+','+type2+','+i+','+fromAgent+')";>'+collated[type2].tags[i].score+'</a></td>';
											else
											h+= '<td>'+(typeof collated[type2].tags[i].score != 'undefined' && collated[type2].tags[i].score != null ? collated[type2].tags[i].score : 'N/A' )+'</td>';
											h+= '<td>'+collated[type2].tags[i].reservations.length+'</td>'+
												'<td>'+collated[type2].tags[i].agents.length+'</td>'+
											'</tr>';
										}
									h+=	'</tbody>'+
									'</table>'+
								'</div>';
							this.lists[index] = {html: h,
												 offset: 0}
							$('ul.selector#list li a.left').prop('disabled', false);
							$('ul.selector#list li a.right').prop('disabled', true);
						}
						else 
							$('ul.selector#list li a.right').prop('disabled', typeof this.lists[index+1] == 'undefined');
						break;
					case TableType.TAGGED_AGENTS:
						console.log("createTable - index:2, type:"+type+", type2:"+type2);
						if (!fromNav &&
							 typeof collated[type].tags[type2] == 'undefined') {
							ahtb.alert("City: "+res[i].city[type].locationStr+" does not have specialty:"+type2+" in the collated table data",
										{height: 150, width: 400} );
							return;
						}
						if (typeof this.lists[index] == 'undefined' ||
							(!fromNav &&
							  this.lists[index].html.indexOf(collated[type].tags[type2].specialtyStr) == -1) ) {
							this.location = type;
							this.specialty = type2;
						h = '<span>Specialty - '+collated[type].tags[type2].specialtyStr+', City - '+collated[type].locationStr+'</span>'+
								'<div id="city-details">'+
									'<div id="city-reservations-div" class="details left">'+
										'<table id="outer"><tbody>'+
											'<tr>'+
												'<td><span>Reservations</span></td>'+
											'</tr>'+
											'<tr><td>'+
												'<table class="city-reservations">' +
													'<thead>' +
														'<tr>' +
															'<th scope="col" class="manage-column column-res-id">ID</th>' +
															'<th scope="col" class="manage-column column-res-seller-id">Seller ID</th>' +
															'<th scope="col" class="manage-column column-res-first">First</th>' +
															'<th scope="col" class="manage-column column-res-last">Last</th>' +
															'<th scope="col" class="manage-column column-phone">Phone</th>' +
															'<th scope="col" class="manage-column column-email">Email</th>' +
														'</tr>' +
													'</thead>' +
													'<tbody>';
													for(var i in collated[this.location].tags[type2].reservations) {
														var reservation = res[collated[this.location].tags[type2].reservations[i]];
													h+=	'<tr>' +
															'<td>'+reservation.id+'</td>'+
															'<td>'+reservation.seller_id+'</td>'+
															'<td><a href="javascript:salesAdmin.list.editReservation('+i+','+this.location+','+type2+');">'+reservation.first_name+'</a></td>'+
															'<td>'+reservation.first_name+'</td>'+
															'<td>'+reservation.last_name+'</td>'+
															'<td>'+reservation.phone+'</td>'+
															'<td>'+reservation.email+'</td>'+
														'</tr>';
													}
												h+=	'</tbody>'+
												'</table>'+
											'</td></tr>'+
										'</tbody></table>'+
									'</div>';
								h+= '<div id="city-agents-div" class="details right">'+
										'<table id="outer"><tbody>'+
											'<tr>'+
												'<td><span>Lifestyled Agents</span></td>'+
											'</tr>'+
											'<tr><td>'+
												'<table class="city-agents">' +
													'<thead>' +
														'<tr>' +
															'<th scope="col" class="manage-column column-res-id">ID</th>' +
															'<th scope="col" class="manage-column column-res-author-id">Author ID</th>' +
															'<th scope="col" class="manage-column column-res-first">First</th>' +
															'<th scope="col" class="manage-column column-res-last">Last</th>' +
															'<th scope="col" class="manage-column column-phone">Mobile</th>' +
															'<th scope="col" class="manage-column column-email">Email</th>' +
														'</tr>' +
													'</thead>' +
													'<tbody>';
													for(var i in collated[this.location].tags[type2].agents) {
													h+=	'<tr>' +
															'<td>'+collated[this.location].tags[type2].agents[i].id+'</td>'+
															'<td>'+collated[this.location].tags[type2].agents[i].author_id+'</td>'+
															'<td>'+collated[this.location].tags[type2].agents[i].first_name+'</td>'+
															'<td>'+collated[this.location].tags[type2].agents[i].last_name+'</td>'+
															'<td>'+collated[this.location].tags[type2].agents[i].mobile+'</td>'+
															'<td>'+collated[this.location].tags[type2].agents[i].email+'</td>'+
														'</tr>';
													}
												h+=	'</tbody>'+
												'</table>'+
											'</td></tr>'+
										'</tbody></table>'+
									'</div>';
							h+= '</div>';
							this.lists[index] = {html: h,
												 offsetLeft: 0,
												 offsetRight: 0}
						}
						$('ul.selector#list li a.left').prop('disabled', false);
						$('ul.selector#list li a.right').prop('disabled', true);
						break;
				}
				if (typeof this.lists[this.index] != 'undefined') {
					switch(this.index) {
						case TableType.ALL_AGENTS:
						case TableType.TAGGED_AGENTS:
							this.lists[this.index].offsetLeft = $('#reservations-list-div .details.left').scrollTop();
							this.lists[this.index].offsetRight = $('#reservations-list-div .details.right').scrollTop();
							break;
						case TableType.CITY_AGENTS:
							this.lists[this.index].offset = $('#reservations-list-div .details').scrollTop();
							break;
					}
				}
				this.index = index;
				// $('#reservations-list-div').html(this.lists[index].html);	
				// $('#reservations-list-div').scrollTop(this.lists[index].offset);		
			}
		}

		this.getMousePosition = function (e) {
		    var posX = 0, posY = 0;
		    e = e || window.event;
		    if (typeof e.pageX !== "undefined") {
		      posX = e.pageX;
		      posY = e.pageY;
		    } else if (typeof e.clientX !== "undefined") { // MSIE
		      posX = e.clientX + scroll.x;
		      posY = e.clientY + scroll.y;
		    }
		    return {
		      left: posX,
		      top: posY
		    };
		  };

		this.getElementPosition = function (h) {
		   	var posX = h.offsetLeft;
		    var posY = h.offsetTop;
		    var parent = h.offsetParent;
		    // Add offsets for all ancestors in the hierarchy
		    while (parent !== null) {
		      // Adjust for scrolling elements which may affect the map position.
		      //
		      // See http://www.howtocreate.co.uk/tutorials/javascript/browserspecific
		      //
		      // "...make sure that every element [on a Web page] with an overflow
		      // of anything other than visible also has a position style set to
		      // something other than the default static..."
		      if (parent !== document.body && parent !== document.documentElement) {
		        posX -= parent.scrollLeft;
		        posY -= parent.scrollTop;
		      }
		      // See http://groups.google.com/group/google-maps-js-api-v3/browse_thread/thread/4cb86c0c1037a5e5
		      // Example: http://notebook.kulchenko.com/maps/gridmove
		      var m = parent;
		      // This is the "normal" way to get offset information:
		      var moffx = m.offsetLeft;
		      var moffy = m.offsetTop;
		      // This covers those cases where a transform is used:
		      if (!moffx && !moffy && window.getComputedStyle) {
		        var matrix = document.defaultView.getComputedStyle(m, null).MozTransform ||
		        document.defaultView.getComputedStyle(m, null).WebkitTransform;
		        if (matrix) {
		          if (typeof matrix === "string") {
		            var parms = matrix.split(",");
		            moffx += parseInt(parms[4], 10) || 0;
		            moffy += parseInt(parms[5], 10) || 0;
		          }
		        }
		      }
		      posX += moffx;
		      posY += moffy;
		      parent = parent.offsetParent;
		    }
		    return {
		      left: posX,
		      top: posY
		    };
    	}

		this.getMousePoint_ = function (e) {
		    var mousePosn = this.getMousePosition(e);
		    mousePosn.left = mousePosn.left;
		    mousePosn.top = mousePosn.top;
		    var p = new google.maps.Point();
		    p.x = mousePosn.left - salesAdmin.mapPosn_.left;
		    p.y = mousePosn.top - salesAdmin.mapPosn_.top + 350;
		    return p;
		};

		this.onMouseDown_ = function (e) {
		    if (salesAdmin.map.map) {
		      salesAdmin.mapPosn_ = this.getElementPosition(salesAdmin.map.map.getDiv());
		      this.dragging_ = true;
		      var mousey = typeof e.Ua != 'undefined' ? e.Ua : e.eb;
		      salesAdmin.mousePt = salesAdmin.getMousePoint_(mousey);
		 	}
		 }

		this.zoneSelect = function(zone) {
			this.hotkey = zone.hotkey; // shift, alt, or ctrl, but only one is valid and determined by what passed into enableKeyDragSelect().
										// by default, the hotKey will always be 'shift'
			console.log("zoneSelect - hotkey:"+this.hotkey+", specifier:"+zone.specifier);
			bounds = JSON.parse( JSON.stringify(zone.bounds) );
			salesAdmin.currentZone = bounds;
			if (salesAdmin.currentZoneRectangle) {
				salesAdmin.currentZoneRectangle.setMap(null);
				delete salesAdmin.currentZoneRectangle;
			}
	        salesAdmin.currentZoneRectangle = new google.maps.Rectangle({
	          strokeColor: '#0000FF',
	          strokeOpacity: 0.2,
	          strokeWeight: 2,
	          fillColor: '#88AAAA',
	          fillOpacity: 0.1,
	          map: salesAdmin.map.map,
	          bounds: {
	            north: bounds.north,
	            south: bounds.south,
	            east:  bounds.east,
	            west:  bounds.west
	          }
	        });

	        google.maps.event.addListener(salesAdmin.currentZoneRectangle, 'rightclick', function (e) {
			 	// e = e || window.event;
			 	// e.preventDefault();
			 	var mousey = typeof e.Ua != 'undefined' ? e.Ua : e.eb;
			 	if (mousey.which == 3) {
					console.log('right mouse clicked');
					// var offsetY = $('div#reservation-map').position().top;
		   //          var posX = e.pixel.x + 'px';
		   //          var posY = (e.pixel.y+offsetY+350) + 'px';

		   			salesAdmin.onMouseDown_(e);

					var ele = $('div.context-menu');
					ele.css('left', salesAdmin.mousePt.x);
					$('div.context-menu').css('top', salesAdmin.mousePt.y);
					$('div.context-menu').show();
				}
			});

			switch(zone.specifier) {
				case 65: // 'A'
					console.log("zoneSelect:"+zone.specifier+" is agent");
					this.gatherData(bounds, 'gather-agents');
					break;
				case 67: // 'C'
					console.log("zoneSelect:"+zone.specifier+" is city");
					this.gatherData(bounds, 'gather-locations');
					break;
				case 76: // 'L'
					console.log("zoneSelect:"+zone.specifier+" is listing");
					this.gatherData(bounds, 'gather-listings');
					break;
				case 88: // 'x'
					console.log("zoneSelect:"+zone.specifier+" is listing exact");
					this.gatherData(bounds, 'gather-listings-exact');
					break;
				default: // zoom
					var z = this.map.map.getZoom();
        			this.map.map.fitBounds(zone.bounds);
			        if (this.map.map.getZoom() < z) {
			          	this.map.map.setZoom(z);
					}
					break;
			}
		}

		this.gatherData = function(zone, what) {
			// var bounds = {west: zone.getSouthWest().lng(),
			// 			  east: zone.getNorthEast().lng(),
			// 			  south: zone.getSouthWest().lat(),
			// 			  north: zone.getNorthEast().lat()};
			$('.spin-wrap').show();
			if (salesAdmin.gatheredZoneRectangle) {
				salesAdmin.gatheredZoneRectangle.setMap(null);
				delete salesAdmin.gatheredZoneRectangle;
			}
			salesAdmin.gatheredZoneRectangle = new google.maps.Rectangle({
	          strokeColor: '#FF0000',
	          strokeOpacity: 0.1,
	          strokeWeight: 2,
	          fillColor: '#FF0000',
	          fillOpacity: 0.1,
	          map: salesAdmin.map.map,
	          bounds: {
	            north: zone.north,
	            south: zone.south,
	            east:  zone.east,
	            west:  zone.west
	          }
	        });
	        google.maps.event.addListener(salesAdmin.gatheredZoneRectangle, 'rightclick', function (e) {
			 	// e = e || window.event;
			 	// e.preventDefault();
			 	var mousey = typeof e.Ua != 'undefined' ? e.Ua : e.eb;
				if (mousey.which == 3) {
					console.log('right mouse clicked');
					// var offsetY = $('div#reservation-map').position().top;
		   //          var posX = e.pixel.x + 'px';
		   //          var posY = (e.pixel.y+offsetY+350) + 'px';

		   			salesAdmin.onMouseDown_(e);

					var ele = $('div.context-menu');
					ele.css('left', salesAdmin.mousePt.x);
					$('div.context-menu').css('top', salesAdmin.mousePt.y);
					$('div.context-menu').show();
				}
			});

			var query = {query: what,
						 data: {bounds: zone},
						 done: function(d) {
						 	$('.spin-wrap').hide();
						 	salesAdmin.showGatheredData(d, what);
						 },
						 error: function(d) {
						 	$('.spin-wrap').hide();
						 	var msg = typeof d == 'string' ? d : "We're very sorry, there was a system failure.";
						 	if (typeof d == 'object') {
								if (typeof d.msg != 'undefined')
									msg += "  "+d.msg;
								else if (typeof d.responseText != 'undefined' &&
										d.responseText.length)
									msg += "  "+d.responseText;
								else if (typeof d.statusText != 'undefined' &&
										d.statusText.length &&
										d.statusText != 'error')
									msg += "  "+d.statusText;
								else if (typeof d.status != 'undefined' &&
										 typeof d.status == 'string' &&
										 d.status.length)
									msg += "  "+d.status;
							}
							ahtb.alert(msg);
						 }};
			this.DB(query);
		}

		this.showGatheredData = function(d, what) {
			if (this.gatheredDataMarkers) {
				// clear out existing markers
				for (var i in this.gatheredDataMarkers) {
					this.gatheredDataMarkers[i].setMap(null);
					delete this.gatheredDataMarkers[i];
					this.gatheredDataMarkers[i] = null;
				}
			}
			this.gatheredDataMarkers = [];

			if (this.gatheredListingMarkers) {
				// clear out existing markers
				for (var i in this.gatheredListingMarkers) {
					this.gatheredListingMarkers[i].setMap(null);
					delete this.gatheredListingMarkers[i];
					this.gatheredListingMarkers[i] = null;
				}
			}
			this.gatheredListingMarkers = [];

			if (this.gatheredDataInfoWindows) {
				for (var i in this.gatheredDataInfoWindows) {
					this.gatheredDataInfoWindows[i].close();
					this.gatheredDataInfoWindows[i].setMap(null);
					delete this.gatheredDataInfoWindows[i];
					this.gatheredDataInfoWindows[i] = null;
				}
			}
			this.gatheredDataInfoWindows = [];

			if (this.gatheredListingInfoWindows) {
				for (var i in this.gatheredListingInfoWindows) {
					this.gatheredListingInfoWindows[i].close();
					this.gatheredListingInfoWindows[i].setMap(null);
					delete this.gatheredListingInfoWindows[i];
					this.gatheredListingInfoWindows[i] = null;
				}
			}
			this.gatheredListingInfoWindows = [];

			this.gatheredData = d;

			// now deal with the array of objects, but may also be empty/undefined
			salesAdmin.createGatheredMarkers(d, what);
		}

		this.createGatheredMarkers = function(d, what) {
			if (typeof google != 'undefined' &&
				typeof salesAdmin.map.map != 'undefined') {
				console.log("createGatheredMarkers - entered for "+what);
				var counter = 1;
				switch(what) {
					case 'gather-agents':
						for(var i in d)
							if ( typeof d[i].agents != 'undefined')
								salesAdmin.createGatheredCitiesMarker(d[i], GatheredTypes.Agents, counter++);
						break;
					case 'gather-listings':
					case 'gather-listings-exact':
						for(var i in d) {
							salesAdmin.createGatheredCitiesMarker(d[i], GatheredTypes.Listings, counter++);
							if ( typeof d[i].listings != 'undefined' &&
								 length(d[i].listings) )
								for(var j in d[i].listings)
									salesAdmin.createGatheredListingsMarker(d[i], d[i].listings[j], GatheredTypes.Listings, counter++);
						}
						break;
					case 'gather-locations':
						for(var i in d)
							salesAdmin.createGatheredCitiesMarker(d[i], GatheredTypes.Cities, counter++);
						break;
				}
			}
		}

		this.updateGatheredCity = function(cityId, tagId, what) {
			var collated = salesAdmin.gatheredData;
			var h = '<div id="updateCity">'+
						'<span>'+collated[cityId].locationStr+' - '+collated[cityId].tags[tagId].tag+'</span>'+
						'<div id="scoring">'+
							'<label>Slide to a new score:</label>&nbsp;'+
							'<span id="scoreValue">'+collated[cityId].tags[tagId].score+'</span>&nbsp;<div id="scoreSlider" />'+
						'</div>' +
						'<div id="controls">'+
							'<button id="ok">OK</button>'+
						'</div>'+
					'</div>';
			var score = collated[cityId].tags[tagId].score;
			ahtb.open({	html: h,
					   	width: 450,
					 	height: 155,
					 	hideSubmit: true,
					 	opened: function() {
					 		$('#ok').on('click', function() {
					 			ahtb.close();
					 		})
					 		$( "div#scoreSlider" ).slider({
						      	range: false,
						      	min: 1,
						      	max: 10,
						      	value: score,
						      	stop: function( event, ui ) {							      	
						        	score = ui.value;
						        	$('span#scoreValue').html(ui.value);
						      	},
						      	slide: function( event, ui) {
						      		$('span#scoreValue').html(ui.value);
						      	}
						    });
					 	},
					 	closed: function() {
					 		if (score != collated[cityId].tags[tagId].score) {
					 			$('.spin-wrap').show();
					 			var x = {query:'update-city-tag', 
										data: {	city_id: cityId,
						                  		tag_id: tagId,
						                  		score: score },
						                done: 	function(d){
						                	$('.spin-wrap').hide();
											salesAdmin.gatheredData[cityId].tags[tagId].score = score;
											salesAdmin.createGatheredCitiesMarker(salesAdmin.gatheredData[cityId], what, 1);

											if (salesAdmin.updateReservationCollationCity(salesAdmin.gatheredData[cityId]))
												salesAdmin.map.createMarker(reservationCollation[cityId], cityId);
										},
										error:  function(d){
											$('.spin-wrap').hide();
											console.log("update-city-tag errored with "+(typeof d != 'undefined' ? (typeof d == 'string' ? d : JSON.stringify(d)) : 'unknown'));
											ahtb.alert("Failed to update "+collated[cityId].locationStr,
														{width: 450, height: 150});
										},
										fail: 	function(d){
											$('.spin-wrap').hide();
											ahtb.alert("Failed to update "+collated[cityId].locationStr,
												{width: 450, height: 150});
										}
									};
					 			salesAdmin.DB(x);
					 		}
						}
					})
		}

		this.applyToAllGatheredCities = function(cityId, tagId, whatGatheredType) {
			var collated = salesAdmin.gatheredData;
			var score = collated[cityId].tags[tagId].score;
			var ids = Object.keys(this.gatheredData);
			$('.spin-wrap').show();
			var x = {query:'update-cities-tag', 
					data: {city_ids: ids,
                  		  	tag_id: tagId,
                  		  	tag: collated[cityId].tags[tagId].tag,
                  		  	score: score,
                  		  	what: 'gather-'+whatGatheredType,
                  		  	bounds: salesAdmin.currentZone },
                  	done: 	function(d){
                  				$('.spin-wrap').hide();
								salesAdmin.gatheredData = d.cities;
								salesAdmin.showGatheredData(salesAdmin.gatheredData, 'gather-'+whatGatheredType);
								var msg = "Updated:"+d.updated+", added:"+d.added+", unchanged:"+d.unchanged;
								ahtb.alert(msg);

								for(var i in salesAdmin.gatheredData) {
									var city_id = i;
									if (salesAdmin.updateReservationCollationCity(salesAdmin.gatheredData[city_id]))
										salesAdmin.map.createMarker(reservationCollation[city_id], city_id);
								}
					},
					error:  function(d) {
								$('.spin-wrap').hide();
								console.log("update-cities-tag errored with "+(typeof d != 'undefined' ? (typeof d == 'string' ? d : JSON.stringify(d)) : 'unknown'));
								ahtb.alert("Failed to update all cities with tag:"+tagId+", with score:"+score,
														{width: 450, height: 150});
					},
					fail: 	function(d){
								$('.spin-wrap').hide();
								ahtb.alert("Failed to update all cities with tag:"+tagId+", with score:"+score,
									{width: 450, height: 150});
							}
					};
			salesAdmin.DB(x);
		}

		this.applyToAllGatheredListings = function(cityId, listingId, tagId, whatGatheredType) {
			var collated = salesAdmin.gatheredData;
			var city = collated[cityId];
			var ids = Object.keys(city.listings);
			$('.spin-wrap').show();
			var x = {query:'update-listings-tag', 
					data: {listing_ids: ids,
                  		  	tag_id: tagId,
                  		  	tag: city.listings[listingId].tags[tagId].tag,
                  		  	city_id: cityId,
                  		  	what: 'gather-'+whatGatheredType,
                  		  	bounds: salesAdmin.currentZone },
                  	done: 	function(d){
                  				$('.spin-wrap').hide();
                  				var counter = 1;
								salesAdmin.gatheredData[cityId] = d.city;
								for(var j in d.city.listings)
									salesAdmin.createGatheredListingsMarker(d.city, d.city.listings[j], GatheredTypes.Listings, counter++);

								var msg = "expectedToAdd:"+d.expectedToAdd+", added:"+d.added+", unchanged:"+d.unchanged;
								ahtb.alert(msg);
					},
					error:  function(d) {
								$('.spin-wrap').hide();
								console.log("update-listings-tag errored with "+(typeof d != 'undefined' ? (typeof d == 'string' ? d : JSON.stringify(d)) : 'unknown'));
								ahtb.alert("Failed to update all listings with tag:"+tagId,
														{width: 450, height: 150});
					},
					fail: 	function(d){
								$('.spin-wrap').hide();
								ahtb.alert("Failed to update all listings with tag:"+tagId,
									{width: 450, height: 150});
							}
					};
			salesAdmin.DB(x);
		}

		this.addToActiveTags = function(tagList, tagId, score) {
			if ( typeof salesAdmin.activeTags[tagId] == 'undefined' ||
				!salesAdmin.activeTags[tagId] ) // could be null 
      			salesAdmin.activeTags[tagId] = {tag_id: tagId,
      											tag: tagList[tagId].tag};

      		if (salesAdmin.activeMode.indexOf('location') != -1 ||
      			salesAdmin.activeMode.indexOf('zone-city') != -1) {
      			salesAdmin.activeTags[tagId].type = 1;
      			salesAdmin.activeTags[tagId].score = score;
      			salesAdmin.activeTags[tagId].city_id = salesAdmin.currentGatheredCity ? salesAdmin.currentGatheredCity.id : 0;
      		}	
      		else { // then doing 'listings'
      			salesAdmin.activeTags[tagId].type = 0;
      			salesAdmin.activeTags[tagId].listing_id = self.activeMode.indexOf('zone-listing') == -1 && salesAdmin.currentGatheredListing ? salesAdmin.currentGatheredListing.id : 0;
      		}
		}

		this.removeFromActiveTags = function(tagId) {
			if ( typeof salesAdmin.activeTags[tagId] == 'undefined') 
				return;

			// do a looping reassign instead of using splie() or delete()
			// since the activeTags are associated array, looping to find the index would be the same as
			// recreating a new array with the one element deleted.  So no benefit with splice().  
			// delete will leave an 'undefined' element in the array
			var x = [];
			for(var i in salesAdmin.activeTags)
				if (i != tagId)
					x[i] = salesAdmin.activeTags[i];

			salesAdmin.activeTags = x;
		}

		this.updateReservationCollationCity = function(city) {
			var city_id = city.id;
			if (typeof reservationCollation[city_id] == 'undefined' ||
				!reservationCollation[city_id]) 
				return false;

			var incomingCityTags = [];
			for(var i in city.tags)
				incomingCityTags[i] = city.tags[i];


			var resCity = reservationCollation[city_id];
			var resCityTags = [];
			for(var i in resCity.tags)
				resCityTags[i] = resCity.tags[i];

			var incomingCityTagsThatMapToReservation = incomingCityTags.filter( function(tag) { 
				return reservationCityTags.indexOf( parseInt(tag.tag_id) ) > -1; 
			});
			if ( incomingCityTagsThatMapToReservation.length ) { // then tags in this gathered city has tags that are reservation compatible
				// now compare the incoming city tags with resCity's tags and see if we can update any of the ones in resCity
				var resCityTagKeys = Object.keys(resCityTags); // current tags in this reservation city
				// find tags that are in the reservation and also in tags from gathered city that can be part of a reservation's tag list
				var overlappedTags = incomingCityTagsThatMapToReservation.filter( function(tag) { 
					return resCityTagKeys.indexOf( tag.tag_id ) > -1; 
				});
				if (overlappedTags.length) {
					for(var i in overlappedTags) {
						var tagId = overlappedTags[i].tag_id;
						if (resCity.tags[tagId].score != overlappedTags[i].score)
							resCity.tags[tagId].score = overlappedTags[i].score;
					}

					var needScoring = false;
					var hasScoring = false;
					for(var i in resCity.tags)
						if (typeof resCity.tags[i].score != 'undefined') {
							if (resCity.tags[i].score == 1)
								needScoring = true;
							else
								hasScoring = true;
						}

					var currentState = reservationCollation[city_id].tag_state;
					if (needScoring)
						reservationCollation[city_id].tag_state = hasScoring ? CityTagState.TAGS_HAVE_SOME : CityTagState.TAGS_NEED_ALL;
					else
						reservationCollation[city_id].tag_state = CityTagState.TAGS_HAVE_ALL;

					return currentState != reservationCollation[city_id].tag_state;  // true if different
				}
			}

			return false;
		}

		this.saveTagSelection = function() {
			self = salesAdmin;
			var tags_categories = self.activeCategory;
			var collated = self.gatheredData;

			$('.tag-category, .tags-list').removeClass('active');
			$('.tags-list li').each(function(){
				var tagId = parseInt($(this).attr('tag-id'));
				if (self.activeMode.indexOf('listing') != -1 ||
					self.activeMode.indexOf('zone-listing') != -1) {
					if ($(this).hasClass('active'))
						salesAdmin.addToActiveTags( allListingTags, tagId);
					else
						salesAdmin.removeFromActiveTags(tagId);
				}
				else {
					var ele = $(this).find('#scoreSlider[for="'+tagId+'"]');
					if (ele.length) {
						var value = ele.slider('option', 'value');
						if (value)
							salesAdmin.addToActiveTags( allCityTags, tagId, value );
						else
							salesAdmin.removeFromActiveTags(tagId);
					}
				}
			});

			var ele = $('input#dozone');
			var dozone = ele.length ? ele.prop('checked') : false;


			var query = '';
			var whatGatheredType = '';
			var x = {data:{}};
			switch(self.activeMode) {
				case 'location':
					x.query = 'add-city-tags';
					x.data.city_id = self.currentGatheredCity.id;
					whatGatheredType = 'locations';
					var city_id = self.currentGatheredCity.id;
					x.done = function(d){
						var msg = "For city:"+collated[city_id].locationStr+" - Added:"+d.added+", updated:"+d.updated+", removed:"+d.removed+", unchanged:"+d.unchanged;
						console.log(msg);
						ahtb.alert(msg);
						collated[city_id].tags = self.activeTags;
						salesAdmin.createGatheredCitiesMarker(collated[city_id], whatGatheredType, 1);
						if (salesAdmin.updateReservationCollationCity(collated[city_id]))
							salesAdmin.map.createMarker(reservationCollation[city_id], city_id);

					};
					x.error =function(d){
						console.log("add-city-tags errored with "+(typeof d != 'undefined' ? (typeof d == 'string' ? d : JSON.stringify(d)) : 'unknown'));
						var keys = Object.keys(self.activeTags);
						ahtb.alert("Failed to add to "+collated[city_id].locationStr+", tagList:"+kyes.join(),
									{width: 450, height: 150});
					};
					x.fail = function(d){
						var keys = Object.keys(self.activeTags);
						ahtb.alert("Failed to add to "+collated[city_id].locationStr+", tagList:"+kyes.join(),
							{width: 450, height: 150});
					};
					break;
				case 'locations':
				case 'locations-specific':
					x.query = self.activeMode == 'locations' ? 'add-cities-tags' : 'add-specific-cities-tags';
					x.data.city_ids = Object.keys(this.gatheredData);
					x.data.dozone = dozone;
					whatGatheredType = 'locations';
					x.done = function(d){
						salesAdmin.gatheredData = d.cities;
						salesAdmin.showGatheredData(salesAdmin.gatheredData, 'gather-'+whatGatheredType);
						var msg = "Overall - Added:"+d.added+", updated:"+d.updated+", removed:"+d.removed+", unchanged:"+d.unchanged;
						var h = '<div id="results" style="overflow-y: scroll">'+
									'<span id="header">'+msg+'</span>'+
									'<ul id="cities">';
						for(var i in d.changes) {
							var city = collated[d.changes[i].city_id];
							msg = "For city:"+city.locationStr+" - Added:"+d.changes[i].added+", updated:"+d.changes[i].updated+", removed:"+d.changes[i].removed+", unchanged:"+d.changes[i].unchanged;
							h += '<li><span>'+msg+'</span></li>';
						}
						h+= '</ul></div>';

						ahtb.open({html: h,
								   width: 500,
								   height: 300});

						for(var i in salesAdmin.gatheredData) {
							var city_id = i;
							if (salesAdmin.updateReservationCollationCity(salesAdmin.gatheredData[city_id]))
								salesAdmin.map.createMarker(reservationCollation[city_id], city_id);
						}
					};
					x.error = function(d) {
						console.log("add-cities-tags errored with "+(typeof d != 'undefined' ? (typeof d == 'string' ? d : JSON.stringify(d)) : 'unknown'));
						var keys = Object.keys(self.activeTags);
						ahtb.alert("Failed to add all cities with tagList:"+ keys.join(),
												{width: 450, height: 150});
					};
					x.fail = function(d){
						var keys = Object.keys(self.activeTags);
						ahtb.alert("Failed to add all cities with tagList:"+keys.join(),
							{width: 450, height: 150});
					};
					break;

				case 'zone-city':
				case 'zone-listing':
					x.query = self.activeMode == 'zone-city' ? 'apply-zone-city' : 'apply-zone-listing';
					whatGatheredType = 'zone';
					x.done = function(d){
						var keys = Object.keys(self.activeTags);
						ahtb.alert("Added to zone, tagList:"+ keys.join(),
												{width: 450, height: 150});
						// salesAdmin.gatheredData = d.cities;
						// salesAdmin.showGatheredData(salesAdmin.gatheredData, 'gather-'+whatGatheredType);
						// var msg = "Overall - Added:"+d.added+", updated:"+d.updated+", removed:"+d.removed+", unchanged:"+d.unchanged;
						// var h = '<div id="results" style="overflow-y: scroll">'+
						// 			'<span id="header">'+msg+'</span>'+
						// 			'<ul id="cities">';
						// for(var i in d.changes) {
						// 	var city = collated[d.changes[i].city_id];
						// 	msg = "For city:"+city.locationStr+" - Added:"+d.changes[i].added+", updated:"+d.changes[i].updated+", removed:"+d.changes[i].removed+", unchanged:"+d.changes[i].unchanged;
						// 	h += '<li><span>'+msg+'</span></li>';
						// }
						// h+= '</ul></div>';

						// ahtb.open({html: h,
						// 		   width: 500,
						// 		   height: 300});

						// for(var i in salesAdmin.gatheredData) {
						// 	var city_id = i;
						// 	if (salesAdmin.updateReservationCollationCity(salesAdmin.gatheredData[city_id]))
						// 		salesAdmin.map.createMarker(reservationCollation[city_id], city_id);
						// }
					};
					x.error = function(d) {
						console.log("add-cities-tags errored with "+(typeof d != 'undefined' ? (typeof d == 'string' ? d : JSON.stringify(d)) : 'unknown'));
						var keys = Object.keys(self.activeTags);
						ahtb.alert("Failed to add all cities with tagList:"+ keys.join(),
												{width: 450, height: 150});
					};
					x.fail = function(d){
						var keys = Object.keys(self.activeTags);
						ahtb.alert("Failed to add all cities with tagList:"+keys.join(),
							{width: 450, height: 150});
					};
					break;

				case 'listing':
					x.query = 'add-listing-tags';
					whatGatheredType = 'listings';
					x.data.city_id = self.currentGatheredCity.id;
					var city_id = self.currentGatheredCity.id;
					x.data.listing_id = self.currentGatheredListing.id;
					var listing_id = self.currentGatheredListing.id;

					x.done = function(d){
          				var counter = 1;
						salesAdmin.gatheredData[city_id].listings[listing_id].tags = d.tags;
						salesAdmin.createGatheredListingsMarker(salesAdmin.gatheredData[city_id], salesAdmin.gatheredData[city_id].listings[listing_id], GatheredTypes.Listings, counter++);

						var msg = "removed:"+d.removed+", added:"+d.added+", unchanged:"+d.unchanged+", propertyTags:"+d.propertyTags+", cityListing:"+d.cityListing;
						ahtb.alert(msg);
					};
					x,error = function(d) {
						var keys = Object.keys(self.activeTags);
						console.log("add-listing-tags errored with "+(typeof d != 'undefined' ? (typeof d == 'string' ? d : JSON.stringify(d)) : 'unknown'));
						ahtb.alert("Failed to update listing with tagList:"+keys.join(),
												{width: 450, height: 150});
					};
					x.fail = function(d){
						var keys = Object.keys(self.activeTags);
						ahtb.alert("Failed to update listing with tagList:"+keys.join(),
							{width: 450, height: 150});
					};
					break;
				case 'listings':
					x.query = 'add-listings-tags';
					whatGatheredType = 'listings';
					x.data.city_id = self.currentGatheredCity.id;
					x.data.dozone = dozone;
					var city_id = self.currentGatheredCity.id;
					x.data.listing_ids = Object.keys(self.currentGatheredCity.listings);

					x.done = function(d){
          				var counter = 1;
          				for(var i in d.changes) {
          					salesAdmin.gatheredData[city_id].listings[d.changes[i].listing_id].tags = d.changes[i].tags;
          				}
						for(var j in salesAdmin.gatheredData[city_id].listings)
							salesAdmin.createGatheredListingsMarker(salesAdmin.gatheredData[city_id], salesAdmin.gatheredData[city_id].listings[j], GatheredTypes.Listings, counter++);

						var msg = "Overall - Added:"+d.added+", removed:"+d.removed+", unchanged:"+d.unchanged;
						var h = '<div id="results" style="overflow-y: scroll">'+
									'<span id="header">'+msg+'</span>'+
									'<ul id="listings">';
						for(var i in d.changes) {
							var listing = salesAdmin.gatheredData[city_id].listings[d.changes[i].listing_id];
							msg = "For listing:"+listing.street_address+" - Added:"+d.changes[i].added+", removed:"+d.changes[i].removed+", unchanged:"+d.changes[i].unchanged+", propertyTags:"+d.changes[i].propertyTags+", cityListing:"+d.changes[i].cityListing;
							h += '<li><span>'+msg+'</span></li>';
						}
						h+= '</ul></div>';

						ahtb.open({html: h,
								   width: 500,
								   height: 300});
					};
					x,error = function(d) {
						console.log("add-listing-tags-tags errored with "+(typeof d != 'undefined' ? (typeof d == 'string' ? d : JSON.stringify(d)) : 'unknown'));
						var keys = Object.keys(self.activeTags);
						ahtb.alert("Failed to update all listings with tagList:"+keys.join(),
												{width: 450, height: 150});
					};
					x.fail = function(d){
						var keys = Object.keys(self.activeTags);
						ahtb.alert("Failed to update all listings with tagList:"+keys.join(),
							{width: 450, height: 150});
					};
					break;
				default:
					ahtb.alert("Unknown activeMode:"+self.activeMode);
					return;
			}
			x.data.bounds = salesAdmin.currentZone;
			x.data.tags = [];
			for(var i in salesAdmin.activeTags)
				x.data.tags[x.data.tags.length] = salesAdmin.activeTags[i];
			x.data.what = 'gather-'+whatGatheredType;

			if (self.activeMode != 'locations') {
				$('.spin-wrap').show();
				salesAdmin.DB(x);
				$('div#tag-selections').removeClass('active');
			}
			else
				ahtb.open({html: '<span>Are you sure?  Tags from other gathered cities might get removed.',
						  width: 450,
						  height: 160,
						  buttons: [{text: 'OK', action: function() {
						  	$('.spin-wrap').show();
						  	salesAdmin.DB(x);
						  	ahtb.close();
						  	$('div#tag-selections').removeClass('active');
						  }},
						  {text: 'Cancel', action: function(){
						  	ahtb.close();
						  }}]})
		}

		this.setTagSelectorHandlers = function() {
			$('ul.tag-categories li').each(function(){
				$(this).hover(function(){
					var tags_categories = salesAdmin.activeCategory;
					for (var i in tags_categories) if (tags_categories[i].category.replace(' ','-') == $(this).attr('category')){
						var theCategory = $(this).attr('category');
						var theDesciption = tags_categories[i].description;
						if ($('.tag-definition h5').html() !== theCategory+' - ') $('.tag-definition').fadeOut(50,function(){
							$(this).children('h5').html(theCategory+' - ');
							$(this).children('span').html(theDesciption && theDesciption.length > 0 ? theDesciption : 'This tag category has no description.');
							$(this).fadeIn(50);
						});
						break;
					}
				},function(){});
				$(this).on('click',function(){
					$('.tag-category, .tags-list').removeClass('active');
					$('[category='+$(this).attr('category')+']').addClass('active');
				})
			});
			$('#tag-window .tags-list li.tag').each(function(){
				$(this).hover(function(){
					var tags_categories = salesAdmin.activeCategory;
					for (var i in tags_categories) if (tags_categories[i].tags.length > 0) for (var j in tags_categories[i].tags)
						if (tags_categories[i].tags[j].tag == $(this).attr('tag')){
							var theTag = $(this).attr('tag');
							var theDesciption = tags_categories[i].tags[j].description;
							if ($('.tag-definition h5').html() !== theTag+' - ') $('.tag-definition').fadeOut(50,function(){
								$(this).children('h5').html(theTag+' - ');
								$(this).children('span').html(theDesciption && theDesciption.length > 0 ? theDesciption : 'This tag has no description.');
								$(this).fadeIn(50);
							});
							break;
						}
				},function(){});
				$(this).on('click',function(){
					if ($(this).hasClass('active')) $(this).removeClass('active');
					else $(this).addClass('active');
				})

				var tagId = $(this).attr('tag-id');
				// if a slider exists, must be a city type
				var ele = $(this).find('#scoreSlider[for='+tagId+']');
				if (ele.length) {
					var myId = tagId;
					ele.slider({
						range: false,
						min: 0,
						max: 10,
						value: typeof salesAdmin.activeTags[tagId] != 'undefined' && salesAdmin.activeTags[tagId] ? salesAdmin.activeTags[tagId].score : 0,
				      	stop: function( event, ui ) {	
				        	score = ui.value;
				        	salesAdmin.addToActiveTags(tagId, score);	
				        	// var scoreValue = $('span#scoreValue[for='+myId+']');	      
				        	var scoreValue = $(this).siblings('#scoreValue');	
				        	scoreValue.html(ui.value);
				      	},
				      	slide: function( event, ui) {
				      		// var scoreValue = $('span#scoreValue[for='+myId+']');
				      		var scoreValue = $(this).siblings('#scoreValue');
				      		scoreValue.html(ui.value);
				      	}
					})
				}
				var ele = $(this).find('#scoreValue[for='+tagId+']');
				if (ele.length)
					ele.html(typeof salesAdmin.activeTags[tagId] != 'undefined' && salesAdmin.activeTags[tagId] ? salesAdmin.activeTags[tagId].score : '0');
			});
			// $('.tag-categories li[category='+self.defaults.tag_category.replace(' ','-')+']').click();
			$('.tag-categories li[category=activities]').click();
		}

		this.getTagSelectors = function(title, tags_categories, activeTags, haveScore, combined) {
			var h = //'<div id="tag-window">'+
						'<h2 class="title">Modify Tags - '+title+'</h2>'+
						'<div class="top"><span class="left">Categories</span><span class="right">'+(haveScore ? 'Make Non-Zero to Add Tags' : 'Click to Add Tags')+(typeof combined != 'undefined' ? ' - if tag is red, means '+(haveScore ? (length(activeTags) ? 'another city has that tag but this one does not' : 'it exists in one of the cities') : 'it exists in one of the listings in this city') : '') +'</span></div>'+
						'<ul class="tag-categories">';
						for (var i in tags_categories) if (length(tags_categories[i].tags) > 0) {
							h+= '<li class="tag-category" category="'+tags_categories[i].category.replace(' ','-')+'"><span class="title">'+tags_categories[i].category+'</span><span class="entypo-record"></span></li>';
						}
					h+= '</ul>'+
					'<div class="tag-definition"><h5></h5><span></span></div><div class="list-wrap">';
					for (var i in tags_categories) if ( length(tags_categories[i].tags) > 0) {
						h+= '<ul class="tags-list" id="'+tags_categories[i].category.replace(' ','-')+'" category="'+tags_categories[i].category.replace(' ','-')+'">';
						for (var j in tags_categories[i].tags){
							var tagId = tags_categories[i].tags[j].id;
							var haveActiveCurrent = length(activeTags) && typeof activeTags[tagId] != 'undefined' && activeTags[tagId];
							var makeRed = typeof combined != 'undefined' && typeof combined[tagId] != 'undefined' && !haveActiveCurrent;
							if (!haveScore)
								h+= '<li class="tag'+(haveActiveCurrent ? ' active' : '')+(makeRed ? ' red' : '')+'" tag="'+tags_categories[i].tags[j].tag+'" tag-id="'+tags_categories[i].tags[j].id+'"><span class="checkmark"><img class="mobile" src="'+ah_local.tp+'/_img/_sellers/check-mobile.png" /><img class="desktop" src="'+ah_local.tp+'/_img/_sellers/check.png" /></span>'+tags_categories[i].tags[j].tag+'</li>';
							else {// need slider
								h+= '<li class="tag" tag="'+tags_categories[i].tags[j].tag+'" tag-id="'+tags_categories[i].tags[j].id+'"><span id="tag" class="scored'+(makeRed ? ' red' : '')+'">'+tags_categories[i].tags[j].tag+'</span><span id="scoreValue" for="'+tags_categories[i].tags[j].id+'">'+(haveActiveCurrent ? activeTags[tagId].score : 0)+'</span><div id="scoreSlider" for="'+tags_categories[i].tags[j].id+'" /></li>';
							}
						}
						h+= '</ul>';
					}
					h+= '</div>';
				//h+= '</div>';
			  return h;
		}

		this.addTagGatheredCity = function(cityId) {
			var collated = salesAdmin.gatheredData;
			this.currentGatheredCity = collated[cityId];
			this.activeTags = this.currentGatheredCity.tags || [];
			this.activeCategory = cityCategories;
			this.activeMode = 'location';
			var haveScore = true;
			var h = this.getTagSelectors(collated[cityId].locationStr, cityCategories, this.activeTags, haveScore);
			$('div#dozone').hide();
			$('input#dozone').prop('checked', false);
			$('div#tag-window').html(h);
			this.setTagSelectorHandlers();
			$('#messageDiv #message').empty();
			$('div#tag-selections').addClass('active');
		}

		this.addTagToAllGatheredCities = function(cityId) {
			var collated = salesAdmin.gatheredData;
			this.currentGatheredCity = collated[cityId];
			this.activeTags = JSON.parse( JSON.stringify( this.currentGatheredCity.tags || [] ) ); // makes an exact copy
			this.activeCategory = cityCategories;
			this.activeMode = 'locations';
			var haveScore = true;
			var combined = [];
			for(var i in collated)
				for(var j in collated[i].tags)
					if (typeof combined[ collated[i].tags[j].tag_id ] == 'undefined')
						combined[ collated[i].tags[j].tag_id ] = collated[i].tags[j].tag_id ;
			var h = this.getTagSelectors('for all gathered cities, based on '+collated[cityId].locationStr, cityCategories, this.activeTags, haveScore, combined);
			$('div#dozone').show();
			$('input#dozone').prop('checked', false);
			$('div#tag-window').html(h);
			this.setTagSelectorHandlers();
			$('#messageDiv #message').empty();
			$('div#tag-selections').addClass('active');
		}

		this.addSpecificTagToAllGatheredCities = function(cityId) {
			var collated = salesAdmin.gatheredData;
			this.currentGatheredCity = collated[cityId];
			// this.activeTags = JSON.parse( JSON.stringify( this.currentGatheredCity.tags || [] ) ); // makes an exact copy
			this.activeTags = []; // makes an exact copy
			this.activeCategory = cityCategories;
			this.activeMode = 'locations-specific';
			var haveScore = true;
			var combined = [];
			for(var i in collated)
				for(var j in collated[i].tags)
					if (typeof combined[ collated[i].tags[j].tag_id ] == 'undefined')
						combined[ collated[i].tags[j].tag_id ] = collated[i].tags[j].tag_id ;
			var h = this.getTagSelectors('add to all gathered cities', cityCategories, this.activeTags, haveScore, combined);
			$('div#dozone').show();
			$('input#dozone').prop('checked', false);
			$('div#tag-window').html(h);
			this.setTagSelectorHandlers();
			$('#messageDiv #message').empty();
			$('div#tag-selections').addClass('active');
		}

		this.applyCityTagsToZone = function() {
			if (this.currentZone == null) {
				ahtb.alert("Define a zone to tag");
				return;
			}

			$('div.context-menu').hide();
			var collated = salesAdmin.gatheredData;
			this.activeTags = []; 
			this.activeCategory = cityCategories;
			this.activeMode = 'zone-city';
			var haveScore = true;
			var combined = [];
			for(var i in collated)
				for(var j in collated[i].tags)
					if (typeof combined[ collated[i].tags[j].tag_id ] == 'undefined')
						combined[ collated[i].tags[j].tag_id ] = collated[i].tags[j].tag_id ;
			var h = this.getTagSelectors('add city tags to zone only', cityCategories, this.activeTags, haveScore, combined);
			$('div#dozone').hide();
			$('input#dozone').prop('checked', false);
			$('div#tag-window').html(h);
			this.setTagSelectorHandlers();
			$('#messageDiv #message').empty();
			$('div#tag-selections').addClass('active');
		}

		this.addTagGatheredListing = function(cityId, listingId) {
			var collated = salesAdmin.gatheredData;
			this.currentGatheredCity = collated[cityId];
			this.currentGatheredListing = collated[cityId].listings[listingId];
			this.activeTags = this.currentGatheredListing.tags || [];
			this.activeCategory = listingCategories;
			this.activeMode = 'listing';
			var haveScore = false;
			var h = this.getTagSelectors(this.currentGatheredListing.street_address, listingCategories, this.activeTags, haveScore);
			$('div#dozone').hide();
			$('input#dozone').prop('checked', false);
			$('div#tag-window').html(h);
			this.setTagSelectorHandlers();
			$('#messageDiv #message').empty();
			$('div#tag-selections').addClass('active');
		}

		this.addTagToAllGatheredListings = function(cityId, listingId) {
			var collated = salesAdmin.gatheredData;
			this.currentGatheredCity = collated[cityId];
			this.currentGatheredListing = collated[cityId].listings[listingId];
			// this.activeTags = this.currentGatheredListing.tags || [];
			this.activeTags = [];
			this.activeCategory = listingCategories;
			this.activeMode = 'listings';
			var haveScore = false;
			var combined = [];
			for(var i in collated[cityId].listings)
				for(var j in collated[cityId].listings[i].tags)
					if (typeof combined[ collated[cityId].listings[i].tags[j].tag_id ] == 'undefined')
						combined[ collated[cityId].listings[i].tags[j].tag_id ] = collated[cityId].listings[i].tags[j].tag_id ;

			var h = this.getTagSelectors('add to all gathered listings', listingCategories, this.activeTags, haveScore, combined);
			var ele = $('div#dozone');
			ele.show();
			ele = $('input#dozone');
			ele.prop('checked', false);
			$('div#tag-window').html(h);
			this.setTagSelectorHandlers();
			$('#messageDiv #message').empty();
			$('div#tag-selections').addClass('active');
		}

		this.applyListingTagsToZone = function() {
			if (this.currentZone == null) {
				ahtb.alert("Define a zone to tag");
				return;
			}

			$('div.context-menu').hide();
			var collated = salesAdmin.gatheredData;
			this.activeTags = []; 
			this.activeCategory = listingCategories;
			this.activeMode = 'zone-listing';
			var haveScore = false;
			var combined = [];
			for(var cityId in collated)
				for(var i in collated[cityId].listings)
					for(var j in collated[cityId].listings[i].tags)
						if (typeof combined[ collated[cityId].listings[i].tags[j].tag_id ] == 'undefined')
							combined[ collated[cityId].listings[i].tags[j].tag_id ] = collated[cityId].listings[i].tags[j].tag_id ;


			var h = this.getTagSelectors('add listing tags to zone only', listingCategories, this.activeTags, haveScore, combined);
			$('div#dozone').hide();
			$('input#dozone').prop('checked', false);
			$('div#tag-window').html(h);
			this.setTagSelectorHandlers();
			$('#messageDiv #message').empty();
			$('div#tag-selections').addClass('active');
		}

		this.showGatheredListings = function(cityId) {
			var city = salesAdmin.gatheredData[cityId];
			if (length(city.listings) == 0) {
				ahtb.alert("This city has no listings");
				return;
			}

			var h = '<div id="listings-list">'+
						'<table id="listings-list">'+
							'<thead>'+
								'<tr>'+
									'<th>Address</th>'+
									'<th>Beds</th>'+
									'<th>Baths</th>'+
									'<th>About</th>'+
									'<th>Image</th>'+
									'<th>Action</th>'+
								'</tr>'+
							'</thead>'+
							'<tbody>';
						var filepath = '';
						for(var i in city.listings) {
							h+=	'<tr>'+
									'<td>'+(city.listings[i].street_address ? city.listings[i].street_address : "N/A")+'</td>'+
									'<td>'+(city.listings[i].beds ? city.listings[i].beds : "N/A")+'</td>'+
									'<td>'+(city.listings[i].baths ? city.listings[i].baths : "N/A")+'</td>'+
									'<td><div id="about">'+(city.listings[i].about ? city.listings[i].about : "N/A")+'</div></td>';
							if (city.listings[i].image) {
								if (city.listings[i].image.substr(0, 4) != 'http')
									filepath = ah_local.tp+'/_img/_cities/210x120/'+city.listings[i].image;
								else filepath = city.listings[i].image;
									h+=	'<td><img src="'+filepath+'" /></td>';
							}
							else
								h+=	'<td>No Image</td>';
								h+=	'<td><a href="'+ah_local.wp+'/listing/'+city.listings[i].id+'" target="_blank">View</a></td>'+
								'</tr>';
						}

						h+=	'</tbody>'+
						'</table>'+
					'</div>';
			ahtb.open({html: h,
					   width: 800,
					   height: 700,
					   buttons:[{text:'OK', action: function() { ahtb.close(); }}]});
		}

		this.createGatheredListingsMarker = function(city, listing, whatGatheredType, counter) {
			var latlng = new google.maps.LatLng(parseFloat(listing.lat), parseFloat(listing.lng));
			var id = listing.id;
			var cityId = city.id;

			// just to be sure it's cleared out...
			if (typeof this.gatheredListingMarkers != 'undefined' &&
				typeof this.gatheredListingMarkers[id] != 'undefined') {
				this.gatheredListingMarkers[id].setMap(null);
				delete this.gatheredListingMarkers[id];
				this.gatheredListingMarkers[id] = null;
			}

			if (typeof this.gatheredListingInfoWindows != 'undefined' &&
				typeof this.gatheredListingInfoWindows[id] != 'undefined') {
				this.gatheredListingInfoWindows[id].setMap(null);
				delete this.gatheredListingInfoWindows[id];
				this.gatheredListingInfoWindows[id] = null;
			}

			var markerName = 'Blue.png';
			var tagLen = typeof listing.tags != 'undefined' ? (Array.isArray(listing.tags) ? listing.tags.length : Object.keys(listing.tags).length) : 0;
			h = '<div id="gathered-div" class="details">'+
							'<span>'+(listing.street_address ? listing.street_address : 'No street address')+', '+city.city+' '+city.state+'</span>'+
							'<table class="listing-specialties">' +
								'<thead>' +
									'<tr>' +
										'<th scope="col" class="manage-column column-res-tag">Tag</th>' +
										'<th scope="col" class="manage-column column-res-action">Action</th>' +
									'</tr>' +
								'</thead>' +
								'<tbody>';
								if (tagLen) {
									for(var i in listing.tags) {
									h+=	'<tr >'+
											'<td>'+listing.tags[i].tag+'</td>';
											var isModifiableTag = tag_listing_for_sales_admin.indexOf(parseInt(i)) != -1;
											if (isModifiableTag)
											h+= '<td><a href="javascript:salesAdmin.applyToAllGatheredListings('+cityId+','+id+','+i+",'"+whatGatheredType+"')"+'";>Apply to ALL gathered listings in this city</a></td>';
											else
											h+= '<td>Cannot modify property tag</td>';
										// h+= '<td>'+city.tags[i].reservations.length+'</td>'+
										// 	'<td>'+city.tags[i].agents.length+'</td>'+
									h+= '</tr>';
									}
								}
								else
									h+= '<tr><td>No tags</td></tr>';
							h+=	'</tbody>'+
							'</table>'+
							'<table id="controls">'+
								'<tbody>';
								h+= '<tr><td><a href="javascript:salesAdmin.addTagGatheredListing('+cityId+','+id+')";>Add Tag to this listing</a></td></tr>';
								// h+= '<tr><td><a href="javascript:salesAdmin.addTagToAllGatheredListings('+cityId+','+id+')";>Make all gathered listings in this city have same tags (based off this '+"listing's tags"+', may add or delete existing tags, so use carefully)</a>'+
								h+= '<tr><td><a href="javascript:salesAdmin.addTagToAllGatheredListings('+cityId+','+id+')";>Added specific tag(s) to all listings in this city.</a>'+
									'</td></tr>';

								h+= '<tr><td><a href="javascript:salesAdmin.showGatheredListings('+cityId+')";>Show all Listings in this city</a></td></tr>';

								// switch(whatGatheredType) {
								// 	case 'locations':
								// 		break;
								// 	case 'agents':
								// 		h+= '<tr><td><a href="javascript:salesAdmin.showGatheredAgents('+id+')";>Show Agents</a></td></tr>';
								// 		break;
								// 	case 'listings':
								// 		h+= '<tr><td><a href="javascript:salesAdmin.showGatheredListings('+id+')";>Show Listings</a></td></tr>';
								// 		break;
								// }
								h+= '<tr><td><div id="messageDiv">'+
										'<span id="message"></span>'+
									'</div></td></tr>';
							h+=	'</tbody>'+
							'</table>'+
						'</div>';

			if (typeof listing.isRadial != 'undefined')
				markerName = 'Orange.png';

			this.gatheredListingInfoWindows[id] = new google.maps.InfoWindow({
																	      	content: h,
																	      	position: latlng
																	  	});
			
			this.gatheredListingMarkers[id] = new google.maps.Marker({
				title: (listing.street_address ? listing.street_address : 'No street address')+', '+city.city+' '+city.state,
	            position: latlng,
	            map: salesAdmin.map.map,
	            zIndex: google.maps.Marker.MAX_ZINDEX + counter,
	            icon: ah_local.tp+'/_img/sales/map-markers/'+markerName
		        });
	        google.maps.event.addListener(this.gatheredListingMarkers[id], 'click', function() {
	        	for (var i in salesAdmin.gatheredDataInfoWindows)
	        		salesAdmin.gatheredDataInfoWindows[i].close();

	        	for (var i in salesAdmin.gatheredListingInfoWindows)
	        		if (i != id) salesAdmin.gatheredListingInfoWindows[i].close();
	        	salesAdmin.gatheredListingInfoWindows[id].open(salesAdmin.map.map); //, salesAdmin.map.markers[id]);
	        	// salesAdmin.map.map.setCenter(salesAdmin.map.markers[id].getPosition());
	        });
		}

		this.createGatheredCitiesMarker = function(city, whatGatheredType, counter) {
			var latlng = new google.maps.LatLng(parseFloat(city.lat), parseFloat(city.lng));
			var id = city.id;

			// just to be sure it's cleared out...
			if (typeof this.gatheredDataMarkers != 'undefined' &&
				typeof this.gatheredDataMarkers[id] != 'undefined') {
				this.gatheredDataMarkers[id].setMap(null);
				delete this.gatheredDataMarkers[id];
				this.gatheredDataMarkers[id] = null;
			}

			if (typeof this.gatheredDataInfoWindows != 'undefined' &&
				typeof this.gatheredDataInfoWindows[id] != 'undefined') {
				this.gatheredDataInfoWindows[id].setMap(null);
				delete this.gatheredDataInfoWindows[id];
				this.gatheredDataInfoWindows[id] = null;
			}

			var listingTags = 0;
			var scoredTags = 0;
			var unscoredTags = 0;
			var cityTags = 0;
			var markerName = 'Red.png';
			var tagLen = typeof city.tags != 'undefined' ? (Array.isArray(city.tags) ? city.tags.length : Object.keys(city.tags).length) : 0;
			h = '<div id="gathered-div" class="details">'+
							'<span>City - '+city.locationStr+'</span>'+
							'<table class="city-specialties">' +
								'<thead>' +
									'<tr>' +
										'<th scope="col" class="manage-column column-res-tag">Tag</th>' +
										'<th scope="col" class="manage-column column-res-score">Score</th>' +
										'<th scope="col" class="manage-column column-res-action">Action</th>' +
									'</tr>' +
								'</thead>' +
								'<tbody>';
								if (tagLen) {
									for(var i in city.tags) {
										if (typeof city.tags[i].score != 'undefined' && city.tags[i].score != null) {
											cityTags++;
											if (city.tags[i].score == 1)
												unscoredTags++;
											else
												scoredTags++;
										}
										else 
											listingTags++;

									h+=	'<tr >'+
											'<td>'+city.tags[i].tag+'</td>';
										if (typeof city.tags[i].score != 'undefined' && city.tags[i].score != null) // && parseInt(city.tags[i].score) == 1)
										h+= '<td><a href="javascript:salesAdmin.updateGatheredCity('+id+','+i+",'"+whatGatheredType+"')"+'";>'+city.tags[i].score+'</a></td>';
										else
										h+= '<td>'+(typeof city.tags[i].score != 'undefined' && city.tags[i].score != null ? city.tags[i].score : 'N/A' )+'</td>';

										h+= '<td><a href="javascript:salesAdmin.applyToAllGatheredCities('+id+','+i+",'"+whatGatheredType+"')"+'";>Apply to ALL gathered cities</a></td>';
										// h+= '<td>'+city.tags[i].reservations.length+'</td>'+
										// 	'<td>'+city.tags[i].agents.length+'</td>'+
										'</tr>';
									}
								}
								else
									h+= '<tr><td>No tags</td></tr>';
							h+=	'</tbody>'+
							'</table>'+
							'<table id="controls">'+
								'<tbody>';
								h+= '<tr><td><a href="javascript:salesAdmin.addTagGatheredCity('+id+')";>Add Tag to this city</a></td></tr>';
								h+= '<tr><td><a href="javascript:salesAdmin.addTagToAllGatheredCities('+id+')";>Make all gathered cities have same tags (based off this '+"city's tags"+', may add or delete existing tags, so use carefully)</a>'+
									'</td></tr>';
								h+= '<tr><td><a href="javascript:salesAdmin.addSpecificTagToAllGatheredCities('+id+')";>Added specific tag(s) to all gathered cities.</a>'+
									'</td></tr>';								
								switch(whatGatheredType) {
									case 'locations':
										break;
									case 'agents':
										h+= '<tr><td><a href="javascript:salesAdmin.showGatheredAgents('+id+')";>Show Agents</a></td></tr>';
										break;
									case 'listings':
										h+= '<tr><td><a href="javascript:salesAdmin.showGatheredListings('+id+')";>Show all Listings in this city</a></td></tr>';
										break;
								}
								h+= '<tr><td><div id="messageDiv">'+
										'<span id="message"></span>'+
									'</div></td></tr>';
							h+=	'</tbody>'+
							'</table>'+
						'</div>';

			if (scoredTags)
				markerName = 'Green.png';

			this.gatheredDataInfoWindows[id] = new google.maps.InfoWindow({
																	      	content: h,
																	      	position: latlng
																	  	});
			
			this.gatheredDataMarkers[id] = new google.maps.Marker({
				title: city.locationStr,
	            position: latlng,
	            map: salesAdmin.map.map,
	            zIndex: google.maps.Marker.MAX_ZINDEX + counter,
	            icon: ah_local.tp+'/_img/sales/map-markers/'+markerName
		        });
	        google.maps.event.addListener(this.gatheredDataMarkers[id], 'click', function() {
	        	if (salesAdmin.gatheredListingInfoWindows)
		        	for (var i in salesAdmin.gatheredListingInfoWindows)
		        		salesAdmin.gatheredListingInfoWindows[i].close();

	        	for (var i in salesAdmin.gatheredDataInfoWindows)
	        		if (i != id) salesAdmin.gatheredDataInfoWindows[i].close();
	        	salesAdmin.gatheredDataInfoWindows[id].open(salesAdmin.map.map); //, salesAdmin.map.markers[id]);
	        	// salesAdmin.map.map.setCenter(salesAdmin.map.markers[id].getPosition());
	        });
		}

		this.map = new function(){
			this.colors = ['00cdff', '33c2ff', '66c2ff', '99ccff', 'cce0ff', 'd4dff0'];
			this.hidden = true;
			this.imageMapType = null;
			this.kmlLayer = null;
			this.currentZoom = 3;
			this.mapCenter =  new google.maps.LatLng(mapCenter.lat, mapCenter.lng);
			this.overlay = null;
			this.showQuick = function() {
				if (this.hidden){
					this.hidden = false;
					$('.view.selector a.map').parent().addClass('active');
					$('.view.selector a.list').parent().removeClass('active');
					$('div#reservations-list-div').hide();
					$('div#reservations-list-div').css('left','100%');
					$('div#reservation-map').show();
					$('div#reservation-map').css('left','0%');
				}
			}
			this.show = function(){
				if (this.hidden){
					this.hidden = false;
					$('.view.selector a.map').parent().addClass('active');
					$('.view.selector a.list').parent().removeClass('active');
					$('div#reservations-list-div').animate(
						{ left: "100%" },
						{ queue: false, duration: 500, done:function(){
							$(this).hide();
							$('div#reservation-map').show().animate({left: '0%'},{ queue: false, duration: 500, complete: function() {								
								if (salesAdmin.map.currentZoom == 3 ||
									salesAdmin.list.index == TableType.ALL_AGENTS) {
									salesAdmin.map.map.setZoom(5);
									salesAdmin.map.currentZoom = 5;
									// salesAdmin.map.map.setCenter(salesAdmin.map.mapCenter);
									salesAdmin.map.setMapBounds();
								}
							} });
							// salesAdmin.map.setMapBounds();
							var collated = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.reservationCollation != 'undefined' ? salesAdmin.salesInfo.reservationCollation : reservationCollation;
							salesAdmin.map.map.setZoom( salesAdmin.list.index == TableType.ALL_AGENTS ? 2 : (typeof collated[salesAdmin.list.location] != 'undefined' ? 10: 13) );
							if (salesAdmin.list.index > TableType.ALL_AGENTS ||
								salesAdmin.salesInfo != null) {
								// var collated = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.reservationCollation != 'undefined' ? salesAdmin.salesInfo.reservationCollation : reservationCollation;
								if (typeof collated[salesAdmin.list.location] != 'undefined') {
									salesAdmin.map.map.setCenter(new google.maps.LatLng(collated[salesAdmin.list.location].lat, collated[salesAdmin.list.location].lng));
									salesAdmin.map.map.setZoom(11);
									salesAdmin.map.currentZoom = 11;
								}
								else {
									salesAdmin.map.map.setCenter(new google.maps.LatLng(salesAdmin.cities[salesAdmin.cityIndex].lat, salesAdmin.cities[salesAdmin.cityIndex].lng));
									salesAdmin.map.map.setZoom(14);
									salesAdmin.map.currentZoom = 14;
								}
							}
							console.log("KML status:"+salesAdmin.map.kmlLayer.getStatus());
						}}
					);
				}
			}
			this.hide = function(){
				if (!this.hidden){
					this.hidden = true;
					$('.view.selector a.list').parent().addClass('active');
					$('.view.selector a.map').parent().removeClass('active');
					$('div#reservation-map').animate(
						{ left: "100%" },
						{ queue: false, duration: 500, done:function(){
							$(this).hide();
							$('div#reservations-list-div').show().animate({left: '0%'},{ queue: false, duration: 500 });
						}}
					);
				}
			}
			this.init = function(){
				this.clearMarkers();
				// if (typeof this.map != 'undefined')
				// 	delete this.map;
				// if (typeof this.bounds != 'undefined')
				// 	delete this.bounds;
				// $('div#reservation-map').html('');
				if (typeof google != 'undefined' &&
					typeof this.map == 'undefined'){
					this.bounds = new google.maps.LatLngBounds();
					this.markers = [];
					this.infowindows = [];
					//zip codes
				     var zoom = 5;
					 this.imageMapType = new google.maps.ImageMapType({
					     getTileUrl: function(coord, zoom) {
						      if (zoom < 5 || zoom > 18 ) {
						       return null;
						      }
						      if (zoom <= 13 ) {
						       var url = "http://storage.googleapis.com/zipmap/tiles/" + zoom + "/" + coord.x + "/" + coord.y + ".png" ;
						       return url ;
						      }
						      var server = coord.x % 6 ;
						      var url = "http://ts" + server + ".usnaviguide.com/tileserver.pl?X=" + coord.x + "&Y=" + coord.y + "&Z=" + zoom + "&T=" + tskey + "&S=Z1001" ;
						      return url;
					     },
					     tileSize: new google.maps.Size(256, 256),
					     opacity:.5,
					     name:ZIP_MAPTYPE_ID
				    });
					this.kmlLayer =  new google.maps.KmlLayer({
						url: ah_local.tp+'/_admin/_data/us_states.kml'
					})
				   
				    var mapOptions = {
					     minZoom: 4,
					     maxZoom: 16,
					     zoom: zoom,
					     center: (salesAdmin.salesInfo == null) ? new google.maps.LatLng(mapCenter.lat, mapCenter.lng) : new google.maps.LatLng(salesAdmin.salesInfo.mapCenter.lat, salesAdmin.salesInfo.mapCenter.lng),
					     mapTypeId: google.maps.MapTypeId.ROADMAP,
					     scrollwheel:true
				    };


					// if (salesAdmin.salesInfo == null)
					this.map = new google.maps.Map(document.getElementById('reservation-map'), mapOptions);
					this.map.overlayMapTypes.push(this.imageMapType);
					this.imageMapType.set('opacity', ZIP_OPACITY); 
					this.kmlLayer.setMap(this.map);

					this.overlay = new google.maps.OverlayView();
			        this.overlay.draw = function () {};
			        this.overlay.setMap(this.map);

					// keydragselect
					this.map.enableKeyDragSelect();
					var ds = this.map.getDragSelectObject();
					google.maps.event.addListener(ds, 'dragend', function (zone) {
			            console.log('KeyDragSelect Ended: ' + JSON.stringify(zone));
			            salesAdmin.zoneSelect(zone);
			          });

					 google.maps.event.addListener(this.map, 'rightclick', function (e) {
					 	if (!salesAdmin.currentZone)
					 		return;

					 	// e = e || window.event;
					 	// e.preventDefault();
						console.log('right mouse clicked');
						var offsetY = $('div#reservation-map').position().top;
						// var point = salesAdmin.map.overlay.getProjection().fromLatLngToDivPixel(e.latLng); 
			            var posX = e.pixel.x + 'px';
			            var posY = (e.pixel.y+offsetY+350) + 'px';

						var ele = $('div.context-menu');
						ele.css('left', posX);
						$('div.context-menu').css('top', posY);
						$('div.context-menu').show();
					 })


					


						// this.map = new google.maps.Map($('.reservations #reservation-map'), {center:new google.maps.LatLng(mapCenter.lat, mapCenter.lng),zoom:12,scrollwheel:true});
					// else
					// 	this.map = new google.maps.Map(document.getElementById('reservation-map'), {center:new google.maps.LatLng(salesAdmin.salesInfo.mapCenter.lat, salesAdmin.salesInfo.mapCenter.lng),zoom:12,scrollwheel:true});
						// this.map = new google.maps.Map($('.reservations #reservation-map'), {center:new google.maps.LatLng(salesAdmin.salesInfo.mapCenter.lat, salesAdmin.salesInfo.mapCenter.lng),zoom:12,scrollwheel:true});
				}

			}
			this.createMarkers = function() {
				var collated = salesAdmin.salesInfo != null && typeof salesAdmin.salesInfo.reservationCollation != 'undefined' ? salesAdmin.salesInfo.reservationCollation : reservationCollation;
				for(var i in collated)
					this.createMarker( collated[i], i );
			}
			this.createMarker = function( city, id ){
				if (typeof google != 'undefined' &&
					typeof city != 'undefined'){
					console.log("createMarker - "+city.locationStr+", lat:"+city.lat+", lng:"+city.lng);
					var latlng = new google.maps.LatLng(parseFloat(city.lat), parseFloat(city.lng));

					if (typeof this.markers != 'undefined' &&
						typeof this.markers[id] != 'undefined') {
						this.markers[id].setMap(null);
						delete this.markers[id];
						this.markers[id] = null;
					}

					if (typeof this.infowindows != 'undefined' &&
						typeof this.infowindows[id] != 'undefined') {
						this.infowindows[id].setMap(null);
						delete this.infowindows[id];
						this.infowindows[id] = null;
					}

					this.bounds.extend(latlng);
					var haveLimit = false;
					var haveAgents = false;
					var haveReservations = false;
					var listingTags = 0;
					var scoredTags = 0;
					var unscoredTags = 0;
					var cityTags = 0;
					h = '<div id="lifestyles-div" class="details">'+
									'<span>City - '+city.locationStr+'</span>'+
									'<table class="city-specialties">' +
										'<thead>' +
											'<tr>' +
												'<th scope="col" class="manage-column column-res-tag">Tag</th>' +
												'<th scope="col" class="manage-column column-res-score">Score</th>' +
												'<th scope="col" class="manage-column column-res-count">Reservations</th>' +
												'<th scope="col" class="manage-column column-am-count">Agents</th>' +
											'</tr>' +
										'</thead>' +
										'<tbody>';
										for(var i in city.tags) {
											if (typeof city.tags[i].score != 'undefined' && city.tags[i].score != null) {
												cityTags++;
												if (city.tags[i].score == 1)
													unscoredTags++;
												else
													scoredTags++;
											}
											else 
												listingTags++;

										h+=	'<tr '+(typeof city.tags[i].score != 'undefined' && city.tags[i].score != null && parseInt(city.tags[i].score) == 1 ? 'style="background-color: red;"' : '' )+'>'+
												'<td><a href="javascript:salesAdmin.list.showCityStatus(2,'+id+','+i+');" >'+city.tags[i].specialtyStr+'</a></td>';
											if (typeof city.tags[i].score != 'undefined' && city.tags[i].score != null) // && parseInt(city.tags[i].score) == 1)
											h+= '<td><a href="javascript:salesAdmin.updateCity(-1,'+id+','+i+','+false+')";>'+city.tags[i].score+'</a></td>';
											else
											h+= '<td>'+(typeof city.tags[i].score != 'undefined' && city.tags[i].score != null ? city.tags[i].score : 'N/A' )+'</td>';
											h+= '<td>'+city.tags[i].reservations.length+'</td>'+
												'<td>'+city.tags[i].agents.length+'</td>'+
											'</tr>';
											if (city.tags[i].reservations.length)
												haveReservations = true;
											if (city.tags[i].agents.length) {
												haveAgents = true;
												if (city.tags[i].agents.length >= 3)
													haveLimit = true; 
											}
										}
									h+=	'</tbody>'+
									'</table>'+
								'</div>';
					var len = Array.isArray(city.tags) ? city.tags.length : Object.keys(city.tags).length;
					if (len == listingTags)
						city.tag_state = CityTagState.TAGS_ALL_LISTINGS;
					else if (len == unscoredTags ||
							 cityTags == unscoredTags)
						city.tag_state = CityTagState.TAGS_NEED_ALL;
					else if (len == scoredTags ||
							 cityTags == scoredTags)
						city.tag_state = CityTagState.TAGS_HAVE_ALL;
					else
						city.tag_state = CityTagState.TAGS_HAVE_SOME;

					this.infowindows[id] = new google.maps.InfoWindow({
											      	content: h,
											      	position: latlng
											  	});
					var markerName = haveLimit ? 'Big' : '';
					markerName += haveAgents ? 'Blue' : 'Gray';
					switch(parseInt(city.tag_state)) {
						case CityTagState.TAGS_ALL_LISTINGS: markerName += 'White.png'; break;
						case CityTagState.TAGS_NEED_ALL: markerName += 'Red.png'; break;
						case CityTagState.TAGS_HAVE_SOME: markerName += 'Yellow.png'; break;
						case CityTagState.TAGS_HAVE_ALL: 
						default:
							markerName += 'Black.png'; break;
					}
				  	iconNumber = haveLimit ? 4 : (haveAgents ? 3 : 0);

					this.markers[id] = new google.maps.Marker({
						title: city.locationStr,
			            position: latlng,
			            map: this.map,
			            icon: ah_local.tp+'/_img/sales/map-markers/'+markerName
				        });
			        google.maps.event.addListener(this.markers[id], 'click', function() {
			        	for (var i in salesAdmin.map.infowindows)
			        		if (i != id) salesAdmin.map.infowindows[i].close();
			        	salesAdmin.map.infowindows[id].open(salesAdmin.map.map); //, salesAdmin.map.markers[id]);
			        	// salesAdmin.map.map.setCenter(salesAdmin.map.markers[id].getPosition());
			        });
				}
			}
			this.clearMarkers = function(){
				if (typeof google != 'undefined' &&
					typeof this.markers != 'undefined'){
					for (var i in this.markers) this.markers[i].setMap(null);
					this.markers = [];
					this.infowindows = [];
					this.bounds = new google.maps.LatLngBounds();
				}
			}
			this.setMapBounds = function(){ 
				if (typeof google != 'undefined' && !this.bounds.isEmpty()) {
					google.maps.event.trigger(this.map, 'resize');
					this.map.fitBounds(this.bounds); 
					if (salesAdmin.salesInfo != null)
						this.map.setZoom(12);
				}
			}
		}

		this.pickedCity = function(id, city_id, city) {
			$('#optionWrapper #addressOption .city').attr('city_id', city_id); 
		    $('#optionWrapper #addressOption .city').attr('city_name', city); 
			$('#optionWrapper #addressOption .city').val(city);
			if (ahtb.opened)
				ahtb.close();

			window.setTimeout(function() {
				salesAdmin.list.location = city_id;
			 	salesAdmin.list.showTable( 20+TableType.CITY_AGENTS, 0, city_id ); // 20 indicates fromOptionSubmit
			}, 250);
		}

		this.getCityId = function(id, city) {
			$.post(ah_local.tp+'/_pages/ajax-quiz.php',
                { query:'find-city',
                  data: { city: city,
                  		  distance: 0 }
                },
                function(){}, 'JSON')
				.done(function(d){
					if (d &&
                        d.data &&
                        d.data.city &&
                        (typeof d.data.city == 'object' ||
                        d.data.city.length) ){
	                    if (d.status == 'OK' &&
	                        (typeof d.data.city == 'object' || d.data.city.length) ) {							                        
	                        console.log(JSON.stringify(d.data));

	                    	if (d.data.allState == 'true' || d.data.allState == true || d.data.allState == 1) {// doing statewide
		                        ahtb.alert("Please enter a city name along with the state.", {height: 150});
		                        return;
		                    }

		                    var len = length(d.data.city); //(typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
	                    	// var len = (typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
							if (len > 1) {
								if (d.data.allState == 'false' || d.data.allState == false) {
			                        var h = '<p>Please pick one of the cities:</p>' +
			                                  '<ul class="cities">';
			                        for(var i in d.data.city) {
			                        	var location = d.data.city[i].city+', '+d.data.city[i].state;
			                          	h += '<li><a href="javascript:salesAdmin.pickedCity('+1+','+d.data.city[i].id+",'"+location+"'"+');" >'+location+'</a></li>';
			                        }
			                        h += '</ul>';
			                        ahtb.open(
			                          { title: 'Cities List',
			                            width: 380,
			                            height: (150 + (len * 25)) < 680 ? (150 + (len * 25)) : 680,
			                            html: h,
			                            buttons: [
			                                  {text: 'Cancel', action: function() {
			                                    ahtb.close();
			                                  }}],
			                            opened: function() {
			                            }
			                          })
			                    }
			                    else {
		                          	ahtb.alert("Only one city/state combination accepted.", {height: 150});
		                        }
		                    } // only one!
		                    else {
		                    	salesAdmin.pickedCity(0, d.data.city[0].id, d.data.city[0].city+', '+d.data.city[0].state);
		                    }
	                    }
	                    else {
	                      ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                      			{title: 'City Match', 
	                      			 height:180, 
	                      			 width:600});
	                    }
	                }
	                else {
	                    ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                      			{title: 'City Match', 
	                      			 height:180, 
	                      			 width:600});
	                }
                })
                .fail(function(d){
                	ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                      			{title: 'City Match', 
	                      			 height:180, 
	                      			 width:600});
                });
		}

		this.setupAutocomplete = function(element) {
			var ele = element.autocomplete({
									    source: salesAdmin.cities,
									    select: function(e, ui){ 
									      if (ui.item.value != null) {
									        element.attr('city_id', ui.item.id); 
									        element.attr('city_index', ui.item.index); 
									        element.attr('city_name', ui.item.label); 
									        testCityName = ui.item.label;
									        element.change();
									      }
									    }
									  });
			ele = ele.on('focus',function(){
			    if ($(this).val() == 'ENTER CITY') {
			      $(this).val(''); $(this).removeClass('inactive');
			    }
			  });

			ele.on('keypress', function(e){
				var ele = $(this);
				var item = $(this).attr('for');
			    var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
			    if(keynum == 13) {//13 is the enter char code
			      e.preventDefault();
			    }
			    $(this).attr('city_id', 0);
			    $(this).attr('city_name', '');	
			  });
		}

		this.DB = function(xx){
			if (xx != null){
				if (xx.data == null) xx.data = {};
				$('#optionWrapper .spinner').fadeIn(250, function(){});
				xx.data.sessionID = ah_local.sessionID;
				$.post(salesAdmin.ajaxSalesUrl,
					{ query: xx.query,
					  data: xx.data },
					function(){}, 'JSON')
				.done(function(d){
					$('#optionWrapper .spinner').fadeOut(250, function(){});
					$('.spin-wrap').fadeOut(250, function(){});
			        if (d.status != 'OK')
			        	xx.error ? xx.error (d.data) : ahtb.alert('There was a problem.<br/><small>'+(typeof d.data == 'string' ? d.data : JSON.stringify(d.data))+'</small>');
			        else if (xx.done != null)
			        	xx.done(d.data);
			        else console.log( (typeof d.data == 'string' ? d.data : JSON.stringify(d.data)) );
			      })
				.fail(function(d){
					$('#optionWrapper .spinner').fadeOut(250, function(){});
					$('.spin-wrap').fadeOut(250, function(){});
					if (d && d.status && d.status != 'OK')
						xx.fail ? xx.fail (d.data) : 
			        			 (xx.error ? xx.error (d.data) : 
								        	((salesAdmin.map.hidden) ? window.setTimeout(function() {
								        								ahtb.alert('There was a problem.<br/><small>'+(typeof d.data == 'string' ? d.data : JSON.stringify(d.data))+'</small>');
								        							  }, 250) :
								        							  window.setTimeout(function() {
													        			$('#messageDiv #message').html('There was a problem.<br/><small>'+(typeof d.data == 'string' ? d.data : JSON.stringify(d.data))+'</small>');
													        		  }, 1000) ));  		
			        else {
			        	if (salesAdmin.map.hidden)
			        		window.setTimeout(function() {
			        			ahtb.alert("There was a problem, sorry!");
			        		}, 250);
			        	else
			        		window.setTimeout(function() {
			        			var ele = $('#messageDiv #message');
			        			if (ele.length)
			        				ele.html("There was a problem doing "+xx.query+", sorry!");
			        			else
			        				ahtb.alert("There was a problem doing "+xx.query+", sorry!")
			        		}, 1000);  		
			        }
				});
			}
	  	}
	}

	salesAdmin.init();
})
