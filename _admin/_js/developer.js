var dev;
var files;
var dontDoIt = false;

var BlockState = {
	VIRGIN: 1,
	PROCESSING: 2,
	COMPLETE: 3,
	ERROR: 4
}

var ChunkState = {
	VIRGIN: 1,
	SENT:   2,
	ACKED:  3,
	PROCESSING: 4,
	COMPLETE: 5,
	ERROR: 6
}

var ImageOpMode = {
	TRIM_IMAGE: 0,
	BLACKOUT_IMAGE: 1,
	REGEN_IMAGE: 3,
	CUSTOM_IMAGE_TASK: 4
}

var FlagsToRemote = {
	OPTIMIZE_TABLE_AFTER_PARSE: 1,
	MODIFY_CPU_LIMIT: 2,
	MODIFY_CPU_AVAILABILITY: 4,
	MODIFY_ENGR1_OVERLOAD_SUSTAINED_TIME_LIMIT: 8,
	MODIFY_LIVE_OVERLOAD_SUSTAINED_TIME_LIMIT: 16,
	MODIFY_AJAX_LIMITER: 32,
	MODIFY_ENGR1_LOAD_LIMIT: 64,
	MODIFY_LIVE_LOAD_LIMIT: 128
}

function flagsToRemoteStr(i) {
	switch(i) {
		case 1:
			return 'OptimizeTableAfterParse';
		case 2:
			return 'ModifyCpuLimit';
		case 4:
			return 'ModifyCpuAvailability';
		case 8:
			return 'ModifyEngr1OverloadSustainedTimeLimit';
		case 16:
			return 'ModifyLiveOverloadSustainedTimeLimit';
		case 32:
			return 'ModifyAjaxLimiter';
		case 64: 
			return 'ModifyEngr1LoadLimit';
		case 128:
			return 'ModifyLiveLoadLimit';
	}
}

Math.log10 = Math.log10 || function(x) {
  return Math.log(x) / Math.LN10;
};

function chunkStateStr(x) {
	switch(x){
		case ChunkState.VIRGIN: return 'VIRGIN';
		case ChunkState.SENT: return 'SENT';
		case ChunkState.ACKED: return 'ACKED';
		case ChunkState.PROCESSING: return 'PROCESSING';
		case ChunkState.COMPLETE: return 'COMPLETE';
		case ChunkState.ERROR: return 'ERROR';
		default: return 'UNKNOWN';
	}
}
var RemoteMode = {
	NONE: 0,
	AWS: 1,
	FULL_PARSE_CRON: 2,
	IMAGE_BORDER: 3,
	IMAGE_BORDER_CRON_ALL: 4,
	IMAGE_BORDER_CRON_FROM_LAST: 5,
	IMAGE_PROCESSING_CUSTOM_TASK: 6,
	IMAGE_PROCESSING_CUSTOM_TASK_FROM_LAST: 7,
	PARSE_FEED_ONLY: 8
}

var SpecialOp = {
	SPECIAL_OP_NONE: 0,
	IMPORT_ACTIVE_LISTINGS_IMAGE: 1,
	CREATE_IMAGES_FROM_IMPORTED: 2,
	IMAGE_BORDER_PARALLEL: 3,
	IMAGE_BORDER_PARALLEL_AFTER_PARSE: 4,
	IMAGE_BORDER_PARALLEL_REMOTELY: 5,
	IMAGE_BORDER_PARALLEL_CRON_ALL: 6,
	IMAGE_BORDER_PARALLEL_CRON_FROM_LAST: 7,
	IMAGE_BORDER_PARALLEL_CUSTOM_TASK: 8,
	USER_ABORTED_PP: 99
}

var LogState = {
	UPDATE: '1',
	RESULT: '2',
	ERROR:  '3',
	FATAL:  '4',
	RESULT_PARALLEL:  '5',
	RESTART_PARALLEL: '6',
	BEGIN_PARALLEL:   '7',
	DENY_PARALLEL:    '8',
	REPEATING_PARALLEL: '9',
	ALIEN_RUNID:      '10',
	END_PARALLEL:     '11',
	PAUSE_PARALLEL:   '12',
	RESUME_PARALLEL:  '13',
	HALT_PARALLEL:    '14', 
	REMOTE_PARALLEL:  '15',
	REMOTE_P2DB:      '16',
	RELOAD_PAGE:      '17',
	WRITE_CSV_GZ:     '18',
	LOCK_SQL: 		  '19',
	UNLOCK_SQL: 	  '20',
	UNUSED_21:  	  '21',
	UNUSED_22: 		  '22',
	CLEAN_FOLDER_UPDATE: '23',
	CLEAN_SQL_TABLE: '24',
	LISTHUB_PARSE_RESULT: '25',
	BEGIN_UPDATE_EMAIL_DB: '26',

	IMAGE_BORDER_START: '30',
	IMAGE_BORDER_LISTING_START: '31',
	IMAGE_BORDER_LISTING_DONE: '32',
	IMAGE_BORDER_ERROR: '33',
	IMAGE_BORDER_END: '34',
	IMAGE_BORDER_PROCESSING_STAT: '43',
	IMAGE_BORDER_NO_FILE: '44',
	IMAGE_BORDER_BAD_DEST_MAKING_LIST_IMAGE: '45',
	IMAGE_BORDER_FAILED_GET_COuNT: '58',
	IMAGE_BORDER_FAILED_CREATE_IMAGE_JPEG: '59',

	REMOTE_IMAGE_PROCESSING: '140',
	CLEAN_IMAGE_INACTIVE: '141',

	CONCLUDE_PP_MEDIAN_VALUES_CONSOLIDATE_DAILY: '150',
	CONCLUDE_PP_OPTIMIZE_TABLES: '151',

	WRITE_LOG_HEADER_START: '160',
	WRITE_LOG_HEADER_RESET_DAILY_MEDIAN_VALUES: '161',
	WRITE_LOG_HEADER_UPDATE_TIERS: '162',
	WRITE_LOG_HEADER_DONE: '163',
	WRITE_LOG_HEADER_PRESTART_SQL: '164',
	WRITE_LOG_HEADER_EMPTY_TABLE: '165',
	WRITE_LOG_HEADER_UPDATE_BLOCK_FLAGS: '166',

	PROCESS_EXEC_START: '170',
	PROCESS_EXEC_END: '171',
	PROCESS_EXEC_OUTPUT: '172',

	CLEAN_IMAGE_START:'180',
	CLEAN_IMAGE_UPDATE:'181',
	CLEAN_IMAGE_END:  '182',
	CLEAN_IMAGE_INIT_COUNT: '183',
	CLEAN_IMAGE_REMOVE_COUNT: '184',
	CLEAN_IMAGE_ERROR: '185',
	CLEAN_IMAGE_MARK_IT: '186',
	CLEAN_IMAGE_REMOVE_IT: '187',
	CLEAN_IMAGE_RESET_IMAGES: '188',

	RESTART_PARALLEL_IP: '190',
	RESTART_PARALLEL_CUSTOM_IP: '191',

	PING_REMOTE: '200',
	STATS_REMOTE: '201',
	FLAGS_TO_REMOTE: '202',

	POST_FROM_FE: '300',
	FAILED_PARSE_TO_DB: '301'
}

var ActiveState = {
	INACTIVE: 0,
	ACTIVE: 1,
	WAITING:   2,
	REJECTED: 3,
	TOOCHEAP: 4,
	NEVER: 5
}

// var CpuGovernor = {
// 	0: {limit: ['20','20'], ajaxLimiter: 4, cpuAvailability: 30},
// 	1: {limit: ['20','20'], ajaxLimiter: 4, cpuAvailability: 30},
// 	4: {limit: ['15','20'], ajaxLimiter: 4, cpuAvailability: 30},
// 	5: {limit: ['12','20'], ajaxLimiter: 4, cpuAvailability: 30},
// 	6: {limit: ['10','20'], ajaxLimiter: 3, cpuAvailability: 30},
// 	7: {limit: ['8','18'], ajaxLimiter: 3, cpuAvailability: 40},
// 	9: {limit: ['8','15'], ajaxLimiter: 3, cpuAvailability: 45},
// 	19: {limit: ['8','15'], ajaxLimiter: 3, cpuAvailability: 45},
// 	20: {limit: ['12','18'], ajaxLimiter: 3, cpuAvailability: 45},
// 	21: {limit: ['15','20'], ajaxLimiter: 3, cpuAvailability: 45},
// 	22: {limit: ['18','20'], ajaxLimiter: 4, cpuAvailability: 40},
// 	23: {limit: ['20','20'], ajaxLimiter: 4, cpuAvailability: 30},
// }
// var CpuGovernor = {
// 	0: {limit:  [[['0','20'],['0','20'],['1.5','15']],[['0','20'],['0','20'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
// 	1: {limit:  [[['0','20'],['0','20'],['1.5','15']],[['0','20'],['0','20'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
// 	4: {limit:  [[['0','15'],['0','15'],['1.5','12']],[['0','20'],['0','20'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
// 	5: {limit:  [[['0','12'],['0','12'],['1.5','9']],[['0','20'],['0','20'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
// 	6: {limit:  [[['0','10'],['0','10'],['1.5','6']],[['0','20'],['0','20'],['1.5','15']]], ajaxLimiter: 3, cpuAvailability: 30},
// 	7: {limit:  [[['0','8'],['0','8'],['1.5','5']],  [['0','20'],['0','20'],['1.5','15']]], ajaxLimiter: 3, cpuAvailability: 40},
// 	9: {limit:  [[['0','8'],['0','8'],['1.5','5']],  [['0','15'],['0','15'],['1.5','10']]], ajaxLimiter: 3, cpuAvailability: 45},
// 	19: {limit: [[['0','8'],['0','8'],['1.5','5']],  [['0','15'],['0','15'],['1.5','10']]], ajaxLimiter: 3, cpuAvailability: 45},
// 	20: {limit: [[['0','12'],['0','12'],['1.5','9']],[['0','18'],['0','18'],['1.5','12']]], ajaxLimiter: 3, cpuAvailability: 45},
// 	21: {limit: [[['0','15'],['0','15'],['1.5','10']],[['0','20'],['0','20'],['1.5','15']]], ajaxLimiter: 3, cpuAvailability: 45},
// 	22: {limit: [[['0','18'],['0','18'],['1.5','12']],[['0','20'],['0','20'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 40},
// 	23: {limit: [[['0','20'],['0','20'],['1.5','15']],[['0','20'],['0','20'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
// }
// var CpuGovernorRemote = {
// 	0: {limit:  [[['0','25'],['0','40'],['1.5','15']],[['0','25'],['0','40'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
// 	1: {limit:  [[['0','25'],['0','40'],['1.5','15']],[['0','25'],['0','40'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
// 	4: {limit:  [[['0','25'],['0','30'],['1.5','12']],[['0','25'],['0','40'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
// 	5: {limit:  [[['0','25'],['0','30'],['1.5','12']],[['0','25'],['0','40'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
// 	6: {limit:  [[['0','25'],['0','30'],['1.5','12']],[['0','25'],['0','40'],['1.5','15']]], ajaxLimiter: 3, cpuAvailability: 30},
// 	7: {limit:  [[['0','25'],['0','30'],['1.5','12']],[['0','25'],['0','40'],['1.5','15']]], ajaxLimiter: 3, cpuAvailability: 40},
// 	9: {limit:  [[['0','25'],['0','30'],['1.5','12']],[['0','25'],['0','40'],['1.5','15']]], ajaxLimiter: 3, cpuAvailability: 45},
// 	19: {limit: [[['0','25'],['0','30'],['1.5','12']],[['0','25'],['0','40'],['1.5','15']]], ajaxLimiter: 3, cpuAvailability: 45},
// 	20: {limit: [[['0','25'],['0','30'],['1.5','12']],[['0','25'],['0','40'],['1.5','15']]], ajaxLimiter: 3, cpuAvailability: 45},
// 	21: {limit: [[['0','25'],['0','40'],['1.5','15']],[['0','25'],['0','40'],['1.5','15']]], ajaxLimiter: 3, cpuAvailability: 45},
// 	22: {limit: [[['0','25'],['0','40'],['1.5','15']],[['0','25'],['0','40'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 40},
// 	23: {limit: [[['0','25'],['0','40'],['1.5','15']],[['0','25'],['0','40'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
// }
var CpuGovernor = {
	0: {limit:  [[['4','20'],['4','20'],['1.5','15']],[['4','20'],['4','20'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
	1: {limit:  [[['4','20'],['4','20'],['1.5','15']],[['4','20'],['4','20'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
	4: {limit:  [[['4','15'],['4','15'],['1.5','12']],[['4','20'],['4','20'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
	5: {limit:  [[['4','12'],['4','12'],['1.5','9']], [['4','20'],['4','20'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
	6: {limit:  [[['4','10'],['4','10'],['1.5','6']], [['4','20'],['4','20'],['1.5','15']]], ajaxLimiter: 3, cpuAvailability: 30},
	7: {limit:  [[['4','8'], ['4','8'],['1.5','5']],  [['4','20'],['4','20'],['1.5','15']]], ajaxLimiter: 3, cpuAvailability: 40},
	9: {limit:  [[['4','8'], ['4','8'],['1.5','5']],  [['4','15'],['4','15'],['1.5','10']]], ajaxLimiter: 3, cpuAvailability: 45},
	19: {limit: [[['4','8'], ['4','8'],['1.5','5']],  [['4','15'],['4','15'],['1.5','10']]], ajaxLimiter: 3, cpuAvailability: 45},
	20: {limit: [[['4','12'],['4','12'],['1.5','9']], [['4','18'],['4','18'],['1.5','12']]], ajaxLimiter: 3, cpuAvailability: 45},
	21: {limit: [[['4','15'],['4','15'],['1.5','10']],[['4','20'],['4','20'],['1.5','15']]], ajaxLimiter: 3, cpuAvailability: 45},
	22: {limit: [[['4','18'],['4','18'],['1.5','12']],[['4','20'],['4','20'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 40},
	23: {limit: [[['4','20'],['4','20'],['1.5','15']],[['4','20'],['4','20'],['1.5','15']]], ajaxLimiter: 4, cpuAvailability: 30},
}
var CpuGovernorRemote = {
	0: {limit:  [[['4','3.5'],['4','8'],['4','8']],[['4','3.5'],['4','8'],['4','38']]], ajaxLimiter: 4, cpuAvailability: 30},
	1: {limit:  [[['4','8'],['4','8'],['4','8']],  [['4','8'],['4','8'],['4','8']]], ajaxLimiter: 4, cpuAvailability: 30},
	4: {limit:  [[['4','8'],['4','8'],['4','8']],  [['4','8'],['4','8'],['4','8']]], ajaxLimiter: 4, cpuAvailability: 30},
	5: {limit:  [[['4','3.5'],['4','8'],['4','8']],[['4','3.5'],['4','8'],['4','8']]], ajaxLimiter: 4, cpuAvailability: 30},
	6: {limit:  [[['4','3'],['4','8'],['4','8']],  [['4','3'],['4','8'],['4','8']]], ajaxLimiter: 3, cpuAvailability: 30},
	7: {limit:  [[['4','2.5'],['4','8'],['4','8']],[['4','2.5'],['4','8'],['4','8']]], ajaxLimiter: 3, cpuAvailability: 40},
	9: {limit:  [[['4','2.5'],['4','8'],['4','8']],[['4','2.5'],['4','8'],['4','8']]], ajaxLimiter: 3, cpuAvailability: 45},
	19: {limit: [[['4','2.5'],['4','8'],['4','8']],[['4','2.5'],['4','8'],['4','8']]], ajaxLimiter: 3, cpuAvailability: 45},
	20: {limit: [[['4','2.5'],['4','8'],['4','8']],[['4','2.5'],['4','8'],['4','8']]], ajaxLimiter: 3, cpuAvailability: 45},
	21: {limit: [[['4','2.5'],['4','8'],['4','8']],[['4','2.5'],['4','8'],['4','8']]], ajaxLimiter: 3, cpuAvailability: 45},
	22: {limit: [[['4','2.5'],['4','8'],['4','8']],[['4','2.5'],['4','8'],['4','8']]], ajaxLimiter: 4, cpuAvailability: 40},
	23: {limit: [[['4','3.5'],['4','8'],['4','8']],[['4','3.5'],['4','8'],['4','8']]], ajaxLimiter: 4, cpuAvailability: 30},
}

var MAX_QUANDL_API_CALLS = 40000;
var MAX_POINTS = 80;
var MAX_ALLOW_AJAXCOUNT_PEGGED_SINCE_LAST_SEND = 3000;
var MAX_ALLOW_AJAXCOUNT_NOT_ZEROED = 5000;
var RESULTS_TIMEROUT = 2000;
var myTimer= null;
var myProgressTimer= null;
var myIsItAllDoneTimer= null;
var myTimerList = []; // subDomain's timers
var ImageProcessingCpuDelayMultiplier = 2.5;
var ImageProcessingCpuAvailabilityMultiplier = 2;
var ImageProcessingDelayPerImageMultiplier = 6;
var ImageProcessingBadCounterMultiplier = 4;
var ParallelProcessingBadCounterMultiplier = 4;
var MAX_PARSE_RETRIES = 3;
var MAX_INITIAL_IMAGE_CHUNK_TO_SEND = 20;
var DELAY_AFTER_INITIAL_IMAGE_CHUNKS_SENT = 90000; // msecs
var ACCEPTABLE_PER_IMAGE_RETRIEVAL_RATE = 3.5; // secs per image
var MAX_IMAGE_PROCESSING_CHUNK_SIZE = 10;
var MAX_LISTHUB_PROCESSING_CHUNK_SIZE = 50;
var MAX_HEAVY_LOAD_LIMIT_ENGR1 = 14;
var MAX_HEAVY_LOAD_LIMIT_LIVE = 4.5;
var MAX_HEAVY_LOAD_SUSTAINED_TIME_LIMIT_ENGR1 = 3 * 60 * 1000;
var MAX_HEAVY_LOAD_SUSTAINED_TIME_LIMIT_LIVE = 3 * 60 * 1000;
var MAX_OVERLOAD_ALLOWED_COUNT = 4;

resultsTimer= null;

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(what) {
    var user = getCookie(what);
    if (user != "") {
        return true;
    } else {
        return false;
    }
}

if (!Date.now) {
  Date.now = function now() {
    return new Date().getTime();
  };
}

if (typeof getTime != 'function')
	window.getTime = function(date) {
		var currentdate = typeof date == 'undefined' ? new Date() : date; 
		var datetime = (currentdate.getMonth()+1)  + "/" 
	                + currentdate.getDate() + "/"
	                + currentdate.getFullYear() + " "  
	                + currentdate.getHours() + ":"  
	                + currentdate.getMinutes() + ":" 
	                + currentdate.getSeconds();
	    return datetime;
	}

var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
var lineChartData = {
	labels : ["0"], //,"5","10","15","20","25","30","35","40","45","55","60"],
	datasets : [
		{
			label: "active",
			fillColor : "rgba(220,220,220,0.2)",
			strokeColor : "rgba(220,220,220,1)",
			pointColor : "rgba(220,220,220,1)",
			pointStrokeColor : "#fff",
			pointHighlightFill : "#fff",
			pointHighlightStroke : "rgba(220,220,220,1)",
			data: [0] //2, 4, 5, 9, 12, 10, 7, 13, 8, 10, 11, 7]
			//data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
		},
		{
			label: "load",
			fillColor : "rgba(151,187,205,0.2)",
			strokeColor : "rgba(151,187,205,1)",
			pointColor : "rgba(151,187,205,1)",
			pointStrokeColor : "#fff",
			pointHighlightFill : "#fff",
			pointHighlightStroke : "rgba(151,187,205,1)",
			data: [0] //.02, 0.8, 1.21, 1.76, 2.28, 1.87, 1.34, 2.01, 0.7, 1.25, 1.57, 0.74]
			//data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
		},
		{
			label: "denied",
			fillColor : "rgba(251,187,205,0.2)",
			strokeColor : "rgba(251,187,205,1)",
			pointColor : "rgba(251,187,205,1)",
			pointStrokeColor : "#fff",
			pointHighlightFill : "#fff",
			pointHighlightStroke : "rgba(251,187,205,1)",
			data: [0] //, 0, 1, 2, 2, 5, 1, 2, 2, 3, 4, 1]
			//data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
		},
		{
			label: "networkerror",
			fillColor : "rgba(251,135,135,0.2)",
			strokeColor : "rgba(251,135,135,1)",
			pointColor : "rgba(251,135,135,1)",
			pointStrokeColor : "#fff",
			pointHighlightFill : "#fff",
			pointHighlightStroke : "rgba(251,135,135,1)",
			data: [0] //, 0, 1, 2, 2, 5, 1, 2, 2, 3, 4, 1]
			//data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
		},
		{
			label: "added",
			fillColor : "rgba(135,135,255,0.2)",
			strokeColor : "rgba(135,135,255,1)",
			pointColor : "rgba(135,135,255,1)",
			pointStrokeColor : "#fff",
			pointHighlightFill : "#fff",
			pointHighlightStroke : "rgba(135,135,255,1)",
			data: [0] //, 0, 1, 2, 2, 5, 1, 2, 2, 3, 4, 1]
			//data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
		},
		{
			label: "dups",
			fillColor : "rgba(135,255,135,0.2)",
			strokeColor : "rgba(135,255,135,1)",
			pointColor : "rgba(135,255,135,1)",
			pointStrokeColor : "#fff",
			pointHighlightFill : "#fff",
			pointHighlightStroke : "rgba(135,255,135,1)",
			data: [0] //, 0, 1, 2, 2, 5, 1, 2, 2, 3, 4, 1]
			//data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
		},
		{
			label: "rejected",
			fillColor : "rgba(120,120,120,0.2)",
			strokeColor : "rgba(120,120,120,1)",
			pointColor : "rgba(120,120,120,1)",
			pointStrokeColor : "#fff",
			pointHighlightFill : "#fff",
			pointHighlightStroke : "rgba(120,120,120,1)",
			data: [0] //, 0, 1, 2, 2, 5, 1, 2, 2, 3, 4, 1]
			//data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
		},
		{
			label: "belowMinPrice",
			fillColor : "rgba(255,50,50,0.2)",
			strokeColor : "rgba(255,50,50,1)",
			pointColor : "rgba(255,50,50,1)",
			pointStrokeColor : "#fff",
			pointHighlightFill : "#fff",
			pointHighlightStroke : "rgba(255,50,50,1)",
			data: [0] //, 0, 1, 2, 2, 5, 1, 2, 2, 3, 4, 1]
			//data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
		}
	]

}

var haveMyOwn = false;
var myAction = '';
var haveIP = false;
var haveIPCustom = false;
var havePP = false;
var haveP2Db = false;
var otherIPOwner = '';
var otherPPOwner = '';
var otherP2DbOwner = '';
				
function setBEStates() {
	haveMyOwn = false;
 	myAction = '';
 	haveIP = false;
 	haveIPCustom = false;
 	havePP = false;
 	haveP2Db = false;
 	otherIPOwner = '';
 	otherPPOwner = '';
 	otherP2DbOwner = '';
 	if (dev.haveActiveBE == 'false' ||
 		dev.haveActiveBE == 'working')
 		return;

	for(var i in dev.haveActiveBE)
		if (dev.haveActiveBE[i]['value'] == dev.myMountain) {
			haveMyOwn = true;
			myAction = dev.haveActiveBE[i]['opt'];
		}
		else if (dev.haveActiveBE[i]['opt'] == 'activeIP') {
			haveIP = true;
			otherIPOwner = dev.haveActiveBE[i]['value'];
		}
		else if (dev.haveActiveBE[i]['opt'] == 'activeIPCustom') {
			haveIPCustom = true;
			otherIPOwner = dev.haveActiveBE[i]['value'];
		}
		else if (dev.haveActiveBE[i]['opt'] == 'activePP') {
			havePP = true;
			otherPPOwner = dev.haveActiveBE[i]['value'];
		}
		else if (dev.haveActiveBE[i]['opt'] == 'activeParseToDb') {
			haveP2Db = true;
			otherP2DbOwner = dev.haveActiveBE[i]['value'];
		}

	if (!haveMyOwn && !haveIP && !havePP && !haveP2Db && !haveIPCustom)
		dev.haveActiveBE = 'false';

}

jQuery(document).ready(function($){ 
dev = new function(){

	this.initMembers = function() {
		this.report = '';
		this.linesReported = this.linesMonitorStart = this.linesMonitorNeedsToRead = this.myProgressStartPoint = 0;
		this.runParallel = true;
		this.parallelList = [];
		//this.myTimer = this.myProgressTimer = this.myIsItAllDoneTimer = 0;
		this.activeChunks = 0;
		this.totalBlocks = 0;
		this.gotTotalBlocks = false;
		this.failedBlocks = 0;
		this.chunksCompleted = 0;
		this.chunksSent = 0;
		this.chunksExpected = 0;
		this.curIndex = 0;
		this.formData = 0;
		this.loadLimitParse = 20;
		this.loadLimitImageProcessing = 20;
		this.userSelectedUnitsPerChunk = false;
		if ( ah_local.tp.indexOf('localhost') == -1) {
			this.unitsPerChunk = MAX_LISTHUB_PROCESSING_CHUNK_SIZE;
			this.numSubDomains = 12;
			this.numChunkPerSubDomain = 6;
			this.loadLimit = this.loadLimitParse;
		} else {
			this.unitsPerChunk = 4;
			this.numSubDomains = 3;
			this.numChunkPerSubDomain = 2;
			this.loadLimit = "5";
			MAX_INITIAL_IMAGE_CHUNK_TO_SEND = this.numSubDomains * this.numChunkPerSubDomain;
		}

		this.minTags = 3;
		this.minPrice = noAskAllowMinPrice;

		this.maxParallelism = this.numSubDomains * this.numChunkPerSubDomain;
		//this.myTimerList = []; // subDomain's timers
		this.doingParallel = false;
		this.doingMonitor = 0; 	  // 0 = regular PP, 
							      // 1 = monitor is just starting up and catching up, 
								  // 2 = monitor is ready to display
								  // 3 = begin replay
								  // 4 = replay slowdown replay
								  // 5 = end replay
								  // 6 = halt command was issued
		this.doingImageProcessing = false;
		this.gotMyTreat = false;
		this.gotMyProgressStartPoint = false;
		this.gettingMyProgressStartPoint = 0;
		this.myParallelResults = []; // each subDomain will have an entry, with number of active and sent values.
		//this.resultsTimer = 0;
		this.ajaxCount = 0;
		this.stopTheMadness = false;
		this.pausePP = false;
		this.startTaskTime = 0;
		this.chart = null;
		this.chartLastUpdatedAt = 0;
		this.tallyDenied = this.tallyLoadCount = 0;
		this.tallyLoad = 0.0;
		this.tallyLoadPing = [];
	 	this.tallyLoadPingCount = [];
		this.chartLapsed = this.chartXAxis = 0;
		this.chartInterval = 10000; // every 10 secs, update chart
		this.tallyNetworkConnection = 0;
		this.tallyAdded = 0;
	 	this.tallyDups = 0;
	 	this.tallyRejected = 0;
	 	this.tallyBelowMinimumAcceptedPrice = 0;
	 	this.tallyTotalBlocks = 0;
	 	this.sumAdded = this.sumDups = this.sumRejected = this.sumBelowMinimumAcceptedPrice = 0;
		this.chartDataList = [];
		if (!checkCookie('everest') ||
			!checkCookie('mountain') ||
			 getCookie('everest').indexOf('null') == 0 ||
			 getCookie('mountain').indexOf('null') == 0) {
			this.myMountain = ah_local.sessionID+"-"+Math.round(Math.random()*10000).toString();
			this.myEverest = 'everest'+"-"+Math.round(Math.random()*10000).toString();
			setCookie('mountain', this.myMountain, 1);
			setCookie('everest', this.myEverest, 1);
		}
		else {
			this.myMountain = getCookie('mountain');
			this.myEverest = getCookie('everest');
		}
		this.clearedMyTreat = false;
		this.haveActiveBE = 'false';
		this.gotMyhaveActiveBE = false;
		this.removedActivePP = false;
		this.removeActiveP2DbStart = 0;
		this.readyStartParsing = false;
		this.startingActivePP = this.haveStartActivePP = this.startingActiveIP = false;
		this.removedActiveP2Db = false;
		this.startingActiveP2Db = this.haveStartActiveP2Db = false;
		this.saveRejected = true;
		this.lastResultTime = this.lastCheckedResultSynchronicity = 0;
		this.checkSynchronicity = false;
		this.synchroDeltaLines = 5;
		this.synchroDeltaTime = 3000; // msec
		this.lastMonitorCheckedActivePP = 0;
		this.wrotelogHeaders = false;
		this.errorWriteLogHeaders = false;
		this.havePPRecords = false;
		this.doingPPRecords = false;
		this.lastTryGetPPRecordRange = this.getPPRecordStart = this.getPPRecordEnd = 0;
		this.startTime = new Date().getTime();
		this.ppRunId = 0;
		this.gotLastStartEntry = false;
		this.startGetLastStartEntryTime = 0;
		this.startGetMyHaveActiveBETime = 0;
		this.startGetLastProgressCount = 0;
		this.reloadPage = false;
		this.processOnlyNewImages = true;
		this.lastCheckedForCronJobs = 0;
		this.imageProcessAfterParse = false;
		this.resetMedianAndTiers = true;
		this.listhubParseOptimizeTableAfterwards = true;
		this.doingRemote = 0;
		this.amazon = null;
		this.pingCycle = 0;
		this.pingEc2Target = 0;
		this.startEc2Count = 5;
		this.startingEc2 = false;
		this.host = 'Linux';
		this.gotHostEnvironment = false;
		this.gotAWSData = false;
		this.preserveActiveListings = true;
		this.sellerCount = 0;
		this.emailDbPage = 0;
		this.emailDbPerPage = 1000;
		this.doingSpecialParallelOperation = false;
		this.useImpressions = false;
		this.imageBorderMode = 0;
		this.imageBorderOp = 0;
		this.imageBorderFromLast = 0;
		this.imageBorderLogDetails = false;
		this.imageBorderOptimizeTableAfterwards = false;
		this.activeListingCount = 0;
		this.activeListingPage = 0;
		this.activeListingPerPage = 100;
		this.haltResetImageOrigin = false;
		this.haltSetFirstImage = false;
		this.activeCitiesCount = 0;
		this.activeCitiesPage = 0;
		this.activeCitiesPerPage = 100;
		this.haltUpdateMedianPrices = false;
		this.activeZipCount = 0;
		this.activeZipPage = 0;
		this.activeZipPerPage = 100;
		this.maxActiveZipApiCalls = MAX_QUANDL_API_CALLS; 

		this.logBuffer = '';
		this.logIndex = 0;
		this.cpuGovernorOverride = false;
		this.recoveringChunk = false;
		this.startRecoveringChunk = 0;
		this.maxSubDomainsAllowed = this.numSubDomains;
		this.lastTimeChunkSent = 0;
		this.lastTimeAjaxCountWentToZero = 0;
		this.maxCpuDelay = 3;
		this.sendingChunk = false;
		this.ajaxLimiter = 3;
		this.cpuAvailability = 30; // div by 100 to use
		this.cpuRange = [0,8];
		this.cpuHeavyLoadSustainedTime = 0;
		this.cpuMaxHeavyLoadSustainedTimeLimitEngr1 = MAX_HEAVY_LOAD_SUSTAINED_TIME_LIMIT_ENGR1;
		this.cpuMaxHeavyLoadSustainedTimeLimitLive = MAX_HEAVY_LOAD_SUSTAINED_TIME_LIMIT_LIVE;
		this.cpuHeavyLoadLimitEngr1 = MAX_HEAVY_LOAD_LIMIT_ENGR1; // if cpu load is more than this for over cpuMaxHeavyLoadSustainedLimit, restart apache on server
		this.cpuHeavyLoadLimitLive = MAX_HEAVY_LOAD_LIMIT_LIVE; // if cpu load is more than this for over cpuMaxHeavyLoadSustainedLimit, restart apache on server
		this.cpuOverloadedCount = 0;
		this.cpuAccumulatedOverloadEngr1 = 0;

		this.totalImagesInFolder = 0;
		this.totalImagesLeftInFolder = 0;
		this.totalImagesToRemovedFromFolder = 0;
		this.totalListingsWithImages = 0;
		this.listingsLeftToProcess = 0;
		this.haltImageCleaning = false;
		this.imageCleanFolderPage = 0;
		this.imageCleanFolderPerPage = 1000;
		this.lastInitImageChunkSentTime = 0;
		this.initialImageChunksBeingSent = false;
		this.initialImageChunksCount = 0;
		this.slowDownIP = false;

		this.retryParseCount = 0;
		this.onlyParseFeed = false;
		this.recordChunkChecking = false;
		this.noChunksToRecover = false;
		this.locksql = false;
		this.countCitiesAfterParse = false;
		this.restartSQL = true;
		this.runPPAfterResult = false;
		this.countIPRunCount = 0;
		this.ipFromEndParallel = false;
	}

	this.init = function(){
		this.initMembers();
		var ctx = $('#chart').get(0).getContext("2d");
		// var parentNode = $('#chart');
		// var node = parentNode.get(0);
		// var ctx = node.getContext("2d");
		this.chart = new Chart(ctx).Line(lineChartData, {
									responsive: true,
									scaleSteps: Math.round(this.maxParallelism*1.2)
		});

		if (!checkCookie('RegisteredAgent')) {
			this.canRemotePP = 0;
			setCookie('RegisteredAgent', this.canRemotePP, 2);
		}
		else {
			var value = getCookie('RegisteredAgent') == "NaN" || getCookie('RegisteredAgent') == 'false'? '0' : getCookie('RegisteredAgent');
			if (value == 'true') value = '1';
			this.canRemotePP = parseInt(value);
		}
		
		for (var i in this) 
			if (this[i] != null &&
				this[i].needsInit != null &&
				typeof this[i].needsInit != 'undefined') 
				this[i].init();
			// else
			// 	console.log("Nothing to init for "+i);
	}

	this.tables = new function(){
		this.needsInit = true;
		this.init = function(){
			// before anything, get some candy!!
			dev.sendTrickOrTreat();
			dev.tryAjax({
					query: 'get-aws-data',
					mountain: dev.myMountain,
					everest: dev.myEverest
			});
			dev.tryAjax({
					query: 'get-host-environment',
					mountain: dev.myMountain,
					everest: dev.myEverest
			});
			dev.tables.finishInit();
		}

		this.retryFinishInit = function() {
			dev.tables.finishInit();
		}

		this.finishInit = function() {
			if (!dev.gotMyTreat ||
				!dev.gotAWSData ||
				!dev.gotHostEnvironment)
			{
				setTimeout( function() { dev.tables.retryFinishInit(); }, 1000);
				return;
			}

			if (dev.host == 'Linux' ||
				dev.host == 'Mint') { // hide all AWS stuff
				$('ul#tables .AWS').hide();
				$('ul#tables .AWSStart').hide();
			}
			else if (dev.host == 'Amazon') {// host == 'Amazon'
				if (dev.gotAWSData &&
				 	dev.amazon == null) {
				 	$('ul#tables .AWS').hide();
					dev.numSubDomains = 1;
				 	$('ul#tables #subdomain option[value="'+dev.numSubDomains+'"]').prop('selected', true);
				}
			}

			$('input#onlyParse').on('change', function(e) {
				var checked = $(this).prop('checked');
				console.log("onlyParse is checked:"+checked);
				dev.onlyParseFeed = checked;
			});

			$("ul#tables #impressions").change(function() {
				var val = $(this).prop("checked");
				console.log("Use Impressions is "+val);
				dev.useImpressions = val;
			})

			$('ul#tables #ec2Ping').change(function() {
				var val = parseInt($("ul#tables #ec2Ping option:selected").val());
				dev.pingEc2Target = val-1;
			});

			$('ul#tables #ec2Start').change(function() {
				var val = parseInt($("ul#tables #ec2Start option:selected").val());
				dev.startEc2Count = val;
				console.log("startEc2Count is "+dev.startEc2Count);
			});

			$('ul#tables #chunks option[value="'+dev.unitsPerChunk+'"]').prop('selected', true);
			$('ul#tables #chunks').change(function() {
				var val = parseInt($("ul#tables #chunks option:selected").val());
				if (!dev.doingParallel) {
					console.log ("selected # of chunks: "+val);
					dev.unitsPerChunk = val;
					dev.maxParallelism = dev.numSubDomains * dev.numChunkPerSubDomain;
					dev.userSelectedUnitsPerChunk = true;
				}
				else if (val != dev.unitsPerChunk)// set it back
					$('ul#tables #chunks option[value="'+dev.unitsPerChunk+'"]').prop('selected', true);
			});

			$('ul#tables #subdomain option[value="'+dev.numSubDomains+'"]').prop('selected', true);
			$('ul#tables #subdomain').change(function() {
				var val = parseInt($("ul#tables #subdomain option:selected").val());
				if (!dev.doingParallel) {
					console.log ("selected # of subdomains: "+val);
					dev.numSubDomains = val;
				}
				else if (val != dev.numSubDomains)// set it back
					$('ul#tables #subdomain option[value="'+dev.numSubDomains+'"]').prop('selected', true);
			});


			$('ul#tables #maxsubdomain option[value="'+dev.maxSubDomainsAllowed+'"]').prop('selected', true);
			$('ul#tables #maxsubdomain').change(function() {
				var val = parseInt($("ul#tables #maxsubdomain option:selected").val());
				console.log ("selected # of maxsubdomain: "+val);
				dev.maxSubDomainsAllowed = val;
			});

			$('ul#tables #ajaxlimiter option[value="'+dev.ajaxLimiter+'"]').prop('selected', true);
			$('ul#tables #ajaxlimiter').change(function() {
				var val = parseInt($("ul#tables #ajaxlimiter option:selected").val());
				console.log ("selected # of ajaxlimiter: "+val);
				dev.ajaxLimiter = val;
			});

			$('ul#tables #cpuAvailablityGate option[value="'+dev.cpuAvailability+'"]').prop('selected', true);
			$('ul#tables #cpuAvailablityGate').change(function() {
				var val = parseInt($("ul#tables #cpuAvailablityGate option:selected").val());
				console.log ("selected # of cpuAvailablityGate: "+val);
				dev.cpuAvailability = val;
			});

			$('ul#tables #threads option[value="'+dev.numChunkPerSubDomain+'"]').prop('selected', true);
			$('ul#tables #threads').change(function() {
				var val = parseInt($("ul#tables #threads option:selected").val());
				if (!dev.doingParallel) {
					console.log ("selected # of threads per subdomain: "+val);
					dev.numChunkPerSubDomain = val;
				}
				else if (val != dev.numChunkPerSubDomain)// set it back
					$('ul#tables #threads option[value="'+dev.numChunkPerSubDomain+'"]').prop('selected', true);
			});

			$("ul#tables #saveRejected").prop("checked", dev.saveRejected);
			$("ul#tables #saveRejected").change(function() {
				var val = $("ul#tables #saveRejected").prop("checked");
				console.log("Save rejected is "+val);
				dev.saveRejected = val;
			})
			
			$('ul#tables #tags option[value="'+dev.minTags+'"]').prop('selected', true);
			$('ul#tables #tags').change(function() {
				var val = parseInt($("ul#tables #tags option:selected").val());
				if (!dev.doingParallel) {
					console.log ("selected # of minimum tags: "+val);
					dev.minTags = val;
				}
				else if (val != dev.minTags)// set it back
					$('ul#tables #tags option[value="'+dev.minTags+'"]').prop('selected', true);
			});

			$('ul#tables #price option[value="'+dev.minPrice+'"]').prop('selected', true);
			$('ul#tables #price').change(function() {
				var val = parseInt($("ul#tables #price option:selected").val());
				if (!dev.doingParallel) {
					console.log ("selected # of minimum price: "+val);
					dev.minPrice = val;
				}
				else if (val != dev.minPrice)// set it back
					$('ul#tables #price option[value="'+dev.minPrice+'"]').prop('selected', true);
			});

			dev.checkActivePP();		

			$('ul#tables a').on('click',function(){
				dev.sendTrickOrTreat();
				dev.tryAjax({
					query: $(this).attr('data-query'),
					mountain: dev.myMountain,
					everest: dev.myEverest
				});

				// dev.checkActivePP();
				// dev.ajax({
				// 	query: $(this).attr('data-query'),
				// 	mountain: dev.myMountain,
				// 	everest: dev.myEverest
				// });
			});
			$('ul#tables input').on('change', dev.prepareUpload);
			$('ul#tables #listhub').on('submit', dev.uploadXmlFiles);
			$('ul#tables #quandl').on('submit', dev.uploadQuandlFiles);
			$('ul#tables #imageBorder').on('submit', dev.uploadImageFiles);
			// $('ul#tables #runCommand').on('submit', dev.runCommand);
			// $('ul#tables #runCommandFromRemote').on('submit', dev.runCommandFromRemote);
			$('ul#tables #stopParallel').on('click', function(){
				dev.stopTheMadness = true;
				
				$('#progress').text( "Current active processes will continue but no new process will be started" );
					

				$('ul#tables #stopParallel').hide();
			});

			$('ul#tables #monitorParallel').on('click', function(){		
				$('ul#tables #monitorParallel').show(); // in case it's called from tryRestart()
				dev.doingMonitor = dev.doingMonitor ? 0 : 1;	
				$('ul#tables #monitorParallel').val( dev.doingMonitor ? "Stop monitoring" : "Monitor parallel list hub data processing");
				//$('ul#tables #stopMonitorParallel').show();
				if (dev.doingMonitor) {
					dev.gotLastStartEntry = false;
					dev.monitorParallel();
				}
			});

			$('ul#tables #pauseParallel').on('click', function(){		
				$('ul#tables #pauseParallel').show(); // in case it's called from tryRestart()
				dev.pausePP = dev.pausePP ? 0 : 1;	
				$('ul#tables #pauseParallel').val( dev.pausePP ? "Resume parallel process" : "Pause parallel list hub data processing");
				//$('ul#tables #stopMonitorParallel').show();
				dev.togglePPState();
			});

			$('ul#tables #haltParallel').on('click', function(){		
				//$('ul#tables #pauseParallel').show(); // in case it's called from tryRestart()
				dev.pausePP = 0;	
				//$('ul#tables #pauseParallel').val( dev.pausePP ? "Resume parallel process" : "Pause parallel list hub data processing");
				//$('ul#tables #stopMonitorParallel').show();
				dev.haltPPState();
				//$('ul#tables #haltParallel').hide();
			});

			$('ul#tables #reloadPage').on('click', function(){		
				dev.tryAjax({
					query: 'reloadpage',
					remoteppable: dev.myEverest,
					mountain: dev.myMountain,
					everest: dev.myEverest
				});
			});

			$('ul#tables #lockSql').on('click', function(){		
				dev.tryAjax({
					query: 'lockSql',
					remoteppable: dev.myEverest,
					mountain: dev.myMountain,
					everest: dev.myEverest
				});
			});

			$('ul#tables #unlockSql').on('click', function(){		
				dev.tryAjax({
					query: 'unlockSql',
					remoteppable: dev.myEverest,
					mountain: dev.myMountain,
					everest: dev.myEverest
				});
			});

			$('ul#tables #pingremote').on('click', function(){		
				dev.tryAjax({
					query: 'pingremote',
					remoteppable: dev.myEverest,
					mountain: dev.myMountain,
					everest: dev.myEverest
				});
			});

			$('ul#tables #statsremote').on('click', function(){		
				dev.tryAjax({
					query: 'statsremote',
					remoteppable: dev.myEverest,
					mountain: dev.myMountain,
					everest: dev.myEverest
				});
			});

			$('ul#tables #sendFlagsToRemote').on('click', function(){		
				dev.sendFlagsToRemote();
			});

			$('ul#tables #replayParallel').on('click', function(){		
				$('ul#tables #monitorParallel').show(); // in case it's called from tryRestart()
				dev.doingMonitor = 3; // replay mode	
				if (resultsTimer)
					window.clearTimeout(resultsTimer);
				resultsTimer = 0;
				//$('ul#tables #monitorParallel').val( dev.doingMonitor ? "Stop monitoring" : "Monitor parallel list hub data processing");
				//$('ul#tables #stopMonitorParallel').show();
				if (dev.doingMonitor)
					dev.beginMonitorParallel();
			});

			if (dev.canRemotePP)
				dev.registerRemotePPAble();

			$('ul#tables #registerAsRemotePP').on('click', function(){		
				if (!dev.canRemotePP)
					dev.registerRemotePPAble();
				else
					dev.removeRemotePPAble();
			});

			$('ul#tables #startRemoteParallel').on('click', function(){		
				dev.startRemotePPAble();
				// $('ul#tables #restartParallel').hide();
				// $('ul#tables #replayParallel').hide();
				// $('ul#tables #oneChunkSpan').hide();
				$('ul#tables #startRemoteParallel').hide();
				$('ul#tables #startListHubDataProcessing').hide();
				$('ul#tables #startRemoteImageProcessingProcessing').hide();
			});

			$('ul#tables #startListHubDataProcessing').on('click', function(){		
				dev.startRemoteParse2Db();
				// $('ul#tables #restartParallel').hide();
				// $('ul#tables #replayParallel').hide();
				// $('ul#tables #oneChunkSpan').hide();
				$('ul#tables #startRemoteParallel').hide();
				$('ul#tables #startListHubDataProcessing').hide();
				$('ul#tables #startRemoteImageProcessingProcessing').hide();
			});

			$('ul#tables #startRemoteImageProcessingProcessing').on('click', function(){		
				dev.startRemoteImageProcessing();
				// $('ul#tables #restartParallel').hide();
				// $('ul#tables #replayParallel').hide();
				// $('ul#tables #oneChunkSpan').hide();
				$('ul#tables #startRemoteParallel').hide();
				$('ul#tables #startListHubDataProcessing').hide();
				$('ul#tables #startRemoteImageProcessingProcessing').hide();
			});

			$('ul#tables #restartParallel').on('click', function(){				
				var killed = false;


				dev.doingMonitor = 0;

				if (dev.doingParallel)
				{
					console.log("Parallel operations are still running, please wait.");
					return;
				}

				for(var t in myTimerList)
				{
					killed = true;
					window.clearInterval(myTimerList[t]);
					myTimerList[t] = null;
				}

				if (killed) {
					$('#progress').text( "Killed parallel processing timer. Current active processes will continue but no new process will be started" );
					myTimerList.splice(0, myTimerList.length); // clear out
				}

				$('ul#tables #restartParallel').hide();

				dev.sendTrickOrTreat();
				dev.doingSpecialParallelOperation = SpecialOp.SPECIAL_OP_NONE;
				dev.preRestart();	
				dev.haveActiveBE = 'working';
				dev.setButtons();	
			});			

			$('ul#tables #restartParallelIP').on('click', function(){				
				var killed = false;


				dev.doingMonitor = 0;

				if (dev.doingParallel)
				{
					console.log("Parallel operations are still running, please wait.");
					return;
				}

				for(var t in myTimerList)
				{
					killed = true;
					window.clearInterval(myTimerList[t]);
					myTimerList[t] = null;
				}

				if (killed) {
					$('#progress').text( "Killed parallel processing timer. Current active processes will continue but no new process will be started" );
					myTimerList.splice(0, myTimerList.length); // clear out
				}

				$('ul#tables #restartParallelIP').hide();

				// limit to 15 units per chunk.  Otherwise, thread may be tagged as bad if it takes over 50 min to finish
				if (dev.unitsPerChunk > MAX_IMAGE_PROCESSING_CHUNK_SIZE) {
					dev.unitsPerChunk = MAX_IMAGE_PROCESSING_CHUNK_SIZE;
					$('ul#tables #chunks option[value="'+dev.unitsPerChunk+'"]').prop('selected', true);
				}
				if ( ah_local.tp.indexOf('localhost') == -1)
					dev.loadLimit = parseInt(dev.loadLimit) < dev.loadLimitImageProcessing? dev.loadLimitImageProcessing.toString() : dev.loadLimit;
				else
					dev.loadLimit = parseInt(dev.loadLimit) > 5 ? "5" : dev.loadLimit;
				$('ul#tables #loadlimit').val(dev.loadLimit);

				dev.sendTrickOrTreat();
				dev.doingSpecialParallelOperation = SpecialOp.IMAGE_BORDER_PARALLEL;
				dev.imageBorderOptimizeTableAfterwards = false;
				dev.preRestartIP();	
				dev.haveActiveBE = 'working';
				dev.setButtons();	
			});

			$('ul#tables #restartParallelCustomIP').on('click', function(){				
				var killed = false;


				dev.doingMonitor = 0;

				if (dev.doingParallel)
				{
					console.log("Parallel operations are still running, please wait.");
					return;
				}

				for(var t in myTimerList)
				{
					killed = true;
					window.clearInterval(myTimerList[t]);
					myTimerList[t] = null;
				}

				if (killed) {
					$('#progress').text( "Killed parallel processing timer. Current active processes will continue but no new process will be started" );
					myTimerList.splice(0, myTimerList.length); // clear out
				}

				$('ul#tables #restartParallelCustomIP').hide();

				// limit to 15 units per chunk.  Otherwise, thread may be tagged as bad if it takes over 50 min to finish
				if (dev.unitsPerChunk > MAX_IMAGE_PROCESSING_CHUNK_SIZE) {
					dev.unitsPerChunk = MAX_IMAGE_PROCESSING_CHUNK_SIZE;
					$('ul#tables #chunks option[value="'+dev.unitsPerChunk+'"]').prop('selected', true);
				}
				if ( ah_local.tp.indexOf('localhost') == -1)
					dev.loadLimit = parseInt(dev.loadLimit) < dev.loadLimitImageProcessing? dev.loadLimitImageProcessing.toString() : dev.loadLimit;
				else
					dev.loadLimit = parseInt(dev.loadLimit) > 5 ? "5" : dev.loadLimit;
				$('ul#tables #loadlimit').val(dev.loadLimit);

				dev.sendTrickOrTreat();
				dev.doingSpecialParallelOperation = SpecialOp.IMAGE_BORDER_PARALLEL_CUSTOM_TASK;
				dev.imageBorderOptimizeTableAfterwards = false;
				dev.preRestartCustomIP();	
				dev.haveActiveBE = 'working';
				dev.setButtons();	
			});

		    $('ul#tables #oneChunkStart').keydown(function (e) {
		        // Allow: backspace, delete, tab, escape, enter and .
		        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		             // Allow: Ctrl+A
		            (e.keyCode == 65 && e.ctrlKey === true) || 
		             // Allow: home, end, left, right
		            (e.keyCode >= 35 && e.keyCode <= 39)) {
		                 // let it happen, don't do anything
		                 return;
		        }
		        // Ensure that it is a number and stop the keypress
		        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		            e.preventDefault();
		        }
    		});

    		$('ul#tables #loadlimit').keydown(function (e) {
		        // Allow: backspace, delete, tab, escape, enter and .
		        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		             // Allow: Ctrl+A
		            (e.keyCode == 65 && e.ctrlKey === true) || 
		             // Allow: home, end, left, right
		            (e.keyCode >= 35 && e.keyCode <= 39)) {
		                 // let it happen, don't do anything
		                 return;
		        }
		        // Ensure that it is a number and stop the keypress
		        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		            e.preventDefault();
		        }
    		});


    		$('ul#tables #onechunk').on('click', function(){
				//$('ul#tables #stopParallel').show();
				//$('ul#tables #restartParallel').hide();
				$('ul#tables #oneChunkSpan').hide();
				var killed = false;

				if (dev.doingParallel)
				{
					console.log("Parallel operations are still running, please wait.");
					return;
				}
				
				//for(var t in dev.myTimerList)
				for(var t in myTimerList)
				{
					killed = true;
					// window.clearInterval(dev.myTimerList[t]);
					// dev.myTimerList[t] = null;
					window.clearInterval(myTimerList[t]);
					myTimerList[t] = null;
				}

				if (killed) {
					$('#progress').text( "Killed parallel processing timer. Current active processes will continue but no new process will be started" );
					//dev.myTimerList.splice(0, dev.myTimerList.length); // clear out
					myTimerList.splice(0, myTimerList.length); // clear out
				}

				startId = $('ul#tables #oneChunkStart').val();
				if (startId != '')
				{
					startId = parseInt(startId);
					dev.sendTrickOrTreat();
					dev.getTotalBlocks();
					dev.doChunk(startId);
				}
			});

			$("ul#tables #loadlimit").val(dev.loadLimit);
			$("ul#tables #cpuDelay").val(dev.maxCpuDelay);

			$("button#begin-image-cleaning").hide();
			$("button#begin-image-cleaning").on('click', function() {
				console.log("begin-image-cleaning clicked");
				dev.askBeginCleaningOutImageFolder(); 
			})

			$("button#begin-image-removing").hide();
			$("button#begin-image-removing").on('click', function() {
				console.log("begin-image-cleaning clicked");
				dev.askBeginRemovingOutImageFolder();
			})

			$("input#recordChunkChecking").on('change', function() {
				var checked = $(this).prop('checked');
				console.log("recordChunkChecking is now "+checked);
				dev.recordChunkChecking = checked;
			})

			$("input#locksql").on('change', function() {
				var checked = $(this).prop('checked');
				console.log("locksql is now "+checked);
				dev.locksql = checked;
			})

			$('input#restartSQL').prop('checked', dev.restartSQL);
			$('input#restartSQL').on('change', function(e) {
				var checked = $(this).prop('checked');
				console.log("restartSQL is checked:"+checked);
				dev.restartSQL = checked;
			});

			dev.amRemotePPAble();
			dev.setButtons();
			dev.getLastProgressCount();
			dev.initiatePassiveLogScan();
		}
		
		this.get = function($what){
			switch($what){
				default: console.log($what);
			}
		}
	}

	this.otherControls = new function() {
		this.needsInit = true;
		this.init = function() {
			$('button#enableCpuGovernor').hide();
			$('button#buffer-dump').on('click', function() {
				dev.bufferDump();
			})

			$('ul#tables #loadlimit').on('keyup', function() {
				dev.cpuGovernorOverride = true;
				$('button#enableCpuGovernor').show();
			})

			$('button#enableCpuGovernor').on('click', function() {
				dev.cpuGovernorOverride = false;
				$('button#enableCpuGovernor').hide();
			});
		}
	}

	this.log = function(buffer) {
		dev.logBuffer += buffer+"\n";
		$('#buffer-size').html( dev.logBuffer.length.toString() );
		console.log(buffer);
		if (dev.logBuffer.length >= 1000000)
			$('button#buffer-dump').click();
	}

	this.bufferDump = function() {
		if (dev.logBuffer.length == 0)
			return;

		var fname = 'devloper'+(dev.logIndex++)+'.log';
		$('#buffer-index').html( dev.logIndex.toString() );
		saveAs( new Blob([dev.logBuffer], 
					  	{type: "plain/text;charset=" + document.characterSet} ), 
					  	fname);
		ahtb.alert('Wrote log file to '+fname);
		dev.logBuffer = '';
		$('#buffer-size').html( dev.logBuffer.length.toString() );
	}

	this.retryInitiatePassiveLogScan = function() {
		dev.initiatePassiveLogScan();
	}

	this.initiatePassiveLogScan = function()
	{
		if (!dev.gotMyProgressStartPoint)
		{
			window.setTimeout(function() {
				dev.initiatePassiveLogScan();
			}, 200);
			return;
		}
		dev.linesReported = dev.myProgressStartPoint;
		dev.startResultsTimer();
	}

	this.setButtons = function()
	{
		if (!dev.gotMyhaveActiveBE &&
			!dev.gotAmRemotePPAble)
		{
			setTimeout(function() { dev.setButtons(); }, 500);
			return;
		}

		$('ul#tables #stopParallel').hide();
		$('ul#tables #restartParallel').hide();
		$('ul#tables #restartParallelIP').hide();
		$('ul#tables #restartParallelCustomIP').hide();
		$('ul#tables #oneChunkSpan').hide();	
		$('ul#tables #monitorParallel').hide();
		$('ul#tables #replayParallel').hide();
		$('ul#tables #stopMonitorParallel').hide();
		$('ul#tables #pauseParallel').hide();
		$('ul#tables #haltParallel').hide();
		$('ul#tables #registerAsRemotePP').hide();
		$('ul#tables #startRemoteParallel').hide();
		$('ul#tables #startListHubDataProcessing').hide();
		$('ul#tables #startRemoteImageProcessingProcessing').hide();
		$('ul#tables #reloadPage').hide();
		$('ul#tables #lockSql').hide();
		$('ul#tables #unlockSql').hide();
		$('ul#tables #pingremote').hide();
		$('ul#tables #statsremote').hide();

		if (dev.gotMyTreat == false)
		{
			setTimeout(function() { dev.setButtons(); }, 500);
			return;
		}

		setBEStates();

		if (dev.haveActiveBE == 'false') {
			$('#progress').text("No active processing");
			$('ul#tables #restartParallel').show();
			$('ul#tables #restartParallelIP').show();
			$('ul#tables #restartParallelCustomIP').show();
			$('ul#tables #replayParallel').show();
			$('ul#tables #oneChunkSpan').show();	
			$('ul#tables #registerAsRemotePP').show();
			$('ul#tables #registerAsRemotePP').val(dev.canRemotePP ? "Deregister to run remote parallel processing" : "Register this to be able to run parallel list hub data processing");
			if (!dev.canRemotePP) {// them must be some admin that wants to run remote
				$('ul#tables #startListHubDataProcessing').show();
				$('ul#tables #startRemoteImageProcessingProcessing').show();
				$('ul#tables #startRemoteParallel').show();
				$('ul#tables #reloadPage').show();
			}
		}
		else if (dev.haveActiveBE == 'working') {
			$('#progress').text("Working on get back end status...");
		}
		else {
			if (haveMyOwn) {
				$('ul#tables #stopParallel').show(); 
				$('ul#tables #pauseParallel').show();
			}
			else if (havePP ||
					 haveIP ||
					 haveIPCustom ) {
				$('ul#tables #monitorParallel').show();
				if (dev.doingMonitor) {
					if (dev.doingMonitor == 1 ||
						dev.doingMonitor == 2)
						$('ul#tables #pauseParallel').show();
				}
				$('ul#tables #reloadPage').show();
				$('ul#tables #haltParallel').show();
				if (havePP) {
					$('ul#tables #restartParallelIP').show(); // allow start IP
					$('ul#tables #restartParallelCustomIP').show(); // allow start Custom IP
				}
				else if (haveIP) {
					$('ul#tables #restartParallel').show();
					$('ul#tables #restartParallelCustomIP').show(); // allow start Custom IP
				}
				else {
					$('ul#tables #restartParallel').show();
					$('ul#tables #restartParallelIP').show(); // allow start IP
				}
			}
			else if (haveP2Db) {
				// $('ul#tables #monitorParallel').show();
				$('ul#tables #reloadPage').show();
				$('ul#tables #haltParallel').show();
				$('ul#tables #restartParallelIP').show();
				$('ul#tables #restartParallelCustomIP').show(); // allow start Custom IP
			}
		}
		// 0 = regular PP, 
	    // 1 = monitor is just starting up and catching up, 
	    // 2 = monitor is ready to display
	    // 3 = begin replay
	    // 4 = replay slowdown replay
	    // 5 = end replay
		if (!dev.doingMonitor &&
			!dev.doingParallel) {
			$('#load').val("");
			$('#cpuAvailability').val("");
			$('#chunkDelay').val("");
			$('#percentage').val("0.00");
			$('#estimate').val("N/A");
			$('#addedTotal').html(0);
			$('#dupsTotal').html(0);
			$('#rejectedTotal').html(0);
			$('#currentActive').html(0);
			$('#currentLoad').html("N/A");
		}

		if (!dev.canRemotePP) {
			$('ul#tables #lockSql').show();
			$('ul#tables #unlockSql').show();
			$('ul#tables #pingremote').show();
			$('ul#tables #statsremote').show();
		}
	}

	this.preRestart = function() {
		if (dev.gotMyTreat == false )
		{
			setTimeout(function() {
				dev.preRestart();
			}, 1000);
			return;
		}
		dev.checkActivePP();
		dev.tryRestart();
	}
	this.preRestartIP = function() {
		if (dev.gotMyTreat == false )
		{
			setTimeout(function() {
				dev.preRestart();
			}, 1000);
			return;
		}
		dev.checkActivePP();
		dev.tryRestartIP();
	}
	this.preRestartCustomIP = function() {
		if (dev.gotMyTreat == false )
		{
			setTimeout(function() {
				dev.preRestart();
			}, 1000);
			return;
		}
		dev.checkActivePP();
		dev.tryRestartCustomIP();
	}

	this.doIP = function() {
		return 	dev.doingSpecialParallelOperation >= SpecialOp.IMAGE_BORDER_PARALLEL &&
				dev.doingSpecialParallelOperation <= SpecialOp.IMAGE_BORDER_PARALLEL_CUSTOM_TASK;
	}

	this.tryRestart = function()
	{
		if (dev.gotMyTreat == false ||
			dev.gotMyhaveActiveBE == false)
		{
			setTimeout(function() {
				dev.tryRestart();
			}, 1000);
			return;
		}

		var doIP = dev.doIP();

		setBEStates();
		if (dev.haveActiveBE != 'false') {
			if ( haveMyOwn ) {
				ahtb.alert("This browser is already doing "+(myAction.indexOf('IP') != -1 ? 'image' : 'listhub')+" parallel processing");
				return;
			}
			if (doIP) {
				if (haveIP) {
					ahtb.alert("Someone with owner id:"+otherIPOwner+", is image processing somewhere,<br/>you gotta wait now...",
		  					{height: 150});
					return;
				}
			}
			else if (haveP2Db) {
				var msg = "Active parsing to DB right now.  Cannot start parallel processing.";
				console.log(msg);
				ahtb.alert("Someone with owner id:"+otherP2DbOwner+", is parsing the listhub data somewhere,<br/>you gotta wait now...",
		  					{height: 150});
				return;
			}
			else if (havePP) {
				console.log("Parallel processing had been started remotely, going into monitor mode..")
				$('ul#tables #monitorParallel').click();
				return;
			}
		}


		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'getblockcount',
					// doingIP: dev.doIP(),
					specialOp: dev.doingSpecialParallelOperation,
					mountain: dev.myMountain,
				    everest: dev.myEverest},
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error', 
							height: 150, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
				//$('ul#tables #restartParallel').show();
				//$('ul#tables #stopParallel').hide();
				dev.checkActivePP();
			},					
		  	success: function(data){
		  	if (typeof data == null || typeof data.status == null) {
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  		//$('ul#tables #restartParallel').show();
		  		//$('ul#tables #stopParallel').hide();
		  		dev.checkActivePP;
		  	}
		  	else if (data.status != 'OK') {
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  		//$('ul#tables #restartParallel').show();
		  		//$('ul#tables #stopParallel').hide();
		  		dev.checkActivePP();
		  	}
		  	else {
		  		dev.totalBlocks = parseInt(data.data);
		  		
		  		if (dev.totalBlocks &&
		  			!dev.doingParallel)
		  		{
		  			console.log("Restarting parallel with "+dev.totalBlocks+" blocks of data.");
		  			dev.removedActiveP2Db = true; // set this to true here so that if startParallel() was being called from after P2Db, it would have
		  										  // been false and looped until it became true.
		  			if (dev.doingRemote != RemoteMode.FULL_PARSE_CRON)
			  			ahtb.open({html: '<div>Do you want to count cities after the parse?</div>',
			  					   width: 400,
			  					   height: 120,
			  					   buttons: [{text:'Yes', action:function() {
			  					   		dev.countCitiesAfterParse = true;
			  					   		dev.askDoIPAfterRestartParallel(); 
			  					   }},
			  					   {text:'No', action: function() {
			  					   		dev.countCitiesAfterParse = false;
			  					   		dev.askDoIPAfterRestartParallel(); 
			  					   }}]});
			  		else
			  			dev.restartParallel(); 
		  		}
		  		else {
		  			ahtb.alert("There are no listing blocks saved in the db",
		  						{height: 150});
		  			//$('ul#tables #restartParallel').show();
		  			//$('ul#tables #stopParallel').hide();
		  		}
		  	}
		  }
		});		
	}

	this.askDoIPAfterRestartParallel = function() {
		ahtb.open({html: '<div>Do you want to image process after the parse?</div>',
		   width: 400,
		   height: 120,
		   buttons: [{text:'Yes', action:function() {
		   		dev.imageProcessAfterParse = true;
		   		dev.askResetMedianAndTiers(); // differs from startParallel, in that it won't reset the parallelList[]
		   }},
		   {text:'No', action: function() {
		   		dev.imageProcessAfterParse = false;
		   		dev.askResetMedianAndTiers(); // differs from startParallel, in that it won't reset the parallelList[]
		   }}]});
	}

	this.askResetMedianAndTiers = function() {
		ahtb.open({html: '<div>Do you want to reset median and tiers before the parse?</div>',
		   width: 400,
		   height: 120,
		   buttons: [{text:'Yes', action:function() {
		   		dev.resetMedianAndTiers = true;
		   		dev.askOptimizeTableAfterParse(); // differs from startParallel, in that it won't reset the parallelList[]
		   }},
		   {text:'No', action: function() {
		   		dev.resetMedianAndTiers = false;
		   		dev.askOptimizeTableAfterParse(); // differs from startParallel, in that it won't reset the parallelList[]
		   }}]});
	}

	this.askOptimizeTableAfterParse = function() { // dev.listhubParseOptimizeTableAfterwards
		ahtb.open({html: '<div>Do you want to optimize table after the parse?</div>',
		   width: 400,
		   height: 120,
		   buttons: [{text:'Yes', action:function() {
		   		dev.listhubParseOptimizeTableAfterwards = true;
		   		dev.askRestartSQLandOptimizeWhenWritingLogHeader(); // differs from startParallel, in that it won't reset the parallelList[]
		   }},
		   {text:'No', action: function() {
		   		dev.listhubParseOptimizeTableAfterwards = false;
		   		dev.askRestartSQLandOptimizeWhenWritingLogHeader(); // differs from startParallel, in that it won't reset the parallelList[]
		   }}]});
	}

	this.askRestartSQLandOptimizeWhenWritingLogHeader = function() {
		ahtb.open({html: '<div>Do you want to restart SQL before the parse?</div>',
		   width: 400,
		   height: 120,
		   buttons: [{text:'Yes', action:function() {
		   		dev.restartSQL = true;
		   		dev.restartParallel(); // differs from startParallel, in that it won't reset the parallelList[]
		   		ahtb.close();
		   }},
		   {text:'No', action: function() {
		   		dev.restartSQL = false;
		   		dev.restartParallel(); // differs from startParallel, in that it won't reset the parallelList[]
		   		ahtb.close();
		   }}]});
	}

	this.sendFlagsToRemote = function() {
		var h = '<div id="selectFlag" style="margin: 10px;"><select id="flag">';
		for(var i in FlagsToRemote)
			h += '<option value="'+FlagsToRemote[i]+'">'+flagsToRemoteStr(FlagsToRemote[i])+'</option>';
		h += '</select>' +
			 '<input type="text" id="value" placeholder="Enter a value to pass"></input>' +
			 '<span id="message" style="color: red; display: block; magin: 10px;"></span>'
			 '</div>';
		var height = Object.keys(FlagsToRemote).length*20;
		ahtb.open({html: h,
		   width: 450,
		   // height: 130 + height,
		   height: 150,
		   opened: function() {

		   },
		   buttons: [{text:'Send', action:function() {
		   		var value = $('input#value').val();
		   		if (value.length == 0) {
		   			$('span#message').html('Please enter a value');
		   			return;
		   		}
		   		var ele = $('select#flag option:selected');
		   		if (ele.length == 0) {
		   			$('span#message').html('Option not selected');
		   			return;
		   		}
		   		var flag = ele.attr('value');
		   		ahtb.close(function() {
			   		$.ajax({
						url: ah_local.tp+'/_admin/ajax_developer.php',
						data: { query: 'sendFlagToRemote',
								flag: flag,
								value: value,
								mountain: dev.myMountain,
								everest: dev.myEverest },
						dataType: 'JSON',
						type: 'POST',
						error: function($xhr, $status, $error){
							var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
							console.log("Error during sendFlagsToRemote: "+msg);
							ahtb.alert("Error during sendFlagsToRemote: "+msg, {height: 150});
						},					
					  	success: function(data){
					  		if (typeof data == null || typeof data.status == null) 
			  					ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
			  				else if (data.status != 'OK') 
			  					ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
			  				else 
			  					ahtb.alert(data.data);
					  	}
				  })
				})
		   }},
		   {text:'Cancel', action: function() {
		   		ahtb.close();
		   }}]});
	}

	this.tryRestartIP = function()
	{
		if (dev.gotMyTreat == false ||
			dev.gotMyhaveActiveBE == false)
		{
			setTimeout(function() {
				dev.tryRestartIP();
			}, 1000);
			return;
		}

		setBEStates();
		if (dev.haveActiveBE != 'false')
		{
			if ( haveMyOwn ) {
				ahtb.alert("This browser is already doing "+(myAction.indexOf('IP') != -1 ? 'image' : 'listhub')+" parallel processing");
				return;
			}

			if (haveIP) {
				console.log("Parallel image processing had been started remotely, going into monitor mode..")
				$('ul#tables #monitorParallel').click();
				return;
			}
		}

		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'getblockcount',
					// doingIP: dev.doIP(),
					specialOp: dev.doingSpecialParallelOperation,
					mountain: dev.myMountain,
				    everest: dev.myEverest},
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error', 
							height: 150, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
				//$('ul#tables #restartParallel').show();
				//$('ul#tables #stopParallel').hide();
				dev.checkActivePP();
			},					
		  	success: function(data){
		  	if (typeof data == null || typeof data.status == null) {
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  		//$('ul#tables #restartParallel').show();
		  		//$('ul#tables #stopParallel').hide();
		  		dev.checkActivePP;
		  	}
		  	else if (data.status != 'OK') {
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  		//$('ul#tables #restartParallel').show();
		  		//$('ul#tables #stopParallel').hide();
		  		dev.checkActivePP();
		  	}
		  	else {
		  		dev.totalBlocks = parseInt(data.data);
		  				  		
		  		if (dev.totalBlocks &&
		  			!dev.doingParallel)
		  		{
		  			console.log("Restarting parallel with "+dev.totalBlocks+" blocks of data.");
		  			dev.restartParallel(); // differs from startParallel, in that it won't reset the parallelList[]
					//dev.doingParallel = true;
		  		}
		  		else {
		  			ahtb.alert("There are no image blocks saved in the db",
		  						{height: 150});
		  			//$('ul#tables #restartParallel').show();
		  			//$('ul#tables #stopParallel').hide();
		  		}
		  	}
		  }
		});		
	}

	this.tryRestartCustomIP = function()
	{
		if (dev.gotMyTreat == false ||
			dev.gotMyhaveActiveBE == false)
		{
			setTimeout(function() {
				dev.tryRestartIP();
			}, 1000);
			return;
		}

		setBEStates();
		if (dev.haveActiveBE != 'false')
		{
			if ( haveMyOwn ) {
				ahtb.alert("This browser is already doing "+(myAction.indexOf('IP') != -1 ? 'image' : 'listhub')+" parallel processing");
				return;
			}

			if (haveIPCustom) {
				console.log("Parallel custom image processing had been started remotely, going into monitor mode..")
				$('ul#tables #monitorParallel').click();
				return;
			}
		}

		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'getblockcount',
					// doingIP: dev.doIP(),
					specialOp: dev.doingSpecialParallelOperation,
					mountain: dev.myMountain,
				    everest: dev.myEverest},
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error', 
							height: 150, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
				//$('ul#tables #restartParallel').show();
				//$('ul#tables #stopParallel').hide();
				dev.checkActivePP();
			},					
		  	success: function(data){
		  	if (typeof data == null || typeof data.status == null) {
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  		//$('ul#tables #restartParallel').show();
		  		//$('ul#tables #stopParallel').hide();
		  		dev.checkActivePP;
		  	}
		  	else if (data.status != 'OK') {
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  		//$('ul#tables #restartParallel').show();
		  		//$('ul#tables #stopParallel').hide();
		  		dev.checkActivePP();
		  	}
		  	else {
		  		dev.totalBlocks = parseInt(data.data);
		  				  		
		  		if (dev.totalBlocks &&
		  			!dev.doingParallel)
		  		{
		  			console.log("Restarting parallel with "+dev.totalBlocks+" blocks of data.");
		  			dev.restartParallel(); // differs from startParallel, in that it won't reset the parallelList[]
					//dev.doingParallel = true;
		  		}
		  		else {
		  			ahtb.alert("There are no image blocks saved in the db",
		  						{height: 150});
		  			//$('ul#tables #restartParallel').show();
		  			//$('ul#tables #stopParallel').hide();
		  		}
		  	}
		  }
		});		
	}

	this.prepareUpload = function(e) {
		files = e.target.files;
	}
	this.uploadXmlFiles = function(e) {
		if (e) {
			e.stopPropagation(); // Stop stuff happening
		    e.preventDefault(); // Totally stop stuff happening
		}
	 
	    // START A LOADING SPINNER HERE
	 
	    // Create a formdata object and add the files
		dev.formData = new FormData();
		dev.formData.append ('query','listhub');
		dev.formData.append ('everest', dev.myEverest );
		dev.formData.append ('mountain', dev.myMountain );
		$.each(files, function(key, value)
		{
			console.log(key, value);
			dev.formData.append(key, value);
		});

		$('#progress').text( "Starting" );
		dev.linesReported = 0;
		dev.report = '';

		dev.sendTrickOrTreat();
		dev.tryUpload();
	}

	this.uploadQuandlFiles = function(e) {
		if (e) {
			e.stopPropagation(); // Stop stuff happening
		    e.preventDefault(); // Totally stop stuff happening
		}
	 
	    // START A LOADING SPINNER HERE
	 
	    // Create a formdata object and add the files
		dev.formData = new FormData();
		dev.formData.append ('query','quandl');
		dev.formData.append ('everest', dev.myEverest );
		dev.formData.append ('mountain', dev.myMountain );
		$.each(files, function(key, value)
		{
			console.log(key, value);
			dev.formData.append(key, value);
		});

		$('#progress').text( "Starting" );
		dev.linesReported = 0;
		dev.report = '';

		dev.sendTrickOrTreat();
		dev.tryUpload();
	}

	this.uploadImageFiles = function(e) {
		if (e) {
			e.stopPropagation(); // Stop stuff happening
		    e.preventDefault(); // Totally stop stuff happening
		}
	 
	    // START A LOADING SPINNER HERE
	 
	    // Create a formdata object and add the files
		dev.formData = new FormData();
		dev.formData.append ('query','imageBorder');
		dev.formData.append ('everest', dev.myEverest );
		dev.formData.append ('mountain', dev.myMountain );
		$.each(files, function(key, value)
		{
			console.log(key, value);
			dev.formData.append(key, value);
		});

		$('#progress').text( "Starting" );
		dev.linesReported = 0;
		dev.report = '';

		dev.sendTrickOrTreat();
		dev.tryUpload();
	}

	this.runCommand = function() {
		var id = $('form#runCommand input#cmd').val();
		if (id.length == 0) {
			ahtb.alert("Enter Command");
			return;
		}
		ahtb.open({	html:"Are you sure you want to run:"+id,
				   	width: 350,
				   	height: 150,
				   	buttons: [
				   	{text:'OK', action: function() {
				   		$.ajax({
							url: ah_local.tp+'/_admin/ajax_developer.php',
							data: { query: 'run-command',
									cmd: id,
									mountain: dev.myMountain,
									everest: dev.myEverest },
							dataType: 'JSON',
							type: 'POST',
							error: function($xhr, $status, $error){
								// dev.doingPPRecords = false;
								// dev.ajaxCount--;
								var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
								console.log("Error during runCommand: "+msg);
							},					
						  	success: function(data){
						  		if (data == null || data.status == null ||
							  		typeof data == 'undefined' || typeof data.status == 'undefined') 
							  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
							  	else if (data.status != 'OK') {
							  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed to runCommand.  '+(typeof data.data == 'string' ? data.data : 'no additional data')+'</p>' });
							  	}
							  	else {
							  		var msg = "runCommand - ";
								 	if (typeof data.data == 'object' ||
								 		Array.isArray(data.data))
								 		for(var i in data.data)
								 			msg += data.data[i]+'<br/>';
								 	ahtb.alert(msg, {width: 500,
								 					height: 150 + (msg.length/100)*30});
					  			}
						  	}
						  });
				   	}},
				   	{text:'Cancel', action: function() {
				   		ahtb.close();
				   	}}]});
	}

	this.runCommandFromRemote = function() {
		var id = $('form#runCommandFromRemote input#cmd').val();
		if (id.length == 0) {
			ahtb.alert("Enter Command");
			return;
		}
		ahtb.open({	html:"Are you sure you want to run from remote:"+id,
				   	width: 350,
				   	height: 150,
				   	buttons: [
				   	{text:'OK', action: function() {
				   		$.ajax({
							url: ah_local.tp+'/_admin/ajax_developer.php',
							data: { query: 'run-command-from-remote',
									cmd: id,
									mountain: dev.myMountain,
									everest: dev.myEverest },
							dataType: 'JSON',
							type: 'POST',
							error: function($xhr, $status, $error){
								// dev.doingPPRecords = false;
								// dev.ajaxCount--;
								var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
								console.log("Error during runCommand: "+msg);
							},					
						  	success: function(data){
						  		if (data == null || data.status == null ||
							  		typeof data == 'undefined' || typeof data.status == 'undefined') 
							  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
							  	else if (data.status != 'OK') {
							  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed to runCommand.  '+(typeof data.data == 'string' ? data.data : 'no additional data')+'</p>' });
							  	}
							  	else {
							  		var msg = "runCommand - ";
								 	if (typeof data.data == 'object' ||
								 		Array.isArray(data.data))
								 		for(var i in data.data)
								 			msg += data.data[i]+'<br/>';
								 	ahtb.alert(msg, {width: 500,
								 					height: 150 + (msg.length/100)*30});
					  			}
						  	}
						  });
				   	}},
				   	{text:'Cancel', action: function() {
				   		ahtb.close();
				   	}}]});
	}

	this.tryUploadAgain = function()
	{
		dev.tryUpload();
	}

	this.tryUpload = function()
	{
		if (dev.gotMyTreat == false)
		{
			setTimeout(function() {
				dev.tryUploadAgain();
			}, 1000);
			return;
		}
		$.ajax({
	        url: ah_local.tp+'/_admin/ajax_developer.php',
	        type: 'POST',
	        data: dev.formData,
	        cache: false,
	        dataType: 'json',
	        processData: false, // Don't process the files
	        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
			error: function($xhr, $status, $error){
				if ( !$xhr.responseText &&
					 !$error)
					return; // ignore it

				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error', 
							height: 150, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},					
		  	success: function(data){
			  	if ( data == null || data.status == null) 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
			  	else if (data.status != 'OK')
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data==null? "No data available." : data.data)+'</p>' });
			  	else {
			  		console.log(data.data);
			  		// ahtb.alert(data.data, {width: 450, height: 150});
			  	}
			},
			xhr: function() {
	            var xhr = new window.XMLHttpRequest();
	            xhr.addEventListener("progress", function(evt) {
			            var lines = evt.currentTarget.response.split("\n");
			            var len = lines.length;
			            
			            var progress = '';
			            while (len > dev.linesReported) {
			            	var line = lines[dev.linesReported].replace(/([^"])*?/, "");
	            			if (line.length)
	            			{
	            				progress = line;
	            				$('#progress').text( progress );
			            		console.log( progress );
	            			}
	            			dev.linesReported++;
			            }

			            if (progress.match(/TERMINATE/) ||
			            	progress.match(/DONE/)) {
			            	xhr.abort();
			            	console.log("xhr terminated");
			            	$('#progress').text( progress );
			            	//dev.report = dev.implode('\n', lines);
			            	//window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
			            	//window.requestFileSystem(window.TEMPORARY, 1024*1024, dev.onInitFs, dev.errorHandler);
			            }

			            
		            }, false);
	           return xhr;
	        } 
    	});
	}

	this.retryAjax = function($x) {
		dev.tryAjax($x);
	}

	this.tryAjax = function($x) {
		if (dev.gotMyTreat == false)
		{
			setTimeout(function() {
				dev.retryAjax($x);
			}, 1000);
			return;
		}
		dev.checkActivePP();
		dev.ajax($x);
	}

	this.tryAgain = function($x)
	{
		dev.ajax($x);
	}

	// dev.sendTrickOrTreat(); should have been called on('click')
	this.ajax = function($x){
		if (dev.gotMyTreat == false ||
			dev.gotMyhaveActiveBE == false)
		{
			setTimeout(function() {
				dev.tryAgain($x);
			}, 1000);
			return;
		}

		if (typeof $x.query == 'undefined') console.error('query was undefined.'); 
		else if ($x.query.match(/getlisthubdata/) == null &&
				 $x.query.match(/fix-image-borders/) == null &&
				 $x.query.match(/fix-one-image-border/) == null &&
				 $x.query.match(/remove-listings/) == null &&
				 $x.query.match(/invitation-list/) == null &&
				 $x.query.match(/ping-aws/) == null &&
				 $x.query.match(/start-aws/) == null &&
				 $x.query.match(/delete-cookie/) == null &&
				 $x.query.match(/import-active-listing-images/) == null &&
				 $x.query.match(/create-images-from-imported/) == null &&
				 $x.query.match(/sim-fix-image-borders/) == null &&
				 $x.query.match(/post-to-progress/) == null &&
				 $x.query.match(/check-repair-listing-image-data/) == null &&
				 $x.query.match(/test-ipinfo/) == null && 
				 $x.query.match(/test-alert/) == null ) {  
			if ($x.query.match(/get-aws-data/) != null) {
	  			dev.gotAWSData = false;
	  			$('.spinner').fadeIn(250, function(){});
	  		}
	  		
	  		ahtb.alert('Running '+$x.query+', please stand back now.', {height: 150});
			$.ajax({
				url: ah_local.tp+'/_admin/ajax_developer.php',
				data: { query: $x.query,
						mountain: dev.myMountain,
						everest: dev.myEverest },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					ahtb.open({ title: 'You Have Encountered an Error', 
								height: 150, 
								html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
							});
					// if (!dev.canRemotePP)
					// 	dev.clearMyTreat();
				},					
			  	success: function(data){
				  	if (data == null || typeof data == 'undefined' || typeof data.status == 'undefined') 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else if (data.status != 'OK') {
				  		if ($x.query.match(/get-aws-data/) != null) {
				  			dev.gotAWSData = true;
				  			$('.spinner').fadeOut(250, function(){});
				  		}
				  		window.setTimeout( function() {
				  			ahtb.open({ title: 'You Have Encountered an Error', height: 150, width: 200, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
				  		}, 500 );
				  	}
				  	else if ($x.query.match(/get-aws-data/) != null) {
				  			dev.gotAWSData = true;
				  			$('.spinner').fadeOut(250, function(){});
				  			if (typeof data.data == 'object') {
						  		console.log("Got option: "+data.data.opt);
						  		dev.amazon = JSON.parse(data.data.value);
						  		var ec2Count = 0;
						  		var running = 0;
						  		var stopped = 0;
						  		for(var data in dev.amazon) {
						  			if (typeof dev.amazon[data] == 'object') {
						  				ec2Count++;
						  				for(var objData in dev.amazon[data]) {
						  					console.log("AWS:"+data+" - "+objData+":"+dev.amazon[data][objData]);
						  					if (objData == 'State')
						  						if (dev.amazon[data][objData] == 'running')
						  							running++;
						  						else
						  							stopped++;
						  				}
						  				if (ec2Count > dev.tallyLoadPing.length) {
						  					dev.tallyLoadPing[dev.tallyLoadPing.length] = 0;
				  							dev.tallyLoadPingCount[dev.tallyLoadPingCount.length] = 0;
						  				}
						  			}
						  			else
						  			 	console.log("AWS:"+data+" - "+dev.amazon[data]);
						  		}
						  		ahtb.alert("There are "+ec2Count+" EC2 instances, running:"+running+", stopped:"+stopped,
						  					{height: 150});
						  		dev.numSubDomains = ec2Count;
				  				$('ul#tables #subdomain option[value="'+dev.numSubDomains+'"]').prop('selected', true);
					  		}
					}
				  	else if ($x.query.match(/get-host-environment/) != null) {
				  		dev.host = data.data;
				  		dev.gotHostEnvironment = true;
				  		ahtb.close();
				  	}
				  	else if ($x.query.match(/get-seller-count/) != null) {
				  		dev.sellerCount = data.data;
				  		dev.emailDbPage = 0;
				  		dev.beginEmailDbUpdate();
				  	}
				  	else if ($x.query.match(/get-active-listings-count/) != null) {
				  		dev.activeListingCount = data.data;
				  		dev.activeListingPage = 0;
				  		dev.haltResetImageOrigin = false;
				  		ahtb.open({html: "<p>Are you sure?  This will remove all processed images.</p>",
				  				   width: 450,
				  				   height: 180,
				  				   buttons:[
				  				   {text:"OK", action:function() {
				  				   		dev.beginResetImageOrigin();
				  				   }},
				  				   {text: "Cancel", action:function() {
				  				   		ahtb.close();
				  				   }}]
				  				});
				  		
				  	}
				  	else if ($x.query.match(/get-active-cities-count/) != null) {
				  		console.log("city count:"+data.data);
				  		dev.activeCitiesCount = data.data;
				  		dev.activeCitiesPage = 0;
				  		dev.haltUpdateMedianPrices = false;
				  		dev.beginUpdateMedianPrices();
				  	}
				  	else if ($x.query.match(/get-active-zip-count/) != null) {
				  		console.log("zip count:"+data.data);
				  		dev.activeZipCount = data.data;
				  		dev.activeZipPage = 0;
				  		dev.haltUpdateMedianPrices = false;
				  		dev.maxActiveZipApiCalls = MAX_QUANDL_API_CALLS;
				  		dev.beginUpdateMedianPricesZip();
				  	}
				  	else if ($x.query.match(/start-set-first-image/) != null) {
				  		console.log("active listings count:"+data.data);
				  		dev.activeListingCount = data.data;
						dev.activeListingPage = 0;
						dev.activeListingPerPage = 100;
						dev.haltSetFirstImage = false;
						dev.beginSettingFirstImage();
				  	}
				  	else if ($x.query.match(/clean-listings-img-folder/) != null) {
				  		var msg = typeof data.data == 'string' ? data.data : JSON.stringify(data.data);
				  		var lineBreaks = (msg.match(/<br/g) || []).length;
				  		console.log($x.query+" - lineBreaks:"+lineBreaks+", returned: "+msg);
				  		var h = '<div id="ajax-msg"><span>'+msg+'<br/></span><span id="extra"></span></div>';
				  		ahtb.alert(h, {height: 180 + (lineBreaks > 3 ? (lineBreaks-3) * 20 : 0)});
				  	}
				  	else if ($x.query.match(/check-image-tracker/) != null) {
				  		var count = parseInt(data.data);
				  		dev.totalImagesInFolder = dev.totalImagesLeftInFolder = count;
				  		console.log('check-image-tracker got back '+count+" rows");
				  		if (count) {
				  			ahtb.alert("Have "+count+" rows we can clean.");
				  			$("button#begin-image-removing").show();
				  		}
				  		else
				  			ahtb.alert("No rows left to process in ImageTracker");
				  	}
				  	else if ($x.query.match(/write-site-maps/) != null) {
				  		var msg = 'Wrote out '+data.data.count+" sitemaps.";
				  		console.log(msg);
				  		ahtb.alert(msg);
				  	}
				  	else if ($x.query.match(/read-temp-data/) != null) {
				  		var msg = 'Temp data: '+data.data;
				  		console.log(msg);
				  		ahtb.alert(msg, {width:450, height:200});
				  	}
				  	else if ($x.query.match(/prep-reload/) != null) {
				  		if (parseInt(data.data) == 1)
				  			location.reload();
				  		else
				  			ahtb.alert("Prep reload failed");
				  	}
				  	else {
				  		var msg = typeof data.data != 'undefined' ? (typeof data.data == 'string' ? data.data : JSON.stringify(data.data)) : "No message";
				  		var lineBreaks = (msg.match(/<br/g) || []).length;
				  		console.log($x.query+" - lineBreaks:"+lineBreaks+", returned: "+msg);
				  		ahtb.alert(msg, {height: 180 + (lineBreaks > 3 ? (lineBreaks-3) * 20 : 0)});
				  	}
			  	}
			});
		}
		else if ($x.query.match(/start-aws/) != null) {
			dev.startingEc2 = false;
			$('.spinner').fadeIn(250, function(){});
			$.ajax({
				url: ah_local.tp+'/_admin/ajax_developer.php',
				data: { query: $x.query,
						count: dev.startEc2Count,
						mountain: dev.myMountain,
						everest: dev.myEverest },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					ahtb.open({ title: 'You Have Encountered an Error', 
								height: 150, 
								html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
							});
					dev.startingEc2 = true;
					$('.spinner').fadeOut(250, function(){});
					// if (!dev.canRemotePP)
					// 	dev.clearMyTreat();
				},					
			  	success: function(data){
			  		dev.startingEc2 = true;
				  	if (data == null || typeof data == 'undefined' || typeof data.status == 'undefined') 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else if (data.status != 'OK')
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, width: 200, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
				  	else if (typeof data.data == 'object') {
				  		$('.spinner').fadeOut(250, function(){});
				  		console.log("Got option: "+data.data.opt);
				  		dev.amazon = JSON.parse(data.data.value);
				  		var ec2Count = 0;
				  		var running = 0;
				  		var stopped = 0;
				  		for(var data in dev.amazon) {
				  			if (typeof dev.amazon[data] == 'object') {
				  				ec2Count++;
				  				for(var objData in dev.amazon[data]) {
				  					console.log("AWS:"+data+" - "+objData+":"+dev.amazon[data][objData]);
				  					if (objData == 'State')
				  						if (dev.amazon[data][objData] == 'running')
				  							running++;
				  						else
				  							stopped++;
				  				}
				  				if (ec2Count > dev.tallyLoadPing.length) {
				  					dev.tallyLoadPing[dev.tallyLoadPing.length] = 0;
		  							dev.tallyLoadPingCount[dev.tallyLoadPingCount.length] = 0;
				  				}
				  			}
				  			else
				  			 	console.log("AWS:"+data+" - "+dev.amazon[data]);
				  		}
				  		console.log("There are "+ec2Count+" EC2 instances, running:"+running+", stopped:"+stopped);
				  		dev.numSubDomains = running;
				  		$('ul#tables .AWS').show();
		  				$('ul#tables #subdomain option[value="'+dev.numSubDomains+'"]').prop('selected', true);
				  	}
				  	else
				  		console.log(data.data);
				  		// if (!dev.canRemotePP)
				  		// 	dev.clearMyTreat();
			  	}
			});
		}
		else if ($x.query.match(/test-alert/) != null) {
			dev.testOverloadedCpu(99, true);
		}
		else if ($x.query.match(/ping-aws/) != null) {
			if (dev.amazon == null) {
				ahtb.alert("Please either start AWS or get AWS data first.",
							{height: 150 });
				return;
			}
			dev.pingAmazon(dev.pingEc2Target);
		}
		else if ($x.query.match(/test-ipinfo/) != null) {
			var h = '<div>'+
						'<input type="text" id="ip" placeholder="Enter IP to test"></input>'+
						'<span id="warning" style="color: red; display: none;">Enter an IP</span>'+
					'</div>';
			ahtb.open({	html:h,
						width: 450,
						height: 150,
						buttons: [{text:'Go', action:function() {
							var val = $('input#ip').val();
							if (val.length == 0) {
								$('span#warning').show();
								return;
							}
							$.ajax({
								url: ah_local.tp+'/_admin/ajax_developer.php',
								data: { query: $x.query,
										ip: val,
										mountain: dev.myMountain,
										everest: dev.myEverest },
								dataType: 'JSON',
								type: 'POST',
								error: function($xhr, $status, $error){
									var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
									ahtb.open({ title: 'You Have Encountered an Error', 
												height: 150, 
												html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
											});
									// if (!dev.canRemotePP)
									// 	dev.clearMyTreat();
								},					
							  	success: function(data){
								  	if (data == null || typeof data == 'undefined' || typeof data.status == 'undefined') 
								  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
								  	else if (data.status != 'OK') {
								  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, width: 200, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
								  	}
								  	else {
								  		console.log(val+" is "+JSON.stringify(data.data));
								  		ahtb.alert(val+" is "+JSON.stringify(data.data));
								  	}
								}
							})
						}},
						{text:'Cancel', action:function() {
							ahtb.close();
						}}]});
		}
		else if ($x.query.match(/delete-cookie/) != null) {
			ahtb.open({
	            title: 'Cookie Management',
	            height: 180,
	            width: 550,
	            html: '<p>Enter the name of cookie to delete (case sensistive)</p>'+
	            	  '<div style="display: inline; padding-bottom: 10px; margin-bottom: 10px;"><input type="text" id="cookie" value="CassiopeiaDirective"/></div><br/><br/>',
		        	buttons:[
	        			{text:"Delete", action:function(){ 
	        				ahtb.close(); 
	        				var val = $('#cookie').val();
	        				if (val.length) {
	        					var name = val + "=";
								document.cookie = name+"; expires=Thu, 01 Jan 1970 00:00:01 GMT"+ "; path=/";
	        					console.log("Delete cookie:"+val);
	        				}
	        			}},
            			{text:"Cancel", action:function(){ 
            				ahtb.close(); 
            			}}
        			],
        		opened: function() {
             	},
	            closed: function(){
	              ahtb.showClose(250);
	              ahtb.closeOnClickBG(1);
	            }	          
	        });
		}
		else if ($x.query.match(/invitation-list/) != null) {
			ahtb.open({
	            title: 'Invitation List Management',
	            height: 180,
	            width: 550,
	            html: '<p>What would you like to do?  Create new invitation codes or view existing ones?</p>'+
	            	  '<div style="display: inline; padding-bottom: 10px; margin-bottom: 10px;"><span>Number of invitation codes to create:</span><span> </span><input type="text" id="inviteCount" /></div><br/><br/>',
		        	buttons:[
	        			{text:"Create", action:function(){ ahtb.close(); dev.inviteCodes($('#inviteCount').val() == '' ? "12" : $('#inviteCount').val()); }},
            			{text:"View", action:function(){ 
            				ahtb.close(); 
            				dev.inviteCodes("0"); }}
        			],
        		opened: function() {
        			$('#inviteCount').keydown(function (e) {
				        // Allow: backspace, delete, tab, escape, and enter
				        if ($.inArray(e.keyCode, [8, 9, 27, 13, 110, 190]) !== -1 ||
				             // Allow: Ctrl+A
				            (e.keyCode == 65 && e.ctrlKey === true) || 
				             // Allow: home, end, left, right
				            (e.keyCode >= 35 && e.keyCode <= 39)) {
				                 // let it happen, don't do anything
				                 return;
				        }
				        // Ensure that it is a number and stop the keypress
				        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				            e.preventDefault();
				        }
		    		});
        			
        		},
	            closed: function(){
	              ahtb.showClose(250);
	              ahtb.closeOnClickBG(1);
	            }	          
	        });
		}
		else if ($x.query.match(/remove-listings/) != null) {
			ahtb.open({
	            title: 'Remove Listings (CAREFUL!!)',
	            height: 220,
	            width: 630,
	            html: '<p>Enter a range of listings id to delete permanently.<br/>It will take a while, the process will be running in the background.</p>'+
	            	  '<div style="display: inline; padding-bottom: 10px; margin-bottom: 10px;">'+
	            	  '<span>Start</span>&nbsp;<input type="text" id="start" />'+
	            	  '<span>End</span>&nbsp;<input type="text" id="end" />'+
	            	  '<span>&nbsp;&nbsp;Preserve active listings </span><span> </span><input type="checkbox" id=preserveActiveListings />'+
	            	  '</div><br/><br/>',
		        	buttons:[
	        			{text:"Remove", action:function(){ 
	        				var start = $('input#start').val();
	        				var end = $('input#end').val();
	        				if (start.length == 0) {
	        					ahtb.push();
	        					ahtb.open({ html: "<p>Enter a starting listing ID.</p>",
	        								width: 450,
	        								height: 150,
	        								buttons: [{text:"Understood", action:function() {
	        									console.log("Understood clicked.");
	        									ahtb.pop();
	        								}}]
	        					});
	        					return;
	        				}
	        				ahtb.close(); 
	        				dev.removeListings(start, end); 
	        			}},
            			{text:"Cancel", action:function(){ 
            				ahtb.close(); 
            			}}
        			],
        		opened: function() {     
        		  	$('#preserveActiveListings').prop('checked', dev.preserveActiveListings);
        			$('#preserveActiveListings').change(function(){
						dev.preserveActiveListings = $(this).prop("checked");
					});	
        		},
	            closed: function(){
	              ahtb.showClose(250);
	              ahtb.closeOnClickBG(1);
	            }	          
	        });
		}
		// else if ($x.query.match(/sim-fix-image-borders/) != null) {
		// 	dev.startRemoteImageProcessing();
		// }
		else if ($x.query.match(/fix-one-image-border/) != null ) {
			ahtb.open({
	            title: 'Choose Listing Group',
	            height: 180,
	            width: 550,
	            html: 	'<div>' +
	            			'<span>Enter the listing id:</span>&nbsp;<input id="id" type="text" placeholder="enter a listing id" />' +
	            	  	'</div>',
		        buttons:[
	        			{text:"OK", action:function(){ 
	        				var id = $('#id').val();
	        				if (id.length == 0) 
	        					return;

	        				data = {  query: 'fixoneimageborder',
		        					  id: id,
		        					  mountain: dev.myMountain,
									  everest: dev.myEverest };
							ahtb.alert("Running "+data.query+", please stand back now.", {height: 150});
							$.ajax({
								url: ah_local.tp+'/_admin/ajax_developer.php',
								data: data,
								dataType: 'JSON',
								type: 'POST',
								error: function($xhr, $status, $error){
									var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
									ahtb.open({ title: 'You Have Encountered an Error', 
												height: 150, 
												html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
											});
									// if (!dev.canRemotePP)
									// 	dev.clearMyTreat();
								},					
							  	success: function(data){
								  	if (data == null || typeof data == 'undefined' || typeof data.status == 'undefined') 
								  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
								  	else if (data.status != 'OK') {
								  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, width: 200, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
								  	}
								  	else {
								  		ahtb.close();
								  	}
								}
							})
	        			}},
            			{text:"Cancel", action:function(){ 
            				ahtb.close(); 
            			}}
        			],
        		opened: function() {
        			
        		},
	            closed: function(){
	              ahtb.showClose(250);
	              ahtb.closeOnClickBG(1);
	            }	          
	        });
		}
		else if ($x.query.match(/fix-image-borders/) != null ||
				 $x.query.match(/sim-fix-image-borders/) != null) {
			ahtb.open({
	            title: 'Choose Listing Group',
	            height: 210,
	            width: 550,
	            html: '<p>Do you want to clean the image border in which group?<br/>It will take a while, the process will be running in the background.</p>'+
	            	  '<div style="display: inline; padding-bottom: 10px; margin-bottom: 10px;">'+
	            	  '<span>Process only new images</span><span> </span><input type="checkbox" id="processOnlyNewImages" '+(dev.processOnlyNewImages ? 'checked' : '')+'/>'+
	            	  '<span>Start from beginning</span><span> </span><input type="checkbox" id="processAllImages" />'+
	            	  '<span>Optimize table afterwards</span><span> </span><input type="checkbox" id="optimizeTable" checked/>'+
	            	  '<span>Custom task</span><span> </span><input type="checkbox" id="customTask" />'+
	            	  '</div><br/><br/>',
		        	buttons:[
	        			{text:"Live", action:function(){ 
	        				// ahtb.close(); 
	        				dev.imageBorderFromLast = $('input#processAllImages').prop('checked') ? 0 : 1;
	        				dev.imageBorderOptimizeTableAfterwards = $('input#optimizeTable').prop('checked') ? 1 : 0;
	        				dev.imageBorderMode = ActiveState.ACTIVE;
	        				var remoteMode = $x.query.match(/sim-fix-image-borders/) != null ? LogState.REMOTE_IMAGE_PROCESSING : 
	        								 ($('input#customTask').prop('checked') ? RemoteMode.IMAGE_PROCESSING_CUSTOM_TASK : RemoteMode.NONE);
	        				dev.getFixImageListingCount(remoteMode);
	        				// dev.fixImageBorders(ActiveState.ACTIVE, val); 
	        			}},
            			{text:"Waiting", action:function(){ 
            				// ahtb.close(); 
            				dev.imageBorderFromLast = $('input#processAllImages').prop('checked') ? 0 : 1;
 	        				dev.imageBorderOptimizeTableAfterwards = $('input#optimizeTable').prop('checked') ? 1 : 0;
           					dev.imageBorderMode = ActiveState.WAITING;
           					var remoteMode = $x.query.match(/sim-fix-image-borders/) != null ? LogState.REMOTE_IMAGE_PROCESSING : 
	        								 ($('input#customTask').prop('checked') ? RemoteMode.IMAGE_PROCESSING_CUSTOM_TASK : RemoteMode.NONE);
            				dev.getFixImageListingCount(remoteMode);
            				// dev.fixImageBorders(ActiveState.WAITING, val); 
            			}}
        			],
        		opened: function() {
        			$('#processOnlyNewImages').prop('checked', dev.processOnlyNewImages);
        			$('#processOnlyNewImages').change(function(){
						dev.processOnlyNewImages = $(this).prop("checked");
					});
					$('#customTask').change(function() {
						var checked = $(this).prop("checked");
						// if (checked) {
						// 	dev.processOnlyNewImages = 1;
						// 	$('#processOnlyNewImages').prop('checked', true);
						// }

					})
        		},
	            closed: function(){
	              ahtb.showClose(250);
	              ahtb.closeOnClickBG(1);
	            }	          
	        });
		}
		else if ($x.query.match(/post-to-progress/) != null) {
			$.ajax({
				url: ah_local.tp+'/_admin/ajax_developer.php',
				data: { query: $x.query,
						msg: $x.msg,
						mountain: dev.myMountain,
						everest: dev.myEverest },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					ahtb.open({ title: 'You Have Encountered an Error', 
								height: 150, 
								html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
							});
					// if (!dev.canRemotePP)
					// 	dev.clearMyTreat();
				},					
			  	success: function(data){
				  	if (data == null || typeof data == 'undefined' || typeof data.status == 'undefined') 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else if (data.status != 'OK') {
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, width: 200, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
				  	}
				  	else {
				  		console.log("Sent: "+$x.query+" "+$x.msg);
				  	}
				}
			})
		}
		else if ($x.query.match(/import-active-listing-images/) != null || // must be getlisthubdata, deprecated, shouldn't be called.
				$x.query.match(/create-images-from-imported/) != null )
		{
			ahtb.alert("Getting count of listings to "+(($x.query.match(/import-active-listing-images/) != null) ? 'import' : 'process') +" images for.", {height: 150});
			$.ajax({
				url: ah_local.tp+'/_admin/ajax_developer.php',
				data: { query: $x.query,
						useImpressions: dev.useImpressions,
						mountain: dev.myMountain,
						everest: dev.myEverest },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					ahtb.open({ title: 'You Have Encountered an Error', 
								height: 150, 
								html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
							});
					// if (!dev.canRemotePP)
					// 	dev.clearMyTreat();
				},					
			  	success: function(data){
				  	if (data == null || typeof data == 'undefined' || typeof data.status == 'undefined') 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else if (data.status != 'OK') {
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, width: 200, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
				  	}
				  	else {
				  		ahtb.alert("Got "+data.data+" number of listings to "+(($x.query.match(/import-active-listing-images/) != null) ? 'import' : 'process') +" images for.", {height: 150});
				  		dev.totalBlocks = parseInt(data.data);
				  		dev.startSpecialParallelOp(($x.query.match(/import-active-listing-images/) != null) ? SpecialOp.IMPORT_ACTIVE_LISTINGS_IMAGE : SpecialOp.CREATE_IMAGES_FROM_IMPORTED);			  	
				  	}
				}
			})
		}
		else if ($x.query.match(/check-repair-listing-image-data/) != null) {
	  		ahtb.open({html: "<p>Are you sure?  This will check for missing processed images and reset image file to point to the URL again.</p>",
	  				   width: 450,
	  				   height: 180,
	  				   buttons:[
	  				   {text:"OK", action:function() {
	  				   		dev.sendTrickOrTreat();
	  				   		dev.tryAjax({query: 'repair-listing-image-data',
	  				   					mountain: dev.myMountain,
										everest: dev.myEverest});
	  				   }},
	  				   {text: "Cancel", action:function() {
	  				   		ahtb.close();
	  				   }}]
	  				});
	  	}
		else if ($x.query.match(/Parallel/) == null) // must be getlisthubdata, deprecated, shouldn't be called.
		{
			dev.linesReported = 0;
			dev.doingSpecialParallelOperation = 0;

			var msg = "about to call "+$x.query+" at "+getTime();
			dev.log(msg);
			dev.setExtraInfo('<p>'+msg+'</p>');

		    //report = '';
			$('#progress').text( "Starting" );
			$.ajax({
				url: ah_local.tp+'/_admin/ajax_developer.php',
				data: { query: $x.query,
						mountain: dev.myMountain,
						everest: dev.myEverest },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					ahtb.open({ title: 'You Have Encountered an Error', 
								height: 150, 
								html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
							});
				},					
			  	success: function(data){
				  	if ( data == null || data.status == null) 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else if (data.status != 'OK')
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data==null? "No data available." : data.data)+'</p>' });
				  	else {
				  		console.log(data.data);
				  	}},
				xhr: function() {
		            var xhr = new window.XMLHttpRequest();
		            xhr.addEventListener("progress", function(evt) {
				            var lines = evt.currentTarget.response.split("\n");
			           		var len = lines.length;
			           		var progress = '';
				            while (len > dev.linesReported) {
				            	var line = lines[dev.linesReported].replace(/([^"])*?/, "");
		            			if (line.length)
		            			{
		            				progress = line;
				            		console.log( progress );
		            				$('#progress').text( line );
		            			}
		            			dev.linesReported++;
				            }

				            if (progress.match(/DONE/)) {
				            	xhr.abort();
				            	console.log("xhr terminated");
		            			$('#progress').text( progress );
				            }
			            }, false);
		           return xhr;
		        }
			});
		}
		else { // doing getlisthubdataParallel, get listhub data and parse it to db
			setBEStates();
			if (dev.haveActiveBE != 'false')
			{
				if ( haveMyOwn &&
					 (myAction.indexOf('PP') != -1 ||
					  !dev.onlyParseFeed) ) {
					ahtb.alert("This browser is already doing "+(myAction.indexOf('IP') != -1 ? 'image' : 'listhub')+" parallel processing");
					return;
				}

				if (haveP2Db) {
					var msg = "Active parsing to DB right now.  Cannot start parallel processing.";
					console.log(msg);
					ahtb.alert("Someone with owner id:"+otherP2DbOwner+", is parsing the listhub data somewhere,<br/>you gotta wait now...",
			  					{height: 150});
					return;
				}
				else if (havePP) {
					console.log("Active PP session is being run by someone right now, you gotta wait...");
					ahtb.alert("Someone with owner id:"+otherPPOwner+",is doing an active PP session somewhere, you gotta wait now...",
			  						{height: 150});
	  				return;
	  			}
			}

			var msg = "about to call "+$x.query+" at "+getTime();
			dev.log(msg);
			dev.setExtraInfo('<p>'+msg+'</p>');


			$('#progress').text( "Starting" );
			dev.linesReported = 0;
			dev.totalBlocks = 0;
			dev.runPPAfterResult = !dev.onlyParseFeed;
			$.ajax({
				url: ah_local.tp+'/_admin/ajax_developer.php',
				data: { query: $x.query,
						parseOnly: dev.onlyParseFeed ? 1 : 0,
						retryCount: dev.retryParseCount,
						mountain: dev.myMountain,
						everest: dev.myEverest },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					ahtb.open({ title: 'You Have Encountered an Error while parsing Listhub data into the db.', 
								height: 150, 
								html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
							});
					dev.removeActiveP2Db();
					dev.retryParse();
					// if (dev.myProgressTimer)
					// 	window.clearInterval(dev.myProgressTimer);
					// if (myProgressTimer)
					// 	window.clearInterval(myProgressTimer);
				},					
			  	success: function(data){
			  	if ( data == null || data.status == null) {
			  		ahtb.open({ title: 'You Have Encountered an Error while parsing Listhub data into the db', height: 150, html: '<p>Got a return of NULL</p>' });
			  		dev.removeActiveP2Db();
			  		dev.retryParse();
			  	}
			  	else if (data.status != 'OK') {
			  		ahtb.open({ title: 'You Have Encountered an Error while parsing Listhub data into the db', height: 150, html: '<p>'+(typeof data.data==null? "No data available." : data.data)+'</p>' });
			  		dev.removeActiveP2Db();
			  		dev.retryParse();
			  	}
			  	else if (data.data != null && data.data != 'undefined') {
			  		console.log(data.data);
			  		$('#progress').text( data.data );
			  		if (!dev.doingRemote) {
				  		dev.synchroDeltaTime = 10000;
						dev.doingSpecialParallelOperation = 0;
						dev.restartSQL = false;
						dev.countCitiesAfterParse = true;
						dev.imageBorderOptimizeTableAfterwards = false;
						dev.imageProcessAfterParse = true;
						dev.resetMedianAndTiers = true;
						dev.listhubParseOptimizeTableAfterwards = true;
					}
			  	}
			  }
			});
			
			// if (resultsTimer) {
			// 	window.clearTimeout(resultsTimer);
			// 	resultsTimer = null;
			// }
			dev.registerActiveP2Db();
			//dev.waitForEndOfParsingToDb();
		} // end else
	}

	this.retryParse = function() {
		if (!dev.removedActiveP2Db) {
			window.setTimeout(function() {
				dev.retryParse();
			}, 500);
			return;
		}
		if (dev.retryParseCount++ < MAX_PARSE_RETRIES) {
			dev.doingRemote = 0;
			dev.startCronParallel();
		}
		else {
			var msg = "We have retried reading parsing data for "+dev.retryParseCount+" times.  Giving up";
			dev.log(msg);
			dev.setExtraInfo('<p>'+msg+'</p>');
		}
	}

	this.getFixImageListingCount = function(doingRemote, fromCron) {
		ahtb.alert("Getting count of listings to image process for remoteMode:"+doingRemote, {height: 150});
		// limit to 15 units per chunk.  Otherwise, thread may be tagged as bad if it takes over 50 min to finish
		if (dev.unitsPerChunk > MAX_IMAGE_PROCESSING_CHUNK_SIZE) {
			dev.unitsPerChunk = MAX_IMAGE_PROCESSING_CHUNK_SIZE;
			$('ul#tables #chunks option[value="'+dev.unitsPerChunk+'"]').prop('selected', true);
		}
		if ( ah_local.tp.indexOf('localhost') == -1)
			dev.loadLimit = parseInt(dev.loadLimit) < dev.loadLimitImageProcessing? dev.loadLimitImageProcessing.toString() : dev.loadLimit;
		else
			dev.loadLimit = parseInt(dev.loadLimit) > 5 ? "5" : dev.loadLimit;
		$('ul#tables #loadlimit').val(dev.loadLimit);

		fromCron = typeof fromCron == 'undefined' ? 0 : fromCron;
		var specialOp = SpecialOp.IMAGE_BORDER_PARALLEL;
		if (typeof doingRemote != 'undefined') {
			switch(doingRemote) {
				case RemoteMode.FULL_PARSE_CRON: 
					dev.imageBorderFromLast = 1;
					dev.imageBorderMode = ActiveState.ACTIVE;
					// dev.unitsPerChunk = 50; 
					fromCron = 1;
					specialOp = SpecialOp.IMAGE_BORDER_PARALLEL_AFTER_PARSE;
					break;
				case RemoteMode.NONE: 
					doingRemote = false;
					specialOp = SpecialOp.IMAGE_BORDER_PARALLEL;
					dev.unitsPerChunk = parseInt($("ul#tables #chunks option:selected").val());
					break;
				case RemoteMode.IMAGE_BORDER: // all specs should have been set
					specialOp = SpecialOp.IMAGE_BORDER_PARALLEL_REMOTELY;
					break;
				case RemoteMode.IMAGE_BORDER_CRON_ALL: 
				case RemoteMode.IMAGE_BORDER_CRON_FROM_LAST:
					dev.imageBorderFromLast = doingRemote == RemoteMode.IMAGE_BORDER_CRON_ALL ? 0 : 1; // from beginning
					dev.imageBorderMode = ActiveState.ACTIVE;
					// if ( ah_local.tp.indexOf('localhost') == -1) {
						// dev.unitsPerChunk = 15;
						dev.numSubDomains = 12;
						dev.numChunkPerSubDomain = 6;
						dev.loadLimit = "20";
					// } else {
					// 	dev.unitsPerChunk = 4;
					// 	dev.numSubDomains = 3;
					// 	dev.numChunkPerSubDomain = 2;
					// 	dev.loadLimit = "5";
					// }
					fromCron = 1;
					specialOp = doingRemote == RemoteMode.IMAGE_BORDER_CRON_ALL ? SpecialOp.IMAGE_BORDER_PARALLEL_CRON_ALL : SpecialOp.IMAGE_BORDER_PARALLEL_CRON_FROM_LAST;
					break;
				case RemoteMode.IMAGE_PROCESSING_CUSTOM_TASK:
				case RemoteMode.IMAGE_PROCESSING_CUSTOM_TASK_FROM_LAST:
					dev.imageBorderFromLast = doingRemote == RemoteMode.IMAGE_PROCESSING_CUSTOM_TASK_FROM_LAST ? 1 : dev.imageBorderFromLast;
					specialOp = SpecialOp.IMAGE_BORDER_PARALLEL_CUSTOM_TASK;
					dev.imageBorderMode = ActiveState.ACTIVE;
					dev.imageBorderOp = ImageOpMode.CUSTOM_IMAGE_TASK;
					dev.processOnlyNewImages = 1; // forced to true always for custom task, at least this one, redo all images to 80%, set first_image and list_image for all listings
					break;

				case LogState.REMOTE_IMAGE_PROCESSING:
					$.ajax({
						url: ah_local.tp+'/_admin/ajax_developer.php',
						data: { query: 'startremoteimageprocessing',
								remoteppable: dev.myEverest, // caller of remote PP
								mode: dev.imageBorderMode,
								op: dev.imageBorderOp,
								onlynew: dev.processOnlyNewImages,
								fromLast: dev.imageBorderFromLast,
								numSubDomains: dev.numSubDomains,
								unitsPerChunk: dev.unitsPerChunk,
								numChunkPerSubDomain: dev.numChunkPerSubDomain,
								cpuLimit: $('ul#tables #loadlimit').val(),
								mountain: dev.myMountain,
								everest: dev.myEverest },
						dataType: 'JSON',
						type: 'POST',
						error: function($xhr, $status, $error){
							var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
							console.log("Error during startremoteimageprocessing: "+msg);
						},					
					  	success: function(data){
						  	if (data == null || data.status == null ||
						  		typeof data == 'undefined' || typeof data.status == 'undefined') 
						  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
						  	else if (data.status != 'OK') {
						  		msg =  data.data != null ? data.data : '<p>Failed to get any P2Db starting point data</p>';
						  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: msg });
						  		$('ul#tables #startRemoteParallel').show();
						  		$('ul#tables #startListHubDataProcessing').show();
						  		$('ul#tables #startRemoteImageProcessingProcessing').show();
						  	}
						  	else {
						  		if (data != null && data.status != null && data.data != null)
						  			console.log(data.data);
						  		else {
						  			console.log('Failed to start remote PP');
						  			$('ul#tables #startRemoteParallel').show();
						  			$('ul#tables #startListHubDataProcessing').show();
						  			$('ul#tables #startRemoteImageProcessingProcessing').show();
						  		}
				  			}
				  		}
			  		});
					return;
			}
		}
		else
			doingRemote = false;
		
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'active-image-count',
					mode: dev.imageBorderMode, // live or waiting
					op: specialOp,
					onlynew: dev.processOnlyNewImages,
					fromLast: dev.imageBorderFromLast,
					fromCron: fromCron,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error', 
							height: 150, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
				// if (!dev.canRemotePP)
				// 	dev.clearMyTreat();
			},					
		  	success: function(data){
			  	if (data == null || typeof data == 'undefined' || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
			  	else if (data.status != 'OK') {
			  		var msg = "Error during getting image count:"+(typeof data.data=='undefined'? "No data available." : data.data);
			  		dev.setExtraInfo('<p>'+msg+', mode:'+dev.imageBorderMode+', op:'+dev.imageBorderOp+', onlynew:'+dev.processOnlyNewImages+', fromLast:'+dev.imageBorderFromLast+'</p>');
			  		dev.log(msg);
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, width: 200, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
			  	}
			  	else {
			  		ahtb.alert("Got "+data.data+" number of listings to fix image borders for.", {height: 150});
			  		dev.totalBlocks = parseInt(data.data);
			  		if ( ah_local.tp.indexOf('localhost') == -1)
						dev.unitsPerChunk = MAX_IMAGE_PROCESSING_CHUNK_SIZE;
					else
						dev.unitsPerChunk = 4;
					
			  		if ( Math.floor(dev.totalBlocks / dev.numSubDomains) > 0)
			  			dev.unitsPerChunk = (dev.totalBlocks / dev.numSubDomains) < dev.unitsPerChunk ? Math.floor(dev.totalBlocks / dev.numSubDomains) : dev.unitsPerChunk;
			  		else
			  			dev.numSubDomains = 1;
			  		var msg = "getFixImageListingCount - total:"+dev.totalBlocks+", specialOp:"+specialOp;
			  		dev.setExtraInfo('<p>'+msg+', mode:'+dev.imageBorderMode+', op:'+dev.imageBorderOp+', onlynew:'+dev.processOnlyNewImages+', fromLast:'+dev.imageBorderFromLast+'</p>');
			  		dev.log(msg);
			  		if (dev.totalBlocks)
			  			dev.startSpecialParallelOp(specialOp);		
			  		else {
			  			dev.setExtraInfo('<p>No image blocks to process at this time</p>');
			  			dev.ipFromEndParallel = false;
			  			dev.countIPRunCount = 0;
			  			dev.doingRemote = RemoteMode.NONE;
			  		}	  	
			  	}
			}
		})
	}

	// this.waitForEndOfParsingToDb = function() {
	// 	//dev.myProgressTimer = window.setInterval(function(){
	// 	myProgressTimer = window.setInterval(function(){
	// 		if (myProgressTimer == null)
	// 		{
	// 			console.log("*********!!!!!  Timer for myProgressTimer is getting called after clearInterval()!!!");
	// 			return;
	// 		}
	// 		$.ajax({
	// 			url: ah_local.tp+'/_admin/ajax_developer.php',
	// 			data: { query: 'listhubcurrentprogress',
	// 					id: (dev.linesReported+1),
	// 					mountain: dev.myMountain,
	// 					everest: dev.myEverest },
	// 			dataType: 'JSON',
	// 			type: 'POST',
	// 			error: function($xhr, $status, $error){
	// 				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
	// 				ahtb.open({ title: 'You Have Encountered an Error while retrieving parsing to db results', 
	// 							height: 150, 
	// 							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
	// 						});
	// 			},					
	// 		  	success: function(data){
	// 			  	if (data == null || data.status == null) 
	// 			  		ahtb.open({ title: 'You Have Encountered an Error while retrieving parsing to db results', height: 150, html: '<p>Got a return of NULL</p>' });
	// 			  	else if (data.status != 'OK')
	// 			  		ahtb.open({ title: 'You Have Encountered an Error while retrieving parsing to db results', height: 150, html: '<p>'+(typeof data.data==null? "No data available." : data.data)+'</p>' });
	// 			  	else {
	// 			  		if (data.data == null || data.data == 'undefined')
	//   						return;

	//   					if (dev.linesReported == data.data['id'])
	// 			  			return;

	// 			  		if ((dev.linesReported+1) != data.data['id'])
	// 			  		{
	// 			  			console.log("!!!!!!! Log retrieval does not have the expect line #, expected:"+(dev.linesReported+1)+", but got instead:"+data.data['id']);
	// 			  			return;
	// 			  		}
	// 			  		var type = data.data['type'];
	// 			  		var msg = data.data['data'];
	// 			  		var showIt = true;
	// 			  		var pos;
	// 			  		dev.linesReported = data.data['id'];
	// 			  		if ( type == LogState.UPDATE ||
	// 						 type == LogState.ERROR ||
	// 						 type == LogState.FATAL)
	// 						showIt = true;

	// 					if (type == LogState.UPDATE &&
	// 						(pos = msg.indexOf("load:")) != -1)
	// 					{
	// 						var load = parseFloat(msg.substring(pos+5));
	// 						dev.tallyLoadCount++;
	// 						dev.tallyLoad += typeof load == 'float' ? load : parseFloat(load);
	// 						$('#load').val((dev.tallyLoad/(dev.tallyLoadCount ? dev.tallyLoadCount : 1)).toFixed(2));
	// 						$('#currentLoad').html((dev.tallyLoad/(dev.tallyLoadCount ? dev.tallyLoadCount : 1)).toFixed(2));
	// 					}
				  		
	// 					if (type == LogState.RESULT ||
	// 						type == LogState.FATAL)
	// 					{
	// 			  			window.clearInterval(myProgressTimer);
	// 			  			if (resultsTimer == null) // must be null, but just checking..
	// 							dev.startResultsTimer(); // and for results

	// 			  			if (type == LogState.RESULT)
	// 			  			{
	// 			  				dev.totalBlocks = parseInt(data.data['data']);
	// 			  				console.log("Parsing finished with "+dev.totalBlocks+" blocks.");
	// 			  				if (dev.totalBlocks)
	// 			  				{
	// 			  					clearInterval(myProgressTimer);
	// 			  					myProgressTimer = null;
	// 			  					if (!dev.doingParallel)
	// 			  					{
	// 			  						console.log("Going to clear out activeP2Db flag");
	// 			  						dev.removeActiveP2Db();
	// 			  						dev.startParallel();
	// 			  					}
	// 			  					else
	// 			  						console.log("Hey, doingParallel flag is true!!");
	// 			  					//dev.doingParallel = true;
	// 			  				}
	// 				  			else {
	// 				  				dev.removeActiveP2Db();
	// 				  				showIt = true;
	// 				  				ahtb.alert("Failed to parse the listhub file, has 0 blocks.",
	// 				  					       {height: 150})
	// 				  			}
	// 				  		}
	// 				  		else
	// 				  			ahtb.open({ title: 'You Have Encountered a Fatal Error', 
	// 										height: 150, 
	// 										html: '<p>Parallel processing not started. Detail: '+msg+'</p>' 
	// 							});
	// 			  		}
	// 			  		if (showIt)
	// 			  		{
	// 				  		console.log('Type: '+type+', line;'+dev.linesReported+', Data: '+msg);
	// 				  		$('#progress').text( msg );
	// 			  		}	
	// 			  	}
 //    		 	}
 //    		 });
	// 	},
	// 	2000);
	// }

	this.startSpecialParallelOp = function(mode) {
		dev.doingSpecialParallelOperation = mode;
		dev.readyStartParsing = false;
		dev.synchroDeltaTime = 1500;
		dev.startParallel();
	}

	this.restartParallel = function() {
		$('#progress').text( "Restarting parallel processing of listhub data" );
		if (!dev.parallelList.length) {
			dev.readyStartParsing = true;
			dev.startParallel();
		}
		else
		{
			if (myTimerList.length)
				myTimerList.splice(0, myTimerList.length); // clear out
			myTimerList[0] = 0;  // just backfill to zero
			for(var i = 1; i <= dev.totalBlocks; i++)
	  		{
	  			if (dev.parallelList[i] != null) {
		  			// if (dev.parallelList[i][1] == BlockState.PROCESSING) // if it's not completed or error'ed previous chunk choked.
		  			dev.parallelList[i][1] = BlockState.VIRGIN; // reset flag
		  		}
		  		else
		  			console.log("** The ith("+i+") element is null during initialization of restasrting parallel processing.");
		  		//dev.myTimerList[i] = 0; // this is 1-based
		  		//myTimerList[i] = 0; // this is 1-based
			}
			//console.log("dev.restartParallel is calling dev.parallelize");
			dev.initParallelize();
		}
	}

	this.retryStartParallel = function() {
		dev.startParallel();
	}
	/**
	* process the listhub data blocks saved into the db
	*/
	this.startParallel = function() {
		// if (!dev.removedActiveP2Db) { // this would be false if startParallel was called after P2Db, but forced to true if restarting PP.
		if (!dev.doingSpecialParallelOperation &&
			!dev.readyStartParsing) { // this would be false if startParallel was called after P2Db, but forced to true if restarting PP.
			var curTime = new Date().getTime();
			if ( (curTime - dev.removeActiveP2DbStart) > 5000) {
				dev.removeActiveP2Db();
				window.setTimeout(function() {
					dev.startParallel();
				}, 200);
			}
			else {
				if (dev.removedActiveP2Db) 
					dev.readyStartParsing = true;
				window.setTimeout(function() {
					dev.startParallel();
				}, 200);
			}
			return;
		}

		if (dev.reloadPage)
			location.reload();

		if (dev.stopTheMadness) // then END_PARALLEL must have been issued.
		{
			dev.stopTheMadness = false;
			dev.doingMonitor = 0;
			return;
		}

  	 	if (dev.parallelList.length)
  	 		dev.parallelList.splice(0, dev.parallelList.length); // clear out

  	 	if (myTimerList.length)
				myTimerList.splice(0, myTimerList.length); // clear out
			myTimerList[0] = 0;  // just backfill to zero

		$('#legend-div span#added').html(!dev.doingSpecialParallelOperation ? 'added' : (dev.doingSpecialParallelOperation == 1 ? 'imported' : (dev.doingSpecialParallelOperation == 2 ? 'imported' : 'fixed')));
		$('#legend-div span#dups').html(!dev.doingSpecialParallelOperation ? 'dups' : (dev.doingSpecialParallelOperation == 1 ? 'ignored' : (dev.doingSpecialParallelOperation == 2 ? 'processed' : 'already good')));
		$('#legend-div span#rejected').html(!dev.doingSpecialParallelOperation ? 'rejected' : (dev.doingSpecialParallelOperation == 1 ? 'failed' : (dev.doingSpecialParallelOperation == 2 ? 'error' : 'no images')));
		$('#legend-div span#belowMinPrice').html(!dev.doingSpecialParallelOperation ? 'belowMinimum' : 'ignored');
		
  	 	$('#progress').text( "Starting parallel processing of listhub data" );

  		for(var i = 1; i <= dev.totalBlocks; i++)
  		{
  			unit = [i, BlockState.VIRGIN];
  			dev.parallelList[i] = unit;
		}
		dev.initParallelize();
	}

	this.initParallelize = function() {
		if (dev.amazon)
			dev.tryAjax({
					query: 'start-aws',
					mountain: dev.myMountain,
					everest: dev.myEverest
			});
		else
			dev.startingEc2 = true; // so it doesn't hang up 

		dev.parallelize();
	}

	this.parallelize = function() {
		if (!dev.startingEc2) {
			window.setTimeout(function() { dev.parallelize(); }, 1000);
			return;
		}
		dev.getLastProgressCount(true);
		dev.writeLogHeaderForParallel();
		dev.startParallizing();
	}

	this.retryStartParallizing = function() {
		dev.getLastProgressCount(true);
		dev.startParallizing();
	}

	this.startParallizing = function() {
		if ( !dev.gotMyProgressStartPoint ||
			 !dev.gotMyhaveActiveBE ||
			 ! (dev.wrotelogHeaders || dev.errorWriteLogHeaders) ||
			 dev.haveActiveBE == 'working' )
		{
			setTimeout(function() {
				dev.startParallizing();
			}, 500);
			return;
		}

		if (dev.errorWriteLogHeaders) {
			return;
		}

		dev.readyStartParsing = true;
		// dev.linesReported = dev.myProgressStartPoint;
		// double check BE status
		setBEStates();
		if (dev.haveActiveBE != 'false') 
		{
			var doIP = dev.doIP();

			if (haveMyOwn) {
				var mode = !doIP ? "Parallel" : "Image";
				ahtb.open({
		            title: 'Parallel Processing Dilemma',
		            height: 180,
		            width: 550,
		            html: '<p>There appears to be '+mode+' Processing being run already, aborted or failed badly.<br/>Do you want to force a '+mode+' Processing?<br/>Beware!! Beware!! This is a very dangerous act.  Be sure you know what you are doing!</p>',
			        buttons:[{text:"No",
		            		   action:function(){ dev.cleanUpFailedPPStart(); ahtb.close(); }},
		            		 {text:"Force PP",
		            		   action:function(){ dev.forcePP(); ahtb.close(); }}
		            		],
		            closed: function(){
		              ahtb.showClose(250);
		              ahtb.closeOnClickBG(1);	              
		            }
		        });
		        return;
			}

			if (doIP) {
				if (haveIP) {
					ahtb.alert("Someone with owner id:"+otherIPOwner+", is image processing somewhere,<br/>you gotta wait now...",
		  					{height: 150});
					return;
				}
			}
			else if (havePP ||
					 haveP2Db) {
				console.log("Active PP session is being run by someone right now, you gotta wait...");
				ahtb.alert("Someone with owner id:"+otherPPOwner+",is doing an active PP session somewhere, you gotta wait now...",
		  						{height: 150});
  				return;
			}
		}

		dev.resetCommonDataStructures();

		var startTime = dev.startTaskTime = new Date().getTime();
		dev.lastInitImageChunkSentTime = 0;
		dev.initialImageChunksBeingSent = true;
		dev.initialImageChunksCount = 0;
		dev.noChunksToRecover = false;

  	 	dev.registerActivePP();
  	 	dev.startPP();
	}

	this.cleanUpFailedPPStart = function() {
		$('#estimate').val("");
		// $('ul#tables #stopParallel').hide();
		// $('ul#tables #restartParallel').show();
		// $('ul#tables #oneChunkSpan').show();
		dev.checkActivePP();
	}

	

	this.retryStartPP = function() {
		dev.startPP();
	}

	this.startPP = function() {
		if (!dev.haveStartActivePP) {
			setTimeout( function() { dev.startPP(); }, 1000);
			return;
		}

		var doIP = dev.doIP();

		if ( (!doIP &&
			 !dev.startingActivePP) ||
			 (doIP &&
			 !dev.startingActiveIP) ) {
			console.log("Failed to get ok to start "+(!doIP ? "PP" : "IP"));
			// $('ul#tables #stopParallel').hide();
			// $('ul#tables #restartParallel').show();
			// $('ul#tables #oneChunkSpan').show();
			var mode = !doIP ? "Parallel" : "Image";
			ahtb.open({
	            title: 'Parallel Processing Dilemma',
	            height: 180,
	            width: 550,
	            html: '<p>There appears to be '+mode+' Processing being run already, aborted or failed badly.<br/>Do you want to force a '+mode+' Processing?<br/>Beware!! Beware!! This is a very dangerous act.  Be sure you know what you are doing!</p>',
		        buttons:[{text:"No",
	            		   action:function(){ dev.cleanUpFailedPPStart(); ahtb.close(); }},
	            		 {text:"Force PP",
	            		   action:function(){ dev.forcePP(); ahtb.close(); }}
	            		],
	            closed: function(){
	              ahtb.showClose(250);
	              ahtb.closeOnClickBG(1);	              
	            }
	        });
			return;
		}

		dev.doingParallel = true;
		dev.slowDownIP = false;
		var i;

		for(i = 1; i <= dev.numSubDomains; i++)
		{
			dev.myParallelResults[i] = {domain: (i), 
										startTime: 0,
										active: 0, 
										sent: 0, 
										completed: 0, 
										failed: 0, 
										total: 0, 
										madnessCount: 0,
										mySubDomain: ''};
			myTimerList[i] = 0;
		}

		ahtb.close();
		dev.insertTable();
		for(i = 1; i <= dev.numSubDomains; i++)
		{
			dev.prepSubdomain(i);
		}

		dev.chartLapsed = new Date().getTime();
				
		//$('ul#tables #stopParallel').show();
		dev.checkActivePP();
		dev.startTime = new Date().getTime();
	}

	this.insertTable = function()
	{
	    var num_rows = !dev.doingSpecialParallelOperation ? 6 : 5;
	    var num_cols = dev.numSubDomains + 1;
	    var width = 100;
	    var theader = "<table id='subDomainTable' width = ' "+ width +"% style='margin: 0px auto;'>";
	    var tbody = "";

	    for(var j = 0; j < num_cols; j++)
	    {
	    	if (j == 0)
	    		theader += "<th> "+ dev.unitsPerChunk+' listings per' +" </th>";
	    	else
	      	theader += "<th># "+ (j) +" </th>";
	    }

	    for(var i = 0; i < num_rows; i++)
	    {
	        tbody += "<tr align='center'>";
	        var title = '';
	        switch(i) {
	        	case 0: title = 'active'; break;
	        	case 1: title = 'completed'; break;
	        	case 2: title = 'total'; break;
	        	case 3: title = 'balanced'; break;
	        	case 4: title = !dev.doingSpecialParallelOperation ? 'listings added' : (dev.doingSpecialParallelOperation == 1 ? 'imported' : (dev.doingSpecialParallelOperation == 2 ? 'imported' : 'fixed')); break;
	        	case 5: title = 'belowMinPrice'; break;
	        }
	        for(var j = 0; j < num_cols; j++)
	        {
	            tbody += "<td id="+title+j+" style='align: center'>";
	            tbody += j == 0 ? title : ((i == 3 || i == 4) ? "0": "?");
	            tbody += "</td>"
	        }
	        tbody += "</tr>";
	    }
	    var tfooter = "</table>";
	    $('#tableForm').html(theader + tbody + tfooter);
	}

	this.updateTable = function(){
		var row = 0;
		$("#subDomainTable").find("tr").each(function () 
	    {
	    	var col = 0;
	    	$(this).find("td").each(function() {
	    		if (col) {
	    			if (dev.myParallelResults[col] != 'undefined')
	    				$(this).html( row == 1 ? dev.myParallelResults[col].active : 
	    							 (row == 2 ? dev.myParallelResults[col].completed : 
	    							 (row == 3 ? dev.myParallelResults[col].total :
	    							 (row == 4 ? dev.myParallelResults[col].chunksAdopted :
	    							 (row == 5 ? dev.myParallelResults[col].added :
	    							 			 dev.myParallelResults[col].belowMinimumAcceptedPrice)))));
	    		}
	    		col++;
	    	});
	    	row++;
	    }); 
	}

	this.prepSubdomain = function(i)
	{
		setTimeout(function() { 
			dev.subDomain(i); 
			if (i == dev.numSubDomains) { // all started
				dev.startAmIDoneTimer(); // so start looking for end of it all
				dev.lastCheckedResultSynchronicity = new Date().getTime();
				if (resultsTimer == null)
					dev.startResultsTimer(); // and for results
			}
		}, 1200 * i);
	}

	this.subDomain = function(myId) // $myId is 1-based!!
	{
		var domainSize = Math.floor(dev.totalBlocks/dev.numSubDomains);
		var myGroupStartId = (myId - 1)*domainSize;
		//var failed = new counter();
		//var activeChunks = new counter();
		//var chunksCompleted = new counter();
		//var chunksSent = new counter();
		var upperBlockLimit = 0; // this is the upper bounds of blocks this domain is processing
		if (myId == dev.numSubDomains)
			domainSize += dev.totalBlocks % dev.numSubDomains; // if the last subDomain, get the remainder
		upperBlockLimit = myGroupStartId+domainSize; // if myGroupStart == 0, and domainSize == 100, this will be 100, so alway compare with <, not <=

		var startTime = new Date().getTime();

		// figure out new subdomain path
		var mySubDomainRoute = ah_local.tp;
		var myPrefix = "ajax"+myId+"-";
		var time = getTime();
		if (mySubDomainRoute.indexOf('//alpha.') != -1)
			myPrefix = 'alpha-'+myPrefix;
		if (dev.amazon != null)
			mySubDomainRoute = "http://"+dev.amazon[myId-1]['PublicIP']+"/wp-content/themes/allure";
		else if (dev.host == 'Linux')
			mySubDomainRoute = mySubDomainRoute.replace("http://", "http://"+myPrefix);
		else if (dev.host == 'Mint')
			mySubDomainRoute = "http://"+myPrefix+"developer/wp-content/themes/allure";

		dev.myParallelResults[myId].mySubDomain = mySubDomainRoute;
		dev.unitsPerChunk = typeof dev.unitsPerChunk == 'number' ? dev.unitsPerChunk : parseInt(dev.unitsPerChunk);
		dev.myParallelResults[myId].startTime = startTime;
		dev.myParallelResults[myId].startId = myGroupStartId+1; // one based
		dev.myParallelResults[myId].domainSize = domainSize;
		dev.myParallelResults[myId].upperBlockLimit = upperBlockLimit;
		// figure out total number of chunks it will run
		dev.myParallelResults[myId].total = Math.floor(domainSize/dev.unitsPerChunk);
		dev.myParallelResults[myId].total += (domainSize % dev.unitsPerChunk) ? 1 : 0;
		dev.myParallelResults[myId].chunkStates = [];
		for(var x = 0; x < dev.myParallelResults[myId].total; x++) {
			var chunkStartId = (x*dev.unitsPerChunk)+dev.myParallelResults[myId].startId;
			var chunkUpperLimit = (chunkStartId + dev.unitsPerChunk - 1) <= upperBlockLimit ? (chunkStartId + dev.unitsPerChunk - 1) : upperBlockLimit;
			dev.myParallelResults[myId].chunkStates.push({	state: ChunkState.VIRGIN, 
															startId: chunkStartId, 
															upperLimit: chunkUpperLimit, 
															sentAt: startTime, 
															badProcessing: 0, 
															badTime: startTime,
															queryStatus: 0,
															queryStatusDenial: 0 });
		}
		
		dev.myParallelResults[myId].added = 0;
		dev.myParallelResults[myId].chunksAdopted = 0;
		dev.myParallelResults[myId].belowMinimumAcceptedPrice = 0;
		if (dev.doingMonitor)
			return; // just set up the myParallelResults array...

		dev.startSubdomain(myId);
	}

	this.findBalance = function(myId) {
		var x = dev.myParallelResults[myId].total-1; // chunks are 0-based
		var virgins = 0;
		for(; x >= 0; x--) {
			if (typeof dev.myParallelResults[myId].chunkStates[x].state != 'undefined' &&
				dev.myParallelResults[myId].chunkStates[x].state == ChunkState.VIRGIN)
				virgins++;
			else
				break;
		}
		return virgins;
	}

	this.spreadTask = function(active, idle)
	{
		active = typeof active == 'number' ? active : parseInt(active);
		idle = typeof idle == 'number' ? idle : parseInt(idle);

		if (active == idle ||
			dev.stopTheMadness)
			return;

		// find consecutive VIRGIN chunkds for active
		var virgins = dev.findBalance(active);

		console.log("spreadTask from SubDomain: "+active+" to SubDomain: "+idle+", finds "+virgins/2+" to distribute");

		if (virgins < 4)
			return; // don't bother

		// easiest thing to do here is to take off the end and adjust the total chunk value in active
		// first clear out the idle's chunkStates...
		dev.myParallelResults[idle].chunkStates.splice(0, dev.myParallelResults[idle].chunkStates.length); // clear out
		dev.myParallelResults[idle].total = 0;
		dev.myParallelResults[idle].domainSize = 0;
		var x = dev.myParallelResults[active].total-1; // reset, chunks are 0-based
		virgins = Math.floor(virgins/2);
		var i = 0; // use as counter
		for(; i < virgins; i++) {
			dev.myParallelResults[idle].chunkStates.unshift(dev.myParallelResults[active].chunkStates.pop());
			dev.myParallelResults[idle].total++;
			dev.myParallelResults[active].total--;

			// get values from this chunk to get ranges to reset
			var range = dev.myParallelResults[idle].chunkStates[0].upperLimit - dev.myParallelResults[idle].chunkStates[0].startId + 1;

			// adjust upperBlockLimits
			if (i == 0) //first one
				dev.myParallelResults[idle].upperBlockLimit = dev.myParallelResults[idle].chunkStates[0].upperLimit;
			dev.myParallelResults[active].upperBlockLimit = dev.myParallelResults[idle].chunkStates[0].startId - 1;
			// ajust startId
			dev.myParallelResults[idle].startId = dev.myParallelResults[idle].chunkStates[0].startId;

			// adjust domainSize & total # of chunks
			dev.myParallelResults[idle].domainSize += range;
			dev.myParallelResults[active].domainSize -= range;
		}

		// reset some governing counters
		dev.myParallelResults[idle].completed = 0;
		dev.myParallelResults[idle].sent = 0;
		dev.myParallelResults[idle].active = 0;
		dev.myParallelResults[idle].failed  = 0;
		dev.myParallelResults[idle].madnessCount = 0;
		dev.myParallelResults[idle].chunksAdopted += dev.myParallelResults[idle].total;

		console.log("spreadTask between active SubDomain: "+active+", to idle SubDomain: "+idle+", "+i+" chunks of task, for a total of "+dev.myParallelResults[idle].domainSize+" blocks, startId:"+dev.myParallelResults[idle].startId+", upperLimit:"+dev.myParallelResults[idle].upperBlockLimit+", active's startId:"+dev.myParallelResults[active].startId+", upperLimit:"+dev.myParallelResults[active].upperBlockLimit);
		dev.startSubdomain(idle);
	}

	this.activeChunksCount = function() {
		if (dev.myParallelResults.length == 0)
			return 0;

		var active = 0;
		for(var i in dev.myParallelResults) {
			if (typeof dev.myParallelResults[i] != 'undefined' &&
				typeof dev.myParallelResults[i] == 'object' &&
				typeof dev.myParallelResults[i].active != 'undefined')
				active += dev.myParallelResults[i].active;
		}

		return active;
	}

	this.startSubdomain = function(myId) {
		// console.log("startSubdomain for "+myId);
		myTimerList[myId] = window.setInterval(function() {
			if (dev.doingMonitor)
				return; // why is it doing here when monitoring?

			if (!myTimerList[myId])
				return; // timer is null'ed out already

			var curDate = new Date();
			var curTime = curDate.getTime();

			if ( dev.initialImageChunksBeingSent &&
				 dev.initialImageChunksCount >= MAX_INITIAL_IMAGE_CHUNK_TO_SEND &&
				 dev.doIP() ) {
				if ( (curTime - dev.lastInitImageChunkSentTime) < DELAY_AFTER_INITIAL_IMAGE_CHUNKS_SENT )
					return;

				dev.initialImageChunksBeingSent = false; // done waiting!
			}

			// if (dev.doingPPRecords) {
			// 	// console.log("startSubdomain("+myId+") - finds doingPPRecords to be true");
			// 	return;
			// }

			// if (dev.gettingMyProgressStartPoint) {
			// if (!dev.gotMyProgressStartPoint) {
			// 	// console.log("startSubdomain("+myId+") - finds gettingMyProgressStartPoint at "+dev.gettingMyProgressStartPoint);
			// 	return;
			// }

			if ( dev.slowDownIP ) {
				if ( dev.activeChunksCount() == 0)
					dev.slowDownIP = false;
				else {
					return;
				}
			}

			// figure out new subdomain path
			var mySubDomainRoute = dev.myParallelResults[myId].mySubDomain;

			var query =  !dev.doingSpecialParallelOperation  ? 'listhubparallel' : 
						 (dev.doingSpecialParallelOperation == SpecialOp.IMPORT_ACTIVE_LISTINGS_IMAGE ? 'importparallel' : 
						 (dev.doingSpecialParallelOperation == SpecialOp.CREATE_IMAGES_FROM_IMPORTED ? 'genImagesParallel' : 
						 (dev.doingSpecialParallelOperation == SpecialOp.IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 'customImageParallel' : 'fixImageBorderParallel')));
			var startTime = new Date().getTime();
			var newChunk = [];					

			var active = 0;
			var complete = 0;
			var curIndex = 0;
			var lost = 0;
			
			// if (dev.myParallelResults[myId].active >= dev.numChunkPerSubDomain)
			// 	return;


			var activeChunks = 0;
			var processing = 0;
			var x = 0;
			var badCounter = ((dev.unitsPerChunk / 10)+(dev.unitsPerChunk % 10 ? 1 : 0)) * (dev.doingSpecialParallelOperation > SpecialOp.CREATE_IMAGES_FROM_IMPORTED ? ImageProcessingBadCounterMultiplier : ParallelProcessingBadCounterMultiplier);
			for( ; x < dev.myParallelResults[myId].total; x++) {
				if ((dev.myParallelResults[myId].chunkStates[x].state == ChunkState.SENT ||
					 dev.myParallelResults[myId].chunkStates[x].state == ChunkState.ACKED) &&
					(curTime - dev.myParallelResults[myId].chunkStates[x].badTime) > 2000)
				{
					// if (dev.myParallelResults[myId].chunkStates[x].badProcessing < 2) {
					// 	dev.myParallelResults[myId].chunkStates[x].badProcessing++;
					// 	dev.myParallelResults[myId].chunkStates[x].badTime = curTime;
						if (dev.recordChunkChecking)
							console.log("###### SubDomain: "+myId+" detected a nth chunk:"+x+" with startId of "+dev.myParallelResults[myId].chunkStates[x].startId+", was "+chunkStateStr(dev.myParallelResults[myId].chunkStates[x].state)+", but never PROCESSED for "+dev.myParallelResults[myId].chunkStates[x].badProcessing+" times.  So far waited  "+(curTime - dev.myParallelResults[myId].chunkStates[x].sentAt)/1000.0+" secs at "+curDate.toTimeString()+" on query:"+query);
					// }
					// else {
						var curState = dev.myParallelResults[myId].chunkStates[x].state;
						dev.checkChunkStatus(myId, x);
						dev.myParallelResults[myId].chunkStates[x].badTime = curTime;
						// var msg = "#-#-#SA SubDomain: "+myId+" detected a nth chunk:"+x+" with startId of "+dev.myParallelResults[myId].chunkStates[x].startId+", was "+chunkStateStr(curState)+", but never PROCESSED for over "+(curTime - dev.myParallelResults[myId].chunkStates[x].sentAt)/1000.0+" secs.  Resetting to "+chunkStateStr(dev.myParallelResults[myId].chunkStates[x].state)+" at "+curDate.toTimeString()+" on query:"+query;
						// dev.log(msg);
						// console.log(msg);
					// }
				} 
				// started processing but never ended
				else if (dev.myParallelResults[myId].chunkStates[x].state == ChunkState.PROCESSING &&
						(curTime - dev.myParallelResults[myId].chunkStates[x].badTime) > 5000) { // every 5 sec..
					// if ( (dev.myParallelResults[myId].chunkStates[x].badProcessing+1) < badCounter &&
					// 	 (curTime - dev.myParallelResults[myId].chunkStates[x].sentAt) < ((dev.unitsPerChunk * 60000) * (dev.doingSpecialParallelOperation > SpecialOp.CREATE_IMAGES_FROM_IMPORTED ? ImageProcessingDelayPerImageMultiplier : 1)) ) {
					// 	dev.myParallelResults[myId].chunkStates[x].badProcessing++;
					// 	dev.myParallelResults[myId].chunkStates[x].badTime = curTime;
						if (dev.recordChunkChecking)
							console.log("###### SubDomain: "+myId+" detected a nth chunk:"+x+" with startId of "+dev.myParallelResults[myId].chunkStates[x].startId+", was "+chunkStateStr(dev.myParallelResults[myId].chunkStates[x].state)+", but never COMPLETE for "+dev.myParallelResults[myId].chunkStates[x].badProcessing+" times.  So far waited  "+(curTime - dev.myParallelResults[myId].chunkStates[x].sentAt)/1000.0+" secs at "+curDate.toTimeString()+" on query:"+query);
					// }
					// else if (dev.myParallelResults[myId].chunkStates[x].state == ChunkState.PROCESSING) { // double check...
						var curState = dev.myParallelResults[myId].chunkStates[x].state;
						dev.checkChunkStatus(myId, x);
						dev.myParallelResults[myId].chunkStates[x].badTime = curTime;
						// var msg = "#-#-#P SubDomain: "+myId+" detected a nth chunk:"+x+" with startId of "+dev.myParallelResults[myId].chunkStates[x].startId+", was "+chunkStateStr(curState)+", but never COMPLETE for over "+(curTime - dev.myParallelResults[myId].chunkStates[x].sentAt)/1000.0+" secs. QueryStatus count:"+dev.myParallelResults[myId].chunkStates[x].queryStatus+", resetting to "+chunkStateStr(dev.myParallelResults[myId].chunkStates[x].state)+" at "+curDate.toTimeString()+" on query:"+query;
						// dev.log(msg);
						// console.log(msg);
						if (dev.myParallelResults[myId].chunkStates[x].queryStatusDenial > 2) {
							dev.myParallelResults[myId].chunkStates[x].queryStatusDenial = 0;
							dev.myParallelResults[myId].chunkStates[x].queryStatus = 0;
							if (dev.recordChunkChecking)
								dev.log("#-#-#PE SubDomain: "+myId+" with nth chunk:"+x+" with startId of "+dev.myParallelResults[myId].chunkStates[x].startId+", has queryStatusDenial > 0, resetting on query:"+query);
						}
				  	// }
				}

	  			if (dev.myParallelResults[myId].chunkStates[x].state == ChunkState.SENT ||
	  				dev.myParallelResults[myId].chunkStates[x].state == ChunkState.ACKED ||
	  				dev.myParallelResults[myId].chunkStates[x].state == ChunkState.PROCESSING) {
	  				activeChunks++; // this could be more than dev.myParallelResults[myId].active, since 'active' is only set during results retrieval
	  				if (dev.myParallelResults[myId].chunkStates[x].state == ChunkState.PROCESSING)
	  					processing++;
	  			}
	  		}

	  		if (activeChunks >= dev.numChunkPerSubDomain)
				return;

			if (dev.myParallelResults[myId].active != processing) {
				console.log("#0#0#C SubDomain: "+myId+" thinks it has "+dev.myParallelResults[myId].active+" active chunks, but with SENT, ACKED, PROCESSING together, there are "+activeChunks+", and "+processing+" PROCESSING, resetting to "+processing);
				dev.myParallelResults[myId].active = processing;
			}


			if (dev.pausePP)
				return;

			if (myId > dev.maxSubDomainsAllowed)
				return;

			// if (newChunk.length)
			// 	newChunk.splice(0, newChunk.length); // clear out

			cpuLimit = $('ul#tables #loadlimit').val();
			dev.maxCpuDelay = $('ul#tables #cpuDelay').val();
			if (cpuLimit == '')
				cpuLimit = '7.0';

			cpuLimit = parseFloat(cpuLimit);
			dev.maxCpuDelay = parseFloat(dev.maxCpuDelay) * (dev.doingSpecialParallelOperation > SpecialOp.CREATE_IMAGES_FROM_IMPORTED ? ImageProcessingCpuDelayMultiplier : 1.0 );
		 	if (cpuLimit < 1.5)
				cpuLimit = 1.5;
			else if (cpuLimit > 50)
				cpuLimit = 50.0;
			$('ul#tables #loadlimit').val(cpuLimit);
			var curLoad = (dev.tallyLoad/(dev.tallyLoadCount ? dev.tallyLoadCount : 1));

			if (dev.amazon != null) {
				if (dev.tallyLoadPing[myId-1]/(dev.tallyLoadPingCount[myId-1] ? dev.tallyLoadPingCount[myId-1]  : 1) > cpuLimit)
					return;
			}
			else if (curLoad >= cpuLimit)
				return;
			// else if (dev.cpuRange[0] > curLoad)
			// 	return;


			var cpuAvailability = (cpuLimit - curLoad)/cpuLimit;
			var currentCpuAvailabilityLimit = (dev.cpuAvailability/100) * (dev.doingSpecialParallelOperation > SpecialOp.CREATE_IMAGES_FROM_IMPORTED ? ImageProcessingCpuAvailabilityMultiplier : 1.0);
			cpuAvailability = cpuAvailability < 0.09 ? 0.09 : (cpuAvailability >= currentCpuAvailabilityLimit ? 1.0 : cpuAvailability);
			var curDelayMSecs = Math.abs(dev.maxCpuDelay * Math.log10(cpuAvailability)) * 1000; // log10() with less than 1.0 will return a negative value

			if ( dev.ajaxCount >= dev.ajaxLimiter &&
				(curTime - dev.lastTimeChunkSent) > MAX_ALLOW_AJAXCOUNT_PEGGED_SINCE_LAST_SEND) {
				//dev.log("*!*!*! - ajaxCount:"+dev.ajaxCount+" pegged for "+(curTime - dev.lastTimeChunkSent)+" msecs, resetting")
				dev.ajaxCount = 0;
			}

			if ( dev.ajaxCount >= dev.ajaxLimiter ||
				(curTime - dev.lastTimeChunkSent) < curDelayMSecs)
				return;

			if ( (curTime - dev.lastTimeAjaxCountWentToZero) > MAX_ALLOW_AJAXCOUNT_NOT_ZEROED) {
				dev.log("*!*!*! - ajaxCount:"+dev.ajaxCount+" not zeroed for "+(curTime - dev.lastTimeAjaxCountWentToZero)+" msecs, resetting")
				dev.ajaxCount = 0;
				dev.lastTimeAjaxCountWentToZero = curTime;
			}

			// console.log("myId"+myId+", cpuAvailability"+cpuAvailability+", curDelayMSecs:"+curDelayMSecs);
			$('#cpuAvailability').val(cpuAvailability.toFixed(3));
			$('#chunkDelay').val(Math.floor(curDelayMSecs).toString());

			var i = dev.myParallelResults[myId].startId;
			while (i <= dev.myParallelResults[myId].upperBlockLimit)
			{
				if (dev.parallelList[i] != null) {
					if (dev.parallelList[i][1] != BlockState.VIRGIN)
						if (dev.parallelList[i][1] == BlockState.PROCESSING)
							active++;
						else
							complete++;
				} else {
					lost++;
					console.log("** The ith("+i+") element is null for subDomain "+myId+" during initial sorting, startId:"+dev.myParallelResults[myId].startId+", domainSize:"+dev.myParallelResults[myId].domainSize+", upperBlockLimit:"+dev.myParallelResults[myId].upperBlockLimit);
				} 
				i++;
			}

			var thinkItsAllDone = 0;

			if (!activeChunks &&
				(dev.stopTheMadness || dev.myParallelResults[myId].completed >= dev.myParallelResults[myId].total) )
				thinkItsAllDone = 1;

			var haveVirgin = 0;
			for( x = 0; x < dev.myParallelResults[myId].total; x++)
				if (dev.myParallelResults[myId].chunkStates[x].state == ChunkState.VIRGIN) {
					haveVirgin = 1;
					break;
				}

			if (x == dev.myParallelResults[myId].total &&
				!activeChunks)
				thinkItsAllDone = 1;

			if (dev.stopTheMadness)
				thinkItsAllDone = 1;

			if (!activeChunks &&
				!thinkItsAllDone &&
				!haveVirgin &&
				// (dev.myParallelResults[myId].total - dev.myParallelResults[myId].completed) <= 1 && // off by one error
				dev.myParallelResults[myId].completed == dev.myParallelResults[myId].sent &&
				!dev.havePotentialChunksLeft(myId)) {
				thinkItsAllDone = 1;
				var msg = "-^-^- SubDomain:"+myId+", has no activeChunks and was told that there was nothing to distribute, setting thinkItsAllDone to true, stat - sent:"+dev.myParallelResults[myId].sent+", completed:"+dev.myParallelResults[myId].completed+", total:"+dev.myParallelResults[myId].total;
				dev.log(msg);
				ahtb.alert(msg, {width: 450, height: 210});
				dev.setExtraInfo('<p>'+msg+'</p>');
			}

			if ( dev.ajaxCount < dev.numSubDomains && // don't overlap too much
				 !thinkItsAllDone &&
				 !dev.stopTheMadness &&
				 haveVirgin )
				 // x != dev.myParallelResults[myId].total )// then everything is now busy, but not done
				 //(complete + dev.myParallelResults[myId].failed + lost) < dev.myParallelResults[myId].domainSize)
			{
				// this forces each block being serviced to be within this chunk's reange
				var chunkId = x;
				for(i = dev.myParallelResults[myId].chunkStates[x].startId; i <= dev.myParallelResults[myId].chunkStates[x].upperLimit; i++)
				{
					if (dev.parallelList[i] == null) {// should never be...
						console.log("SubDomain: "+myId+" - nth chunk:"+x+", find block at "+i+" to be NULL, this shouldn't be happenin!!!!");
						continue;
					}
					
					// dev.parallelList[i][1] = BlockState.PROCESSING;
					newChunk.push(dev.parallelList[i][0]);
				}

				dev.myParallelResults[myId].chunkStates[x].state = ChunkState.SENT; // mark processing
				dev.myParallelResults[myId].chunkStates[x].sentAt = dev.myParallelResults[myId].chunkStates[x].badTime = curTime;
 				dev.sendingChunk = true;
				// console.log("Requesting for SubDomain: "+myId+" - beginning at block:"+newChunk[0]+" for nth chunk:"+x+" at: "+curDate.toTimeString()+" on query:"+query);
				// window.setTimeout( function() {
					var sent = new Date();
					dev.lastTimeChunkSent = sent.getTime();
					dev.ajaxCount++;	
					if ( dev.initialImageChunksBeingSent &&
						 dev.doIP() ) {
						dev.initialImageChunksCount++;
						if ( dev.initialImageChunksCount == MAX_INITIAL_IMAGE_CHUNK_TO_SEND )
							dev.lastInitImageChunkSentTime = dev.lastTimeChunkSent;
					}

					$.ajax({
						url: mySubDomainRoute+'/_admin/ajax_developer.php',
						data: { query: query,
								list: newChunk,
								mountain: dev.myMountain,
								everest: dev.myEverest,
								id: myId,
								cpuLimit: cpuLimit,
								saverejected: dev.saveRejected,
								minTags: dev.minTags,
								minPrice: dev.minPrice,
								ppRunId: dev.ppRunId,
								op: dev.imageBorderOp,
								onlynew: dev.processOnlyNewImages,
								specialOp: dev.doingSpecialParallelOperation,
								locksql: dev.locksql },
						dataType: 'JSON',
						type: 'POST',
						timeout: 36000000,
						error: function($xhr, $status, $error){
							if (dev.ajaxCount)
								dev.ajaxCount--;

							var curData = new Date();
							if (dev.ajaxCount == 0)
								dev.lastTimeAjaxCountWentToZero = curData.getTime();

							dev.sendingChunk = false;
							dev.tallyNetworkConnection++;
							console.log("********* Failed chunk - in ajax error frame subDomain: "+myId+", unknown ids, total failures:"+dev.failedBlocks+", possible startId:"+newChunk[0]+", active now:"+dev.myParallelResults[myId].active+", at: "+curDate.toTimeString());
					  		
							if ($xhr.responseText &&
								$xhr.responseText.indexOf("database"))
								return;

							var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
							ahtb.alert('You Have Encountered an Error during chunk processing.<br/>Status: '+($status ? $status : "No status.")+"-  Detail: "+msg+", in subDomain:"+myId+", possible startId:"+newChunk[0],
										{height: 150});
							dev.log("#-#-#E calling checkChunkStatus SubDomain: "+myId+" detected a nth chunk:"+chunkId+", errored out in ajax call");
							window.setTimeout(function() {
								dev.checkChunkStatus(myId, chunkId);
							}, 2000);
					  		
						},
					  	success: function(data){
					  		if (dev.ajaxCount)
					  			dev.ajaxCount--;

					  		var curData = new Date();
					  		if (dev.ajaxCount == 0)
								dev.lastTimeAjaxCountWentToZero = curData.getTime();
					  		dev.sendingChunk = false;
						  	if (data == null || data.status == null ||
						  		typeof data == 'undefined' || typeof data.status == 'undefined') 
						  	{
						  		ahtb.alert('You Have Encountered an Error during chunk processing.<br/>Unknown cause, in subDomain:'+myId,
						  					{height: 150});
						  		// window.setTimeout(function() {
								dev.checkChunkStatus(myId, chunkId);
								// }, 2000); // retry it
						  		// dev.failedBlocks += dev.unitsPerChunk; // we don't know what was good or bad..
						  		// dev.myParallelResults[myId].failed += dev.unitsPerChunk;
						  		
						  		console.log("********* Failed chunk - in success frame, ajax call, SubDomain: "+myId); //+", unknown ids, total failures:"+dev.failedBlocks+", DECREMENTED active now:"+dev.myParallelResults[myId].active+" **********");
						  	}
						  	else if (data.status == 'OK') {
						  		$showIt = false;
						  		$('#progress').text( data.data+", ajaxCount:"+dev.ajaxCount);
						  		if (typeof data.data == 'string')
						  		{
						  			if (data.data.indexOf("CPU") != -1) { // too much load, denied.
						  				var pos = data.data.indexOf("startId:");
						  				var startingAt = parseInt(data.data.substring(pos+8));
						  				
						  				pos = data.data.indexOf("load:");
						  				var load = parseFloat(data.data.substring(pos+5));
						  				dev.tallyDenied++;
						  				dev.tallyLoadCount++;
						  				dev.tallyLoad += typeof load == 'float' ? load : parseFloat(load);
										$('#load').val((dev.tallyLoad/(dev.tallyLoadCount ? dev.tallyLoadCount : 1)).toFixed(2));
						  				$('#currentLoad').html((dev.tallyLoad/(dev.tallyLoadCount ? dev.tallyLoadCount : 1)).toFixed(2));
										dev.checkChunkStatus(myId, chunkId);
						  				return;
						  			}
						  			else if (data.data.indexOf("Processing") != -1) {
										var pos;
										var load
										if ( (pos = data.data.indexOf("load:")) != -1)
										{
											load = parseFloat(data.data.substring(pos+5)).toFixed(2);
											dev.tallyLoadCount++;
						  					dev.tallyLoad += typeof load == 'float' ? load : parseFloat(load);
											$('#load').val((dev.tallyLoad/(dev.tallyLoadCount ? dev.tallyLoadCount : 1)).toFixed(2));
						  					$('#currentLoad').html((dev.tallyLoad/dev.tallyLoadCount).toFixed(2));
										}
										// console.log(data.data+", at: "+curDate.toTimeString()+", ajaxCount:"+dev.ajaxCount);
										for(i = dev.myParallelResults[myId].chunkStates[x].startId; i <= dev.myParallelResults[myId].chunkStates[x].upperLimit; i++)
										{
											if (dev.parallelList[i] == null) {// should never be...
												console.log("SubDomain: "+myId+" - nth chunk:"+x+", find block at "+i+" to be NULL, this shouldn't be happenin!!!!");
												continue;
											}
											
											dev.parallelList[i][1] = BlockState.PROCESSING;
										}
										dev.myParallelResults[myId].chunkStates[x].state = ChunkState.ACKED;
						  			}
						  			else
						  				console.log(data.data);
						  		}
						  	}
						  	else {
						  		// window.setTimeout(function() {
								dev.checkChunkStatus(myId, chunkId);
								// }, 2000); // retry it
								ahtb.alert('You Have Encountered an Error during chunk processing.<br/>'+data.data,
						  					{height: 150});
								
						  		console.log("Failed chunk - in success frame subDomain: "+myId+", reason:"+data.data+" unknown ids, total failures:"+dev.failedBlocks+", possible startId:"+newChunk[0]+", active now:"+dev.myParallelResults[myId].active);
						  	}// success but not 'OK'
						} // success frame
					});	// ajax	
				// }, dev.ajaxCount * 100);
								
				return; // get out and see if it helps...
			} // if ( (complete + failed) < domainSize)
			//else 
			//	thinkItsAllDone = 1;

			if (thinkItsAllDone == 1)
			{
				window.clearInterval(myTimerList[myId]); // end the madness
				myTimerList[myId] = null;
				var endTime = new Date().getTime();
				var msg = "***** Subdomain:"+myId+" has "+(!dev.stopTheMadness ? 'completed' : 'halted')+" it's group of "+dev.myParallelResults[myId].domainSize+" blocks, completed:"+complete+", failed:"+dev.myParallelResults[myId].failed+", lost: "+lost+", expected chunks:"+dev.myParallelResults[myId].total +", completed chunks:"+dev.myParallelResults[myId].completed+", took "+(endTime-startTime)/1000.0+" secs.";
				console.log(msg);
			}
		},
		// 1200 * ((myId % 4)+1) );
		1000);
	}


	this.retryBeginMonitoringSession = function() {
		dev.beginMonitoringSession();
	}

	this.resetCommonDataStructures = function()
	{
		dev.linesMonitorNeedsToRead = 0;
		dev.failedBlocks = 0;
		dev.activeChunks = 0;
		dev.chunksCompleted = 0;
		dev.chunksSent = 0;
		dev.curIndex = 0;
		dev.chunksExpected = Math.floor(dev.totalBlocks/dev.unitsPerChunk)
		dev.chunksExpected += (dev.totalBlocks % dev.unitsPerChunk) ? 1 : 0;
		dev.noChunksToRecover = false;
		dev.cpuOverloadedCount = 0;
		dev.cpuAccumulatedOverloadEngr1 = 0;

		// char stuff;
		dev.tallyDenied = dev.tallyLoadCount = 0;
		dev.tallyLoad = 0.0;
		dev.chartLapsed = 0;
		var maxPts = dev.chartXAxis > MAX_POINTS ? MAX_POINTS : dev.chartXAxis;
		for(var i = maxPts; i > 0; i--)
			dev.chart.removeData();
		
		dev.chartInterval = 10000; // every 10 secs, update chart
		dev.tallyNetworkConnection = 0;
		dev.tallyAdded = 0;
	 	dev.tallyDups = 0;
	 	dev.tallyRejected = 0;
	 	dev.tallyBelowMinimumAcceptedPrice = 0;
	 	dev.tallyTotalBlocks = 0;
	 	dev.chartLapsed = dev.chartXAxis = 0;
	 	dev.sumAdded = dev.sumDups = dev.sumRejected = dev.sumBelowMinimumAcceptedPrice  = 0;

	 	if (dev.chartDataList.length)
	 	 	dev.chartDataList.splice(0, dev.chartDataList.length); // clear out

		//console.log("entered dev.startParallizing");

		$('#load').val("");
		$('#percentage').val("0.00");
		$('#estimate').val("calculating...");
		$('#addedTotal').html(0);
		$('#dupsTotal').html(0);
		$('#rejectedTotal').html(0);
		$('#currentActive').html(0);
		$('#currentLoad').html("N/A");

		if (dev.myParallelResults.length)
  	 		dev.myParallelResults.splice(0, dev.myParallelResults.length); // clear out
  	 	dev.myParallelResults[0] = 0; // backfill to zero

	}

	this.beginMonitoringSession = function() {
		if (!dev.gotMyProgressStartPoint) {
			setTimeout(function() {
				dev.beginMonitoringSession();
			}, 500);
			return;
		}

		if (dev.doingMonitor != 3)
			dev.doingMonitor = 1; // 0 = regular PP, 
								  // 1 = monitor is just starting up and catching up, 
								  // 2 = monitor is ready to display
								  // 3 = begin replay
								  // 4 = replay slowdown replay
								  // 5 = end replay
		var linesToRead = dev.myProgressStartPoint - dev.linesMonitorStart;
		// prep data structures
		dev.resetCommonDataStructures();
		
		if (dev.parallelList.length)
  	 		dev.parallelList.splice(0, dev.parallelList.length); // clear out

		var i;
  	 	for(i = 1; i <= dev.totalBlocks; i++)
  		{
  			unit = [i, BlockState.VIRGIN];
  			dev.parallelList[i] = unit;
		}

  	 	if (myTimerList.length)
				myTimerList.splice(0, myTimerList.length); // clear out
			myTimerList[0] = 0;  // just backfill to zero

		for(i = 1; i <= dev.numSubDomains; i++)
		{
			dev.myParallelResults[i] = {domain: (i), active: 0, sent: 0, completed: 0, failed: 0, total: 0, madnessCount: 0};
			dev.subDomain(i); // just to set up myParallelResults properly
			myTimerList[i] = 0;
		}

		// at this point, we should be good to go to start monitoring
		// lets see if linesToRead is > chart width 
		dev.linesMonitorNeedsToRead = dev.myProgressStartPoint;
		dev.linesReported = dev.linesMonitorStart; // this will start getResults to read of at the beginning.

		dev.insertTable();

		if ( (dev.linesMonitorNeedsToRead - dev.linesReported) > MAX_POINTS || 
			 (dev.doingMonitor == 3 && (dev.chunksCompleted/dev.chunksExpected)*100.0 < 95.0) ) {
			dev.getPPRecordRange(dev.linesReported+1, dev.linesReported+100);
			dev.readPPRecords();
		}
		else
			dev.startMonitorResultsTimer();		
	}

	this.retryReadPPRecords = function() {
		dev.readPPRecords();
	}

	this.readPPRecords = function() {
		if (!dev.havePPRecords) {
			var curTime = Date.now();
			// if ( ((curTime - dev.lastTryGetPPRecordRange) % 5000) == 0 )
			// 	console.log("Waited for PPRecords for "+((curTime - dev.lastTryGetPPRecordRange)/1000)+" secs");
				// dev.getPPRecordRange();
			setTimeout(function() {
				dev.readPPRecords();
			}, 500);
			return;
		}

		dev.setCpuGovernor();

		//console.log("readPPRecords - records "+((dev.ppRecords == null || typeof dev.ppRecords == 'undefined') ? 'are undefined' : "contain "+dev.ppRecords.length+" records"));
		if (dev.ppRecords == null ||
			typeof dev.ppRecords == 'undefined') {
			dev.checkSynchronicity = false;
			dev.doingPPRecords = false;
			return;
		}

		for(var x in dev.ppRecords) {
			dev.ppRecords[x].type = typeof dev.ppRecords[x].type == 'number' ? dev.ppRecords[x].type : parseInt(dev.ppRecords[x].type);
			dev.ppRecords[x].id = typeof dev.ppRecords[x].id == 'number' ? dev.ppRecords[x].id : parseInt(dev.ppRecords[x].id);
			dev.parsePPResults({status: "OK",
								data: dev.ppRecords[x]});

		}

		if ( (dev.doingMonitor == 1 && (dev.linesMonitorNeedsToRead - dev.linesReported) > (4 * MAX_POINTS)) || 
			 (dev.doingMonitor == 3 && (dev.chunksCompleted/dev.chunksExpected)*100.0 < 95.0) ) {
			dev.getPPRecordRange(dev.linesReported+1, dev.linesReported+100);
			dev.readPPRecords();
		}
		else if (dev.doingMonitor && !dev.checkSynchronicity) {
			// reset some tallys so the chart doesn't go ballistic
			dev.tallyDenied = dev.tallyAdded = dev.tallyDups = dev.tallyRejected = dev.tallyBelowMinimumAcceptedPrice  = 0;
  			dev.tallyLoadCount = 0;
  			dev.tallyLoad = 0.0;
  			dev.tallyNetworkConnection = 0;
  			dev.tallyTotalBlocks = 0;
  			dev.lastCheckedResultSynchronicity = Date.now();
  			if (resultsTimer)
  				window.clearTimeout(resultsTimer); // just in case
			dev.startMonitorResultsTimer();		
		}
		// release the sync flag
		dev.checkSynchronicity = false;
		dev.doingPPRecords = false;
	}

	this.retryStartMonitorResultsTimer = function() {
		dev.startMonitorResultsTimer();
	}

	this.startMonitorResultsTimer = function() {
		//dev.resultsTimer = window.setInterval(function(){
		resultsTimer = window.setTimeout(function(){

			var curTime = Date.now();
			if ( (curTime - dev.lastMonitorCheckedActivePP) > 10000)
			{
				dev.lastMonitorCheckedActivePP = curTime;
				dev.checkActivePP();
			}

			if (!dev.gotMyhaveActiveBE) {
				setTimeout(function(){
					dev.startMonitorResultsTimer();
				}, 200);
				return;
			}

			var timeout = 50;
			if ( (dev.doingMonitor == 1 &&
				  dev.linesReported > (dev.linesMonitorNeedsToRead - (3 * MAX_POINTS)))  || 
			 	 (dev.doingMonitor == 3 && (dev.chunksCompleted/dev.chunksExpected)*100.0 >= 95.0) ) {
				if ( dev.doingMonitor == 1  ||
					 dev.doingMonitor == 3 )
					dev.doingMonitor++; // switch to playback, it should be either 2 or 4 now
			}	
			else if (dev.linesReported >= dev.linesMonitorNeedsToRead)
				timeout = RESULTS_TIMEROUT*3/4; // slow down

			dev.checkSynchronicity = false; // force it off here, just in case...
			dev.getResults();
			if ((dev.doingMonitor == 3 || // replayMode
				 dev.doingMonitor == 4) ||
				(dev.haveActiveBE != 'false' && dev.doingMonitor) )
			 	resultsTimer = window.setTimeout(function(){ dev.startMonitorResultsTimer(); }, timeout);
			else {
			 	var msg = "Monitoring has ended, BE is no longer active.";
			 	if (dev.doingMonitor &&
			 		dev.doingMonitor != 5)
			 		msg = "Monitoring has been halted.";
			 	else if (dev.doingMonitor == 5) // end of replay
			 		msg = "Replay has reached the end.";
			 	dev.doingMonitor = 0;
			 	$('ul#tables #monitorParallel').val( dev.doingMonitor ? "Stop monitoring" : "Monitor parallel list hub data processing");
			 	console.log(msg);
			 	ahtb.alert(msg,
			 				{height: 150});
			 	dev.checkActivePP();
			 	dev.startResultsTimer();
			}
		}, 200)
	}

	this.havePotentialChunksLeft = function(callerId) {
		for(var i = 1; i <= dev.numSubDomains; i++)
			if (i != callerId &&
				dev.findBalance(i) > 4)
				return true;
			
		return false;
	}

	this.startAmIDoneTimer = function() {
		//dev.myIsItAllDoneTimer = window.setInterval(function(){
		myIsItAllDoneTimer = window.setInterval(function(){
			if (myIsItAllDoneTimer == null)
				return;
			var alldone = true;
			var active = [];
			var idle = [];
			for(var i in myTimerList)
				if (i > 0) {
					if (myTimerList[i]) {
						alldone = false;
						active.push(i);
					}
					else
						idle.push(i);
				}

			var curTime = Date.now();
			if ( (curTime - dev.lastResultTime) > (RESULTS_TIMEROUT*4) ) {
				// someething wrong with timer?
				window.clearTimeout(resultsTimer);
				resultsTimer = null;
				if ( dev.doingPPRecords ||
					  dev.checkSynchronicity) {
					if ( (curTime - dev.lastResultTime) > (RESULTS_TIMEROUT*8) &&
						 dev.ppRecords ) {
						var len = dev.ppRecords.length;
						dev.ppRecords = null;
						var msg = "startAmIDoneTimer is resetting ppRecords, "+len+" lines held, from "+dev.getPPRecordStart+" to "+dev.getPPRecordEnd+", it has been held for "+(curTime - dev.lastResultTime)/1000.0+" secs";
						console.log (msg);
						dev.setExtraInfo('<p>'+msg+'</p>');
					}
					 
					if ( dev.ppRecords == null ) {
						dev.doingPPRecords = false;
						dev.checkSynchronicity = false;
						var msg = "startAmIDoneTimer reset doingPPRecords and checkSynchronicity";
						console.log (msg);
						dev.setExtraInfo('<p>'+msg+'</p>');
					}
				}
				dev.lastCheckedResultSynchronicity = Date.now();
				dev.startResultsTimer();
				console.log ("startAmIDoneTimer is restarting resultsTimer, it has been non-responsive for "+(curTime - dev.lastResultTime)/1000.0+" secs");
			}

			if (dev.stopTheMadness && !alldone) {
				var notYet = false;
				for(var i = 1; i <= dev.numSubDomains; i++)
				{
					if (dev.myParallelResults[i].completed != dev.myParallelResults[i].sent) {
						if (dev.myParallelResults[i].active == 1) {
							if (dev.myParallelResults[i].madnessCount++ > 10) {// 10 * 10 secs already
								var missedStartIds = [];
								for(var x = 0 ; x < dev.myParallelResults[i].total; x++) 
									if (dev.myParallelResults[i].chunkStates[x].state == ChunkState.PROCESSING ||
										dev.myParallelResults[i].chunkStates[x].state == ChunkState.ACKED ||
										dev.myParallelResults[i].chunkStates[x].state == ChunkState.SENT)
										missedStartIds.push(dev.myParallelResults[i].chunkStates[x].startId);

								if (missedStartIds.length) {
									var msg = '';
									for(var j = 0; j < missedStartIds.length; j++) {
										if (j > 0)
											msg += ","
										msg += missedStartIds[j].toString();
									}
									console.log("SubDomain: "+i+", has active chunk(s), chunk(s) not tagged as complete with startids:"+msg);
									dev.myParallelResults[i].madnessCount = 0; // reset, hope it clears out
								}
								else // all chunkStates are COMPLETED or ERROR, apparently, so set active to 0
								{
									dev.myParallelResults[i].active = 0;
									dev.myParallelResults[i].completed = dev.myParallelResults[i].sent;
								}
							}						
						}
						notYet = true; // still need to poll results
					}
				}

				if (notYet) { 
					dev.recoverChunk();
					return;
				}

				for(var i in myTimerList)
					if (myTimerList[i]) {
						clearInterval(myTimerList[i]);
					}

				myTimerList.splice(0, myTimerList.length); // clear out
				alldone = true;
			}

			if (!alldone) {
				dev.ping();
				if (active.length < dev.numSubDomains) {// then try to redistribute chunks
					for(var a in active) {
						if (dev.findBalance(active[a]) > 4) {
							if (idle.length) {
								var i = idle.shift(); // get rid of the head element
								dev.spreadTask(active[a], i);
							}
							else
								break; // no more idle ones
						}
					}

				}
			}


			if (alldone)
			{
				var notYet = false;
				for(var i = 1; i <= dev.numSubDomains; i++)
				// for(var i = 1; i <= dev.maxSubDomainsAllowed; i++)
				{
					if (dev.myParallelResults[i].completed != dev.myParallelResults[i].sent) {
						if (dev.myParallelResults[i].active){
							if (dev.myParallelResults[i].madnessCount++ > 10) {// 30 * 10 secs already
								var missedStartIds = [];
								for(var x = 0 ; x < dev.myParallelResults[i].total; x++) 
									if (dev.myParallelResults[i].chunkStates[x].state == ChunkState.PROCESSING ||
										dev.myParallelResults[i].chunkStates[x].state == ChunkState.SENT)
										missedStartIds.push(dev.myParallelResults[i].chunkStates[x].startId);

								if (missedStartIds.length) {
									var msg = '';
									for(var j = 0; j < missedStartIds.length; j++) {
										if (j > 0)
											msg += ","
										msg += missedStartIds[j].toString();
									}
									console.log("SubDomain: "+i+", has active chunk(s), chunk(s) not tagged as complete with startids:"+msg);
									dev.myParallelResults[i].madnessCount = 0; // reset, hope it clears out
								}
								else // all chunkStates are 2, apparently, so set active to 0
								{
									dev.myParallelResults[i].active = 0;
									dev.myParallelResults[i].completed = dev.myParallelResults[i].sent;
								}
							}						
						}	
						notYet = true; // still need to poll results
					}					
				}

				if (notYet &&
					!dev.noChunksToRecover) { // if noChunksToRecover is true, then there are no more chunks to process
					dev.recoverChunk();
					return;
				}

				var msg;
				if (dev.noChunksToRecover) {
					msg = "ll blocks appear finished, noChunksToRecover is true";
					dev.log(msg);
					$('#progress').text(msg);
					dev.setExtraInfo('<p>'+msg+'</p>');
				}
				
				var endTime = Date.now();
				if (!dev.stopTheMadness)
					msg = "All blocks "+dev.totalBlocks+" have been completed, took "+(endTime-dev.startTime)/1000.0+" secs, added:"+dev.sumAdded+", dups:"+dev.sumDups+", rejected:"+dev.sumRejected+", belowMinimumAcceptedPrice:"+dev.sumBelowMinimumAcceptedPrice;
				else
					msg = "All blocks finished, jobs have been terminated.";
				$('#estimate').val('All done..');

				dev.cpuOverloadedCount = 0;
				dev.cpuAccumulatedOverloadEngr1 = 0;

				dev.log(msg);
				$('#progress').text(msg);
				dev.setExtraInfo('<p>'+msg+'</p>');
				if (!dev.doingRemote)
					ahtb.alert(msg,
			  					{height: 150});

				$.ajax({
					url: ah_local.tp+'/_admin/ajax_developer.php',
					data: { query: 'record-parse-result',
							mountain: dev.myMountain,
							everest: dev.myEverest,
							result: msg },
					dataType: 'JSON',
					type: 'POST',
					error: function($xhr, $status, $error){
						var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
						ahtb.open({ title: 'You Have Encountered an Error during record-parse-result', 
									height: 150, 
									html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
								});
					},					
				  	success: function(data){
				  	if (data == null || data.status == null ||
				  		typeof data == 'undefined' || typeof data.status == 'undefined') 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else if (data.status != 'OK') {
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed to precord-parse-result for parallel processing</p>' });
				  	}
				  	else {
				  		console.log("Result recorded: "+msg);
				  	}
				  }
				});

				// if (dev.doIP()) {
				// 	dev.imageBorderOptimizeTableAfterwards = true; // always optimize table after image processing
				// }
				//window.clearTimeout(resultsTimer);
				//resultsTimer = null;
		
				window.clearInterval(myIsItAllDoneTimer);
				myIsItAllDoneTimer = null;
				
				dev.removeActivePP();
				dev.checkActivePPStatus();
			}
		},
		10000);
	}

	this.startResultsTimer = function() {
		//dev.resultsTimer = window.setInterval(function(){
		//dev.lastCheckedResultSynchronicity = new Date().getTime();
		resultsTimer = window.setTimeout(function(){
			// if (myIsItAllDoneTimer == null)
			//  	return;

			var curTime = Date.now();
			if ( !dev.doingParallel &&
				 (curTime - dev.lastMonitorCheckedActivePP) > 10000)
			{
				dev.lastMonitorCheckedActivePP = curTime;
				dev.checkActivePP();
			}

			 resultsTimer = window.setTimeout(function(){ dev.startResultsTimer(); }, RESULTS_TIMEROUT);
			 dev.getResults();
			 dev.ping(); // this is called all the time
		}, RESULTS_TIMEROUT)
	}

	this.rangeCheck = function(subDomainId, blockId) {
		return (dev.myParallelResults[subDomainId].startId <= blockId &&
				dev.myParallelResults[subDomainId].upperBlockLimit >= blockId) ? true : false;
	}

	this.retryCheckIfSychronizationNeeded = function() {
		dev.checkIfSychronizationNeeded();
	}

	this.checkIfSychronizationNeeded = function() {
		if (!dev.checkSynchronicity) {
			dev.checkSynchronicity = true; 
			var curTime = Date.now();
			if ( (curTime - dev.lastCheckedResultSynchronicity) > dev.synchroDeltaTime) {// try again... 
				console.log("checkIfSychronizationNeeded did not get gotMyProgressStartPoint for "+(curTime - dev.lastCheckedResultSynchronicity)+" secs, retrying..");
				dev.lastCheckedResultSynchronicity = curTime;
				if (!dev.gettingMyProgressStartPoint) dev.getLastProgressCount();
			}
		}

		// console.log("checkIfSychronizationNeeded is calling out verifySynchronicity");
		dev.verifySynchronicity();

		// if (!dev.gotMyProgressStartPoint)
		// if (dev.gettingMyProgressStartPoint)
		// {
		// 	dev.checkSynchronicity = true; // so if doingMonitor, don't kick off another startMonitorResultsTimer().
		// 	var curTime = new Date().getTime();
		// 	if ( (curTime - dev.lastCheckedResultSynchronicity) > 5000) {// try again... 
		// 		console.log("checkIfSychronizationNeeded did not get gotMyProgressStartPoint for "+(curTime - dev.lastCheckedResultSynchronicity)+" secs, retrying..");
		// 		dev.lastCheckedResultSynchronicity = curTime;
		// 		dev.getLastProgressCount();
		// 	}
		// 	setTimeout(function() {
		// 		dev.checkIfSychronizationNeeded();
		// 	}, 50);
		// 	return;
		// }
	}

	this.verifySynchronicity = function() {
		// if (dev.doingMonitor &&
		// 	dev.linesReported < dev.linesMonitorNeedsToRead)
		// 	return;

		// if (dev.gettingMyProgressStartPoint) {
		if (!dev.gotMyProgressStartPoint) {
			// console.log("verifySynchronicity is waiting on gettingMyProgressStartPoint:"+dev.gettingMyProgressStartPoint);
			// console.log("verifySynchronicity is waiting on gotMyProgressStartPoint");
			setTimeout(function() {
				dev.verifySynchronicity();
			}, 500);
			return;
		}

		//console.log("verifySynchronicity - myProgressStartPoint"+dev.myProgressStartPoint+", linesReported:"+dev.linesReported+", has a diff of "+(dev.myProgressStartPoint - dev.linesReported)+" lines of records");

		if ( (dev.myProgressStartPoint - dev.linesReported) < 0 ) {// uh oh
			dev.getLastProgressCount(true);
			var msg = "Negative line value:"+ (dev.myProgressStartPoint - dev.linesReported) + ", resetting start point";
			dev.log(msg);
			dev.setExtraInfo('<p>'+msg+'</p>');
			setTimeout(function() {
				dev.verifySynchronicity();
			}, 500);
		}

		if ( (dev.myProgressStartPoint - dev.linesReported) > dev.synchroDeltaLines ) // need to catch up!!
		{
			//console.log("verifySynchronicity sees "+(dev.myProgressStartPoint - dev.linesReported)+" records are behind, readdPPRecords() called.");
			dev.linesMonitorNeedsToRead = dev.myProgressStartPoint; // do this so we can re-use readPPRecords()
			var total = dev.myProgressStartPoint - dev.linesReported > 1000 ? dev.linesReported+1000 : dev.myProgressStartPoint;
			dev.getPPRecordRange(dev.linesReported+1, total);
			dev.readPPRecords();
		}
		else {
			dev.retrieveResult();
			dev.checkSynchronicity = false;
		}
	}

	this.getResults = function() {
		// if (dev.checkSynchronicity) {
		// 	console.log("getResults - finds checkSynchronicity to be true");
		// 	return;
		// }
		if (dev.doingPPRecords ||
			dev.checkSynchronicity) {
			var curTime = Date.now();
			// console.log("getResults - finds doingPPRecords to be true");
			if (dev.checkSynchronicity) {
				
				if ( (curTime - dev.lastCheckedResultSynchronicity) > 2000) {
					dev.checkSynchronicity = false;
					console.log("checkSynchronicity has been over 2 secs, resettting");
				}
				else
					return;
			}
			else if ( (curTime - dev.lastResultTime) > (RESULTS_TIMEROUT*8) ) {
					if (dev.ppRecords ) {
						var len = dev.ppRecords.length;
						dev.ppRecords = null;
						var msg = "getResults is resetting ppRecords, "+len+" lines held, from "+dev.getPPRecordStart+" to "+dev.getPPRecordEnd+", it has been held for "+(curTime - dev.lastResultTime)/1000.0+" secs";
						console.log (msg);
						dev.setExtraInfo('<p>'+msg+'</p>');
					}

					dev.doingPPRecords = false;
					dev.checkSynchronicity = false;
					var msg = "getResults reset doingPPRecords and checkSynchronicity";
					console.log (msg);
					dev.setExtraInfo('<p>'+msg+'</p>');
			}
			else
				return;
		}

		// console.log("entered dev.getResults()");

		dev.lastResultTime = Date.now();

		if ( (dev.lastResultTime - dev.lastCheckedForCronJobs) > 60000) {
			dev.checkCronJob();
		}
		if ( !dev.checkSynchronicity &&
			 (dev.lastResultTime - dev.lastCheckedResultSynchronicity) > dev.synchroDeltaTime) {// 1/2 min ago
			// console.log("getResults is calling out checkIfSychronizationNeeded()");
			dev.lastCheckedResultSynchronicity = dev.lastResultTime;
			dev.getLastProgressCount();
			dev.checkIfSychronizationNeeded();
		}
		else
			dev.retrieveResult();
	}

	this.setCpuGovernor = function() {
		if (dev.cpuGovernorOverride)
			return;

		var lastTimeCheck = 0;
		var hourCheck = Object.keys(CpuGovernor);
		var date = new Date();
		var hour = date.getHours();
		var day = date.getDay();
		var governorLen = hourCheck.length;
		var pos = 0;
		var whichDay = day == 0 || day == 6 ? 1 : 0; 
		var whichCpu = 0;
		if (dev.doIP()) {
			if (havePP)
				whichCpu = 2;
		}
		else if (haveIP || haveIPCustom)
			whichCpu = 1;

		var serverIsRemote = ah_local.wp.indexOf('engr') != -1;
		var governorSet = serverIsRemote ? CpuGovernorRemote : CpuGovernor;
		
		for(var index in governorSet) {
			if ( parseInt(index) == hour) {
				dev.cpuRange = governorSet[index].limit[whichDay][whichCpu];
				dev.loadLimit = !dev.doIP() ? dev.cpuRange[1] : dev.cpuRange[0];
				dev.cpuRange[0] = parseFloat(dev.cpuRange[0]);
				dev.cpuRange[1] = parseFloat(dev.cpuRange[1]);
				// $('ul#tables #loadlimit').val(CpuGovernor[index].limit[whichDay]);
				$('ul#tables #loadlimit').val(dev.loadLimit); // upper limit of range
				dev.ajaxLimiter = governorSet[index].ajaxLimiter;
				$('ul#tables #ajaxlimiter option[value="'+dev.ajaxLimiter+'"]').prop('selected', true);
				dev.cpuAvailability = governorSet[index].cpuAvailability;
				$('ul#tables #cpuAvailablityGate option[value="'+dev.cpuAvailability+'"]').prop('selected', true);
				break;
			}
			else if (pos < (governorLen-1)) { // else last one, so keep whatever was
				if ( parseInt(hourCheck[pos]) < hour &&
					 hour <  parseInt(hourCheck[pos+1]) ) {
					dev.cpuRange = governorSet[hourCheck[pos]].limit[whichDay][whichCpu];
					dev.loadLimit = !dev.doIP() ? dev.cpuRange[1] : dev.cpuRange[0];
					dev.cpuRange[0] = parseFloat(dev.cpuRange[0]);
					dev.cpuRange[1] = parseFloat(dev.cpuRange[1]);
					// $('ul#tables #loadlimit').val(CpuGovernor[hourCheck[pos]].limit[whichDay]);
					$('ul#tables #loadlimit').val(dev.loadLimit); // upper limit of range
					dev.ajaxLimiter = governorSet[hourCheck[pos]].ajaxLimiter;
					$('ul#tables #ajaxlimiter option[value="'+dev.ajaxLimiter+'"]').prop('selected', true);
					dev.cpuAvailability = governorSet[hourCheck[pos]].cpuAvailability;
					$('ul#tables #cpuAvailablityGate option[value="'+dev.cpuAvailability+'"]').prop('selected', true);
					break;
				}
			}
			pos++;

		}
	}

	this.retrieveResult = function() {
		// dev.ajaxCount++;
		dev.setCpuGovernor();

		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'listhubcurrentprogress',
					id: (dev.linesReported+1),
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				dev.tallyNetworkConnection++;
				// dev.ajaxCount--;
				if ($xhr.responseText &&
					$xhr.responseText.indexOf("database"))
					return;

				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log(msg);
				// ahtb.open({ title: 'You Have Encountered an Error trying to get current progress', 
				// 			height: 150, 
				// 			html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
				// 		});
			},					
		  	success: function(data){
		  		// dev.ajaxCount--;
			  	if (data == null || data.status == null ||
			  		typeof data == 'undefined' || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error during log retrieval', height: 150, html: '<p>Got a return of NULL</p>' });
			  	else if (data.status != 'OK')
			  		ahtb.open({ title: 'You Have Encountered an Error during log retrieval', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
			  	else {
			  		if (data.data == null || data.data == 'undefined')
			  			return;
			  		if (dev.linesReported == data.data['id'])
			  			return;

			  		if ((dev.linesReported+1) != data.data['id'])
			  		{
			  			console.log("!!!!!!! Log retrieval does not have the expect line #, expected:"+(dev.linesReported+1)+", but got instead:"+data.data['id']);
			  			return;
			  		}
			  		dev.parsePPResults(data);
			  	}
		  	} // end success frame
	  	});
	}

	this.parsePPResults = function(data) {
		var type = data.data['type'];
  		var msg = data.data['data'];
  		var showIt = false;	
  		var updateChart = false;
  		var pos;	  		
  		var curTime = new Date();
  		dev.linesReported = parseInt(data.data['id']);

  		// console.log("parsePPResults - type:"+type+" from "+dev.linesReported);

  		if ( type == LogState.IMAGE_BORDER_START ) {
			var result = JSON.parse(data.data.data);
			var mode = result.mode;
			var listingCount = result.listingCount;
			var msg = result.msg+", at "+curTime.toTimeString();
			console.log(msg);
			$('#progress').text( msg );
			return;
		}
		else if ( type == LogState.IMAGE_BORDER_END ) {
			dev.doingImageProcessing = false;
			// RESULTS_TIMEROUT = 300; // slow back down..
			var result = JSON.parse(data.data.data);
			var msg = result.msg+", at "+curTime.toTimeString();
			console.log(msg);
			$('#progress').text( msg );
			return;
		}
		else if ( type == LogState.IMAGE_BORDER_FAILED_GET_COUNT ) {
			dev.setExtraInfo('<p>'+data.data.data+'</p>');
		}
		else if ( type == LogState.IMAGE_BORDER_LISTING_START ||
				  type == LogState.IMAGE_BORDER_LISTING_DONE ||
				  type == LogState.IMAGE_BORDER_FAILED_CREATE_IMAGE_JPEG) {
  			dev.doingImageProcessing = true;
  			// RESULTS_TIMEROUT = 100;
  			if (!dev.imageBorderLogDetails)
  				return;
			var result = JSON.parse(data.data.data);
			var id = result.id;
		 	var msg = result.msg+", at "+curTime.toTimeString();
			console.log(msg);
			$('#progress').text( msg );
			return;
		}
		else if (type == LogState.IMAGE_BORDER_PROCESSING_STAT) {
			if (!dev.doIP())
				return;

			var result = JSON.parse(data.data.data);
			var id = result.listing;
			var retrieved = result.retrieved;
			var perImage = parseFloat(result.perImage);
			var fixedTB = parseInt(result.fixedTB);
			var fixedLR = parseInt(result.fixedLR);

			var msg = "Listing:"+id+", retrieved:"+retrieved+", perImage:"+perImage+", fixedTB:"+fixedTB+", fixedLR:"+fixedLR;
			console.log(msg);
			$('#progress').text( msg );
			$('input#imageRetrievalPerSec').val(result.perImage);
			if ( perImage > ACCEPTABLE_PER_IMAGE_RETRIEVAL_RATE) {
				dev.slowDownIP = true;
			}
			else
				dev.slowDownIP = false;
			return;
		}
		else if ( type == LogState.CLEAN_IMAGE_START ||
				  type == LogState.CLEAN_IMAGE_UPDATE ||
				  type == LogState.CLEAN_IMAGE_END ||
				  type == LogState.CLEAN_IMAGE_INACTIVE ||
				  type == LogState.CLEAN_IMAGE_INIT_COUNT ||
				  type == LogState.CLEAN_IMAGE_RESET_IMAGES ) {
			var result = JSON.parse(data.data.data);
			console.log(result.msg);
			$('#progress').text( result.msg );
			if (type != LogState.CLEAN_IMAGE_INACTIVE) 
				dev.setExtraInfo('<p>'+result.msg+'</p>');
			$('#ajax-msg span#extra').html(result.msg);

			if (type == LogState.CLEAN_IMAGE_INIT_COUNT) {
				dev.beginCleaningOutImageFolder(result);
			}
			return;
		}
		else if ( type == LogState.CLEAN_SQL_TABLE) {
			console.log(msg);
			$('#progress').text( msg );
			return;
		}
		else if ( type == LogState.CLEAN_FOLDER_UPDATE) {
			var result = JSON.parse(data.data.data);
			console.log(result.msg);
			$('#progress').text( result.msg );
		}
		else if ( type == LogState.CONCLUDE_PP_MEDIAN_VALUES_CONSOLIDATE_DAILY ||
				  type == LogState.CONCLUDE_PP_OPTIMIZE_TABLES ||
				  type == LogState.WRITE_LOG_HEADER_START ||
				  type == LogState.WRITE_LOG_HEADER_PRESTART_SQL ||
				  type == LogState.WRITE_LOG_HEADER_EMPTY_TABLE ||
				  type == LogState.WRITE_LOG_HEADER_RESET_DAILY_MEDIAN_VALUES ||
				  type == LogState.WRITE_LOG_HEADER_UPDATE_TIERS ||
				  type == LogState.WRITE_LOG_HEADER_UPDATE_BLOCK_FLAGS ||
				  type == LogState.PROCESS_EXEC_START ||
				  type == LogState.PROCESS_EXEC_END ||
				  type == LogState.PROCESS_EXEC_OUTPUT ) {
			console.log(msg);
			$('#progress').text( msg );
			dev.setExtraInfo('<p>'+msg+'</p>');
			return;
		}
		else if ( type == LogState.WRITE_LOG_HEADER_DONE ) {
			if ( !dev.wrotelogHeaders ) 
				dev.wrotelogHeaders = true;
			console.log(msg);
			$('#progress').text( msg );
			dev.setExtraInfo('<p>'+msg+'</p>');
			return;
		}
		// else if ( type == LogState.BEGIN_UPDATE_EMAIL_DB ) {
		// 	console.log(result.msg);
		// 	$('#progress').text( result.msg );
		// 	getFixImageListingCount(RemoteMode.IMAGE_BORDER_CRON_ALL);
		// 	return;
		// }

  		if ( type == LogState.RELOAD_PAGE &&
  			 dev.canRemotePP ) {
  			console.log('<!<!<!<! Got RELOAD_PAGE command');
  			var result = JSON.parse(data.data.data);
  			dev.ajax({query: 'post-to-progress',
  					  msg: 'Registered unit is reloading page from '+result.remoteRequester+' to - everest:'+dev.myEverest,
					  mountain: dev.myMountain,
					  everest: dev.myEverest });
  			// if (dev.doingParallel ||
  			// 	dev.startingActiveP2Db) {
  			// 	dev.reloadPage = true;
  			// 	dev.stopTheMadness = true;
  			// }
  			// else 
  			if (dev.canRemotePP) {// no more safety net...
  				// location.reload();
  				dev.ajax({query: 'prep-reload',
  					  msg: 'Registered unit is preparing reloading page from '+result.remoteRequester+' to - everest:'+dev.myEverest,
					  mountain: dev.myMountain,
					  everest: dev.myEverest });
  			}
  		}

  		if ( type == LogState.PING_REMOTE &&
  			 dev.canRemotePP ) {
  			console.log('<!<!<!<! Got PING_REMOTE command');
  			dev.ajax({query: 'post-to-progress',
  					  msg: 'Ping to remote acked for - everest:'+dev.myEverest,
					  mountain: dev.myMountain,
					  everest: dev.myEverest });
  			return;
  		}
  		else if ( type == LogState.FLAGS_TO_REMOTE ) {
  			var result = JSON.parse(data.data.data);
  			var applied = false;
			if ( dev.canRemotePP ) {
	  			switch(result.flag) {
	  				case FlagsToRemote.OPTIMIZE_TABLE_AFTER_PARSE:
	  					dev.listhubParseOptimizeTableAfterwards = parseInt(result.value);
	  					applied = true;
	  					break;
	  				case FlagsToRemote.MODIFY_CPU_LIMIT:
	  					$('ul#tables #loadlimit').val(result.value);
	  					dev.cpuGovernorOverride = true;
						$('button#enableCpuGovernor').show();
						applied = true;
	  					break;
	  				case FlagsToRemote.MODIFY_CPU_AVAILABILITY:
	  					dev.cpuAvailability = parseInt(result.value);
	  					$('ul#tables #cpuAvailablityGate option[value="'+dev.cpuAvailability+'"]').prop('selected', true);
	  					dev.cpuGovernorOverride = true;
						$('button#enableCpuGovernor').show();
						applied = true;
	  					break;
	  				case FlagsToRemote.MODIFY_ENGR1_OVERLOAD_SUSTAINED_TIME_LIMIT:
	  					if ( ah_local.wp.indexOf('engr1') != -1) {
	  						dev.cpuMaxHeavyLoadSustainedTimeLimitEngr1 = parseFloat(result.value);
	  						applied = true;
	  					}
	  					break;
	  				case FlagsToRemote.MODIFY_LIVE_OVERLOAD_SUSTAINED_TIME_LIMIT:
	  					if ( ah_local.wp.indexOf('lifestyledlistings.com') != -1) {
	  						dev.cpuMaxHeavyLoadSustainedTimeLimitLive = parseFloat(result.value);
	  						applied = true;;
	  					}
	  					break;
	  				case FlagsToRemote.MODIFY_AJAX_LIMITER:
	  					dev.ajaxLimiter = parseInt(result.value);
	  					$('ul#tables #ajaxLimiter option[value="'+dev.ajaxLimiter+'"]').prop('selected', true);
	  					dev.cpuGovernorOverride = true;
						$('button#enableCpuGovernor').show();
						applied = true;
						break;
					case FlagsToRemote.MODIFY_ENGR1_LOAD_LIMIT:
						if ( ah_local.wp.indexOf('engr1') != -1) {
	  						dev.cpuHeavyLoadLimitEngr1 = parseFloat(result.value);
	  						applied = true;
	  					}
	  					break;
					case FlagsToRemote.MODIFY_LIVE_LOAD_LIMIT:
						if ( ah_local.wp.indexOf('lifestyledlistings.com') != -1) {
	  						dev.cpuHeavyLoadLimitLive = parseFloat(result.value);
	  						applied = true;
	  					}
	  					break;
	  			}
	  		}
	  		else if ( ah_local.wp.indexOf('lifestyledlistings.com') != -1) {
	  			switch(result.flag) {
	  				case FlagsToRemote.MODIFY_LIVE_OVERLOAD_SUSTAINED_TIME_LIMIT:
	  					dev.cpuMaxHeavyLoadSustainedTimeLimitLive = parseFloat(result.value);
	  					applied = true;
	  					break;
	  				case FlagsToRemote.MODIFY_LIVE_LOAD_LIMIT:
	  					dev.cpuHeavyLoadLimitLive = parseFloat(result.value);
	  					applied = true;
	  					break;
	  			}
	  		}
	  		console.log('<!<!<!<! Got FLAGS_TO_REMOTE command for flag:'+flagsToRemoteStr(result.flag)+", value:"+result.value+", applied:"+applied);
  			dev.ajax({query: 'post-to-progress',
  					  msg: 'FLAGS_TO_REMOTE received for flag:'+flagsToRemoteStr(result.flag)+", value:"+result.value+", applied:"+applied+' - everest:'+dev.myEverest,
					  mountain: dev.myMountain,
					  everest: dev.myEverest });
  			return;
 		}
  		else if ( type == LogState.STATS_REMOTE &&
  			 dev.canRemotePP ) {
  			console.log('<!<!<!<! Got STATS_REMOTE command');
  			var activeTimer = 0;
  			for(var i in myTimerList)
  				if (myTimerList[i])
  					activeTimer++;

  			dev.ajax({query: 'post-to-progress',
  					  msg: {everest: dev.myEverest,
  					  		specialOp: dev.doingSpecialParallelOperation,
  					  		doingRemote: dev.doingRemote,
  					  		monitor: dev.doingMonitor,
  					  		imageProcessAfterParse: dev.imageProcessAfterParse,
  					  		linesReported: dev.linesReported,
  					  		pause: dev.pausePP,
  					  		parallelResultCount: dev.myParallelResults.length,
  					  		added: dev.sumAdded,
  					  		dups: dev.sumDups,
  					  		rejected: dev.sumRejected,
  					  		active: dev.activeChunksCount(),
  					  		ppRunId: dev.ppRunId,
  					  		slowDownIP: dev.slowDownIP,
  					  		load: (dev.tallyLoad/(dev.tallyLoadCount ? dev.tallyLoadCount : 1)),
  					  		cpuLimit: $('ul#tables #loadlimit').val(),
  					  		ajaxCount: dev.ajaxCount,
  					  		stopTheMadness: dev.stopTheMadness,
  					  		activeTimer: activeTimer
  					  	},
					  mountain: dev.myMountain,
					  everest: dev.myEverest });
  			return;
  		}

 		if ( (type == LogState.LOCK_SQL ||
 			  type == LogState.UNLOCK_SQL) &&
  			 dev.canRemotePP ) {
  			console.log('<!<!<!<! Got '+(type == LogState.LOCK_SQL ? 'LOCK_SQL' : 'UNLOCK_SQL')+' command');
  			dev.ajax({query: 'post-to-progress',
  					  msg: 'Registered unit is '+(type == LogState.LOCK_SQL ? 'locking' : 'unlocking')+' SQL - everest:'+dev.myEverest,
					  mountain: dev.myMountain,
					  everest: dev.myEverest });
  			dev.locksql = type == LogState.LOCK_SQL ? true : false;
  			$('input#locksql').prop('checked', dev.locksql)
  		}

  		if (!dev.doingMonitor &&
  			!dev.doingParallel &&
  			!dev.startingActiveP2Db) {
  			if (type == LogState.REMOTE_PARALLEL &&
  				dev.canRemotePP) {
  				$('ul#tables #restartParallel').click();
  				console.log('Got REMOTE_PARALLEL command');
  			} else if (type == LogState.REMOTE_P2DB &&
  					   dev.canRemotePP) {
  				$("ul#tables a[data-query='getlisthubdataParallel']").click();
  				var msg = '<!<!<!<! Got REMOTE_P2DB command'+" at "+getTime();
  				console.log(msg);
  				dev.log(msg)
  				dev.setExtraInfo('<p>'+msg+'</p>');
  			} else if (type == LogState.REMOTE_IMAGE_PROCESSING &&
  				dev.canRemotePP) {
  				var result = JSON.parse(data.data.data);
  				dev.imageBorderMode = result.mode;
  				dev.imageBorderOp = result.op;
  				dev.processOnlyNewImages = result.onlyNew;
  				dev.imageBorderFromLast = result.fromLast;
  				dev.maxSubDomainsAllowed = dev.numSubDomains = result.numSubDomains;
  				dev.unitsPerChunk = result.unitsPerChunk;
  				dev.numChunkPerSubDomain = result.numChunkPerSubDomain;
  				$('ul#tables #loadlimit').val(result.cpuLimit);
  				dev.getFixImageListingCount(RemoteMode.IMAGE_BORDER);
  				console.log('<!<!<!<! Got REMOTE_IMAGE_PROCESSING command');
  			}
  			else if ( type == LogState.END_PARALLEL) {
  				var result = JSON.parse(data.data.data);
				var mode = parseInt(result.mode);
				dev.log('END_PARALLEL - '+result.msg);
				dev.setExtraInfo('<p>END_PARALLEL - '+result.msg+', mode:'+mode+'</p>');
				dev.ajax({query: 'post-to-progress',
  					  		msg: {	message: 'END_PARALLEL received - '+result.msg,
  					  				everest: dev.myEverest,
  					  				mode: mode,
  					  				imageProcessAfterParse: dev.imageProcessAfterParse,
  					  				canRemotePP: dev.canRemotePP,
  					  				haveIP: haveIP,
  					  				haveMyOwn: haveMyOwn,
  					  				haveStartActiveP2Db: dev.haveStartActiveP2Db
  					  			},
							  mountain: dev.myMountain,
							  everest: dev.myEverest 
						});

	  			if ( mode == SpecialOp.SPECIAL_OP_NONE &&
	  				 dev.imageProcessAfterParse ) { // if another is doing some image processing, don't start this one...
	  				if (haveIP) {
	  					msg = "Got END_PARALLEL, but someone else:"+otherIPOwner+", is doing some image processing, not starting this one";
	  					dev.log(msg);
						dev.setExtraInfo('<p>'+msg+'</p>');
						dev.ajax({query: 'post-to-progress',
	  					  		  msg: {message: msg,
	  					  				everest: dev.myEverest,
	  					  			   },
								  mountain: dev.myMountain,
								  everest: dev.myEverest 
						});
	  					ahtb.alert(msg);
	  					return;
	  				}
	  				if (haveMyOwn ) {
	  					msg = "Got END_PARALLEL, but I'm busy with my own processing, not starting this one";
	  					dev.log(msg);
						dev.setExtraInfo('<p>'+msg+'</p>');
						dev.ajax({query: 'post-to-progress',
	  					  		  msg: {message: msg,
	  					  				everest: dev.myEverest,
	  					  			   },
								  mountain: dev.myMountain,
								  everest: dev.myEverest 
						});
	  					ahtb.alert(msg);
	  					return;
	  				}
	  				if (dev.haveStartActiveP2Db) {
	  					msg = "Got END_PARALLEL, but I'm parsing the feed right now, not starting this one";
	  					dev.log(msg);
						dev.setExtraInfo('<p>'+msg+'</p>');
						dev.ajax({query: 'post-to-progress',
	  					  		  msg: {message: msg,
	  					  				everest: dev.myEverest,
	  					  			   },
								  mountain: dev.myMountain,
								  everest: dev.myEverest 
						});
	  					ahtb.alert(msg);
	  					return;
	  				}
	  				msg = "END_PARALLEL command is calling getFixImageListingCount(IMAGE_BORDER_CRON_FROM_LAST)";
		  			console.log(msg);
		  			dev.log(msg);
		  			dev.setExtraInfo('<p>'+msg+'</p>');
		  			dev.doingRemote = RemoteMode.IMAGE_BORDER_CRON_FROM_LAST;
		  			dev.countIPRunCount = 0;
		  			dev.ipFromEndParallel = true;
		  			dev.ajax({query: 'post-to-progress',
  					  		msg: {	message: 'starting IP - '+msg,
  					  				everest: dev.myEverest,
  					  				doingRemote: dev.doingRemote,
  					  				imageProcessAfterParse: dev.imageProcessAfterParse,
  					  				canRemotePP: dev.canRemotePP,
  					  				haveIP: haveIP,
  					  				haveMyOwn: haveMyOwn,
  					  				haveStartActiveP2Db: dev.haveStartActiveP2Db
  					  			},
							  mountain: dev.myMountain,
							  everest: dev.myEverest 
						});
	   				dev.getFixImageListingCount(RemoteMode.IMAGE_BORDER_CRON_FROM_LAST);
	   			}
	 			console.log('<!<!<!<! Got END_PARALLEL command');
	 			dev.log(msg);
		  		dev.setExtraInfo('<p><!<!<!<! Got END_PARALLEL command</p>');
	  		}
  			return;
  		}
  		else if ( type == LogState.END_PARALLEL) {
  			dev.log(data.data.data);
  			dev.setExtraInfo('<p>'+data.data.data+'</p>');
  			if (//dev.doingParallel ||
  				dev.doingMonitor ) { //||
  				//dev.startingActiveP2Db) {
	  			dev.doingMonitor = 5;
	  			dev.stopTheMadness = 1;
	  		}
	  		console.log('<!<!<!<! Got END_PARALLEL command');
	  		return;
	  	}
	  	else if ( dev.startingActiveP2Db &&
	  			  type == LogState.FAILED_PARSE_TO_DB ) { // uh, oh..
	  		ahtb.open({ title: 'You Have Encountered a Fatal Error', 
						height: 150, 
						html: '<p>Parallel processing not started. Detail: '+msg+'</p>' 
			});
			dev.ajax({query: 'post-to-progress',
	  					  msg: 'Parallel processing not started. Detail: '+msg+' for everest:'+dev.myEverest+", retrying parse, count: "+dev.retryParseCount,
						  mountain: dev.myMountain,
						  everest: dev.myEverest });
			dev.removeActiveP2Db();
			dev.retryParse();
			return;
	  	}


  		// if (dev.doingMonitor == 5) // reading too much, ignore
  		// 	return;

  		if ( type == LogState.PAUSE_PARALLEL) {
  			var result = JSON.parse(data.data.data);
  			if (result.ppRunId != dev.ppRunId) {
  				console.log("Got PAUSE_PARALLEL with different ppRunId:"+result.ppRunId);
  				return;
  			}

  			dev.pausePP = true;
  			if (dev.doingMonitor) {
  				$('ul#tables #pauseParallel').val( dev.pausePP ? "Resume parallel process" : "Pause parallel list hub data processing");
  			}
  			ahtb.alert("Parallel processing has been paused.  No additional processes will be started and current ones will complete their tasks.",
		  				{height: 150});
  			console.log('<!<!<!<! Got PAUSE_PARALLEL command');
		 }
  		else if ( type == LogState.RESUME_PARALLEL) {
  			var result = JSON.parse(data.data.data);
  			if (result.ppRunId != dev.ppRunId) {
  				console.log("Got RESUME_PARALLEL with different ppRunId:"+result.ppRunId);
  				return;
  			}

  			dev.pausePP = false;
  			dev.initialImageChunksBeingSent = false;
  			dev.initialImageChunksCount = 0;
  			dev.lastInitImageChunkSentTime = 0;
  			if (dev.doingMonitor) {
  				$('ul#tables #pauseParallel').val( dev.pausePP ? "Resume parallel process" : "Pause parallel list hub data processing");
  			}
  			ahtb.alert("Parallel processing has been resumed.  Data stream will begin shortly.",
		  				{height: 150});
   			console.log('<!<!<!<! Got RESUME_PARALLEL command');
   			for(var i = 1; i <= dev.numSubDomains; i++)
   				dev.startSubdomain(i);
 		}
  		else if ( type == LogState.HALT_PARALLEL) {
  			var result = JSON.parse(data.data.data);
  			if (result.ppRunId != dev.ppRunId) {
  				console.log("Got HALT_PARALLEL with different ppRunId:"+result.ppRunId);
  				return;
  			}

  			dev.pausePP = false;
  			if (!dev.doingMonitor) // then the real deal
  				$('ul#tables #stopParallel').click(); // activate closure
  			else {
  				dev.doingMonitor = 6;
  				$('ul#tables #pauseParallel').hide();
				//$('ul#tables #haltParallel').hide();
  			}
  			console.log('<!<!<!<! Got HALT_PARALLEL command');
  			ahtb.alert("Parallel processing has been halted.  Currently active processes will complete before the session terminates.",
		  				{height: 150});
  		}
  		else if ( type == LogState.UPDATE ||
			 	  type == LogState.ERROR ||
			 	  type == LogState.FATAL)
  		{
			showIt = true;
		
			if (type == LogState.UPDATE &&
				(pos = msg.indexOf("load:")) != -1)
			{
				var load = parseFloat(msg.substring(pos+5));
				dev.tallyLoadCount++;
				dev.tallyLoad += typeof load == 'float' ? load : parseFloat(load);
				$('#load').val((dev.tallyLoad/(dev.tallyLoadCount ? dev.tallyLoadCount : 1)).toFixed(2));
				$('#currentLoad').html((dev.tallyLoad/(dev.tallyLoadCount ? dev.tallyLoadCount : 1)).toFixed(2));
			}
		}
		else if (type == LogState.RESULT ||
			     type == LogState.FATAL)
		{
  			//window.clearInterval(dev.myProgressTimer);
  		// 	window.clearInterval(myProgressTimer);
  		// 	if (resultsTimer == null) // must be null, but just checking..
				// dev.startResultsTimer(); // and for results

  			if (type == LogState.RESULT)
  			{
  				//dev.linesReported = data.data['id']; // record where we need to start looking for parallel results
  				dev.totalBlocks = parseInt(data.data['data']);
  				console.log("Parsing finished with "+dev.totalBlocks+" blocks.");
   				console.log("Going to clear out activeP2Db flag");
	  			dev.removeActiveP2Db();
 				if (dev.totalBlocks)
  				{
  					// clearInterval(myProgressTimer);
  					// myProgressTimer = null;
  					if (dev.onlyParseFeed) {
  						ahtb.alert("Got RESULT with "+dev.totalBlocks+" chunks.");
  					}
  					else if (!dev.doingParallel)
  					{
  						if (dev.runPPAfterResult) {
	  						dev.readyStartParsing = false;
	  						dev.doingSpecialParallelOperation = 0;
	  						var val = parseInt($("ul#tables #chunks option:selected").val());
							console.log ("cron - selected # of chunks: "+val);
							dev.unitsPerChunk = val;
							dev.restartSQL = false;
							dev.countCitiesAfterParse = true;
							dev.imageBorderOptimizeTableAfterwards = false;
							dev.imageProcessAfterParse = true;
							dev.resetMedianAndTiers = true;
							dev.listhubParseOptimizeTableAfterwards = true;
	  						dev.startParallel();
	  						dev.retryParseCount = 0;
	  						dev.runPPAfterResult = false; // reset
	  					}
	  					else {
	  						dev.ajax({query: 'post-to-progress',
	  								  msg: 'Got RESULT, but runPPAfterResult is false for everest:'+dev.myEverest,
	  								  mountain: dev.myMountain,
					  				  everest: dev.myEverest });
	  					}
  					}
  					else
  						console.log("Hey, doingParallel flag is true!!");
  					//dev.doingParallel = true;
  				}
	  			else {
	  				showIt = true;
	  				ahtb.alert("Failed to parse the listhub file, has 0 blocks.",
	  					       {height: 150});
	  				dev.ajax({query: 'post-to-progress',
		  					  msg: 'Failed to parse the listhub file, has 0 blocks for everest:'+dev.myEverest+", retrying parse, count: "+dev.retryParseCount,
							  mountain: dev.myMountain,
							  everest: dev.myEverest });
	  				dev.retryParse();
	  			}
	  			return;
	  		}
	  		else {
	  			ahtb.open({ title: 'You Have Encountered a Fatal Error', 
							height: 150, 
							html: '<p>Parallel processing not started. Detail: '+msg+'</p>' 
				});
				dev.ajax({query: 'post-to-progress',
		  					  msg: 'Parallel processing not started. Detail: '+msg+' for everest:'+dev.myEverest+", retrying parse, count: "+dev.retryParseCount,
							  mountain: dev.myMountain,
							  everest: dev.myEverest });
				dev.removeActiveP2Db();
				dev.retryParse();
			}
  		}
		else if (type == LogState.RESULT_PARALLEL)
		{
			if (data.status == 'OK') {
				var result = JSON.parse(data.data.data);
				var id = result.id; // subDomain index, 1-based.
				var startId = result.startid;
				var load = result.load;
				var added = result.added;
				var dups = result.duplicate;
				var rejected = result.rejected;
				var belowMinimumAcceptedPrice = result.belowMinimumAcceptedPrice;
				var recordedAt = result.endtime; // data.data.added;
				var host = result.host;

				if (result.ppRunId != dev.ppRunId)
					return;

				// var dateTime = recordedAt.split(" ");
				// var datePart = dateTime[0].split('-');
				// var timePart = dateTime[1].split(':');

				// //this makes it browser friendly
				// recordedAt = new Date(datePart[0], datePart[1], datePart[2], timePart[0], timePart[1], timePart[2]).getTime();
				//recordedAt = recordedAt.replace(" ", "T");
				//recordedAt = new Date(recordedAt).getTime(); // now in msecs

				id = typeof id == 'number' ? id : parseInt(id);
				startId = typeof startId == 'number' ? startId : parseInt(startId);
				msg = "RESULT_PARALLEL for id:"+id+", startId:"+startId+" from "+dev.linesReported;
				console.log(msg);
				// if (dev.doingMonitor)
				// 	dev.log(msg);
				//recordedAt = typeof recordedAt == 'number' ? recordedAt : parseInt(recordedAt);
				var timesplit = recordedAt.split(" ");
		  		recordedAt = parseFloat(timesplit[0])*1000 + parseFloat(timesplit[1])*1000;

				dev.tallyLoadCount++;
				dev.tallyLoad += typeof load == 'float' ? load : parseFloat(load);
				var amazonId = dev.getAmazonIndex(host);
				if (amazonId != -1) {
					dev.tallyLoadPingCount[amazonId]++;
	  				dev.tallyLoadPing[amazonId] += typeof load == 'float' ? load : parseFloat(load);
				}
				added = typeof added == 'number' ? added : parseInt(added);
				dev.tallyAdded += added;
				dev.tallyDups += typeof dups == 'number' ? dups : parseInt(dups);
				dev.tallyRejected += typeof rejected == 'number' ? rejected : parseInt(rejected);
				dev.tallyBelowMinimumAcceptedPrice += typeof belowMinimumAcceptedPrice == 'number' ? belowMinimumAcceptedPrice : parseInt(belowMinimumAcceptedPrice);
				dev.tallyTotalBlocks += result.good.length + result.bad.length + dev.tallyRejected;
				dev.sumAdded += typeof added == 'number' ? added : parseInt(added);
				dev.sumDups += typeof dups == 'number' ? dups : parseInt(dups);
				dev.sumRejected  += typeof rejected == 'number' ? rejected : parseInt(rejected);
				dev.sumBelowMinimumAcceptedPrice  += typeof belowMinimumAcceptedPrice == 'number' ? belowMinimumAcceptedPrice : parseInt(belowMinimumAcceptedPrice);
				$('#addedTotal').html(dev.sumAdded);
				$('#dupsTotal').html(dev.sumDups);
				$('#rejectedTotal').html(dev.sumRejected);
				$('#belowMinimumAcceptedPrice').html(dev.sumBelowMinimumAcceptedPrice);

				if (dev.myParallelResults.length < id)
		  			return;

		  		var x = 0;	  		
		  		for( ; x < dev.myParallelResults[id].total; x++) 
		  			if (dev.myParallelResults[id].chunkStates[x].startId <= startId &&
		  				dev.myParallelResults[id].chunkStates[x].upperLimit >= startId) 
		  				break;

				var j = 0;
		  		try {
		  			if (!dev.doingMonitor) {
				  		for(j in result.good)
				  			if (dev.parallelList[result.good[j]]) {
				  				if (dev.rangeCheck(id, dev.parallelList[result.good[j]][0]))
				  					dev.parallelList[result.good[j]][1] = BlockState.COMPLETE;
				  				else {
				  					var msg = "###### Block id(good): "+dev.parallelList[result.good[j]][0]+", does not fall in range of SubDomain: "+id+", which is "+dev.myParallelResults[id].startId+" to "+dev.myParallelResults[id].upperBlockLimit;
				  					console.log(msg);
				  				}
				  			}
				  			else
				  			{
				  				console.log("*** In success (good) block, jth("+j+") element is null, for subDomain: "+id);
				  				if (x != dev.myParallelResults[id].total &&
				  					dev.myParallelResults[id].chunkStates[x].state != ChunkState.ERROR)
				  					dev.myParallelResults[id].failed++; // assume it's gone
				  				dev.failedBlocks++;
				  			}
				  	}
			  	}
			  	catch(err) {
			  		console.log("********* Caught exception in success (good) block when j=="+j+", why:"+err.message);
			  	}
		  		if (!dev.doingMonitor && result.bad.length)
		  		{
		  			var err = "****** Found listing parse error on block(s): ";
			  		for(j in result.bad)
			  		{
			  			if (dev.parallelList[result.bad[j]]) {
			  				if (dev.rangeCheck(id, dev.parallelList[result.bad[j]][0]))
			  					dev.parallelList[result.bad[j]][1] = BlockState.ERROR;
			  				else {
			  					var msg = "###### Block id(bad): "+dev.parallelList[result.bad[j]][0]+", does not fall in range of SubDomain: "+id+", which is "+dev.myParallelResults[id].startId+" to "+dev.myParallelResults[id].upperBlockLimit;
			  					console.log(msg);
			  				}
			  			}
						else
						{
			  				console.log("*** In success (bad) block, jth("+j+") element is null, for subDomain: "+id);
			  				if (x != dev.myParallelResults[id].total &&
				  				dev.myParallelResults[id].chunkStates[x].state != ChunkState.ERROR)
			  				dev.myParallelResults[id].failed++; // assume it's gone
			  				dev.failedBlocks++;
			  			}
			  			if (j != '0')
			  				err += ', ';
			  			err += result.bad[j];
			  		}
			  		console.log(err);
			  	}

		  		// dev.chunksCompleted++;
		  		// dev.myParallelResults[id].completed++;
		  		// dev.activeChunks--;
		  		// dev.myParallelResults[id].active--;	
		  		//dev.myParallelResults[id].added += added;		
				//dev.myParallelResults[id].belowMinimumAcceptedPrice += belowMinimumAcceptedPrice;		
		  		x = 0;	  		
		  		for( ; x < dev.myParallelResults[id].total; x++) 
		  			if (dev.myParallelResults[id].chunkStates[x].startId <= startId &&
		  				dev.myParallelResults[id].chunkStates[x].upperLimit >= startId) {
		  				dev.myParallelResults[id].added += added;		
		  				dev.myParallelResults[id].belowMinimumAcceptedPrice += belowMinimumAcceptedPrice;	
		  				if (!dev.doingMonitor &&
		  					 dev.myParallelResults[id].chunkStates[x].state != ChunkState.COMPLETE) {
		  					switch(dev.myParallelResults[id].chunkStates[x].state) {
		  						case ChunkState.ERROR: // else took longer than expected, was marked as ERROR
		  							dev.myParallelResults[id].failed -= dev.unitsPerChunk; // undo fail count
				  					dev.setChunkStatus(id,
				  									   x,
				  									   2);
				  					console.log("*-*-*- SubDomain: "+id+" - nth chunk:"+x+", took longer than expected, was marked as ERROR");
		  							break;
		  						case ChunkState.PROCESSING: // normal case
		  							dev.chunksCompleted++;
				  					dev.activeChunks--;
				  					if (!dev.doingMonitor ||
				  						(dev.doingMonitor && dev.myParallelResults[id].active))
			  							dev.myParallelResults[id].active--;	
			  						dev.myParallelResults[id].completed++;
			  						break;

			  					case ChunkState.VIRGIN:
			  					case ChunkState.SENT:
			  					case ChunkState.ACKED: // then somehow, this slipped through too quick?
			  						console.log("*+*+*! SubDomain: "+id+" - nth chunk:"+x+", was "+chunkStateStr(dev.myParallelResults[id].chunkStates[x].state)+", set to COMPLETE");
			  						dev.chunksCompleted++;
			  						dev.myParallelResults[id].completed++;
			  						break;
		  					}
		  				}
		  				else if (!dev.doingMonitor)
		  					console.log("*+*+*+ SubDomain: "+id+" - nth chunk:"+x+", already was COMPLETE");
		  				else {
		  					dev.chunksCompleted++;
		  					dev.activeChunks--;
		  					if (dev.myParallelResults[id].active)
	  							dev.myParallelResults[id].active--;	
	  						dev.myParallelResults[id].completed++;
		  				}
		  				dev.myParallelResults[id].chunkStates[x].state = ChunkState.COMPLETE; // mark completed
		  				break;
		  			}
		  		console.log("- ---- Finished chunk results on line:"+dev.linesReported+", retrieval for SubDomain: "+id+" - nth chunk:"+x+", startId:"+result.startid+", good count:"+result.good.length+", bad:"+result.bad.length+", rejected:"+rejected+", chunksCompleted:"+dev.chunksCompleted+", DECREMENTED active now:"+dev.myParallelResults[id].active+", at "+curTime.toTimeString()+" from "+dev.linesReported);
		  	}
		  	else
		  		console.log("!!!!Retrieved LogState.RESULT_PARALLEL but status is not OK!!!!!")

		  	// $('#currentActive').html(dev.activeChunks);
		  	$('#currentActive').html(dev.activeChunksCount());
		  	
		  	var ended = (dev.myParallelResults[id].total == dev.myParallelResults[id].completed) || // cycled throught them all
		  				(dev.myParallelResults[id].active == 0 && dev.stopTheMadness);     // or we are aborting

		  	// only do it for real PP session
		  	if (!dev.doingMonitor && ended)
		  	{
				if (myTimerList[id]) {
					console.log("@@@@ clearing out timerId:"+myTimerList[id]+" for SubDomain:"+id)
			  		window.clearInterval(myTimerList[id]);
					myTimerList[id] = null;
				}
				if (dev.stopTheMadness)
					console.log(">>>> SubDomain:"+id+" has halted operations <<<<");
				else
					console.log("-----  SubDomain:"+id+" has completed operations -----");
		  	}

		  	// percentage and estimate...
		  	var percentage = (dev.chunksCompleted/dev.chunksExpected)*100.0;
		  	$('#percentage').val(percentage.toPrecision(3));
		  	curTime = dev.doingMonitor ? recordedAt : curTime.getTime();
		  	var diffTime = curTime - dev.startTaskTime;
		  	var estimate = (diffTime/percentage)*(100.0-percentage);
		  	var hms = '';
		  	if (estimate > 0)
		  		hms = dev.makeTimeLabel(estimate);
		  	else
		  		hms = 'Almost done...';
		  	$('#estimate').val(hms);
		  	updateChart = true;
		} // type = LogState.RESULT_PARALLEL
		else if ( type == LogState.BEGIN_PARALLEL ||
				  type == LogState.REPEATING_PARALLEL ||
				  type == LogState.DENY_PARALLEL ||
				  type == LogState.ALIEN_RUNID) {
			var result = JSON.parse(data.data.data);
			var id = result.id; // subDomain index, 1-based.
			var startId = result.startid;
			var load = result.load;

			if (result.ppRunId != dev.ppRunId)
				return;

			id = typeof id == 'number' ? id : parseInt(id);
			startId = typeof startId == 'number' ? startId : parseInt(startId);

			if (dev.myParallelResults.length < id)
		  		return;

			dev.tallyLoadCount++;
			dev.tallyLoad += typeof load == 'float' ? load : parseFloat(load);
			$('#load').val((dev.tallyLoad/(dev.tallyLoadCount ? dev.tallyLoadCount : 1)).toFixed(2));
			$('#currentLoad').html((dev.tallyLoad/(dev.tallyLoadCount ? dev.tallyLoadCount : 1)).toFixed(2));

			if (type == LogState.BEGIN_PARALLEL) {
				// mark chunk
				var x = 0;
				for( ;x < dev.myParallelResults[id].total; x++) 
		  			if (dev.myParallelResults[id].chunkStates[x].startId <= startId &&
		  				dev.myParallelResults[id].chunkStates[x].upperLimit >= startId) {
		  				if (dev.myParallelResults[id].chunkStates[x].state == ChunkState.PROCESSING ||
		  				 	dev.myParallelResults[id].chunkStates[x].state == ChunkState.ERROR ||
		  				 	dev.myParallelResults[id].chunkStates[x].state == ChunkState.COMPLETE) {
			  				console.log("!!!! SubDomain: "+id+" - on line:"+dev.linesReported+", nth chunk:"+x+", startId:"+startId+" has already been tagged as "+chunkStateStr(dev.myParallelResults[id].chunkStates[x].state));
			  				return;
			  			}
			  		}
		  			
				// mark chunk
				x = 0;
				for( ;x < dev.myParallelResults[id].total; x++) 
		  			if (dev.myParallelResults[id].chunkStates[x].startId <= startId &&
		  				dev.myParallelResults[id].chunkStates[x].upperLimit >= startId) {
		  				if (dev.myParallelResults[id].chunkStates[x].state < ChunkState.PROCESSING) { // can be VIRGIN, SENT, ACKED
		  					dev.myParallelResults[id].active++;
		  					dev.myParallelResults[id].sent++;
		  					dev.activeChunks++;
		  					dev.chunksSent++;
		  					dev.myParallelResults[id].chunkStates[x].state = ChunkState.PROCESSING; // mark processing
		  					// $('#currentActive').html(dev.activeChunks);
		  					$('#currentActive').html(dev.activeChunksCount());
		  				}
		  				break;
		  			}

		  		if (x < dev.myParallelResults[id].total) {
			  		dev.myParallelResults[id].chunkStates[x].badProcessing = 0; // reset
			  		dev.myParallelResults[id].chunkStates[x].sentAt = dev.myParallelResults[id].chunkStates[x].badTime = curTime.getTime(); // reset
					console.log("+++++ SubDomain: "+id+" - on line:"+dev.linesReported+", load:"+load+", nth chunk:"+x+", startId:"+startId+", INCREMENT active now:"+dev.myParallelResults[id].active+", at "+curTime.toTimeString()+" from "+dev.linesReported);
					updateChart = true;
				}
				else if (!dev.doingMonitor)
					console.log("**** SubDomain: "+id+" - on line:"+dev.linesReported+", load:"+load+", overshot nth chunk:"+x+", startId:"+startId+", INCREMENT active now:"+dev.myParallelResults[id].active+", at "+curTime.toTimeString()+" from "+dev.linesReported);					
			}
			else if (type == LogState.REPEATING_PARALLEL) {
				var status = result.status;

				status = typeof status == 'number' ? status : parseInt(status);
				var x = 0;
				for( ;x < dev.myParallelResults[id].total; x++) 
		  			if (dev.myParallelResults[id].chunkStates[x].startId <= startId &&
		  				dev.myParallelResults[id].chunkStates[x].upperLimit >= startId) {
		  				if (status == 1) {
		  					dev.myParallelResults[id].chunkStates[x].state = ChunkState.PROCESSING;
		  				}
		  				else if (status == 2) {
		  					dev.myParallelResults[id].chunkStates[x].state = ChunkState.COMPLETE;
		  				}
		  				break;
		  			}
		  				
		  		if (x < dev.myParallelResults[id].total)
					console.log("-_-_-_ Block id(repeating) - SubDomain: "+id+" - on line:"+dev.linesReported+", load:"+load+", nth chunk:"+x+" is now "+chunkStateStr(dev.myParallelResults[id].chunkStates[x].state)+", startId:"+startId+", active now:"+dev.myParallelResults[id].active+", at "+curTime.toTimeString());
				else if (!dev.doingMonitor)
					console.log("**** Block id(repeating) - SubDomain: "+id+" - on line:"+dev.linesReported+", load:"+load+", nth chunk:"+x+" is out of range, max is "+dev.myParallelResults[id].total);
			}
			else if (type == LogState.ALIEN_RUNID) {
				var ppRunId = result.ppRunId;
				console.log("@@@@@ Block id(alien) - SubDomain: "+id+" - on line:"+dev.linesReported+", got ppRunId:"+ppRunId+", current ppRunId:"+dev.ppRunId+", startId:"+startId+", at "+curTime.toTimeString());				
			}
			else { // DENY_PARALLEL
				dev.tallyDenied++;

				var x = 0;
				for( ;x < dev.myParallelResults[id].total; x++) 
		  			if (dev.myParallelResults[id].chunkStates[x].startId <= startId &&
		  				dev.myParallelResults[id].chunkStates[x].upperLimit >= startId) {
		  				if (dev.myParallelResults[id].chunkStates[x].state == ChunkState.PROCESSING ||
		  					dev.myParallelResults[id].chunkStates[x].state == ChunkState.ERROR ||
		  					dev.myParallelResults[id].chunkStates[x].state == ChunkState.COMPLETE) {
							console.log("**** Block id(deny) - SubDomain: "+id+" - on line:"+dev.linesReported+", load:"+load+", nth chunk:"+x+" is already "+chunkStateStr(dev.myParallelResults[id].chunkStates[x].state)+", startId:"+startId+", active now:"+dev.myParallelResults[id].active+", this denial is ignored, at "+curTime.toTimeString());
							return;
						}
		  			}

				// mark blocks as VIRGIN
				for(var j = startId; j && dev.doingMonitor == 0 &&  j < (startId+dev.unitsPerChunk - 1); j++)
					if (dev.rangeCheck(id, dev.parallelList[j][0]))
						dev.parallelList[j][1] = BlockState.VIRGIN; // reset
					else {
						var msg = "###### Block id(deny): "+dev.parallelList[j][0]+", does not fall in range of SubDomain: "+id+", which is "+dev.myParallelResults[id].startId+" to "+dev.myParallelResults[id].upperBlockLimit;
			  			console.log(msg);
					}
				// mark chunk
				x = 0;
				for( ;x < dev.myParallelResults[id].total; x++) 
		  			if (dev.myParallelResults[id].chunkStates[x].startId <= startId &&
		  				dev.myParallelResults[id].chunkStates[x].upperLimit >= startId) {
		  				dev.myParallelResults[id].chunkStates[x].state = ChunkState.VIRGIN; // mark virgin
		  				break;
		  			}
		  		if (x < dev.myParallelResults[id].total)
					console.log("..... SubDomain: "+id+" - on line:"+dev.linesReported+", load:"+load+", startId:"+startId+", denied due to cpu limit active now:"+dev.myParallelResults[id].active+", at "+curTime.toTimeString());
				else if (!dev.doingMonitor)			
					console.log("**** SubDomain: "+id+" - on line:"+dev.linesReported+", load:"+load+", startId:"+startId+", denied due to cpu limit, but "+x+" overshot total range, active now:"+dev.myParallelResults[id].active+", at "+curTime.toTimeString());
			}
		}

		$('#load').val((dev.tallyLoad/(dev.tallyLoadCount ? dev.tallyLoadCount : 1)).toFixed(2));
		$('#currentLoad').html((dev.tallyLoad/(dev.tallyLoadCount ? dev.tallyLoadCount : 1)).toFixed(2));
  		if (dev.doingMonitor != 1 && showIt)
  		{
	  		console.log('Type: '+type+', Data: '+msg+', line:'+dev.linesReported);
	  		$('#progress').text( msg );
	  	}

	  	if ( (dev.doingMonitor % 2) == 0  && // 0 = regular PP, 1 = monitor is just starting up and catching up, 2 = monitor is ready to display, 3 = begin replay, 4 = replay slowdown replay, 5 = end replay, 6 = halt command issued
	  		  updateChart) {
	  		dev.updateChart(recordedAt);
	  		dev.updateTable();
  		}
	}

	this.updateChart = function(recordedAt) {
		var curTime = Date.now();
  		var diffTime = 0;
  		if (!dev.doingMonitor)
  			diffTime = curTime - dev.chartLapsed;
  		else
  			diffTime = recordedAt - dev.chartLastUpdatedAt;
  		if (diffTime >= dev.chartInterval)
  		{
  			dev.chartLastUpdatedAt = !dev.doingMonitor ? curTime : recordedAt;
  			var numChunks = 1;
  			dev.chartXAxis++;
  			dev.chartLapsed = curTime; // reset to new starting point
  			var newLabel = dev.makeTimeLabel(!dev.doingMonitor ? curTime - dev.startTaskTime : recordedAt - dev.startTaskTime, 0, false);
  			dev.tallyLoadCount = dev.tallyLoadCount ? dev.tallyLoadCount : 1; // prevent divide by zero
  			var loadAvg = dev.tallyLoad/dev.tallyLoadCount;
  			if (dev.chartXAxis <= 5 &&
  				dev.tallyNetworkConnection > (dev.unitsPerChunk*1.1))
  				dev.tallyNetworkConnection = (dev.unitsPerChunk*1.1); // cap it. since network error just seems to occur a lot at the beginning.

  			dev.tallyTotalBlocks = dev.tallyTotalBlocks ? dev.tallyTotalBlocks : 1; // prevent divide by zero
  			var normalizer = dev.maxParallelism*numChunks; 
  			var dataPoints =   [//dev.activeChunks, 
  								dev.activeChunksCount(),
  								loadAvg, 
  								dev.tallyDenied, 
  								dev.tallyNetworkConnection,
  								(dev.tallyAdded/dev.tallyTotalBlocks) * normalizer,
  								(dev.tallyDups/dev.tallyTotalBlocks) * normalizer,
  								(dev.tallyRejected/dev.tallyTotalBlocks) * normalizer,
  								(dev.tallyBelowMinimumAcceptedPrice/dev.tallyTotalBlocks) * normalizer ];
  			if (dev.chartXAxis > MAX_POINTS)
  				dev.chart.removeData(); // shift it over...
  			dev.chart.addData(dataPoints, newLabel);
  			// reset
  			dev.tallyDenied = dev.tallyAdded = dev.tallyDups = dev.tallyRejected = dev.tallyBelowMinimumAcceptedPrice = 0;
  			dev.tallyLoadCount = 0;
  			dev.tallyLoad = 0.0;
  			dev.tallyNetworkConnection = 0;
  			dev.tallyTotalBlocks = 0;
  			if (dev.tallyLoadPingCount.length) {
  				for(var i in dev.tallyLoadPingCount) {
  					dev.tallyLoadPingCount[i] = 0;
  					dev.tallyLoadPing[i] = 0;
  				}
  			}
  			if (dev.chartDataList.length > 1000)
  				dev.chartDataList.shift();
  			dev.chartDataList.push(dataPoints);
  		}
	}

	this.makeTimeLabel = function(timeIsNow, precision, haveLabel) {
		if (typeof precision === 'undefined') { precision = 2; }
		if (typeof haveLabel === 'undefined') { haveLabel = true; }
		var hours = Math.floor(timeIsNow/(3600*1000));
	  	timeIsNow = timeIsNow % (3600*1000);
	  	var min = Math.floor(timeIsNow / 60000);
	  	timeIsNow = timeIsNow % 60000;
	  	var secs = timeIsNow/1000;
	  	var hms = '';
	  	if (haveLabel)
	  		hms = hours+"hr "+min+"min "+secs.toPrecision(precision)+"sec";
	  	else
	  		hms = hours+":"+min+":"+secs.toFixed(0);
	  	return hms;
	}
	
	this.retryOneChunk = function($startId)
	{
		dev.doChunk($startId);
	}
		
	this.doChunk = function($startId)
	{
		if (dev.gotMyTreat == false)
		{
			setTimeout(function() {
				dev.doChunk($startId);
			}, 1000);
			return;
		}

		if (dev.gotTotalBlocks == false) {
			setTimeout(function() {
				dev.doChunk($startId);
			}, 1000);
			return;
		}

		if (dev.totalBlocks == 0) {
			ahtb.alert("No blocks to process");
			return;
		}

		if (dev.parallelList.length == 0 ||
			dev.parallelList.length != dev.totalBlocks)
		{
			if (dev.parallelList.length)
  	 			dev.parallelList.splice(0, dev.parallelList.length); // clear out

			for(var i = 1; i <= dev.totalBlocks; i++)
	  		{
	  			unit = [i, BlockState.VIRGIN];
	  			dev.parallelList[i] = unit;
			}
		}

		if ($startId > dev.totalBlocks)
		{
			ahtb.alert("Cannot start a chunk above number available, which is "+dev.totalBlocks,
						{height: 150});
			return;
		}

		var i = $startId;
		var count = 0;
		var newChunk = [];
		for(; count < dev.unitsPerChunk && i <= dev.parallelList.length; i++, count++)
		{
			dev.parallelList[i][1] = BlockState.PROCESSING;
			newChunk.push(dev.parallelList[i][0]);
		}

		$('#progress').text( "Started process with starting block of "+newChunk[0] );
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'listhubchunk',
					mountain: dev.myMountain,
					everest: dev.myEverest,
					list: newChunk,
					firstone: 'true'},
			dataType: 'JSON',
			type: 'POST',
			timeout: 36000000,
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.alert('You Have Encountered an Error<br/>Status: '+($status ? $status : "No status.")+"-  Detail: "+msg,
							{height: 150});
		  		console.log("Failed chunk - in error frame");
			},
		  	success: function(data){
			  	if (data == null || data.status == null ||
			  		typeof data == 'undefined' || typeof data.status == 'undefined') 
			  	{
			  		ahtb.alert('You Have Encountered an Error<br/>Unknown cause.',
			  					{height: 150});
			  		console.log("Failed chunk - in success frame.");
			  	}
			  	else {
			  		result = data.data; // should be 2D array
			  		var j = 0;
			  		if (result['good'].length) {
			  			console.log("Got back "+result['good'].length+" good parses");
				  		for(j in result['good'])
				  			dev.parallelList[result['good'][j]][1] = BlockState.COMPLETE;
				  	}
			  		if (result['bad'].length)
			  		{
			  			var err = "Found listing parse error on block(s): ";
				  		for(j in result['bad'])
				  		{
				  			dev.parallelList[result['bad'][j]][1] = BlockState.ERROR;
				  			if (j != '0')
				  				err += ', ';
				  			err += result['bad'][j];
				  		}
				  		console.log(err);
				  	}
			  		var msg = "Finished block - startId:"+result['startId']+", good count:"+result['good'].length+", bad:"+result['bad'].length;	
			  		console.log(msg);				
				  	ahtb.open({ title: 'Completed processing one chunk', height: 150, html: '<p>'+msg+'</p>' });
			  	}
			}
		}); // ajax
	}

	this.recoverChunk = function()
	{
		// if ( dev.doingRemote != RemoteMode.FULL_PARSE_CRON)
		// 	return;

		var curTime = Date.now();

		if (dev.recoveringChunk) {
			if ( (curTime - dev.startRecoveringChunk) > (dev.unitsPerChunk * 60000) ) { // shit
				var msg = "last chunk recovery did not return!";
				ahtb.alert(msg);
				dev.setFirstErrorChunkToFatal();
			}
			return;
		}

		dev.recoveringChunk = true;
		dev.startRecoveringChunk = curTime;
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'recover-one-chunk',
					specialOp: dev.doingSpecialParallelOperation,
					mountain: dev.myMountain,
					everest: dev.myEverest,
					chunkSize: dev.unitsPerChunk},
			dataType: 'JSON',
			type: 'POST',
			timeout: 36000000,
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.alert('You Have Encountered an Error<br/>Status: '+($status ? $status : "No status.")+"-  Detail: "+msg,
							{height: 150});
		  		console.log("Failed recoverChunk - in error frame");
		  		dev.recoveringChunk = false;
			},
		  	success: function(data){
		  		dev.recoveringChunk = false;
			  	if (data == null || data.status == null ||
			  		typeof data == 'undefined' || typeof data.status == 'undefined') 
			  	{
			  		ahtb.alert('You Have Encountered an Error<br/>Unknown cause.',
			  					{height: 150});
			  		console.log("Failed recoverChunk - in success frame.");
			  	}
			  	else if (data.status == 'OK') {
			  		result = data.data; // should be 2D array
			  		var j = 0;
			  		if (result['good'].length) {
			  			console.log("Got back "+result['good'].length+" good parses");
				  		for(j in result['good'])
				  			dev.parallelList[result['good'][j]][1] = BlockState.COMPLETE;
				  	}
			  		if (result['bad'].length)
			  		{
			  			var err = "Found listing parse error on block(s): ";
				  		for(j in result['bad'])
				  		{
				  			dev.parallelList[result['bad'][j]][1] = BlockState.ERROR;
				  			if (j != '0')
				  				err += ', ';
				  			err += result['bad'][j];
				  		}
				  		console.log(err);
				  	}
			  		var msg = "recoverChunk block - subDomain:"+result['subDomain']+", startId:"+result['startId']+", good count:"+result['good'].length+", bad:"+result['bad'].length;	
			  		dev.log(msg);				
			  	}
			  	else if (data.data == 'Looks all done') {
			  		dev.noChunksToRecover = true;
			  	}
			}
		}); // ajax
	}

	this.setFirstErrorChunkToFatal = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'set-first-error-chunk-to-fatal',
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during setFirstErrorChunkToFatal: "+msg);
				ahtb.alert("Error during setFirstErrorChunkToFatal: "+msg, {height: 150});
				dev.recoveringChunk = false;			
			},					
		  	success: function(data){
		  		dev.recoveringChunk = false;
		  		if (data == null || typeof data == 'undefined' || 
			  		data.status == null || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL for setFirstErrorChunkToFatal</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.alert( typeof data.data == 'undefined' ? "Unknown error during setFirstErrorChunkToFatal" : data.data);
			  	}
			  	else {
			  		var result = parseInt(data.data);
			  		console.log("setFirstErrorChunkToFatal - got back:"+result);
			  	}
			}
		});
	}

	this.retryCheckActivePPStatus = function()
	{
		dev.checkActivePPStatus();
	}

	this.checkActivePPStatus = function()
	{
		if (!dev.removedActivePP)
		{
			setTimeout(function() { dev.checkActivePPStatus() }, 200);
			return;
		}

		dev.checkActivePP();
		dev.releaseMountain();
	}

	this.retryReleaseMountain = function()
	{
		dev.releaseMountain();
	}

	this.releaseMountain = function()
	{
		if (!dev.gotMyhaveActiveBE)
		{
			setTimeout(function() { dev.releaseMountain() }, 200);
			return;
		}	
		if (dev.doingRemote == RemoteMode.FULL_PARSE_CRON && // if 2, then was a cron job, so stop the instances
			dev.amazon)
			dev.tryAjax({
				query: 'stop-aws',
				mountain: dev.myMountain,
				everest: dev.myEverest
			});
		//console.log("releaseMountain calling to free treats");
		// if (!dev.canRemotePP)
		// 	dev.clearMyTreat();
		dev.doingParallel = false;
		// dev.stopTheMadness = false;
		dev.doingMonitor = 0;

		var doingSpecialParallelOperation = dev.doingSpecialParallelOperation; // make copy since getFixImageListingCount() will change it.
		dev.doingSpecialParallelOperation = 0;

		var msg = "releaseMountain - doingSpecialParallelOperation:"+doingSpecialParallelOperation+", doingRemote:"+dev.doingRemote+", countCitiesAfterParse:"+dev.countCitiesAfterParse+", imageBorderOptimizeTableAfterwards:"+dev.imageBorderOptimizeTableAfterwards+", backupAfterFullParse:"+backupAfterFullParse;
		console.log(msg);
		dev.log(msg);
		dev.setExtraInfo('<p>'+msg+'</p>');

		if (!dev.stopTheMadness) {
			if (!doingSpecialParallelOperation) {
				if (dev.doingRemote == RemoteMode.FULL_PARSE_CRON || // cron job
					dev.countCitiesAfterParse) { // or chose to do 
					dev.ajax({
						query: 'count-cities',
						mountain: dev.myMountain,
						everest: dev.myEverest });
					// HOLD OFF ImageProcessing until concludePP() is all done.
					// This is because the parser still needs to clean out dead listings so that we
					// can get an accurate active listing count and tables optimized.
					// dev.fixImageBorders(ActiveState.ACTIVE, 1);
					// dev.getFixImageListingCount(RemoteMode.IMAGE_BORDER_CRON_ALL);
				}
			}
			else if (dev.imageBorderOptimizeTableAfterwards) {
				if (backupAfterFullParse) {
					dev.ajax({
						query: 'do-backup',
						mountain: dev.myMountain,
						everest: dev.myEverest });
					msg = "Called do-backup after image processing";
				}
				else {
					dev.ajax({
						query: 'optimize-table',
						mountain: dev.myMountain,
						everest: dev.myEverest });
					msg = "Called optimize-table after image processing";
				}
				console.log(msg);
				dev.log(msg);
				dev.setExtraInfo('<p>'+msg+'</p>');
			}
		}
		
		dev.doingRemote = RemoteMode.NONE;
		dev.stopTheMadness = false;
		
		if (!doingSpecialParallelOperation)
			msg = "parser process exiting";
		else
			msg = doingSpecialParallelOperation < 3 ? "import listing exiting" : "image processing exiting";

		console.log(msg);
		dev.log(msg);
		dev.setExtraInfo('<p>'+msg+'</p>');

		if (dev.reloadPage &&
			dev.canRemotePP)
			location.reload(); 
		else
			dev.reloadPage = false;

		// if this ipFromEndParallel is set, then we want to rerun IP to clean up any missed blocks
		if (dev.ipFromEndParallel) {
			if (dev.countIPRunCount == 0) {
				window.setTimeout(function() {
					dev.countIPRunCount++;
					dev.doingRemote = RemoteMode.IMAGE_BORDER_CRON_FROM_LAST;
					dev.doingSpecialParallelOperation = SpecialOp.IMAGE_BORDER_PARALLEL;
					dev.imageBorderOptimizeTableAfterwards = false;
					dev.preRestartIP();	
					dev.haveActiveBE = 'working';
					dev.setButtons();	
					dev.ajax({query: 'post-to-progress',
  					  		  msg: { message: 'rerun IP - after END_PARALLEL received from daily parsing to clean up any missed blocks',
  					  				everest: dev.myEverest,
  					  				doingRemote: dev.doingRemote,
  					  				countIPRunCount: dev.countIPRunCount
  					  			   },
							  mountain: dev.myMountain,
							  everest: dev.myEverest 
						});
					// dev.getFixImageListingCount(RemoteMode.IMAGE_BORDER_CRON_FROM_LAST);
				}, 1000 * 60 * 20);
			}
			else {
				dev.countIPRunCount = 0;
		  		dev.ipFromEndParallel = false;
			}
		}
	}


	this.sendTrickOrTreat = function() {
		if (dev.gotMyTreat)
			return;

		dev.gotMyTreat = false;
		dev.clearedMyTreat = false;
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'trickortreat',
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error', 
							height: 150, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK')
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  	else {
		  		console.log(data.data);
		  		dev.gotMyTreat = true;
		  	}
		  }
		});
	}

	this.clearMyTreat = function() {
		dev.gotMyTreat = false;
		dev.clearedMyTreat = false;
		console.log("making call to clear treat...");
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'clearMyMountain',
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error during clearMyTreat', 
							height: 150, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},					
		  	success: function(data){
			  	if (data == null || data.status == null ||
			  		typeof data == 'undefined' || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
			  	else if (data.status != 'OK')
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
			  	else {
			  		dev.clearedMyTreat = true;
			  		console.log("clearMyTreat: "+data.data);
		  		}
		  }
		});
	}

	this.getLastProgressCount = function(reset) {
		dev.gettingMyProgressStartPoint++;
		dev.gotMyProgressStartPoint = false;
		dev.startGetLastProgressCount = Date.now();
		// dev.ajaxCount++;
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'getlastprogresscount',
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			cache: false,
			error: function($xhr, $status, $error){
				// dev.ajaxCount--;
				dev.gettingMyProgressStartPoint--;
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during getLastProgressCount: "+msg);
			},					
		  	success: function(data){
		  	dev.gettingMyProgressStartPoint--;
		  	// dev.ajaxCount--;
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK')
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  	else {
		  		if (data.data != null && data.data != undefined)
		  			dev.myProgressStartPoint = parseInt(data.data);
		  		else
		  			dev.myProgressStartPoint = 0;
		  		dev.gotMyProgressStartPoint = true;
		  		if ( typeof reset != 'undefined' && reset ) dev.linesReported = dev.myProgressStartPoint;
		  		//console.log("Results registration will start at db row:"+(dev.myProgressStartPoint+1)+", gettingMyProgressStartPoint:"+dev.gettingMyProgressStartPoint);
		  	}
		  }
		});

		dev.monitorGetLastProgressCount();
	}

	this.monitorGetLastProgressCount = function() {
		if (!dev.gotMyProgressStartPoint) {
			var curTime = Date.now();
			if ( (curTime - dev.startGetLastProgressCount) > 5000)
				dev.getLastProgressCount();
			setTimeout(function() {
				dev.monitorGetLastProgressCount();
			}, 500);
		}
	}

	this.checkActivePP = function() {
		if (typeof dev.checkActivePP.counter == 'undefined')
		 	dev.checkActivePP.counter = 0;
		 else
		 	dev.checkActivePP.counter++;

		dev.gotMyhaveActiveBE = false;
		dev.haveActiveBE = 'working';
		// console.log("checkActivePP active");
		dev.startGetMyHaveActiveBETime = Date.now();
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'haveactiveparallelprocessing',
					// doingIP: dev.doIP(),
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during checkActivePP: "+msg);
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK')
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  	else {
		  		dev.haveActiveBE = data.data;
		  		dev.gotMyhaveActiveBE = true;
		  		var state = "Current status: "+(dev.haveActiveBE == 'false' ? "inactive" : "haveMyOwn:"+haveMyOwn+", haveIP:"+haveIP+", haveIPCustom:"+haveIPCustom+", havePP:"+havePP+", haveP2Db:"+haveP2Db);
		  		setBEStates();
		  		if ( (dev.checkActivePP.counter % 60) == 0 ) {
		  			console.log(state);
		  			$('#progress').text(state);
		  		}
		  		if (dev.haveActiveBE != 'false')
		  			for(var i in dev.haveActiveBE) 
				  		switch(dev.haveActiveBE[i]['opt']) {
				  			case 'activePP':
				  				if (dev.haveActiveBE[i]['value'] == dev.myMountain) {// hey it's mine!
						  			if (dev.startingActivePP != true )
						  				console.log("dev.startingActivePP is not true, setting it to true!");
						  			dev.startingActivePP = true;
						  		}
				  				break;
				  			case 'activeParseToDb':
				  				if (dev.haveActiveBE[i]['value'] == dev.myMountain) {// hey it's mine!
						  			if (dev.startingActiveP2Db != true )
						  				console.log("dev.startingActiveP2Db is not true, setting it to true!");
						  			dev.startingActiveP2Db = true;
						  			dev.runPPAfterResult = true;
						  			dev.imageProcessAfterParse = true;
						  		}
				  				break;
				  			case 'activeIP':
				  			case 'activeIPCustom':
				  				if (dev.haveActiveBE[i]['value'] == dev.myMountain) {// hey it's mine!
						  			if (dev.startingActiveIP != true )
						  				console.log("dev.startingActivePP is not true, setting it to true!");
						  			dev.startingActiveIP = true;
						  		}
				  				break;
				  		}
		  		
		  		dev.setButtons();
		  	}
		  }
		});
		dev.monitorCheckActivePP();
	}

	this.monitorCheckActivePP = function() {
		if (!dev.gotMyhaveActiveBE) {
			var curTime = Date.now();
			if ( (curTime - dev.startGetMyHaveActiveBETime) > 5000)
				dev.checkActivePP();
			setTimeout(function() {
				dev.monitorCheckActivePP();
			}, 500);
		}
	}

	this.removeActivePP = function() {
		dev.removedActivePP = false;
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'removeactiveparallelprocessing',
					specialOp: dev.doingSpecialParallelOperation,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error', 
							height: 150, 
							html: '<p>removeActivePP - Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK')
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  	else {
		  		dev.removedActivePP = true;
		  		dev.haveActiveBE = 'false';
			  	console.log("removeActivePP: "+data.data+", haveActiveBE:"+dev.haveActiveBE);
		  	}
		  }
		});

		// conclude PP, write marker to db, so if replaying, know when to stop reading the db
		var msg = "removeActivePP - doingSpecialParallelOperation:"+dev.doingSpecialParallelOperation;
		console.log(msg);
		dev.log(msg);
		dev.setExtraInfo('<p>'+msg+'</p>');

		if (!dev.doingSpecialParallelOperation &&
			!dev.stopTheMadness)
			$.ajax({
				url: ah_local.tp+'/_admin/ajax_developer.php',
				data: { query: 'concludepp',
						mode: dev.stopTheMadness ? SpecialOp.USER_ABORTED_PP : SpecialOp.SPECIAL_OP_NONE,
						optimizeTable: dev.listhubParseOptimizeTableAfterwards,
						ppRunId: dev.ppRunId,
						mountain: dev.myMountain,
						everest: dev.myEverest },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					ahtb.open({ title: 'You Have Encountered an Error', 
								height: 150, 
								html: '<p>removeActivePP::concludePP - Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
							});
				},					
			  	success: function(data){
			  	if (data == null || data.status == null ||
			  		typeof data == 'undefined' || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
			  	else if (data.status != 'OK')
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
			  	else {
				  	console.log("concludePP: "+data.data);
			  	}
			  }
			});
	}

	this.togglePPState = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: (dev.pausePP ? 'pausepp' : 'resumepp'),
					ppRunId: dev.ppRunId,
					specialOp: dev.doingSpecialParallelOperation,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error during togglePPState',
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK')
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  	else {
			  	console.log("togglePPState: "+data.data);
		  	}
		  }
		});
	}

	this.haltPPState = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'haltpp',
					ppRunId: dev.ppRunId,
					specialOp: dev.doingSpecialParallelOperation,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error during haltPPState',
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK')
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  	else {
			  	console.log("haltPPState: "+data.data);
		  	}
		  }
		});
	}

	this.registerActivePP = function() {
		dev.startingActivePP = false;
		dev.startingActiveIP = false;
		dev.haveStartActivePP = false;
		console.log("calling startactiveparallelprocessing for specialOp:"+dev.doingSpecialParallelOperation);
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'startactiveparallelprocessing',
					specialOp: dev.doingSpecialParallelOperation,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error during registerActivePP', 
							height: 150, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') {
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  		return;
		  	}
		  	else if (data.status != 'OK') {
		  		ahtb.alert("Failed to startactiveparallelprocessing");
		  	}
		  	else {
		  		dev.doIP() ? dev.startingActiveIP = true : dev.startingActivePP = true;
		  	}
		  	console.log(data.data);
		  	dev.haveStartActivePP = true;
		  	dev.getLastProgressCount();
		  }
		});
	}

	this.registerActiveP2Db = function() {
		dev.startingActiveP2Db = false;
		dev.haveStartActiveP2Db = false;
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'startactiveparse2db',
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error during registerActiveP2Db', 
							height: 150, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},					
		  	success: function(data){
		  	dev.haveStartActiveP2Db = true;
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') {
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  		return;
		  	}
		  	else if (data.status != 'OK') {
		  		ahtb.alert("Failed to startactiveparse2db");
		  	}
		  	else {
		  		dev.startingActiveP2Db = true;
		  	}
		  	console.log(data.data);
		  	dev.getLastProgressCount();
		  }
		});
	}

	this.removeActiveP2Db = function() {
		dev.removedActiveP2Db = false;
		dev.startingActiveP2Db = false;
		dev.removeActiveP2DbStart = Date.now();
		dev.doingRemote = RemoteMode.NONE;
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'removeactiveparse2db',
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error during removeActiveP2Db', 
							height: 150, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK')
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  	else {
		  		dev.removedActiveP2Db = true;
		  		dev.haveStartActiveP2Db = false;
		  		dev.haveActiveBE = 'false';
			  	console.log("removeActiveP2Db: "+data.data+", haveActiveBE:"+dev.haveActiveBE);
		  	}
		  }
		});
	}

	this.forcePP = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'forceremoveactiveparallelprocessing',
					specialOp: dev.doingSpecialParallelOperation,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error during forcePP', 
							height: 150, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK') {
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed force clean for parallel processing</p>' });
		  	}
		  	else {
		  		console.log("Forceful removal of parallel processing flag completed");
		  		dev.checkActivePP();
		  		dev.getLastProgressCount(true);
		  		dev.startParallizing();
		  	}
		  }
		});
	}

	this.forceP2Db = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'forceremoveparse2db',
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error during forceP2Db', 
							height: 150, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK') {
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed force clean for P2Db</p>' });
		  	}
		  	else {
		  		console.log("Forceful removal of P2Db flag completed");
		  		dev.checkActivePP();
		  		dev.getLastProgressCount(true);
		  		dev.startParallizing();
		  	}
		  }
		});
	}

	this.haveAllAmazonEc2Pings = function() {
		if (!dev.amazon)
			return true;

		var gotPing = 0;
		for(var i in dev.tallyLoadPingCount) {
			if (dev.tallyLoadPingCount[i])
				gotPing++;
		}
		return gotPing == dev.startEc2Count;
	}

	this.writeLogHeaderForParallel = function(retry) {
		dev.wrotelogHeaders = false;
		if (!dev.haveAllAmazonEc2Pings()) {
			dev.ping();	
			window.setTimeout(function() {
				dev.writeLogHeaderForParallel();
			}, 1000);
			return;
		}
		if ( typeof retry == 'undefined')
			dev.ppRunId = Math.round(Math.random()*1000000).toString();
		var msg = 'Writing Log Header with runId:'+dev.ppRunId+', remote:'+dev.doingRemote+', specialOp:'+dev.doingSpecialParallelOperation+", restartSQL:"+dev.restartSQL+", countCitiesAfterParse:"+dev.countCitiesAfterParse+", resetMedianAndTiers:"+dev.resetMedianAndTiers;
		dev.log(msg);
		dev.setExtraInfo('<p>'+msg+'</p>');

		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'writelogheaderforparallel',
					mountain: dev.myMountain,
					everest: dev.myEverest,
					subdomains: dev.numSubDomains,
					chunks: dev.numChunkPerSubDomain,
					unitsperchunk: dev.unitsPerChunk,
					saverejected: dev.saveRejected,
					cpulimit: $('ul#tables #loadlimit').val(),
					minTags: dev.minTags,
					minPrice: dev.minPrice,
					totalBlocks: dev.totalBlocks,
					ppRunId: dev.ppRunId,
					specialOp: dev.doingSpecialParallelOperation,
					restartSQL: dev.restartSQL,
					resetMedianAndTiers: dev.resetMedianAndTiers },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error during writeLogHeaderForParallel, process aborted', 
							height: 180, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
				// try again
				// if (!dev.wrotelogHeaders) // may have completed.
				// 	dev.writeLogHeaderForParallel(true);
				dev.errorWriteLogHeaders = true;
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK') {
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed to prep log header for parallel processing</p>' });
		  	}
		  	else {
		  		if (typeof data.data == 'object') { // we have Amazon data
		  			dev.amazon = JSON.parse(data.data.value);
		  			var ec2Count = 0;
		  			var running = 0;
					var stopped = 0;
		  			for(var data in dev.amazon) {
		  				if (typeof dev.amazon[data] == 'object' &&
		  					dev.amazon[data]['PublicIP'] != null && dev.amazon[data]['PublicIP'] != 'undefined') {
		  					ec2Count++;
	  						if (dev.amazon[data]['State'] == 'running')
	  							running++;
	  						else
	  							stopped++;
		  					if (ec2Count > dev.tallyLoadPing.length) {
			  					dev.tallyLoadPing[dev.tallyLoadPing.length] = 0;
			  					dev.tallyLoadPingCount[dev.tallyLoadPingCount.length] = 0;
			  				}
		  				}
		  			}
		  			dev.maxSubDomainsAllowed = dev.numSubDomains = running;
		  			$('ul#tables #subdomain option[value="'+dev.numSubDomains+'"]').prop('selected', true);
		  			$('ul#tables #maxsubdomain option[value="'+dev.maxSubDomainsAllowed+'"]').prop('selected', true);
		  		}
		  		console.log("Prepped log headers for parallel processing");
		  		dev.wrotelogHeaders = true;
		  	}
		  }
		});
	}

	this.beginMonitorParallel = function() {
		dev.gotLastStartEntry = false;
		dev.startGetLastStartEntryTime = Date.now();
		var h = '<div id="choose">'+
					'<span>Choose what kink of parellel prcessing you want to monitor</span><br/>'+
					'<input type="radio" name="process" id="listhub">&nbsp;Feed Parsing</input>' +
					'<input type="radio" name="process" id="image">&nbsp;Image Processing</input>' +
					'<input type="radio" name="process" id="custom">&nbsp;Custom Image Processing</input>' +
				'</div>';
		ahtb.open({	html: h,
				   	width: 500,
					height: 200,
					buttons: [
						{text:"OK", action: function() {
							var doingIP = $('input#image').prop('checked');
							if (doingIP)
								dev.doingSpecialParallelOperation = SpecialOp.IMAGE_BORDER_PARALLEL;
							else if ( $('input#custom').prop('checked') )
								dev.doingSpecialParallelOperation = SpecialOp.IMAGE_BORDER_PARALLEL_CUSTOM_TASK;
							else
								dev.doingSpecialParallelOperation = SpecialOp.SPECIAL_OP_NONE;
							ahtb.close();
							dev.reallyMonitor();
						}},
						{text:"Cancel", action: function() {
							ahtb.close();
						}}
					],
					opened: function() {}
				})
	}

	this.reallyMonitor = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'getlaststartentry',
					specialOp: dev.doingSpecialParallelOperation,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error during monitorParallel', 
							height: 150, 
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
				dev.gotLastStartEntry = true;
				dev.doingMonitor = 0;
				dev.setButtons();
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK') {
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed to get any PP starting point data</p>' });
		  	}
		  	else {
		  		dev.gotLastStartEntry = true;
		  		if (data.data == null) {
		  			dev.doingMonitor = 0;
					dev.setButtons();
					ahtb.open({ title: 'Cannot find starting point to monitor from', 
							height: 150, 
							html: '<p>Make sure the parse to DB completed and parallel processing started.</p>' 
						});
					return;
		  		}
		  		var type = data.data['type'];
		  		if (!dev.doIP() &&
		  			type != LogState.RESTART_PARALLEL)
		  		{
		  			var msg = "Line returned is not of type RESTART_PARALLEL, it has a value of type: "+type;
		  			console.log(msg);
		  			ahtb.alert(msg,
		  						{height: 150});
		  			dev.doingMonitor = 0;
					dev.setButtons();
		  			return;
		  		}
		  		else if ( (dev.doingSpecialParallelOperation == SpecialOp.IMAGE_BORDER_PARALLEL &&
		  				   type != LogState.RESTART_PARALLEL_IP) ||
		  				   (dev.doingSpecialParallelOperation == SpecialOp.IMAGE_PROCESSING_CUSTOM_TASK &&
		  				   type != LogState.RESTART_PARALLEL_CUSTOM_IP) ) {
		  			var msg = "Line returned is not of type "+(dev.doingSpecialParallelOperation == SpecialOp.IMAGE_BORDER_PARALLEL ? 'RESTART_PARALLEL_IP' : 'RESTART_PARALLEL_CUSTOM_IP')+", it has a value of type: "+type;
		  			console.log(msg);
		  			ahtb.alert(msg,
		  						{height: 150});
		  			dev.doingMonitor = 0;
					dev.setButtons();
		  			return;
		  		}
		  		var result = JSON.parse(data.data.data);
		  		var subdomains = typeof result.subdomains == 'number' ? result.subdomains : parseInt(result.subdomains);
		  		var chunks = typeof result.chunks == 'number' ? result.chunks : parseInt(result.chunks);
		  		var unitsperchunk = typeof result.unitsperchunk == 'number' ? result.unitsperchunk : parseInt(result.unitsperchunk);
		  		var saverejected = typeof result.saverejected == 'string' ? result.saverejected : parseInt(result.saverejected);
		  		var cpulimit = typeof result.cpulimit == 'float' ? result.cpulimit : parseFloat(result.cpulimit);
		  		var minTags = typeof result.minTags == 'number' ? result.minTags : parseInt(result.minTags);
		  		var minPrice = typeof result.minPrice == 'number' ? result.minPrice : parseInt(result.minPrice);
		  		var totalBlocks = typeof result.totalBlocks == 'number' ? result.totalBlocks : parseInt(result.totalBlocks);
		  		var ppRunId = typeof result.ppRunId == 'number' ? result.ppRunId : parseInt(result.ppRunId);
		  		var specialOperation = typeof result.specialOperation == 'number' ? result.specialOperation : parseInt(result.specialOperation);
		  		var starttime = result.starttime;
		  		//starttime = typeof starttime == 'number' ? starttime : parseInt(starttime);


		  // 		var dateTime = starttime.split(" ");
				// var datePart = dateTime[0].split('-');
				// var timePart = dateTime[1].split(':');

				// //this makes it browser friendly
				// dev.startTaskTime = new Date(datePart[0], datePart[1], datePart[2], timePart[0], timePart[1], timePart[2]).getTime();
		  		//starttime = starttime.replace(" ", "T");

		  		// transfer data to dev
		  		//dev.startTaskTime = new Date(starttime).getTime();
		  		var timesplit = starttime.split(" ");
		  		starttime = parseFloat(timesplit[0])*1000 + parseFloat(timesplit[1])*1000;
		  		dev.startTaskTime = starttime;
		  		dev.maxSubDomainsAllowed = dev.numSubDomains = subdomains;
		  		dev.numChunkPerSubDomain = chunks;
		  		dev.unitsPerChunk = unitsperchunk;
		  		dev.minTags = minTags;
		  		dev.minPrice = minPrice;
		  		dev.totalBlocks = totalBlocks;
		  		dev.ppRunId = ppRunId;
		  		dev.linesMonitorStart = parseInt(data.data.id);
		  		dev.doingSpecialParallelOperation = specialOperation;

		  		console.log("Got start point data of current parallel processing, subdomains:"+subdomains+", chunks:"+chunks+", unitsperchunk:"+unitsperchunk+", totalBlocks:"+totalBlocks+", minTags:"+minTags+", minPrice:"+minPrice+", saveRejected: "+saverejected+", starttime:"+starttime+", ppRunId:"+ppRunId);
		  		console.log("Results registration will start at db row:"+dev.linesMonitorStart+1);

		  		$('ul#tables #subdomain option[value="'+dev.numSubDomains+'"]').prop('selected', true);
		  		$('ul#tables #maxsubdomain option[value="'+dev.maxSubDomainsAllowed+'"]').prop('selected', true);
		  		$('ul#tables #threads option[value="'+dev.numChunkPerSubDomain+'"]').prop('selected', true);
		  		$('ul#tables #tags option[value="'+dev.minTags+'"]').prop('selected', true);
		  		$('ul#tables #price option[value="'+dev.minPrice+'"]').prop('selected', true);
		  		$('ul#tables #loadlimit').val(cpulimit);
		  		$("ul#tables #saveRejected").prop('checked', saverejected == 'true');

		  		dev.monitorParallel();
		  	}
		  }
		});
	}

	this.monitorParallel = function() {
		if (!dev.gotLastStartEntry) {
			// var curTime = new Date().getTime();
			// if ( (curTime - dev.startGetLastStartEntryTime) > 5000)
			dev.beginMonitorParallel();
			// setTimeout(function() {
			// 	dev.monitorParallel();
			// }, 500);
			return;
		}

		if (!dev.doingMonitor) {
			console.log("Monitoring is aborted due to failure to get startup data");
			return;
		}
		dev.getLastProgressCount();
		dev.beginMonitoringSession();
	}

	this.getAmazonIndex = function(host) {
		if (!dev.amazon)
			return -1;

		for(var i in dev.amazon) {
			if (typeof dev.amazon[i] == 'object')
				if (dev.amazon[i]['PublicIP'] == host)
					return i;
		}
		return -1;
	}

	this.pingAmazon = function(id) {
		var myPrefix = "http://"+dev.amazon[id]['PublicIP']+"/wp-content/themes/allure";
		$.ajax({
			url: myPrefix+'/_admin/ajax_developer.php',
			data: { query: 'ping',
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during ping: "+msg);
			},					
		  	success: function(data, status, xhr){
			  	if (data == null || data.status == null ||
			  		typeof data == 'undefined' || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed to get any Ping response</p>' });
			  	}
			  	else {
			  		var load = data.data.load;
			  		var host = data.data.host;
			  		console.log('Host: '+host);
	  				dev.tallyLoadPingCount[id]++;
	  				dev.tallyLoadPing[id] += typeof load == 'float' ? load : parseFloat(load);
	  				if ((id+1) == dev.numSubDomains) {
	  					load = dev.tallyLoadPing.reduce(function(previousValue, currentValue, index, array) {
						  return previousValue + currentValue;
						});
						loadCount = dev.tallyLoadPingCount.reduce(function(previousValue, currentValue, index, array) {
						  return previousValue + currentValue;
						});

		  				$('#load').val((load/loadCount).toFixed(2));
		  				$('#currentLoad').html((load/loadCount).toFixed(2));
		  			}
	  			}
	  		}
  		})
	}

	this.ping = function() {
		var myPrefix = ah_local.tp;
		if (dev.amazon != null) {
			for(var i = 0; i < dev.numSubDomains; i++)
				dev.pingAmazon(i);
			return;
		}
		$.ajax({
			url: myPrefix+'/_admin/ajax_developer.php',
			data: { query: 'ping',
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during ping: "+msg);
			},					
		  	success: function(data){
			  	if (data == null || data.status == null ||
			  		typeof data == 'undefined' || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed to get any Ping response</p>' });
			  	}
			  	else {
			  		var load = data.data.load;
			  		dev.tallyLoadCount++;
	  				dev.tallyLoad += typeof load == 'float' ? load : parseFloat(load);

	  				if ( (Math.abs((dev.tallyLoad/dev.tallyLoadCount)-load)/load) > 0.05 ) {
	  					dev.tallyLoadCount = 1;
	  					dev.tallyLoad = load;
	  				}
	  				// dev.tallyLoadPingCount[dev.pingCycle]++;
	  				// dev.tallyLoadPing[dev.pingCycle] += typeof load == 'float' ? load : parseFloat(load);
	  				var curLoad = (dev.tallyLoad/(dev.tallyLoadCount ? dev.tallyLoadCount : 1));
	  				$('#load').val(curLoad.toFixed(2));
	  				$('#currentLoad').html(curLoad.toFixed(2));
	  				dev.testOverloadedCpu(curLoad);
	  			}
	  		}
  		})
	}

	this.testOverloadedCpu = function(load, force) {
		force = typeof force == 'undefined' ? false : force;
		var which = 'local';
		var limit = 100;
		var sustainedTimeLimit = dev.cpuMaxHeavyLoadSustainedTimeLimitEngr1;
		if ( ah_local.wp.indexOf('engr1') != -1) {
			which = 'engr1';
			limit = dev.cpuHeavyLoadLimitEngr1;
		}
		else if ( ah_local.wp.indexOf('lifestyledlistings.com') != -1) {
			which = 'live server';
			limit = dev.cpuHeavyLoadLimitLive;
			sustainedTimeLimit = dev.cpuMaxHeavyLoadSustainedTimeLimitLive;
		}

		if (which == 'local') {
			return;
		}
		if (parseFloat(load) > limit ||
			force ) { // dev.cpuHeavyLoadLimit is set at MAX_HEAVY_LOAD_LIMIT right now
			//this.cpuHeavyLoadSustainedTime = 0;
			//this.cpuMaxHeavyLoadSustainedLimit = 5*60*1000; // 5 min
			// is it the first time?
			var now = Date.now();
			if (!dev.cpuHeavyLoadSustainedTime &&
				!force) // first overload
				dev.cpuHeavyLoadSustainedTime = now;
			else {
				var duration = 0;
				if ( force ||
					 (now - dev.cpuHeavyLoadSustainedTime) > sustainedTimeLimit ) {// uh oh..
					duration = (now - dev.cpuHeavyLoadSustainedTime)/1000.0;
					if ( which == 'engr1' ) {
						if (haveMyOwn &&
							(dev.doingRemote == RemoteMode.IMAGE_BORDER_CRON_FROM_LAST ||
							dev.doingRemote == RemoteMode.IMAGE_BORDER_CRON_ALL ||
							dev.doingRemote == RemoteMode.IMAGE_PROCESSING_CUSTOM_TASK_FROM_LAST ||
							dev.doingRemote == RemoteMode.IMAGE_PROCESSING_CUSTOM_TASK) ) {
							dev.cpuHeavyLoadSustainedTime = 0;
							dev.cpuOverloadedCount++;
							dev.cpuAccumulatedOverloadEngr1 += duration;
							if (dev.cpuOverloadedCount < MAX_OVERLOAD_ALLOWED_COUNT)
								return;
						}
						dev.cpuAccumulatedOverloadEngr1 += duration;
					}
					
					$.ajax({
						url: ah_local.tp+'/_admin/ajax_developer.php',
						data: { query: 'alert-overload',
								mountain: dev.myMountain,
								everest: dev.myEverest,
								which: which,
								load: load,
								duration: which == 'engr1' ? dev.cpuAccumulatedOverloadEngr1 : duration,
								havePP: havePP,
								haveIP: haveIP,
								haveIPCustom: haveIPCustom,
								haveMyOwn: haveMyOwn
							  },
						dataType: 'JSON',
						type: 'POST',
						error: function($xhr, $status, $error){
							// dev.doingPPRecords = false;
							// dev.ajaxCount--;
							var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
							console.log("Error during alert-overload: "+msg);
						},					
					  	success: function(data){
					  		// dev.ajaxCount--;
						  	if (data == null || data.status == null ||
						  		typeof data == 'undefined' || typeof data.status == 'undefined') 
						  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
						  	else if (data.status != 'OK') {
						  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed to alert-overload</p>' });
						  	}
						  	else {
						  		console.log("Alerted Tom about alert-overload");
						  		dev.cpuOverloadedCount = 0; // reset
						  		dev.cpuAccumulatedOverloadEngr1 = 0;
						  		dev.ajax({query: 'post-to-progress',
			  					  		  msg: {message: "Alerted Tom about alert-overload on "+which,
												load: load,
												duration: duration,
												havePP: havePP,
												haveIP: haveIP,
												haveIPCustom: haveIPCustom,
												haveMyOwn: haveMyOwn		
											   },
										  mountain: dev.myMountain,
										  everest: dev.myEverest 
								});
				  			}
				  		}
			  		});
					dev.cpuHeavyLoadSustainedTime = 0; // reset to zero
				}
			}


		}
		else
			dev.cpuHeavyLoadSustainedTime = 0; // reset to zero
	}

	this.getPPRecordRange = function(startId, endId) {
		dev.lastTryGetPPRecordRange = new Date().getTime();
		if (typeof startId == 'undefined')
			startId = dev.getPPRecordStart;
		else
			dev.getPPRecordStart = startId;
		if (typeof endId == 'undefined')
			endId = dev.getPPRecordEnd;
		else
			dev.getPPRecordEnd = endId;

		dev.havePPRecords = false;
		dev.ppRecords = null;
		dev.doingPPRecords = true;
		//console.log("getPPRecordRange - requesting from "+startId+" to "+endId);
		// dev.ajaxCount++;
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'getpprecordrange',
					mountain: dev.myMountain,
					everest: dev.myEverest,
					startid: startId,
					endid: endId },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				dev.havePPRecords = true;
				// dev.doingPPRecords = false;
				// dev.ajaxCount--;
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during getPPRecordRange: "+msg);
			},					
		  	success: function(data){
		  		// dev.ajaxCount--;
			  	if (data == null || data.status == null ||
			  		typeof data == 'undefined' || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed to get any PP recording range</p>' });
			  	}
			  	else {
			  		//console.log("Got record batch from "+startId+" to "+endId);
			  		dev.ppRecords = data.data;
	  			}
				dev.havePPRecords = true;
	  		}
  		});
	}

	this.registerRemotePPAble = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'registerremoteppable',
					remoteppable: dev.myEverest,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during registerRemotePPAble: "+msg);
			},					
		  	success: function(data){
			  	if (data == null || data.status == null ||
			  		typeof data == 'undefined' || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed to get register as remote PP able</p>' });
			  	}
			  	else {
			  		if (data != null && data.status != null && data.status == 'OK') {
			  			dev.canRemotePP = 1;
			  			console.log(data.data);
			  			$('ul#tables #startRemoteParallel').hide();
			  		}
			  		else {
			  			console.log('Failed to register');
			  			dev.canRemotePP = 0;
			  		}
					$('ul#tables #registerAsRemotePP').val(dev.canRemotePP ? "Deregister to run remote parallel processing" : "Register this to be able to run parallel list hub data processing");
	  			}
	  		}
  		})
	}

	this.amRemotePPAble = function() {
		dev.gotAmRemotePPAble = false;
		dev.canRemotePP = 0;
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'amremoteppable',
					remoteppable: dev.myEverest,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during amRemotePPAble: "+msg);
			},					
		  	success: function(data){
			  	if (data == null || data.status == null ||
			  		typeof data == 'undefined' || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
			  	else if (data.status != 'OK') {
			  		dev.gotAmRemotePPAble = true;
			  	}
			  	else {
			  		dev.gotAmRemotePPAble = true;
			  		if (data != null && data.status != null && data.status == 'OK') {
			  			console.log(data.data);
			  			dev.canRemotePP = 1;
			  			setCookie('RegisteredAgent', dev.canRemotePP, 30);
			  		}
			  		else
			  			console.log(data.data);
	  			}
	  		}
  		})
	}

	this.removeRemotePPAble = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'removeremoteppable',
					remoteppable: dev.myEverest,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during removeRemotePPAble: "+msg);
			},					
		  	success: function(data){
			  	if (data == null || data.status == null ||
			  		typeof data == 'undefined' || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed to get remove PP remote registration</p>' });
			  	}
			  	else {
			  		if (data != null && data.status != null && data.status == 'OK') {
			  			dev.canRemotePP = 0;
			  			setCookie('RegisteredAgent', dev.canRemotePP, 2);
			  			console.log(data.data);
			  			dev.setButtons();
			  		}
			  		else
			  			console.log('Failed to remove PP registration');
	  			}
	  		}
  		})
	}
		
	this.startRemotePPAble = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'startremotepp',
					remoteppable: dev.myEverest, // caller of remote PP
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during startRemotePPAble: "+msg);
			},					
		  	success: function(data){
			  	if (data == null || data.status == null ||
			  		typeof data == 'undefined' || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
			  	else if (data.status != 'OK') {
			  		msg =  data.data != null ? data.data : '<p>Failed to get any PP starting point data</p>';
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: msg });
			  		$('ul#tables #startRemoteParallel').show();
			  		$('ul#tables #startListHubDataProcessing').show();
			  		$('ul#tables #startRemoteImageProcessingProcessing').show();
			  	}
			  	else {
			  		if (data != null && data.status != null && data.data != null)
			  			console.log(data.data);
			  		else {
			  			console.log('Failed to start remote PP');
			  			$('ul#tables #startRemoteParallel').show();
			  			$('ul#tables #startListHubDataProcessing').show();
			  			$('ul#tables #startRemoteImageProcessingProcessing').show();
			  		}
	  			}
	  		}
  		})
	}

	this.startRemoteParse2Db = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'startremotep2db',
					remoteppable: dev.myEverest, // caller of remote PP
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during startRemoteParse2Db: "+msg);
			},					
		  	success: function(data){
			  	if (data == null || data.status == null ||
			  		typeof data == 'undefined' || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
			  	else if (data.status != 'OK') {
			  		msg =  data.data != null ? data.data : '<p>Failed to get any P2Db starting point data</p>';
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: msg });
			  		$('ul#tables #startRemoteParallel').show();
			  		$('ul#tables #startListHubDataProcessing').show();
			  		$('ul#tables #startRemoteImageProcessingProcessing').show();
			  	}
			  	else {
			  		if (data != null && data.status != null && data.data != null)
			  			console.log(data.data);
			  		else {
			  			console.log('Failed to start remote PP');
			  			$('ul#tables #startRemoteParallel').show();
			  			$('ul#tables #startListHubDataProcessing').show();
			  			$('ul#tables #startRemoteImageProcessingProcessing').show();
			  		}
	  			}
	  		}
  		})
	}

	this.startRemoteImageProcessing = function() {
		ahtb.open({
	            title: 'Choose Listing Group',
	            height: 180,
	            width: 550,
	            html: '<p>Do you want to clean the image border in which group?<br/>It will take a while, the process will be running in the background.</p>'+
	            	  '<div style="display: inline; padding-bottom: 10px; margin-bottom: 10px;">'+
	            	  '<span>Process only new images</span><span> </span><input type="checkbox" id=processOnlyNewImages />'+
	            	  '<span>Start from beginning</span><span> </span><input type="checkbox" id=processAllImages />'+
	            	  '</div><br/><br/>',
		        	buttons:[
	        			{text:"Live", action:function(){ 
	        				// ahtb.close(); 
	        				dev.imageBorderFromLast = $('input#processAllImages').prop('checked') ? 0 : 1;
	        				dev.imageBorderMode = ActiveState.ACTIVE;
	        				dev.getFixImageListingCount(LogState.REMOTE_IMAGE_PROCESSING);
	        				// dev.fixImageBorders(ActiveState.ACTIVE, val); 
	        			}},
            			{text:"Waiting", action:function(){ 
            				// ahtb.close(); 
            				dev.imageBorderFromLast = $('input#processAllImages').prop('checked') ? 0 : 1;
            				dev.imageBorderMode = ActiveState.WAITING;
            				dev.getFixImageListingCount(LogState.REMOTE_IMAGE_PROCESSING);
            				// dev.fixImageBorders(ActiveState.WAITING, val); 
            			}}
        			],
        		opened: function() {
        			$('#processOnlyNewImages').prop('checked', dev.processOnlyNewImages);
        			$('#processOnlyNewImages').change(function(){
						dev.processOnlyNewImages = $(this).prop("checked");
					});
        		},
	            closed: function(){
	              ahtb.showClose(250);
	              ahtb.closeOnClickBG(1);
	            }	          
	        });
	}

	this.onInitFs = function (fs) {
	  fs.root.getFile('log.txt', 
		  				{create: true}, 
		  				function(fileEntry) {
						    // Create a FileWriter object for our FileEntry (log.txt).
						    fileEntry.createWriter(function(fileWriter) {
						      fileWriter.seek(fileWriter.length); // Start write position at EOF.

						      // Create a new Blob and write it to log.txt.
						      var blob = new Blob([dev.report], {type: 'text/plain'});

						      fileWriter.write(blob);

						    }, 
						 	dev.errorHandler);
						}, 
						dev.errorHandler
					);
	}

	this.removeListings = function(start, end) {
		console.log("Chose to remove listings from "+start+", to "+(typeof end != 'undefined' ? (end.length ? end : start) : start)+", preserveActive:"+(dev.preserveActiveListings ? "yes" : "no") );
		var data = { query: 'remove-listings',
					 start: start,
					 end: (typeof end != 'undefined' ? (end.length ? end : start) : start),
					 preserveActive: dev.preserveActiveListings ? 1 : 0,
					 mountain: dev.myMountain,
					 everest: dev.myEverest };

		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: data,
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error during removeListings',
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK')
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  	else {
			  	console.log("removeListings: "+data.data);
			  	ahtb.alert(data.data,
			  			   {height: 150});
		  	}
		  }
		});
	}

	this.fixImageBorders = function(mode, fromLast) {
		//ActiveState.ACTIVE
		if (typeof fromLast == 'undefined')
			fromLast = 1;
		console.log("Chose to clean up images in group:"+mode+", only new:"+dev.processOnlyNewImages+", fromLast:"+fromLast);
		var data = { query: 'fiximageborders',
					mode: mode,
					op: 0,
					onlynew: dev.processOnlyNewImages,
					fromLast: fromLast,
					mountain: dev.myMountain,
					everest: dev.myEverest };

		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: data,
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error during fixImageBorders',
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK')
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  	else {
			  	console.log("fixImageBorders: "+data.data);
			  	ahtb.alert(data.data,
			  			   {height: 150});
		  	}
		  }
		});
	}

	this.checkCronElement = function(curDate, cron, what) {
		var match = true;
		if (cron != "*") {
			var time = cron.split(",");
			for(var t in time) {
				var now = 0;
				switch(parseInt(what)) {
					case 0: now = curDate.getMinutes(); break;
					case 1: now = curDate.getHours(); break;
					case 2: now = curDate.getDate(); break;
					case 3: now = curDate.getMonth()+1; break;
					case 4: now = curDate.getDay(); break;
				}
				if (time[t] == now) {
					match = true;
					break;
				}
				else
					match = false;
			}
		}
		else
			match = true;

		return match;
	}
					// array
	this.cronTimeIsNow = function(cronTime) {
		var curDate = new Date();

		for(var x in cronTime) {
			if (!dev.checkCronElement(curDate, cronTime[x], x))
				return false;
		}
		return true;
	}

	this.checkCronJob = function() {
		dev.lastCheckedForCronJobs = new Date().getTime();
		// console.log("Checking for cron jobs");
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'checkcronjob',
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log(msg);
				window.setTimeout(function() {
					dev.checkCronJob();
				}, 5000);
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK')
		  		console.log ('You Have Encountered an Error during cron check.  '+(typeof data.data=='undefined'? "No data available." : data.data));
		  		//ahtb.open({ title: 'You Have Encountered an Error during cron check', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  	else {
		  		if (data.status == 'OK') {
			  		// console.log("checkCronJob: "+data.data);

			  		if (data.data == "No cron jobs")
					  	ahtb.alert(data.data,
					  			   {height: 150});
					else {
						var result = data.data;
						if (result.opt == 'scheduledPPJobs') {
							var jobs = JSON.parse(result.value);
							for(var job in jobs) {
								var task = jobs[job];
								if (task.hasOwnProperty('Full')) {
									var options = task.Full.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											window.setTimeout(function() {
												dev.startCronParallel();
											}, 90000);
										}
									}
								}
								if (task.hasOwnProperty('FullNoOptimizeTable')) {
									var options = task.FullNoOptimizeTable.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											window.setTimeout(function() {
												dev.startCronParallel(false);
											}, 90000);
										}
									}
								}
								else if (task.hasOwnProperty('FullNoImageProcessing')) {
									var options = task.FullNoImageProcessing.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											window.setTimeout(function() {
												dev.startCronParallel(true, false);
											}, 90000);
										}
									}
								}
								else if (task.hasOwnProperty('UseExistingDataParse')) {
									var options = task.UseExistingDataParse.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											var msg = "startCronReuseParallel() received cron ok at "+getTime();
											dev.ajax({query: 'post-to-progress',
											  		msg: {	message: msg,
											  				everest: dev.myEverest,
											  			},
												  mountain: dev.myMountain,
												  everest: dev.myEverest 
											});
											window.setTimeout(function() {
												dev.startCronReuseParallel();
											}, 90000);
										}
									}
								}
								else if (task.hasOwnProperty('UseExistingDataParseResetTiers')) {
									var options = task.UseExistingDataParseResetTiers.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											var msg = "startCronReuseParallel(reset) received cron ok at "+getTime();
											dev.ajax({query: 'post-to-progress',
											  		msg: {	message: msg,
											  				everest: dev.myEverest,
											  			},
												  mountain: dev.myMountain,
												  everest: dev.myEverest 
											});
											window.setTimeout(function() {
												dev.startCronReuseParallel(true);
											}, 90000);
										}
									}
								}
								else if (task.hasOwnProperty('WriteSiteMaps')) {
									var options = task.WriteSiteMaps.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											window.setTimeout(function() {
												dev.writeSiteMaps();
											}, 60000);
										}
									}
								}
								else if (task.hasOwnProperty('StartAWS')) {
									var options = task.StartAWS.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											dev.doingRemote = RemoteMode.AWS;
											dev.startEc2Count = 3;
											dev.ajax({
												query: 'start-aws',
												mountain: dev.myMountain,
												everest: dev.myEverest });
											break;
										}
									}
								}
								else if (task.hasOwnProperty('ImageCropAll')) {
									var options = task.ImageCropAll.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											dev.imageBorderOptimizeTableAfterwards = true;
											window.setTimeout(function() {
												var msg = "ImageCropAll is starting IP"+" at "+getTime();
												dev.ajax({query: 'post-to-progress',
												  		msg: {	message: msg,
												  				everest: dev.myEverest,
												  				mode: dev.doingRemote,
												  				imageBorderOptimizeTableAfterwards: dev.imageBorderOptimizeTableAfterwards
												  			},
													  mountain: dev.myMountain,
													  everest: dev.myEverest 
												});
												dev.startImageProcessing(RemoteMode.IMAGE_BORDER_CRON_ALL);
											}, 60000);
										}
									}
								}
								else if (task.hasOwnProperty('ImageCropFromLast')) {
									var options = task.ImageCropFromLast.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											dev.imageBorderOptimizeTableAfterwards = false;
											window.setTimeout(function() {
												var msg = "ImageCropFromLast is starting IP"+" at "+getTime();
												dev.ajax({query: 'post-to-progress',
												  		msg: {	message: msg,
												  				everest: dev.myEverest,
												  				mode: dev.doingRemote,
												  				imageBorderOptimizeTableAfterwards: dev.imageBorderOptimizeTableAfterwards
												  			},
													  mountain: dev.myMountain,
													  everest: dev.myEverest 
												});
												dev.startImageProcessing(RemoteMode.IMAGE_BORDER_CRON_FROM_LAST);			
											}, 60000);
										}
									}
								}
								else if (task.hasOwnProperty('UseExistingDataImageProcessing')) {
									var options = task.UseExistingDataImageProcessing.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											dev.imageBorderOptimizeTableAfterwards = false;
											var msg = "UseExistingDataImageProcessing is starting IP"+" at "+getTime();
											dev.ajax({query: 'post-to-progress',
											  		msg: {	message: msg,
											  				everest: dev.myEverest,
											  				mode: dev.doingRemote,
											  				imageBorderOptimizeTableAfterwards: dev.imageBorderOptimizeTableAfterwards
											  			},
												  mountain: dev.myMountain,
												  everest: dev.myEverest 
											});
											$('ul#tables #restartParallelIP').click();
										}
									}
								}
								else if (task.hasOwnProperty('CustomImageAll')) {
									var options = task.CustomImageAll.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											dev.imageBorderFromLast = 0;
											dev.imageBorderOptimizeTableAfterwards = true;
											window.setTimeout(function() {
												dev.startImageProcessing(RemoteMode.IMAGE_PROCESSING_CUSTOM_TASK);		
											}, 60000);
										}
									}
								}
								else if (task.hasOwnProperty('CustomImageFromLast')) {
									var options = task.CustomImageFromLast.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											dev.imageBorderFromLast = 1;
											dev.imageBorderOptimizeTableAfterwards = false;
											window.setTimeout(function() {
												dev.startImageProcessing(RemoteMode.IMAGE_PROCESSING_CUSTOM_TASK_FROM_LAST);			
											}, 60000);
										}
									}
								}
								else if (task.hasOwnProperty('CustomImageReuseExistingData')) {
									var options = task.CustomImageFromLast.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											$('ul#tables #restartParallelCustomIP').click();								
										}
									}
								}
								else if (task.hasOwnProperty('PausePP')) {
									var options = task.PausePP.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP &&
											(dev.doingRemote ||
											 dev.doingParallel)) {
											$('ul#tables #pauseParallel').click();					
										}
									}
								}
								else if (task.hasOwnProperty('ResumePP')) {
									var options = task.ResumePP.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP &&
											(dev.doingRemote ||
											 dev.doingParallel)) {
											$('ul#tables #pauseParallel').click();					
										}
									}
								}
								else if (task.hasOwnProperty('Reload')) {
									var options = task.Reload.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP &&
											!dev.doingRemote) {
											window.setTimeout(function() {
												dev.ajax({query: 'prep-reload',
									  					  msg: 'Registered unit is preparing reloading page from '+result.remoteRequester+' to - everest:'+dev.myEverest,
														  mountain: dev.myMountain,
														  everest: dev.myEverest });
											}, 60000);
														
										}
									}
								}
								else if (task.hasOwnProperty('CountCities')) {
									var options = task.CountCities.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											dev.countCities();			
										}
									}
								}
								else if (task.hasOwnProperty('ParseFeedOnly')) {
									var options = task.ParseFeedOnly.split(" ");
									if (dev.cronTimeIsNow(options)) {
										if (dev.canRemotePP) {
											dev.parseFeedOnly();			
										}
									}
								}
							}
						}
					}
				}
		  	}
		  }
		});
	}

	this.writeSiteMaps = function() {
		console.log("Writing site maps!");
		$('ul#tables a[data-query=write-site-maps]').click();
	}

	this.parseFeedOnly = function() {
		if (dev.haveActiveBE == 'working') {
			window.setTimeout(function() {
				dev.parseFeedOnly();
			}, 100);
			return;
		}

		setBEStates();
		if (dev.haveActiveBE != 'false') {
			if (dev.startingActiveP2Db) {
				console.log("parseFeedOnly has startingActiveP2Db as true");
				return;
			}
			if (havePP) {
				dev.log("parseFeedOnly not queued - haveMyOwn:"+haveMyOwn+", havePP:"+havePP);
				return;
			}
		}

		dev.onlyParseFeed = true;
		dev.doingRemote = RemoteMode.PARSE_FEED_ONLY;
		dev.retryParseCount = 0;

		var msg = "parseFeedOnly is calling getlisthubdataParallel"+" at "+getTime();
		dev.log(msg);
		dev.setExtraInfo('<p>'+msg+'</p>');
		dev.ajax({query: 'post-to-progress',
		  		msg: {	message: msg,
		  				everest: dev.myEverest,
		  			},
			  mountain: dev.myMountain,
			  everest: dev.myEverest 
		});

		dev.ajax({
			query: 'getlisthubdataParallel',
			mountain: dev.myMountain,
			everest: dev.myEverest });
	}

	this.startCronParallel = function(optimizeTable, doIP) {
		if (dev.haveActiveBE == 'working') {
			window.setTimeout(function() {
				dev.startCronParallel(optimizeTable, doIP);
			}, 100);
			return;
		}

		setBEStates();
		if (dev.haveActiveBE != 'false') {
			if (dev.startingActiveP2Db) {
				console.log("startCronParallel has startingActiveP2Db as true");
				return;
			}
			if (haveMyOwn ||
				havePP) {
				dev.log("startCronParallel not queued - haveMyOwn:"+haveMyOwn+", havePP:"+havePP);
				return;
			}
		}

		if (dev.doingRemote != RemoteMode.NONE) {
			dev.log("startCronParallel - doingRemote is not NONE, it is set to"+dev.doingRemote);
			return;
		}

		if ( typeof optimizeTable == 'undefined')
			optimizeTable = true;
		if ( typeof doIP == 'undefined')
			doIP = true;

		dev.synchroDeltaTime = 10000;
		dev.doingSpecialParallelOperation = 0;
		dev.restartSQL = false;
		dev.countCitiesAfterParse = true;
		dev.imageBorderOptimizeTableAfterwards = false;
		dev.imageProcessAfterParse = doIP;
		dev.resetMedianAndTiers = true;
		dev.listhubParseOptimizeTableAfterwards = optimizeTable;
		dev.doingRemote = RemoteMode.FULL_PARSE_CRON;
		dev.runPPAfterResult = true;
		dev.errorWriteLogHeaders = false;
		dev.onlyParseFeed = false;
		if ( ah_local.tp.indexOf('localhost') == -1) {
			if (!dev.userSelectedUnitsPerChunk)
				dev.unitsPerChunk = MAX_LISTHUB_PROCESSING_CHUNK_SIZE;
			dev.userSelectedUnitsPerChunk = false;
			dev.loadLimit = parseInt(dev.loadLimit) < dev.loadLimitParse ? dev.loadLimitParse.toString() : dev.loadLimit;
			$('ul#tables #loadlimit').val(dev.loadLimit);
		}
		else
			dev.unitsPerChunk = 4;
		dev.retryParseCount = 0;
		$('ul#tables #chunks option[value="'+dev.unitsPerChunk+'"]').prop('selected', true);

		var msg = "startCronParallel is calling getlisthubdataParallel"+" at "+getTime();
		dev.log(msg);
		dev.setExtraInfo('<p>'+msg+'</p>');
		dev.ajax({query: 'post-to-progress',
		  		msg: {	message: msg,
		  				everest: dev.myEverest,
		  			},
			  mountain: dev.myMountain,
			  everest: dev.myEverest 
		});

		dev.ajax({
			query: 'getlisthubdataParallel',
			mountain: dev.myMountain,
			everest: dev.myEverest });
	}

	this.startCronReuseParallel = function(resetTiers) {
		if (dev.haveActiveBE == 'working') {
			window.setTimeout(function() {
				dev.startCronReuseParallel(resetTiers);
			}, 100);
			return;
		}

		setBEStates();
		if (dev.haveActiveBE != 'false') {
			if (haveMyOwn ||
				havePP) {
				dev.log("startCronReuseParallel not queued - haveMyOwn:"+haveMyOwn+", havePP:"+havePP);
				var msg = "startCronReuseParallel not queued - haveMyOwn:"+haveMyOwn+", havePP:"+havePP;
				dev.ajax({query: 'post-to-progress',
				  		msg: {	message: msg,
				  				everest: dev.myEverest,
				  			},
					  mountain: dev.myMountain,
					  everest: dev.myEverest 
				});
				return;
			}
		}

		if (dev.doingRemote != RemoteMode.NONE) {
			dev.log("startCronReuseParallel - doingRemote is not NONE, it is set to"+dev.doingRemote);
			return;
		}

		if ( typeof resetTiers == 'undefined')
			resetTiers = false;

		dev.synchroDeltaTime = 10000;
		dev.doingSpecialParallelOperation = 0;
		dev.restartSQL = false;
		dev.countCitiesAfterParse = true;
		dev.imageBorderOptimizeTableAfterwards = false;
		dev.imageProcessAfterParse = true;
		dev.resetMedianAndTiers = resetTiers;
		dev.listhubParseOptimizeTableAfterwards = true;
		dev.doingRemote = RemoteMode.FULL_PARSE_CRON;
		dev.runPPAfterResult = false;
		dev.errorWriteLogHeaders = false;
		if ( ah_local.tp.indexOf('localhost') == -1) {
			if (!dev.userSelectedUnitsPerChunk)
				dev.unitsPerChunk = MAX_LISTHUB_PROCESSING_CHUNK_SIZE;
			dev.userSelectedUnitsPerChunk = false; // reset
			dev.loadLimit = parseInt(dev.loadLimit) < dev.loadLimitParse ? dev.loadLimitParse.toString() : dev.loadLimit;
			$('ul#tables #loadlimit').val(dev.loadLimit);
		}
		else
			dev.unitsPerChunk = 4;
		$('ul#tables #chunks option[value="'+dev.unitsPerChunk+'"]').prop('selected', true);

		var msg = "startCronReuseParallel is restarting Parallel"+" at "+getTime();
		dev.ajax({query: 'post-to-progress',
		  		msg: {	message: msg,
		  				everest: dev.myEverest,
		  			},
			  mountain: dev.myMountain,
			  everest: dev.myEverest 
		});

		$('ul#tables #restartParallel').click();
		// dev.ajax({
		// 	query: 'restartParallel',
		// 	mountain: dev.myMountain,
		// 	everest: dev.myEverest });
	}

	this.startImageProcessing = function(mode) {
		if (dev.haveActiveBE == 'working') {
			window.setTimeout(function() {
				dev.startImageProcessing(mode);
			}, 100);
			return;
		}

		setBEStates();
		if (dev.haveActiveBE != 'false') {
			if ( haveMyOwn ||
				 haveIP ) {
				window.setTimeout(function() {
					dev.startImageProcessing(mode);
				},1000*60*15); // retry in 15 min
				return;
			}
		}

		dev.doingRemote = mode;
		var msg = "startImageProcessing is restarting IP"+" at "+getTime();
		dev.ajax({query: 'post-to-progress',
		  		msg: {	message: msg,
		  				everest: dev.myEverest,
		  				mode: dev.doingRemote
		  			},
			  mountain: dev.myMountain,
			  everest: dev.myEverest 
		});
		dev.errorWriteLogHeaders = false;
		dev.getFixImageListingCount(dev.doingRemote, true); // true, fromCron
	}

	this.countCities = function() {
		dev.ajax({
			query: 'count-cities',
			mountain: dev.myMountain,
			everest: dev.myEverest 
		});
	}

	this.inviteCodes = function(mode) {
		ahtb.close();
		console.log("Invitation code - count:"+mode);
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'invitationcodes',
					mode: mode,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.open({ title: 'You Have Encountered an Error during inviteCodes',
							html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
						});
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error during inviteCodes', height: 150, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK')
		  		ahtb.open({ title: 'You Have Encountered an Error during inviteCodes', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  	else {
			  	console.log("inviteCodes: "+data.data);
		  		if (typeof data.data == "string")
		  			ahtb.alert("Invitation code status:"+data.data, {height: 150});
		  		else {
				  	var h = '<div style="diplay: block; height: 280px;">';
				  	h += '<table style="width:280px;" class="scrollTable">';
				  	h += '<thead><tr class="alternate" style="display: block;"><th>Key</th><th>Seller ID</th></tr></thead>';
					h += '<tbody class="scrollContent" style="display: block; width: 100%; overflow: auto; height: 250px">';
					for( var i = 0; i < data.data.length; i++) {
						h += '<tr';
						if ( (i % 2) == 1)
							h += ' class="alternate"';
						var sellers = '';
						if (typeof data.data[i].seller_id != 'undefined' &&
							data.data[i].seller_id != null &&
							data.data[i].seller_id.length) {
							for(var id in data.data[i].seller_id)
								sellers += (sellers.length ? ", " : "") + data.data[i].seller_id[id];
						}
						else
							sellers = "None";
						h += '><td>'+data.data[i].code+'</td><td>'+sellers+'</td></tr>';
					}
					h += '</tbody></table></div></br>';
				  	ahtb.open({title: "Current Invitation Code",
				  			   height: 365,
				  			   width: 320,
				  			   html: h,
				  				opened: function() {
				  					var $table = $('.scrollTable'),
									    $bodyCells = $table.find('tbody tr:first').children(),
									    colWidth;

									// Get the tbody columns width array
									colWidth = $bodyCells.map(function() {
									    return $(this).width();
									}).get();

									// Set the width of thead columns
									$table.find('thead tr').children().each(function(i, v) {
									    $(v).width(colWidth[i]);
									});  
				  				}});
				}
		  	}
		  }
		});
	}

	this.beginEmailDbUpdate = function() {
		var h = '<div id="emailDb">';
			h+= 	'<label id="message">Remaining sellers to process</label>&nbsp;';
			h+= 	'<span id="sellerCount"></span>';
			h+= '</div>';
			ahtb.open({html: h,
					   width: 450,
						height: 150,
						opened: function() {
							var ele = $('#emailDb #sellerCount');
							ele.html(dev.sellerCount.toString());
							dev.doOnePageEmailDb();
						}});
	}

	this.doOnePageEmailDb = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'update-seller-email-db',
					page: dev.emailDbPage,
					pagePer: dev.emailDbPerPage,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during doOnePageEmailDb: "+msg);
				ahtb.alert("Error during doOnePageEmailDb: "+msg, {height: 150});
			},					
		  	success: function(data){
			  	if (data == null || typeof data == 'undefined' || 
			  		data.status == null || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL for doOnePageEmailDb</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.open({ title: 'Update completed', height: 150, html: '<p>Completed :'+dev.emailDbPage+' pages of '+dev.emailDbPerPage+' each for doOnePageEmailDb</p>' });
			  	}
			  	else {
		  			dev.emailDbPage++;
		  			remaining = dev.sellerCount-(dev.emailDbPage*dev.emailDbPerPage);
		  			$('#emailDb #sellerCount').html(remaining.toString());	
		  			dev.doOnePageEmailDb();	  		
	  			}
	  		}
  		})
	}

	this.beginResetImageOrigin = function() {
		var h = '<div id="listingDb">';
			h+= 	'<label id="message">Remaining listings to process</label>&nbsp;';
			h+= 	'<span id="listingCount"></span>';
			h+= '</div>';
			ahtb.open({html: h,
					   width: 450,
						height: 150,
						buttons: [{text:'Cancel', action:function() {
							dev.haltResetImageOrigin = true;
						}}],
						opened: function() {
							var ele = $('#listingDb #listingCount');
							ele.html(dev.activeListingCount.toString());
							dev.doOnePageResetImageOrigin();
						}});
	}

	this.doOnePageResetImageOrigin = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'reset-image-origin',
					page: dev.activeListingPage,
					pagePer: dev.activeListingPerPage,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during doOnePageResetImageOrigin: "+msg);
				ahtb.alert("Error during doOnePageResetImageOrigin: "+msg, {height: 150});
			},					
		  	success: function(data){
			  	if (data == null || typeof data == 'undefined' || 
			  		data.status == null || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL for doOnePageResetImageOrigin</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.open({ title: 'Update completed', height: 150, html: '<p>Completed :'+dev.activeListingPage+' pages of '+dev.activeListingPerPage+' each for doOnePageResetImageOrigin</p>' });
			  	}
			  	else {
			  		if (dev.haltResetImageOrigin) {
			  			var completed = (dev.activeListingPage*dev.activeListingPerPage);
			  			ahtb.alert("Resetting image origins halted after processing "+completed.toString()+" listings", {width: 450, height: 130});
			  			return;
			  		}
		  			dev.activeListingPage++;
		  			remaining = dev.activeListingCount-(dev.activeListingPage*dev.activeListingPerPage);
		  			$('#listingDb #listingCount').html(remaining.toString());	
		  			dev.doOnePageResetImageOrigin();	  		
	  			}
	  		}
  		})
	}

	this.beginUpdateMedianPrices = function() {
		var h = '<div id="citiesDb">';
			h+= 	'<label id="message">Remaining cities to process</label>&nbsp;';
			h+= 	'<span id="citiesCount"></span>';
			h+= '</div>';
			ahtb.open({html: h,
					   width: 450,
						height: 150,
						buttons: [{text:'Cancel', action:function() {
							dev.haltUpdateMedianPrices = true;
						}}],
						opened: function() {
							var ele = $('#citiesDb #citiesCount');
							ele.html(dev.activeCitiesCount.toString());
							dev.doOnePageUpdateMedianPrices();
						}});
	}

	this.doOnePageUpdateMedianPrices = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'update-median-price',
					page: dev.activeCitiesPage,
					pagePer: dev.activeCitiesPerPage,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during doOnePageUpdateMedianPrices: "+msg);
				ahtb.alert("Error during doOnePageUpdateMedianPrices: "+msg, {height: 150});
			},					
		  	success: function(data){
			  	if (data == null || typeof data == 'undefined' || 
			  		data.status == null || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL for doOnePageUpdateMedianPrices</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.open({ title: 'Update completed', height: 150, html: '<p>Completed :'+dev.activeCitiesPage+' pages of '+dev.activeCitiesPerPage+' each for doOnePageUpdateMedianPrices</p>' });
			  	}
			  	else {
			  		if (dev.haltUpdateMedianPrices) {
			  			var completed = (dev.activeCitiesPage*dev.activeCitiesPerPage);
			  			ahtb.alert("Updating median prices halted after processing "+completed.toString()+" cities", {width: 450, height: 130});
			  			return;
			  		}
		  			dev.activeCitiesPage++;
		  			remaining = dev.activeCitiesCount-(dev.activeCitiesPage*dev.activeCitiesPerPage);
		  			$('#citiesDb #citiesCount').html(remaining.toString());	
		  			dev.doOnePageUpdateMedianPrices();	  		
	  			}
	  		}
  		})
	}

	this.beginUpdateMedianPricesZip = function() {
		var h = '<div id="zipDb">';
			h+= 	'<label id="message">Remaining ZIP to process</label>&nbsp;';
			h+= 	'<span id="zipCount"></span><br/>';
			h+=		'<span id="delay"></span>';
			h+= '</div>';
			ahtb.open({html: h,
					   width: 450,
						height: 150,
						buttons: [{text:'Cancel', action:function() {
							dev.haltUpdateMedianPrices = true;
						}}],
						opened: function() {
							var ele = $('#zipDb #zipCount');
							ele.html(dev.activeZipCount.toString());
							dev.doOnePageUpdateMedianPricesZip();
						}});
	}

	this.doOnePageUpdateMedianPricesZip = function() {
		var start = Date.now();
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'update-median-price-from-zip',
					page: dev.activeZipPage,
					pagePer: dev.activeZipPerPage,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during doOnePageUpdateMedianPricesZip: "+msg);
				ahtb.alert("Error during doOnePageUpdateMedianPricesZip: "+msg, {height: 150});
			},					
		  	success: function(data){
		  		var end = Date.now();
		  		var timeTookMsec = end-start;
				var msg = "Time to process "+dev.activeZipPerPage+" zip codes was "+(timeTookMsec/1000)+" secs, used "+data.data.api_count+" api calls";
				console.log(msg);
				// ahtb.alert(msg, {width: 450, height: 150});
				// return;
			  	if (data == null || typeof data == 'undefined' || 
			  		data.status == null || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL for doOnePageUpdateMedianPricesZip</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.open({ title: 'Update completed', height: 150, html: '<p>Completed :'+dev.activeZipPage+' pages of '+dev.activeZipPerPage+' each for doOnePageUpdateMedianPricesZip</p>' });
			  	}
			  	else {
			  		dev.maxActiveZipApiCalls -= parseInt(data.data.api_count);
			  		if (dev.haltUpdateMedianPrices) {
			  			var completed = (dev.activeZipPage*dev.activeZipPerPage);
			  			ahtb.alert("Updating median prices halted after processing "+completed.toString()+" cities", {width: 450, height: 130});
			  			return;
			  		}

			  		dev.activeZipPage++;
			  		if (dev.maxActiveZipApiCalls <= 0) {
			  			var completed = (dev.activeZipPage*dev.activeZipPerPage);
			  			ahtb.alert("Updating median prices used all API calls after processing "+completed.toString()+" cities", {width: 450, height: 130});
			  			return;
			  		}
		  			
		  			remaining = dev.activeZipCount-(dev.activeZipPage*dev.activeZipPerPage);
		  			$('#zipDb #zipCount').html(remaining.toString());	
		  			// var waitForMsec = (dev.activeZipPerPage*60000)/65;
		  			var waitForMsec = data.data.api_count * 334; // third sec per api call
		  			if (timeTookMsec >= waitForMsec) {
		  				$('#zipDb #delay').html('');
		  				dev.doOnePageUpdateMedianPricesZip(); // continue w/o delay
		  			}
		  			else {
			  			$('#zipDb #delay').html('Delaying next batch of '+dev.activeZipPerPage+" for "+((waitForMsec-timeTookMsec)/1000).toFixed(3)+" secs");
		  				window.setTimeout(function() {
			  				dev.doOnePageUpdateMedianPricesZip();
			  			}, (waitForMsec-timeTookMsec)); 	
			  		}	
	  			}
	  		}
  		})
	}

	this.beginSettingFirstImage = function() {
		var h = '<div id="firstImageReset">';
			h+= 	'<label id="message">Remaining listings to process</label>&nbsp;';
			h+= 	'<span id="listingCount"></span><br/>';
			h+= '</div>';
			ahtb.open({html: h,
					   width: 450,
						height: 150,
						buttons: [{text:'Cancel', action:function() {
							dev.haltSetFirstImage = true;
						}}],
						opened: function() {
							var ele = $('#firstImageReset #listingCount');
							ele.html(dev.activeListingCount.toString());
							dev.doOnePageSetFirstImage();
						}});
	}

	this.doOnePageSetFirstImage = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'set-first-image',
					page: dev.activeListingPage,
					pagePer: dev.activeListingPerPage,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during doOnePageSetFirstImage: "+msg);
				ahtb.alert("Error during doOnePageSetFirstImage: "+msg, {height: 150});
			},					
		  	success: function(data){
			  	if (data == null || typeof data == 'undefined' || 
			  		data.status == null || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL for doOnePageSetFirstImage</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.open({ title: 'Update completed', height: 150, html: '<p>Completed :'+dev.activeListingPage+' pages of '+dev.activeListingPerPage+' each for doOnePageSetFirstImage</p>' });
			  	}
			  	else {
			  		if (dev.haltSetFirstImage) {
			  			var completed = (dev.activeListingPage*dev.activeListingPerPage);
			  			ahtb.alert("Setting first image halted after processing "+completed.toString()+" listings", {width: 450, height: 130});
			  			return;
			  		}
		  			dev.activeListingPage++;
		  			remaining = dev.activeListingCount-(dev.activeListingPage*dev.activeListingPerPage);
		  			$('#firstImageReset #listingCount').html(remaining.toString());	
		  			dev.doOnePageSetFirstImage();	  		
	  			}
	  		}
  		})
	}

	this.reallyBeginCleaningOutImageFolder = function() {
		var h = '<div id="cleanImageFolder">';
			h+= 	'<label id="message">Remaining image files in ImageTracker:</label>&nbsp;';
			h+= 	'<span id="imageCount"></span><br/>';
			h+= 	'<label id="message">Remaining listings to process:</label>&nbsp;';
			h+= 	'<span id="listingCount"></span>';
			h+= '</div>';
			ahtb.open({html: h,
					   width: 450,
						height: 150,
						buttons: [{text:'Cancel', action:function() {
							dev.haltImageCleaning = true;
						}}],
						opened: function() {
							var ele = $('#cleanImageFolder #imageCount');
							ele.html(dev.totalImagesInFolder.toString());
							$('#cleanImageFolder #listingCount').html(dev.totalListingsWithImages.toString());
							dev.doOnePageCleanImageFolder();
						}});
	}

	this.askBeginRemovingOutImageFolder = function() {
		var h = "<p>Do you want to begin removing "+dev.totalImagesLeftInFolder+" images?</p>";
		ahtb.open({title:"Begin Image Removing", 
				   width: 500,
				   height: 150,
				   html: h,
				   buttons:[{text:'OK', action: function() {
				   		dev.beginRemovingImagesFromFolder();
				   }},
				   {text:'Cancel', action: function() {
				   		ahtb.close();
				   }}]})
	}

	this.askBeginCleaningOutImageFolder = function() {
		var h = "<p>Do you want to begin cleaning "+dev.totalImagesInFolder+" images?</p>";
		ahtb.open({title:"Begin Image Cleaning", 
				   width: 500,
				   height: 150,
				   html: h,
				   buttons:[{text:'OK', action: function() {
				   		dev.beginCleaningOutImageFolder({countImages: dev.totalImagesInFolder,
				   										 countListings: dev.totalListingsWithImages});
				   }},
				   {text:'Cancel', action: function() {
				   		ahtb.close();
				   }}]})
	}

	this.beginCleaningOutImageFolder = function(totals) {
		dev.totalImagesInFolder = dev.totalImagesLeftInFolder = totals.countImages;
		dev.totalListingsWithImages = dev.listingsLeftToProcess = totals.countListings;
		dev.haltImageCleaning = false;
		dev.imageCleanFolderPage = 0;
		var h = "<p>Do you want to begin processing "+totals.countImages+" images?</p>";
		ahtb.open({title:"Image Prep Completed", 
				   width: 500,
				   height: 150,
				   html: h,
				   buttons:[{text:'OK', action: function() {
				   		dev.reallyBeginCleaningOutImageFolder();
				   }},
				   {text:'Cancel', action: function() {
				   		$("button#begin-image-cleaning").show();
				   		ahtb.close();
				   }}]})		
	}

	this.doOnePageCleanImageFolder = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'clean-image-folder',
					page: dev.imageCleanFolderPage,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during doOnePageCleanImageFolder: "+msg);
				ahtb.alert("Error during doOnePageCleanImageFolder: "+msg, {height: 150});
			},					
		  	success: function(data){
			  	if (data == null || typeof data == 'undefined' || 
			  		data.status == null || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL for doOnePageCleanImageFolder</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.open({ title: 'Errored out', height: 150, html: '<p>Completed so far:'+dev.imageCleanFolderPage+' pages of '+dev.imageCleanFolderPerPage+' each for doOnePageCleanImageFolder</p>' });
			  	}
			  	else {
			  		if (dev.haltImageCleaning) {
			  			var completed = (dev.imageCleanFolderPage*dev.imageCleanFolderPerPage);
			  			ahtb.alert("Cleaning image folder halted after processing "+completed.toString()+" listings", {width: 450, height: 130});
			  			return;
			  		}
			  		data = data.data;
		  			if (data.status == 'green') {
		  				dev.imageCleanFolderPage++;
		  				if (data.operation == LogState.CLEAN_IMAGE_REMOVE_IT)
		  					dev.totalImagesLeftInFolder  -=  (data.removed > dev.totalImagesLeftInFolder ? dev.totalImagesLeftInFolder : data.removed);
		  				else
		  					dev.totalImagesLeftInFolder  -=  (data.marked > dev.totalImagesLeftInFolder ? dev.totalImagesLeftInFolder : data.marked);
		  				dev.listingsLeftToProcess -= data.listings;
			  			console.log("doOnePageCleanImageFolder - reported removed:"+data.removed+", marked:"+data.marked+", errored:"+data.errored);
			  			$('#cleanImageFolder #imageCount').html(dev.totalImagesLeftInFolder.toString());	
			  			$('#cleanImageFolder #listingCount').html(dev.listingsLeftToProcess.toString());	
		  				dev.doOnePageCleanImageFolder(); // do another page
		  			}
		  			else {
		  				console.log("doOnePageCleanImageFolder - totalLeft in ImageTracker:"+data.totalLeft);
		  				if (data.operation == LogState.CLEAN_IMAGE_MARK_IT) {
		  					var h = '<p>Processed '+dev.totalListingsWithImages+' listings, with '+(dev.totalImagesInFolder - dev.totalImagesLeftInFolder)+' good images in ImageTracker. ';
		  					if (dev.totalImagesLeftInFolder)
		  						h+= 'Do you want to remove '+(dev.totalImagesLeftInFolder)+' images from the Image Folder?</p>';
		  					else
		  						h+= 'No images to clean.';
		  					ahtb.open({title:"Completed Processing", width: 500, height: 180, html:h,
		  							   buttons:[{text:'OK', action:function() {
			  							   	if (dev.totalImagesLeftInFolder)
			  							   		dev.reallyBeginRemovingImagesFromFolder();
			  							   	else
			  							   		ahtb.close();
		  							   }},
		  							   {text:'Cancel', action: function() {
		  							   		$("button#begin-image-cleaning").hide();
		  							   		if (dev.totalImagesLeftInFolder)
		  							   			$("button#begin-image-removing").show();
		  							   		else
		  							   			$("button#begin-image-removing").hide();
		  							   		ahtb.close();
		  							   }}]});
		  				}
		  				else
		  					ahtb.open({title: "Completed Processing", width: 500, height: 150, html:'<p>Processed '+dev.totalListingsWithImages+' listings, with '+data.totalLeft+' good images in Image Folder'});
		  			} 		
	  			}
	  		}
  		})
	}

	this.reallyBeginRemovingImagesFromFolder = function() {
		dev.totalImagesToRemovedFromFolder = dev.totalImagesLeftInFolder;
		dev.haltImageCleaning = false;
		dev.imageCleanFolderPage = 0;

		var h = '<div id="cleanImageFolder">';
			h+= 	'<label id="message">Remaining image files in ImageTracker:</label>&nbsp;';
			h+= 	'<span id="imageCount"></span><br/>';
			h+= '</div>';
			ahtb.open({html: h,
					   width: 450,
						height: 150,
						buttons: [{text:'Cancel', action:function() {
							dev.haltImageCleaning = true;
						}}],
						opened: function() {
							var ele = $('#cleanImageFolder #imageCount');
							ele.html(dev.totalImagesLeftInFolder.toString());
							dev.doOnePageRemoveImagesFromFolder();
						}});
	}

	this.beginRemovingImagesFromFolder = function() {
		dev.totalImagesToRemovedFromFolder = dev.totalImagesLeftInFolder;
		dev.haltImageCleaning = false;
		dev.imageCleanFolderPage = 0;

		var h = "<p>Do you want to begin removing "+dev.totalImagesToRemovedFromFolder+" images?</p>";
		ahtb.open({title:"Are you sure!?", 
				   width: 500,
				   height: 150,
				   html: h,
				   buttons:[{text:'OK', action: function() {
				   		dev.reallyBeginRemovingImagesFromFolder();
				   }},
				   {text:'Cancel', action: function() {
				   		$("button#begin-image-cleaning").show();
				   		ahtb.close();
				   }}]})		
	}

	this.doOnePageRemoveImagesFromFolder = function() {
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'remove-images-from-folder',
					page: dev.imageCleanFolderPage,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during doOnePageRemoveImagesFromFolder: "+msg);
				ahtb.alert("Error during doOnePageRemoveImagesFromFolder: "+msg, {height: 150});
			},					
		  	success: function(data){
			  	if (data == null || typeof data == 'undefined' || 
			  		data.status == null || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL for doOnePageRemoveImagesFromFolder</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.open({ title: 'Errored out', height: 150, html: '<p>Completed so far:'+dev.imageCleanFolderPage+' pages of '+dev.imageCleanFolderPerPage+' each for doOnePageRemoveImagesFromFolder</p>' });
			  	}
			  	else {
			  		if (dev.haltImageCleaning) {
			  			var completed = dev.totalImagesLeftInFolder;
			  			ahtb.alert("Cleaning image folder halted after processing "+completed.toString()+" images", {width: 450, height: 130});
			  			return;
			  		}
			  		data = data.data;
		  			if (data.status == 'green') {
		  				dev.imageCleanFolderPage++;
		  				dev.totalImagesLeftInFolder  -=  (data.removed > dev.totalImagesLeftInFolder ? dev.totalImagesLeftInFolder : data.removed);
		  				
			  			console.log("doOnePageRemoveImagesFromFolder - reported removed:"+data.removed+", errored:"+data.errored);
			  			$('#cleanImageFolder #imageCount').html(dev.totalImagesLeftInFolder.toString());	
		  				dev.doOnePageRemoveImagesFromFolder(); // do another page
		  			}
		  			else {
		  				console.log("doOnePageRemoveImagesFromFolder - "+data.msg);
		  				ahtb.open({title: "Completed Processing", width: 500, height: 150, html:'<p>Processed '+dev.totalImagesToRemovedFromFolder+' images from the Image Folder'});
		  				$("button#begin-image-removing").hide();
		  			} 		
	  			}
	  		}
  		})
	}

	this.getTotalBlocks = function() {
		dev.gotTotalBlocks = false;
		dev.totalBlocks = 0;
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'get-block-count',
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during getTotalBlocks: "+msg);
				ahtb.alert("Error during getTotalBlocks: "+msg, {height: 150});
				dev.gotTotalBlocks = true;			
			},					
		  	success: function(data){
		  		if (data == null || typeof data == 'undefined' || 
			  		data.status == null || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL for getTotalBlocks</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.alert( typeof data.data == 'undefined' ? "Unknown error during getTotalBlocks" : data.data);
			  	}
			  	else {
			  		dev.totalBlocks = parseInt(data.data);
			  	}
			  	dev.gotTotalBlocks = true;
			}
		});
	}

	this.setChunkStatus = function(id, x, status) {
		var startId = dev.myParallelResults[id].chunkStates[x].startId;
		console.log("setChunkStatus - subDomain:"+id+", chunk:"+x+", startId:"+startId+", status:"+status);
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'set-chunk-status',
					subDomain: id,
					startId: startId,
					status: status,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during setChunkStatus: "+msg);
				ahtb.alert("Error during setChunkStatus: "+msg, {height: 150});
				dev.gotTotalBlocks = true;			
			},					
		  	success: function(data){
		  		if (data == null || typeof data == 'undefined' || 
			  		data.status == null || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL for setChunkStatus</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.alert( typeof data.data == 'undefined' ? "Unknown error during getTotalBlocks" : data.data);
			  	}
			  	else {
			  		var result = parseInt(data.data);
			  		console.log("setChunkStatus - got back:"+result);
			  	}
			}
		});
	}

	this.checkChunkStatus = function(id, x) {
		if (dev.doingMonitor)
			return;

		var startId = dev.myParallelResults[id].chunkStates[x].startId;
		var curState = dev.myParallelResults[id].chunkStates[x].state;
		if (curState == ChunkState.ERROR) {
			dev.log("#E#E#1 - SubDomain: "+id+" at nth chunk:"+x+" with startId of "+dev.myParallelResults[id].chunkStates[x].startId+", is already in ERROR state.");
			return;
		}
		if (dev.myParallelResults[id].chunkStates[x].queryStatus > 0) {
			if (dev.recordChunkChecking) 
				dev.log("#E#E#2 - SubDomain: "+id+" at nth chunk:"+x+" with startId of "+dev.myParallelResults[id].chunkStates[x].startId+", queryStatus > 0.");
			dev.myParallelResults[id].chunkStates[x].queryStatusDenial++;
			return;
		}
		dev.myParallelResults[id].chunkStates[x].queryStatus++;
		$.ajax({
			url: ah_local.tp+'/_admin/ajax_developer.php',
			data: { query: 'check-chunk',
					subDomain: id,
					startId: startId,
					// doIP: dev.doIP(),
					specialOp: dev.doingSpecialParallelOperation,
					mountain: dev.myMountain,
					everest: dev.myEverest },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				console.log("Error during checkChunkStatus: "+msg);
				ahtb.alert("Error during checkChunkStatus: "+msg, {height: 150});
				dev.myParallelResults[id].chunkStates[x].queryStatus = 0;
			},					
		  	success: function(data){
		  		if (data == null || typeof data == 'undefined' || 
			  		data.status == null || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL for checkChunkStatus</p>' });
			  	else if (data.status != 'OK') {
			  		ahtb.alert( typeof data.data == 'undefined' ? "Unknown error during checkChunkStatus" : data.data);
			  	}
			  	else {
			  		var status = typeof data.data == 'object' || Array.isArray(data.data) ? parseInt(data.data.status) : parseInt(data.data);
			  		var added = new Date(data.data.added);
			  		addedTime = added.getTime();
			  		var curDate = new Date();
			  		var curTime = curDate.getTime();
			  		var msg = '';
			  		var isError = false;
			  		if (dev.recordChunkChecking) {
				  		msg = "checkChunkStatus - curState:"+chunkStateStr(curState)+", chunkId:"+data.data.id+", status:"+status+", subdomain:"+data.data.subdomain+", startid:"+data.data.startid;
				  		dev.log(msg);
				  		console.log(msg);
				  	}
			  		var postCurState = dev.myParallelResults[id].chunkStates[x].state;

			  		switch(status) {
			  			case 0: // nothing there
			  				if (dev.recordChunkChecking) {
				  				msg = "#*#*#0 checkChunkStatus - SubDomain: "+id+" detected a nth chunk:"+x+" with startId of "+dev.myParallelResults[id].chunkStates[x].startId;
				  				msg+=", was "+chunkStateStr(curState)+", but never "+(curState == ChunkState.ACKED ? 'PROCESSED' : 'ACKED')+" for over "+(curTime - dev.myParallelResults[id].chunkStates[x].sentAt)/1000.0+" secs.  Resetting as VIRGIN at "+curDate.toTimeString()+", with ajaxCount:"+dev.ajaxCount;
				  				dev.log(msg);
				  			}
			  				if (dev.myParallelResults[id].chunkStates[x].state == ChunkState.SENT && // not ACKED
			  					dev.ajaxCount)
			  					dev.ajaxCount--;

			  				var curData = new Date();
							if (dev.ajaxCount == 0)
								dev.lastTimeAjaxCountWentToZero = curData.getTime();

							dev.myParallelResults[id].chunkStates[x].state = ChunkState.VIRGIN;
							dev.myParallelResults[id].chunkStates[x].badProcessing = 0;
							for(var y = dev.myParallelResults[id].chunkStates[x].startId; y <= dev.myParallelResults[id].chunkStates[x].upperLimit; y++)
								dev.parallelList[y][1] = BlockState.VIRGIN; // reset blocks
							break;
						case 1: // processing
							if (dev.recordChunkChecking) {
								msg = "#*#*#1A checkChunkStatus - SubDomain: "+id+" with startId of "+dev.myParallelResults[id].chunkStates[x].startId;
								msg+= ", entered as "+chunkStateStr(curState)+" and is now:"+chunkStateStr(postCurState);
								dev.log(msg);
							}
							if (postCurState == ChunkState.PROCESSING) {
								if ((curTime - dev.myParallelResults[id].chunkStates[x].sentAt) < ((dev.unitsPerChunk * 60000) * (dev.doingSpecialParallelOperation > SpecialOp.CREATE_IMAGES_FROM_IMPORTED ? ImageProcessingDelayPerImageMultiplier : 1)) ) {// that's minute per listing
									dev.myParallelResults[id].chunkStates[x].badProcessing = 0;
									break;
								}
								else
									isError = true;
							}
							else if (postCurState == ChunkState.SENT && // not ACKED
			  						 dev.ajaxCount)
			  					dev.ajaxCount--;

			  				var curData = new Date();
							if (dev.ajaxCount == 0)
								dev.lastTimeAjaxCountWentToZero = curData.getTime();
							if (dev.recordChunkChecking) {
								msg = "#*#*#1B checkChunkStatus - SubDomain: "+id+" detected a nth chunk:"+x+" with startId of "+dev.myParallelResults[id].chunkStates[x].startId;
								msg+= ", entered as "+chunkStateStr(curState)+" and now:"+chunkStateStr(postCurState)+", but didn't get "+(postCurState == ChunkState.ACKED ? 'PROCESSED' : (postCurState == ChunkState.SENT ? "ACKED" : "COMPLETE"))+" for over "+(curTime - dev.myParallelResults[id].chunkStates[x].sentAt)/1000.0+" secs."
								msg+= "  Setting as "+(!isError ? "PROCESSING" : "ERROR")+" at "+curDate.toTimeString()+", with ajaxCount:"+dev.ajaxCount;
				  				dev.log(msg);
				  			}
							if (!isError) {
			  					dev.myParallelResults[id].active++;
			  					dev.myParallelResults[id].sent++;
			  					dev.activeChunks++;
			  					dev.chunksSent++;
			  					// $('#currentActive').html(dev.activeChunks);
			  					$('#currentActive').html(dev.activeChunksCount());
			  				
				  				dev.myParallelResults[id].chunkStates[x].state = ChunkState.PROCESSING
				  				for(var y = dev.myParallelResults[id].chunkStates[x].startId; y <= dev.myParallelResults[id].chunkStates[x].upperLimit; y++)
									dev.parallelList[y][1] = BlockState.PROCESSING; // reset blocks
							}
							else {
								dev.myParallelResults[id].chunkStates[x].state = ChunkState.ERROR;
								for(var y = dev.myParallelResults[id].chunkStates[x].startId; y <= dev.myParallelResults[id].chunkStates[x].upperLimit; y++)
									dev.parallelList[y][1] = BlockState.ERROR; // reset blocks
								dev.chunksCompleted++;
						  		dev.myParallelResults[id].completed++;
						  		// if (dev.activeChunks)
						  			dev.activeChunks--;
						  		dev.myParallelResults[id].active--;	
						  		dev.myParallelResults[id].failed += dev.unitsPerChunk;
						  		dev.setChunkStatus(id,
						  						   x,
						  						   3);
							}
							break;
						case 2: // completed, but just sent, so bypassed PROCESSING stop altogether!
							if (dev.recordChunkChecking) {
								msg = "#*#*#2 checkChunkStatus - SubDomain: "+id+" detected a nth chunk:"+x+" with startId of "+dev.myParallelResults[id].chunkStates[x].startId;
								msg+= ", entered as "+chunkStateStr(curState)+" and now:"+chunkStateStr(postCurState)+", but didn't get COMPLETE for over "+(curTime - dev.myParallelResults[id].chunkStates[x].sentAt)/1000.0+" secs.  Setting as COMPLETE at "+curDate.toTimeString()+", with ajaxCount:"+dev.ajaxCount;
				  				dev.log(msg);
				  			}

							if (dev.myParallelResults[id].chunkStates[x].state != ChunkState.COMPLETE) {
								if (postCurState == ChunkState.SENT && // not ACKED
			  						dev.ajaxCount)
			  						dev.ajaxCount--;

			  					var curData = new Date();
								if (dev.ajaxCount == 0)
									dev.lastTimeAjaxCountWentToZero = curData.getTime();

			  					if (postCurState == ChunkState.VIRGIN ||
			  						postCurState == ChunkState.SENT ||
			  						postCurState == ChunkState.ACKED) {
			  						dev.myParallelResults[id].sent++;
			  						if (dev.recordChunkChecking) {
				  						msg = "#*#*#3 checkChunkStatus - SubDomain: "+id+", nth chunk:"+x+" with startId of "+dev.myParallelResults[id].chunkStates[x].startId;
										msg+= ", now:"+chunkStateStr(postCurState)+", is incrementing sent";
					  					dev.log(msg);
					  				}
			  					}
			  					else if (postCurState == ChunkState.PROCESSING) { // cursState == ChunkState.PROCESSING
		  							dev.activeChunks--;
			  						dev.myParallelResults[id].active--;	
			  					}
			  					dev.chunksCompleted++;
			  					dev.myParallelResults[id].completed++;
			  				}
			  				dev.myParallelResults[id].chunkStates[x].badProcessing = 0;
			  				dev.myParallelResults[id].chunkStates[x].state = ChunkState.COMPLETE; // mark completed
			  				for(var y = dev.myParallelResults[id].chunkStates[x].startId; y <= dev.myParallelResults[id].chunkStates[x].upperLimit; y++)
								dev.parallelList[y][1] = BlockState.COMPLETE; // reset blocks
							break;
						default:
							msg = "#*#*#X checkChunkStatus - SubDomain: "+id+" detected a nth chunk:"+x+" with startId of "+dev.myParallelResults[id].chunkStates[x].startId;
							msg+= " got back a mystery status value:"+status;
							dev.log(msg);
							break;
			  		}
			  		dev.myParallelResults[id].chunkStates[x].queryStatus = 0;
			  	}
		  	}
		});
	}


	this.setExtraInfo = function(msg) {
		// var val = $('#legend-div #extra-info').val();
	 //  	$('#legend-div #extra-info').val(val+'<p>'+JSON.parse(data.data.data)+'</p>');	
		$('#legend-div #extra-info').append(msg);
	}

	this.errorHandler = function(e) {
	  var msg = e.name+': '+e.message;

	  console.log('Error: ' + msg);
	}

	this.implode = function(glue, pieces) {
	  //  discuss at: http://phpjs.org/functions/implode/
	  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // improved by: Waldo Malqui Silva
	  // improved by: Itsacon (http://www.itsacon.net/)
	  // bugfixed by: Brett Zamir (http://brett-zamir.me)
	  //   example 1: implode(' ', ['Kevin', 'van', 'Zonneveld']);
	  //   returns 1: 'Kevin van Zonneveld'
	  //   example 2: implode(' ', {first:'Kevin', last: 'van Zonneveld'});
	  //   returns 2: 'Kevin van Zonneveld'

	  var i = '',
	    retVal = '',
	    tGlue = '';
	  if (arguments.length === 1) {
	    pieces = glue;
	    glue = '';
	  }
	  if (typeof pieces === 'object') {
	    if (Object.prototype.toString.call(pieces) === '[object Array]') {
	      return pieces.join(glue);
	    }
	    for (i in pieces) {
	      retVal += tGlue + pieces[i];
	      tGlue = glue;
	    }
	    return retVal;
	  }
	  return pieces;
	}

}
dev.init();
});
