jQuery(document).ready(function($){ 
	editor = new function() {
		this.currentOption = $('div#option-type select.selection-list').find(":selected").attr("value");
		this.currentAgent = parseInt($('div#agent-list select.selection-list').find(":selected").attr("value"));
		this.newAgent = $('select.new-agent-list').find(":selected").attr("value");
		this.createNew = false;
		this.changed = false;
		this.nextOption = '';
		this.editOption = null;

		this.init = function() {
			$('div#option-type select.selection-list').on('change', function() {
				var value = $('div#option-type select.selection-list').find(":selected").attr("value");
				console.log("option-type has a new value:"+value);
				editor.setNewOption(value, -2);
			})

			$('div#agent-list select.selection-list').on('change', function() {
				var value = $('div#agent-list select.selection-list').find(":selected").attr("value");
				console.log("agent-list has a new value:"+value);
				editor.currentAgent = parseInt(value);
				editor.setNewOption( editor.currentOption, editor.currentAgent);
			})

			$('div#makeNew input#doNew').on('click', function(){
				var checked = $(this).prop('checked');
				console.log("New is now "+checked);
				if (checked)
					$('div#newAgentList').removeClass('hidden');
				else
					$('div#newAgentList').addClass('hidden');
			})

			$('select.new-agent-list').on('change', function() {
				var value = $(this).find(":selected").attr("value");
				console.log("new agent-list has a new value:"+value);
				editor.newAgent = parseInt(value);
				$('button#makeNew').show();
			})

			$('button#save').prop('disabled', true);
			$('button#save').on('click', function() {
				editor.save(true);
			})

			$('button#makeNew').on('click', function() {
				editor.createNew = true;
				if (!editor.newAgent) {
					editor.newAgent = $('select.new-agent-list').find(":selected").attr("value");
					console.log("button#makeNew set newAgent to "+editor.newAgent);
				}
				editor.setNewOption(editor.currentOption, editor.newAgent);
			})

			editor.buildOption();
		}

		this.setChange = function(mode) {
			editor.changed = mode;
			$('button#save').prop('disabled', !mode);
			//$('button#makeNew').hide();
			if (mode)
				$('div#status span').html("Option values has changed");
		}

		this.verifyHtmlElementsInOptions = function(target, groupId, failedMsg) {
			for(var i in target) {
				if (i == 'agentID'||
					i == 'repeating')
					continue;

				var failedCheck = false;

				if (typeof target['type'] != 'undefined' &&
					typeof target['html'] != 'undefined' &&
					typeof target['id'] != 'undefined') {
					if ((target.type == 'span' ||
						 target.type == 'button') &&
						(typeof target.html == 'undefined' ||
						 target.html.length == 0)) {
						failedCheck = true;
						failedMsg.msg += (failedMsg.msg.length ? ', ' : '')+(typeof groupId != 'undefined' ? 'groupId:'+groupId+' is ' : '')+"missing html data for "+target.type;
					}
					if ( typeof target.id == 'undefined' ||
						 target.id.length == 0) {
						failedCheck = true;
						failedMsg.msg += (failedMsg.msg.length ? ', ' : '')+(typeof groupId != 'undefined' ? 'groupId:'+groupId+' is ' : '')+"missing id data for "+target.type;
					}
					if (target.type == 'img' &&
						(typeof target.src == 'undefined' ||
						 target.src.length == 0)) {
						failedCheck = true;
						failedMsg.msg += (failedMsg.msg.length ? ', ' : '')+(typeof groupId != 'undefined' ? 'groupId:'+groupId+' is ' : '')+"missing src data for "+target.type;
					}
					if (target.type == 'a' &&
						(typeof target.href == 'undefined' ||
						 target.href.length == 0)) {
						failedCheck = true;
						failedMsg.msg += (failedMsg.msg.length ? ', ' : '')+(typeof groupId != 'undefined' ? 'groupId:'+groupId+' is ' : '')+"missing href data for "+target.type;
					}

					return failedCheck;
				}
				else if (typeof target[i] == 'object') {
					var tmpTarget = target[i];
					var tmpGroupId = typeof groupId == 'undefined' ? i : groupId+'-'+i;
					failedCheck = editor.verifyHtmlElementsInOptions(tmpTarget, tmpGroupId, failedMsg);
				}

				if (failedCheck)
					return failedCheck;
			}
			return failedCheck;
		}

		this.cleanUpHtmlOptions = function(target) {
			for(var i in target) {
				if (i == 'agentID' ||
					i == 'repeating')
					continue;

				if (typeof target['type'] != 'undefined' &&
					typeof target['html'] != 'undefined' &&
					typeof target['id'] != 'undefined') {
					// title, id, html
					if (target.type == 'span' ||
						target.type == 'button') {
						if (typeof target.src != 'undefined')
							delete target.src;
						if (typeof target.href != 'undefined')
							delete target.href;
						if (typeof target.placeholder != 'undefined')
							delete target.placeholder;
						if (typeof target.ischecked != 'undefined')
							delete target.ischecked;
						if (target.type == 'span')
							if (typeof target.onclick != 'undefined')
								delete target.onclick;
					}
					if (target.type == 'img' ) {
						if (typeof target.href != 'undefined')
							delete target.href;
						if (typeof target.placeholder != 'undefined')
							delete target.placeholder;
						if (typeof target.onclick != 'undefined')
							delete target.onclick;
						if (typeof target.ischecked != 'undefined')
							delete target.ischecked;
					}
					if (target.type == 'a' ) {
						if (typeof target.src != 'undefined')
							delete target.src;
						if (typeof target.placeholder != 'undefined')
							delete target.placeholder;
						if (typeof target.onclick != 'undefined')
							delete target.onclick;
						if (typeof target.ischecked != 'undefined')
							delete target.ischecked;
					}
					if (target.type == 'input' ) {
						if (typeof target.src != 'undefined')
							delete target.src;
						if (typeof target.href != 'undefined')
							delete target.href;
						if (typeof target.onclick != 'undefined')
							delete target.onclick;
						if (typeof target.ischecked != 'undefined')
							delete target.ischecked;
					}
					if (target.type == 'checkbox' ) {
						if (typeof target.src != 'undefined')
							delete target.src;
						if (typeof target.href != 'undefined')
							delete target.href;
						if (typeof target.onclick != 'undefined')
							delete target.onclick;
						if (typeof target.placeholder != 'undefined')
							delete target.placeholder;
					}
				}
				else if (typeof target[i] == 'object') {
					var tmpTarget = target[i];
					editor.cleanUpHtmlOptions(tmpTarget);
				}
			}
		}

		this.save = function(fromButton) {
			var failedCheck = false;
			var target = editor.editOption;
			var error = {msg: ''};

			switch(editor.currentOption) {
				case 'QuizResultsPortalUserCaptureOptions':
					for(var i in editor.editOption) {
						if (i == 'agentID' ||
							i == 'byPassCount' ||
							i == 'forceHardStopOnViewAllListingAtQuizLoad' ||
							i == 'informUserNotFullyRegisteredUserAfterSignUp')
							continue;
						
						if (typeof editor.editOption[i].captureMode == 'undefined') {
							// delete it, as it wasn't defined anyway
							delete editor.editOption[i];
							continue;
						}
						if (i.indexOf('LeadIn') != -1) {
							if (editor.editOption[i].captureMode) {
								if (typeof editor.editOption[i].title == 'undefined' ||
									editor.editOption[i].title.length == 0) {
									failedCheck = true;
									failedMsg += (failedMsg.length ? ', ' : '')+i+" is missing title data";
								}
								if (typeof editor.editOption[i].message == 'undefined' ||
									editor.editOption[i].message.length == 0) {
									failedCheck = true;
									failedMsg += (failedMsg.length ? ', ' : '')+i+" is missing message data";
								}
							}
						}
					}
					break;
				case 'PortalLandingStrings':
				case 'PredefinedQuizList':
				case 'PredefinedQuizListHome':
				case 'SellerLeadCaptureOption':
					failedCheck = editor.verifyHtmlElementsInOptions(target, undefined, error);
					// for(var i in editor.editOption) {
					// 	if (i == 'agentID'||
					// 		i == 'repeating')
					// 		continue;
					// 	if ((editor.editOption[i].type == 'span' ||
					// 		 editor.editOption[i].type == 'button') &&
					// 		(typeof editor.editOption[i].html == 'undefined' ||
					// 		 editor.editOption[i].html.length == 0)) {
					// 		failedCheck = true;
					// 		failedMsg += (failedMsg.length ? ', ' : '')+i+" is missing html data for "+editor.editOption[i].type;
					// 	}
					// 	if ( typeof editor.editOption[i].id == 'undefined' ||
					// 		 editor.editOption[i].id.length == 0) {
					// 		failedCheck = true;
					// 		failedMsg += (failedMsg.length ? ', ' : '')+i+" is missing id data for "+editor.editOption[i].type;
					// 	}
					// 	if (editor.editOption[i].type == 'img' &&
					// 		(typeof editor.editOption[i].src == 'undefined' ||
					// 		 editor.editOption[i].src.length == 0)) {
					// 		failedCheck = true;
					// 		failedMsg += (failedMsg.length ? ', ' : '')+i+" is missing src data for "+editor.editOption[i].type;
					// 	}
					// 	if (editor.editOption[i].type == 'a' &&
					// 		(typeof editor.editOption[i].href == 'undefined' ||
					// 		 editor.editOption[i].href.length == 0)) {
					// 		failedCheck = true;
					// 		failedMsg += (failedMsg.length ? ', ' : '')+i+" is missing href data for anchor";
					// 	}
					// }
					if (!failedCheck) {
						// clean up objects to hold information pertinent to element type
						target = editor.editOption;
						editor.cleanUpHtmlOptions(target);
						// for(var i in editor.editOption) {
						// 	if (i == 'agentID' ||
						// 		i == 'repeating')
						// 		continue;
						// 	// title, id, html
						// 	if (editor.editOption[i].type == 'span' ||
						// 	 	editor.editOption[i].type == 'button') {
						// 		if (typeof editor.editOption[i].src != 'undefined')
						// 			delete editor.editOption[i].src;
						// 		if (typeof editor.editOption[i].href != 'undefined')
						// 			delete editor.editOption[i].href;
						// 		if (typeof editor.editOption[i].placeholder != 'undefined')
						// 			delete editor.editOption[i].placeholder;
						// 		if (editor.editOption[i].type == 'span')
						// 			if (typeof editor.editOption[i].onclick != 'undefined')
						// 				delete editor.editOption[i].onclick;
						// 	}
						// 	if (editor.editOption[i].type == 'img' ) {
						// 		if (typeof editor.editOption[i].href != 'undefined')
						// 			delete editor.editOption[i].href;
						// 		if (typeof editor.editOption[i].placeholder != 'undefined')
						// 			delete editor.editOption[i].placeholder;
						// 		if (typeof editor.editOption[i].onclick != 'undefined')
						// 			delete editor.editOption[i].onclick;
						// 	}
						// 	if (editor.editOption[i].type == 'a' ) {
						// 		if (typeof editor.editOption[i].src != 'undefined')
						// 			delete editor.editOption[i].src;
						// 		if (typeof editor.editOption[i].placeholder != 'undefined')
						// 			delete editor.editOption[i].placeholder;
						// 		if (typeof editor.editOption[i].onclick != 'undefined')
						// 			delete editor.editOption[i].onclick;
						// 	}
						// 	if (editor.editOption[i].type == 'input' ) {
						// 		if (typeof editor.editOption[i].src != 'undefined')
						// 			delete editor.editOption[i].src;
						// 		if (typeof editor.editOption[i].href != 'undefined')
						// 			delete editor.editOption[i].href;
						// 		if (typeof editor.editOption[i].onclick != 'undefined')
						// 			delete editor.editOption[i].onclick;
						// 	}
						// }
					}
					break;
				case 'PortalLandingSlides':
					if (typeof editor.editOption.desktop == 'undefined' ||
						editor.editOption.desktop.length == 0) {
						failedCheck = true;
						failedMsg = "desktop slides must be defined.";
					}
					if (typeof editor.editOption.mobile == 'undefined' ||
						editor.editOption.mobile.length == 0) {
						failedCheck = true;
						failedMsg += (failedMsg.length ? ', ' : '')+"mobile slides must be defined.";
					}
					break;

			}
			if (failedCheck) {
				ahtb.alert(error.msg);
				return;
			}

			var currentOptions = currentOptionsFromDb[ editor.currentOption ]; // get whatever was in the database
	   		if ( options[ editor.currentOption ]['agentList'] == -1 ) { // then replace what's in the database
	   			currentOptions = editor.editOption;
	   		}
	   		else {
	   			var foundOne = false;
	   			for(var i in currentOptions)
					if (currentOptions[i].agentID == editor.editOption.agentID) {
						currentOptions[i] = editor.editOption;
						foundOne = true;
						break;
					}
				if (!foundOne)
					currentOptions.push(editor.editOption);
	   		}

	   		var agentID = typeof fromButton != 'undefined' && fromButton ? editor.currentAgent : -1;
	   		editor.DBget({query: 'update-option',
	   					  data: {
	   					  	option: currentOptions,
	   					  	name: editor.currentOption
	   					  },
	   					  done:function(d) {
	   					  	console.log(d);
	   					  	$('div#status span').html(d);
	   					  	ahtb.close(function() {
					   			editor.setChange(false);
					   			if (editor.nextOption.length == 0)
					   				editor.updateEditOption();
					   			else
					   				editor.setNewOption(editor.nextOption, editor.currentAgent);
					   		});
	   					  },
	   					  error: function(d){
	   					  	var msg = "Sorry, had an error."
	   					  	if (typeof d == 'string')
	   					  		msg += " "+d;
	   					  	else
	   					  		msg += " "+JSON.stringify(d);
	   					  	ahtb.alert(msg);
	   					  }
	   		})
		}

		this.askSave = function() {
			ahtb.open({html: "<p>Do you want to save the current edits for "+editor.currentOption+"?",
					   width: 600,
					   height: 150,
					   buttons:[
					   	{text:'OK', action:function() {
					   		// assemble object and save it to DB
					   		editor.save();
					   	}},
					   	{text:'Go Back to editing', action: function() {
					   		ahtb.close();
					   		var ele = $('div#option-type select.selection-list option[value='+editor.currentOption+']');
					   		ele.prop('selected', true);
					   	}},
					   	{text:'Abandon all changes', action: function(){
					   		// load this nextOption
					   		editor.setChange(false);
					   		ahtb.close(function() {
					   			editor.setNewOption(editor.nextOption, -2);
					   		})
					   	}}
					   ]})
		}

		// if agentID == -1, reset the div#agent-list select.selection-list
		this.setNewOption = function(option, agentID) {
			if (editor.changed) {
				editor.nextOption = option;
				editor.askSave();
				return;
			}

			editor.nextOption = '';
			editor.currentOption = option;

			if ( options[ editor.currentOption ]['agentList'] != -1 ) {
				$('div#agent-list').show();
				$('div#makeNew').show();
				if (agentID == -2) { // then reset div#agent-list select.selection-list
					var currentOptions = currentOptionsFromDb[ editor.currentOption ]; // get whatever was in the database
					editor.currentAgent = 0;
					for(var i in currentOptions) {
						editor.currentAgent = currentOptions[i].agentID;
						console.log("setNewOption assigned current agent:"+editor.currentAgent);
						break;
					}
				}
			}
			else {
				$('div#agent-list').hide();
				$('div#makeNew').hide();
			}

			if (agentID == -2)
				$('div#status span').html('');

			editor.buildOption();
		}

		this.setOptionalElements = function(id, groupId, element) {
			switch(editor.currentOption) {
				case 'PredefinedQuizList':
				case 'PredefinedQuizListHome':
				case 'PortalLandingStrings':
				case 'SellerLeadCaptureOption':
					if (id == 'type') {
						// hide everything first
						domId = 'div.optional'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']');
						$(domId).each(function() {
							$(this).hide();
						});
						switch(element) {
							case 'span':
								// show html
								domId = 'div[id=html]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']');
								ele = $(domId);
								ele.show();
								break;
							case 'img':
								domId = 'div.optional[id=src]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']');
								ele = $(domId);
								ele.show();
								// hide html
								domId = 'div[id=html]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']');
								ele = $(domId);
								ele.hide();
								break;
							case 'a':
								domId = 'div.optional[id=href]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']');
								ele = $(domId);
								ele.show();
								// show html
								domId = 'div[id=html]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']');
								ele = $(domId);
								ele.show();
								break;
							case 'input':
								domId = 'div.optional[id=placeholder]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']');
								ele = $(domId);
								ele.show();
								// hide html
								domId = 'div[id=html]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']');
								ele = $(domId);
								ele.hide();
								break;
							case 'button':
								domId = 'div.optional[id=onclick]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']');
								ele = $(domId);
								ele.show();
								// show html
								domId = 'div[id=html]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']');
								ele = $(domId);
								ele.show();
								break;
							case 'checkbox':
								domId = 'div.optional[id=ischecked]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']');
								ele = $(domId);
								ele.show();
								// show html
								domId = 'div[id=html]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']');
								ele = $(domId);
								ele.show();
								break;
						}
					}
					break;
					
			}
		}

		this.handleHideShowPredefinedQuizList = function(id, groupId, value, msg) {
			switch(id) {
				case 'quizMode':
					if (value == 0) {
						$('div[id=quizID]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).show();
						$('div[id=quizSEO]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).hide();
					}
					else {
						$('div[id=quizID]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).hide();
						$('div[id=quizSEO]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).show();
					}
					break;
				case 'slideSize':
					if (value != 'custom') {
						$('div[id=width]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).hide();
						$('div[id=height]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).hide();
					}
					else {
						$('div[id=width]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).show();
						$('div[id=height]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).show();
						if (typeof msg != 'undefined')
							ahtb.alert(msg);
					}
					break;
				case 'slideSizeDialog':
					if (value != 'custom') {
						$('div[id=widthDialog]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).hide();
						$('div[id=heightDialog]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).hide();
					}
					else {
						$('div[id=widthDialog]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).show();
						$('div[id=heightDialog]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).show();
						if (typeof msg != 'undefined')
							ahtb.alert(msg);
					}
					break;
				case 'slideSizeOnPage':
					if (value != 'custom') {
						$('div[id=widthOnPage]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).hide();
						$('div[id=heightOnPage]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).hide();
					}
					else {
						$('div[id=widthOnPage]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).show();
						$('div[id=heightOnPage]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')).show();
						if (typeof msg != 'undefined')
							ahtb.alert(msg);
					}
					break;
			}
		}

		this.assignValues = function(structure, groupId, defaultValues) {
			for(var i in structure) {
				if (i == 'agentID' ||
					i == 'repeating')
					continue;
				switch(typeof structure[i]) {
					case 'string':
					case 'number':
						var origIndex = i;
						var ele = '';
						if (i.indexOf('optional_') != -1) {
							pos = i.indexOf('_') + 1;
							i = i.substring(pos, i.length);
						}
						if (structure[i] == 'textarea')
							ele = $('textarea[id='+i+']'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']'));
						else
							ele = $('input[id='+i+']'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']'));

						if ( typeof groupId == 'undefined' ) {
							if ( typeof editor.editOption[i] == 'undefined' ) {
								editor.editOption[i] = defaultValues[i];
								editor.setChange(true);
							}
							ele.val( editor.editOption[i]);
						}
						else {
							var groups = groupId.split('-');
							var target = editor.editOption;
							var baseDefaultValues = defaultValues;
							var haveBase = true;
							for(var x in groups) {
								if (typeof baseDefaultValues[groups[x]] != 'undefined')
									baseDefaultValues = baseDefaultValues[groups[x]];
								else
									haveBase = false;
								if (typeof target[groups[x]] == 'undefined') {
									target[groups[x]] = {};
									editor.setChange(true);
								}
								target = target[groups[x]];
							}
							if (typeof target[i] == 'undefined') {
								target[i] = haveBase ? baseDefaultValues[i] : (typeof structure[i] == 'string' ? '' : 0);
								if ( origIndex == i )
									editor.setChange(true);
								else {
									switch(target['type']) {
										case 'img':
											if (i == 'src') editor.setChange(true); break;
										case 'a':
											if (i == 'href') editor.setChange(true); break;
										case 'input':
											if (i == 'placeholder') editor.setChange(true); break;
										case 'button':
											if (i == 'onclick') editor.setChange(true); break;
										case 'checkbox':
											if (i == 'ischecked') editor.setChange(true); break;
										case 'span':
											break;
										default:
											ahtb.alert("Html type:"+target['type']+' is not handled properly.');
											break;
									}
								}
							}
							var value = typeof target[i] == 'number' ? target[i].toString() : target[i];
							ele.val( value );
						}
						break;
					case 'object': // treat as array
						if ( Array.isArray(structure[i]) ) {
							var iterator = structure[i].keys();
							if (structure[i][iterator.next().value] == 'csv_strings') {
								var ele = $('input[id='+i+']'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']'));
								var strings = null;
								if ( typeof groupId == 'undefined' ) {
									if ( typeof editor.editOption[i] == 'undefined' ) {
										editor.editOption[i] = defaultValues[i];
										editor.setChange(true);
									}
									strings = editor.editOption[i];
								}
								else {
									var groups = groupId.split('-');
									var target = editor.editOption;
									var baseDefaultValues = defaultValues;
									for(var x in groups) {
										baseDefaultValues = baseDefaultValues[groups[x]];
										if (typeof target[groups[x]] == 'undefined') {
											target[groups[x]] = {};
											editor.setChange(true);
										}
										target = target[groups[x]];
									}
									if (typeof target[i] == 'undefined') {
										target[i] = baseDefaultValues[i];
										editor.setChange(true);
									}
									strings = target[i];
								}
								var val = '';
								for(var j in strings)
									val += (val.length ? ',' : '')+strings[j];
								ele.val(val);
							}
							else {
								var val = null;
								var origIndex = i;
								if (i.indexOf('optional_') != -1) {
									pos = i.indexOf('_') + 1;
									i = i.substring(pos, i.length);
								}
								if ( typeof groupId == 'undefined' ) {
									if ( typeof editor.editOption[i] == 'undefined' )
										editor.editOption[i] = defaultValues[i];
									val = editor.editOption[i];
								}
								else {
									var groups = groupId.split('-');
									var target = editor.editOption;
									var baseDefaultValues = defaultValues;
									for(var x in groups) {
										baseDefaultValues = baseDefaultValues[groups[x]];
										if (typeof target[groups[x]] == 'undefined') {
											target[groups[x]] = {};
											editor.setChange(true);
										}
										target = target[groups[x]];
									}
									if (typeof target[i] == 'undefined') {
										target[i] = baseDefaultValues[i];
										if ( origIndex == i )
											editor.setChange(true);
										else {
											switch(target['type']) {
												case 'img':
													if (i == 'src') editor.setChange(true); break;
												case 'a':
													if (i == 'href') editor.setChange(true); break;
												case 'input':
													if (i == 'placeholder') editor.setChange(true); break;
												case 'button':
													if (i == 'onclick') editor.setChange(true); break;
												case 'checkbox':
													if (i == 'ischecked') editor.setChange(true); break;
												case 'span':
													break;
												default:
													ahtb.alert("Html type:"+target['type']+' is not handled properly.');
													break;
											}
										}
									}
									val = target[i];
								}
								if (typeof val == 'boolean')
									val = val ? 'true' : 'false';

								if (editor.currentOption.indexOf('PredefinedQuizList') != -1) {
									switch(i) {
										case 'type':
											editor.setOptionalElements(i, groupId, val);
											break;
										case 'quizMode':
										case 'slideSize':
										case 'slideSizeDialog':
										case 'slideSizeOnPage':
											editor.handleHideShowPredefinedQuizList(i, groupId, val);
											break;
										case 'tag': // remap string to tag id (number) value for the option
											val = activityList[val];
											if (typeof target['icon'] == 'undefined' ||
												target['icon'].length == 0) {
												target['icon'] = activitySlides[val].icon;
												var icon = $('input[id=icon]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']'));
												icon.val( activitySlides[val].icon );
											}
											break;
									}
								}

								var domId = 'select[id='+i+']'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')+' option[value='+val+']';
								var ele = $(domId);
								ele.prop("selected", true);

								console.log("assignValues(array) - id:"+i+", groupId:"+groupId+", value:"+val);

								editor.setOptionalElements(i, groupId, val);
							}
						}
						else {
							var keys = Object.keys(structure[i]);
							if (keys.indexOf('isOption') != -1) {
								var val = null;
								var origIndex = i;
								if (i.indexOf('optional_') != -1) {
									pos = i.indexOf('_') + 1;
									i = i.substring(pos, i.length);
								}
								if ( typeof groupId == 'undefined' ) {
									if ( typeof editor.editOption[i] == 'undefined' ) {
										editor.editOption[i] = defaultValues[i];
										editor.setChange(true);
									}
									val = editor.editOption[i];
								}
								else {
									var groups = groupId.split('-');
									var target = editor.editOption;
									var baseDefaultValues = defaultValues;
									for(var x in groups) {
										baseDefaultValues = baseDefaultValues[groups[x]];
										if (typeof target[groups[x]] == 'undefined') {
											target[groups[x]] = {};
											editor.setChange(true);
										}
										target = target[groups[x]];
									}
									if (typeof target[i] == 'undefined') {
										target[i] = baseDefaultValues[i];
										if ( origIndex == i )
											editor.setChange(true);
										else {
											switch(target['type']) {
												case 'img':
													if (i == 'src') editor.setChange(true); break;
												case 'a':
													if (i == 'href') editor.setChange(true); break;
												case 'input':
													if (i == 'placeholder') editor.setChange(true); break;
												case 'button':
													if (i == 'onclick') editor.setChange(true); break;
												case 'checkbox':
													if (i == 'ischecked') editor.setChange(true); break;
												case 'span':
													break;
												default:
													ahtb.alert("Html type:"+target['type']+' is not handled properly.');
													break;
											}
										}
									}
									val = target[i];
								}
								if (typeof val == 'boolean')
									val = val ? 'true' : 'false';

								if (editor.currentOption.indexOf('PredefinedQuizList') != -1) {
									switch(i) {
										case 'type':
											editor.setOptionalElements(i, groupId, val);
											break;
										case 'quizMode':
										case 'slideSize':
										case 'slideSizeDialog':
										case 'slideSizeOnPage':
											editor.handleHideShowPredefinedQuizList(i, groupId, val);
											break;
										case 'tag': // remap string to tag id (number) value for the option
											if (val.length == 0)
												val = baseDefaultValues[i];
											val = activityList[val];
											if (typeof target['icon'] == 'undefined' ||
												!target['icon'] ||
												target['icon'].length == 0) {
												target['icon'] = activitySlides[val].icon ? activitySlides[val].icon : '';
												var icon = $('input[id=icon]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']'));
												icon.val( activitySlides[val].icon ? activitySlides[val].icon : '');
											}
											break;
									}
								}

								var domId = 'select[id='+i+']'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']')+' option[value='+val+']';
								var ele = $(domId);
								ele.prop("selected", true);

								console.log("assignValues(object) - id:"+i+", groupId:"+groupId+", value:"+val);
								editor.setOptionalElements(i, groupId, val);
							}
							else
								editor.assignValues(structure[i], (typeof groupId == 'undefined' ? '' : groupId+'-')+i, defaultValues);
						}
						break;
				}
			}
		}

		this.buildOption = function() {
			editor.setEditOption();
			editor.processEditOption();
		}

		this.buildStructure = function(target, structure) {
			var didRepeats = false;
			for(var i in target) {
				if (typeof structure.repeating != 'undefined' &&
					!didRepeats) {
					var keys = Object.keys(target);
					var baseKey = 'data0';
					var propLen = keys.length;
					propLen -= typeof structure.agentID != 'undefined' ? 1 : 0;

					// if (propLen > 1) for(var x = 1; x < propLen; x++) {
					if (propLen > 1) for(var x in keys) {
						if (keys[x] == 'agentID')
							continue;

						if (keys[x] == i) {
							if (i != 'data0') {
								structure[keys[x]] = JSON.parse( JSON.stringify(structure[baseKey]) ); // make copy
								baseKey = keys[x];
								delete structure.data0;
							}
							continue;
						}

						// var name = 'data'+x;
						var name = keys[x];
						structure[name] = JSON.parse( JSON.stringify(structure[baseKey]) ); // make copy
						didRepeats = true;
					}
				}

				if (typeof target[i] == 'object') {
					var subTarget = target[i];
					var subStructure = structure[i];
					editor.buildStructure(subTarget, subStructure);
				}
			}
		}

		this.checkToSeeShowFirstSaveButton = function() {
			var optionsEditHeight = $('div#options-edit').innerHeight();
			var screenHeight = $(window).height();
			if ( optionsEditHeight < (screenHeight * 0.80) )
				$('div#save.first').hide();
			else
				$('div#save.first').show();
		}

		// this is deal with creating the html for the current editOption object
		this.processEditOption = function(defaultValues) {
			var structure = JSON.parse( JSON.stringify(options[ editor.currentOption ]['structure']) ); // make a copy
			var target = editor.editOption;
			editor.buildStructure(target, structure);

			// for(var i in target) {
			// 	if (typeof structure.repeating != 'undefined') {
			// 		var propLen = Object.keys(target).length;
			// 		propLen -= typeof structure.agentID != 'undefined' ? 1 : 0;
			// 		if (propLen > 1) for(var x = 1; x < propLen; x++) {
			// 			var name = 'data'+x;
			// 			structure[name] = structure.data0;
			// 		}
			// 	}
			// 	else if (typeof target[i] == 'object') {}
			// }

			var h = editor.getHtml(structure);
			$('div#options-edit').html(h);
			editor.checkToSeeShowFirstSaveButton();
			defaultValues = typeof defaultValues == 'undefined' ? JSON.parse( JSON.stringify(options[ editor.currentOption ]['default']) ) : defaultValues;
			editor.assignValues(structure, undefined, defaultValues);

			$('textarea').on('change', function() {
				var id = $(this).attr('id');
				var what = $(this).attr('what');
				var groupId = $(this).attr('group');
				var val = $(this).val();

				if (typeof groupId == 'undefined')
					editor.editOption[id] = parseInt(val);
				else {
					var groups = groupId.split('-');
					var target = editor.editOption;
					for(var x in groups) {
						if (typeof target[groups[x]] == 'undefined')
							target[groups[x]] = {};
						target = target[groups[x]];
					}
					target[id] = val;
				}
				editor.setChange(true);
			})

			$('input[type=text]').on('change', function() {
				var id = $(this).attr('id');
				var what = $(this).attr('what');
				var groupId = $(this).attr('group');
				var val = $(this).val();
				editor.setChange(true);
				if ( typeof what != 'undefined' ) {
					switch(what) {
						case 'number':
							if (typeof groupId == 'undefined')
								editor.editOption[id] = parseInt(val);
							else {
								var groups = groupId.split('-');
								var target = editor.editOption;
								for(var x in groups) {
									if (typeof target[groups[x]] == 'undefined')
										target[groups[x]] = {};
									target = target[groups[x]];
								}
								target[id] = parseInt(val);
							}
							if (id == 'predefinedQuizSlide' ||
								id == 'activitySlide') {
								ahtb.alert("Save option and reload editor to update image list for tags.");
							}
							break;
						case 'csv':
							var pieces = val.split(',');
							// var csv = '';
							// for(var i in pieces)
							// 	csv += (csv.length ? ',' : '')+'"'+pieces[i]+'"';
							if (typeof groupId == 'undefined')
								editor.editOption[id] = pieces;
							else {
								var groups = groupId.split('-');
								var target = editor.editOption;
								for(var x in groups) {
									if (typeof target[groups[x]] == 'undefined')
										target[groups[x]] = {};
									target = target[groups[x]];
								}
								target[id] = pieces;
							}
							break;
					}
				}
				else if (typeof groupId == 'undefined')
					editor.editOption[id] = val;
				else {
					var groups = groupId.split('-');
					var target = editor.editOption;
					for(var x in groups) {
						if (typeof target[groups[x]] == 'undefined')
							target[groups[x]] = {};
						target = target[groups[x]];
					}
					target[id] = val;
				}
			})

			$('button.removeArticle').on('click', function() {
				var id = $(this).attr('id');
				var groupId = $(this).attr("group");

				if (typeof groupId == 'undefined') {
					delete editor.editOption[id];
					$('article[title='+id+']').remove();
				}
				else {
					var groups = groupId.split('-');
					var target = editor.editOption;
					for(var x in groups) {
						target = target[groups[x]];
					}
					delete target[id];
					$('article[group='+groupId+'-'+id+']').remove();
				}
			})

			$('select.option').on('change', function() {
				var id = $(this).find(":selected").attr('id');
				var value = $(this).find(":selected").attr("value");
				var groupId = $(this).find(":selected").attr("group");
				value = value == 'true' ? true :
						value == 'false' ? false :
						$.isNumeric(value) ? parseInt(value) : value;
				editor.setChange(true);
				console.log("option changed for id:"+id+", group:"+groupId+", value:"+value);
				if (typeof groupId == 'undefined') {
					editor.editOption[id] = value;
					if (editor.currentOption.indexOf('PredefinedQuizList') != -1) {
						switch(id) {
							case 'type':
								editor.setOptionalElements(id, groupId, value);
								break;
							case 'quizMode':
								editor.handleHideShowPredefinedQuizList(id, groupId, value);
								break;
							case 'slideSize':
							case 'slideSizeDialog':
							case 'slideSizeOnPage':
								editor.handleHideShowPredefinedQuizList(id, groupId, value, "Choosing custom size, make sure the images reside in the custom folder");
								break;
						}
					}

					editor.setOptionalElements(id, groupId, value);
				}
				else {
					var groups = groupId.split('-');
					var target = editor.editOption;
					for(var x in groups) {
						if (typeof target[groups[x]] == 'undefined')
							target[groups[x]] = {};
						target = target[groups[x]];
					}

					if (editor.currentOption.indexOf('PredefinedQuizList') == -1)
						target[id] = value;
					else {
						switch(id) {
							case 'type':
								target[id] = value;
								editor.setOptionalElements(id, groupId, value);
								break;
							case 'quizMode':
								target[id] = value;
								editor.handleHideShowPredefinedQuizList(id, groupId, value);
								break;
							case 'slideSize':
							case 'slideSizeDialog':
							case 'slideSizeOnPage':
								target[id] = value;
								editor.handleHideShowPredefinedQuizList(id, groupId, value, "Choosing custom size, make sure the images reside in the custom folder");
								break;
							case 'tag':
								target[id] = activitySlides[value].tag;
								var title = $('input[id=title]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']'));
								title.val( activitySlides[value].title );
								target.title = activitySlides[value].title;
								var img = $('input[id=img]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']'));
								img.val( activitySlides[value].file );
								target.img = activitySlides[value].file;
								var icon = $('input[id=icon]'+(typeof groupId == 'undefined' ? '' : '[group='+groupId+']'));
								icon.val( activitySlides[value].icon );
								target.icon = activitySlides[value].icon;
								break;
						}
					}

					console.log("select.option.onChange() - id:"+id+", groupId:"+groupId+", value:"+value);

					editor.setOptionalElements(id, groupId, value);
				}
			})
		}

		// called from save() upon success to write bak to the currentOptionsFromDb array
		this.updateEditOption = function() {
			var currentOptions = currentOptionsFromDb[ editor.currentOption ]; // get whatever was in the database
			if ( options[ editor.currentOption ]['agentList'] != -1 ) {
					// editor.currentAgent = currentOptions.length ? editor.currentAgent : -1; // if option is empty, make one for everyone
					// makeForAgent = parseInt(editor.currentAgent);
				for(var i in currentOptions)
					if (currentOptions[i].agentID == editor.currentAgent) {
						currentOptions[i] = editor.editOption;
						break;
					}
			}
			else {
				currentOptions = editor.editOption;
			}
		}

		// this will set the current editOption, based on if editor.createnew is true or not, called from buildOption()
		this.setEditOption = function() {
			editor.editOption = null;
			var makeForAgent = 0;
			var agentsWithOptionOwnership = []; // list of agents from existing option from db
			var currentOptions = currentOptionsFromDb[ editor.currentOption ]; // get whatever was in the database
			if (!editor.createNew) {
				// if agentID is not defined, but be either PortalLandingSlides or PortalLandingBxSlider
				if ( options[ editor.currentOption ]['agentList'] != -1 ) {
					editor.currentAgent = currentOptions.length ? editor.currentAgent : -1; // if option is empty, make one for everyone
					makeForAgent = parseInt(editor.currentAgent);
					for(var i in currentOptions)
						if (currentOptions[i].agentID == editor.currentAgent) {
							editor.editOption = currentOptions[i];
							break;
						}

					if (!editor.editOption) {// hey!
						console.log("Failed to find "+editor.currentOption+" for agent:"+editor.currentAgent);
					}
				}
				else {
					// this is a singular Option, with no agentID ownership
					console.log("agentID field not present in "+editor.currentOption);
					editor.editOption = currentOptions;
				}
			}
			else {
				makeForAgent = editor.newAgent;
				agentsWithOptionOwnership.push(makeForAgent);
			}

			if ( options[ editor.currentOption ]['agentList'] != -1 ) {
				$('div#makeNew').show();
				agentsWithOptionOwnership.push(parseInt(editor.currentAgent));
				for(var i in currentOptions)
					if ( agentsWithOptionOwnership.indexOf(currentOptions[i].agentID) == -1)
						agentsWithOptionOwnership.push(currentOptions[i].agentID);

				agentsWithOptionOwnership.sort();

				var selectOptions = '';
				var agents = agentList[2];
				for(var i in agentsWithOptionOwnership) {
					if ( agentsWithOptionOwnership[i] == -1 )
						selectOptions += '<option value="'+agentsWithOptionOwnership[i]+'" '+(agentsWithOptionOwnership[i] == makeForAgent ? 'selected="selected"' : '')+'>'+'Any'+'</option>';
					else {
						if (typeof agents[agentsWithOptionOwnership[i]] != 'undefined')
							selectOptions += '<option value="'+agentsWithOptionOwnership[i]+'" '+(agentsWithOptionOwnership[i] == makeForAgent ? 'selected="selected"' : '')+'>ID:'+agentsWithOptionOwnership[i]+' - '+agents[agentsWithOptionOwnership[i]].first_name+agents[agentsWithOptionOwnership[i]].last_name+'</option>';
						else
							selectOptions += '<option value="'+agentsWithOptionOwnership[i]+'" '+(agentsWithOptionOwnership[i] == makeForAgent ? 'selected="selected"' : '')+'>ID:'+agentsWithOptionOwnership[i]+' - special agent'+'</option>';
					}
				}

				$('div#agent-list select.selection-list').html(selectOptions);
			}
			else
				$('div#makeNew').hide();

			editor.setNewAgentsList(agentsWithOptionOwnership, options[ editor.currentOption ]['agentList']);

			if (editor.editOption == null ||
				 (Array.isArray(editor.editOption) && editor.editOption.length == 0) ||
				 Object.keys(editor.editOption).length == 0 ) { // createNew, or simply didn't match the agent to any existing options
				editor.editOption = JSON.parse( JSON.stringify(options[ editor.currentOption ]['default']) );
				editor.setChange(true);
				if ( options[ editor.currentOption ]['agentList'] != -1 ) {
					editor.editOption.agentID = makeForAgent;
					if (!editor.createNew) {
						ahtb.alert("Setup a default "+editor.currentOption+" for agentID:"+makeForAgent);
						$('button#save').prop('disabled', false);
					}
					else {
						editor.currentAgent = editor.newAgent;
						editor.newAgent = 0;
						editor.createNew = false;
					}
				}
			}
			return true;
		}

		// this sets the div#makeNew select.agent-list so that the correct list of agents who don't yet own the current option is listed
		this.setNewAgentsList = function(agentsWithOptionOwnership, agentListType) {
			if (agentListType == -1) { // then singular ownership
				$('div#agent-list').hide();
				return;
			}

			$('div#agent-list').show();

			var agentsWithoutOwnership = agentList[agentListType];
			var newListOfAgents = JSON.parse( JSON.stringify(agentsWithoutOwnership) ); // make copy
			if (agentListType != 2)
				newListOfAgents['-1'] = {author_id: -1,
										 first_name: 'All',
										 last_name: 'Agents'};// add Any agent option

			for(var i in agentsWithOptionOwnership) {
				if ( newListOfAgents.hasOwnProperty(agentsWithOptionOwnership[i].toString()) )
					delete newListOfAgents[agentsWithOptionOwnership[i].toString()];
			}

			var selectOptions = '';
			var first = true;
			for(var i in newListOfAgents) {
				selectOptions += '<option value="'+newListOfAgents[i].author_id+'" '+(first ? 'selected="selected"' : '')+' >ID:'+newListOfAgents[i].author_id+' - '+newListOfAgents[i].first_name+' '+newListOfAgents[i].last_name+'</option>';
				if (first) {
					// editor.newAgent = newListOfAgents[i].author_id;
					first = false;
				}
			}

			$('div#makeNew select.new-agent-list').html(selectOptions);
		}

		// when another data element is added to current editOption
		this.addAnother = function(groupId) {
			var structure = JSON.parse( JSON.stringify(options[ editor.currentOption ]['structure']) ); // make a copy
			var blank_data = JSON.parse( JSON.stringify(options[ editor.currentOption ]['blank_data']) ); // make a copy
			var defaultValues = JSON.parse( JSON.stringify(options[ editor.currentOption ]['default']) ); // make a copy
			var target = editor.editOption;
			var targetDefaultValues = defaultValues;
			if (groupId &&
				groupId.length) {
				var groups = groupId.split('-');
				for(var x in groups) {
					if (typeof target[groups[x]] == 'undefined')
						target[groups[x]] = {};
					target = target[groups[x]];
					blank_data = blank_data[groups[x]]; 
					structure = structure[groups[x]]; 
					targetDefaultValues = targetDefaultValues[groups[x]];
				}
			}
			var propLen = Object.keys(target).length;
			propLen -= typeof structure.agentID != 'undefined' ? 1 : 0;
			var name = 'data'+propLen++;
			var keys = Object.keys(target);
			while ( keys.indexOf(name) != -1) // make sure we have a new name
				name = 'data'+propLen++;
			target[name] = blank_data;
			targetDefaultValues[name] = blank_data;
			editor.processEditOption(defaultValues);
			editor.setChange(true);
		}

		// generate framework for current option type
		this.getHtml = function(structure) {
			var h = '<div id="options-list">';
			h = editor.htmlFromObject(structure, h, undefined, 0);
			
			h+= '</div>';
			return h;
		}

		// generate html for a given structure
		this.htmlFromObject = function(structure, h, groupId, depth) {
			if (typeof groupId != 'undefined') {
				var groupTitle = (depth == 1 ? ' --- ' : (depth == 2 ? ' - ' : ''))+groupId+(depth == 1 ? ' --- ' : (depth == 2 ? ' - ' : ''));
				h+= '<div id="group-title" class="depth'+depth+'"><span>'+groupTitle+'</span></div>';
			}

			var hasRepeating = false;
			for(var i in structure) {
				if (i == 'agentID')
					continue;
				if (i == 'repeating'){
					hasRepeating = true;
					continue;
				}
				var isOptional = false;
				if ( typeof groupId == 'undefined' )
					h+=	'<div class="option'+(typeof groupId == 'undefined' ? '' : '-'+groupId)+'" id="'+i+'">';
				switch(typeof structure[i]) {
					case 'string':
						var pos = 0;
						if (i.indexOf('optional_') != -1) {
							pos = i.indexOf('_') + 1;
							i = i.substring(pos, i.length);
							isOptional = true;
						}

						if (structure[i] == 'textarea')
							h+= '<div id="'+i+'" '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+'>'+
								'<span id="title">'+i+':</span>'+
								'<textarea what="textarea" id="'+i+'" '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+' placeholder="Enter your text"></textarea>'+
							'</div>';
						else
							h+= '<div id="'+i+'" '+(isOptional ? 'class="optional"' : '')+' '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+'>'+
									'<span id="title">'+i+':</span>'+
									'<input type="text" id="'+i+'" '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+' placeholder="Enter information for '+i+'" />'+
								'</div>';
						break;
					case 'object': // treat as array
						if ( Array.isArray(structure[i]) ) {
							var iterator = structure[i].keys();
							if (structure[i][iterator.next().value] == 'csv_strings') {
								h+= '<div id="'+i+'" '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+'>'+
										'<span id="title">'+i+':</span>'+
										'<input type="text" id="'+i+'" what="csv" '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+' placeholder="Enter comma separated strings for '+i+'" />' +
									'</div>';
							}
							else {
								if (i.indexOf('optional_') != -1) {
									pos = i.indexOf('_') + 1;
									i = i.substring(pos, i.length);
									isOptional = true;
								}
								
								h+= '<div id="'+i+'" '+(isOptional ? 'class="optional"' : '')+' '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+'>'+
										'<span id="title">'+i+':</span>'+
										'<select class="option" id="'+i+'" '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+'>';
									for(var j in structure[i])
										h+=	'<option value="'+structure[i][j]+'" id="'+i+'" '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+'>'+j+'</option>';
									h+= '</select>'+
									'</div>';
							}
						}
						else {
							var keys = Object.keys(structure[i]);
							if (keys.indexOf('isOption') != -1) {
								if (i.indexOf('optional_') != -1) {
									pos = i.indexOf('_') + 1;
									i = i.substring(pos, i.length);
									isOptional = true;
								}
								h+= '<div id="'+i+'" '+(isOptional ? 'class="optional"' : '')+' '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+'>'+
										'<span id="title">'+i+':</span>'+
										'<select class="option" id="'+i+'" '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+'>';
								for(var j in structure[i]) {
									if ( j == 'isOption' )
										continue;
									h+=	'<option value="'+structure[i][j]+'" id="'+i+'" '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+'>'+j+'</option>';
								}
									h+= '</select>'+
									'</div>';
							}
							else {
								h+= '<article class="section" title="'+i+'" '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'-'+i+'"')+'>';
								h = editor.htmlFromObject(structure[i], h, (typeof groupId == 'undefined' ? '' : groupId+'-')+i, depth+1);
								if (hasRepeating)
									h+= '<button id="'+i+'" class="removeArticle" '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+'>Remove</button>';
								h+= '</article>';
							}
						}
						break;
					case 'number':
						h+= '<div id="'+i+'" '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+'>'+
								'<span id="title">'+i+':</span>'+
								'<input type="text" what="number" id="'+i+'" '+(typeof groupId == 'undefined' ? '' : 'group="'+groupId+'"')+' onkeypress='+'"return event.charCode >= 48 && event.charCode <= 57"'+' placeholder="Enter a number"></input>'+
							'</div>';
						break;
				}
			if ( typeof groupId == 'undefined' )
				h+= '</div>';
			}
			if (typeof structure.repeating != 'undefined') {
				h+= '<div id="add-another">'+
						'<span id="add-title"><a href="javascript:editor.addAnother('+"'"+(typeof groupId == 'undefined' ? '' : groupId)+"'"+')" >Add another</a></span>'+
					'</div>';
			}
			return h;
		}

		this.DBget = function(d){
			if(typeof d.query !== 'undefined'){
				var h = [];
				if (typeof d.data === 'undefined') h.headers = {query:d.query}; else h.headers = {query:d.query,data:d.data};
				if (typeof d.done === 'undefined') h.done = function(){}; else h.done = d.done;
				// if (typeof d.fail === 'undefined') h.fail = function(){}; else h.fail = d.fail;
				if (typeof d.error !== 'undefined') h.error = d.error;
				if (typeof d.before === 'undefined') h.before = function(){}; else h.before = d.before;
				$.post(ah_local.tp+'/_admin/ajax_options.php', h.headers, function(){h.before()},'json')
					.done(function(x){
						if (x.status == 'OK') h.done(x.data);
						else if (h.error) h.error(x.data);
						else ahtb.close(function(){ahtb.alert(x.data, 'Database Error')});
					})
					.fail(function(x){
						if (h.fail) h.fail(x);
						if (h.error) h.error(x);
						else ahtb.alert("Database failure, please retry.", {height: 150});
					});
			} else ahtb.alert('Query was undefined');
		}
	}

	editor.init();
});