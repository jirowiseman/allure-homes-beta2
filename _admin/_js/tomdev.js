var dev;

jQuery(document).ready(function($) {
	dev = {
		init: function() {
			for (var i in this)
				if (typeof this[i].needsInit != 'undefined') this[i].init();
		},
		tables: {
			needsInit: true,
			init: function() {
				$('ul#tables a').on('click', function() {
					if ($(this).attr('data-query') == 'import-cities-from-listings')
						ahtb.open({
							html: '<p>Importing... <span>0%</span></p>',
							hideClose: true,
							closeOnClickBG: false,
							height: 100,
							opened: function() {
								dev.runImport = true;
								dev.runImportCities(0);
							},
							buttons: [{
								text: 'Cancel',
								action: function() {
									ahtb.close();
								}
							}],
							close: function() {
								dev.runImport = false;
							}
						});
					else if ($(this).attr('data-query') == 'log-cities')
						dev.logfile.open(ah_local.tp + '/_classes/_logs/cities.log');
					else if ($(this).attr('data-query') == 'clean-images')
						dev.imageCleaner.init();
					// dev.cleanImages.start( ($(this).attr('data-type') ? $(this).attr('data-type') : null), ($(this).attr('data-dir') ? $(this).attr('data-dir') : null) );
					else if ($(this).attr('data-query') == 'geocode-cities') {
						dev.running = true;
						dev.geocodeCities();
					} else if ($(this).attr('data-query') == 'geocode-listings') {
						dev.running = true;
						dev.geocodeListings();
					} else if ($(this).attr('data-query') == 'listings-cities-pivot') {
						dev.running = true;
						dev.createListingsPivot();
					} else {
						var theQuery = $(this).attr('data-query');
						ahtb.open({
							html: '<p>Loading..</p>',
							height: 90,
							buttons: [],
							opened: function() {
								dev.ajax({
									query: theQuery,
									done: function() {
										ahtb.close();
									}
								});
							}
						});
					}
				});
			}
		},
		createListingsPivot: function(remaining) {
			if (dev.running && !dev.creatingListingsPivot) ahtb.open({
				html: '<p>Parsing... ' + (typeof remaining == 'undefined' ? '' : remaining.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' remaining.') + '</p>',
				height: 140,
				buttons: [{
					text: 'Cancel',
					action: function() {
						dev.running = false;
						ahtb.close();
					}
				}],
				close: function() {
					dev.running = false;
					dev.creatingListingsPivot = false;
				},
				opened: function() {
					dev.creatingListingsPivot = false;
					dev.ajax({
						query: 'listings-cities-pivot',
						data: 2500, // number of rows at a time
						done: function(d) {
							dev.creatingListingsPivot = false;
							if (dev.running && d.remaining > 0) dev.createListingsPivot(d.remaining);
							else ahtb.close();
						}
					});
				}
			});
		},
		geocodeCities: function(remaining) {
			// check rejected cities
			if (dev.running && !dev.geocoding) ahtb.open({
				html: '<p>Geocoding... ' + (typeof remaining == 'undefined' ? '' : remaining.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' remaining.') + '</p>',
				height: 140,
				buttons: [{
					text: 'Cancel',
					action: function() {
						dev.running = false;
						ahtb.close();
					}
				}],
				close: function() {
					dev.running = false;
					dev.geocoding = false;
				},
				opened: function() {
					dev.geocoding = false;
					dev.ajax({
						query: 'geocode-cities',
						data: 50, // number of rows at a time
						done: function(d) {
							dev.geocoding = false;
							if (dev.running && d.remaining > 0) dev.geocodeCities(d.remaining);
							else ahtb.close();
						}
					});
				}
			});
		},
		geocodeListings: function(completed, remaining) {
			var listings_at_a_time = 5;
			if (typeof completed == 'undefined')
				completed = 0;
			// check rejected listings
			if (dev.running && !dev.geocoding)
				ahtb.open({
					html: '<p>Geocoded ' + (typeof completed == 'undefined' ? 0 : completed.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")) + ' listings' + (typeof remaining == 'undefined' ? '' : ', ' + remaining.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' remaining') + '...</p>',
					height: 140,
					buttons: [{
						text: 'Cancel',
						action: function() {
							dev.running = false;
							ahtb.close();
						}
					}],
					close: function() {
						dev.running = false;
						dev.geocoding = false;
					},
					opened: function() {
						dev.geocoding = false;
						dev.ajax({
							query: 'geocode-listings',
							data: {start: completed,
								   size: listings_at_a_time}, // number of rows at a time
							done: function(d) {
								if (typeof completed == 'undefined')
									completed = 0;
								dev.geocoding = false;
								if (dev.running && d && typeof d.remaining != 'undefined')
									dev.geocodeListings(parseInt(completed) + listings_at_a_time, (d.remaining != 'undefined' ? d.remaining : 0));
								else
									ahtb.close();
							}
						});
					}
				});
		},
		runImportCities: function(page) {
			dev.ajax({
				query: 'import-cities-from-listings',
				data: page,
				done: function(d) {
					if (d == 'done') ahtb.close();
					else($('#tb-submit p span').html((typeof d == 'undefined' ? 0 : d) + '%'));
					if (dev.runImport) dev.runImportCities(parseInt(page) + 1);
				}
			});
		},
		imageCleaner: {
			init: function() {
				self = dev.imageCleaner;
				self.history = [];
				self.query = {};
				ahtb.loading('Retreiving image sizes from database...', {
					hideTitle: true,
					height: 55,
					opened: function() {
						self.image_class.send_query('get-image-sizes', function($in) {
							self.sizes = $in;
							self.window.choose_type();
						});
					}
				});
			},
			loop: {
				iterate: function(d) {
					if (self.query.id != d.id)
						self.query.id = d.id;
					if (self.query.status != d.status)
						self.query.status = d.status;

					if (d.status == 'running')
						self.window.set_message(d.message, function() {
							self.image_class.get_results('query-from-cleaner', {
								id: self.query.id,
								status: self.query.status,
								force_regenerate: typeof self.query.force_regenerate != 'undefined' ? self.query.force_regenerate : 0
							}, function(d) {
								self.loop.iterate(d);
							});
						});
					else if (d.status == 'done')
						self.window.alert('Done.');
				},
			},
			window: {
				choose_type: function() {
					var h = '<p><strong>Choose an image type:</strong></p>' +
						'<p style="margin-bottom:1.75em"><select id="image-type">' +
						'<option value="0">All</option>';

					for (var $type in self.sizes)
						h += '<option value="' + $type + '">' + $type + '</option>';
					h += '</select></p>';
					ahtb.open({
						hideTitle: true,
						height: 160,
						width: 250,
						html: h,
						buttons: [{
							text: "Next",
							action: function() {
								var $type = $('#image-type').val();
								self.query.types = {};
								if ($type === 0)
									for (var $t in self.sizes)
										self.query.types[$t] = [];
								else
									self.query.types[$type] = [];

								for (var i in self.query.types) {
									self.window.choose_sizes(i);
									break;
								}

							}
						}, {
							text: "Cancel",
							action: function() {
								ahtb.close();
							}
						}, ]
					});
				},
				choose_sizes: function($type) {
					var $height = 130;
					var h = "<p><strong>Image sizes to generate for " + $type + '</strong></p>' +
						'<ul id="image-sizes" data-type="' + $type + '">' +
						'<li><a id="check-all-sizes-boxes">Select all</a> / <a id="uncheck-all-sizes-boxes">Deselect all</a></li>';
					for (var i in self.sizes[$type]) {
						h += '<li><input type="checkbox" data-dir="' + self.sizes[$type][i].dir + '" /> ' + self.sizes[$type][i].dir + '</li>';
						$height += 24;
					}
					h += '</ul>';
					ahtb.open({
						hideTitle: true,
						height: $height,
						width: 250,
						html: h,
						buttons: [{
							text: "Next",
							action: function() {
								var $type = $('#image-sizes').attr('data-type');

								$('#image-sizes li input').each(function() {
									if ($(this).is(':checked') && self.query.types[$type])
										self.query.types[$type].push($(this).attr('data-dir'));
								});

								if (self.query.types[$type] && self.query.types[$type].length < 1)
									self.query.types[$type] = 0;

								found = false;
								for (var i in self.query.types)
									if (i != $type && self.query.types[i] !== 0 && self.query.types[i].length < 1) {
										found = i;
										break;
									}

								if (found !== false)
									self.window.choose_sizes(found);
								else
									self.window.loading('Sending image query to server...', function() {
										self.image_class.get_results('get-type-counts', self.query, function(d) {
											self.window.confirm_types(d);
										});
									});
							}
						}, {
							text: "Cancel",
							action: function() {
								ahtb.close();
							}
						}, ],
						opened: function() {
							$('a#check-all-sizes-boxes').on('click', function() {
								$('#image-sizes li input').prop('checked', true);
							});
							$('a#uncheck-all-sizes-boxes').on('click', function() {
								$('#image-sizes li input').prop('checked', false);
							});
						}
					});
				},
				confirm_types: function($resp) {
					var h = '<p>The following operation will generate image sizes for:<br/>' +
						'<small>Listings may have more than 1 image per row</small></p>' +
						'<ul style="padding-left: .75em">';
					for (var i in $resp)
						h += '<li><strong>' + $resp[i].count + '</strong> ' + i + ' rows</li>';
					h += '</ul><br/>';
					h += '<input type="checkbox" class="forceRegen">&nbsp;Force regen</input><br/>';
					self.query.force_regenerate = 0;
					ahtb.open({
						hideTitle: true,
						height: 160 + (Object.keys($resp).length * 24),
						width: 450,
						html: h,
						opened: function() {
							$('input.forceRegen').on('change', function() {
								var checked = $(this).prop('checked');
								console.log('regen is now '+checked);
								self.query.force_regenerate = checked ? 1 : 0;
							});
						},
						buttons: [{
							text: "Start",
							action: function() {
								self.window.loading('Preparing to start...', function() {
									self.query.status = 'start';
									self.image_class.get_results('query-from-cleaner', self.query, function(d) {
										self.loop.iterate(d);
									});
								});
							}
						}, {
							text: "Cancel",
							action: function() {
								ahtb.close();
							}
						}, ]
					});
				},
				alert: function($message, $height) {
					ahtb.open({
						hideTitle: true,
						height: $height ? $height : 117,
						html: '<p>' + $message + '</p>',
						buttons: [{
							text: "OK",
							action: function() {
								ahtb.close();
							}
						}],
						width: 450,
					});
				},
				loading: function($message, $callback, $height) {
					ahtb.loading($message, {
						hideTitle: true,
						height: $height ? $height : 55,
						width: 450,
						opened: function() {
							if ($callback) $callback();
						}
					});
				},
				set_message: function($message, $callback) {
					$('#tb-submit p:first-child').html($message);
					// $('#tb-submit p:first-child').finish().fadeOut(10, function(){
					if ($callback) $callback();
					// 	$(this).html( $message ).fadeIn(100);
					// });
				},
			},
			image_class: {
				send_query: function($query, $callback) {
					this.DB({
						query: $query,
						done: function(d) {
							if ($callback) $callback(d);
						}
					});
				},
				get_results: function($query, $data, $callback) {
					this.DB({
						query: $query,
						data: $data,
						done: function(d) {
							if ($callback) $callback(d);
						}
					});
				},
				DB: function($x) {
					if (typeof $x.query == 'undefined') console.error('query was undefined.');
					else $.ajax({
						url: ah_local.tp + '/_classes/Image.class.php',
						data: {
							query: $x.query,
							data: typeof $x.data == 'undefined' ? null : $x.data
						},
						dataType: 'JSON',
						type: 'POST',
						error: function($xhr, $status, $error) {
							ahtb.open({
								title: 'You Have Encountered an Error',
								height: 140,
								html: '<p>' + $error + '</p>'
							});
						},
						success: function(data) {
							if (typeof data.status == 'undefined' || data.status != 'OK')
								ahtb.open({
									title: 'You Have Encountered an Error',
									height: 140,
									html: '<p>' + (typeof data.data == 'undefined' ? data : data.data) + '</p>',
									buttons: [{
										text: 'OK',
										action: function() {
											ahtb.close();
										}
									}]
								});
							else if ($x.done) $x.done(data.data);
							else console.log(data.data);
						}
					});
				}
			}
		},
		logfile: {
			init: false,
			auto_refresh: false,
			refresh_interval: 2000, // in ms
			timers: [],
			statusTimer: null,
			open: function(theFile) {
				ahtb.open({
					title: 'Log: ' + theFile,
					width: 750,
					height: 354,
					html: '<div style="position:relative;overflow:auto;margin:1em 0"><span style="position:absolute;" class="status"></span>' +
						'<ul class="log-menu">' +
						'<li><a class="refresh-logfile">Refresh</a></li><li> | </li>' +
						'<li class="auto-toggle"></li><li> | </li>' +
						'<li>Refresh Rate: <select style="font-size:0.7em" id="refresh-interval">' +
						'<option value="10">10ms</option>' +
						'<option value="50">50ms</option>' +
						'<option value="250">250ms</option>' +
						'<option value="500">500ms</option>' +
						'<option value="1000">1s</option>' +
						'<option value="2000" selected>2s</option>' +
						'<option value="5000">5s</option>' +
						'<option value="10000">10s</option>' +
						'<option value="30000">30s</option>' +
						'</select></li>' +
						'</ul>' +
						'</div>' +
						'<pre id="logfile-viewer"></pre>',
					close: dev.logfile.disable_auto_refresh(),
					opened: function() {
						$log = dev.logfile;
						$log.file = theFile;
						$log.status('Starting...');
						$log.init = true;
						$log.isRunning = false;
						$('.log-menu .refresh-logfile').on('click', function() {
							$log.refresh(true);
						});
						$('.log-menu #refresh-interval').change(function() {
							$log.disable_auto_refresh();
							$log.refresh_interval = parseInt($(this).val());
						});
						if ($log.auto_refresh) $log.enable_auto_refresh();
						else $log.disable_auto_refresh();
						$log.refresh(true);
					}
				});
			},
			refresh: function(scrollToBottom) {
				$log = dev.logfile;
				$.ajax({
					url: $log.file,
					dataType: 'text',
					beforeSend: function() {
						$log.isRunning = true;
						$log.status('Retrieving file from server...');
					},
					error: function($xhr, $status, $error) {
						$log.status('<span style="color:red;">Error</span>');
						$log.view($status);
						$log.disable_auto_refresh();
					},
					success: function(data) {
						$log.status('Loaded, parsing...');
						$log.view(data);
						$log.status('Done.');
						$log.statusTimer = setTimeout(function() {
							$log.status();
						}, ($log.refresh_interval < 1000 ? $log.refresh_interval - 25 : $log.refresh_interval));
						if (scrollToBottom) $('pre#logfile-viewer').scrollTop($('pre#logfile-viewer')[0].scrollHeight);
						if ($log.refreshing) {
							$log.disable_auto_refresh();
							$log.enable_auto_refresh();
						}
						$log.isRunning = false;
					}
				});
			},
			disable_auto_refresh: function() {
				var $log = dev.logfile;
				$log.refreshing = false;
				for (var i in $log.timers) clearTimeout($log.timers[i]);
				$log.timers = [];
				$('li.auto-toggle').html('<a class="enable-auto">Enable Auto-Refresh</a>');
				$('.log-menu .enable-auto').one('click', function() {
					$log.enable_auto_refresh();
				});
			},
			enable_auto_refresh: function() {
				var $log = dev.logfile;
				$log.refreshing = true;
				$log.timers.push(setTimeout(function() {
					if (!$log.isRunning)
						$log.refresh(true);
				}, $log.refresh_interval));
				$('li.auto-toggle').html('<a class="disable-auto">Disable Auto-Refresh</a>');
				$('.log-menu .disable-auto').one('click', function() {
					$log.disable_auto_refresh();
				});
			},
			status: function(newStatus) {
				var $log = dev.logfile,
					fade = $log.refresh_interval < 125 ? ($log.refesh_interval > 5 ? $log.refesh_interval - 5 : 1) : 100;
				clearTimeout($log.statusTimer);
				$log.statusTimer = null;
				$('#tb-submit .status').finish().fadeOut(fade, function() {
					$(this).html(newStatus ? newStatus : '').fadeIn(fade);
				});
			},
			view: function(newView) {
				$('pre#logfile-viewer').html(newView);
			},
		},
		ajax: function($x) {
			if (typeof $x.query == 'undefined') console.error('query was undefined.');
			else $.ajax({
				url: ah_local.tp + '/_admin/ajax_tomdev.php',
				data: {
					query: $x.query,
					data: typeof $x.data == 'undefined' ? null : $x.data
				},
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error) {
					ahtb.open({
						title: 'You Have Encountered an Error',
						height: 140,
						html: '<p>' + $error + '</p>'
					});
				},
				success: function(data) {
					if (typeof data.status == 'undefined' || data.status != 'OK')
						ahtb.open({
							title: 'You Have Encountered an Error',
							height: 140,
							html: '<p>' + (typeof data.data == 'undefined' ? data : data.data) + '</p>',
							buttons: [{
								text: 'OK',
								action: function() {
									ahtb.close();
								}
							}]
						});
					else if ($x.done) $x.done(data.data);
					else console.log(data.data);
				}
			});
		}
	};
	dev.init();
});