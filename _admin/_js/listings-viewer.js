var listingAdmin;

function deleteCookie(what) {
	var name = what + "=";
	document.cookie = name+"; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    deleteCookie(cname);
    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(what) {
    var user = getCookie(what);
    if (user != "") {
        return true;
    } else {
        return false;
    }
}

var ActiveState = {
	INACTIVE: 0,
	ACTIVE: 1,
	WAITING:   2,
	REJECTED: 3,
	TOOCHEAP: 4,
	NEVER: 5,
	AVERAGE: 6
}

var ActiveStateNames = [
	'inactive',
	'active',
	'waiting',
	'rejected',
	'toocheap',
	'never',
	'average'
]

var OptionState = {
	SELLER: 0,
	ADDRESS: 1
}

var ButtonOptions = [ [1,5], [2,3,5], [1,3,5], [1,2,5], [1,2,3,5,6], [2], [2,5] ];

jQuery(document).ready(function($){
	listingAdmin = new function(){
		this.ajaxUrl = ah_local.tp+"/_admin/ajax_viewer.php";
		this.authors = ah_local.authors;
		this.curPage = 0;
		this.pageSize = 10;
		this.listings = [];
		this.rowCount = 0;
		this.gotRowCount = false;
		this.activeSelected = 1; // AH_ACTIVE
		this.option = 0;
		this.lastOption = null;
		this.animate = false;
		this.showReminderRefresh = 1;
		this.price = 400000;
		this.errorMax = 6;
		this.keywordsApplied = false;
		this.idSort = 0;
		this.priceSort = 0;
		this.homeImgs = [];

		this.init = function() {
			this.organizeHomePageImgs();
			
			if (!checkCookie('ActivePage')) {
				listingAdmin.activeSelected = 1;
				setCookie('ActivePage', listingAdmin.activeSelected, 2);
			}
			else {
				var value = getCookie('ActivePage') == "NaN" || getCookie('ActivePage') == 'false' ? '1' : getCookie('ActivePage');
				listingAdmin.activeSelected = parseInt(value) > 5 ? 1 : parseInt(value); 
			}

			if (!checkCookie('ReminderRefresh')) {
				listingAdmin.showReminderRefresh = 1;
				setCookie('ReminderRefresh', listingAdmin.showReminderRefresh, 7);
			}
			else {
				var value = getCookie('ReminderRefresh') == "NaN" ? '1' : getCookie('ReminderRefresh');
				listingAdmin.showReminderRefresh = parseInt(value);
			}

			$('#keywordOption .spinner').fadeOut(1, function(){});

			$("#priceSlider").show();		
			$('#priceValue').html("Current min price: "+listingAdmin.price);
			$('#priceValue').show();
		    $( "#priceSlider" ).slider({
		      range: false,
		      min: 50000,
		      max: 5000000,
		      step: 50000,
		      value: listingAdmin.price,
		      stop: function( event, ui ) {
		        listingAdmin.price = ui.value;
		       // listingAdmin.getPage(listingAdmin.curPage);
		        var target = listingAdmin.getActiveName(listingAdmin.activeSelected)+'Price';
		        setCookie(target, listingAdmin.price, 2);
		        var val = ui.value;
		        $('#priceValue').html("Current min price: "+val);
		        listingAdmin.getRowCount();
		      },
		      slide: function( event, ui) {
		      	$('#priceValue').html("Current min price: "+(ui.value));
		      }
		    });

		    $('#sliderValue').show();
			$('#sliderValue').html("Current Page: "+(listingAdmin.curPage+1));
		    $("#pageSlider").show();
		    $( "#pageSlider" ).slider({
		      range: false,
		      min: 0,
		      max: listingAdmin.curPage,
		      value: listingAdmin.curPage,
		      stop: function( event, ui ) {
		        listingAdmin.curPage = ui.value;
		        listingAdmin.getPage(listingAdmin.curPage);
		        var target = listingAdmin.getActiveName(listingAdmin.activeSelected)+'Page';
		        setCookie(target, listingAdmin.curPage, 2);
		        $('#sliderValue').html("Current Page: "+(ui.value+1));
		      },
		      slide: function( event, ui) {
		      	$('#sliderValue').html("Current Page: "+(ui.value+1));
		      }
		    });


			listingAdmin.getCookies(listingAdmin.activeSelected);
			$("#activeChooser #"+listingAdmin.getActiveName(listingAdmin.activeSelected)).prop("checked", true);

			// $("#activeChooser #live").prop("checked", true);
			// $("#optionChooser #seller").prop("checked", true);
			// listingAdmin.lastOption = $("#optionChooser #seller");
			// $("#optionChooser .optionActive").prop("checked", false);
			// listingAdmin.disable($('#sellerOption'));
			// listingAdmin.disable($('#addressOption'));
			// $('#sellerOption').children().prop("disabled", !listingAdmin.option);
			// $('#addressOption').children().prop("disabled", !listingAdmin.option);

			$('#activeChooser input').on('change', function() {
				if ($(this).prop('checked'))
				{
					var oldSelection = listingAdmin.activeSelected;
					listingAdmin.getActiveSelection();
					listingAdmin.getSetPageCookies(oldSelection);
					listingAdmin.setRowCountCookies(oldSelection);
					listingAdmin.keywordsApplied = false;
					listingAdmin.getRowCount();
					setCookie('ActivePage', listingAdmin.activeSelected, 2);
					$('#keyword').val("");
					$('#keyword').css('placeHolder', "Enter a keyword to search in the description");
					if (listingAdmin.activeSelected == 1 ||
						listingAdmin.activeSelected == 2) 
						$('#applyKeyword').prop("disabled", false);
					else
						$('#applyKeyword').prop("disabled", true);
				}
			});

			$("#optionChooser .optionActive").change(function(){
				listingAdmin.option = $(this).prop("checked");
				console.log("Option is now "+listingAdmin.option );
				listingAdmin.setOptions();

				var oldSelection = listingAdmin.activeSelected;
				listingAdmin.getActiveSelection();
				listingAdmin.getSetPageCookies(oldSelection);
				listingAdmin.setRowCountCookies(oldSelection);
				listingAdmin.getRowCount();
			});

			$("#optionChooser input[name='option']").on('click', function() {
				if ($(this).prop('checked'))
				{
					if ($(this).attr('id') == "seller") {
						console.log("seller checked");
						listingAdmin.lastOption = $(this);
						$('#submitName').prop("disabled", false);
						$('#submitAddress').prop("disabled", true);
					}
					else {
						console.log("address checked");
						listingAdmin.lastOption = $(this);
						$('#submitName').prop("disabled", true);
						$('#submitAddress').prop("disabled", false);
				 	}
				}

			});

			$('#optionChooser #submitName').on('click', function() {
				listingAdmin.curPage = 0;
				listingAdmin.setCookies(listingAdmin.activeSelected);
				listingAdmin.getRowCount();
			});

			$('#optionChooser #submitAddress').on('click', function() {
				listingAdmin.curPage = 0;
				listingAdmin.setCookies(listingAdmin.activeSelected);
				listingAdmin.getRowCount();
			});
			listingAdmin.getRowCount();
			

			$('#prevButton').on('click', function() {
				console.log("Previous selected, curPage:"+listingAdmin.curPage);
				if (listingAdmin.curPage > 0) {
					listingAdmin.getPage(listingAdmin.curPage - 1);
					listingAdmin.curPage--;
					var target = listingAdmin.getActiveName(listingAdmin.activeSelected)+'Page';
					setCookie(target, listingAdmin.curPage, 2);
					console.log("Set page for "+target+" to "+listingAdmin.curPage);
					$('#sliderValue').html("Current Page: "+(listingAdmin.curPage+1));
					$( "#pageSlider" ).slider('value', listingAdmin.curPage);
				}
			});

			$('#nextButton').on('click', function() {
				console.log("Next selected, curPage:"+listingAdmin.curPage+", last:"+listingAdmin.rowCount);
				if ( (listingAdmin.curPage + 1)*listingAdmin.pageSize < listingAdmin.rowCount ) {
					listingAdmin.getPage(listingAdmin.curPage + 1);
					listingAdmin.curPage++;
					var target = listingAdmin.getActiveName(listingAdmin.activeSelected)+'Page';
					setCookie(target, listingAdmin.curPage, 2);
					console.log("Set page for "+target+" to "+listingAdmin.curPage);
					$('#sliderValue').html("Current Page: "+(listingAdmin.curPage+1));
					$( "#pageSlider" ).slider('value', listingAdmin.curPage);
				}
			});

			$('#applyKeyword').on('click', function() {
				$('#keywordOption .spinner').fadeIn(250, function() {
		         	listingAdmin.applyKeyword($('#keyword').val());
		        })
			});

			$('button#cleanTags').on('click', function() {
				$('#keywordOption .spinner').fadeIn(250, function() {
		         	listingAdmin.cleanTags();
		        })
			});

			$('button#geocode').on('click', function() {
				console.log('geocode clicked');
				$('#keywordOption .spinner').fadeIn(250, function() {
		         	listingAdmin.geocode();
		        })
			});
		}

		this.setOptions = function() {
			if (listingAdmin.option) {
				listingAdmin.enable($('#sellerOption'));
				listingAdmin.enable($('#addressOption'));
				//listingAdmin.lastOption.click();
			}
			else {
				listingAdmin.disable($('#sellerOption'));
				listingAdmin.disable($('#addressOption'));
			}
			listingAdmin.lastOption.prop("checked", true);
		}

		this.enable = function(parent)
		{
			$.each( parent.children(), function() {
				$(this).prop("disabled", false);
				listingAdmin.enable($(this));
			});
		}
		this.disable = function(parent)
		{
			$.each( parent.children(), function() {
				$(this).prop("disabled", true);
				listingAdmin.enable($(this));
			});
		}

		this.getActiveSelection = function() {
			var selected = 0;
			var i = 0;
			$.each( $("#activeChooser input[type='radio']"), function() {
				if ($(this).prop('checked') ) {
					selected = i;
				}
				i++;
			});
			console.log("getActiveSelection:"+selected);
			listingAdmin.activeSelected = selected;
			setCookie('ActivePage', listingAdmin.activeSelected, 2);
		}

		this.getActiveName = function(active) {
			switch(active) {
				case ActiveState.INACTIVE: return 'inactive';
				case ActiveState.ACTIVE: return 'live';
				case ActiveState.WAITING: return 'waiting';
				case ActiveState.REJECTED: return 'rejected';
				case ActiveState.TOOCHEAP: return 'tooCheap';
				case ActiveState.NEVER: return 'never';
				case ActiveState.AVERAGE: return 'average';
				// case ActiveState.SELLER: return 'seller';
				// case ActiveState.ADDRESS: return 'address';
				default: return 'unknown';
			}
		}

		this.getOptionName = function(opt) {
			switch(opt) {
				case OptionState.SELLER: return 'seller';
				case OptionState.ADDRESS: return 'address';
			}
		}

		this.setCookies = function(which) {
			var target = listingAdmin.getActiveName(which)+'Page';
			switch (which) {
				case ActiveState.INACTIVE:
				case ActiveState.ACTIVE:
				case ActiveState.WAITING:
				case ActiveState.REJECTED: 
				case ActiveState.TOOCHEAP: 
				case ActiveState.NEVER: 
				case ActiveState.AVERAGE:
					setCookie(target, listingAdmin.curPage, 2); 

					target = listingAdmin.getActiveName(which)+'Option';
					setCookie(target, listingAdmin.option ? "Active" : "Inactive", 2); 
					var option = listingAdmin.lastOption.attr('id');
					target += "Which";
					setCookie(target, option);
					console.log("setCookies for "+listingAdmin.getActiveName(which)+" - option:"+(listingAdmin.option ? "Active" : "Inactive")+", which:"+option);

					target = listingAdmin.getActiveName(which)+'First';
					setCookie(target, $('#optionChooser #firstName').val(), 2);
					target = listingAdmin.getActiveName(which)+'Last';
					setCookie(target, $('#optionChooser #lastName').val(), 2);
					target = listingAdmin.getActiveName(which)+'Street';
					setCookie(target, $('#optionChooser #street').val(), 2);
					target = listingAdmin.getActiveName(which)+'City';
					setCookie(target, $('#optionChooser #city').val(), 2);
					target = listingAdmin.getActiveName(which)+'State';
					setCookie(target, $('#optionChooser #state').val(), 2); 

					target = listingAdmin.getActiveName(which)+'Price';
					setCookie(target, listingAdmin.price);

					target = listingAdmin.getActiveName(which)+'Keyword';
		        	value = $('#keyword').val();
		        	setCookie(target, value);
		        	
					break;
			}
		}

		this.getCookie = function(target) {
			var retval = ''
			if (!checkCookie(target)) {
				setCookie(target, '', 2);
			}
			else {
				retval = getCookie(target);
				console.log("Got value for "+target+" is "+retval);
			}
			return retval;
		}


		this.getNumCookie = function(target) {
			if (!checkCookie(target)) {
				listingAdmin.curPage = 0;
				setCookie(target, listingAdmin.curPage, 2);
			}
			else {
				var value = getCookie(target) == "NaN" ? '0' : getCookie(target);
				listingAdmin.curPage = parseInt(value);
				console.log("Got page for "+target+" is "+listingAdmin.curPage);
				$('#sliderValue').html("Current Page: "+(listingAdmin.curPage+1));
				$( "#pageSlider" ).slider('value', listingAdmin.curPage);
			}
		}

		this.getStrCookie = function(target, element){
			if (!checkCookie(target)) {			
				$(element).val("");		
				setCookie(target, "", 2);
			}
			else {
				var value = getCookie(target);
				$(element).val(value);
				console.log("Got for "+target+" is "+value);
			}
		}

		this.getCookies = function(which) {
			var target = listingAdmin.getActiveName(which)+'Page';
			var value = '';
			switch (listingAdmin.activeSelected) {
				case ActiveState.INACTIVE:
				case ActiveState.ACTIVE:
				case ActiveState.WAITING:
				case ActiveState.REJECTED:
				case ActiveState.TOOCHEAP: 
				case ActiveState.NEVER: 
				case ActiveState.AVERAGE:
					listingAdmin.getNumCookie(target);

					target = listingAdmin.getActiveName(which)+'Option';
					value = listingAdmin.getCookie(target);
					listingAdmin.option = (value && value == 'Active') ? 1 : 0;
					$("#optionChooser .optionActive").prop("checked", listingAdmin.option ? true : false);

					target += "Which";
					value = listingAdmin.getCookie(target);
					if (value && value == 'address')
						listingAdmin.lastOption = $("#optionChooser #address");
					else
						listingAdmin.lastOption = $("#optionChooser #seller");

					console.log("getCookies for "+listingAdmin.getActiveName(which)+" - option:"+listingAdmin.option+", which:"+value);

					listingAdmin.setOptions();
					
					target = listingAdmin.getActiveName(which)+'First';
					listingAdmin.getStrCookie(target, '#optionChooser #firstName');

					target = listingAdmin.getActiveName(which)+'Last';
					listingAdmin.getStrCookie(target, '#optionChooser #lastName');
					
					target = listingAdmin.getActiveName(which)+'Street';
					listingAdmin.getStrCookie(target, '#optionChooser #street');

					target = listingAdmin.getActiveName(which)+'City';
					listingAdmin.getStrCookie(target, '#optionChooser #city');

					target = listingAdmin.getActiveName(which)+'State';
					listingAdmin.getStrCookie(target, '#optionChooser #state');

					target = listingAdmin.getActiveName(which)+'Price';
		        	value = listingAdmin.getCookie(target);
		        	if (value == '' ||
		        		value == "NaN")
		        		listingAdmin.price = 800000;
		        	else
		        		listingAdmin.price = parseInt(value);
		        	if (which == ActiveState.TOOCHEAP) {
		        		if (listingAdmin.price < 600000 ||
		        			listingAdmin.price >= 800000)
							listingAdmin.price = 600000;
					} else if (which == ActiveState.AVERAGE) {
						if (listingAdmin.price > 600000)
							listingAdmin.price = 500000;
					}

		        	$('#priceValue').html("Current min price: "+listingAdmin.price);
		        	$( "#priceSlider" ).slider('value', listingAdmin.price);

		        	target = listingAdmin.getActiveName(which)+'Keyword';
		        	value = listingAdmin.getCookie(target);
		        	$('#keyword').val(value);
					break;
			}
		}

		this.getSetPageCookies = function(oldSelection)
		{
			listingAdmin.setCookies(oldSelection);
			listingAdmin.getCookies(listingAdmin.activeSelected);
		}

		this.setRowCountCookies = function(oldSelection)
		{
			var old = listingAdmin.getActiveName(oldSelection)+'RowCount';
			setCookie(old, listingAdmin.rowCount, 2);
		}

		this.setSlider = function() {
			if (!listingAdmin.gotRowCount) {
				window.setTimeout(function(){
					listingAdmin.setSlider();
				}, 100);
				return;
			}

			$('#sliderValue').html("Current Page: "+(listingAdmin.curPage+1));

			if ( (listingAdmin.rowCount / listingAdmin.pageSize) > 2) {
				console.log("Max page is "+(listingAdmin.rowCount / listingAdmin.pageSize));
				var maxi = Math.floor(listingAdmin.rowCount / listingAdmin.pageSize) - 1;
				maxi = (listingAdmin.rowCount % listingAdmin.pageSize) ? maxi + 1 : maxi;
				$( "#pageSlider" ).slider('option', 'max', maxi);
				$( "#pageSlider" ).slider('value', listingAdmin.curPage);				
			}
			else
				$("#pageSlider").hide();
		}

		this.sortId = function() {
			console.log ("sorId called cur dir: ", listingAdmin.idSort);
			removeClass = "entypo-down-open"
			if (listingAdmin.idSort === 0) {// first time
				myClass = "entypo-down-open";
			}
			else {
				myClass = listingAdmin.idSort === true ? "entypo-up-open" : "entypo-down-open" ;
				removeClass = listingAdmin.idSort === true ? "entypo-down-open" : "entypo-up-open" ;
			}
			$('.column-listing-id').removeClass(removeClass);
			$('.column-listing-id').addClass(myClass);

			listingAdmin.idSort = !listingAdmin.idSort;
			if ($('.column-price').hasClass("entypo-up-open"))
				$('.column-price').removeClass("entypo-up-open");
			if ($('.column-price').hasClass("entypo-down-open"))
				$('.column-price').removeClass("entypo-down-open");
			listingAdmin.priceSort = 0;

			listingAdmin.getPage(listingAdmin.curPage);
		}

		this.sortPrice = function() {
			console.log ("sortPrice called cur dir: ", listingAdmin.priceSort);
			removeClass = "entypo-down-open"
			if (listingAdmin.priceSort === 0) {// first time
				myClass = "entypo-down-open";
			}
			else {
				myClass = listingAdmin.priceSort === true ? "entypo-up-open" : "entypo-down-open" ;
				removeClass = listingAdmin.priceSort === true ? "entypo-down-open" : "entypo-up-open" ;
			}
			$('.column-price').removeClass(removeClass);
			$('.column-price').addClass(myClass);
			listingAdmin.priceSort = !listingAdmin.priceSort;

			if ($('.column-listing-id').hasClass("entypo-up-open"))
				$('.column-listing-id').removeClass("entypo-up-open");
			if ($('.column-listing-id').hasClass("entypo-down-open"))
				$('.column-listing-id').removeClass("entypo-down-open");
			listingAdmin.idSort = 0;

			listingAdmin.getPage(listingAdmin.curPage);
		}

		this.getPage = function(page) {
			//var startAt = listingAdmin.listings.length ? listingAdmin.listings[listingAdmin.listings.length - 1].id +1 : 0;
			//listingAdmin.getActiveSelection();
			var one = '';
			var two = '';
			var three = ''
			var whichOne = OptionState.SELLER;
			if (typeof listingAdmin.listings == 'object')
				listingAdmin.listings.splice(0, listingAdmin.listings.length); // clear out
			if (listingAdmin.lastOption.attr('id') == 'seller')
			{
				one = $('#optionChooser #firstName').val().trim();
				two = $('#optionChooser #lastName').val().trim();
				if (!one && !two && listingAdmin.option) {
					listingAdmin.showListings();
					return;
				}
			}
			else // must be 'address'
			{
				one = $('#optionChooser #street').val().trim();
				two = $('#optionChooser #city').val().trim();
				three = $('#optionChooser #state').val().trim();
				whichOne = OptionState.ADDRESS;
				if (!one && !two && !three && listingAdmin.option) {
					listingAdmin.showListings();
					return;
				}
			}

			$.ajax({
				url: listingAdmin.ajaxUrl,
				data: { query: 'getpage',
						data: {	page: page,
							perPage: listingAdmin.pageSize,
							active: listingAdmin.activeSelected,
							option: listingAdmin.option,
							price: listingAdmin.price,
							which: whichOne,
							first: one,
							second: two,
							third: three,
							haveKeywordMatch: listingAdmin.keywordsApplied,
							sortid: listingAdmin.idSort,
							sortprice: listingAdmin.priceSort }
					  },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during getPage: "+msg);
					listingAdmin.getPage(page); // try again
				},					
			  	success: function(data){
				  	if (data == null || data.status == null ||
				  		typeof data == 'undefined' || typeof data.status == 'undefined') 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else if (data.status != 'OK') {
				  		var msg = 'Failed to get any PP starting point data';
				  		if (data.data != null)
				  			msg += ': '+data.data;
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+msg+'</p>' });
				  	}
				  	else {
				  		if (data.data) {
					  		listingAdmin.listings = data.data;
					  	}
						listingAdmin.showListings();
	  				}
		  		}
	  		})
		}

		this.showListings = function() {
			$('#controls #applyChanges').prop('disabled', true);
			var body = '';
			for(var x = 0; typeof listingAdmin.listings == 'object' && x < listingAdmin.listings.length; x++) {
				body += '<tr';
				if ( (x % 2) == 1)
					body += " class='alternate'";
				body += '>';
				body += "<td style='width: 5%'>"+listingAdmin.listings[x].id+"</td>";
				body += "<td style='width: 8%'>"+listingAdmin.listings[x].price+"</td>";
				body += "<td style='width: 5%'>"+listingAdmin.listings[x].beds+"/"+listingAdmin.listings[x].baths+"</td>";
				var err = '';
				for(var j = 0; j < listingAdmin.errorMax; j++) {
					if (listingAdmin.listings[x].error & (1 << j)) {
						if (err.length)
							err += ",";
						err += j+1;
					}
				}
				body += "<td style='width: 5%'>"+err+"</td>";

				body += "<td style='width: 5%'>"+listingAdmin.listings[x].tagCount+"</td>";

				body += "<td style='width: 2%'><input type='checkbox' name='home-"+listingAdmin.listings[x].id+"' id='home-"+listingAdmin.listings[x].id+"'"+((typeof listingAdmin.homeImgs != 'undefined') && listingAdmin.homeImgs.indexOf(listingAdmin.listings[x].id) != -1 ? " checked" : "")+" /></td>";
				// if (!listingAdmin.listings[x].images.isArray() &&
				// 	typeof listingAdmin.listings[x].images == 'string')
				// 	listingAdmin.listings[x].images = JSON.parse(listingAdmin.listings[x].images);

				var imgCount = 0;
				if (listingAdmin.listings[x].images != 'undefined' &&
					listingAdmin.listings[x].images != null &&
					listingAdmin.listings[x].images.length) {
					var galleryName = 'gallery'+x;
					body += "<td style='width: 220px'><ul class='listing-"+galleryName+"'>";
					var item = '';
					for(var i = 0; i < 6 && i < listingAdmin.listings[x].images.length; i++) {
						if (typeof listingAdmin.listings[x].images[i].file != 'undefined' &&
							listingAdmin.listings[x].images[i].file != null) {
							if (listingAdmin.listings[x].images[i].file.indexOf("http") != -1) {
								if (listingAdmin.listings[x].images.length == 1) {
									item = "<li class='listing-image-single' style='width: 210px; height: 120px; margin-left: 40px'>";
											//"<div style='display: inline;'><img src='"+listingAdmin.listings[x].images[i].file+"' style='width: 210px; height: 120px'/>";
									if (listingAdmin.listings[x].images[i].width != 'undefined' && listingAdmin.listings[x].images[i].width != null) {
										item += '<span>('+listingAdmin.listings[x].images[i].width+'x'+listingAdmin.listings[x].images[i].height+')</span>';
									}
									item += "<img src='"+listingAdmin.listings[x].images[i].file+"' style='width: 210px; height: 120px'/>";
									//body += "</div></li>"; style="position: absolute; right: 15px; top: -15px; z-index: 10;"
									item += "</li>";
									body += item;
								}
								else {
									item = "<li class='listing-image'>";
											//"<div style='display: inline;'><img src='"+listingAdmin.listings[x].images[i].file+"' style='width: 100%; height: 100%'/>";
									if (listingAdmin.listings[x].images[i].width != 'undefined' && listingAdmin.listings[x].images[i].width != null) {
										item += '<span >('+listingAdmin.listings[x].images[i].width+'x'+listingAdmin.listings[x].images[i].height+')</span>';
									} // style="position: absolute; right: 15px; top: -15px; z-index: 10;"
									item += "<img src='"+listingAdmin.listings[x].images[i].file+"' style='width: 100%; height: 100%'/>";
									//body += "</div></li>";
									item += "</li>";
									body += item;
								}
							}
							else {
								item = "<li class='listing-image'>";
										//"<div style='display: inline;'><img src='"+ah_local.tp+"/_img/_listings/210x120/"+listingAdmin.listings[x].images[i].file+"' style='width: 100%; height: 100%'/>";
								if (listingAdmin.listings[x].images[i].width != 'undefined' && listingAdmin.listings[x].images[i].width != null) {
									item += '<span >('+listingAdmin.listings[x].images[i].width+'x'+listingAdmin.listings[x].images[i].height+')</span>';
								}		// style="position: absolute; right: 15px; top: -15px; z-index: 10;"
								item += "<img src='"+ah_local.tp+"/_img/_listings/210x120/"+listingAdmin.listings[x].images[i].file+"' style='width: 100%; height: 100%'/>";
								//body += "</div></li>";
								item += "</li>";
								body += item;
							}
							imgCount++;
						} else if (typeof listingAdmin.listings[x].images[i].discard != 'undefined' &&
								   listingAdmin.listings[x].images[i].discard != null ) {
							if (listingAdmin.listings[x].images[i].discard.indexOf("http") != -1) {
								if (listingAdmin.listings[x].images.length == 1)
									body += "<li class='listing-image-single' style='width: 210px; height: 120px; margin-left: 10px'>"+                                      
											"<span>DISCARD</span><img src='"+listingAdmin.listings[x].images[i].discard+"' style='width: 210px; height: 120px'/></li>";
								else
									body += "<li class='listing-image'>"+																									
											"<span>DISCARD</span><img src='"+listingAdmin.listings[x].images[i].discard+"' style='width: 100%; height: 100%'/></li>";
							}
							else
								body += "<li class='listing-image'>"+																																		
										"<span>DISCARD</span><img src='"+ah_local.tp+"/_img/_listings/210x120/"+listingAdmin.listings[x].images[i].discard+"' style='width: 100%; height: 100%'/></span></li>";
							imgCount++;
						} 

					}
					body += "</ul></td>";				
				}
				if (imgCount == 0)
					body += "<td style='padding-left: 180px'>No Image</td>";
				//body += "<td><a href="+ah_local.wp+"/listing/1-"+listingAdmin.listings[x].id+" target='_blank'>"+listingAdmin.listings[x].title+"</a></td>";
				body += '<td><a href="javascript:listingAdmin.gotoListing('+listingAdmin.listings[x].id+')" target="_blank">'+listingAdmin.listings[x].title+"</a></td>";
				body += "<td>";
				var optCount = 0;
				if (ButtonOptions[listingAdmin.activeSelected].length) {					
					for(j = 0; j < ButtonOptions[listingAdmin.activeSelected].length; j++) {
						var name = listingAdmin.getActiveName( ButtonOptions[listingAdmin.activeSelected][j] );
						if (j > 0)
							body += "/";
						body += "<a href=javascript:listingAdmin.transfer("+listingAdmin.listings[x].id+","+ButtonOptions[listingAdmin.activeSelected][j]+")>"+name+"</a>"+
								'<span> <span><input type="radio" name="active'+listingAdmin.listings[x].id+'" id="'+ActiveStateNames[ButtonOptions[listingAdmin.activeSelected][j]]+'-'+listingAdmin.listings[x].id+'">';
					}
					body += "</td>"
				}
				else
					body += "No Option</td>";
				//body += "<td><a href=javascript:listingAdmin.transfer("+listingAdmin.listings[x].id+")>Transfer</a></td>"
				body += "</tr>";
			}
			$('.widefat tbody').html(body);
			var slider;

			$('.widefat input[type="radio"]').on('change',function() {
				if ($(this).prop('checked'))
					$('#controls #applyChanges').prop('disabled', false);
			});

			$('.widefat input[type="checkbox"]').on('change',function() {
				var name = $(this).attr('name').split("-");
				listingAdmin.organizeHomePageImgs(parseInt(name[1]), $(this).prop('checked'));
			});

			$('#controls #applyChanges').on('click', function() {
				listingAdmin.transferAll();
			})

			// $('.listing-image-single').hover(
			// 	function() {
			// 		console.log("entered hover");
			// 		$(this).animate({
			// 			width: "800px",
			// 			height: "640px"
			// 		}, 100, function(){});
			// 		$(this).children().animate({
			// 			width: "800px",
			// 			height: "640px"
			// 		}, 100, function(){});
			// 	},
			// 	function() {
			// 		console.log("leave hover");
			// 		$(this).animate({
			// 			width: "210px",
			// 			height: "120px"
			// 		}, 100, function(){});
			// 		$(this).children().animate({
			// 			width: "210px",
			// 			height: "120px"
			// 		}, 100, function(){});
			// 	}
			// );
			
			for(var x = 0; x < listingAdmin.listings.length; x++) {
				var slider;
				var targetName = 'ul.listing-gallery'+x;
			//$.each('ul.listing-gallery',function() {
				if ($(targetName).children('li').length > 1){
					slider = $(targetName).bxSlider({
					  auto: true,
					  pause: 3000,
					  mode: 'fade',
					  responsive: 'false',
					  controls: 'false',
					  autoHover: 'true' //,
					  // onSlideAfter: function($element, oldIndex, newIndex) {
					  // 		$element.hover(
							// 	function() {
							// 		console.log("entered hover:"+newIndex);
							// 		listingAdmin.animate = true;
							// 		element = $(this);
							// 		window.setTimeout(function() {
							// 			listingAdmin.expand(element);
							// 		}, 200);
							// 	},
							// 	function() {
							// 		console.log("leave hover:"+newIndex);
							// 		listingAdmin.animate = false;
							// 		$(this).parent().parent().parent().animate({
							// 			width: "210px",
							// 			height: "120px"
							// 		}, 100, function(){});
							// 		$(this).parent().parent().animate({
							// 			width: "210px",
							// 			height: "120px"
							// 		}, 100, function(){});
							// 		$(this).animate({
							// 			width: "210px",
							// 			height: "120px"
							// 		}, 100, function(){});
							// 	})
					  // }
					});
				}
			}
		}

		this.expand = function(what) {
			if (listingAdmin.animate) {
				listingAdmin.animate = false;
			}
			what.parent().parent().parent().animate({
				width: "800px",
				height: "640px"
			}, 100, function(){});
			what.parent().parent().animate({
				width: "800px",
				height: "640px"
			}, 100, function(){});
			what.animate({
				width: "800px",
				height: "640px"
			}, 100, function(){});
			listingAdmin.animate = false;
		}

		this.organizeHomePageImgs = function(id, adding) {
			var data = {};
			if (typeof id != 'undefined' &&
				typeof adding != 'undefined') {
				data.id = id;
				data.adding = adding;
			}
			$.ajax({
				url: listingAdmin.ajaxUrl,
				data: { query: 'home-page-img',
						data: {	id: id,
				 		 		mode: adding } 
					  },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during home-page-img: "+msg);
				},					
			  	success: function(data){
				  	if (data == null || data.status == null ||
				  		typeof data == 'undefined' || typeof data.status == 'undefined') 
				  		ahtb.open({ title: 'Error during home-page-img', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else if (data.status == 'fail') {
				  		ahtb.open({ title: 'Error during home-page-img', height: 150, html: '<p>'+data.data+'</p>' });
				  	}
				  	else {
				  		if (typeof data.data == 'object' )
				  			listingAdmin.homeImgs = $.map(data.data, function(value, index) {
													    return [value];
													});
				  		else
				  			listingAdmin.homeImgs = data.data;
						console.log("Got back home image indices: "+JSON.stringify(data.data));
					}
				}
			});
		}

		this.gotoListing = function(id) {
			if (listingAdmin.showReminderRefresh) {
				h = '<div id="informUserToRefresh">'+
						"<p>Refresh page to update listing's info.</p>"+
						'<div id="userStopReminder" style="display: inline">'+
							"<span>Don't remind me again: </span>"+
							'<input type="checkbox" id="stopReminder" />';
				var opened = function() {
					$('#informUserToRefresh #stopReminder').prop('checked', !listingAdmin.showReminderRefresh)
					$('#informUserToRefresh #stopReminder').on('click', function() {
					 	listingAdmin.showReminderRefresh = $(this).prop('checked') ? 0 : 1;
					 	setCookie('ReminderRefresh', listingAdmin.showReminderRefresh, 7);
					})
				}
		      	ahtb.open({ hideSubmit: false, height: 150, width: 300, title: 'Reminder', html: h, opened: opened });
				// ahtb.alert("Refresh page to update listing's info.",
				// 	{height: 150});
			}
			window.open(ah_local.wp+"/listing/1-"+id, '_blank');
		}

		this.getRowCount = function() {
			listingAdmin.gotRowCount = false;
			listingAdmin.rowCount = 0;
			var one = '';
			var two = '';
			var three = ''
			var whichOne = OptionState.SELLER;
			if (listingAdmin.option) {
				if (listingAdmin.lastOption.attr('id') == 'seller')
				{
					one = $('#optionChooser #firstName').val().trim();
					two = $('#optionChooser #lastName').val().trim();
					if (!one && !two && listingAdmin.option) {
						listingAdmin.gotRowCount = true;
						ahtb.open({ title: 'No name detected', height: 150, html: '<p>Please enter either first/last name or both.</p>' });
						return;
					}
					console.log("Seller option - first:"+one+", last:"+two);
				}
				else // must be 'address'
				{
					one = $('#optionChooser #street').val().trim();
					two = $('#optionChooser #city').val().trim();
					three = $('#optionChooser #state').val().trim();
					whichOne = OptionState.ADDRESS;
					if (!one && !two && !three && listingAdmin.option) {
						listingAdmin.gotRowCount = true;
						ahtb.open({ title: 'No address detected', height: 150, html: '<p>Please enter any part of an address.</p>' });
						return;
					}
					else
						console.log("Address option - street:"+one+", city:"+two+", state:"+three);
				}
			}

			console.log("getRowCount - active:"+listingAdmin.activeSelected+", option:"+listingAdmin.option+", which:"+whichOne+", one:"+one+", two:"+two+", three:"+three);
			$.ajax({
				url: listingAdmin.ajaxUrl,
				data: { query: 'row-count',
						data: {	active: listingAdmin.activeSelected,
								option: listingAdmin.option,
								price: listingAdmin.price,
								which: whichOne,
								first: one,
								second: two,
								third: three,
								haveKeywordMatch: listingAdmin.keywordsApplied } 
					  },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during getRowCount: "+msg);
					//listingAdmin.getRowCount();  // try again
				},					
			  	success: function(data){
				  	if (data == null || data.status == null ||
				  		typeof data == 'undefined' || typeof data.status == 'undefined') 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else if (data.status == 'fail') {
				  		ahtb.open({ title: 'No rows detected', height: 150, html: '<p>'+data.data+'</p>' });
				  	}
				  	else {
				  		listingAdmin.rowCount = data.data ? parseInt(data.data) : 0;
				  		$('.tally').html("# of listings: "+ listingAdmin.rowCount);
				  		console.log("RowCount for "+listingAdmin.getActiveName(listingAdmin.activeSelected)+" is "+listingAdmin.rowCount);
				  		listingAdmin.setCookies(listingAdmin.activeSelected);
				  		if ( ((listingAdmin.curPage+1)*listingAdmin.pageSize) > listingAdmin.rowCount)  // reset then
				  			listingAdmin.curPage = 0;
				  		
				  		listingAdmin.getPage(listingAdmin.curPage);

					  	if (!listingAdmin.rowCount) {
					  		var msg = 'For the search target: '+listingAdmin.getActiveName(listingAdmin.activeSelected)+", there appears to be no listings";
					  		if (listingAdmin.option) {
					  			if (listingAdmin.lastOption.attr('id') == 'seller') {
					  				var first = $('#optionChooser #firstName').val().trim();
									var last = $('#optionChooser #lastName').val().trim();
									msg += ", where firstName is "+(!first ? "blank" : first)+" and lastName is "+(!last ? "blank" : last);
									msg += ", no listings were found.";
					  			}
					  			else {
					  				one = $('#optionChooser #street').val().trim();
									two = $('#optionChooser #city').val().trim();
									three = $('#optionChooser #state').val().trim();
									msg += ", where street is "+(!one ? "blank" : one)+", city is "+(!two ? "blank" : two)+" and state is "+(!three ? "blank" : three);
									msg += ", no listings were found.";
					  			}
					  		}
					  		ahtb.open({ title: 'No listings detected', height: 180, html: '<p>'+msg+'</p>' });
					  	}
		  			}
					listingAdmin.gotRowCount = true;
					listingAdmin.setSlider();
		  		}
	  		})
		}

		this.transfer = function(listingID, dest){
			if (confirm("Are you sure you want to transfer listing: "+listingID+" to the "+listingAdmin.getActiveName(dest)+" category?")) {
				$.post(listingAdmin.ajaxUrl,
						{'query':'transfer-listing',
						 'data':{id:listingID,
						 		 active: dest,
						 		 user: ah_local.user }},
						function(){},
						'json')
					.done(function(data){
						if (data.status == 'OK'){ 
							listingAdmin.rowCount--;
							location.reload(); 
						}
					 	console.log(data.data);
					});
			}	
		}

		this.okToTransfer = function(id, dest) {
			for(var x in listingAdmin.listings) {
				if (listingAdmin.listings[x].id == id) {
					if (listingAdmin.listings[x].active != dest)
						return true;
					else
						return false;
				}
			}
		}

		this.transferAll = function() {
			var didTransfer = false;
			$('.widefat input[type="radio"]').each(function() {
				if ($(this).prop('checked')) {
					var attr;
					attr = $(this).attr('id');
					var pair = attr.split("-");
					console.log("Got "+pair[0]+", id:"+pair[1]);
					var id = parseInt(pair[1]);
					var dest = ActiveStateNames.indexOf(pair[0]);
					$(this).prop('checked', false);
					if (listingAdmin.okToTransfer(id, dest)) {					
						$.post(listingAdmin.ajaxUrl,
							{'query':'transfer-listing',
							 'data':{id: id,
							 		 active: dest,
							 		 user: ah_local.user }},
							function(){},
							'json')
						.done(function(data){
							if (data.status == 'OK'){ 
								listingAdmin.rowCount--;
								didTransfer = true;
							}
						 	console.log(data.data);
						});
					}
				}
			})

			window.setTimeout(function() {
				if (didTransfer)
					location.reload(); 
			}, 500);
		}

		this.applyKeyword = function(val) {
			listingAdmin.keywordsApplied = false;
			if (val.length) {
				ahtb.open({	html: '<div><span>Are you sure you want to search using keywords:'+val+'?</span></div>',
						   	width: 450,
							height: 150,
							buttons:[{text:'OK', action: function() {
								ahtb.close();
								$('#keywordOption .spinner').fadeIn(250, function(){});
								var words = val.split(",");
								for(var i in words)
									words[i] = words[i].trim().toLowerCase();

								var data = {
									query: 'applykeyword',
									data: { key: words,
											active: listingAdmin.activeSelected }
								};

								$.ajax({
									url: listingAdmin.ajaxUrl,
									data: data,
									dataType: 'JSON',
									type: 'POST',
									error: function($xhr, $status, $error){
										var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
										console.log("Error during applyKeyword: "+msg);
										$('#keywordOption .spinner').fadeOut(250, function(){});
										//listingAdmin.getRowCount();  // try again
									},					
								  	success: function(data){
								  		$('#keywordOption .spinner').fadeOut(250, function(){});
								  		if (data == null || data.status == null ||
								  			typeof data == 'undefined' || typeof data.status == 'undefined') 
								  			ahtb.open({ title: 'You Have Encountered an Error in applyKeyword', height: 150, html: '<p>Got a return of NULL</p>' });
								  		else if (data.status == 'OK'){ 
								  			var count = parseInt(data.data);
								  			if (count) {
								  				ahtb.alert(count+" listings matched the keywords, but actual number displayed will be limited by price and options.",
								  						   {height: 180});
								  				listingAdmin.keywordsApplied = true;
								  				listingAdmin.getRowCount();
								  			}
								  			else
								  				ahtb.alert("None of the listings matched the keyword(s)",
								  					 {height: 150});
								  		}
								  		else if (data.data)
								  			ahtb(data.data, {height: 150});
								  		else
								  			console.log("Some sort of failure in applyKeyword, no info.");

								  	}
								  });
							}},
							{text:'Cancel', action: function() {
								ahtb.close();
							}}]})
			}
			else {
				$('#keywordOption .spinner').fadeOut(250, function(){});
				listingAdmin.getRowCount();
				ahtb.alert("Please enter a keyword(s) or phrase(s) separated by commas.",
					{height: 150});
			}
		}

		this.cleanTags = function() {
			var data = {
				query: 'cleanTags',
				data: { active: listingAdmin.activeSelected }
			};

			$.ajax({
				url: listingAdmin.ajaxUrl,
				data: data,
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during cleanTags: "+msg);
					$('#keywordOption .spinner').fadeOut(250, function(){});
					//listingAdmin.getRowCount();  // try again
				},					
			  	success: function(data){
			  		$('#keywordOption .spinner').fadeOut(250, function(){});
			  		if (data == null || data.status == null ||
			  			typeof data == 'undefined' || typeof data.status == 'undefined') 
			  			ahtb.open({ title: 'You Have Encountered an Error in cleanTags', height: 150, html: '<p>Got a return of NULL</p>' });
			  		else if (data.status == 'OK'){ 
			  			var count = 0;
			  			if ( !(typeof data.data == 'undefined' || data.data == null) )
			  				count = parseInt(data.data);
			  			if (count) {
			  				ahtb.alert(count+" listings cleaned.",
			  						   {height: 180});
			  				listingAdmin.getPage(listingAdmin.curPage);
			  			}
			  			else
			  				ahtb.alert("None of the listings had any tags to be cleaned",
			  					 {height: 150});
			  		}
			  		else if (data.data)
			  			ahtb.alert(data.data, {height: 150});
			  		else
			  			console.log("Some sort of failure in cleanTags, no info.");

			  	}
			});
		}

		this.geocode = function() {
			var data = {
				query: 'geocode-nonlisthub'
			};

			$.ajax({
				url: listingAdmin.ajaxUrl,
				data: data,
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during geocode: "+msg);
					$('#keywordOption .spinner').fadeOut(250, function(){});
					//listingAdmin.getRowCount();  // try again
				},					
			  	success: function(data){
			  		$('#keywordOption .spinner').fadeOut(250, function(){});
			  		if (data == null || data.status == null ||
			  			typeof data == 'undefined' || typeof data.status == 'undefined') 
			  			ahtb.open({ title: 'You Have Encountered an Error in geocode', height: 150, html: '<p>Got a return of NULL</p>' });
			  		else if (data.status == 'OK'){ 
			  			var count = 0;
			  			if ( !(typeof data.data == 'undefined' || data.data == null) ) {
			  				ahtb.alert(data.data,
			  						   {height: 150});
			  				// listingAdmin.getPage(listingAdmin.curPage);
			  			}
			  			else
			  				ahtb.alert("None of the listings were geocoded",
			  					 {height: 150});
			  		}
			  		else if (data.data)
			  			ahtb.alert(data.data, {height: 150});
			  		else
			  			console.log("Some sort of failure in geocode, no info.");

			  	}
			});
		}
	}
	
	listingAdmin.init();
});