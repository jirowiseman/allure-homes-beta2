var tagAdmin
jQuery(document).ready(function($){
tagAdmin = new function(){
	this.ajaxURL = ah_local.tp+"/_admin/ajax.php";
	this.init = function(){
		ahtb.loading('Loading tags...',{
			hideTitle: true,
			height: 55,
			opened: function(){ tagAdmin.DB({
				query: 'get-tags',
				done: function(d){
					tagAdmin.tags = d;
					tagAdmin.categories = tags_categories_from_db;
					tagAdmin.print.tags();
					ahtb.close();
				}
			})}
		});
		$('a.add-new-tag').on('click',function(){
			ahtb.open({
				title: 'Add New Tag',
				height: 140,
				html: '<p><input type="text" placeholder="Enter a new tag" style="width: 90%" /></p>',
				buttons: [
					{ text: 'Add', action: function(){
						newVal = $.trim( $('#tb-submit input').val() );
						if (newVal.length > 0)
							tagAdmin.DB({ query: 'add-tag', data: newVal, done: function(){ location.reload() } });
					}},
					{ text: 'Cancel', action: function(){ ahtb.close() }},
				]
			});
		});
	}
	this.print = {
		tags: function(){
			for (var i in tagAdmin.tags) if (tagAdmin.tags[i]) {
				var tag = tagAdmin.tags[i];
				var h = '<tr tag-id="'+tag.id+'"'+(i%2===1 ? ' class="alternate"' : '')+'>'+
					'<td class="column-delete"><a class="delete-tag">x</a></td>'+
					'<td class="column-tag"><a class="edit-tag">'+tag.tag+'</a></td>'+
					'<td class="column-description" id="edit-description-'+tag.id+'" data-tid="'+tag.id+'"><a class="edit-description">';
					tag.description == null ? h+= '(none)' : h+= tag.description;
					h+= '</a></td>'+
					'<td class="column-type"><select class="edit-type" id="edit-type-'+tag.id+'" data-tid="'+tag.id+'">'+
						'<option value="0" '+(tag.type == 0 ? ' selected' : '')+'>Listing</option>'+
						'<option value="1" '+(tag.type == 1 ? ' selected' : '')+'>City</option>'+
					'</select></td>'+
					'<td class="column-category"><select class="edit-category" id="edit-category-'+tag.id+'" data-tid="'+tag.id+'">';
					h+= '<option value="null" disabled';
					if (tag.category == null) h+= ' selected';
					h+= '>(category)</option>';
					for (var j in tagAdmin.categories){
						h+= '<option value="'+j+'"';
						if (tag.category == tagAdmin.categories[j].category) h+= ' selected';
						h+= '>'+tagAdmin.categories[j].category+'</option>';
					}
					h+= '</select></td>'+
					'<td class="column-in-quiz">'+(tag.count_quiz ? tag.count_quiz.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0)+'</td>'+
					'<td class="column-in-listings">'+(tag.count_listings ? tag.count_listings.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0)+'</td>'+
					'<td class="column-in-cities">'+(tag.count_cities ? tag.count_cities.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0)+'</td>'+
				'</tr>';
				$('#tags tbody').append(h);
				$('[tag-id='+tag.id+'] a').on('click',function(){
					var tag_id = parseInt($(this).parent().parent().attr('tag-id'));
					if ($(this).hasClass('delete-tag')){
						ahtb.open({
							hideTitle: true,
							height: 105,
							html: '<p>Are you sure you want to delete this tag?</p>',
							buttons: [
								{ text: 'Delete Tag', action: function(){
									ahtb.loading('Deleting...',{ height: 60, hideTitle: true, opened: function(){
										tagAdmin.DB({
											query: 'delete-tag',
											data: { tag_id: tag_id },
											done: function(){
												$('[tag-id='+tag_id+']').remove();
												ahtb.close();
											}
										})
									}});
								}},
								{ text: 'Cancel', action: function(){ ahtb.close() }},
							]
						});
					} else {
						var type = null;
						var fields = {};
						if ($(this).hasClass('edit-tag')) type = 'tag';
						else if ($(this).hasClass('edit-description')) type = 'description';
						if (type !== null){
							fields[type] = $(this).html();
							if (fields[type] == '(none)') fields[type] = '';
							if (type == 'tag'){
								var h = '<input type="text" value="'+fields[type]+'" style="margin: 1em 0; width: 90%;" /><br/>';
								var height = 135;
								var $container = '#tb-submit input';
							} else if (type == 'description'){
								var h = '<textarea rows="5" style="float: left; display: block; width: 93.25%;">'+fields[type]+'</textarea>'+
								'<br style="display: block; position:relative; clear:both;" />';
								var height = 210;
								var $container = '#tb-submit textarea';
							}
							ahtb.open({
								title: 'Edit '+(type.charAt(0).toUpperCase() + type.slice(1)),
								html: h,
								height: height,
								width: 500,
								buttons: [
									{ text: 'Save', action: function(){
										var newVal = $.trim( $($container).val() );
										if (newVal == fields[type]) ahtb.close();
										else {
											fields[type] = newVal;
											ahtb.loading('Saving '+type+'...', { hideTitle: true, height: 55, opened: function(){
												tagAdmin.DB({
													query: 'update-tag',
													data:{ tag_id: tag_id, fields: fields },
													done: function() { $('[tag-id='+tag_id+'] .edit-'+type).html(newVal); ahtb.close() }
												});
											}})
										}
									} },
									{ text: 'Cancel', action: function(){ ahtb.close() } }
								]
							});
						}
					}

				});
				$('[tag-id='+tag.id+'] select').change(function(){
					if ($(this).hasClass('edit-category')){
						var newCat = parseInt($(this).val());
						var tag_id = parseInt( $(this).parent().parent().attr('tag-id') );
						ahtb.loading('Updating tag category...',{
							hideTitle: true,
							height: 55,
							opened: function(){ tagAdmin.DB({
								query: 'update-tag-category',
								data:{ tag_id: tag_id, category_id: newCat },
								done: function(d){ ahtb.close() }
							}) }
						});
					} else if ($(this).hasClass('edit-type')) {
						var newType = parseInt($(this).val());
						var tag_id = parseInt( $(this).parent().parent().attr('tag-id') );
						ahtb.loading('Updating tag type...',{
							hideTitle: true,
							height: 55,
							opened: function(){ tagAdmin.DB({
								query: 'update-tag',
								data:{ tag_id: tag_id, fields: { type: newType } },
								done: function(d){ ahtb.close() }
							}) }
						});
					}
				});
			}
		}
	}
	this.DB = function(x){
		if (this.dbQueue == null) this.dbQueue = [];
		if (this.dbRunning == null) this.dbRunning = false;
		if (x){
			if (this.dbRunning) { x.id = this.dbQueue.length; this.dbQueue.push(x); }
			else {
				this.dbRunning = true;
				if (x.before) x.before();
				$.post(tagAdmin.ajaxURL, {query: x.query, data: (x.data?x.data:null)}, function(){}, 'json').done(function(d){
					tagAdmin.DBdone();
					if (d.status != 'OK') { ahtb.alert(d.data, 'Error', {height: 60+(16*Math.round(d.data.length/75)) }); console.log(d); }
					else if (x.done) x.done(d.data);
					else console.log(d);
				});
			}
		}
	}
	this.DBdone = function(){
		this.dbRunning = false;
		this.DB(tagAdmin.dbQueue.shift());
	}
}
tagAdmin.init();
});