<?php
namespace AH;

require_once(__DIR__.'/../_classes/_Controller.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php');
require_once(__DIR__.'/../_classes/Logger.class.php');
require_once(__DIR__.'/../../../../wp-load.php');

function userIP() {
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	    $ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
	    $ip = $_SERVER['REMOTE_ADDR'];
	}

	if ( ($pos = strpos($ip, ',')) !== false)
		$ip = substr($ip, 0, $pos);
	
	return $ip;
}

global $sLogger;
$sLogger = new Logger(1, 'ajax-paywhirl');
$sLogger->log("ajax_paywhirl entered, uri:".$_SERVER['REQUEST_URI']);
$referer = isset($_SERVER["HTTP_REFERER"]) ? (!empty($_SERVER["HTTP_REFERER"]) ? strtolower($_SERVER["HTTP_REFERER"]) : "") : '';
$ip = userIP();

$homeURL = get_home_url();

// event->type we need to be concernced with:
// customer.created
// 	event->customer
//			first_name
//			last_name
//			email
//			phone
//			address (street)
//			city
//			state
//			zip
//			country
//			user_id  (paywhirl)
//			id (message)

// card.created (I think we can ignore this one)
// 	event->card->customer_id seems to be interesting... only begins to appear here, not in customer.created
// 	event->customer (same as above)

// subscription.created (maybe ignore this one too)
//	event->subscription
//			user_id  (paywhirl)
//			customer_id (should be the same one from card.created)
//			plan_id	  (not sure)

// invoice.created (point of payment)
// 	event->customer
//			user_id
//	event->invoice->subscription_id (maybe unique identifier this subscription event)
//	event->invoice->items[0]->user_id
//	event->invoice->items[0]->sku
//	event->invoice->items[0]->invoice_id

// subscription.deleted
// 	event->customer
//			first_name
//			last_name
//			email
//			phone
//			address (street)
//			city
//			state
//			zip
//			country
//			user_id  (paywhirl)
//	event->subscription
//			user_id
//			customer_id
//			plan_id
//			id (maybe the event->invoice->subscription_id from invoice.created)


// Retrieve the request's body and parse it as JSON
$input = @file_get_contents("php://input", true);

// if ($referer == '54.183.249.23' ||
// 	$ip == '54.183.249.23' ||
// 	$homeURL == '54.183.249.23') {
// 	$uri = $_SERVER['REQUEST_URI'];
// 	$sLogger->log("suspect Amazon AWS IP: 54.183.249.23, ip:$ip, referer:$referer, home:$homeURL, uri:$uri, input:".$input);
// 	$controller = new Controller();
// 	global $query_packet;
// 	global $data_packet;
// 	if (!empty($query_packet))
// 		$sLogger->log("query_packet:".print_r($query_packet, true));
// 	elseif (!empty($data_packet))
// 		$sLogger->log("data_packet:".print_r($query_packet, true));
// 	else
// 		$sLogger->log("no data packet sent in _POST or _GET");
// 	die;
// }

if (!empty($input) &&
	isJson($input)) {
	// $sLogger->log("input received:".print_r($input, true));
	$event_json = json_decode($input);

	// Do something with $event_json
	$sLogger->log("Invoking PayWhirl->handleEvent(), received:".print_r($event_json, true));

	require_once(__DIR__.'/../_classes/PayWhirl.class.php'); $PayWhirl = new PayWhirl(1);
	$retval = $PayWhirl->handleEvent($event_json);
	http_response_code(200); // PHP 5.4 or greater
	die;
}

$sLogger->log("About to invoke AJAX_Paywhirl");

// if here, then php://input was empty, so let's check $_POST/$_GET
class AJAX_Paywhirl extends Controller {
	private $logIt = true;

	public function __construct(){
		$in = parent::__construct();
		$this->log = new Log(__DIR__.'/../_classes/_logs/ajax-paywhirl.log');
		global $query_packet;
		global $data_packet;
		$in = empty($in) ? $data_packet : $in;
		set_time_limit(0);

		$uri = $_SERVER['REQUEST_URI'];
		$browser = getBrowser();

		try {
			if (empty($in) || !isset($in->query) || empty($in->query)) {
				// $msg = __FILE__.':('.__LINE__.') - invalid request sent from ip:'.$this->userIP().", browser: ".print_r(getBrowser(), true).", GET:".print_r($_GET, true).", POST:".print_r($_POST, true);
				$msg = 'invalid request sent from ip:'.$this->userIP().", browser:".$browser['shortname'].", referer:$referer, home:$homeURL, uri:$uri, in:".(!empty($in) ? print_r($in, true) : 'empty');
				$this->log($msg);
				$out = new Out('OK', $msg); //throw new \Exception (__FILE__.':('.__LINE__.') - no query sent from ip:'.$this->userIP().", broswer: ".print_r(getBrowser(), true));
				echo json_encode($out);
				die;
			}

			$msg = 'incoming request from ip:'.$this->userIP().", browser:".$browser['shortname'].", referer:$referer, home:$homeURL, uri:$uri, in:".(!empty($in) ? print_r($in, true) : 'empty');
			$this->log($msg);

			switch ($in->query){
				case 'testMe':
					$out = new Out('OK', 'test complete');
					break;
				default:
					$this->log("$in->query not handled");
			}
			if ($out) echo json_encode($out);
			else echo json_encode(new Out('fail', "No handler for $in->query"));
		}
		catch (\Exception $e) { parseException($e, true); die(); }
	}

}
new AJAX_Paywhirl();




