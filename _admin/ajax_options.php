<?php
namespace AH;
require_once(__DIR__.'/../_classes/_Controller.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php');

class AJAX_Options extends Controller {
	// comes from Controller now
	// global $timezone_adjust;
	// protected $timezone_adjust = $timezone_adjust;
	private $logIt = true;

	public function __construct(){
		$in = parent::__construct();
		set_time_limit(0);
		try {
			$this->log = $this->logIt ? new Log(__DIR__.'/../_classes/_logs/ajax-options.log') : null;
			if (empty($in->query)) {
				// $msg = __FILE__.':('.__LINE__.') - invalid request sent from ip:'.$this->userIP().", browser: ".print_r(getBrowser(), true).", GET:".print_r($_GET, true).", POST:".print_r($_POST, true);
				$msg = 'invalid request sent from ip:'.$this->userIP().", browser: ".print_r(getBrowser(), true).", GET:".print_r($_GET, true).", POST:".print_r($_POST, true);
				$this->log($msg);
				$out = new Out('OK', $msg); //throw new \Exception (__FILE__.':('.__LINE__.') - no query sent from ip:'.$this->userIP().", broswer: ".print_r(getBrowser(), true));
				echo json_encode($out);
				die;
				// if (!isset($in->query))
				// 	$in->query = "bust";
			}
			// $this->log("ajax-analytics doing $in->query");
			switch ($in->query){
				case 'update-option':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['option'])) throw new \Exception('No option sent.');
					if (empty($in->data['name'])) throw new \Exception('No name sent.');
					// if (empty($in->data['session_id'])) throw new \Exception("No session_id sent for type:.{$in->data['type']}, origin:{$in->data['origin']}, what:".(isset($in->data['what']) ? $in->data['what'] : ''.", value_str:".(isset($in->data['value_str']) ? $in->data['value_str'] : '').", value_int:".(isset($in->data['value_int']) ? intval($in->data['value_int']) : 0)));

					// $this->exitNow("update-option is running");

					$option = $in->data['option'];
					$name = $in->data['name'];

					$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>$name]]);
					if (empty($opt)) {
						$this->fixOptions($option);
						if (gettype($option) == 'array') {
							if (isset($option['agentID'])) 
								$option = [(object)$option]; // wrap with array
						}
						elseif (isset($option->agentID))
							$option = [$option]; // wrap with array

						$x = $this->getClass('Options')->add((object)['opt'=>$name,
																	  'value'=>json_encode($option)]);
						$out = new Out(!empty($x) ? 'OK' : 'fail', !empty($x) ? "Added new option: $name" : "Failed to add option: $name");
					}
					else {
						$existingOpt = $opt[0]->value;
						$this->fixOptions($option);
						$incomingOption = json_encode($option);
						if ($existingOpt != $incomingOption) {// double check
							$x = $this->getClass('Options')->set([(object)['where'=>['opt'=>$name],
																		   'fields'=>['value'=>json_encode($option)]]]);
							$out = new Out(!empty($x) ? 'OK' : 'fail', !empty($x) ? "Updated $name" : "Failed to update $name");
						}
						else
							$out = new Out('OK', "Nothing changed for $name");
					}
					break;

				case 'update-one-option':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['option'])) throw new \Exception('No option sent.');
					if (empty($in->data['name'])) throw new \Exception('No name sent.');
					// if (empty($in->data['session_id'])) throw new \Exception("No session_id sent for type:.{$in->data['type']}, origin:{$in->data['origin']}, what:".(isset($in->data['what']) ? $in->data['what'] : ''.", value_str:".(isset($in->data['value_str']) ? $in->data['value_str'] : '').", value_int:".(isset($in->data['value_int']) ? intval($in->data['value_int']) : 0)));

					// $this->exitNow("update-option is running");

					$option = $in->data['option'];
					$name = $in->data['name'];
					$this->fixOptions($option);

					$option = (object)$option;

					$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>$name]]);
					if (empty($opt)) {
						if (isset($option->agentID))
							$option = [$option]; // wrap with array

						$x = $this->getClass('Options')->add((object)['opt'=>$name,
																	  'value'=>json_encode($option)]);
						$out = new Out(!empty($x) ? 'OK' : 'fail', !empty($x) ? "Added new option: $name" : "Failed to add option: $name");
					}
					else {
						$existingOpt = json_decode($opt[0]->value); // convert to objects
						if ( gettype($existingOpt) == 'object') { // then must be a singleton
							$incomingOption = json_encode($option);
							if ($opt[0]->value != $incomingOption) {// double check
								$x = $this->getClass('Options')->set([(object)['where'=>['opt'=>$name],
																			   'fields'=>['value'=>$incomingOption]]]);
								$out = new Out(!empty($x) ? 'OK' : 'fail', !empty($x) ? "Updated $name" : "Failed to update $name");
							}
							else
								$out = new Out('OK', "Nothing changed for $name");
						}
						else {
							$update = false;
							$found = false;
							foreach($existingOpt as $i=>$existing) {
								if ($existing->agentID == $option->agentID) {
									$found = true;
									if (json_encode($existing) != json_encode($option)) {
										$existingOpt[$i] = $option;
										$update = true;
									}
								}
							}
							if (!$found) {
								$existingOpt[] = $option;
								$update = true;
							}
							if ($update) {
								$x = $this->getClass('Options')->set([(object)['where'=>['opt'=>$name],
																			   'fields'=>['value'=>json_encode($existingOpt)]]]);
								$out = new Out(!empty($x) ? 'OK' : 'fail', !empty($x) ? "Updated $name" : "Failed to update $name");
							}
							else
								$out = new Out('OK', "Nothing changed for $name");
						}
					}
					break;
			}
			if ($out) echo json_encode($out);
			else echo json_encode(new Out('fail', "No handler for $in->query"));
		}
		catch (\Exception $e) { parseException($e, true); die(); }
	}

	protected function fixOptions(&$options) {
		foreach($options as $id=>&$value) {
			switch(gettype($value)) {
				case 'string':
					$value = is_numeric($value) ? (strpos($value,".") !== false ? floatval($value) : intval($value)) :
							 ($value === '' ? '' :
							 (strtolower($value) == 'yes' || strtolower($value) == 'no' ? $value : 
							 (strtolower($value) == 'off' || strtolower($value) == 'on' ? $value : 
							 (filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) !== null ? is_true($value) : $value))));
					break;
				case 'object':
					$this->fixOptions($value);
					break;
				case 'array':
					$this->fixOptions($value);
					break;
				case 'integer':
					break;
			}
			unset($value);
		}
	}
}
new AJAX_Options();
