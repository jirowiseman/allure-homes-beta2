<?php
namespace AH;

require_once(__DIR__.'/../_classes/Utility.class.php');
require_once(__DIR__.'/../_classes/InvitationBaseSet.php');
require_once(__DIR__.'/../_classes/AWS.class.php');
require_once(__DIR__.'/../_classes/ListHubData.class.php');


/* Just the AJAX for calls from developer.php (WP Admin backend) */
require_once(__DIR__.'/../_classes/_Controller.class.php');

ini_set('display_startup_errors',1);
ini_set('display_errors',1);
set_time_limit(0);
error_reporting(0);
error_reporting(-1);

define('RESTART_APACHE_AT_LOAD', 5.0);

class DeveloperDispatcher extends Controller {
	private $logIt = true;
	// comes from Controller now
	// global $timezone_adjust;
	// private $timezone_adjust = $timezone_adjust;

	public function __construct(){
		$in = parent::__construct();
		try {
			// if (empty($in->query)) throw new \Exception (__FILE__.':('.__LINE__.') - no query sent');
			if (empty($in->query)) throw new \Exception ('no query sent');
			$this->log = $this->logIt ? new Log(__DIR__.'/../_classes/_logs/ajax-developer.log') : null;
			if ($in->query != 'listhubcurrentprogress' &&
				$in->query != 'ping' &&
				$in->query != 'haveactiveparallelprocessing' &&
				$in->query != 'getlastprogresscount' &&
				$in->query != 'checkcronjob' &&
				$in->query != 'check-chunk' &&
				$in->query != 'listhubparallel' &&
				$in->query != 'getpprecordrange')
				$this->log("ajax_developer - query: $in->query, ev:".(isset($_POST['everest']) ? $_POST['everest'] : 'N/A').", mt:".(isset($_POST['mountain']) ? $_POST['mountain'] : 'N/A'));
			if ($in->query == 'versionId') {
				$_POST['myVersion'] = $this->getClass("ListHubData")->version();
				return;
			}
			if ($in->query == 'trickortreat') {
				$t = $this->getClass("Options");
				$q = new \stdClass();
				$q->where = array('opt'=>$_POST['everest']);
				$x = $t->get($q);
				if (!empty($x)) // update with new
					$t->delete($q->where); // get rid of the old
				// add new one
				$mt = array('opt'=>$_POST['everest'],
							'value'=>$_POST['mountain']);
				$x = $t->add((object)$mt);
				$out = new Out('OK', 'Treat is in '.$x.' bucket.');
			}
			else if ( !($x = $this->getClass("Options")->get((object)array('where'=>array('opt'=>$_POST['everest'])))) ||
				   $x[0]->value != $_POST['mountain'] )
			{
				$out = new Out(0, 'ERROR 401: Unauthorized entry for '.$in->query." x:".(!empty($x) ? $x[0]->value : "N/A").", y:".$_POST['mountain']); 
			}
			else switch ($in->query){
				case 'listings-update-from-tag-space':
					$this->getClass('ListHubData')->createDebugFile();
					$this->getClass('ListHubData')->beginOutputBuffering();
					$out = $this->getClass('ListHubData')->checkListingTagSpaces(true);
					$this->getClass('ListHubData')->stopOutputBuffering();
					break;

				case 'cities-update-from-tag-space':
					$this->getClass('ListHubData')->createDebugFile();
					$this->getClass('ListHubData')->beginOutputBuffering();
					$out = $this->getClass('ListHubData')->checkCityTagSpaces(true);
					$this->getClass('ListHubData')->stopOutputBuffering();
					break;

				case 'restart-apache':
					$cmd = "/usr/bin/schedule-restart";
					$out = [];
					if ( strpos( strtolower(php_uname()), 'linux') !== false) {
						exec($cmd, $out);
						$out = new Out('OK', $out);
					}
					else
						$out = new Out('fail', "Only on linux");
					break;

				case 'fix-portal-owners':
					$Sellers = $this->getClass('Sellers');
					global $wpdb;
					$sql = 'SELECT a.*, c.meta_value AS nickname from '.$Sellers->getTableName().' AS a ';
					$sql.= 'INNER JOIN '.$wpdb->prefix.'users AS b ON a.author_id = b.id ';
					$sql.= 'INNER JOIN '.$wpdb->prefix.'usermeta AS c ON c.user_id = b.id ';
					$sql.= "WHERE c.meta_key = 'nickname' ";
 					$sql.= "AND c.meta_value NOT LIKE '%unknown%' ";
 					$sql.= 'AND !( a.flags &8 ) ';
 					$this->log("fix-portal-owners - sql:$sql");
 					
 					$sellers = $Sellers->rawQuery($sql);
 					$fixed = 0;
 					if (!empty($sellers)) {
 						foreach($sellers as $seller) {
 							$fixed++;
 							$fields['flags'] = $seller->flags | SELLER_IS_PREMIUM_LEVEL_1;
 							$metas = [];
 							$nicknameMeta = null;
 							$amOrder = null;
 							$havePortalItem = false;
 							$haveOrder = false;
 							$sellerId = $seller->id;
 							$needOrderUpdate = false;
 							$needUpdate = false;
 							$seller->meta = json_decode($seller->meta);
 							$portal = $seller->nickname;
 							foreach($seller->meta as $meta) {
 								$meta = (object)$meta;
 								if (!isset($meta->action)) {
 									// $this->log("fix-portal-owners - no action for sellerId:$sellerId, meta:".print_r($meta, true));
 								}
 								elseif ($meta->action == SELLER_NICKNAME)
 									$nicknameMeta = $meta;
 								unset($meta);
 							}

 							foreach($seller->meta as &$meta) {
 								$meta = (object)$meta;
 								if (!isset($meta->action)) {
 									// $this->log("fix-portal-owners - no action for sellerId:$sellerId, meta:".print_r($meta, true));
 									if (isset($meta->learntools)) {
	 									$meta->action = SELLER_COMPLETED_CARDS;
	 									$needUpdate = true;
	 									$this->log("fix-portal-owners - added action tag of SELLER_COMPLETED_CARDS to meta for sellerId:$sellerId");
	 								}
	 							}
 								elseif ($meta->action == SELLER_AGENT_ORDER) {
 									$haveOrder = true;
 									$amOrder = $meta;
									if (!empty($amOrder->item)) foreach($amOrder->item as &$sellerItem) {
										$sellerItem = (object)$sellerItem;
										if ($sellerItem->type == ORDER_PORTAL) {
											if ($sellerItem->mode != ORDER_BOUGHT) {
												$sellerItem->mode = ORDER_BOUGHT;
												$needOrderUpdate = true;
												$sellerItem->order_id = DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM;
												$sellerItem->inviteUsedDate = !empty($nicknameMeta) && isset($nicknameMeta->inviteUsedDate) ? $nicknameMeta->inviteUsedDate : date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
												if (!isset($sellerItem->inviteCode) ||
													empty($sellerItem->inviteCode)) {
													$sellerItem->inviteCode = !empty($nicknameMeta) && isset($nicknameMeta->inviteCode) ? $nicknameMeta->inviteCode : 'freeportal';
													$sellerItem->cart_item_key = !empty($nicknameMeta) && isset($nicknameMeta->inviteCode) ? $nicknameMeta->inviteCode : 'freeportal';
												}
												if (!isset($sellerItem->nickname) ||
													$sellerItem->nickname != $seller->nickname)
													$sellerItem->nickname = $seller->nickname;
												$this->log("fix-portal-owners - updated SELLER_AGENT_ORDER with ORDER_PORTAL - sellerId:$sellerId, portal:$portal");
											}
											$havePortalItem = true;
										}
										unset($sellerItem);
									}
									if (!$havePortalItem) {
										$sellerItem = new \stdClass();
										$sellerItem->mode = ORDER_BOUGHT;
										$sellerItem->type = ORDER_PORTAL;
										$sellerItem->order_id = DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM;
										$sellerItem->inviteCode = !empty($nicknameMeta) && isset($nicknameMeta->inviteCode) ? $nicknameMeta->inviteCode : 'freeportal';
										$sellerItem->inviteUsedDate = !empty($nicknameMeta) && isset($nicknameMeta->inviteUsedDate) ? $nicknameMeta->inviteUsedDate : date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
										$sellerItem->cart_item_key = !empty($nicknameMeta) && isset($nicknameMeta->inviteCode) ? $nicknameMeta->inviteCode : 'freeportal';
										$sellerItem->nickname = $seller->nickname;
										$sellerItem->priceLevel = GRADE_6;
										$sellerItem->subscriptionType = MONTHLY_SUBSCRIPTION;
										$needOrderUpdate = true;
										$this->log("fix-portal-owners - added to SELLER_AGENT_ORDER, ORDER_PORTAL - sellerId:$sellerId, portal:$portal");
										$amOrder->item[] = $sellerItem;
									}
 								}
 								elseif (isset($meta->action)) {
 									if ($meta->action == SELLER_NICKNAME &&
 									    $meta->nickname != $portal) {
 										$meta->nickname = $portal;
 										$needUpdate = true;
 									}
 								}
 
 								$metas[] = $meta;
 								unset($meta);
 							}
 							if (!$haveOrder) {
 								$item = new \stdClass();
								$item->type = ORDER_PORTAL;
								$item->mode = ORDER_BOUGHT;
								$item->order_id = DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM;
								$item->inviteCode = !empty($nicknameMeta) && isset($nicknameMeta->inviteCode) ? $nicknameMeta->inviteCode : 'freeportal';
								$item->priceLevel = GRADE_6;
								$item->subscriptionType = MONTHLY_SUBSCRIPTION;
								$item->inviteUsedDate = !empty($nicknameMeta) && isset($nicknameMeta->inviteUsedDate) ? $nicknameMeta->inviteUsedDate : date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
								$item->cart_item_key = !empty($nicknameMeta) && isset($nicknameMeta->inviteCode) ? $nicknameMeta->inviteCode : 'freeportal';
								$item->nickname = $seller->nickname;
								$amOrder = (object) ['action'=>SELLER_AGENT_ORDER,
													 'order_id_last_purchased'=>0,
													 'order_id_last_cancelled'=>0,
													 'agreed_to_terms'=>0,
													 'item'=>[$item]
												 	];
								$metas[] = $amOrder;
								$needOrderUpdate = true;
								$this->log("fix-portal-owners - added new SELLER_AGENT_ORDER with ORDER_PORTAL - sellerId:$sellerId, portal:$portal");
 							}

 							if (!$nicknameMeta) {
 								$meta = new \stdClass();
								$meta->action = SELLER_NICKNAME;
								$meta->nickname = $portal;
								$meta->inviteCode = 'freeportal';
								$meta->inviteUsedDate = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
								$metas[] = $meta;
								$needUpdate = true;
								$this->log("fix-portal-owners - added SELLER_NICKNAME, sellerId:$sellerId, portal:$portal");
 							}
 							if ($needOrderUpdate ||
 								$needUpdate)
 								$fields['meta'] = $metas;
 							$Sellers->set([(object)['where'=>['id'=>$seller->id],
 													'fields'=>$fields]]);
 							unset($fields, $metas);
 						}
 					}
 					$out = new Out('OK', "fixed $fixed sellers");
					break;
				case 'prep-reload':
					$retval = $this->getClass('Register')->loginUser('gtvracer','@38di6530#!88');
					$out = new Out($retval === true ? 'OK' : 'fail', $retval === true ? 1 : 0);
					break;
				case 'alert-overload':
					$which = $_POST['which'];
					$load = $_POST['load'];
					$duration = $_POST['duration'];
					$havePP = $_POST['havePP'];
					$haveIP = $_POST['haveIP'];
					$haveMyOwn = $_POST['haveMyOwn'];
					$haveIPCustom = $_POST['haveIPCustom'];
					$duration = $_POST['duration'];

					$diskUsage = [];
					$cmd = "getImageIoStats.sh";
					if ($which == 'engr1')
						$cmd = "ssh www-data@192.168.1.3 $cmd";
					@exec($cmd, $diskUsage);
					$line = explode(' ', $diskUsage[0]);
					$this->log("alert-overload for $which got for image disk usage:".$diskUsage[0]);
					$diskUsage = array_pop( $line );
					$Email = $this->getClass('Email');
					$Email->textMessage('8312344865', 'Cpu Overloading', "$which is now at $load for $duration secs, image disk load:$diskUsage");
					$out = new Out('OK', "messaged Tom");

					$isLive = strpos($which, 'live') !== false;
					if ($isLive) {
						$this->log("alert-overload checking for active processing.");
						$opt = $this->getClass('Options', 1)->get((object)['or'=>['opt'=>['activePP','activeIP','activeParseToDb']]]);
						if (empty($opt)) {
							$this->log("alert-overload for which has no active PP, IP or ParseToDb with load:$load");
							if (floatval($load) >= RESTART_APACHE_AT_LOAD) {
								$cmd = "/usr/bin/schedule-restart";
								$output = [];
								if ( strpos( strtolower(php_uname()), 'linux') !== false) {
									exec($cmd, $output);
									$this->log("alert-overload called out $cmd");
									$msg = "$cmd is executed.";
									if (!empty($output)) foreach($output as $line)
										$msg .= "  $line";
									$Email->textMessage('8312344865', 'Apache restart scheduled', $msg);
								}
							}
						}
						else
							$this->log("alert-overload for which has active PP, IP or ParseToDb, with load:$load");
					}
					break;

				case 'test-email':
					$Email = $this->getClass('Email');
					$banner_orig = get_option('email_banner_img');
					update_option('email_banner_img', get_template_directory_uri()."/_img/_banners/_email/portal_agent.jpg");
					$Email->sendMail('tomtong58@gmail.com', "Testing banner", "Hey, did you get it?");
					update_option('email_banner_img', $banner_orig);
					$out = new Out('OK');
					break;

				case 'test-ipinfo':
					if (!isset($_POST['ip'])) throw new \Exception ('No ip sent');
					$result = null;
					$ip = $_POST['ip'];
					$this->log("test-ipinfo got ip:$ip");
					$call = "http://ipinfo.io/".$ip;
					// $call = "http://www.whatsmyip.org/ip-geo-location/?ip=".$ip;
					// $raw = file_get_contents($call);

					$raw = file_get_contents( 'http://api.ip2location.com/?' . 'ip='.$ip.'&key=demo' . '&package=WS10&format=json', true );
					$result = @json_decode( $raw );
					$this->log("test-ipinfo got back - raw:".(!empty($raw) ? print_r($raw, true) : "N/A").", result:".(!empty($result) ? print_r($result, true) : "N/A"));
					if (empty($result) ||
						(!isset($result->city) &&
						 !isset($result->city_name))) {
						if (is_callable(geoip_record_by_name)) {
							$result = geoip_record_by_name($ip);
							$this->log("getCity - geoip_record_by_name got back:".(!empty($result) ? print_r($result, true) : "N/A"));
							if (!empty($result))
								$result = (object)$result;
						}
					}
					$out = new Out('OK', $result);
					break;

				case 'check-authored-listing-images':
					$listings = $this->getClass('Listings')->get((object)['where'=>['author_has_account'=>1],
																		  'greaterthan'=>['author'=>1]]);
					if (empty($listings)) {
						$out = new Out('fail', 'Could not find any listings with author_has_account == 1');
						break;
					}

					$count = 0;
					foreach($listings as $listing) {
						if (!empty($listing->images)) {
							foreach($listing->images as $image) {
								if (isset($image->file) && substr($image->file, 0, 4 ) != 'http') {  // then a static image exists for this 
									$result = $this->getClass('Image')->generateSizes('listings', __DIR__.'/../_img/_listings/uploaded/'.$image->file);
									if ($result->status == 'OK')
										$count++;
								}
							}
						}
					}
					$out = new Out('OK', "Fixed $count images from ".count($listings)." listings");
					break;

				case 'write-site-maps':
					$SiteMapper = $this->getClass('SiteMapper', 1);
					$count = $SiteMapper->generateSiteMaps();
					$out = new Out($count ? 'OK' : 'fail', ['count'=>$count]);
					break;

				case 'read-temp-data':
					$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'TempData']]);
					$out = new Out(!empty($opt), !empty($opt) ? $opt[0]->value : "not found");
					break;

				case 'run-command':
					if (!isset($_POST['cmd'])) throw new \Exception ('No cmd sent');
					$cmd = stripslashes($_POST['cmd']);
					$this->log("run-command - $cmd, from ".get_home_url());
					$out = [];
					if ( strpos( strtolower(php_uname()), 'linux') !== false) {
						exec($cmd, $out);
					// $Process = $this->getClass('Process', $cmd);
					// while( $Process->status() )
					// 	sleep(1);
					//$out = new Out('OK', "Unlocked ".$in->data['user']);
						$out = new Out('OK', $out);
					}
					else
						$out = new Out('fail', "Only on linux");
					break;

				case 'run-command-from-remote':
					if (!isset($_POST['cmd'])) throw new \Exception ('No cmd sent');
					$cmd = stripslashes($_POST['cmd']);
					$cmd = "ssh www-data@192.168.1.3 $cmd";
					$this->log("run-command - $cmd");
					$out = [];
					if ( strpos( strtolower(php_uname()), 'linux') !== false) {
						exec($cmd, $out);
					// $Process = $this->getClass('Process', $cmd);
					// while( $Process->status() )
					// 	sleep(1);
					//$out = new Out('OK', "Unlocked ".$in->data['user']);
						$out = new Out('OK', $out);
					}
					else
						$out = new Out('fail', "Only on linux");
					break;
				case 'safe-ip':
					$cmd = "ssh www-data@192.168.1.3 ".'"'."echo 'sshd: ".$this->userIP()."' >> /etc/hosts.allow".'"';
					$this->log("safe-ip - $cmd");
                    $out = [];
                    if ( strpos( strtolower(php_uname()), 'linux') !== false) {
                        exec($cmd, $out);
                        $cmd = "scp www-data@192.168.1.3:/etc/hosts.allow /etc/hosts.allow";
                        exec($cmd, $out);
                        $out = new Out('OK', $out);
                    }
                    else
                            $out = new Out('fail', "Only on linux");
					break;

				case 'test-or':
					$q = new \stdClass();
					$q->like = ['first_name'=>'Jiro'];
					$q->like2 = ['last_name'=>'Wiseman'];
					// $q->or = ['elements' => [['phone'=>6784940644],
					// 		   				 ['mobile'=>1234567890]] ];
					$q->or = ['phone' => [2147483647, 1234567890] ];
					$x = $this->getClass('Sellers', 1)->get($q);
					$out = new Out('OK', !empty($x) ? $x[0]->first_name.' '.$x[0]->last_name : "no worke");
					break;
				case 'do-backup':
					$this->process("backup", false); // false for no wait till done
					$out = new Out('OK',"Started backup");
					break;
				case 'update-most-viewed-listings':
					$sql = 'SELECT a.id, b.clicks, b.viewed FROM '.$this->getClass('Listings')->getTableName().' AS a ';
					$sql.= 'INNER JOIN '.$this->getClass('Listings')->getTableName('listings-viewed').' AS b ';
					$sql.= 'WHERE b.listing_id = a.id ';

					$list = $this->getClass('Listings')->rawQuery($sql);
					usort($list, function($a, $b) {
						if ($a->clicks != $b->clicks)
							return $b->clicks - $a->clicks;
						else
							return strtotime($b->viewed) - strtotime($a->viewed);
					});

					// $ids = $this->getClass('Listings')->get((object)['what'=>['id'],
					// 												 'orderby'=>'clicks',
					// 												 'order'=>'desc',
					// 												 'limit'=>100]);
					$updates = [];
					$i = 0;
					foreach($list as $id) {
						$updates[] = $id->id;
						if ($i++ == 100)
							break;
					}
					$this->getClass('Options')->set([(object)['where'=>['opt'=>'ListingsMostViewed'],
															  'fields'=>['value'=>json_encode($updates)]]]);
					$out = new Out('OK',"Updated 100 most viewed listings into Options::ListingsMostViewed");
					break;
				case 'recover-one-chunk':
					// if (!isset($_POST['startId'])) throw new Exception("No startId supplied");
					if (!isset($_POST['chunkSize'])) throw new \Exception("No chunkSize supplied");
					// $startId = $_POST['startId'];
					$chunkSize = $_POST['chunkSize'];
					$specialOp = !isset($_POST['specialOp']) ? intval($_POST['specialOp']) : 0;
					// if (count($list) == 0) throw new \Exception("Empty list supplied");
					$lhd = $this->getClass('ListHubData');
					$this->log("recover-one-chunk - specialOp:$specialOp, chunkSize:$chunkSize");
					$out = $lhd->processChunkRecover($chunkSize, $specialOp);
					break;

				case 'set-first-error-chunk-to-fatal':
					$chunk = $this->getClass('ListhubChunkStatus')->getFirst('id',['where'=>['status'=>3]]);
					if (!empty($chunk)) {
						$chunk = array_pop($chunk);
						$this->log("set-first-error-chunk-to-fatal - subDomain:$chunk->subdomain, startId:$chunk->startid, setting to FATAL");
						$this->getClass('ListhubChunkStatus')->set([(object)['where'=>['id'=>$chunk->id],
																			 'fields'=>['status'=>4]]]);
					}
					$out = new Out('OK', !empty($chunk) ? $chunk->id : 0);
					break;

				case 'set-chunk-status':
					$subDomain = $_POST['subDomain'];
					$startId = $_POST['startId'];
					$status = $_POST['status'];
					$x = $this->getClass('ListhubChunkStatus')->get((object)['where'=>['subdomain'=>$subDomain,
																					   'startid'=>$startId]]);
					if (!empty($x)) {
						if ($x[0]->status != $status) {
							$x = $this->getClass('ListhubChunkStatus')->set([(object)['where'=>['subdomain'=>$subDomain,
																							   'startid'=>$startId],
																					  'fields'=>['status'=>$status]]]);
							if (!empty($x))
								$x = $x[0][1];
							else
								$x = -1;
						}
						else {
							$this->log("set-chunk - subDomain:$subDomain, startId:$startId, already has status:$status");
							$x = 0;
						}
					}
					else
						$x = $this->getClass('ListhubChunkStatus')->add((object)['subdomain'=>$subDomain,
																				'startid'=>$startId,
																				'status'=>$status]);
					$out = new Out('OK', $x);
					break;
				case 'check-chunk':
					$subDomain = $_POST['subDomain']; //which subDomain
					$startId = $_POST['startId'];
					// $doIP = isset($_POST['doIP']) ? is_true($_POST['doIP']) : 0;
					$specialOperation = !isset($_POST['specialOp']) ? intval($_POST['specialOp']) : 0;
					$doingImagePP = !($specialOperation >= IMAGE_BORDER_PARALLEL && $specialOperation <= IMAGE_BORDER_PARALLEL_BATCH) ? 0 :
							 			 ($specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 1 : 2);
					$chunkTable = !$doingImagePP ? 'ListhubChunkStatus' :
								   ($doingImagePP == 1 ? 'ImageChunkStatus' : 'CustomImageChunkStatus');
					$x = $this->getClass($chunkTable, 1)->get((object)['where'=>['subdomain'=>$subDomain,
																			     'startid'=>$startId]]);
					if (empty($x)) {// retry with just startid
						$x = $this->getClass($chunkTable, 1)->get((object)['where'=>['startid'=>$startId]]);
						$this->log("check-chunk, retried with just startid:$startId, and ".(empty($x) ? 'failed to find one' : 'found one'));
					}

					$this->log("check-chunk for $chunkTable, $subDomain:$startId was ".(empty($x) ? 'empty' : 'valid')." - time:".date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)));
					$out = new Out('OK', empty($x) ? 0 : $x[0]);
					break;
				case 'post-to-progress':
					$msg = isset($_POST['msg']) ? $_POST['msg'] : "empty msg";
					if (gettype($msg) != 'string')
						$msg = json_encode($msg);
					$this->log("post-to-progress - $msg");
					$this->getClass('ListHubData')->updateToDb($msg, POST_FROM_FE);
					$out = new Out('OK', 'message posted');
					break;
				case 'record-parse-result':
					$result = $_POST['result'];
					$lhd = $this->getClass('ListHubData');
					$lhd->recordParseResult($result);
					$out = new Out('OK', "Parse result recorded");
					break;
				case 'import-active-listing-images':
				case 'create-images-from-imported':
					$lhd = $this->getClass('ListHubData');
					$useImpressions = isset($_POST['useImpressions']) ? is_true($_POST['useImpressions']) : false;
					$out = $lhd->getListingImportImageCount($useImpressions);
					break;
				case 'active-image-count':
					$lhd = $this->getClass('ListHubData');
					$mode = $_POST['mode']; // live or waiting
					$op = $_POST['op'];
					$onlynew = $_POST['onlynew']; // not used
					$fromLast = isset($_POST['fromLast']) && ($_POST['fromLast'] == 1 || $_POST['fromLast'] == '1');
					$fromCron = isset($_POST['fromCron']) ? is_true($_POST['fromCron']) : 0;
					$out = $lhd->getActiveListingCount($mode, $fromLast, $fromCron, $op);
					break;
				case 'get-seller-count':
					$count = $this->getClass('Sellers')->count();
					$out = new Out('OK', $count);
					break;
				case 'get-block-count':
					$this->log("Calling getClass('ListhubBlocks')->count()");
					$count = $this->getClass('ListhubBlocks')->count();
					$this->log("getClass('ListhubBlocks')->count() returned $count");
					$out = new Out('OK', $count);
					break;
				case 'get-active-listings-count':
				case 'start-set-first-image':
					$count = $this->getClass('Listings')->count((object)['where'=>['active'=>1]]);
					$out = new Out('OK', $count);
					break;
				case 'get-active-zip-count':
					$out = $this->getClass('MedianPrices')->getActiveZipCount();
					break;
				case 'consolidate-median-prices':
					$out = $this->getClass('MedianPrices', 1)->consolidateDailyMedianPrices();
					break;
				case 'reset-daily-prices':
					$out = $this->getClass('MedianPrices', 1)->resetDaily();
					break;
				case 'get-active-cities-count':
					$out = $this->getClass('Cities')->getActiveCityCount();
					break;
				case 'update-seller-email-db':
					$Sellers = $this->getClass('Sellers', 1);
					$page = $_POST['page'];
					$pagePer = $_POST['pagePer'];
					$out = $Sellers->updatePageEmailDb($page, $pagePer);
					// $out = new Out( empty($count) ? 'fail' : 'OK', empty($count) ? "Failed to update sellers email db" : "Updated $count sellers email db");
					break;

				case 'reset-image-origin':
					$Listings = $this->getClass('Listings', 1);
					$page = $_POST['page'];
					$pagePer = $_POST['pagePer'];
					$out = $Listings->resetImageOrigins($page, $pagePer);
					break;

				case 'set-first-image':
					$Listings = $this->getClass('Listings', 1);
					$page = $_POST['page'];
					$pagePer = $_POST['pagePer'];
					$out = $Listings->setFirstImage($page, $pagePer);
					break;

				case 'update-median-price':
					$Cities = $this->getClass('Cities', 1);
					$page = $_POST['page'];
					$pagePer = $_POST['pagePer'];
					$out = $Cities->updateMedianPrices($page, $pagePer);
					break;

				case 'update-median-price-from-zip':
					$MedianPrices = $this->getClass('MedianPrices', 1);
					$page = $_POST['page'];
					$pagePer = $_POST['pagePer'];
					$out = $MedianPrices->updateMedianPrices($page, $pagePer);
					break;

				case 'optimize-table':
					try {
						$t = $this->getClass("ListHubData");
						$t->doingParallel = 1;
						ob_start();
						$t->optimizeTable();
						$tasksDone = ob_get_contents();
						ob_end_clean();
						$tasksDone .= '<br/>done';
						$this->log("optimize-table - after ob_end_clean(): $tasksDone");
						$tasksDone = str_replace("\n", "<br/>", $tasksDone);
						$this->log("optimize-table - returning: $tasksDone");
						$out = new Out('OK', $tasksDone);
						//if ($out) $out = new AH\Out('OK', $out);
						//else $out = new AH\Out('fail', "Try again!");
					} catch (\Exception $e) {$out = $e->getMessage();}
					break;
				case 'clean-images':
					$out = new Out(0, 'Run from TomDev instead.');
					break;
				case 'check-tables-add':
					require_once(__DIR__.'/../_classes/Tables.class.php'); $t = new Tables();
					$out = $t->checkAllTables('add');
					$out = new Out($out ? 'OK' : 0, $out);
					break;
				case 'check-all-tables':
					require_once(__DIR__.'/../_classes/Tables.class.php'); $t = new Tables();
					$out = $t->checkAllTables();
					$out = new Out($out ? 'OK' : 0, $out);
					break;
				// case 'check-table-options':
				// 	require_once(__DIR__.'/../_classes/Tables.class.php'); $t = new Tables();
				// 	$out = $t->checkTable('tags-categories-taxonomy');
				// 	$out = new Out($out ? 'OK' : 0, $out);
				// 	break;
				case 'migrate':
					// require_once(__DIR__.'/../_classes/Tables.class.php'); $t = new Tables();
					// $out = $t->migrate('tags-cats-to-ids');
					// $out = $t->migrate('new-tags');
					// if ($out) $out = new Out('OK', $out);
					// else $out = new Out(0, $out);
					$out = new Out('OK', 'commented out for safety');
					break;
				case 'get-table-info':
					require_once(__DIR__.'/../_classes/Tables.class.php'); $t = new Tables();
					$out = $t->get('table-info', 'listings-geoinfo');
					$out = new Out($out ? 'OK' : 0, $out);
					break;
				case 'quandl':
					try {
						// header("Access-Control-Allow-Origin: *");
						$t = $this->getClass("Cities", 1);
						$out = $t->updateQuandlCityCodes(empty($_FILES) ? null : $_FILES);
						// echo 'DONE';
						// die();
					} catch (\Exception $e) {$out = $e->getMessage();}
					break;
				case 'imageBorder':
					try {
						// header("Access-Control-Allow-Origin: *");
						$t = $this->getClass("ListHubData", 1);
						$out = $t->processBorder(empty($_FILES) ? null : $_FILES);
					} catch (\Exception $e) {$out = $e->getMessage();}
					break;
				case 'listhub':
					try {
						header("Access-Control-Allow-Origin: *");
						$t = $this->getClass("ListHubData");
						$out = $t->perform(empty($_FILES) ? null : $_FILES);
						// echo 'DONE';
						die();
					} catch (\Exception $e) {$out = $e->getMessage();}
					break;
				case 'getlisthubdata':
					try {
						$out = new Out('fail', 'deprecated call');
						break;
						
						header("Access-Control-Allow-Origin: *");
						$t = $this->getClass("ListHubData");
						$out = $t->getListHubData();
						echo 'DONE';
						die();
					} catch (\Exception $e) {$out = $e->getMessage();}
					break;
	 			case 'getlisthubdataParallel':
					try {
						$onlyParseFeed = isset($_POST['parseOnly']) ? is_true($_POST['parseOnly']) : 0;
						$retryCount = isset($_POST['retryCount']) ? intval($_POST['retryCount']) : 0;
						$t = $this->getClass("ListHubData");
						$t->getListHubData(true, $onlyParseFeed, false, $retryCount); //just parse it and save to db
						//if ($out) $out = new AH\Out('OK', $out);
						//else $out = new AH\Out('fail', "Try again!");
					} catch (\Exception $e) {$out = $e->getMessage();}
					break;

				case 'fixImageBorderParallel':
				case 'importparallel':	
				case 'genImagesParallel':		
				case 'listhubparallel':	
				case 'customImageParallel':
					try {
						header("Access-Control-Allow-Origin: *");
						$lhd = $this->getClass('ListHubData');
				        // session_write_close();
		//$t->updateToDb("Entered listhubparallel for host:".$_SERVER['HTTP_HOST'], 1);
						$list = $_POST['list'];
						// if ($firstOne == 'last')
						// 	sleep(10); // just to give the previous ones a head start...
						$id = $_POST['id']; //which subDomain
						$cpuLimit = $_POST['cpuLimit'];
						$saveRejected = $_POST['saverejected'];
						$minTags = $_POST['minTags'];
						$minPrice = $_POST['minPrice'];
						$ppRunId = $_POST['ppRunId'];
						//$t->updateToDb("About to call processBlocks for host:".$_SERVER['HTTP_HOST'], 1);
						switch($in->query) {
							case 'customImageParallel': 
								$op = intval($_POST['op']);
								$onlynew = is_true($_POST['onlynew']);
								$specialOp = isset($_POST['specialOp']) ? intval($_POST['specialOp']) : IMAGE_BORDER_PARALLEL_CUSTOM_TASK;
								$locksql = isset($_POST['locksql']) ? is_true($_POST['locksql']) : 0;
								$lhd->processCustomImageProcessing($list, $cpuLimit, $id, $ppRunId, $op, $onlynew, $specialOp, $locksql); 
								break;

							case 'fixImageBorderParallel': 
								$op = intval($_POST['op']);
								$onlynew = is_true($_POST['onlynew']);
								$specialOp = isset($_POST['specialOp']) ? intval($_POST['specialOp']) : IMAGE_BORDER_PARALLEL;
								$lhd->processImageBorders($list, $cpuLimit, $id, $saveRejected, $minTags, $minPrice, $ppRunId,
																 $op, $onlynew, $specialOp, $locksql); 
								break;
							case 'importparallel':	$lhd->processListingImportImageBlocks($list, $cpuLimit, $id, $saveRejected, $minTags, $minPrice, $ppRunId);  break;// response was echo'ed out first, just die here..
							case 'genImagesParallel': $lhd->processGenerateImages($list, $cpuLimit, $id, $saveRejected, $minTags, $minPrice, $ppRunId); break;// response was echo'ed out first, just die here..
							case 'listhubparallel': 
								$specialOperation = SPECIAL_PARSE_BATCH;
								$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'RunPPAsBatch']]);
								if (!empty($opt))
									$specialOperation = intval($opt[0]->value) ? SPECIAL_PARSE_BATCH : SPECIAL_OP_NONE;
								$locksql = isset($_POST['locksql']) ? is_true($_POST['locksql']) : 0;
								$this->log("SubDomain: $id startId:{$list[0]} - $in->query for {$_SERVER[SERVER_NAME]}{$_SERVER[REQUEST_URI]}");
								$lhd->processBlocks($list, $cpuLimit, $id, $saveRejected, $minTags, $minPrice, $ppRunId, $specialOperation, $locksql); break; //SPECIAL_OP_NONE); break;// response was echo'ed out first, just die here..
						}
						//if ($out) $out = new AH\Out('OK', $out);
						//else $out = new AH\Out('fail', "Failed to process chunk starting with index:".$list[0]);
					} catch (\Exception $e) {$out = $e->getMessage();}
					break;

				case 'listhubchunk':
					if (!isset($_POST['list'])) throw new \Exception("No list supplied");
					$list = $_POST['list'];
					if (count($list) == 0) throw new \Exception("Empty list supplied");
					$lhd = $this->getClass('ListHubData');
					$out = $lhd->performChunk($list);
					break;

				case 'listhubprogress':
					try {
						$t = $this->getClass("ListHubData");
						$out = $t->getProgress();
						$out = new Out('OK', $out);
						//if ($out) $out = new AH\Out('OK', $out);
					} catch (\Exception $e) {$out = $e->getMessage();}
					break;
				case 'listhubcurrentprogress':
					try {
						$id = $_POST['id'];
						// $t = $this->getClass("ListHubData");
						// $out = $t->getThisProgress($id);
						// $out = new Out('OK', $out);
						//if ($out) $out = new AH\Out('OK', $out);
						$x = $this->getClass('ListhubProgress')->get((object)['where'=>['id'=>$id]]);
						// if (empty($x))
						// 	$this->log("listhubcurrentprogress failed to get progress at $id");
						$out = new Out('OK', !empty($x) ? $x[0] : false);
					} catch (\Exception $e) {$out = $e->getMessage();}
					break;
				case 'getpprecordrange':
					try {
						$t = $this->getClass("ListHubData");
						$start = $_POST['startid'];
						$end = $_POST['endid'];
						//$this->log("getpprecordrange from $start to $end");
						$out = $t->getPPRecordRange($start, $end);
						$out = new Out('OK', $out);
						//if ($out) $out = new AH\Out('OK', $out);
					} catch (\Exception $e) {$out = $e->getMessage();}		
					break;
				case 'getlastprogresscount':
					try {
						// $t = $this->getClass("ListHubData");
						// $out = $t->getLastProgressCount();
						// $out = new Out('OK', $out);
						//if ($out) $out = new AH\Out('OK', $out);
						try {
							$x = $this->getClass("ListhubProgress", 1)->getLast('id');
							if ($x)
								$out = new Out('OK', $x[0]->id);
							else
								$out = new Out('OK', "0");
						}
						catch(\Exception $e) { $out = new Out('fail', "0"); }
					} catch (\Exception $e) {$out = $e->getMessage();}				
					break;
				case 'getblockcount':
					try {
						// $doingIP = isset($_POST['doingIP']) && is_true($_POST['doingIP']) ? 1 : 0;
						$specialOperation = isset($_POST['specialOp']) ? intval($_POST['specialOp']) : 0;
						$out = $this->getClass('ListHubData')->getBlockCount($specialOperation);
						$out = new Out('OK', $out);
						//if ($out) $out = new AH\Out('OK', $out);
					} catch (\Exception $e) {$out = $e->getMessage();}				
					break;
				case 'getlaststartentry':
					try {
						$t = $this->getClass("ListHubData");
						$specialOperation = isset($_POST['specialOp']) ? intval($_POST['specialOp']) : 0;
						$doingImagePP = !($specialOperation >= IMAGE_BORDER_PARALLEL && $specialOperation <= IMAGE_BORDER_PARALLEL_BATCH) ? 0 :
							 			 ($specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 1 : 2);
						$this->log("getlaststartentry - doingImagePP:$doingImagePP");
						$out = $t->getLastStartEntryFromLHP($doingImagePP);
						$out = new Out('OK', $out);
					} catch (\Exception $e) {$out = $e->getMessage();}	
					break;
				case 'ping':
					try {
						$t = $this->getClass("ListHubData");
						$out = $t->ping();
						$out = new Out('OK', $out);
					} catch (\Exception $e) {$out = $e->getMessage();}	
					break;
				case 'pingremote':
					$t = $this->getClass("ListHubData");
					$out = $t->pingRemote();
					break;
				case 'statsremote':
					$t = $this->getClass("ListHubData");
					$out = $t->statsRemote();
					break;

				case 'sendFlagToRemote':
					$flag = isset($_POST['flag']) ? intval($_POST['flag']) : 0;
					$value = isset($_POST['value']) ? $_POST['value'] : 0;
					$this->log("sendFlagToRemote - flag:$flag, value:$value");
					$t = $this->getClass("ListHubData");
					$out = $t->sendFlagToRemote($flag, $value);
					break;

				case 'writelogheaderforparallel':
					try {
						$subdomains = $_POST['subdomains'];
						$o = $this->getClass('Options');
						$q = new \stdClass();
						$q->where = array('opt'=>'host-environment');
						$x = $o->get($q);
						$amazon = false;
						if (!empty($x) &&
							$x[0]->value == "Amazon") {
							$q->where = array('opt'=>'AWS_EC2_Keys');
							$x2 = $o->get($q);
							if (!empty($x2)) {
								$amazon = true;
								$a = $this->getClass('AWS');
								$out = $a->createEc2Instances(intval($subdomains), false); // bool is for $dryRun
								if ($out->status == 'fail') {
									$out->data = "Failed to start Amazon EC2 instances";
									break;
								}
								else
									$amazonData = $out->data;
							}
						}
						$t = $this->getClass("ListHubData");
						$chunks = $_POST['chunks'];
						$unitsperchunk = $_POST['unitsperchunk'];
						$saverejected = $_POST['saverejected'];
						$cpulimit = $_POST['cpulimit'];
						$minTags = $_POST['minTags'];
						$minPrice = $_POST['minPrice'];
						$totalBlocks = $_POST['totalBlocks'];
						$ppRunId = $_POST['ppRunId'];
						$specialOp = isset($_POST['specialOp']) ? intval($_POST['specialOp']) : 0;
						$restartSQL = isset($_POST['restartSQL']) ? is_true($_POST['restartSQL']) : 0;
						$resetMedianAndTiers = isset($_POST['resetMedianAndTiers']) ? is_true($_POST['resetMedianAndTiers']) : 0;
						$this->log("writelogheaderforparallel calling ListHubData for $specialOp, restartSQL:$restartSQL, resetMedianAndTiers:$resetMedianAndTiers");
						$t->writeLogHeaderForParallelFile($subdomains, $chunks, $unitsperchunk, $saverejected, $cpulimit, $minTags, $minPrice, $totalBlocks, $ppRunId, $specialOp, $restartSQL, $resetMedianAndTiers);
						$this->log("writelogheaderforparallel returned from ListHubData");
						if (!$amazon)
							$out = new Out('OK', "Parallel log initialized.");
						else
							$out = new Out('OK', $amazonData); // this will have the IP address, etc..
						//if ($out) $out = new AH\Out('OK', $out);
					} catch (\Exception $e) {$out = $e->getMessage();}		
					break;
				case 'concludepp':
					$t = $this->getClass("ListHubData");
					$mode = isset($_POST['mode']) ? intval($_POST['mode']) : 0;
					$optimizeTable = isset($_POST['optimizeTable']) ? is_true($_POST['optimizeTable']) : 0;
					$ppRunId = $_POST['ppRunId'];
					$t->concludePP($mode, $ppRunId, $optimizeTable);
					// $out = new Out('OK', "End of Parallel Processing marked.");
				 //    break;
					return;
				case 'pausepp': // this would be issued from a remote monitor to tell the controlling FE to pause
					$t = $this->getClass("ListHubData");
					$ppRunId = $_POST['ppRunId'];
					$specialOperation = isset($_POST['specialOp']) ? intval($_POST['specialOp']) : 0;
					$doingImagePP = !($specialOperation >= IMAGE_BORDER_PARALLEL && $specialOperation <= IMAGE_BORDER_PARALLEL_BATCH) ? 0 :
							 		 ($specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 1 : 2);
					$out = $t->pausePP($ppRunId, $doingImagePP);
				    break;
				case 'resumepp': // this would be issued from a remote monitor to tell the controlling FE to resume
					$t = $this->getClass("ListHubData");
					$ppRunId = $_POST['ppRunId'];
					$specialOperation = isset($_POST['specialOp']) ? intval($_POST['specialOp']) : 0;
					$doingImagePP = !($specialOperation >= IMAGE_BORDER_PARALLEL && $specialOperation <= IMAGE_BORDER_PARALLEL_BATCH) ? 0 :
							 		 ($specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 1 : 2);
					$out = $t->resumePP($ppRunId, $doingImagePP);
				    break;
				case 'haltpp': // this would be issued from a remote monitor to tell the controlling FE to halt
					$t = $this->getClass("ListHubData");
					$ppRunId = $_POST['ppRunId'];
					$specialOperation = isset($_POST['specialOp']) ? intval($_POST['specialOp']) : 0;
					$doingImagePP = !($specialOperation >= IMAGE_BORDER_PARALLEL && $specialOperation <= IMAGE_BORDER_PARALLEL_BATCH) ? 0 :
							 		 ($specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 1 : 2);
					$out = $t->haltPP($ppRunId, $doingImagePP);
					break;
				case 'reloadpage': // this would be issued from a remote monitor to tell the controlling FE to reload page
					$t = $this->getClass("ListHubData");
					$out = $t->reloadPage($_POST['everest']);
				    break;
				case 'lockSql': // this would be issued from a remote monitor to tell the controlling FE to lockSql
					$t = $this->getClass("ListHubData");
					$out = $t->lockSql(true);
				    break;
				case 'unlockSql': // this would be issued from a remote monitor to tell the controlling FE to unlockSql
					$t = $this->getClass("ListHubData");
					$out = $t->lockSql(false);
				    break;
				case 'startremotepp':
					$t = $this->getClass("Options");
					$q = new \stdClass();
					$q->where = array('opt'=>'activePP');
					$x = $t->get($q);
					if (!empty($x)) // uh oh..
						$out = new Out('fail', 'Have PP already active!');
					else
					{
						// make sure remote is registered to run pp
						$t = $this->getClass("Options");
						$q = new \stdClass();
						$q->where = array('opt'=>'remotePPAble');
						$x = $t->get($q);
						$canRemote = !empty($x);
						
						if (!$canRemote) {
							$out = new Out('fail', 'None registered to run remote');
						}
						else {
							$t = $this->getClass("ListHubData");
							$out = $t->startRemotePP($_POST['remoteppable']);
						}
					}
					break;
				case 'startremotep2db':
					$t = $this->getClass("Options");
					$q = new \stdClass();
					$q->where = array('opt'=>'activeParseToDb');
					$x = $t->get($q);
					if (!empty($x)) // uh oh..
						$out = new Out('fail', 'Have P2Db already active!');
					else
					{
						// make sure remote is registered to run pp/p2db
						$t = $this->getClass("Options");
						$q = new \stdClass();
						$q->where = array('opt'=>'remotePPAble');
						$x = $t->get($q);
						$canRemote = !empty($x);
						
						if (!$canRemote) {
							$out = new Out('fail', 'None registered to run remote');
						}
						else {
							$t = $this->getClass("ListHubData");
							$out = $t->startRemoteP2Db($_POST['remoteppable']);
						}
					}
					break;
				case 'startremoteimageprocessing':
					$t = $this->getClass("Options");
					$q = new \stdClass();
					$q->where = array('opt'=>'activePP');
					$x = $t->get($q);
					if (!empty($x)) // uh oh..
						$out = new Out('fail', 'Have PP already active!');
					else
					{
						// make sure remote is registered to run pp
						$t = $this->getClass("Options");
						$q = new \stdClass();
						$q->where = array('opt'=>'remotePPAble');
						$x = $t->get($q);
						$canRemote = !empty($x);
						
						if (!$canRemote) {
							$out = new Out('fail', 'None registered to run remote');
						}
						else {
							$t = $this->getClass("ListHubData");
							$mode = $_POST['mode'];
							$op = $_POST['op'];
							$onlynew = $_POST['onlynew'];
							$fromLast = isset($_POST['fromLast']) && ($_POST['fromLast'] == 1 || $_POST['fromLast'] == '1');
							$numSubDomains = $_POST['numSubDomains'];
							$unitsPerChunk = $_POST['unitsPerChunk'];
							$numChunkPerSubDomain = $_POST['numChunkPerSubDomain'];
							$cpuLimit = $_POST['cpuLimit'];
							$out = $t->startRemoteImageProcessing($_POST['remoteppable'],
																  $mode,
																  $op,
																  $onlyNew,
																  $fromLast,
																  $numSubDomains,
																  $unitsPerChunk,
																  $numChunkPerSubDomain);
						}
					}
					break;

				case 'clearMyMountain':
					try {
						$t = $this->getClass("Options");
						$q = new \stdClass();
						$q->where = array('opt'=>$_POST['everest']);
						$t->delete($q->where);
						$out = new Out('OK', 'done');
					} catch (\Exception $e) {$out = $e->getMessage();}				
					break;
				// if PP or P2Db is active, tell te client
				case 'haveactiveparallelprocessing':
					// $doingIP = isset($_POST['doingIP']) && is_true($_POST['doingIP']) ? 1 : 0;
					// $t = $this->getClass("Options");
					$opt = $this->getClass("Options")->get((object)['like'=>['opt'=>'active']]);
					if ( empty($opt) )
						$out = new Out('OK', "false");   // report false, no active PP
					else
						$out = new Out('OK', $opt);
					// $q = new \stdClass();
					// $q->where = array('opt'=> $doingIP ? 'activeIP' : 'activePP'); // actual parallel processing
					// $x = $t->get($q);
					// if (!empty($x)) // ok, something's happening
					// 	$out = new Out('OK', array('action'=> $doingIP ? 'IP' : "PP",
					// 								'owner'=>$x[0]->value)); // report true, have active PP
					// else {
					// 	$q->where = array('opt'=>'activeParseToDb'); // reading in listhub data to parse to db
					// 	$x = $t->get($q);
					// 	if (!empty($x)) // ok, something's happening
					// 		$out = new Out('OK', array('action'=>"P2Db",
					// 									'owner'=>$x[0]->value)); // report true, have active PP
					// 	else
					// 		$out = new Out('OK', "false");   // report false, no active PP
					// }
					break;
				// set/remove Parallel Processing (PP)
				case 'startactiveparallelprocessing':
					if (!isset($_POST['specialOp'])) throw new \Exception("No specialOp was sent");

					$specialOperation = isset($_POST['specialOp']) ? intval($_POST['specialOp']) : 0;
					$doIP = ($specialOperation >= IMAGE_BORDER_PARALLEL && $specialOperation <= IMAGE_BORDER_PARALLEL_BATCH);
					$doingImagePP = !$doIP ? 0 :
							 		 ($specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 1 : 2);
					$t = $this->getClass("Options");
					$q = new \stdClass();
					$q->where = array('opt'=> !$doingImagePP ? 'activePP' : ($doingImagePP == 1 ? 'activeIP' : 'activeIPCustom'),
									  'value' => $_POST['mountain']);
					$this->log("startactiveparallelprocessing is called with specialOperation:$specialOperation, doingImagePP:$doingImagePP, doIP:$doIP, for:".(!$doingImagePP ? 'activePP' : ($doingImagePP == 1 ? 'activeIP' : 'activeIPCustom')));
					$x = $t->get($q);
					if (!empty($x)) // uh oh..
						$out = new Out('fail', 'Have '.(!$doingImagePP ? 'PP' : ($doingImagePP == 1 ? 'IP' : 'IPCustom')).' already active!');
					else
					{
						// clear out listhub chunk status data
						$this->getClass(!$doingImagePP ? "ListhubChunkStatus" : ($doingImagePP == 1 ?  'ImageChunkStatus' : 'CustomImageChunkStatus'))->emptyTable(true);
						// add new one
						$q = array('opt'=> !$doingImagePP ? 'activePP' : ($doingImagePP == 1 ? 'activeIP' : 'activeIPCustom'),
									'value'=>$_POST['mountain']); // unique signature
						$x = $t->add((object)$q);
						$out = new Out('OK', 'Flag is in '.$x.' bucket.');
					}
					break;
				case 'removeactiveparallelprocessing':
					try {
						$specialOperation = isset($_POST['specialOp']) ? intval($_POST['specialOp']) : 0;
						$doingImagePP = !($specialOperation >= IMAGE_BORDER_PARALLEL && $specialOperation <= IMAGE_BORDER_PARALLEL_BATCH) ? 0 :
							 		 	 ($specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 1 : 2);
						$t = $this->getClass("Options");
						$q = new \stdClass();
						$q->where = array('opt'=> !$doingImagePP ? 'activePP' : ($doingImagePP == 1 ? 'activeIP' : 'activeIPCustom'));
						$t->delete($q->where);
						$out = new Out('OK', 'done');
					} catch (\Exception $e) {$out = $e->getMessage();}	
					break;
				case 'forceremoveactiveparallelprocessing':
					try {
						$specialOperation = isset($_POST['specialOp']) ? intval($_POST['specialOp']) : 0;
						$doingImagePP = !($specialOperation >= IMAGE_BORDER_PARALLEL && $specialOperation <= IMAGE_BORDER_PARALLEL_BATCH) ? 0 :
							 		 	 ($specialOperation != IMAGE_BORDER_PARALLEL_CUSTOM_TASK ? 1 : 2);
						$t = $this->getClass("Options");
						$q = new \stdClass();
						$q->where = array('opt'=> !$doingImagePP ? 'activePP' : ($doingImagePP == 1 ? 'activeIP' : 'activeIPCustom'));
						$x = $t->get($q);
						if (!empty($x)) {// remove it
							$t->delete($q->where);
						}
						$out = new Out('OK', 'done');
					} catch (\Exception $e) {$out = $e->getMessage();}	
					break;
				// set/remove ParseToDb
				case 'startactiveparse2db':
					$t = $this->getClass("Options");
					$q = new \stdClass();
					$q->where = array('opt'=>'activeParseToDb');
					$x = $t->get($q);
					if (!empty($x)) // uh oh..
						$out = new Out(0, 'Have P2Db already active!');
					else
					{
						// clear out listhub chunk status data
						//$this->getClass("ListhubChunkStatus")->emptyTable(true);
						// add new one
						$q = array('opt'=>'activeParseToDb',
									'value'=>$_POST['mountain']); // unique signature
						$x = $t->add((object)$q);
						$out = new Out('OK', 'Flag is in '.$x.' bucket.');
						$this->log("startactiveparse2db - is returning OK with flag in $x bucket, cleared ListhubProgress table");
						$opt = $this->getClass('Options')->get((object)['like'=>['opt'=>'activeIP']]);
						$doingImagePP = !empty($opt);
						if (!$doingImagePP)
							$this->getClass("ListhubProgress")->emptyTable(true);
					}
					break;
				case 'removeactiveparse2db':
					try {
						$t = $this->getClass("Options");
						$q = new \stdClass();
						$q->where = array('opt'=>'activeParseToDb');
						$t->delete($q->where);
						$out = new Out('OK', 'done');
					} catch (\Exception $e) {$out = $e->getMessage();}	
					break;
				case 'forceremoveparse2db':
					try {
						$t = $this->getClass("Options");
						$q = new \stdClass();
						$q->where = array('opt'=>'activeParseToDb');
						$x = $t->get($q);
						if (!empty($x)) {// remove it
							$t->delete($q->where);
						}
						$out = new Out('OK', 'done');
					} catch (\Exception $e) {$out = $e->getMessage();}	
					break;
				case 'registerremoteppable':
					try {
						$t = $this->getClass("Options");
						$q = new \stdClass();
						$q->where = array('opt'=>'remotePPAble');
						$x = $t->get($q);
						$needRegister = true;
						if (!empty($x)) {
							foreach($x as $reg) {
								if ($reg->value == $_POST['remoteppable']) {
									$out = new Out('OK','Already registered:'.$reg->value);
									$needRegister = false;
									break;
								}
							}
						}
						if ($needRegister)
						{
							$q = array('opt'=>'remotePPAble',
										'value'=>$_POST['remoteppable']); // unique signature
							$x = $t->add((object)$q);
							$out = new Out('OK', 'Registered in '.$x.' bucket.');
						}
					} catch (\Exception $e) {$out = $e->getMessage();}	
					break;
				case 'amremoteppable':
					try {
						$t = $this->getClass("Options");
						$q = new \stdClass();
						$q->where = array('opt'=>'remotePPAble');
						$x = $t->get($q);
						$canRemote = false;
						if (!empty($x)) {
							foreach($x as $reg) {
								if ($reg->value == $_POST['remoteppable']) {
									$out = new Out('OK','Able to run remote:'.$reg->value);
									$canRemote = true;
									break;
								}
							}
						}
						if (!$canRemote)
						{
							$out = new Out('fail', 'Not registered to run remote');
						}
					} catch (\Exception $e) {$out = $e->getMessage();}	
					break;
				case 'removeremoteppable':
					try {
						$t = $this->getClass("Options");
						$q = new \stdClass();
						$q->where = array('opt'=>'remotePPAble',
										  'value'=>$_POST['remoteppable']);
						$x = $t->get($q);
						if (!empty($x)) {// remove it
							$t->delete($q->where);
						}
						$out = new Out('OK', 'done');
					} catch (\Exception $e) {$out = $e->getMessage();}	
					break;

				case 'remove-listings':
					$t = $this->getClass("ListHubData");
					$start = $_POST['start'];
					$end = $_POST['end'];
					$preserveActive = intval($_POST['preserveActive']);
					$out = $t->removeListings($start, $end, $preserveActive);
					break;

				case 'fiximageborders':
					$t = $this->getClass("ListHubData");
					$mode = $_POST['mode'];
					$op = $_POST['op'];
					$onlynew = $_POST['onlynew'];
					$fromLast = isset($_POST['fromLast']) && ($_POST['fromLast'] == 1 || $_POST['fromLast'] == '1');
					$out = $t->fixImageBorders($mode, $op, $onlynew, $fromLast);
					break;

				case 'fixoneimageborder':
					$t = $this->getClass("ListHubData");
					$id = $_POST['id'];
					$retval = $t->processOneImage($id);
					$out = new Out( $retval ? 'OK': 'fail', $retval ? "Success for $id" : "Failed for $id");
					break;

				case 'checkcronjob':
					$t = $this->getClass("Options");
					$q = new \stdClass();
					$q->where = array('opt'=>'scheduledPPJobs');
					$x = $t->get($q);
					if (!empty($x))
						$out = new Out('OK', $x[0]);
					else
						$out = new Out('No cron jobs');
					break;

				case 'invitationcodes':
					global $colors, $animals;
					$count = $_POST['mode'];
					$t = $this->getClass("Invitations");
					$q = new \stdClass();
					// $q->where = array('opt'=>INVITATION_LIST);
					if ($count == "0") {// just get the list
						$x = $t->get();
						if (empty($x))
							$x = "Empty List";
						// else
						// 	$x = json_decode($x[0]->value);
						$out = new Out('OK', $x);
					} else { // create and save
						$count = intval($count);
						$codes = array();
						$x = $t->get();
						if (!empty($x)) { // update with new
							// $x = json_decode($x[0]->value); // decode into array
							foreach($x as $code) {
								// if ($code->seller_id != 0 ||
								// 	$code->flags & IC_USED) // keep used ones
									$codes[] = $code;
								// else
								// 	$t->delete((object)['where'=>['code'=>$code->code]]);
							}
							// $t->delete($q->where); // get rid of the old
						}

						for($i = 0; $i < $count;) {
							// $idx1 = rand(1, count($colors)) - 1;
							// $idx2 = rand(1, count($animals)) - 1;
							// $code = $colors[$idx1].$animals[$idx2].(string)rand(1,100);
							$code = $this->makeIC();
							if (!$this->codeInArray($code, $codes, 'code')) {
								// $codes[] = array('key'=>$code,
								// 				  'seller_id'=>0);
								$t->add(['code'=>$code]);
								$x = $t->get((object)['where'=>['code'=>$code]]);
								$codes[] = $x[0];
								$i++;
							}
						}
						

						// $codestr = JSON_encode($codes);

						// $data = array('opt'=>INVITATION_LIST,
						// 			  'value'=>$codestr);
						// $x = $t->add((object)$data);
						if ($x)
							$out = new Out('OK', $codes);
						else
							$out = new Out('OK', "Failed to add new codes to db.");
					}
					sleep(1);
					break;

				case 'get-cities-keys':
					$c = $this->getClass('Cities');
					$c = getTableName($c->table);
					$ct = $this->getClass('CitiesTags');
					$ct = getTableName($ct->table);
					$sql = 'SELECT a.city, b.tag_id FROM `'.$c.'` AS a INNER JOIN '.$ct.' AS b ON a.id = b.city_id ORDER BY a.city ASC';
					$cities = $this->getClass('Cities')->rawQuery($sql);
					$out = new Out('OK', $out);
					break;

				case 'start-aws':
					$t = $this->getClass('AWS');
					$count = intval($_POST['count']);
					$out = $t->createEc2Instances($count, false); // bool is for $dryRun
					//$out = new Out('OK',"made one AWS!");
					break;
				case 'stop-aws':
					$t = $this->getClass('AWS');
					$out = $t->stopEc2Instances(false); // bool is for $dryRun
					//$out = new Out('OK',"made one AWS!");
					break;
				case 'terminate-aws':
					$t = $this->getClass('AWS');
					$out = $t->terminateEc2Instances(false); // bool is for $dryRun
					//$out = new Out('OK',"made one AWS!");
					break;

				case 'get-aws-data':
					$o = $this->getClass('Options');
					$q = new \stdClass();
					$q->where = array('opt'=>'AWS_EC2_Keys');
					$x = $o->get($q);
					if (!empty($x)) {
						$out = new Out('OK', $x[0]);
					}
					else
						$out = new Out('fail','Could not find any AWS_EC2_Keys');
					break;

				case 'get-host-environment':
					$o = $this->getClass('Options');
					$q = new \stdClass();
					$q->where = array('opt'=>'host-environment');
					$x = $o->get($q);
					if (!empty($x)) {
						$out = new Out('OK', $x[0]->value);
					}
					else
						$out = new Out('OK','Linux'); //default
					break;

				case 'count-cities':
					$out = new Out('OK', $this->getClass('Cities', 1)->countCities());
					break;

				case 'clean-inactive-listings':
					$t = $this->getClass("ListHubData");
					$out = $t->cleanInactiveListings();
					break;

				case 'clean-listings-img-folder':
					$t = $this->getClass("ListHubData");
					// $t->cleanListingsImgFolder();
					$t->prepareImageFolderCleaning();
					return;

				case 'clean-image-folder': // works with 'clean-listings-img-folder' which populates ImageTracker databsae table
					if (!isset($_POST['page'])) throw \Exception('No page id sent');
					$t = $this->getClass("ListHubData");
					$out = $t->cleanImageFolder($_POST['page']);
					break;
					
				case 'remove-images-with-old-naming-scheme':
					$images = $this->getClass('Image');
					$images->unlinkOldImages();
					return;

				case 'remove-images-from-folder':
					$t = $this->getClass("ListHubData");
					$page = isset($_POST['page']) ? $_POST['page'] : 0;
					$this->log("remove-images-from-folder for page:$page");
					$out = $t->deleteImagesNotFlaggedAsKeeper($page);
					break;

				case 'check-image-tracker':
					$t = $this->getClass("ListhubImageTracker");
					$x = $t->count(['where'=>['flags'=>0]]);
					$out = new Out('OK', $x);
					break;

				case 'check-repair-listing-image-data':
					$t = $this->getClass("ListHubData");
					$t->repairListingImageData();
					return;
					
				default: throw new \Exception(__FILE__.':('.__LINE__.') - invalid query: '.$in->query); break;
			}
			if ($out) echo json_encode($out);
		} catch (\Exception $e) { parseException($e); die(); }
	}

	protected function codeInArray($code, $codes, $key) {
		foreach($codes as $data) {
			if (gettype($code) == "string") {
				if ((is_array($data) &&
					 $data[$key] == $code) ||
					(gettype($data) == "object" &&
					 $data->$key == $code))
					return true;
			}
			elseif ((is_array($data) && // then $code must be from an pre-existing code with a valid seller_id
					 $data[$key] == $code->$key) ||
					(gettype($data) == "object" &&
					 $data->$key == $code->$key))
				return true;
		}
		return false;
	}

	protected function makeIC() {
		$code = '';
		for($i = 0; $i < 8; $i++) {
			$what = rand(1, 1000) % 2; // 0 = number, 1 = Caps, 2 = letter
			switch($what) {
				case 1: $code .= chr(rand(0, 25) + ord('a')); break;
				case 2: $code .= chr(rand(0, 25) + ord('A')); break;
				case 0: $code .= number_format(rand(0, 9));
			}
		}
		return $code;
	}

	private function process($cmd, $wait = true) {
		$Process = $this->getClass('Process',$cmd);
		if ($wait)
			while( $Process->status() )
				sleep(1);

		$this->record("process - $cmd, exiting", 3);
	}
}
new DeveloperDispatcher();
