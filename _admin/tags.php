<?php
	require_once(__DIR__.'/../_classes/TagsCategories.class.php');
	$TagCats = new AH\TagsCategories();
	$tag_cats = [];
	foreach ($TagCats->get((object)[ 'what' => ['id', 'category', 'parent'] ]) as &$row){
		$tag_cats[$row->id] = $row;
		unset($row);
	}
?>
<style>
ul, li{padding:0; margin: 0; }
#tags select{ 
	font-size:0.8em;
	height: 20px;
}
#tb-submit textarea{
	display: block;
	position: relative;
	margin: 1em auto;
	width: 80%;
}
.column-delete{ width: 25px; }
.column-category{ width: 125px; }
.column-tag{ width: 200px; }
.column-in-quiz { width: 50px; }
.column-in-listings{ width: 70px; }
.column-in-cities{ width: 60px; }
td.column-description{ font-size:0.8em; }
</style>
<p>
	<a class="add-new-tag">Add New Tag</a>
</p>
<script type="text/javascript">
	var tags_categories_from_db = <?php echo json_encode( $tag_cats ) ?>;
</script>
<table id="tags" class="widefat">
	<thead>
		<tr>
			<th scope="col" class="manage-column column-delete">Del</th>
			<th scope="col" class="manage-column column-tag">Tag</th>
			<th scope="col" class="manage-column column-description">Description</th>
			<th scope="col" class="manage-column column-type">Type</th>
			<th scope="col" class="manage-column column-category">Category</th>
			<th scope="col" class="manage-column column-in-quiz">In Quiz</th>
			<th scope="col" class="manage-column column-in-listings">In Listings</th>
			<th scope="col" class="manage-column column-in-cities">In Cities</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th scope="col" class="manage-column column-delete">Del</th>
			<th scope="col" class="manage-column column-tag">Tag</th>
			<th scope="col" class="manage-column column-description">Description</th>
			<th scope="col" class="manage-column column-type">Type</th>
			<th scope="col" class="manage-column column-category">Category</th>
			<th scope="col" class="manage-column column-in-quiz">In Quiz</th>
			<th scope="col" class="manage-column column-in-listings">In Listings</th>
			<th scope="col" class="manage-column column-in-cities">In Cities</th>
		</tr>
	</tfoot>
	<tbody>
	</tbody>
</table>