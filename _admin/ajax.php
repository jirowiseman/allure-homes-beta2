<?php
namespace AH;
require_once(__DIR__.'/../_classes/_Controller.class.php');
class AJAX_wpAdmin extends Controller {
	public function __construct(){
		$in = call_user_func_array(array(&$this, 'parent::__construct'), func_get_args());
		try {
			// if (empty($in->query)) throw new \Exception (__FILE__.':('.__LINE__.') - no query sent');
			if (empty($in->query)) throw new \Exception ('no query sent');
			switch ($in->query){
				/* Listings */
					case 'delete-listing':
						if (empty($in->data) || (empty($in->data) && $in->data !== 0)) throw new \Exception('No listing id sent.');
						if ( !$this->getClass('Listings')->delete(['id'=>$in->data['id']]) ) throw new \Exception('Unable to delete listing.');
						$out = new Out('OK');
						break;
					case 'get-listings':
						if ( !$out = $this->getClass('Listings')->get() ) throw new \Exception('No listings found.');
						$out = new Out('OK', $out);
						break;
					case 'get-listings-with-geoinfo':
						if ( !$listings = $this->getClass('Listings')->get( (object)['what'=>[ 'id' ]] ) ) throw new \Exception('No listings found.');
						if ( !$geoinfo = $this->getClass('ListingsGeoinfo')->get() ) throw new \Exception('No geoinfo found for listings.');
						$x = [];
						foreach ($listings as &$listing) foreach ($geoinfo as $geo) if ($geo->listing_id == $listing->id && isset($geo->lat)) {
							$x[] = $listing->id;
							break;
						}
						$out = new Out('OK', $x);
						break;
					case 'get-listings-and-points':
						if ( !$listings = $this->getClass('Listings')->get( (object)['what'=>[ 'id','title','street_address' ]] ) ) throw new \Exception('No listings found.');
						if ( !$geoinfo = $this->getClass('ListingsGeoinfo')->get() ) throw new \Exception('No geoinfo found for listings.');
						if ( !$points = $this->getClass('ListingsPoints')->get() ) throw new \Exception('No points found for listings.');
						$x = [];
						foreach ($listings as $listing){
							$id = $listing->id;
							// setup geoinfo
							foreach ($geoinfo as $geo) if ($geo->listing_id == $id) {
								foreach ($geo as $field=>$value)
									if ($field == 'listing_id' || $field == 'id') continue;
									else $listing->$field = $value;
								break;
							}
							$listing->points = [];
							// setup points
							foreach ($points as $point) if ($point->listing_id == $id)
								$listing->points[$point->point_id] = $point->distance;
							$x[$id] = $listing;
							unset($id);
						}
						$out = new Out('OK', $x);
						break;
					case 'update-listing':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No listing id sent.');
						if (empty($in->data['fields'])) throw new \Exception('No fields sent.');
						if ( !$this->getClass()->set( (object) ['where'=>['id'=>$in->data['id']], 'fields'=>$in->data['fields']] ) )
							throw new \Exception('Unable to update listing.');
						$out = new Out('OK');
						break;
				/* Points */
					case 'add-points':
						if (empty($in->data)) throw new \Exception('No points sent.');
						$results = [];
						foreach ($in->data as $point)
							if (!$this->getClass('Points')->exists((object)['name'=>$point['name'], 'address'=>$point['address']])){
								$in_obj = (object)array(
									'name'=>$point['name'],
									'meta'=>isset($point['meta']) ? $point['meta'] : (object)[],
									'lat'=>$point['lat'],
									'lng'=>$point['lng'],
									'address'=>$point['address']
								);
								$point_id = $this->getClass('Points')->add($in_obj);
								if ($point_id === false) throw new \Exception('Unable to add point to db.');
								$results[] = [ 'point'=>$point_id, 'category'=>$this->getClass('PointsTaxonomy')->add((object)[ 'point_id'=>$point_id, 'category_id'=>$point['category'] ]) ];
								unset($in_obj);
							} else $results[] = 'already exists';
						$out = new Out('OK', $results);
						break;
				/* Cities */
					case 'add-city-tags':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (!is_array($in->data)) throw new \Exception('Data must be an array, '. gettype($in->data) .' sent.');
						$out = $this->cityTags('add', $in->data);
						$out = $out === true ? new Out('OK') : new Out(0, $out);
						break;
					case 'delete-city-tags':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (!is_array($in->data)) throw new \Exception('Data must be an array, '. gettype($in->data) .' sent.');
						$out = $this->cityTags('delete', $in->data);
						$out = $out === true ? new Out('OK') : new Out(0, $out);
						break;
					case 'edit-city-tags':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (!is_array($in->data) && !is_object($in->data)) throw new \Exception('Data must be an array or object, '. gettype($in->data) .' sent.');
							$results = (object)[ 'ignored' => (object)[] ];
							$status = 'OK';
							foreach ($in->data as $type=>&$rows) if (!empty($rows)) {
								switch ($type){
									case 'add': case 'delete':
										$results->$type = $this->cityTags($type, $rows);
										$prop = $type.'Tags';
										$results->$prop = $rows;
										if ($results->$type === true){
											$results->$type = new Out('OK', $type);
										} else {
											$status = 0;
											$results->$type = new Out(0, $results->$type);
										}
										$added = null;
										break;
									default: $results->ignored->$type = $rows; if ($status) $status = 0; break;
								}
								unset($rows, $type);
							}
							$out = new Out($status, $results);
						break;
					case 'update-city':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (!isset($in->data['id'])) throw new \Exception('No city id sent.');
						if (empty($in->data['fields'])) throw new \Exception('No fields sent.');
						if ( !$this->getClass('Cities')->set(array((object)[ 'where'=>['id'=>$in->data['id']], 'fields'=>$in->data['fields']]) ))
							throw new \Exception('Unable to update city.');
						$out = new Out('OK');
						break;
					case 'update-city-tag':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (!isset($in->data['city_id'])) throw new \Exception('No city id sent.');
						if (!isset($in->data['tag_id'])) throw new \Exception('No tag id sent.');
						if (!isset($in->data['score'])) throw new \Exception('No score sent.');
						$where = (object)['city_id'=>$in->data['city_id'], 'tag_id'=>$in->data['tag_id']];
						$tag = $this->getClass('Tags')->get((object)['where'=>['id'=>$in->data['tag_id']]]);
						if ( !$this->getClass('CitiesTags')->exists($where) ){
							$where->score = $in->data['score'];
							$out = ($x = $this->getClass('CitiesTags')->add($where) ? new Out('OK', !empty($tag) ? $tag[0] : null) : new Out(0, $x));
						} else
							$out = (
								( $x = $this->getClass('CitiesTags')->set([ (object)[ 'where'=>$where, 'fields'=>['score'=>$in->data['score']] ] ]) ) ?
									new Out('OK', !empty($tag) ? $tag[0] : null) : new Out(0, $x)
							);
						break;
					case 'get-cities':
						$query = (object)['orderby'=>'count', 'order'=>'desc', 'limit' => 10];
						if (!empty($in->data)) {
							$query->page = !empty($in->data['page']) ? intval($in->data['page']) : 0;
							if ( !empty($in->data['fields']) ) {
								$query->where = (object)[];
								foreach ($in->data['fields'] as $field => &$value){
									if ($field == 'city') $query->like = (object)[ 'city' => $value ];
									else $query->where->$field = $value;
								}
							}
						}

						if ( !$out = $this->getClass('Cities')->get($query) ) throw new \Exception('No cities found - query:'.print_r($in->data, true));
						require_once(__DIR__.'/../_classes/CitiesTags.class.php'); $CitiesTags = new CitiesTags();
						require_once(__DIR__.'/../_classes/Tags.class.php'); $Tags = new Tags();
						$tag_ids = [];
						$city_tags = $CitiesTags->get((object)[
							'what' => ['city_id', 'tag_id', 'score'],
							'where' => [ 'city_id' => wp_list_pluck( $out, 'id' ) ]
						]);
						if (!empty($city_tags)){
							$t = [];
							$tag_ids = [];
							foreach ($city_tags as &$row){
								if (!in_array( intval($row->tag_id), $tag_ids )) $tag_ids[] = intval($row->tag_id);
								if (!isset( $t[$row->city_id] )) $t[$row->city_id] = [];
								$t[$row->city_id][$row->tag_id] = $row->score;
								unset($row);
							}
							$city_tags = $t;
							unset($t);
							$tag_names = wp_list_pluck( $Tags->get((object)[ 'what'=>['id', 'tag'], 'where' => [ 'id' => $tag_ids ] ]), 'tag', 'id');
							foreach ($out as &$row) if (in_array( $row->id, array_keys($city_tags) )) {
								$row->tags = $city_tags[$row->id];
								foreach ($row->tags as $tag_id => &$score){
									$score = [ 'tag' => $tag_names[$tag_id], 'score' => $score ];
								}
							}
						}
						$out = new Out('OK', $out);
						break;
				/* Tags */
					case 'add-tag':
						if ( empty( $in->data ) ) throw new \Exception('No data sent.');
						if ( !$this->getClass('Tags')->exists((object)[ 'tag' => $in->data ]) )
							$out = new Out( $this->getClass('Tags')->add((object)[ 'tag' => $in->data ]) ? 'OK' : 0 );
						break;
					case 'delete-tag':
						if ( empty( $in->data ) ) throw new \Exception('No data sent.');
						if ( empty( $in->data['tag_id'] ) ) throw new \Exception('No tag_id sent.');
						$results = [];
						$results['ListingsTags'] = $this->getClass('ListingsTags')->delete([ 'tag_id' => $in->data['tag_id'] ]);
						$results['QuizTags'] = $this->getClass('QuizTags')->delete([ 'tag_id' => $in->data['tag_id'] ]);
						$results['CitiesTags'] = $this->getClass('CitiesTags')->delete([ 'tag_id' => $in->data['tag_id'] ]);
						$results['Tags'] = $this->getClass('Tags')->delete([ 'id' => $in->data['tag_id'] ]);
						$out = new Out('OK', $results);
						break;
					case 'get-tags':
						$Tags = $this->getClass('Tags')->get();
						$TagsCategories = $this->getClass('TagsCategories')->get((object)['sortby'=>'category']);
						$TagsTaxonomy = $this->getClass('TagsTaxonomy')->get((object)['what'=> ['tag_id','category_id'] ]);

						$order_by = 'tag';
						$order = wp_list_pluck($Tags, $order_by);
						natcasesort( $order );
						$new_tags = [];
						foreach (array_keys($order) as &$i) if (array_key_exists($i, $Tags)) $new_tags[] = $Tags[$i];
						$Tags = $new_tags;

						// $activeListings = $this->getClass('Listings')->get((object)[ 'what' => ['id'], 'where' => [ 'active' => 1 ] ]);
						// $activeListings = wp_list_pluck($activeListings, 'id');

						foreach ($Tags as &$t){
							$t->count_quiz = $this->getClass('QuizTags')->count((object)[ 'where' => [ 'tag_id' => $t->id ] ]);
							$t->count_listings = $this->getClass('ListingsTags')->count((object)[ 'where' => [ 'tag_id' => $t->id ] ]);
							$t->count_cities = $this->getClass('CitiesTags')->count((object)[ 'where' => [ 'tag_id' => $t->id ] ]);
							// foreach ($list_count as &$row) if (in_array($row->listing_id, $activeListings)) $t->count_listings++;
							foreach ($TagsTaxonomy as $tax) if ($tax->tag_id == $t->id) {
								foreach ($TagsCategories as $cat) if ($cat->id == $tax->category_id){
									if (isset($in->data['group-by-category'])){
										if (!isset($x[$cat->category])) $x[$cat->category] = [];
										$x[$cat->category][] = $t;
									}
									else $t->category = $cat->category;
									break;
								}
								break; // only one category per tag
							}
						}
						// if (isset($in->data['group-by-category'])) $Tags = $x;
						// else usort($Tags,function($a,$b){ strnatcmp($a->tag, $b->tag); });
						$out = new Out('OK', $Tags);
						break;
					case 'update-tag':
						if ( empty( $in->data ) ) throw new \Exception('No data sent.');
						if ( empty( $in->data['tag_id'] ) ) throw new \Exception('No tag_id sent.');
						if ( empty( $in->data['fields'] ) ) throw new \Exception('No fields sent.');
						$check = $in->data['fields'];
						$check['id'] = $in->data['tag_id'];
						if (!$this->getClass('Tags')->exists( $check )){
							$res = $this->getClass('Tags')->set([ (object)[ 'where' => [ 'id' => $in->data['tag_id'] ], 'fields' => $in->data['fields'] ] ]);
							$out = new Out( ($res ? 'OK' : 0) );
						} else $out = new Out('OK');
						break;
					case 'update-tag-category':
						if ( empty( $in->data ) ) throw new \Exception('No data sent.');
						if ( empty( $in->data['tag_id'] ) ) throw new \Exception('No tag_id sent.');
						if ( empty( $in->data['category_id'] ) ) throw new \Exception('No category_id sent.');
						$prevCat = $this->getClass('TagsTaxonomy')->get((object)[ 'what' => ['category_id'], 'where' => [ 'tag_id' => $in->data['tag_id']  ] ]);
						if (!$prevCat) {
							$this->getClass('TagsTaxonomy')->add((object)[ 'tag_id' => $in->data['tag_id'], 'category_id' => $in->data['category_id'] ]);
							$out = new Out('OK');
						} else {
							$prevCat = array_pop($prevCat);
							if ($prevCat->category_id !== intval($in->data['category_id'])) {
								$res = $this->getClass('TagsTaxonomy')->set([ (object)[ 'where' => [ 'tag_id' => $in->data['tag_id'] ], 'fields' => [ 'category_id' => $in->data['category_id'] ] ] ]);
								$out = new Out( ($res ? 'OK' : 0) );
							} else $out = new Out('OK');
						}
						break;
				/* Quiz */
					case 'add-question':
						if ( $this->getClass('QuizQuestions')->add() !== false ) $out = new Out('OK');
						break;
					case 'delete-question':
						$out = ( $x = $this->getClass('QuizQuestions')->delete(array('id'=>$in->data['question_id'])) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags() ) :
							new Out(0, array('unable to delete question', $x));
						break;
					case 'get-quiz-questions':
						$out = new Out('OK', $this->getQuestionsSlidesAndTags());
						break;
					case 'update-questions':
						$q = $ids = array();
						foreach ($in->data as $d) {
							$ids[] = $d['question_id'];
							$q[] = (object) array('where'=>array('id'=>$d['question_id']), 'fields'=>$d['fields']);
						}
						$out = ( $x = $this->getClass('QuizQuestions')->set($q) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($ids) ) :
							new Out(0, $x);
						break;
					case 'add-slide':
						$out = ($x = $this->getClass('QuizSlides')->add(array( 'question_id'=>$in->data['question_id'], 'orderby'=> isset($in->data['orderby']) ? $in->data['orderby'] : null )) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id'])) :
							new Out(0, array('Unable to add slide.', $x));
						break;
					case 'delete-slide':
						$out = ( $x = $this->getClass('QuizSlides')->delete( array('id'=>$in->data['slide_id']) ) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id']) ) :
							new Out(0, array('Unable to delete slide', $x));
						break;
					case 'update-slides':
						$q = array();
						foreach ($in->data as $d)
							$q[] = (object)array( 'where'=>array('id'=>$d['slide_id']), 'fields'=>$d['fields'] );
						$out = ( $x = $this->getClass('QuizSlides')->set($q) ) ?
							new Out('OK') :
							new Out(0, array('Unable to update slides.', $x));
						break;
					case 'add-tag-to-slide':
						$out = ( $x = $this->getClass('QuizTags')->add(array('tag_id'=>$in->data['tag_id'],'slide_id'=>$in->data['slide_id'])) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id']) ) :
							new Out(0, array('Unable to add row', $x));
						break;
					case 'remove-tag-from-slide':
						$out = ( $x = $this->getClass('QuizTags')->delete(array('tag_id'=>$in->data['tag_id'],'slide_id'=>$in->data['slide_id'])) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id']) ) :
							new Out(0, array('Unable to delete tag from slide', $x));
						break;
 				default: throw new \Exception(__FILE__.':('.__LINE__.') - invalid query: '.$in->query); break;
			}
			if ($in->call_type == 'runtime') return $out;
			elseif (isset($out)) echo json_encode($out);
		} catch (\Exception $e) { parseException($e, true); die(); }
	}
	private function getQuestionsSlidesAndTags($question_id = null){
		$query = (object)array();
		if (!empty($question_id)) $query->where = array('id'=>$question_id);
		$Questions = $this->getClass('QuizQuestions')->get($query);
		$Slides = $this->getClass('QuizSlides')->get();
		$SlideTags = $this->getClass('QuizTags')->get();
		$Tags = $this->getClass('Tags')->get();
		foreach ($Questions as &$q){
			$q->slides = array();
			foreach ($Slides as &$slide) if ($slide->question_id == $q->id){
				$slide->tags = array();
				foreach ($SlideTags as $slide_tag) if ($slide_tag->slide_id == $slide->id) {
					foreach ($Tags as $tag) if ($tag->id == $slide_tag->tag_id){
						$slide->tags[] = $tag;
						break;
					}
				}
				$q->slides[] = $slide;
			}
		}
		return $Questions;
	}
	private function cityTags($query = null, &$in = null){
		if ($query === null) throw new \Exception('No query sent.');
		else switch($query){
			case 'delete':
				$results = [];
				$status = 'OK';
				foreach ($in as &$row) if (isset($row['city_id']) && isset($row['tag_id'])) {
					$tag = $this->getClass('Tags')->get((object)['where'=>['id'=>$row['tag_id']]]);
					$row['tag'] = !empty($tag) ? $tag[0]->tag : "Unknown".$row['tag_id'];
					$success = $this->getClass('CitiesTags')->delete((object)['city_id'=>$row['city_id'], 'tag_id'=>$row['tag_id']]);
					if (!$success) $status = 0; // fail
					$results[] = [$row, $success];
					unset($row, $success);
				}
				if ($status == 'OK') return true;
				else return $results;
				break;
			case 'add':
				$results = [];
				$status = 'OK';
				foreach ($in as &$row) if (isset($row['city_id']) && isset($row['tag_id'])) {
					$tag = $this->getClass('Tags')->get((object)['where'=>['id'=>$row['tag_id']]]);
					$row['tag'] = !empty($tag) ? $tag[0]->tag : "Unknown".$row['tag_id'];
					$where = (object)['city_id'=>intval($row['city_id']), 'tag_id'=>intval($row['tag_id'])];
					if ( !$this->getClass('CitiesTags')->exists( $where ) ){
						$where->score = intval($row['score']);
						$success = $this->getClass('CitiesTags')->add( $where );
						if (!$success && $success !== 0) $status = 0; // fail
						$results[] = [$where, $success];
						unset($row, $success);
					}
					else {
						$x = $this->getClass('CitiesTags')->get((object)['where'=>$where]);
						if (!empty($x)) {
							if ($x[0]->score != intval($row['score'])) {
								$where->score = intval($row['score']);
								$x = $this->getClass('CitiesTags')->set([(object)['fields'=>['score'=>intval($row['score'])],
																	              'where'=>$where]]);
								$status = $x ? "OK" : "fail";
							}
						}
						else {
							$status = 'fail';
							$results[] = [$where, "Cannot find tag"];
						}
					}
				}
				if ($status == 'OK') return true;
				else return $results;
			default: $status = 0; $out = 'Invalid query: '.$query; break;
		}
	}
}
if (!empty($_POST['query'])) new AJAX_wpAdmin();