<?php
namespace AH;
require_once(__DIR__.'../../../../../wp-includes/pluggable.php');
require_once(__DIR__.'../../../../../wp-config.php');
require_once(__DIR__.'../../../../../wp-load.php');
require_once(__DIR__.'../../../../../wp-includes/wp-db.php');
require_once(__DIR__.'/../_classes/_Controller.class.php');


define('OPT_SELLER',   0);
define('OPT_ADDRESS',  1);
define('OPT_IDS',      2);

class AJAX_wpAdmin extends Controller {
	// comes from Controller now
	// global $timezone_adjust;
	// protected $timezone_adjust = $timezone_adjust;
	private $logIt = true;
	protected $activeModification = true;
	protected $allowed = [1,3,4,5,8];
	protected $colors = [
		"100,100,100",
		"135,135,255",
		"251,135,135",
		"251,187,205",
		"135,255,135",
		"120,120,120",
		"251,187,205",
		"151,187,205",
		"240,240,140",
		"120,160,240"
	];

	protected $chimpCampaigns = [
		["key"=>"7-7-M","cid"=>"67e0c47b82","name"=>"Batch 1 (7/7)","type"=>8],
		["key"=>"7-8-M","cid"=>"6679381340","name"=>"New Mass Test","type"=>8],
		["key"=>"7-8-A","cid"=>"37dfe403dc","name"=>"Evening Send 7/8","type"=>8],
		["key"=>"7-8-E","cid"=>"301bc70bf3","name"=>"Evening Send 7/8 (Copy 01)","type"=>8],
		["key"=>"7-9-A","cid"=>"7573b76f66","name"=>"7-9 12=>36PM","type"=>8],
		["key"=>"7-10-M","cid"=>"3868d0bc86","name"=>"7/10 6AM","type"=>8],
		["key"=>"7-13-M","cid"=>"dcd55cbdf2","name"=>"Sentout 7/13AM","type"=>8],
		["key"=>"7-13-M-2-1","cid"=>"812081bb14","name"=>"Followup to batch 7/7","type"=>16],
		["key"=>"7-13-M-2-2","cid"=>"babeef62aa","name"=>"Followup to batch 7/7 (Copy 01)","type"=>16],
		["key"=>"7-14-M","cid"=>"1d43f87bd8","name"=>"Followup to New Mass Test", "type"=>16],
		["key"=>"7-14-A","cid"=>"cf512c1d5b","name"=>"Phase 2 Batch 1", "type"=>16],
		["key"=>"7-15-M","cid"=>"ade0645540","name"=>"FollowupA to Evening Send 7/8", "type"=>16],
		["key"=>"7-15-M-2","cid"=>"88db0b6d40","name"=>"FollowupB to Evening Send 7/9", "type"=>16]
	];

	public function __construct(){
		$in = parent::__construct();
		set_time_limit(0);
		try {
			// if (empty($in->query)) throw new \Exception (__FILE__.':('.__LINE__.') - no query sent');
			if (empty($in->query)) throw new \Exception ('no query sent');
			$this->log = $this->logIt ? new Log(__DIR__.'/../_classes/_logs/ajax-sellermanagement.log') : null;
			$this->log("ajax-sellermanagement doing $in->query");
			switch ($in->query){
					case 'unlock-user':
						if (!isset($in->data['user'])) throw new \Exception ('No user sent');
						$cmd = "unlock ".$in->data['user'];
						$out = [];
						if ( strpos( strtolower(php_uname()), 'linux') !== false) {
							exec($cmd, $out);
						// $Process = $this->getClass('Process', $cmd);
						// while( $Process->status() )
						// 	sleep(1);
						//$out = new Out('OK', "Unlocked ".$in->data['user']);
							$out = new Out('OK', $out);
						}
						else
							$out = new Out('fail', "Only on linux");
						break;
					/* seller's management */
					case 'delete-seller':
						if (!isset($in->data['id'])) throw new \Exception ('No id sent');
						$id = filter_var($in->data['id'], FILTER_SANITIZE_NUMBER_INT);
						$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$id]]);
						if (!empty($seller)) {
							
							$x = $this->getClass('Reservations')->delete(['seller_id'=>$id]);
							if (empty($x)) {
								$this->log("Did not delete Reservations with seller_id: $id");
							}

							if (!empty($seller[0]->author_id)) {
								$author = $seller[0]->author_id;

								$x = $this->getClass('Sellers')->delete(['id'=>$id]);
								if (empty($x)) {
									$this->log("Did not delete Seller with id: $id");
									$out = new Out('fail', "Did not delete Seller with id: $id");
									break;
								}
								
								$x = $this->getClass('SellersTags')->delete(['author_id'=>$author]);
								if (empty($x)) {
									$out = new Out('fail', "Did not delete SellersTags with author_id: $author");
									break;
								}
								global $wpdb;
								$sql = "DELETE FROM ".$wpdb->prefix."users WHERE ID=$author";
								$x = $this->getClass('Sellers')->rawQuery($sql);
								if (empty($x)) {
									$out = new Out('fail', "Did not delete USER with ID: $author");
									break;
								}
								$sql = "DELETE FROM ".$wpdb->prefix."usermeta WHERE user_id=$author";
								$x = $this->getClass('Sellers')->rawQuery($sql);
								if (empty($x)) {
									$out = new Out('fail', "Did not delete USERMETA with user_id: $author");
									break;
								}
								$out = new Out('OK', ['first_name'=>$seller[0]->first_name,
													  'last_name'=>$seller[0]->last_name]);
							}
							else {
								$seller = array_pop($seller);
								$x = $this->getClass('Sellers')->delete(['id'=>$id]);
								if (empty($x)) {
									$this->log("Did not delete Seller with id: $id");
									$out = new Out('fail', "Did not delete Seller with id: $id");
									break;
								}
								global $wpdb;
								$sql = "SELECT * FROM ".$wpdb->prefix."users WHERE user_email='$seller->email'";
								$user = $this->getClass('Sellers')->rawQuery($sql);
								if (!empty($user)) {
									$user = array_pop($user);
									$sql = "DELETE FROM ".$wpdb->prefix."users WHERE ID=$user->ID";
									$x = $this->getClass('Sellers')->rawQuery($sql);
									if (empty($x)) {
										$this->log("Did not delete USER with ID: $user->ID, sql:$sql");
										$out = new Out('fail', "Did not delete USER with ID: $user->ID");
										break;
									}
									$sql = "DELETE FROM ".$wpdb->prefix."usermeta WHERE user_id=$user->ID";
									$x = $this->getClass('Sellers')->rawQuery($sql);
									if (empty($x)) {
										$this->log("Did not delete USERMETA with user_id: $user->ID, sql:$sql");
										$out = new Out('fail', "Did not delete USERMETA with user_id: $user->ID");
										break;
									}
								}
								else
									$this->log("Could not get USER with email:$seller->email");
								$out = new Out('OK', ['first_name'=>$seller->first_name,
													  'last_name'=>$seller->last_name]);
							}
						}
						else
							$out = new Out('fail', 'Could not find seller with id:'.$id);
						break;

					case 'update-seller-portal-visits':
						$sellers = $this->getClass('Sellers')->get((object)['bitand'=>['flags'=>SELLER_IS_PREMIUM_LEVEL_1]]);
						if (!empty($sellers)) {
							$updated = 0;
							foreach($sellers as $seller) {
								if (!empty($seller->meta)) foreach($seller->meta as $meta) {
									if ($meta->action == SELLER_VISITATIONS) {
										$visits = 0;
										foreach($meta->users as $user) {
											foreach($user as $ip)
												foreach($ip as $visit)
													$visits++;
										}
										if ($visits &&
											$visits != $seller->portal_visits) {
											$this->getClass('Sellers')->set([(object)['where'=>['id'=>$seller->id],
																					  'fields'=>['portal_visits'=>$visits]]]);
											$updated++;
										}
									}
								}
							}
							$out = new Out('OK', ['total'=>$updated]);
						}
						else
							$out = new Out('OK', ['total'=>0]);
						break;
					case 'scrub-seller-tags':
						$opt = $this->getClass('Options', 1)->get((object)['where'=>['opt'=>'DirectoryPermittedUsers']]);
						if (!empty($opt)) {
							$this->getClass('Options')->log("DirectoryPermittedUsers - {$opt[0]->value}");
							$optAr = json_decode($opt[0]->value);
							if (count($optAr)) {
								foreach($optAr as $i=>$val)
									$optAr[$i] = intval($val);
								$this->allowed = array_merge($this->allowed, $optAr);
							}
						}
						$sellers = $this->getClass('Sellers')->get((object)['bitand'=>['flags'=>SELLER_IS_PREMIUM_LEVEL_2]]);
						$tagMods = 0;
						$sellerMods = 0;
						if (!empty($sellers)) foreach($sellers as $seller) {
							$amMeta = null;
							if (!empty($seller->author_id)) {
								if ( in_array($seller->author_id, $this->allowed) ) {
									unset($seller);
									continue;
								}
								$sellerTags = $this->getClass('SellersTags')->get((object)['where'=>['author_id'=>$seller->author_id]]);
								// $this->log("For author:$seller->author_id, found ".count($sellerTags)." sellers tags: ".print_r($sellerTags, true));
								if (!empty($seller->meta)) foreach($seller->meta as $meta) {
									if ($meta->action == SELLER_AGENT_ORDER) {
										if (!empty($meta->item)) {
											foreach($meta->item as &$sellerItem) {
												if ($sellerItem->type == ORDER_AGENT_MATCH &&
													$sellerItem->mode == ORDER_BOUGHT) {
													$this->log("author:$seller->author_id has sellerItem - {$sellerItem->specialtyStr}:{$sellerItem->locationStr}, {$sellerItem->specialty}:{$sellerItem->location}");
													if (!empty($sellerTags)) {
														foreach($sellerTags as &$tag) {
															// $this->log("author:$seller->author_id has tag - $tag->tag_id:$tag->city_id, type:".number_format($tag->type));
															if ($tag->city_id == $sellerItem->location &&
																$tag->tag_id == $sellerItem->specialty &&
																$tag->type == 1) {
																$tag->found = true;
																$sellerItem->found = true;
																$this->log("found tag for author:$seller->author_id in both {$sellerItem->specialtyStr}:{$sellerItem->locationStr}");
																break;
															}
														}
													}
												}
											}
										}
										$amMeta = $meta;
										break; // out of foreach($seller->meta as $meta) 
									} // end if ($meta->action == SELLER_AGENT_ORDER)
								} // end foreach($seller->meta as $meta) 
								// first set type to 0 if it wasn't found
								if (!empty($sellerTags)) {
									// $this->log("scrub-seller-tags - author:$seller->author_id, sellerTags: ".print_r($sellerTags, true));
									foreach($sellerTags as $id=>&$tag) {
										// $this->log("author:$seller->author_id test tag $id - $tag->tag_id:$tag->city_id, type:".number_format($tag->type));
										if (!isset($tag->found) &&
											$tag->type == 1 &&
											$tag->tag_id != 170 /*multilingual*/) {
											$tagMods++;
											if ($this->activeModification) {
												$x = $this->getClass('SellersTags')->set([(object)['where'=>['author_id'=>$tag->author_id,
																											  'city_id'=>$tag->city_id,
																											  'tag_id'=>$tag->tag_id],
																									'fields'=>['type'=>0]]]);
												$this->log("scrub-seller-tags - type to 0 for author:$tag->author_id, $tag->tag_id:$tag->city_id was ".(!empty($x) ? 'successful' : 'a failure'));
											}
											else
												$this->log("scrub-seller-tags - type to 0 for author:$tag->author_id, $tag->tag_id:$tag->city_id would have happened");
										}
									}
								}
								if ($amMeta &&
									!empty($amMeta->item)) {
									$SellersTags = $this->getClass('SellersTags');
									foreach($amMeta->item as &$sellerItem) {
										if ($sellerItem->type == ORDER_AGENT_MATCH &&
											$sellerItem->mode == ORDER_BOUGHT &&
											!isset($sellerItem->found)) {
											if ( $SellersTags->exists(['author_id'=>$seller->author_id,
															   		   'city_id'=>$sellerItem->location,
																	   'tag_id'=>$sellerItem->specialty])) {
												$tag = $SellersTags->get((object)['where'=>['author_id'=>$seller->author_id,
																							'city_id'=>$sellerItem->location,
																							'tag_id'=>$sellerItem->specialty]]);
												if (!empty($tag) && $tag[0]->type != 1) {
													if ($this->activeModification) {
														$SellersTags->set([(object)['where'=>['author_id'=>$seller->author_id,
																							  'city_id'=>$sellerItem->location,
																							  'tag_id'=>$sellerItem->specialty],
																							  'fields'=>['type'=>1]]]);
														$this->log("updated tag for author_id:$seller->author_id, city_id:{$sellerItem->location}, city:{$sellerItem->locationStr}, specialty:{$sellerItem->specialty},{$sellerItem->specialtyStr}, score:{$tag[0]->score}, type:1");
													}
													else
														$this->log("updated tag for author_id:$seller->author_id, city_id:{$sellerItem->location}, city:{$sellerItem->locationStr}, specialty:{$sellerItem->specialty},{$sellerItem->specialtyStr}, score:{$tag[0]->score}, type:1 would have happened");
												}
												else
													$this->log("tag already exists for author_id:$seller->author_id, city_id:{$sellerItem->location}, city:{$sellerItem->locationStr}, specialty:{$sellerItem->specialty},{$sellerItem->specialtyStr}, score:{$tag[0]->score}, type:1");
											}
											else {
											// add to sellers tag table
												$tagId = intval($sellerItem->specialty);
										
												$sql = "SELECT a.*, c.type FROM {$SellersTags->getTableName('cities-tags')} AS a ";
												$sql.= "INNER JOIN {$SellersTags->getTableName('cities')} AS b ON b.id = a.city_id ";
												$sql.= "INNER JOIN {$SellersTags->getTableName('tags')} as c ON c.id = a.tag_id ";
												$sql.= "WHERE b.id={$sellerItem->location} AND a.tag_id=$tagId";
												// $this->log("process-agent-match-invite - sql:$sql");

												$tag = $SellersTags->rawQuery($sql);				
												$score = (!empty($tag) && $tag[0]->type == 1 ? ($tag[0]->score == -1 ? 10 : $tag[0]->score) : 0);

												$sellerMods++;
												if ($this->activeModification) {
													$x = $SellersTags->add(['author_id'=>$seller->author_id,
																			'city_id'=>$sellerItem->location,
																			'tag_id'=>$sellerItem->specialty,
																			'score'=>$score,
																			'type'=>1]);
													$this->log("scrub-seller-tags - added tag for author_id:$seller->author_id, city_id:{$sellerItem->location}, city:{$sellerItem->locationStr}, specialty:{$sellerItem->specialty},{$sellerItem->specialtyStr}, score:{$tag[0]->score}, type:1 was ".(!empty($x) ? 'successful' : 'a failure'));
												}
												else
													$this->log("scrub-seller-tags - added tag for author_id:$seller->author_id, city_id:{$sellerItem->location}, city:{$sellerItem->locationStr}, specialty:{$sellerItem->specialty},{$sellerItem->specialtyStr}, score:{$tag[0]->score}, type:1 would have happened");
											}
										}
									}
								}
								unset($sellerTags);
							} // end if (!empty($seller->author_id))
							unset($seller);
						}
						$out = new Out('OK', ['tagMods'=>$tagMods,
											  'sellerMods'=>$sellerMods]);
						break;
					case 'get-seller-password':
						if (!isset($in->data['id'])) throw  new \Exception('No id value sent.');
						if (!isset($in->data['author_id'])) throw  new \Exception('No author_id value sent.');
						$id = $in->data['id'];
						$author_id = $in->data['author_id'];
						$this->log("get-seller-password - id:$id, author_id: $author_id");
						$testPasswd = '';
						$magic = get_user_meta($author_id, 'user_key', true);
						$i = 0;
						if (!empty($magic)) foreach($magic as $value)
							$testPasswd .= chr( $value ^ ($i++ + ord('.')));
						else {
							$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$id]]);
							if (!empty($seller) &&
								!empty($seller[0]->meta)) {
								foreach($seller[0]->meta as $meta) {
									if ($meta->action == SELLER_KEY) {
										$key = json_decode($meta->key);
										$len = count($key); // strlen($meta->key);
										for($i = 0; $i < $len; $i++)
											$testPasswd .= chr( $key[$i] ^ ($i + ord('.')) );
									}
								}
							}							
						}
						$login = '';
						if (!empty($testPasswd)) {
							$user = get_userdata($author_id);
							if ($user !== false )
								$login = $user->user_login;
						}
						$out = new Out(empty($testPasswd) ? 'fail' : 'OK', empty($testPasswd) ? "Cannot find password for id:$id" : (object)['pwd'=>$testPasswd,
																																			 'login'=>$login]);
						break;

					case 'update-seller-password':
						if (!isset($in->data['id'])) throw  new \Exception('No id value sent.');
						if (!isset($in->data['author_id'])) throw  new \Exception('No author_id value sent.');
						if (!isset($in->data['password'])) throw  new \Exception('No password value sent.');
						$id = $in->data['id'];
						$author_id = $in->data['author_id'];
						$password = $in->data['password'];
						$user_info = get_userdata($author_id);
						$this->log("update-seller-password - id:$id, author_id: $author_id, pswd:$password");
						$r = $this->getClass('Register');
						$out = $r->userResetPassword($user_info, $password, '', true);
						break;

					case 'make-radian-array':
						require_once(__DIR__.'/../_classes/DistanceScale.php');
						$tagSet = $this->getClass('Sellers')->getAMTagList();
						$steps = count($tagSet);
						$radialList = [];
						$radialListStr = '[';
						foreach($DistanceScale as $index=>$scale) {
							$radials = [];
							$radialsStr = '[';
							$deltaLng = 1/$DistanceScale[$index][0];
							$deltaLat = 1/$DistanceScale[$index][1]; // pretty constant
							for($i = 0; $i < $steps; $i++) {
								$rad = (360/$steps) * (M_PI/180) * $i;
								$x = cos( $rad ) * $deltaLng;
								$y = sin( $rad ) * $deltaLat;
								$radials[] = [$x, $y];
								$radialsStr .= "[$x,$y],";
							}
							$radialsStr .= ']';
							$radialList[] = $radials;
							$radialListStr .= $radialsStr.",\n";
							unset($scale, $radials);
						}
						$radialListStr .= '];';
						// $out = new Out('OK', ['txt'=>print_r($radialList, true)]);
						$file = __DIR__.'/../_classes/RadialDistance.php';
						$fileContents = "<?php\nnamespace AH;\n// lng, lat arrays per 5 degrees starting from 5, 15, 25, etc...,  each containing stepped radial offsets for # of agent match tags we have from Sellers::getAMTagList()\n".'$'."RadialDistance = $radialListStr";
						file_put_contents($file, $fileContents);

						$out = new Out('OK', ['txt'=>$radialListStr]);
						break;
					case 'get-zoho-crm-csv':
						$reservations = $this->getClass('Reservations')->get();
						$blankCity = ''; $blankState = '';
						$blank1 = '';
						$blank2 = '';
						$blank3 = '';
						$csv = "Email, First Name, Last Name, Phone, Seller ID, Street, City, State, Company, Portal, Target City, Target State, LifeStyle 1, LifeStyle 2, LifeStyle 3, Date\n";
						foreach($reservations as $reserve) {				
							$seller = null;
							if (!empty($reserve->seller_id)) {
								$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$reserve->seller_id]]);
								if (!empty($seller))
									$seller = array_pop($seller);
							}
							$lastName = ($pos = strpos($reserve->last_name, ",")) === false ? $reserve->last_name : substr($reserve->last_name, 0, $pos);
							$lastName = ucwords(strtolower($lastName));
							if (substr($lastName, 0,2) == "Mc" &&
								strlen($lastName) > 2)
								$lastName[2] = chr( ord($lastName[2]) - 32 ); // cap that 3rd letter
							$csv .= strtolower($reserve->email).", ".ucwords(strtolower($reserve->first_name)).", ".$lastName.", $reserve->phone, ";
							if (!empty($seller)) {
								$company = !empty($seller->company) ? ( (($pos = strpos($seller->company, ",")) !== false) ? $seller->company = str_replace(",", " ", $seller->company) : $seller->company) : 'Unknown';
								$address = !empty($seller->street_address) ? ( (($pos = strpos($seller->street_address, ",")) !== false) ? $seller->street_address = substr($seller->street_address, 0, $pos) : $seller->street_address) : '';
								$city = !empty($seller->city) ? ( (($pos = strpos($seller->city, ",")) !== false) ? $seller->city = str_replace(",", "-", $seller->city) : $seller->city) : '';
								$csv .= $seller->id.", ".$address.", ";
								$csv .= $city.", ";
								$csv .= (!empty($seller->state) ? $seller->state : '').", ";
								$csv .= ucwords(strtolower($company)).", ";
								unset($seller);
							}
							else {
								$csv .= "0, $blank1, $blank2, $blank3, Unknown, ";
							}
							$csv .= (!empty($reserve->portal) ? $reserve->portal : '').", ";
							if (empty($reserve->meta)) $csv .= "$blankCity, $blankState, $blank1, $blank2, $blank3, $reserve->added\n";
							else foreach($reserve->meta as $meta) {
								if ($meta->action == ORDER_AGENT_MATCH) {
									$meta = (object)$meta;
									$nth = 0;
									foreach($meta->item as $item) {
										$item = (object)$item;
										if (!$nth)
											$csv .= "$item->locationStr, ";
										$csv .= "$item->specialtyStr, ";
										$nth++;
										unset($item);
									}
									for( ; $nth < 3; $nth++)
										$csv .= "$blank1, ";
									$csv .= "$reserve->added\n";
								}
								unset($meta);
							}
							unset($reserve);
						}
						$out = new Out('OK', ['csv'=>$csv]);
						break;
					case 'rebuild-meta':
						$count = 0;
						$bypassed = 0;

						$x = $this->getClass('Options')->get((object)array('where'=>array('opt'=>'ChimpCampaigns')));
						if (!empty($x))
							$this->chimpCampaigns = json_decode($x[0]->value);

						// create a list of last timestamp for each type of campaign
						$timestamps = [];
						foreach($this->chimpCampaigns as $camp) {
							$camp = (object)$camp;
							$summary = $this->getClass('Chimp')->getSummary($camp);
							$startTime = date('Y-m-d H:i:s', strtotime($summary['timeseries'][0]['timestamp']) + ($this->timezone_adjust*3600));
							if (!isset($timestamps[$camp->type]))
								$timestamps[$camp->type] = ['first'=>$startTime,
															'unix_first'=>strtotime($startTime),
															'last'=>$startTime,
															'unix_last'=>strtotime($startTime)];
							else {
								$timestamps[$camp->type]['last'] =$startTime;
								$timestamps[$camp->type]['unix_last'] = strtotime($startTime);
							}
							unset($camp, $summary);
						}

						$sellersEmailDb = $this->getClass('SellersEmailDb');
						// get data for all EmailTrackers that has info (meta) and matches EmailDb's with PROMO_FIRST and PROMO_SECOND
						$sql = "SELECT b.* , a.id AS sed_id FROM {$sellersEmailDb->getTableName()} AS a ";
						$sql.= "INNER JOIN {$sellersEmailDb->getTableName('email-tracker')} AS b ON a.seller_id = b.agent_id ";
						$sql.= "WHERE a.flags & ( ".PROMO_FIRST." | ".PROMO_SECOND.") AND b.meta IS NOT NULL ";

						$dataSet = $sellersEmailDb->rawQuery($sql);
						foreach($dataSet as $data) {
							$sed = $this->getClass('SellersEmailDb')->get((object)['where'=>['id'=>$data->sed_id]]);
							if (empty($sed))
								continue;
							$sed = array_pop($sed);
							$data->meta = json_decode($data->meta);
							$needUpdate = false;
							$flags = 0;
							$originalSedFlags = $sed->flags;
							foreach($data->meta as $etMeta) {
								if ( strtotime($etMeta->date) < $timestamps[PROMO_FIRST]['unix_first'] )
									continue; // ignore this, happened before chimp campaings

								if ($etMeta->action == ET_LISTING_VIEWED) {
									$needIt = true;
									if (!empty($sed->meta)) foreach($sed->meta as $meta) {
										if ($meta->action == VIEWED_LISTING &&
											(strtotime($meta->date) - strtotime($etMeta->date)) < 5) { // existing is within 5 secs, so this one is good
											$needIt = false;
										}
										unset($meta);
									}
									if ($needIt) {
										$respondMeta = new \stdClass();
										$respondMeta->action = VIEWED_LISTING;
										$respondMeta->date = $etMeta->date;
										$respondMeta->emailTrackerId = $data->id;
										$respondMeta->listing = $etMeta->listing;
										if (empty($sed->meta))
											$sed->meta = [$respondMeta];
										else
											$sed->meta[] = $respondMeta;
										$needUpdate = true;
										$sed->flags |= RESPONDED_TO_VIEW_LISTING;
										$this->log("rebuild-meta for sellerId: $sed->seller_id adding VIEWED_LISTING, $etMeta->listing on $etMeta->date");
										unset($respondMeta);
									}
								}
								elseif ($etMeta->action == ET_AGENT_RESPONSE) {
									$needIt = true;
									$action = RESPONSE_FIRST_EMAIL; // default
									$flag = RESPONDED_TO_FIRST;     // default
									if (strpos($etMeta->to, 'UNSUBSCRIBE') !== false) {
										$action = EMAIL_UNSUBSCRIBED_DATA;
										$flag = EMAIL_UNSUBSCRIBED;
									}
									elseif (strpos($etMeta->to, 'PROMO EMAIL') !== false)
										$flag = RESPONDED_TO_EMAIL;
									elseif (strpos($etMeta->to, 'INTRO EMAIL') !== false)
										$flag = RESPONDED_TO_FIRST;
									elseif (strpos($etMeta->to, 'SECOND EMAIL') !== false)
										$flag = RESPONDED_TO_SECOND;
									elseif (strpos($etMeta->to, 'THIRD EMAIL') !== false) {
										$action = RESPONSE_THIRD_EMAIL;
										$flag = RESPONDED_TO_THIRD;
									}
									elseif (strpos($etMeta->to, 'FOURTH EMAIL') !== false) {
										$action = RESPONSE_FOURTH_EMAIL;
										$flag = RESPONDED_TO_FOURTH;
									}
									elseif (strpos($etMeta->to, 'IMPROVE IT') !== false) {
										$action = IMPROVE_LISTING_RESPONSE;
										$flag = RESPONDED_TO_IMPROVE_LISITNG;
									}
									else {
										$this->log("rebuild-meta for sellerId: $sed->seller_id unknown meta:$etMeta->to");
										continue; // don't know what it is...
									}

									// check for anomaly where etMeta has SECOnD EMAIL (or EMAIL), but the timestamp is less than first mailing
									// of the second campaign.  In this case, reversed logic, in that default is for FIRST, so only set it to
									// SECOND if the timestamp of etMeta is greater than start of 2nd campagin.
									if ( ($flag == RESPONDED_TO_EMAIL ||
										  $flag == RESPONDED_TO_SECOND) &&
										 strtotime($etMeta->date) >= $timestamps[PROMO_SECOND]['unix_first'] ) {
										$action = RESPONSE_SECOND_EMAIL;
										$flag = RESPONDED_TO_SECOND;
									}

									if (!empty($sed->meta)) foreach($sed->meta as $meta) {
										if ($meta->action == $action &&
											(strtotime($meta->date) - strtotime($etMeta->date)) < 5) { // existing is within 5 secs, so this one is good
											$needIt = false;
										}
										unset($meta);
									}
									if ($needIt) {
										$respondMeta = new \stdClass();
										$respondMeta->action = $action;
										$respondMeta->date = $etMeta->date;
										$respondMeta->emailTrackerId = $data->id;
										$respondMeta->type = "LLC Internal";
										if (empty($sed->meta))
											$sed->meta = [$respondMeta];
										else
											$sed->meta[] = $respondMeta;
										$needUpdate = true;
										$sed->flags |= $flag;
										$this->log("rebuild-meta for sellerId: $sed->seller_id adding ".$this->emailDbMetaActionType($action)." , date:$etMeta->date");
										unset($respondMeta);
									}
								}
								unset($etMeta);
							} // foreach($data->meta as $etMeta) 

							if ($needUpdate) {
								$fields = [];
								$fields['meta'] = $sed->meta;
								if ($sed->flags != $originalSedFlags)
									$fields['flags'] = $sed->flags;
								$x = $this->getClass('SellersEmailDb')->set([(object)['where'=>['id'=>$sed->id],
												   								  	  'fields'=>$fields]]);
								$this->log("rebuild-meta updating from EmailTracker:$data->id, for sellerId: $sed->seller_id was ".(empty($x) ? "a failure" : "successful"));
								$count++;
							}
							unset($sed, $data);
						} // foreach($dataSet as $data) 

						// now deal with reservations
						$sql = "SELECT a . * , b.id as sed_id FROM  {$sellersEmailDb->getTableName('reservations')} AS a ";
						$sql.= "INNER JOIN {$sellersEmailDb->getTableName()} AS b ON a.seller_id = b.seller_id";

						$dataSet = $sellersEmailDb->rawQuery($sql);
						foreach($dataSet as $data) {
							$sed = $this->getClass('SellersEmailDb')->get((object)['where'=>['id'=>$data->sed_id]]);
							if (empty($sed))
								continue;
							$sed = array_pop($sed);
							$data->meta = json_decode($data->meta);
							$needUpdate = false;
							$flags = 0;
							$originalSedFlags = $sed->flags;
							$agentMatch = null;
							$needIt = true;
							if (!empty($data->meta)) foreach($data->meta as $reserve) {
								if ($reserve->action == ORDER_AGENT_MATCH) {
									$agentMatch = $reserve;
									break;
								}
								unset($reserve);
							}

							if ($agentMatch) {
								if (!empty($sed->meta) )
									foreach($sed->meta as $meta) 
										if ($meta->action == RESERVED_AGENT_MATCH) {
											if ( (strtotime($data->added) - strtotime($meta->date)) < 5) {
												$this->log("rebuild-meta for sellerId: $sed->seller_id has a reservation dated: $data->added, but has one in emailDb with $meta->date");
												$needIt = false;
											}
										}
							}
							else
								$needIt = false;

							if ($needIt &&
								$agentMatch) {
								$meta = new \stdClass();
								$meta->action = RESERVED_AGENT_MATCH;
								$meta->date = $data->added;
								$meta->reservation_id = $data->id;
								$meta->lifestyles = $agentMatch->item;
								if (empty($sed->meta))
									$sed->meta = [$meta];
								else
									$sed->meta[] = $meta;
								$needUpdate = true;
								$sed->flags |= RESERVATION_MADE;
								$this->log("rebuild-meta for sellerId: $sed->seller_id adding agentMatch from id:$data->id made $data->added");
								unset($meta);
							}
							unset($agentMatch);
							
							$needIt = true;
							if (isset($data->portal) &&
								!empty($data->portal) ) {
								if (!empty($sed->meta)) 
									foreach($sed->meta as $meta) 
										if ($meta->action == RESERVED_PORTAL) {
											if ($data->portal == $meta->portal) {
												$needIt = false;
											}
										}
								
							}
							else
								$needIt = false;

							if ( isset($data->portal) &&
								 !empty($data->portal) &&
								 $needIt) {
								$meta = new \stdClass();
								$meta->action = RESERVED_PORTAL;
								$meta->date = $data->added;
								$meta->reservation_id = $data->id;
								$meta->portal = $data->portal;
								if (empty($sed->meta))
									$sed->meta = [$meta];
								else
									$sed->meta[] = $meta;
								$needUpdate = true;
								$sed->flags |= RESERVATION_MADE;
								$this->log("rebuild-meta for sellerId: $sed->seller_id adding portal:$data->portal from id:$data->id");
								unset($meta);
							}

							if ($needUpdate) {
								$fields = [];
								$fields['meta'] = $sed->meta;
								if ($sed->flags != $originalSedFlags)
									$fields['flags'] = $sed->flags;
								$x = $this->getClass('SellersEmailDb')->set([(object)['where'=>['id'=>$sed->id],
												   								  	  'fields'=>$fields]]);
								$this->log("rebuild-meta updating from Reservation:$data->id, for sellerId: $sed->seller_id was ".(empty($x) ? "a failure" : "successful"));
								$count++;
							}
							unset($data, $sed);
						} //foreach($dataSet as $data) 

						$this->log("rebuild-meta finished processing $page pages, mdified:$count, bypassed:$bypassed");
						$out = new Out('OK', (object)['total'=>$count]);
						break;

					case 'fix-email-response':
						$count = 0;
						$bypassed = 0;
						$page = 0;
						$pagePer = 2000;
						$working = true;
						//these flags should pick of 2nd, 3rd, 4th emailing, but has been flagged to be a 1st or any response
						$queryFlags = PROMO_SECOND | PROMO_THIRD | PROMO_FOURTH | RESPONDED_TO_FIRST | RESPONDED_TO_EMAIL;
						//do the adding meta section
						while( $working ) {					
							$emailDbs = $this->getClass("SellersEmailDb")->get((object)['bitand'=>['flags'=>$queryFlags],
																								  'page'=>$page,
																								  'limit'=>$pagePer]);
							$page++;
							if ( empty($emailDbs) ) {
								break;
							}

							foreach($emailDbs as $emailDb) {
								if (!empty($emailDb->meta))  {
									$metas = [];
									$gotOne = false;
									$firstResponse = null;
									$replaced = false;
									$sentFlags = [PROMO_FOURTH, PROMO_THIRD, PROMO_SECOND];
									foreach($sentFlags as $sentFlag) {
										$gotTestSend = $emailDb->flags & $sentFlag; // which send out was it?
										if (!$gotTestSend)
											continue;
										$testAction = $this->getResponseForCampaign($gotTestSend);
										$haveActionForThisSend = false;
										foreach($emailDb->meta as $meta) {
											if ($meta->action & (RESPONSE_FIRST_EMAIL | RESPONSE_TO_EMAIL)) // then this email db has meta for receiving 1st email, 
												$firstResponse = $meta;					// but in fact was subsequent one, so replace it
											else {
												$metas[] = $meta;
												if ($meta->action == $testAction)
													$haveActionForThisSend = true;
											}
											unset($meta);
										}
										if ( $firstResponse &&
											 !$haveActionForThisSend ) { // then we have suspectd meta, but didn't have associated meta for $gotTestSend
											$firstResponse->action = $testAction; // replace RESPONSE_FIRST_EMAIL
											$metas[] = $firstResponse;
											$replaced = true;
											// reset flags
											$emailDb->flags = ($emailDb->flags & ~(RESPONDED_TO_FIRST | RESPONDED_TO_EMAIL)) | $this->getFlagFromAction($testAction);
											$this->getClass("SellersEmailDb")->set([(object)['where'=>['id'=>$emailDb->id],
																							 'fields'=>['flags'=>$emailDb->flags,
																							 			'meta'=> $metas]]]);
											$count++;
											if ( isset($firstResponse->emailTrackerId) &&
												 $firstResponse->emailTrackerId > 0 ) {
												$tracker = $this->getClass('EmailTracker')->get((object)['where'=>['id'=>$firstResponse->emailTrackerId]]);
												if (!empty($tracker)) {
													$tracker = array_pop($tracker);
													$trackerMeta = new \stdClass();
													$trackerMeta->action = ET_AGENT_RESPONSE;
													$trackerMeta->date = $firstResponse->date;
													switch($gotTestSend) {
														case PROMO_SECOND:
															$trackerMeta->to = "response to SECOND EMAIL";
															break;
														case PROMO_THIRD: 
															$trackerMeta->to = "response to THRD EMAIL";
															break;
														case PROMO_FOURTH: 
															$trackerMeta->to = "response to FOURTH EMAIL";
															break;
													}
													if (empty($tracker->meta))
														$tracker->meta = [$trackerMeta];
													else
														$tracker->meta[] = $trackerMeta;
													$this->getClass("EmailTracker")->set([(object)['where'=>['id'=>$tracker->id],
																		   						  'fields'=>['meta'=>$tracker->meta]]]);
													unset($tracker);
												}
											}
										}
										unset($metas);
									}
									if (!$replaced)
										$bypassed++;
								}
								else
									$bypassed++;
								unset($emailDb);
							}
							if ( count($emailDbs) < $pagePer)
								$working = false;
							unset($emailDbs);
						}

						// now do the delete extra meta one section
						// $page = 0;
						// $working = true;
						// $queryFlags = PROMO_FIRST | PROMO_SECOND | PROMO_THIRD | PROMO_FOURTH;
						// while ( $working ) {
						// 	$emailDbs = $this->getClass("SellersEmailDb")->get((object)['bitand'=>['flags'=>$queryFlags],
						// 																		  'page'=>$page,
						// 																		  'limit'=>$pagePer]);
						// 	$page++;
						// 	if ( empty($emailDbs) ) {
						// 		break;
						// 	}

						// 	$haveMetaFlags = [PROMO_FIRST, PROMO_SECOND, PROMO_THIRD, PROMO_FOURTH];
						// 	foreach($emailDbs as $emailDb) {
						// 		if (empty($emailDb->meta))
						// 			contineu;
						// 		// $shouldHaveOneOfs = $emailDb->flags & $queryFlags;
						// 		$shouldHaveMeta = 0;
						// 		foreach($haveMetaFlags as $haveMetaFlag) 
						// 			if ( ($emailDb->flags & $haveMetaFlag) != 0)
						// 				$shouldHaveMeta |= $this->getResponseForCampaign($haveMetaFlag);


						// 		$gotOne = [];
						// 		$shouldBe = false;
						// 		$metas = [];
						// 		foreach($emailDb->meta as $meta){
						// 			if ( ($meta->action & $shouldHaveMeta) != 0 &&
						// 				 !isset($gotOne[$meta->action])) {
						// 				$gotOne[$meta->action] = 1;
						// 				$metas[] = $meta;
						// 			}
						// 			unset($meta);	
						// 		}
						// 		if ( count($metas) != count($emailDb->meta) )
						// 			$this->getClass("SellersEmailDb")->set([(object)['where'=>['id'=>$emailDb->id],
						// 															 'fields'=>['meta'=> $metas]]]);
						// 		unset($gotOne, $metas, $emailDb);
						// 	}

						// 	if ( count($emailDbs) < $pagePer)
						// 		$working = false;
						// 	unset($emailDbs);
						// }
						$this->log("fix-email-response finished processing $page pages, mdified:$count, bypassed:$bypassed");
						$out = new Out('OK', (object)['total'=>$count]);
						break;
					case 'rebuild-flags':
						if (!isset($in->data['what'])) throw  new \Exception('No what value sent.');
						$page = 0;
						$pagePer = 2000;
						$count = 0;
						$listingFlags = SED_ALL_INACTIVE | SED_ACTIVE | SED_WAITING | SED_REJECTED | SED_TOOCHEAP | SED_NEVER | SED_AVERAGE;
						$bypassed = 0;
						while( true ) {
							$emailDbs = $this->getClass("SellersEmailDb")->get((object)['page'=>$page,
																						'limit'=>$pagePer]);
							if ( empty($emailDbs) ) {
								break;
							}
							$page++;
							$this->log("rebuild-flags processing page:$page at limit:".($page*$pagePer));
							foreach($emailDbs as $emailDb) {
								if (!empty($emailDb->meta)) {
									$flag = 0;
									foreach($emailDb->meta as $meta) {
										$flag |= $this->getFlagFromAction($meta->action);
										unset($meta);
									}
									if ($flag) {
										if ( ($emailDb->flags & ~$listingFlags) == $flag)
											$bypassed++;
										else {
											$count++;
											$flag |= $emailDb->flags & $listingFlags; // strip out all flags except for listing related flags
										 	$this->getClass('SellersEmailDb')->set([(object)['where'=>['id'=>$emailDb->id],
																 							'fields'=>['flags'=>$flag]]]);
										}
									}

								}
								unset($emailDb);
							}
							unset($emailDbs);
						}
						$this->log("rebuild-flags finished processing $page pages, mdified:$count, bypassed:$bypassed");
						$out = new Out('OK', (object)['total'=>$count]);
						break;
					case 'clean-meta':
						if (!isset($in->data['index'])) throw  new \Exception('No index value sent.');
						if (!isset($in->data['resetFlag'])) throw  new \Exception('No resetFlag value sent.');
						if (!isset($in->data['removeMeta'])) throw  new \Exception('No removeMeta value sent.');
						$action = intval($in->data['index']);
						$resetFlag = intval($in->data['resetFlag']);
						$removeMeta = intval($in->data['removeMeta']);

						$flag = $this->getFlagFromAction($action);

						$this->log("clean-meta - entered for action:$action for flag:$flag");
						$count = 0;
						if ($flag ||
							(!$flag && !$resetFlag)) {
							$page = 0;
							$pagePer = 2000;
							$working = true;
							while( $working ) {
								$q = new \stdClass();
								if ($flag)
									$q->bitand = ['flags'=>$flag];
								$q->page = $page;
								$q->limit = $pagePer;
								$emailDbs = $this->getClass("SellersEmailDb")->get($q);
								unset($q);
								$page++;
								if (empty($emailDbs)) {
									$this->log("clean-meta - got empty list on page:$page");
									break;
								}

								$this->log("clean-meta - cleaning page $page, up to id:".($page*$pagePer));
								foreach($emailDbs as $emailDb) {
									$metas = [];
									$fields = [];
									$needUpdate = false;
									if ($removeMeta &&
									 	$emailDb->meta) {
									 	foreach($emailDb->meta as $meta) {
									 		if ($meta->action == $action)
									 			$needUpdate = true;
									 		else
									 			$metas[] = $meta;
									 		unset($meta);
									 	}
									 }
									 if ($needUpdate)
									 	$fields['meta'] = $metas;

									 if ( $resetFlag &&
									 	  ($emailDb->flags & $flag) != 0 ) {
									 	$needUpdate = true;
									 	$fields['flags'] = $emailDb->flags &  ~$flag;
									 }
									 if ($needUpdate) {
									 	$count++;
									 	$this->getClass('SellersEmailDb')->set([(object)['where'=>['id'=>$emailDb->id],
															 							'fields'=>$fields]]);
									 }
									 unset($metas, $emailDb, $fields);									 
								}
								$this->log("clean-meta - done with page:$page");
								unset($emailDbs);
							}
						}
						$out = new Out('OK', (object)['total'=>$count]);
						break;
					case 'get-chimp-complaints':
						if (!isset($in->data['index'])) throw  new \Exception('No index value sent.');
						$index = $in->data['index'];
						$list = $this->getClass('Chimp')->getChimpComplaints($index);
						$out = new Out('OK', $list);
						break;
					case 'get-chimp-unsubscribes':
						if (!isset($in->data['index'])) throw  new \Exception('No index value sent.');
						$index = $in->data['index'];
						$list = $this->getClass('Chimp')->getChimpUnsubscribes($index);
						$out = new Out('OK', $list);
						break;
					case 'get-chimp-open':
						if (!isset($in->data['index'])) throw  new \Exception('No index value sent.');
						$index = $in->data['index'];
						$list = $this->getClass('Chimp')->getChimpOpen($index);
						$out = new Out('OK', $list);
						break;
					case 'update-campaign-sentto':
						if (!isset($in->data['index'])) throw  new \Exception('No index value sent.');
						$index = $in->data['index'];
						$this->log("update-campaign-sentto for $index, calling updateCampaignSentTo");
						$list = $this->getClass('Chimp')->updateCampaignSentTo($index);
						$out = new Out('OK', $list);
						break;
					case 'update-campaign-sentto-perpage':
						if (!isset($in->data['index'])) throw  new \Exception('No index value sent.');
						$index = $in->data['index'];
						$this->log("update-campaign-sentto for $index, calling updateCampaignSentToOnePage");
						$list = $this->getClass('Chimp')->updateCampaignSentToOnePage($index);
						$out = new Out('OK', $list);
						break;
					case 'update-campaign-compaints-percampaign':
						if (!isset($in->data['index'])) throw  new \Exception('No index value sent.');
						$index = $in->data['index'];
						$this->log("update-campaign-compaints-percampaign for $index, calling updateChimpComplaintsPerCampaign");
						$list = $this->getClass('Chimp')->updateChimpComplaintsPerCampaign($index);
						$out = new Out('OK', $list);
						break;
					case 'get-email-activity-stats':
						$this->log("entered get-email-activity-stats");
						$chimp = $this->getClass('Chimp');
						$this->log("entered get-email-activity-stats calling updateChimpComplaints");
						$out = $chimp->updateChimpComplaints();
						$this->log("get-email-activity-stats - updated ".$out->data." chimp complaint accounts");
						$this->log("entered get-email-activity-stats calling updateChimpSentTo");
						$out = $chimp->updateChimpSentTo();
						$this->log("get-email-activity-stats - updated ".$out->data." chimp sentTo accounts");
						$sendFlags = PROMO_FIRST | PROMO_SECOND | PROMO_THIRD | PROMO_FOURTH | IMPROVE_LISTING; // | NO_TABLE_INTRO;
						$respondFlags = RESPONDED_TO_FIRST | RESPONDED_TO_SECOND | RESPONDED_TO_THIRD | RESPONDED_TO_FOURTH | RESPONDED_TO_EMAIL | RESPONDED_TO_IMPROVE_LISITNG | RESPONDED_TO_VIEW_LISTING | RESERVATION_MADE | EMAIL_UNSUBSCRIBED;

						$x = $this->getClass('Options')->get((object)array('where'=>array('opt'=>'ChimpCampaigns')));
						if (!empty($x))
							$this->chimpCampaigns = json_decode($x[0]->value);

						// create a list of last timestamp for each type of campaign
						$timestamps = [];
						foreach($this->chimpCampaigns as $camp) {
							$camp = (object)$camp;
							$summary = $this->getClass('Chimp')->getSummary($camp);
							$startTime = date('Y-m-d H:i:s', strtotime($summary['timeseries'][0]['timestamp']) + ($this->timezone_adjust*3600));
							if (!isset($timestamps[$camp->type]))
								$timestamps[$camp->type] = ['first'=>$startTime,
															'unix_first'=>strtotime($startTime),
															'last'=>$startTime,
															'unix_last'=>strtotime($startTime)];
							else {
								$timestamps[$camp->type]['last'] =$startTime;
								$timestamps[$camp->type]['unix_last'] = strtotime($startTime);
							}
							unset($camp, $summary);
						}

						$chartActivity = [];
						$whatTypes = 0;
						$h = '<table><thead><tr>';
						$h.= 	'<th style="width:12%;">Seller</th>';
						$h.=	'<th style="width:10%;">City</th>';
						$h.=	'<th style="width:5%;">State</th>';
						$h.=	'<th style="width:12%;">Server</th>';
						$h.=	'<th>Activity</th>';
						$h.= '</tr></thead><tbody>';

						$page = 0;
						$pagePer = 2000;

						while( true ) {
						// if (empty($emailDbs)) {
						// 	$out = new Out('OK', "<span>No activity found</span>");
						// 	break;
						// }
							$emailDbs = $this->getClass("SellersEmailDb")->get((object)['bitand'=>['flags'=>($sendFlag | $respondFlags)],
																								  'page'=>$page,
																								  'limit'=>$pagePer]);
							$page++;
							if ( empty($emailDbs) ) {
								$this->log("get-email-activity-stats - got empty list on page:$page");
								break;
							}

							foreach($emailDbs as $emailDb) {
								if ($emailDb->smtp_id) {
									$smtpServer = $this->getClass('SmtpServer')->get((object)['where'=>['id'=>$emailDb->smtp_id]]);
									if (empty($smtpServer))
										continue;
									$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$emailDb->seller_id]]);
									if (empty($seller))
										continue;

									$smtpServer = array_pop($smtpServer);
									$seller = array_pop($seller);
									$h .= "<tr><td>".$seller->first_name.' '.$seller->last_name."</td>";
									$h .= "<td>$seller->city</td><td>$seller->state</td>";
									$h .= "<td>$smtpServer->name0</td>";
									$this->fixEmailDb($emailDb);
									if (!empty($emailDb->meta)) {
										$h .= '<td>';
										$activity = [];
										foreach($emailDb->meta as $meta) {
											$splitDate = explode(' ', $meta->date);
											switch($meta->action) {
												case SENT_FIRST_EMAIL: $what = 'SENT INTRO'; break;
												case SENT_SECOND_EMAIL: $what = 'SENT SECOND'; break;
												case SENT_THIRD_EMAIL: $what = 'SENT THIRD'; break;
												case SENT_FOURTH_EMAIL: $what = 'SENT FOURTH'; break;
												case IMPROVE_LISTING_SENT: $what = 'SENT IMPROVE_IT'; break;
												case RESPONSE_FIRST_EMAIL: //$what = 'RESPONSE INTRO'; break;
												case RESPONSE_SECOND_EMAIL: //$what = 'RESPONSE SECOND'; break;
												case RESPONSE_THIRD_EMAIL: //$what = 'RESPONSE THIRD'; break;
												case RESPONSE_FOURTH_EMAIL: //$what = 'RESPONSE FOURTH'; break;
												case RESPONSE_TO_EMAIL: $what = 'RESPONSE'; break;
												case IMPROVE_LISTING_RESPONSE: $what = 'RESPONSE IMPROVE'; break;
												case VIEWED_LISTING: $what = 'VIEWED LISTING'; break;
												case EMAIL_UNSUBSCRIBED_DATA: $what = 'UNSUBSCRIBED'; break;
												case RESERVED_PORTAL: $what = 'PORTAL RESERVATION'; break;
												case RESERVED_AGENT_MATCH: $what = 'AGENT MATCH RESERVATION'; break;
												case RESERVED_SOMETHING: $what = "RESERVATION MADE"; break;
											}
											if (!isset($activity[$splitDate[0]]))
												$activity[$splitDate[0]] = [];
											if (!in_array($what, $activity[$splitDate[0]]))
												$activity[$splitDate[0]][] = $what;

											if ($this->isChartData($meta->action) &&
												strtotime($meta->date) >= $timestamps[PROMO_FIRST]['unix_first']) {
												$whatTypes |= $meta->action;
												if ( !array_key_exists($splitDate[0], $chartActivity) )
													$chartActivity[$splitDate[0]] = [];

												if ( !array_key_exists($meta->action, $chartActivity[$splitDate[0]]) )
													$chartActivity[$splitDate[0]][$meta->action] = 0;
												$chartActivity[$splitDate[0]][$meta->action]++;
											}
											unset($meta);
										}
										$t = '<table id="activity-table"><thead><tr>';
										foreach($activity as $date=>$whatList)
											$t .= "<th>$date</th>";
										$t .= "</tr></thead><tbody><tr>";
										foreach($activity as $date=>$whatList) {
											$t .= '<td><div id="email-activity" style="display: inline;">';
											foreach($whatList as $what)
												$t .= "<span style='display: block;'>$what</span>";
											$t .= '</div></td>';
										}
										$t .= '</tr></tbody></table>';
										$h .= $t;									
										$h .= '</td>';
									}
									else
										$h .= "<td>No Activity</td>";
									$h .= "</tr>";
								}
							}
							$this->log("get-email-activity-stats - gdone with page:$page");
						}

						$h .= "</tbody></table>";

						$chartData = new \stdClass();
						$chartData->labels = [];
						$chartData->datasets = [];

						ksort($chartActivity);
						// now add missing actions to each date
						foreach($chartActivity as $date=>&$data) {
							$needWhat = 0;
							$hasWhat = 0;
							// make list of actions we have for this date
							foreach($data as $action=>$count) {
								$action = intval($action);
								$hasWhat |= $action;
							}
							$needWhat = $whatTypes ^ $hasWhat;
							// if there are any missing, add them
							if ($needWhat) for($i = 0; $i < 63; $i++) {
								if ( ($needWhat & (1 << $i)) != 0 )
									$data[$needWhat & (1 << $i)] = 0;
							}
						}

						// create datasets
						$nth = 0;
						for($i = 0; $i < 63; $i++) {
							if ( ($whatTypes & (1 << $i)) != 0 ) {
								$what = new \stdClass();
								$index = $nth % count($this->colors);
								$what->label = $this->emailDbMetaActionType($whatTypes & (1 << $i));							
								$what->fillColor = "rgba(".$this->colors[$nth].",0.5)";
					            $what->strokeColor = "rgba(".$this->colors[$nth].",0.8)";
					            $what->highlightFill = "rgba(".$this->colors[$nth].",0.75)";
					            $what->highlightStroke = "rgba(".$this->colors[$nth].",1)";
					            $what->data = [];
					            $chartData->datasets[$whatTypes & (1 << $i)] = $what;
								$nth++;
							}
						}

						// now populate our char data
						foreach($chartActivity as $date=>&$data) {
							$chartData->labels[] = $date;
							ksort($data);
							foreach($data as $action=>$count) {
								$chartData->datasets[$action]->data[] = $count;
							}
						}

						$out = new Out('OK', (object)['table'=>$h, 'chart'=>$chartData]);
						break;
					case 'get-email-stats':
						$servers = $this->getClass('SmtpServer')->get();
						$days = [];
						for($i = 0; $i < 7; $i++) {
							$days[] = date('Y-m-d', strtotime("-$i days") );
						}
						$h = '<table><thead><tr><th>Server</th><th>Total</th>';
						foreach($days as $day) {
							$today = explode('-', $day);
							$today = $today[1].'-'.$today[2];
							$h .= "<th>$today</th>";
						}
						$h .= '</tr></thead><tbody>';
						if (!empty($servers)) foreach($servers as $server) {
							$access = $this->getClass('SmtpAccessCount')->get((object)['where'=>['smtp_id'=>$server->id],
																						'not'=>['flags'=>EMAIL_STATS],
																						'orderby'=>'date',
																						'order'=>'DESC']);
							$lastDay = 0;
							$i = 0;
							$h .= '<tr><td>'.$server->name0.'</td>';
							if (!empty($access)) {
								$sum = 0;
								foreach($access as $data) {
									$sum += $data->count; unset($data);
								}
								$h .= "<td>$sum</td>";
								foreach($access as $data) {
									$index = array_search($data->date, $days);
									if ($index !== false) {
										if ($index > $i) // add empty days
											for($j = $i; $j < $index; $j++)
												$h .= '<td>0</td>';
										$i = $index+1;
										$h .= "<td>$data->count</td>";
									}
									unset($data);
								}
							}
							else
								$h .= '<td>0</td>'; // for the sum value

							for(; $i < 7; $i++)
								$h .= '<td>0</td>';
							$h .= "</tr>";
							unset($access);
						}
						unset($servers);
						$h .= "</tbody></table>";
						$out = new Out('OK', $h);
						break;

					case 'unsubscribe-agent':
						$id = $in->data['id'];
						$data = $this->getClass('SellersEmailDb')->get((object)['where'=>['seller_id'=>$id]]);
						$x = -1;
						if (!empty($data) ) {
							$fields = [];
							$fields['flags'] = $data[0]->flags | EMAIL_UNSUBSCRIBED;
							$respondMeta = new \stdClass();
							$respondMeta->action = EMAIL_UNSUBSCRIBED_DATA;
							$respondMeta->date = date('Y-m-d H:i:s');
							if (empty($data[0]->meta))
								$fields['meta'] = [$respondMeta];
							else {
								$fields['meta'] = $data[0]->meta;
								$fields['meta'][] = $respondMeta;
							}

							$x = $this->getClass('SellersEmailDb')->set([(object)['where'=>['seller_id'=>$id],
																			 	  'fields'=>$fields]]);
						}
						$out = new Out($x == -1 ? 'fail' : 'OK', $x == -1 ? "Failed to find seller email db with id:$d" : (empty($x) ? "Did not update seller email db with id:$id" : "Updated seller email db with id:$id"));
						break;

					case 'clear-flags':
						$id = $in->data['id'];
						$data = $this->getClass('SellersEmailDb')->get((object)['where'=>['seller_id'=>$id]]);
						$x = -1;
						if (!empty($data) ) {
							if ( $data[0]->flags) {
								$allFlags = SED_ALL_INACTIVE | SED_ACTIVE | SED_WAITING | SED_REJECTED | SED_TOOCHEAP | SED_NEVER | SED_AVERAGE;
								$data[0]->flags &= $allFlags; // strip out any none listing type flags
								$x = $this->getClass('SellersEmailDb')->set([(object)['where'=>['seller_id'=>$id],
																			  	  	  'fields'=>['flags'=>$data[0]->flags]]]);
							}
							else
								$x = 0;
						}
						$out = new Out($x == -1 ? 'fail' : 'OK', $x == -1 ? "Failed to find seller email db with id:$d" : ($empty($x) ? "Did not update seller email db with id:$id" : "Updated seller email db with id:$id"));
						break;

					case 'get-sellers-email-db-count':
						$count = $this->getClass('SellersEmailDb')->count();
						$out = new Out('OK', $count);
						break;

					case 'clear-all-flags':
						if (!isset($in->data['page'])) throw  new \Exception('No page value sent.');
						if (!isset($in->data['pagePer'])) throw  new \Exception('No pagePer value sent.');
						$page = intval($in->data['page']);
						$pagePer = intval($in->data['pagePer']);
						$count = 0;
						$sendFlags = PROMO_FIRST | PROMO_SECOND | PROMO_THIRD | IMPROVE_LISTING | PROMO_FOURTH | PORTAL_PROMO_1;// | NO_TABLE_INTRO;
						if ( ($dataSet = $this->getClass('SellersEmailDb')->get((object)[	'bitand'=>['flags'=>$sendFlags],
																							'limit'=>$pagePer,
													  									 	'page'=>$page])) ) {
							$allFlags = SED_ALL_INACTIVE | SED_ACTIVE | SED_WAITING | SED_REJECTED | SED_TOOCHEAP | SED_NEVER | SED_AVERAGE;
							foreach($dataSet as $data) {
								$orig = $data->flags;
								$data->flags &= $allFlags; // strip out any none listing type flags
								if ($data->flags != $orig) {
									$x = $this->getClass('SellersEmailDb')->set([(object)['where'=>['id'=>$data->id],
																				  	  	  'fields'=>['flags'=>$data->flags]]]);
									$count++;
								}
								unset($data);
							}
							unset($dataSet);
							$out = new Out('OK', $count);
						}
						else
							$out = new Out('fail', "All done");
						break;

					case 'get-preview':
						$what = intval($in->data['what']);
						$this->log("get-preview - entered for $what");
						$reg = $this->getClass('Email', 1);
						$content = file_get_contents($reg->getFile($what));
						switch($what) {
							case PROMO_FIRST:
								$content = str_replace('%first_name%', "Joe", $content);
								$content = str_replace('%city%', "Santa Cruz", $content);
								$content = str_replace('%life_styles%', 'golf, art, music, ocean front property, beach.', $content);
								$content = str_replace("%link_back%", "dummy", $content);
								break;
							case PROMO_SECOND:
							case PROMO_THIRD:
								$imgSrc = get_template_directory_uri().'/_mail/_img/email-background.png';
								$imgSrc1 = get_template_directory_uri().'/_mail/_img/step1.png';
								$imgSrc2 = get_template_directory_uri().'/_mail/_img/step2.png';
								$imgSrc3 = get_template_directory_uri().'/_mail/_img/step3.png';
								$content = str_replace('%first_name%', "Joe", $content);
								$content = str_replace("%last_name%", "Champion", $content);
								$content = str_replace("%listing_count%", "5 listings", $content);
								$content = str_replace("%connector%", "and", $content);
								$content = str_replace("%img_src%", $imgSrc, $content);
								$content = str_replace("%img_src1%", $imgSrc1, $content);
								$content = str_replace("%img_src2%", $imgSrc2, $content);
								$content = str_replace("%img_src3%", $imgSrc3, $content);
								$content = str_replace("%link_back%", "dummy", $content);
								break;
							case PORTAL_PROMO_1:
								$content = str_replace('%first_name%', "Joe", $content);
								$content = str_replace("%link_back%", "dummy", $content);
								$content = str_replace("%price%", "$5.00", $content);
								break;
						}
						$out = new Out('OK', $content);
						break;
				/* Listings */
					case 'getpage':
						$page = gettype($in->data['page']) == 'string' ? intval($in->data['page']) : $in->data['page'];
						$perPage = gettype($in->data['perPage']) == 'string' ? intval($in->data['perPage']) : $in->data['perPage'];
						$id = gettype($in->data['id']) == 'string' ? intval($in->data['id']) : $in->data['id'];
						$idSort = $in->data['sortid'];
						$priceSort = $in->data['sortprice'];

						
						$t = $this->getClass('Listings');
						$q = new \stdClass();
						
						if ($idSort != '0') {
							$q->orderby = 'id';
							$q->order = $idSort == 'true' ? 'ASC' : 'DESC';
						}
						elseif ($priceSort != '0') {
							$q->orderby = 'price';
							$q->order = $priceSort == 'true' ? 'ASC' : 'DESC';
						}
				    	$q->page = $page; // 0-based
					    $q->limit = $perPage;
					    $q->where = array('author'=>$id);
						$out = $t->get($q);
						if (!empty($out)) {
							$tags = $this->getClass("ListingsTags");
							$t = new \stdClass();
							foreach($out as $listing) {
								$t->where = array('listing_id'=>$listing->id);
								$listing->tagCount = $tags->count($t);
								unset($t->where);
							}
						}
						else {
							$tmp = str_replace("\n "," ",print_r($q, true));
							$out = "Failed to find listings using: $tmp";
							$out = new Out('fail', $out);
							break;
						}
						$out = new Out('OK', $out);
					    break;
					case 'transfer-listing':
						$id = $in->data['id'];
						$active = $in->data['active'];
						$user = $in->data['user'];
						$l = $this->getClass('Listings');
						$activity = $this->getClass("ListingsActivity");
						$q = new \stdClass(); 
						$q->where = array('listing_id'=>$id);
						$activity = $activity->get($q); 
						
						$q->where = array('id'=>$id);
						$listing = $l->get($q); // need this for the current active state to record
						if (empty($listing)) {
							$out = new Out('fail', "Count not find listing with id: $id");
							break;
						}
						$listing = $listing[0];

						// create a new meta data to record active transition
						$meta = new \stdClass(); // record the forced transition
						$meta->action = "active state change";
						$meta->date = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));	
						$meta->from = $listing->active;
						$meta->to   = $active;
						$meta->who  = $user;

						// apply this meta data to the activity for the listing
						$newArray = array();
						if (!empty($activity)) { // append to it
							$activity = array_pop($activity);
							if (!empty($activity->data))
								foreach($activity->data as $value)
									$newArray[] = $value;

							$newArray[] = $meta;
							$q->fields = array('data'=>$newArray);
							$q->where = array('listing_id'=>$id);
							$a = array();
							$a[] = $q;
							$this->getClass("ListingsActivity")->set($a);
							unset($q->fields);
						}
						else { // create new activity set
							$newArray[] = $meta;
							$activity = new \stdClass();
							$activity->listing_id = $id;
							$activity->author = $user;
							$activity->data = $newArray;
							$x = $this->getClass("ListingsActivity")->add($activity);
						}

						// now update the actual listing's active col
						$q->where = array('id'=>$id);
						$q->fields = array('active'=>$active);
						$a = array();
						$a[] = $q;
						$x = $l->set($a);
						if (!empty($x))
							$out = new Out('OK', "Updated listing with id:".$id.", to active:".$active);
						else
							$out = new OUt('fail', "Count not update listing with id:".$id.", to active:".$active);
						break;

					case 'row-count':
						$id = gettype($in->data['id']) == 'string' ? intval($in->data['id']) : $in->data['id'];
						$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$id]]);
						if (!empty($seller)) {
							$author_has_account = !empty($seller[0]->author_id);
							$l = $this->getClass('Listings');
							$q = new \stdClass();
							$q->where = array('author'=>$id,
											  'author_has_account'=>$author_has_account);
							$val = $l->count($q);
							$out = new Out('OK', $val);
						}
						else
							$out = new Out('OK', 0);
						break;

					case 'getsellercount':
						$which = intval($in->data['which']);
						$first = $in->data['first'];
						$second = $in->data['second'];
						$third = $in->data['third'];
						$active = intval($in->data['active']);
						$distance = isset($in->data['distance']) ? intval($in->data['distance']) : 0;
						$emailDbFlags = isset($in->data['emailDbFlags']) ? intval($in->data['emailDbFlags']) : 0;
						$includeSellerWithNoListing = false;
						if ($emailDbFlags & SED_NO_LISTING) {
							$includeSellerWithNoListing = true;
							$emailDbFlags &= ~SED_NO_LISTING;
						}
						// $allFlags = SED_ALL_INACTIVE | SED_ACTIVE | SED_WAITING | SED_REJECTED | SED_TOOCHEAP | SED_NEVER | SED_AVERAGE;

						$s = $this->getClass('Sellers');

						$sql = $this->createSellerSearchSQL($which, $active, $first, $second, $third, $distance, $emailDbFlags, $includeSellerWithNoListing);
						$sql = str_replace("%what%", 'COUNT(DISTINCT a.id)', $sql);

						// $sql = "SELECT COUNT(DISTINCT a.id) from {$s->getTableName()} AS a ";
						// $sql.= "INNER JOIN {$s->getTableName('sellers-email-db')} as b ON b.seller_id = a.id ";
						// switch($active) {
						// 	case 0: $sql .= "WHERE  (b.flags & ".(SED_ACTIVE | SED_WAITING).") "; break;
						// 	case 1: $sql .= "WHERE  (b.flags & ".SED_ACTIVE.") "; break;
						// 	case 2:
						// 		$sql .= "WHERE  (b.flags & ".$emailDbFlags.") ".($includeSellerWithNoListing ? " OR !(b.flags & $allFlags) " : '');
						// 		break;

						// }

						$this->log("getsellercount = mode:$active");
							
						// $where = '';
						// if ($which == OPT_ADDRESS) {
						// 	if ($distance == 0 ||
						// 		($distance && (empty($first) || empty($second))) ) {
						// 		if (!empty($first)) {
						// 			$where = "a.city = '$first' ";
						// 		}
						// 		if (!empty($second)) {
						// 			$where .= (!empty($where) ? "AND " : '')."a.state = '$second' ";
						// 		}
						// 		if (!empty($third)) {
						// 			$where .= (!empty($where) ? "AND " : '')."a.zip = '$third' ";
						// 		}
						// 	}	
						// 	else {
						// 		if (empty($second)) {
						// 			$out = new Out('fail', "The state cannot be empty!");
						// 		 	echo json_encode($out);
						// 		 	return;
						// 		}

						// 		require_once(__DIR__.'/../_classes/DistanceScale.php');
						// 		$city = $this->getClass('Cities')->get((object)['where'=>['city'=>$first,
						// 																  'state'=>$second]]);
						// 		if (empty($city)) {
						// 			$out = new Out('fail', "Cound not find $first, $state in Cities table");
						// 		 	echo json_encode($out);
						// 		 	return;
						// 		}

						// 		$orig_city = array_pop($city);
						// 		$locations = '';
						// 		$first = true;

						// 		if ($orig_city->lng == -1 ||
						// 			$orig_city->lng == 0) {
						// 			$where = "a.city = '$first' AND a.state = '$second'";
						// 			$this->log("getsellers - no geocode for $first, $second");
						// 		}
						// 		else {
						// 			$lat = (int)(abs( is_float($orig_city->lat) ? $orig_city->lat : floatval($orig_city->lat) ) / 10);
						// 			$deltaLng = $distance / $DistanceScale[$lat][0];
						// 			$deltaLat = $distance / $DistanceScale[$lat][1]; // latitude remains pretty constant at about 60miles/degree
						// 			$locations .= "(c.lng >= ".($orig_city->lng - $deltaLng)." AND c.lng <= ".($orig_city->lng + $deltaLng)." AND c.lat >= ".($orig_city->lat - $deltaLat)." AND c.lat <= ".($orig_city->lat + $deltaLat).")";

						// 		 	$loc = "SELECT DISTINCT * FROM ".$this->getClass('Cities')->getTableName($Cities->table)." AS c WHERE ".$locations;
						// 			$cities = $this->getClass('Cities')->rawQuery($loc);
						// 			$temp = array();
						// 			foreach($cities as $city)
						// 				$temp[$city->id] = $city;
						// 			$cities = $temp;
						// 			unset($temp);
						// 			if (!array_key_exists($orig_city->id, $cities) ) // add original into the list
						// 				$cities[$orig_city->id] = $orig_city;

						// 			foreach($cities as $city) {
						// 				if (empty($city))
						// 					continue;
						// 				$first ? $first = false : $where .= ' OR ';
						// 				$where .= "(a.city= '$city->city' AND a.state='$city->state')";
						// 				unset($city);
						// 			}
						// 		}
						// 		unset($orig_city);
						// 	}	
						// }
						// else if ($which == OPT_IDS) {
						// 	if (!empty($second)) {
						// 		$where = "(a.id >= $first AND a.id <= $second) ";
						// 	}
						// 	else {
						// 		$pieces = explode(',', $first);
						// 		if (count($pieces)) foreach($pieces as $piece) {
						// 			if (empty($piece))
						// 				continue;
						// 			if (strpos($piece, '-') !== false) {
						// 				$range = explode('-', $piece);
						// 				$range[0] = intval($range[0]);
						// 				$range[1] = intval($range[1]);
						// 				for($i = $range[0]; $i <= $range[1]; $i++)
						// 					$where .= (!empty($where) ? 'OR ' : '(') . "a.id = $i ";

						// 			}
						// 			else
						// 				$where .= (!empty($where) ? 'OR ' : '(') . "a.id = $piece ";
						// 		}
						// 		$where .= ") ";
						// 	}
						// }

						// // if ($active == 2)
						// // 	$sql .= 'WHERE '.(empty($where) ? "1 " : $where);
						// // else
						// $sql .= empty($where) ? '' : "AND ".$where;
						$this->log("getsellercount - sql: $sql");
						global $wpdb;
						$count = $wpdb->get_var($sql);
						$this->log("getsellercount - count: $count");
						// $s = $this->getClass('Sellers');
						// $q = new \stdClass();
						// if ($which == OPT_ADDRESS) {
						// 	if (!empty($first))
						// 		$q->where = array('city'=>$first);
						// 	if (!empty($second)) {
						// 		if (property_exists($q, 'where'))
						// 			$q->where['state'] = $second;
						// 		else
						// 			$q->where = array('state'=>$second);
						// 	}
						// 	if (!empty($third)) {
						// 		if (property_exists($q, 'where'))
						// 			$q->where['zip'] = $third;
						// 		else
						// 			$q->where = array('zip'=>$third);
						// 	}
						// }
						// else if ($which == OPT_IDS) {
						// 	if (!empty($second)) {
						// 		$q->between = ['id'=>[$first, $second]];
						// 		// $val = $s->count($q);
						// 	}
						// 	else {
						// 		$ids = [];
						// 		$pieces = explode(',', $first);
						// 		if (count($pieces)) foreach($pieces as $piece) {
						// 			if (empty($piece))
						// 				continue;
						// 			if (strpos($piece, '-') !== false) {
						// 				$range = explode('-', $piece);
						// 				$range[0] = intval($range[0]);
						// 				$range[1] = intval($range[1]);
						// 				for($i = $range[0]; $i <= $range[1]; $i++)
						// 					$ids[] = $i;

						// 			}
						// 			else
						// 				$ids[] = $piece;
						// 		}
						// 		$q->where = ['id'=>$ids];
						// 		// $val = $s->count($q);
						// 	}
						// }
						// else
						// 	unset($q);

						// $val = $s->count($q);
						$out = new Out('OK', $count);
						break;

					case 'getsellers':
						$page = gettype($in->data['page']) == 'string' ? intval($in->data['page']) : $in->data['page'];
						$perPage = gettype($in->data['perPage']) == 'string' ? intval($in->data['perPage']) : $in->data['perPage'];
						$perPage = empty($perPage) ? 1 : $perPage;
						$which = intval($in->data['which']);
						$first = $in->data['first'];
						$second = $in->data['second'];
						$third = $in->data['third'];
						$citySort = $in->data['sortcity'];
						$stateSort = $in->data['sortstate'];
						$zipSort = $in->data['sortzip'];
						$authorIdSort = $in->data['sortauthorid'];
						$idSort = isset($in->data['sortid']) ? $in->data['sortid'] : '0';
						$emailSort = isset($in->data['sortemail']) ? $in->data['sortemail'] : '0';
						$active = intval($in->data['active']);
						$distance = isset($in->data['distance']) ? intval($in->data['distance']) : 0;
						$emailDbFlags = isset($in->data['emailDbFlags']) ? intval($in->data['emailDbFlags']) : 0;
						$includeSellerWithNoListing = false;
						if ($emailDbFlags & SED_NO_LISTING) {
							$includeSellerWithNoListing = true;
							$emailDbFlags &= ~SED_NO_LISTING;
						}
						// $allFlags = SED_ALL_INACTIVE | SED_ACTIVE | SED_WAITING | SED_REJECTED | SED_TOOCHEAP | SED_NEVER | SED_AVERAGE;

						$s = $this->getClass('Sellers');
						$sql = $this->createSellerSearchSQL($which, $active, $first, $second, $third, $distance, $emailDbFlags, $includeSellerWithNoListing);
						$sql = str_replace("%what%", 'a.id, a.author_id, a.first_name, a.last_name, a.email, a.city, a.state, a.zip, a.meta, b.name, b.host, b.flags, b.meta as emailMeta', $sql);
						// if ($active == 2)
						// 	$sql = "SELECT DISTINCT a.id, a.author_id, a.first_name, a.last_name, a.email, a.city, a.state, a.zip, a.meta from {$s->getTableName()} AS a ";
						// else {
						// $sql = "SELECT DISTINCT a.id, a.author_id, a.first_name, a.last_name, a.email, a.city, a.state, a.zip, a.meta, b.name, b.host, b.flags, b.meta as emailMeta from {$s->getTableName()} AS a ";
						// $sql.= "INNER JOIN {$s->getTableName('sellers-email-db')} as b ON b.seller_id = a.id ";
						// switch($active) {
						// 	case 0: $sql .= "WHERE  (b.flags & ".(SED_ACTIVE | SED_WAITING).") "; break;
						// 	case 1: $sql .= "WHERE  (b.flags & ".SED_ACTIVE.") "; break;
						// 	case 2:
						// 		$sql .= "WHERE  (b.flags & ".$emailDbFlags.") ".($includeSellerWithNoListing ? " OR !(b.flags & $allFlags) " : '');
						// 		break;
						// }
						// 	// if ($active == 1) {
						// 	// 	// $sql.= "INNER JOIN (SELECT author_has_account, author FROM {$s->getTableName('listings')} WHERE `active` = 1 ) AS c ";
						// 	// 	// $sql.= "WHERE ((c.author_has_account = 0 AND a.id = c.author) OR (c.author_has_account = 1 AND a.author_id = c.author)) "; //" OR c.active = 2)";
						// 	// 	$sql .= "WHERE  (b.flags & ".SED_ACTIVE.")";
						// 	// }
						// 	// else
						// 	// 	$sql .= "WHERE  (b.flags & ".(SED_ACTIVE | SED_WAITING).")";
						// // }
						// $where = '';
						// if ($which == OPT_ADDRESS) {
						// 	if ($distance == 0 ||
						// 		($distance && (empty($first) || empty($second))) ) {
						// 		if (!empty($first)) {
						// 			$where = "a.city = '$first' ";
						// 		}
						// 		if (!empty($second)) {
						// 			$where .= (!empty($where) ? "AND " : '')."a.state = '$second' ";
						// 		}
						// 		if (!empty($third)) {
						// 			$where .= (!empty($where) ? "AND " : '')."a.zip = '$third' ";
						// 		}	
						// 	}	
						// 	else {
						// 		require_once(__DIR__.'/../_classes/DistanceScale.php');
						// 		$city = $this->getClass('Cities')->get((object)['where'=>['city'=>$first,
						// 																  'state'=>$second]]);
						// 		if (empty($city)) {
						// 			$out = new Out('fail', "Cound not find $first, $state in Cities table");
						// 		 	echo json_encode($out);
						// 		 	return;
						// 		}

						// 		$orig_city = array_pop($city);
						// 		$locations = '';
						// 		$first = true;

						// 		if ($orig_city->lng == -1 ||
						// 			$orig_city->lng == 0) {
						// 			$where = "a.city = '$first' AND a.state = '$second'";
						// 			$this->log("getsellers - no geocode for $first, $second");
						// 		}
						// 		else {
						// 			$lat = (int)(abs( is_float($orig_city->lat) ? $orig_city->lat : floatval($orig_city->lat) ) / 10);
						// 			$deltaLng = $distance / $DistanceScale[$lat][0];
						// 			$deltaLat = $distance / $DistanceScale[$lat][1]; // latitude remains pretty constant at about 60miles/degree
						// 			$locations .= "(c.lng >= ".($orig_city->lng - $deltaLng)." AND c.lng <= ".($orig_city->lng + $deltaLng)." AND c.lat >= ".($orig_city->lat - $deltaLat)." AND c.lat <= ".($orig_city->lat + $deltaLat).")";

						// 			$loc = "SELECT DISTINCT * FROM ".$this->getClass('Cities')->getTableName($Cities->table)." AS c WHERE ".$locations;
						// 			$cities = $this->getClass('Cities')->rawQuery($loc);
						// 			$temp = array();
						// 			foreach($cities as $city)
						// 				$temp[$city->id] = $city;
						// 			$cities = $temp;
						// 			unset($temp);
						// 			if (!array_key_exists($orig_city->id, $cities) ) // add original into the list
						// 				$cities[$orig_city->id] = $orig_city;

						// 			foreach($cities as $city) {
						// 				if (empty($city))
						// 					continue;
						// 				$first ? $first = false : $where .= ' OR ';
						// 				$where .= "(a.city='$city->city' AND a.state='$city->state')";
						// 				unset($city);
						// 			}
						// 		}
						// 		unset($orig_city);
						// 	}				
						// }
						// else if ($which == OPT_IDS) {
						// 	if (!empty($second)) {
						// 		$where = "(a.id >= $first AND a.id <= $second) ";
						// 	}
						// 	else {
						// 		$pieces = explode(',', $first);
						// 		if (count($pieces)) foreach($pieces as $piece) {
						// 			if (empty($piece))
						// 				continue;
						// 			if (strpos($piece, '-') !== false) {
						// 				$range = explode('-', $piece);
						// 				$range[0] = intval($range[0]);
						// 				$range[1] = intval($range[1]);
						// 				for($i = $range[0]; $i <= $range[1]; $i++)
						// 					$where .= (!empty($where) ? 'OR ' : '(') . "a.id = $i ";

						// 			}
						// 			else
						// 				$where .= (!empty($where) ? 'OR ' : '(') . "a.id = $piece ";
						// 		}
						// 		$where .= ") ";
						// 	}
						// }

						// // if ($active == 2)
						// // 	$sql .= 'WHERE '.(empty($where) ? "1 " : $where);
						// // else
						// $sql .= empty($where) ? '' : "AND ".$where;

						if ($citySort != '0') {
							$sql .= "ORDER BY a.city ".($citySort == 'true' ? 'ASC ' : 'DESC ');
						}
						elseif ($stateSort != '0') {
							$sql .= "ORDER BY a.state ".($stateSort == 'true' ? 'ASC ' : 'DESC ');
						}
						elseif ($zipSort != '0') {
							$sql .= "ORDER BY a.zip ".($zipSort == 'true' ? 'ASC ' : 'DESC ');
						}
						elseif ($authorIdSort != '0') {
							$sql .= "ORDER BY a.author_id ".($authorIdSort == 'true' ? 'ASC ' : 'DESC ');
						}
						elseif ($emailSort != '0') {
							$sql .= "ORDER BY b.host ".($emailSort == 'true' ? 'ASC ' : 'DESC ');
						}
						elseif ($idSort != '0') {
							$sql .= "ORDER BY a.id ".($idSort == 'true' ? 'ASC ' : 'DESC ');
						}
						else {
							$sql .= "ORDER BY a.id ASC ";
						}

				    	$pageStart = $page*$perPage; // 0-based
					    $sql .= " LIMIT $pageStart,$perPage ";

					    $this->log("getsellers - sql: $sql");
					    $x = $s->rawQuery($sql);

					    if (!empty($x)) foreach($x as &$seller) {
					    	if (!empty($seller->meta))
					    		$seller->meta = json_decode($seller->meta);
					    }
					   //  	if (!empty($seller->email)) {
					   //  		$email = explode('@', $seller->email);
								// if (count($email) == 2) {
								// 	$seller->name = $email[0];
								// 	$seller->host = strtolower( $email[1] );
								// }
					   //  	}
					   //  	if (!isset($seller->host)) {
					   //  		$seller->name = $seller->first_name;
					   //  		$seller->host = "xxxUnknownxxx";
					   //  	}
					   //  }
						
						$out = new Out('OK', $x);
						break;

					case 'getsingleseller':
						$first = $in->data['first'];
						$second = $in->data['second'];
						$third = $in->data['third'];
						$s = $this->getClass('Sellers', 1);
						$q = new \stdClass();
						if (!empty($first))
							$q->like = ['first_name'=>$first];
						if (!empty($second))
							$q->like2 = ['last_name'=>$second];
						if (!empty($third))
							$q->like3 = ['email'=>$third];
					
						$x = $s->get($q);
						if (empty($x))
							$out = new Out('OK');
						else {
							$seller = array_pop($x);
							unset($q);
							$q = new \stdClass();
							$l = $this->getClass('Listings');
							$author = empty($seller->author_id) ? $seller->id : $seller->author_id;
							$q->where = array('author'=>$author);
							$seller->listingCount = $l->count($q);
							$q->what = array('active');
							$list = $l->get($q);
							$seller->listingTypes = array();
							if (!empty($list)) {
								foreach($list as $listing) {
									if (empty($seller->listingTypes[$listing->active]))
										$seller->listingTypes[$listing->active] = 1;
									else
										$seller->listingTypes[$listing->active]++;
								}
							}
							$reservation = $this->getClass('Reservations')->get((object)['where'=>['seller_id'=>$seller->id]]);
							if (!empty($reservation))
								$seller->reservation = $reservation[0];
							$out = new Out('OK', $seller);
						}
						break;

					case 'getsellerlistingstat':
						$id = $in->data['id'];
						$author = $in->data['author']; 	// this value is already considered whether agent is registered or not
														// so can be either the 'id' or 'author_id' from Seller row
						$q = new \stdClass();
						$seller = new \stdClass();
						$l = $this->getClass('Listings');
						$q->where = array('author'=>$author);
						$q->what = array('active');
						$list = $l->get($q);
						$seller->listingCount = 0;
						$seller->listingTypes = array();
						if (!empty($list)) {
							$seller->listingCount = count($list);
							foreach($list as $listing) {
								if (empty($seller->listingTypes[$listing->active]))
									$seller->listingTypes[$listing->active] = 1;
								else
									$seller->listingTypes[$listing->active]++;
							}
						}
						$reservation = $this->getClass('Reservations')->get((object)['where'=>['seller_id'=>$id]]);
						if (!empty($reservation))
							$seller->reservation = $reservation[0];
						$out = new Out('OK', $seller);
						break;

					case 'addsellercode':
						$id = $in->data['id'];
						$code = $in->data['code'];
						$s = $this->getClass('Sellers');
						$q = new \stdClass();
						$q->where = array('id'=>$id);
						$x = $s->get($q);
						if (empty($x))
							$out = new Out('fail',"Failed to find seller with id:$id");
						else {
							$metas = array();
							$x = array_pop($x);
							// make sure this isn't redundant
							$haveCodeAlready = false;
							if (!empty($x->meta))
								foreach($x->meta as $meta) {
									if ($meta->action == SELLER_INVITE_CODE &&
										$meta->key == $code) {
										$haveCodeAlready = true;
									}
									$metas[] = $meta;

								}
							if (!$haveCodeAlready) {
								$newMeta = new \stdClass();
								$newMeta->action = SELLER_INVITE_CODE;
								$newMeta->key = $code;
								$newMeta->owner = true;
								$metas[] = clone $newMeta;
								$q->fields = array('meta'=>$metas);
								$a = array();
								$a[] = $q;
								$x = $s->set($a);
								if (empty($x))
									$out = new Out('fail',"Failed to set meta data for seller with id:$id");
								else
									$out = new Out('OK', "Updated meta data for seller with id:$id");
							}
							else
								$out = new Out('OK', "This invite code: $code already exists for seller with id:$id");
						}
						break;

					case 'updateinvitecodes':
						$id = $in->data['id'];
						$code = $in->data['code'];
						$o = $this->getClass('Options');
						$q = new \stdClass();
						$q->where = array('opt'=>INVITATION_LIST);
						$invites = $o->get($q);
						if (empty($invites))
							$out = new Out('fail', "Sorry, there are no invitations to update.");
						else {
							$invites = json_decode($invites[0]->value);
							$invite = $this->inviteInList($code, $invites);
							if (!$invite)
								$out = new Out('fail', "Sorry, there is no invitation with this code:$code.");
							else {
								$invite->seller_id = $id;
								$invites = json_encode($invites);
								$q->fields = array('value'=>$invites);
								$a = array();
								$a[] = $q;
								$x = $o->set($a);
								if (empty($x))
									$out = new Out('fail',"Failed to set invitation codes to Options for $code");
								else
									$out = new Out('OK', "Updated invitation codes for $code with owner:$id");
							}
						}
						break;

					case 'create-email-stats': 
						$id = $in->data['id'];
						$what = $in->data['what'];
						$which = intval($in->data['which']);
						$target = isset($in->data['target']) ? $in->data['target'] : [];
						$e = $this->getClass('Email', 1);
						$stats = [];
						$masterStats = [];

						$this->log("create-email-stats for $what ");

						// process mass mailing!!!
						if ( ($what == 'email-stats' ||
							 ($what == 'email-stats-range' &&
							  $which == OPT_ADDRESS)) &&
							empty($target)) {
							
							$first = $in->data['first'];
							$second = $in->data['second'];
							$third = $in->data['third'];
							$active = intval($in->data['active']);
							$distance = isset($in->data['distance']) ? intval($in->data['distance']) : 0;
							$emailDbFlags = isset($in->data['emailDbFlags']) ? intval($in->data['emailDbFlags']) : 0;
							$includeSellerWithNoListing = false;
							if ($emailDbFlags & SED_NO_LISTING) {
								$includeSellerWithNoListing = true;
								$emailDbFlags &= ~SED_NO_LISTING;
							}
							$allFlags = SED_ALL_INACTIVE | SED_ACTIVE | SED_WAITING | SED_REJECTED | SED_TOOCHEAP | SED_NEVER | SED_AVERAGE;

							if ($what == 'email-stats-range' &&
							  	$which == OPT_ADDRESS) 
							  	$distance = 0; // start at zero and work up...

							$s = $this->getClass('Sellers');
							// if ($active == 2)
							// 	$sql = "SELECT DISTINCT a.id from {$s->getTableName()} AS a ";
							// else {
							$sql = "SELECT DISTINCT a.id from {$s->getTableName()} AS a ";
							$sql.= "INNER JOIN {$s->getTableName('sellers-email-db')} as b ON b.seller_id = a.id ";
							switch($active) {
								case 0: $sql .= "WHERE  (b.flags & ".(SED_ACTIVE | SED_WAITING).") "; break;
								case 1: $sql .= "WHERE  (b.flags & ".SED_ACTIVE.") "; break;
								case 2:
									$sql .= "WHERE  ((b.flags & ".$emailDbFlags.") ".($includeSellerWithNoListing ? " OR !(b.flags & $allFlags)) " : '');
									break;
							}
								// if ($active == 1) {
								// 	// $sql.= "INNER JOIN (SELECT author_has_account, author FROM {$s->getTableName('listings')} WHERE `active` = 1 ) AS c ";
								// 	// $sql.= "WHERE ((c.author_has_account = 0 AND a.id = c.author) OR (c.author_has_account = 1 AND a.author_id = c.author)) "; //" OR c.active = 2)";
								// 	$sql .= "WHERE  (b.flags & ".SED_ACTIVE.")";
								// }
								// else
								// $sql .= "WHERE  (b.flags & ".(SED_ACTIVE | SED_WAITING).")";
							// }

							$this->log("create-email-stats for $what ");
							$iterateCount = 0;
							while(true) {
								$where = '';
								if ($which == OPT_ADDRESS) {
									if ($distance == 0 ||
										($distance && (empty($first) || empty($second))) ) {
										if (!empty($first)) {
											$where = '`city` = "'.$first.'" ';
										}
										if (!empty($second)) {
											$where .= (!empty($where) ? "AND " : '')."a.state = '$second' ";
										}
										if (!empty($third)) {
											$where .= (!empty($where) ? "AND " : '')."a.zip = '$third' ";
										}	
									}	
									else {
										require_once(__DIR__.'/../_classes/DistanceScale.php');
										$city = $this->getClass('Cities')->get((object)['where'=>['city'=>$first,
																								  'state'=>$second]]);
										if (empty($city)) {
											$out = new Out('fail', "Cound not find $first, $state in Cities table");
										 	echo json_encode($out);
										 	return;
										}

										$orig_city = array_pop($city);
										$locations = '';

										if ($orig_city->lng == -1 ||
											$orig_city->lng == 0) {
											$where = "a.city = '$first' AND a.state = '$second'";
											$this->log("getsellers - no geocode for $first, $second");
											if ($what == 'email-stats-range') {
												$out = new Out('OK', "<span>Cannot do range stats on a city without geocoding.</span>");
												break; // out of while()
											}
										}
										else {
											$lat = (int)(abs( is_float($orig_city->lat) ? $orig_city->lat : floatval($orig_city->lat) ) / 10);
											$deltaLng = $distance / $DistanceScale[$lat][0];
											$deltaLat = $distance / $DistanceScale[$lat][1]; // latitude remains pretty constant at about 60miles/degree
											$locations .= "(c.lng >= ".($orig_city->lng - $deltaLng)." AND c.lng <= ".($orig_city->lng + $deltaLng)." AND c.lat >= ".($orig_city->lat - $deltaLat)." AND c.lat <= ".($orig_city->lat + $deltaLat).")";
											if ($what == 'email-stats-range' &&
												$distance > 5) {
												$deltaLng = ($distance-5.01) / $DistanceScale[$lat][0];
												$deltaLat = ($distance-5.01) / $DistanceScale[$lat][1]; 
												$locations .= " AND !(c.lng >= ".($orig_city->lng - $deltaLng)." AND c.lng <= ".($orig_city->lng + $deltaLng)." AND c.lat >= ".($orig_city->lat - $deltaLat)." AND c.lat <= ".($orig_city->lat + $deltaLat).")";
											}

											$loc = "SELECT DISTINCT * FROM ".$this->getClass('Cities')->getTableName($Cities->table)." AS c WHERE ".$locations;
											// $this->log("create-email-stats - loc sql: ".$loc);
											$cities = $this->getClass('Cities')->rawQuery($loc);
											$temp = array();
											foreach($cities as $city)
												$temp[$city->id] = $city;
											$cities = $temp;
											unset($temp);
											// if (!array_key_exists($orig_city->id, $cities) ) // add original into the list
											// 	$cities[$orig_city->id] = $orig_city;

											$cityList = '';
											foreach($cities as $city) {
												if (empty($city))
													continue;
												if ($what == 'email-stats-range' &&
													// $distance == 5 &&
													$city->city == $orig_city->city)
													continue;
												$where .= (strlen($where) ? ' OR ' : '(').'(a.city="'.$city->city.'" '."AND a.state='$city->state')";
												$cityList .= "$city->city, ";
												unset($city);
											}
											$where .= strlen($where) ? ")" : '';
											$this->log("create-email-stats - where: $cityList");
										}
										unset($orig_city);
									}					
								}
								else if ($which == OPT_IDS) {
									if (!empty($second)) {
										$where = "(a.id >= $first AND a.id <= $second) ";
									}
									else {
										$pieces = explode(',', $first);
										if (count($pieces)) foreach($pieces as $piece) {
											if (empty($piece))
												continue;
											if (strpos($piece, '-') !== false) {
												$range = explode('-', $piece);
												$range[0] = intval($range[0]);
												$range[1] = intval($range[1]);
												for($i = $range[0]; $i <= $range[1]; $i++)
													$where .= (!empty($where) ? 'OR ' : '(') . "a.id = $i ";

											}
											else
												$where .= (!empty($where) ? 'OR ' : '(') . "a.id = $piece ";
										}
										$where .= ") ";
									}
								}

								$workSql = $sql;
								// if ($active == 2)
								// 	$workSql .= 'WHERE '.(empty($where) ? "1 " : $where);
								// else
								$workSql .= empty($where) ? '' : "AND ".$where;

								$page = 0; $pageSize = 1000;
								$doSql = $workSql." LIMIT $page, $pageSize";
								// $this->log("create-email-stats - sql: ".$doSql);

								$this->log("create-email-stats - distance:$distance, $doSql");
								while( !empty($where) && // then no city was included
									   !empty($target = $s->rawQuery($doSql)) ) {
									// $out = $e->sendEmail(intval($id), $what, $target);
									$this->log("create-email-stats calling buildStats with ".count($target).", for distance:$distance");
									$e->buildStats(intval($id), $what, $target, $stats);
									unset($target);
									$page++;
									$newPage = $page*$pageSize;
									$doSql = $workSql." LIMIT $newPage, $pageSize";
									unset($target);
								}
								if ($what != 'email-stats-range') {
									if (!empty($stats)) {
										$days = [];
										$today = date('Y-m-d');
									
										$h = '<table><thead><tr><th>Server</th><th>Total</th>';
										$h .= '</tr></thead><tbody>';
										foreach($stats as $access) {
											$smtpServer = $this->getClass('SmtpServer')->get((object)['where'=>['id'=>$access->smtp_id]]);
											if (empty($smtpServer)) {
												$this->log("create-email-stats - failed to find smtpServer with id: $access->smtp_id");
												continue;
											}
											$smtpServer = array_pop($smtpServer);
											$h .= "<tr><td>$smtpServer->name0</td><td>$access->count</td></tr>";
										}
										$h .= "</tbody></table>";
										$out = new Out('OK', (object)['html'=>$h]);
									}
									else
										$out = new Out('OK', "<span>No stat results!</span>");
									break; // out of while()
								}
								else {
									// gather data
									if (!empty($stats)) {
										foreach($stats as $data) {
											if (!array_key_exists($data->smtp_id, $masterStats))
												$masterStats[$data->smtp_id] = [];
											$masterStats[$data->smtp_id][$distance] = $data;
											$this->log("create-email-stats - server:$data->smtp_id, distance:$distance, count:$data->count");
											unset($data);
										}
									}
									$iterateCount++;
									$distance = $iterateCount*5;
									$this->log("create-email-stats - distance: $distance");
									// make master table
									if ($distance > 100) {
										$csv = 'Server';
										$h = '<table><thead><tr><th>Server</th>';
										for($i = 0; $i <= 100; $i += 5) {
											$h .= "<th>$i</th>";
											$csv .= ", $i";
										}
										$csv .= "\n\r";

										$h .= '</tr></thead><tbody>';
										foreach($masterStats as $smtp_id=>$stat) {
											$smtpServer = $this->getClass('SmtpServer')->get((object)['where'=>['id'=>$smtp_id]]);
											if (empty($smtpServer)) {
												$this->log("create-email-stats - failed to find smtpServer with id: $smtp_id");
												continue;
											}
											$smtpServer = array_pop($smtpServer);
											$h .= "<tr><td>$smtpServer->name0</td>";
											$csv .= "$smtpServer->name0";
											$total = 0;
											for($i = 0; $i <= 20; $i++) {
												if (isset($stat[$i*5])) {
													$csv .= ", ".$stat[$i*5]->count+$total;
													$h .= "<td>".($stat[$i*5]->count+$total)."</td>";
													$this->log("create-email-stats - {$smtpServer->name0} at ".($i*5).", count:".$stat[$i*5]->count.", total:$total");
													$total += $stat[$i*5]->count;
												}
												else {
													$h .= "<td>0</td>";
													$csv .= ", 0";
												}
											}
											$csv .= "\n\r";
											$h .= "</tr>";
											unset($smtpServer, $stat);
										}
										$h .= "</tbody></table>";
										$out = new Out('OK', (object)['html'=>$h, 'csv'=>$csv]);
										break;
									}
								} // end while()
								unset($stats);
							}
						}
						else // must be email-test
							$out = new Out('fail', "create-email-stats not supporting $what");
						break;

					case 'sendemail':
						$id = $in->data['id'];
						$what = $in->data['what'];
						$target = isset($in->data['target']) ? $in->data['target'] : [];
						$e = $this->getClass('Email', 1);
						$csv = '';

						$noTrip = isset($in->data['noTrip']) ? intval($in->data['noTrip']) : 0;
						$mailchimp = isset($in->data['mailchimp']) ? intval($in->data['mailchimp']) : 0;
						
						// process mass mailing!!!
						if ($what == 'email-sellers' &&
							empty($target)) {
							$which = intval($in->data['which']);
							$first = $in->data['first'];
							$second = $in->data['second'];
							$third = $in->data['third'];
							$active = intval($in->data['active']);
							$distance = isset($in->data['distance']) ? intval($in->data['distance']) : 0;
							$emailDbFlags = isset($in->data['emailDbFlags']) ? intval($in->data['emailDbFlags']) : 0;
							$includeSellerWithNoListing = false;
							if ($emailDbFlags & SED_NO_LISTING) {
								$includeSellerWithNoListing = true;
								$emailDbFlags &= ~SED_NO_LISTING;
							}
							// $allFlags = SED_ALL_INACTIVE | SED_ACTIVE | SED_WAITING | SED_REJECTED | SED_TOOCHEAP | SED_NEVER | SED_AVERAGE;

							$s = $this->getClass('Sellers');
							$sql = $this->createSellerSearchSQL($which, $active, $first, $second, $third, $distance, $emailDbFlags, $includeSellerWithNoListing);
							$sql = str_replace("%what%", 'DISTINCT a.id', $sql);
							// if ($active == 2)
							// 	$sql = "SELECT DISTINCT a.id from {$s->getTableName()} AS a ";
							// else {
							// $sql = "SELECT DISTINCT a.id from {$s->getTableName()} AS a ";
							// $sql.= "INNER JOIN {$s->getTableName('sellers-email-db')} as b ON b.seller_id = a.id ";
							// switch($active) {
							// 	case 0: $sql .= "WHERE  (b.flags & ".(SED_ACTIVE | SED_WAITING).") "; break;
							// 	case 1: $sql .= "WHERE  (b.flags & ".SED_ACTIVE.") "; break;
							// 	case 2: 
							// 		$sql .= "WHERE  (b.flags & ".$emailDbFlags.") ".($includeSellerWithNoListing ? " OR !(b.flags & $allFlags) " : '');
							// 		break;
							// }
							// 	// if ($active == 1) {
							// 	// 	// $sql.= "INNER JOIN (SELECT author_has_account, author FROM {$s->getTableName('listings')} WHERE `active` = 1 ) AS c ";
							// 	// 	// $sql.= "WHERE ((c.author_has_account = 0 AND a.id = c.author) OR (c.author_has_account = 1 AND a.author_id = c.author)) "; //" OR c.active = 2)";
							// 	// 	$sql .= "WHERE  (b.flags & ".SED_ACTIVE.")";
							// 	// }
							// 	// else
							// 	// 	$sql .= "WHERE  (b.flags & ".(SED_ACTIVE | SED_WAITING).")";
							// // }
							// $where = '';
							// if ($which == OPT_ADDRESS) {
							// 	if ($distance == 0 ||
							// 		($distance && (empty($first) || empty($second))) ) {
							// 		if (!empty($first)) {
							// 			$where = "`city` = '$first' ";
							// 		}
							// 		if (!empty($second)) {
							// 			$where .= (!empty($where) ? "AND " : '')."a.state = '$second' ";
							// 		}
							// 		if (!empty($third)) {
							// 			$where .= (!empty($where) ? "AND " : '')."a.zip = '$third' ";
							// 		}	
							// 	}	
							// 	else {
							// 		require_once(__DIR__.'/../_classes/DistanceScale.php');
							// 		$city = $this->getClass('Cities')->get((object)['where'=>['city'=>$first,
							// 																  'state'=>$second]]);
							// 		if (empty($city)) {
							// 			$out = new Out('fail', "Cound not find $first, $state in Cities table");
							// 		 	echo json_encode($out);
							// 		 	return;
							// 		}

							// 		$orig_city = array_pop($city);
							// 		$locations = '';
							// 		$first = true;

							// 		if ($orig_city->lng == -1 ||
							// 			$orig_city->lng == 0) {
							// 			$where = "a.city = '$first' AND a.state = '$second'";
							// 			$this->log("getsellers - no geocode for $first, $second");
							// 		}
							// 		else {
							// 			$lat = (int)(abs( is_float($orig_city->lat) ? $orig_city->lat : floatval($orig_city->lat) ) / 10);
							// 			$deltaLng = $distance / $DistanceScale[$lat][0];
							// 			$deltaLat = $distance / $DistanceScale[$lat][1]; // latitude remains pretty constant at about 60miles/degree
							// 			$locations .= "(c.lng >= ".($orig_city->lng - $deltaLng)." AND c.lng <= ".($orig_city->lng + $deltaLng)." AND c.lat >= ".($orig_city->lat - $deltaLat)." AND c.lat <= ".($orig_city->lat + $deltaLat).")";

							// 			$loc = "SELECT DISTINCT * FROM ".$this->getClass('Cities')->getTableName($Cities->table)." AS c WHERE ".$locations;
							// 			$cities = $this->getClass('Cities')->rawQuery($loc);
							// 			$temp = array();
							// 			foreach($cities as $city)
							// 				$temp[$city->id] = $city;
							// 			$cities = $temp;
							// 			unset($temp);
							// 			if (!array_key_exists($orig_city->id, $cities) ) // add original into the list
							// 				$cities[$orig_city->id] = $orig_city;

							// 			foreach($cities as $city) {
							// 				if (empty($city))
							// 					continue;
							// 				$first ? $first = false : $where .= ' OR ';
							// 				$where .= "(a.city='$city->city' AND a.state='$city->state')";
							// 				unset($city);
							// 			}
							// 		}
							// 		unset($orig_city);
							// 	}					
							// }
							// else if ($which == OPT_IDS) {
							// 	if (!empty($second)) {
							// 		$where = "(a.id >= $first AND a.id <= $second) ";
							// 	}
							// 	else {
							// 		$pieces = explode(',', $first);
							// 		if (count($pieces)) foreach($pieces as $piece) {
							// 			if (empty($piece))
							// 				continue;
							// 			if (strpos($piece, '-') !== false) {
							// 				$range = explode('-', $piece);
							// 				$range[0] = intval($range[0]);
							// 				$range[1] = intval($range[1]);
							// 				for($i = $range[0]; $i <= $range[1]; $i++)
							// 					$where .= (!empty($where) ? 'OR ' : '(') . "a.id = $i ";

							// 			}
							// 			else
							// 				$where .= (!empty($where) ? 'OR ' : '(') . "a.id = $piece ";
							// 		}
							// 		$where .= ") ";
							// 	}
							// }

							// // if ($active == 2)
							// // 	$sql .= 'WHERE '.(empty($where) ? "1 " : $where);
							// // else
							// $sql .= empty($where) ? '' : "AND ".$where;

							$page = 0; $pageSize = 1000;
							$doSql = $sql." LIMIT $page, $pageSize";
							$this->log("sendemail - $doSql");
							$total = 0;
							while( !empty($target = $s->rawQuery($doSql)) ) {
								$this->log("sendemail - got ".count($target)." sellers");
								$out = $e->sendEmail(intval($id), $what, $target, $csv, $mailchimp, $noTrip);
								$total += count($target);
								unset($target);
								$page++;
								$newPage = $page*$pageSize;
								$doSql = $sql." LIMIT $newPage, $pageSize";
								$this->log("sendemail - page: $page - status: {$out->data->status}");
							}
							// if ($mailchimp)
							$out = new Out('OK', (object)['csv'=>$csv, 'status'=>"Completed $total users."]);
						}
						else {// mailchimp should always be zero
							if ($mailchimp)
								$out = new Out('OK', (object)['csv'=>$csv, 'status'=>'mailchimp cannot be true when doing a test email']);
							else
								$out = $e->sendEmail(intval($id), $what, $target, $csv, $mailchimp, $noTrip);
						}
						break;

					case 'sendemailperpage':
						$id = $in->data['id'];
						$what = $in->data['what'];
						$page = $in->data['page'];
						$pageSize = $in->data['pagePer'];
						$target = isset($in->data['target']) ? $in->data['target'] : [];
						$e = $this->getClass('Email', 1);
						$csv = '';

						$this->log("sendemailperpage - entered for $what, page:$page, target:".(count($target) ? count($target) : "empty"));

						// process mass mailing!!!
						if ($what == 'email-sellers' &&
							empty($target)) {
							$which = intval($in->data['which']);
							$first = $in->data['first'];
							$second = $in->data['second'];
							$third = $in->data['third'];
							$active = intval($in->data['active']);
							$distance = isset($in->data['distance']) ? intval($in->data['distance']) : 0;
							$mailchimp = isset($in->data['mailchimp']) ? intval($in->data['mailchimp']) : 0;
							$emailDbFlags = isset($in->data['emailDbFlags']) ? intval($in->data['emailDbFlags']) : 0;
							$noTrip = isset($in->data['noTrip']) ? intval($in->data['noTrip']) : 0;
							$includeSellerWithNoListing = false;
							if ($emailDbFlags & SED_NO_LISTING) {
								$includeSellerWithNoListing = true;
								$emailDbFlags &= ~SED_NO_LISTING;
							}
							// $allFlags = SED_ALL_INACTIVE | SED_ACTIVE | SED_WAITING | SED_REJECTED | SED_TOOCHEAP | SED_NEVER | SED_AVERAGE;

							$s = $this->getClass('Sellers');
							$sql = $this->createSellerSearchSQL($which, $active, $first, $second, $third, $distance, $emailDbFlags, $includeSellerWithNoListing);
							$sql = str_replace("%what%", 'DISTINCT a.id', $sql);


							// $page = 0; $pageSize = 1000;
							$page = $page * $pageSize;
							$doSql = $sql." LIMIT $page, $pageSize";
							$this->log("sendemailperpage - $doSql");
							$total = 0;
							if ( !empty($target = $s->rawQuery($doSql)) ) {
								$this->log("sendemailperpage - got ".count($target)." sellers for page:$page");
								$out = $e->sendEmail(intval($id), $what, $target, $csv, $mailchimp, $noTrip);
								$total += count($target);
								unset($target);
								// $page++;
								// $newPage = $page*$pageSize;
								// $doSql = $sql." LIMIT $newPage, $pageSize";
								$this->log("sendemailperpage - page: $page - status: {$out->data->status}");
							}
							// if ($mailchimp)
							$out = new Out($total ? 'OK' : 'fail', (object)['csv'=>$csv, 'status'=>($total ? "Completed $total users." : "Complete all users")]);
						}
						else {// mailchimp should always be zero
							if ($mailchimp)
								$out = new Out('OK', (object)['csv'=>$csv, 'status'=>'mailchimp cannot be true when doing a test email']);
							else
								$out = $e->sendEmail(intval($id), $what, $target, $csv, $mailchimp);
						}
						break;

					case 'applykeyword':
						$keywords = $in->data['key'];
						$active = intval($in->data['active']);
						$l = $this->getClass('ListHubData');
						$out = $l->applyKeywords($keywords, $active);
						break;

					case 'cleanTags':
						$active = intval($in->data['active']);
						$l = $this->getClass('ListHubData');
						$out = $l->cleanTags($active);
						break;

					case 'delete-listing':
						if (empty($in->data) || (empty($in->data) && $in->data !== 0)) throw new \Exception('No listing id sent.');
						if ( !$this->getClass('ListingsRejected')->delete(['id'=>$in->data['id']]) ) throw new \Exception('Unable to delete listing.');
						$out = new Out('OK');
						break;
					case 'get-listings':
						if ( !$out = $this->getClass('ListingsRejected')->get() ) throw new \Exception('No listings found.');
						$out = new Out('OK', $out);
						break;
					case 'get-listings-with-geoinfo':
						if ( !$listings = $this->getClass('ListingsRejected')->get( (object)['what'=>[ 'id' ]] ) ) throw new \Exception('No listings found.');
						if ( !$geoinfo = $this->getClass('ListingsGeoinfo')->get() ) throw new \Exception('No geoinfo found for listings.');
						$x = [];
						foreach ($listings as &$listing) foreach ($geoinfo as $geo) if ($geo->listing_id == $listing->id && isset($geo->lat)) {
							$x[] = $listing->id;
							break;
						}
						$out = new Out('OK', $x);
						break;
					case 'get-listings-and-points':
						if ( !$listings = $this->getClass('ListingsRejected')->get( (object)['what'=>[ 'id','title','street_address' ]] ) ) throw new \Exception('No listings found.');
						if ( !$geoinfo = $this->getClass('ListingsGeoinfo')->get() ) throw new \Exception('No geoinfo found for listings.');
						if ( !$points = $this->getClass('ListingsPoints')->get() ) throw new \Exception('No points found for listings.');
						$x = [];
						foreach ($listings as $listing){
							$id = $listing->id;
							// setup geoinfo
							foreach ($geoinfo as $geo) if ($geo->listing_id == $id) {
								foreach ($geo as $field=>$value)
									if ($field == 'listing_id' || $field == 'id') continue;
									else $listing->$field = $value;
								break;
							}
							$listing->points = [];
							// setup points
							foreach ($points as $point) if ($point->listing_id == $id)
								$listing->points[$point->point_id] = $point->distance;
							$x[$id] = $listing;
							unset($id);
						}
						$out = new Out('OK', $x);
						break;
					case 'update-listing':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No listing id sent.');
						if (empty($in->data['fields'])) throw new \Exception('No fields sent.');
						if ( !$this->getClass()->set( (object) ['where'=>['id'=>$in->data['id']], 'fields'=>$in->data['fields']] ) )
							throw new \Exception('Unable to update listing.');
						$out = new Out('OK');
						break;
				/* Points */
					case 'add-points-in-radius':
						if (empty($in->data) || (empty($in->data) && $in->data !== 0)) throw new \Exception('No radius sent.');
						if ( !$points = $this->getClass('Points')->get() ) throw new \Exception('No points found.');
						if ( !$geoinfo = $this->getClass('ListingsGeoinfo')->get() ) throw new \Exception('No geoinfo found for listings.');
						foreach ($geoinfo as $geo)
							if ($geo->lat && $geo->lng)
								foreach ($points as $point) {
									$distance = $this->getClass('GoogleLocation')->getDistance($geo->lat, $geo->lng, $point->lat, $point->lng);
									if ($distance && $distance <= intval($in->data)) $this->getClass('ListingsPoints')->add((object)[ 'listing_id'=>$geo->listing_id, 'point_id'=>$point->id, 'distance'=>$distance ]);
								}
						$out = new Out('OK');
						break;
					case 'add-from-google':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (!isset($in->data['query'])) throw new \Exception('No query sent.');
						if (!isset($in->data['listing_id'])) throw new \Exception('No listing_id sent.');
						if ( !$geoinfo = $this->getClass('ListingsGeoinfo')->get((object)[ 'where'=>['listing_id'=>$in->data['listing_id']] ])[0] ) throw new \Exception('No geoinfo found for listing '.$in->data['listing_id']);

						$out = new Out('OK', $this->getClass('GoogleLocation')->nearbySearch($geoinfo->lat, $geoinfo->lng, $in->data['query']));
						break;
					case 'get-points-by-id':
						if (empty($in->data)) throw new \Exception('No point ids sent.');
						if ( !$PointsCategories = $this->getClass('PointsCategories')->get() ) throw new \Exception('No points categories found.');
						if ( !$PointsTaxonomy = $this->getClass('PointsTaxonomy')->get( (object)[ 'what'=>['category_id','point_id'], 'where'=>['point_id'=>$in->data] ] )) throw new \Exception('No points taxonomy found.');
						if ( !$Points = $this->getClass('Points')->get((object)[ 'what'=>['id','name','address'], 'where'=>['id'=>$in->data] ]) ) throw new \Exception('No geoinfo found for listing '.$in->data['listing_id']);
						$x = [];
						foreach ($Points as $point) {
							foreach ($PointsTaxonomy as $tax) if ($tax->point_id == $point->id){
								foreach ($PointsCategories as $cat) if ($cat->id == $tax->category_id){
									$point->category = $cat->category;
									break;
								}
								break;
							}
							$x[$point->id] = $point; unset($x[$point->id]->id);
						}
						$out = new Out('OK', $x);
						break;
					case 'add-points':
						if (empty($in->data)) throw new \Exception('No points sent.');
						$results = [];
						foreach ($in->data as $point)
							if (!$this->getClass('Points')->exists((object)['name'=>$point['name'], 'address'=>$point['address']])){
								$in_obj = (object)array(
									'name'=>$point['name'],
									'meta'=>$point['meta'],
									'lat'=>$point['lat'],
									'lng'=>$point['lng'],
									'address'=>$point['address']
								);
								$point_id = $this->getClass('Points')->add($in_obj);
								if ($point_id === false) throw new \Exception('Unable to add point to db.');
								$results[] = [ 'point'=>$point_id, 'category'=>$this->getClass('PointsTaxonomy')->add((object)[ 'point_id'=>$point_id, 'category_id'=>$point['category'] ]) ];
								unset($in_obj);
							} else $results[] = 'already exists';
						$out = new Out('OK', $results);
						break;
				/* Cities */
					case 'update-city':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No city id sent.');
						if (empty($in->data['fields'])) throw new \Exception('No fields sent.');
						if ( !$this->getClass('Cities')->set(array((object)[ 'where'=>['id'=>$in->data['id']], 'fields'=>$in->data['fields']]) ))
							throw new \Exception('Unable to update city.');
						$out = new Out('OK');
						break;
					case 'get-cities':
						if ( !$out = $this->getClass('Cities')->get((object)['orderby'=>'count']) ) throw new \Exception('No cities found.');
						$out = new Out('OK', array_reverse($out));
						break;
				/* Tags */
					case 'get-tags':
						$Tags = $this->getClass('Tags')->get((object)['sortby'=>'tag']);
						$TagsCategories = $this->getClass('TagsCategories')->get((object)['sortby'=>'category']);
						$TagsTaxonomy = $this->getClass('TagsTaxonomy')->get((object)['what'=> ['tag_id','category_id'] ]);
						$QuizTags = $this->getClass('QuizTags')->get();
						$ListingsTags = $this->getClass('ListingsTags')->get();
						$x = [];
						foreach ($QuizTags as $tag) {
							if ( !isset($x[$tag->tag_id]) ) $x[$tag->tag_id] = 0;
							$x[$tag->tag_id]++;
						} $QuizTags = $x;
						foreach ($ListingsTags as $tag) {
							if ( !isset($x[$tag->tag_id]) ) $x[$tag->tag_id] = 0;
							$x[$tag->tag_id]++;
						} $ListingsTags = $x;
						$x = [];
						foreach ($Tags as &$t){
							$t->count_quiz = !isset($QuizTags[$t->id]) ? 0 : $QuizTags[$t->id];
							$t->count_listings = !isset($ListingsTags[$t->id]) ? 0 : $ListingsTags[$t->id];
							foreach ($TagsTaxonomy as $tax) if ($tax->tag_id == $t->id) {
								foreach ($TagsCategories as $cat) if ($cat->id == $tax->category_id){
									if (isset($in->data['group-by-category'])){
										if (!isset($x[$cat->category])) $x[$cat->category] = [];
										$x[$cat->category][] = $t;
									}
									else $t->category = $cat->category;
									break;
								}
								break; // only one category per tag
							}
						}
						if (isset($in->data['group-by-category'])) $Tags = $x;
						// else usort($Tags,function($a,$b){ strcasecmp($a->tag, $b->tag); });
						$out = new Out('OK', $Tags);
						break;
				/* Quiz */
					case 'add-question':
						if ( $this->getClass('QuizQuestions')->add() !== false ) $out = new Out('OK');
						break;
					case 'delete-question':
						$out = ( $x = $this->getClass('QuizQuestions')->delete(array('id'=>$in->data['question_id'])) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags() ) :
							new Out(0, array('unable to delete question', $x));
						break;
					case 'get-quiz-questions':
						$out = new Out('OK', $this->getQuestionsSlidesAndTags());
						break;
					case 'update-questions':
						$q = $ids = array();
						foreach ($in->data as $d) {
							$ids[] = $d['question_id'];
							$q[] = (object) array('where'=>array('id'=>$d['question_id']), 'fields'=>$d['fields']);
						}
						$out = ( $x = $this->getClass('QuizQuestions')->set($q) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($ids) ) :
							new Out(0, $x);
						break;
					case 'add-slide':
						$out = ($x = $this->getClass('QuizSlides')->add(array( 'question_id'=>$in->data['question_id'], 'orderby'=> isset($in->data['orderby']) ? $in->data['orderby'] : null )) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id'])) :
							new Out(0, array('Unable to add slide.', $x));
						break;
					case 'delete-slide':
						$out = ( $x = $this->getClass('QuizSlides')->delete( array('id'=>$in->data['slide_id']) ) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id']) ) :
							new Out(0, array('Unable to delete slide', $x));
						break;
					case 'update-slides':
						$q = array();
						foreach ($in->data as $d)
							$q[] = (object)array( 'where'=>array('id'=>$d['slide_id']), 'fields'=>$d['fields'] );
						$out = ( $x = $this->getClass('QuizSlides')->set($q) ) ?
							new Out('OK') :
							new Out(0, array('Unable to update slides.', $x));
						break;
					case 'add-tag-to-slide':
						$out = ( $x = $this->getClass('QuizTags')->add(array('tag_id'=>$in->data['tag_id'],'slide_id'=>$in->data['slide_id'])) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id']) ) :
							new Out(0, array('Unable to add row', $x));
						break;
					case 'remove-tag-from-slide':
						$out = ( $x = $this->getClass('QuizTags')->delete(array('tag_id'=>$in->data['tag_id'],'slide_id'=>$in->data['slide_id'])) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id']) ) :
							new Out(0, array('Unable to delete tag from slide', $x));
						break;
 				default: throw new \Exception(__FILE__.':('.__LINE__.') - invalid query: '.$in->query); break;
			}
			if ($out) echo json_encode($out);
		} catch (\Exception $e) { parseException($e, true); die(); }
	}
	private function getQuestionsSlidesAndTags($question_id = null){
		$query = (object)array();
		if (!empty($question_id)) $query->where = array('id'=>$question_id);
		$Questions = $this->getClass('QuizQuestions')->get($query);
		$Slides = $this->getClass('QuizSlides')->get();
		$SlideTags = $this->getClass('QuizTags')->get();
		$Tags = $this->getClass('Tags')->get();
		foreach ($Questions as &$q){
			$q->slides = array();
			foreach ($Slides as &$slide) if ($slide->question_id == $q->id){
				$slide->tags = array();
				foreach ($SlideTags as $slide_tag) if ($slide_tag->slide_id == $slide->id) {
					foreach ($Tags as $tag) if ($tag->id == $slide_tag->tag_id){
						$slide->tags[] = $tag;
						break;
					}
				}
				$q->slides[] = $slide;
			}
		}
		return $Questions;
	}

	protected function inviteInList($invite, $invites) {
		foreach($invites as &$data) {
			if ($data->key == $invite)
				return $data;
		}
		return false;
	}

	// this one is for fixing meta data for 2nd on up campaigns that has RESPONSE_FIRST_EMAIL meta, and needs to 
	// get the correct meta data to replace that one.
	// called from ix-email-response
	protected function getResponseForCampaign($flag) {
		$action = 0;
		switch($flag) {
			case PROMO_FIRST:
				$action = RESPONSE_FIRST_EMAIL; 
				break;
			case PROMO_SECOND:
				$action = RESPONSE_SECOND_EMAIL; 
				break;
			case PROMO_THIRD: 
				$action = RESPONSE_THIRD_EMAIL; 
				break;
			case PROMO_FOURTH: 
				$action = RESPONSE_FOuRTH_EMAIL; 
				break;
		}
	}

	protected function getFlagFromAction($action) {
		$flag = 0;
		switch($action) {
			case SENT_FIRST_EMAIL:
				$flag = PROMO_FIRST; break;
			case RESPONSE_FIRST_EMAIL:
				$flag = RESPONDED_TO_FIRST; break;
			case SENT_SECOND_EMAIL:
				$flag = PROMO_SECOND; break;
			case RESPONSE_SECOND_EMAIL:
				$flag = RESPONDED_TO_SECOND; break;
			case SENT_THIRD_EMAIL:
				$flag = PROMO_THIRD; break;
			case RESPONSE_THIRD_EMAIL:
				$flag = RESPONDED_TO_THIRD; break;
			case SENT_FOURTH_EMAIL:
				$flag = PROMO_FOURTH; break;
			case RESPONSE_FOURTH_EMAIL:
				$flag = RESPONDED_TO_FOURTH; break;
			case RESERVED_SOMETHING:
			case RESERVED_PORTAL:
			case RESERVED_AGENT_MATCH:
				$flag = RESERVATION_MADE; break;
			case IMPROVE_LISTING_SENT:
				$flag = IMPROVE_LISTING; break;
			case IMPROVE_LISTING_RESPONSE:
				$flag = RESPONDED_TO_IMPROVE_LISITNG; break;
			case EMAIL_UNSUBSCRIBED_DATA:
				$flag = EMAIL_UNSUBSCRIBED; break;
			case VIEWED_LISTING:
				$flag = RESPONDED_TO_VIEW_LISTING; break;
		}
		return $flag;
	}

	protected function isChartData(&$action) {
		switch($action) {
			case SENT_FIRST_EMAIL: 
			case SENT_SECOND_EMAIL: 
			case SENT_THIRD_EMAIL: 
			case SENT_FOURTH_EMAIL: 
			case IMPROVE_LISTING_SENT: return false;
			case RESPONSE_FIRST_EMAIL: 
			case RESPONSE_SECOND_EMAIL: 
			case RESPONSE_THIRD_EMAIL: 
			case RESPONSE_FOURTH_EMAIL: 
			case RESPONSE_TO_EMAIL: $action = RESPONSE_TO_EMAIL; return true; // make it generic
			case IMPROVE_LISTING_RESPONSE: 
			case VIEWED_LISTING: 
			case EMAIL_UNSUBSCRIBED_DATA: 
			case RESERVED_PORTAL: 
			case RESERVED_AGENT_MATCH: 
			case RESERVED_SOMETHING: 
				return true;
			default:
				$this->log("isChartData - unknown action type: $action", 3);
				return false;
		}
	}

	protected function emailDbMetaActionType($action) {
		$what = "UNKNOWN";
		switch($action) {
			case SENT_FIRST_EMAIL: $what = 'SENT INTRO'; break;
			case SENT_SECOND_EMAIL: $what = 'SENT SECOND'; break;
			case SENT_THIRD_EMAIL: $what = 'SENT THIRD'; break;
			case SENT_FOURTH_EMAIL: $what = 'SENT FOURTH'; break;
			case IMPROVE_LISTING_SENT: $what = 'SENT IMPROVE_IT'; break;
			case RESPONSE_FIRST_EMAIL: $what = 'RESPONSE INTRO'; break;
			case RESPONSE_SECOND_EMAIL: $what = 'RESPONSE SECOND'; break;
			case RESPONSE_THIRD_EMAIL: $what = 'RESPONSE THIRD'; break;
			case RESPONSE_FOURTH_EMAIL: $what = 'RESPONSE FOURTH'; break;
			case RESPONSE_TO_EMAIL: $what = 'RESPONSE EMAIL'; break;
			case IMPROVE_LISTING_RESPONSE: $what = 'RESPONSE IMPROVE'; break;
			case VIEWED_LISTING: $what = 'VIEWED LISTING'; break;
			case EMAIL_UNSUBSCRIBED_DATA: $what = 'UNSUBSCRIBED'; break;
			case RESERVED_PORTAL: $what = 'PORTAL RESERVATION'; break;
			case RESERVED_AGENT_MATCH: $what = 'AGENT MATCH RESERVATION'; break;
			case RESERVED_SOMETHING: $what = 'RESERVATION'; break;
		}
		return $what;
	}

	protected function getEmailDbAction($flag) {
		$action = 0;
		switch(intval($flag)) {
			case RESPONDED_TO_FIRST: 
				$action = RESPONSE_FIRST_EMAIL; 
				break;
			case RESPONDED_TO_SECOND:
				$action = RESPONSE_SECOND_EMAIL; 
				break;
			case RESPONDED_TO_IMPROVE_LISITNG: 
				$action = IMPROVE_LISTING_RESPONSE; 
				break;
			case EMAIL_UNSUBSCRIBED:
				$action = EMAIL_UNSUBSCRIBED_DATA;
				break;
			case PROMO_FIRST: 
				$action = SENT_FIRST_EMAIL; 
				break;
			case PROMO_SECOND:
				$action = SENT_SECOND_EMAIL; 
				break;
			case PROMO_THIRD: 
				$action = SENT_THIRD_EMAIL; 
				break;
			case PROMO_FOURTH: 
				$action = SENT_FOURTH_EMAIL; 
				break;
			case IMPROVE_LISTING: 
				$action = IMPROVE_LISTING_SENT; 
				break;
			case RESERVATION_MADE:
				$action = RESERVED_SOMETHING;
				break;
		}
		return $action;
	}

	protected function emailDbHasMeta($emailDb, $action) {
		if (empty($emailDb->meta))
			return false;

		foreach($emailDb->meta as $meta)
			if ( ($meta->action & $action) ||
				 ($action == RESERVED_SOMETHING &&
				 	($meta->action & RESERVED_PORTAL ||
				 	 $meta->action & RESERVED_AGENT_MATCH)) )
				return true;

		return false;
	}

	// this creates a new meta for any set flags that do not have an associated meta data for it
	protected function fixEmailDb(&$emailDb) {
		$checkFlags = [PROMO_FIRST, PROMO_SECOND, PROMO_THIRD, PROMO_FOURTH, IMPROVE_LISTING,//NO_TABLE_INTRO,
					   RESPONDED_TO_FIRST, RESPONDED_TO_SECOND, RESPONDED_TO_THIRD, RESPONDED_TO_FOURTH, RESPONDED_TO_IMPROVE_LISITNG, RESPONDED_TO_VIEW_LISTING, RESERVATION_MADE, EMAIL_UNSUBSCRIBED];

		$today = date('Y-m-d H:i:s');
		$needUpdate = false;
		foreach($checkFlags as $flag) {
			if ( ($emailDb->flags & $flag) != 0) {
				$action = $this->getEmailDbAction($flag);
				if ($action &&
					!$this->emailDbHasMeta($emailDb, $action)) {
					$meta = new \stdClass();
					$meta->action = $action;
					$meta->date = $today;
					$meta->emailTrackerId = -1;
					$meta->type = "LLC Internal";
					if ($action & (SENT_FIRST_EMAIL | SENT_SECOND_EMAIL | IMPROVE_LISTING_SENT))
						$meta->smtp_id = $emailDb->smtp_id;
					if (empty($emailDb->meta))
						$emailDb->meta = [$meta];
					else {
						$emailDb->meta = (array)$emailDb->meta;
						$emailDb->meta[] = $meta;
					}
					$needUpdate = true;
				}
			}
		}
		if ($needUpdate)
			$this->getClass('SellersEmailDb')->set([(object)['where'=>['id'=>$emailDb->id],
															 'fields'=>['meta'=>$emailDb->meta]]]);
	}

	private function createSellerSearchSQL($which, $active, $first, $second, $third, $distance, $emailDbFlags, $includeSellerWithNoListing) {
		$allFlags = SED_ALL_INACTIVE | SED_ACTIVE | SED_WAITING | SED_REJECTED | SED_TOOCHEAP | SED_NEVER | SED_AVERAGE;

		$s = $this->getClass('Sellers');
		$sql = "SELECT %what% from {$s->getTableName('sellers-email-db')} AS b ";
		$sql.= "INNER JOIN (SELECT * FROM {$s->getTableName()} WHERE %sellerSpecifier%) AS a ON b.seller_id = a.id ";

		switch($active) {
			case 0: $sql .= "WHERE  (b.flags & ".(SED_ACTIVE | SED_WAITING).") "; break;
			case 1: $sql .= "WHERE  (b.flags & ".SED_ACTIVE.") "; break;
			case 2:
				$sql .= "WHERE  ((b.flags & ".$emailDbFlags.") ".($includeSellerWithNoListing ? " OR !(b.flags & $allFlags)) " : ') ');
				break;

		}

		$sellerSpecifier = '';
		if ($which == OPT_ADDRESS) {
			if ($distance == 0 ||
				($distance && (empty($first) || empty($second))) ) {
				if (!empty($first)) {
					$sellerSpecifier = '`city` = "'.$first.'" ';
				}
				if (!empty($second)) {
					$sellerSpecifier .= (!empty($sellerSpecifier) ? "AND " : '')."`state` = '$second' ";
				}
				if (!empty($third)) {
					$sellerSpecifier .= (!empty($sellerSpecifier) ? "AND " : '')."`zip` = '$third' ";
				}
			}	
			else {
				if (empty($second)) {
					$out = new Out('fail', "The state cannot be empty!");
				 	echo json_encode($out);
				 	die;
				}

				require_once(__DIR__.'/../_classes/DistanceScale.php');
				$city = $this->getClass('Cities')->get((object)['where'=>['city'=>$first,
																		  'state'=>$second]]);
				if (empty($city)) {
					$out = new Out('fail', "Cound not find $first, $state in Cities table");
				 	echo json_encode($out);
				 	die;
				}

				$orig_city = array_pop($city);
				$first = true;

				if ($orig_city->lng == -1 ||
					$orig_city->lng == 0) {
					$sellerSpecifier = "`city` = '$first' AND `state` = '$second'";
					$this->log("getsellers - no geocode for $first, $second");
				}
				else {
					$locations = '';
					$lat = (int)(abs( is_float($orig_city->lat) ? $orig_city->lat : floatval($orig_city->lat) ) / 10);
					$deltaLng = $distance / $DistanceScale[$lat][0];
					$deltaLat = $distance / $DistanceScale[$lat][1]; // latitude remains pretty constant at about 60miles/degree
					$locations .= "(c.lng >= ".($orig_city->lng - $deltaLng)." AND c.lng <= ".($orig_city->lng + $deltaLng)." AND c.lat >= ".($orig_city->lat - $deltaLat)." AND c.lat <= ".($orig_city->lat + $deltaLat).")";

				 	$loc = "SELECT DISTINCT * FROM ".$this->getClass('Cities')->getTableName($Cities->table)." AS c WHERE ".$locations;
					$cities = $this->getClass('Cities')->rawQuery($loc);
					$temp = array();

					$this->log("createSellerSearchSQL - distance:$distance found ".count($cities)." cities");
					foreach($cities as $city)
						$temp[$city->id] = $city;
					$cities = $temp;
					unset($temp);
					if (!array_key_exists($orig_city->id, $cities) ) // add original into the list
						$cities[$orig_city->id] = $orig_city;

					foreach($cities as $city) {
						if (empty($city))
							continue;
						$sellerSpecifier .= (strlen($sellerSpecifier) ? ' OR ' : "(")."(`city`= '$city->city' AND `state`='$city->state')";
						// $first ? $first = false : $sellerSpecifier .= ' OR ';
						// // $sellerSpecifier .= "(a.city= '$city->city' AND a.state='$city->state')";
						// $sellerSpecifier .= "(`city`= '$city->city' AND `state`='$city->state')";
						unset($city);
					}
					$sellerSpecifier .= strlen($sellerSpecifier) ? ")" : '';
				}
				unset($orig_city);
			}	
		}
		else if ($which == OPT_IDS) {
			if (!empty($second)) {
				$sellerSpecifier = "(`id` >= $first AND `id` <= $second) ";
			}
			else {
				$pieces = explode(',', $first);
				if (count($pieces)) foreach($pieces as $piece) {
					if (empty($piece))
						continue;
					if (strpos($piece, '-') !== false) {
						$range = explode('-', $piece);
						$range[0] = intval($range[0]);
						$range[1] = intval($range[1]);
						for($i = $range[0]; $i <= $range[1]; $i++)
							$sellerSpecifier .= (!empty($sellerSpecifier) ? 'OR ' : '(') . "`id` = $i ";

					}
					else
						$sellerSpecifier .= (!empty($sellerSpecifier) ? 'OR ' : '(') . "`id` = $piece ";
				}
				$sellerSpecifier .= ") ";
			}
		}
		else
			$sellerSpecifier = '1';

		$sql = str_replace("%sellerSpecifier%", $sellerSpecifier, $sql);
		return $sql;
	}
}
new AJAX_wpAdmin();
