<?php
namespace AH;
require_once(__DIR__.'/../_classes/_Controller.class.php');

class AJAX_wpAdmin extends Controller {
	public function __construct(){
		$in = parent::__construct();
		try {
			if (empty($in->query)) throw new \Exception (__FILE__.':('.__LINE__.') - no query sent');
			switch ($in->query){
				/* Listings */
					case 'getpage':
						$page = gettype($_POST['page']) == 'string' ? intval($_POST['page']) : $_POST['page'];
						$perPage = gettype($_POST['perPage']) == 'string' ? intval($_POST['perPage']) : $_POST['perPage'];
						$t = $this->getClass('ListingsRejected');
						$sql = 'SELECT id, title, author, price, beds, baths, images FROM '.getTableName('listings-rejected')." ORDER BY `id` ASC LIMIT ".($page*$perPage).",$perPage";
						$out = $t->wpdb->get_results($sql);
						// if ($page == 0) {
						// 	$sql = 'SELECT id, title, author, price, beds, baths FROM '.getTableName('listings-rejected')." LIMIT $perPage";
						// 	$out = $t->wpdb->get_results($sql);
						// }
						// else {
						// 	$startId = gettype($_POST['startId']) == 'string' ? intval($_POST['startId']) : $_POST['startId'];
						// 	$sql = 'SELECT id, title, author, price, beds, baths FROM '.getTableName('listings-rejected')." WHERE `id` >= $startId AND `id` < ".($startId+$perPage);
						// 	$out = $t->wpdb->get_results($sql);
						// }
						$out = new Out('OK', $out);
					    break;
					case 'transfer-listing':
						$id = $_POST['data']['id'];
						$lj = $this->getClass('ListingsRejected');
						$sql = 'SELECT * FROM '.getTableName('listings-rejected')." WHERE `id` = $id";
						$out = $lj->wpdb->get_results($sql);
						if ($out) {			
							// add to listings				
							$l = $this->getClass('Listings');
							unset($out[0]->id);
							$x = $l->add($out[0]);
							if ($x) // succeeded
							{
								// move tags over
								$ljt = $this->getClass('ListingsRejectedTags');
								$lt = $this->getClass('ListingsTags');
								$sql = 'SELECT * FROM '.getTableName('listings-rejected-tags')." WHERE `listing_id` = $id";
								$tags = $ljt->wpdb->get_results($sql);
								$q = new \stdClass();
								foreach($tags as $tag) {
									$q->tag_id = $tag->tag_id;
									$q->listing_id = $x;
									$lt->add($q);
								}
								$out = new Out('OK', "Added to listings table at row:".$x);

								$q = new \stdClass();
								// delete from rejected tags
								$q->listing_id = $id;
								$x = $ljt->delete($q);
								unset($q->listing_id);

								// delete from rejected listings
								$q->id = $id;
								$x = $lj->delete($q);								
							}
							else
								$out = new Out('fail', "Did not add listing id:".$id);
						}
						else
							$out = new Out('fail', "Unable to retrieve listing id:".$id);
						break;

					case 'row-count':
						$lj = $this->getClass('ListingsRejected');
						$sql = "SELECT COUNT(*) FROM ".getTableName('listings-rejected');
						$out = $lj->wpdb->get_results($sql);
						foreach($out[0] as $val);
						$out = new Out('OK', $val);
						break;

					case 'delete-listing':
						if (empty($in->data) || (empty($in->data) && $in->data !== 0)) throw new \Exception('No listing id sent.');
						if ( !$this->getClass('ListingsRejected')->delete(['id'=>$in->data['id']]) ) throw new \Exception('Unable to delete listing.');
						$out = new Out('OK');
						break;
					case 'get-listings':
						if ( !$out = $this->getClass('ListingsRejected')->get() ) throw new \Exception('No listings found.');
						$out = new Out('OK', $out);
						break;
					case 'get-listings-with-geoinfo':
						if ( !$listings = $this->getClass('ListingsRejected')->get( (object)['what'=>[ 'id' ]] ) ) throw new \Exception('No listings found.');
						if ( !$geoinfo = $this->getClass('ListingsGeoinfo')->get() ) throw new \Exception('No geoinfo found for listings.');
						$x = [];
						foreach ($listings as &$listing) foreach ($geoinfo as $geo) if ($geo->listing_id == $listing->id && isset($geo->lat)) {
							$x[] = $listing->id;
							break;
						}
						$out = new Out('OK', $x);
						break;
					case 'get-listings-and-points':
						if ( !$listings = $this->getClass('ListingsRejected')->get( (object)['what'=>[ 'id','title','street_address' ]] ) ) throw new \Exception('No listings found.');
						if ( !$geoinfo = $this->getClass('ListingsGeoinfo')->get() ) throw new \Exception('No geoinfo found for listings.');
						if ( !$points = $this->getClass('ListingsPoints')->get() ) throw new \Exception('No points found for listings.');
						$x = [];
						foreach ($listings as $listing){
							$id = $listing->id;
							// setup geoinfo
							foreach ($geoinfo as $geo) if ($geo->listing_id == $id) {
								foreach ($geo as $field=>$value)
									if ($field == 'listing_id' || $field == 'id') continue;
									else $listing->$field = $value;
								break;
							}
							$listing->points = [];
							// setup points
							foreach ($points as $point) if ($point->listing_id == $id)
								$listing->points[$point->point_id] = $point->distance;
							$x[$id] = $listing;
							unset($id);
						}
						$out = new Out('OK', $x);
						break;
					case 'update-listing':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No listing id sent.');
						if (empty($in->data['fields'])) throw new \Exception('No fields sent.');
						if ( !$this->getClass()->set( (object) ['where'=>['id'=>$in->data['id']], 'fields'=>$in->data['fields']] ) )
							throw new \Exception('Unable to update listing.');
						$out = new Out('OK');
						break;
				/* Points */
					case 'add-points-in-radius':
						if (empty($in->data) || (empty($in->data) && $in->data !== 0)) throw new \Exception('No radius sent.');
						if ( !$points = $this->getClass('Points')->get() ) throw new \Exception('No points found.');
						if ( !$geoinfo = $this->getClass('ListingsGeoinfo')->get() ) throw new \Exception('No geoinfo found for listings.');
						foreach ($geoinfo as $geo)
							if ($geo->lat && $geo->lng)
								foreach ($points as $point) {
									$distance = $this->getClass('GoogleLocation')->getDistance($geo->lat, $geo->lng, $point->lat, $point->lng);
									if ($distance && $distance <= intval($in->data)) $this->getClass('ListingsPoints')->add((object)[ 'listing_id'=>$geo->listing_id, 'point_id'=>$point->id, 'distance'=>$distance ]);
								}
						$out = new Out('OK');
						break;
					case 'add-from-google':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (!isset($in->data['query'])) throw new \Exception('No query sent.');
						if (!isset($in->data['listing_id'])) throw new \Exception('No listing_id sent.');
						if ( !$geoinfo = $this->getClass('ListingsGeoinfo')->get((object)[ 'where'=>['listing_id'=>$in->data['listing_id']] ])[0] ) throw new \Exception('No geoinfo found for listing '.$in->data['listing_id']);

						$out = new Out('OK', $this->getClass('GoogleLocation')->nearbySearch($geoinfo->lat, $geoinfo->lng, $in->data['query']));
						break;
					case 'get-points-by-id':
						if (empty($in->data)) throw new \Exception('No point ids sent.');
						if ( !$PointsCategories = $this->getClass('PointsCategories')->get() ) throw new \Exception('No points categories found.');
						if ( !$PointsTaxonomy = $this->getClass('PointsTaxonomy')->get( (object)[ 'what'=>['category_id','point_id'], 'where'=>['point_id'=>$in->data] ] )) throw new \Exception('No points taxonomy found.');
						if ( !$Points = $this->getClass('Points')->get((object)[ 'what'=>['id','name','address'], 'where'=>['id'=>$in->data] ]) ) throw new \Exception('No geoinfo found for listing '.$in->data['listing_id']);
						$x = [];
						foreach ($Points as $point) {
							foreach ($PointsTaxonomy as $tax) if ($tax->point_id == $point->id){
								foreach ($PointsCategories as $cat) if ($cat->id == $tax->category_id){
									$point->category = $cat->category;
									break;
								}
								break;
							}
							$x[$point->id] = $point; unset($x[$point->id]->id);
						}
						$out = new Out('OK', $x);
						break;
					case 'add-points':
						if (empty($in->data)) throw new \Exception('No points sent.');
						$results = [];
						foreach ($in->data as $point)
							if (!$this->getClass('Points')->exists((object)['name'=>$point['name'], 'address'=>$point['address']])){
								$in_obj = (object)array(
									'name'=>$point['name'],
									'meta'=>$point['meta'],
									'lat'=>$point['lat'],
									'lng'=>$point['lng'],
									'address'=>$point['address']
								);
								$point_id = $this->getClass('Points')->add($in_obj);
								if ($point_id === false) throw new \Exception('Unable to add point to db.');
								$results[] = [ 'point'=>$point_id, 'category'=>$this->getClass('PointsTaxonomy')->add((object)[ 'point_id'=>$point_id, 'category_id'=>$point['category'] ]) ];
								unset($in_obj);
							} else $results[] = 'already exists';
						$out = new Out('OK', $results);
						break;
				/* Cities */
					case 'update-city':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No city id sent.');
						if (empty($in->data['fields'])) throw new \Exception('No fields sent.');
						if ( !$this->getClass('Cities')->set(array((object)[ 'where'=>['id'=>$in->data['id']], 'fields'=>$in->data['fields']]) ))
							throw new \Exception('Unable to update city.');
						$out = new Out('OK');
						break;
					case 'get-cities':
						if ( !$out = $this->getClass('Cities')->get((object)['orderby'=>'count']) ) throw new \Exception('No cities found.');
						$out = new Out('OK', array_reverse($out));
						break;
				/* Tags */
					case 'get-tags':
						$Tags = $this->getClass('Tags')->get((object)['sortby'=>'tag']);
						$TagsCategories = $this->getClass('TagsCategories')->get((object)['sortby'=>'category']);
						$TagsTaxonomy = $this->getClass('TagsTaxonomy')->get((object)['what'=> ['tag_id','category_id'] ]);
						$QuizTags = $this->getClass('QuizTags')->get();
						$ListingsTags = $this->getClass('ListingsTags')->get();
						$x = [];
						foreach ($QuizTags as $tag) {
							if ( !isset($x[$tag->tag_id]) ) $x[$tag->tag_id] = 0;
							$x[$tag->tag_id]++;
						} $QuizTags = $x;
						foreach ($ListingsTags as $tag) {
							if ( !isset($x[$tag->tag_id]) ) $x[$tag->tag_id] = 0;
							$x[$tag->tag_id]++;
						} $ListingsTags = $x;
						$x = [];
						foreach ($Tags as &$t){
							$t->count_quiz = !isset($QuizTags[$t->id]) ? 0 : $QuizTags[$t->id];
							$t->count_listings = !isset($ListingsTags[$t->id]) ? 0 : $ListingsTags[$t->id];
							foreach ($TagsTaxonomy as $tax) if ($tax->tag_id == $t->id) {
								foreach ($TagsCategories as $cat) if ($cat->id == $tax->category_id){
									if (isset($in->data['group-by-category'])){
										if (!isset($x[$cat->category])) $x[$cat->category] = [];
										$x[$cat->category][] = $t;
									}
									else $t->category = $cat->category;
									break;
								}
								break; // only one category per tag
							}
						}
						if (isset($in->data['group-by-category'])) $Tags = $x;
						// else usort($Tags,function($a,$b){ strcasecmp($a->tag, $b->tag); });
						$out = new Out('OK', $Tags);
						break;
				/* Quiz */
					case 'add-question':
						if ( $this->getClass('QuizQuestions')->add() !== false ) $out = new Out('OK');
						break;
					case 'delete-question':
						$out = ( $x = $this->getClass('QuizQuestions')->delete(array('id'=>$in->data['question_id'])) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags() ) :
							new Out(0, array('unable to delete question', $x));
						break;
					case 'get-quiz-questions':
						$out = new Out('OK', $this->getQuestionsSlidesAndTags());
						break;
					case 'update-questions':
						$q = $ids = array();
						foreach ($in->data as $d) {
							$ids[] = $d['question_id'];
							$q[] = (object) array('where'=>array('id'=>$d['question_id']), 'fields'=>$d['fields']);
						}
						$out = ( $x = $this->getClass('QuizQuestions')->set($q) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($ids) ) :
							new Out(0, $x);
						break;
					case 'add-slide':
						$out = ($x = $this->getClass('QuizSlides')->add(array( 'question_id'=>$in->data['question_id'], 'orderby'=> isset($in->data['orderby']) ? $in->data['orderby'] : null )) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id'])) :
							new Out(0, array('Unable to add slide.', $x));
						break;
					case 'delete-slide':
						$out = ( $x = $this->getClass('QuizSlides')->delete( array('id'=>$in->data['slide_id']) ) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id']) ) :
							new Out(0, array('Unable to delete slide', $x));
						break;
					case 'update-slides':
						$q = array();
						foreach ($in->data as $d)
							$q[] = (object)array( 'where'=>array('id'=>$d['slide_id']), 'fields'=>$d['fields'] );
						$out = ( $x = $this->getClass('QuizSlides')->set($q) ) ?
							new Out('OK') :
							new Out(0, array('Unable to update slides.', $x));
						break;
					case 'add-tag-to-slide':
						$out = ( $x = $this->getClass('QuizTags')->add(array('tag_id'=>$in->data['tag_id'],'slide_id'=>$in->data['slide_id'])) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id']) ) :
							new Out(0, array('Unable to add row', $x));
						break;
					case 'remove-tag-from-slide':
						$out = ( $x = $this->getClass('QuizTags')->delete(array('tag_id'=>$in->data['tag_id'],'slide_id'=>$in->data['slide_id'])) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id']) ) :
							new Out(0, array('Unable to delete tag from slide', $x));
						break;
 				default: throw new \Exception(__FILE__.':('.__LINE__.') - invalid query: '.$in->query); break;
			}
			if ($out) echo json_encode($out);
		} catch (\Exception $e) { parseException($e); die(); }
	}
	private function getQuestionsSlidesAndTags($question_id = null){
		$query = (object)array();
		if (!empty($question_id)) $query->where = array('id'=>$question_id);
		$Questions = $this->getClass('QuizQuestions')->get($query);
		$Slides = $this->getClass('QuizSlides')->get();
		$SlideTags = $this->getClass('QuizTags')->get();
		$Tags = $this->getClass('Tags')->get();
		foreach ($Questions as &$q){
			$q->slides = array();
			foreach ($Slides as &$slide) if ($slide->question_id == $q->id){
				$slide->tags = array();
				foreach ($SlideTags as $slide_tag) if ($slide_tag->slide_id == $slide->id) {
					foreach ($Tags as $tag) if ($tag->id == $slide_tag->tag_id){
						$slide->tags[] = $tag;
						break;
					}
				}
				$q->slides[] = $slide;
			}
		}
		return $Questions;
	}
}
new AJAX_wpAdmin();