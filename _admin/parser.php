<?php add_thickbox(); ?>
<style>
#parse-start{display:block;}
#parser-nav { margin:0 0 2em; padding:0; }
#parser-nav > li { display:inline-block; margin-right: 1em; }
#parser-nav > li.selected { font-weight:bold; }
span.page-sub-title{ font-size: 0.7em; }
.listing-check { text-align:center; width: 20px; }
.listing-check input { position: relative; top: 7px; left: 5px; }
.listing-thumbnail { text-align:center; width: 40px; }
.listing-thumbnail img { height: 32px; }
ul.thumbnails li { display: inline-block; margin: 0 2px 2px 0; }
ul.thumbnails li img{ height: 25px; }
.raw-listings tbody td{
	font-size: 0.8em;
}
</style>
<ul id="parser-nav"></ul>
<div id="parser-page"><p>Loading parser...</p></div>