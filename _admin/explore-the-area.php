<?php
require_once(__DIR__.'/../_classes/PointsCategories.class.php');
$Cats = new AH\PointsCategories(); $Cats = $Cats->get((object)[ 'what'=>['id','category'] ]);
?>

<style>
.explore-page{
	display: none;
}
.wrap{position: relative;}
.wrap table td { vertical-align: middle; }
#the-menu {margin: 10px 0;}
#the-menu li {display: inline-block; margin:0 1em 0 0;}
#the-menu li a { cursor: pointer; }
#the-menu li a.active { color: #444; }
th.column-checkbox{ padding: 6px 0 0 4px;}
th.column-action{width: 40px;}
th.column-icon{width: 35px;}
#add-manual label, #add-manual select { display: block; }
.pac-container { z-index: 100051 !important; }
</style>
<ul id="the-menu">
	<li><a id="nearby-search">Add By Location</a></li>
	<li><a id="add-manual">Add Manually</a></li>
</ul>
<div id="alr-content">

</div>
<div id="add_from_google" class="explore-page">
	<small><strong>Note:</strong> (theoretical - from Google) We are limited to 100,000 queries a day. Each listing takes 1 query.</small>
	<br/><br/>
	<form id="map-query">
		<label for="search-query">Enter Query Term:</label><br/>
		<small>(no quotes)</small><br/>
		<input type="text" name="search-query" />
		<button>Submit</button>
	</form>
	<ul id="action-menu">
		<li>
			<a id="add-all-checked" href="javascript:;">Add Checked POIs to Category:</a>
			<select id="multi-category"><option disabled selected value="null">Select a category</option>;
				<option value="Attractions">Attractions</option>
				<option value="Dining">Dining</option>
				<option value="Shopping">Shopping</option>
				<option value="Nightlife">Nightlife</option>
				<option value="Golf">Golf</option>
				<option value="Outdoors">Outdoors</option>
			</select>
		</li>
	</ul>
	<table id="nearby-results" class="wp-list-table widefat" style="display:none;">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-cb check-column"><input type="checkbox" /></th>
				<th scope="col" class="manage-column column-action">Action</th>
				<th scope="col" class="manage-column column-icon">Icon</th>
				<th scope="col" class="manage-column column-name">Name/Address</th>
				<th scope="col" class="manage-column column-listing">Listing</th>
				<th scope="col" class="manage-column column-distance">Distance</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th scope="col" class="manage-column column-cb check-column"><input type="checkbox" /></th>
				<th scope="col" class="manage-column column-action">Action</th>
				<th scope="col" class="manage-column column-icon">Icon</th>
				<th scope="col" class="manage-column column-name">Name/Address</th>
				<th scope="col" class="manage-column column-listing">Listing</th>
				<th scope="col" class="manage-column column-distance">Distance</th>
			</tr>
		</tfoot>
		<tbody></tbody>
	</table>
</div>
<div id="add_new" class="explore-page"></div>
<div id="edit_current" class="explore-page">
	<table id="all-listings" class="wp-list-table widefat" style="display:none;">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-action">Action</th>
				<th scope="col" class="manage-column column-listing-id">ID</th>
				<th scope="col" class="manage-column column-num-poi"># of POI</th>
				<th scope="col" class="manage-column column-name">Name</th>
				<th scope="col" class="manage-column column-address">Address</th>
				<th scope="col" class="manage-column column-latlng">Lat / Lng</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th scope="col" class="manage-column column-action">Action</th>
				<th scope="col" class="manage-column column-listing-id">ID</th>
				<th scope="col" class="manage-column column-num-poi"># of POI</th>
				<th scope="col" class="manage-column column-name">Name</th>
				<th scope="col" class="manage-column column-address">Address</th>
				<th scope="col" class="manage-column column-latlng">Lat / Lng</th>
			</tr>
		</tfoot>
		<tbody><tr><td colspan="4">Loading listings...</td></tr></tbody>
	</table>
	<table id="current-places" class="wp-list-table widefat" style="display:none;">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-action">Action</th>
				<th scope="col" class="manage-column column-icon">Icon</th>
				<th scope="col" class="manage-column column-name">Name/Address</th>
				<th scope="col" class="manage-column column-listing">Listing</th>
				<th scope="col" class="manage-column column-distance">Distance</th>
				<th scope="col" class="manage-column column-category">Category</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th scope="col" class="manage-column column-action">Action</th>
				<th scope="col" class="manage-column column-icon">Icon</th>
				<th scope="col" class="manage-column column-name">Name/Address</th>
				<th scope="col" class="manage-column column-listing">Listing</th>
				<th scope="col" class="manage-column column-distance">Distance</th>
				<th scope="col" class="manage-column column-category">Category</th>
			</tr>
		</tfoot>
		<tbody></tbody>
	</table>
</div>
<script type="text/javascript"> point_categories = <?php echo json_encode($Cats); ?>; </script>