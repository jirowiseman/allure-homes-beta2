<style>
ul, li{padding:0; margin: 0; }
.slide-list{width:84px;}
.slide-image{ display: inline-block; margin:1px 1px 0 0;  }
.slide-image img { height: 50px; width: 25px; }
.column-add-slide{width:100px;}
.column-add-slide select {display:block; font-size: 0.9em; padding: 0.5em;}
.column-multi{ width: 100px; }
#tb-submit{ text-align: left; }
#tb-submit input[type=text] { width: 100%;}
#dropzone .dz-message span { color: black; }
#slide-tags li {display: inline-block; margin: 0 3px 3px 0; }
#slide-tags li a{
	background: #0074a2;
	color: white;
	display: block;
	font-size: 0.8em;
	line-height: 1.2em;
	padding: .25em .5em;
	margin: 0;
	text-decoration: none;
}
#slide-tags li a:hover{background: #00abef; }
#add-tags-list li{display: inline-block; width:32%; }
#tb-submit .menu { width: 100%; }
.ui-autocomplete { background: white; font-size: 0.8em; max-height: 150px; width: 80%; z-index: 50;}
.ui-autocomplete li { color: black; }
</style>
<p>
	<a href="javascript:;" class="add-blank-question">Add Blank Question</a> | 
	<a href="<?php bloginfo('wpurl'); ?>/quiz" target="_blank">Go To Quiz</a>
</p>

<table id="questions" class="widefat">
	<thead>
		<tr>
			<th scope="col" class="manage-column column-enabled">Enabled</th>
			<th scope="col" class="manage-column column-predefined">Predefined Quiz</th>
			<th scope="col" class="manage-column column-slides">Slides</th>
			<th scope="col" class="manage-column column-add-slide">Add Slide</th>
			<th scope="col" class="manage-column column-prompt">Prompt/Desc</th>
			<th scope="col" class="manage-column column-multi">Multi</th>
			<th scope="col" class="manage-column column-quiz">Quiz</th>
			<th scope="col" class="manage-column column-weight">Weight</th>
			<th scope="col" class="manage-column column-section">Section</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th scope="col" class="manage-column column-enabled">Enabled</th>
			<th scope="col" class="manage-column column-predefined">Predefined Quiz</th>
			<th scope="col" class="manage-column column-slides">Slides</th>
			<th scope="col" class="manage-column column-add-slide">Add Slide</th>
			<th scope="col" class="manage-column column-prompt">Prompt/Desc</th>
			<th scope="col" class="manage-column column-multi">Multi</th>
			<th scope="col" class="manage-column column-quiz">Quiz</th>
			<th scope="col" class="manage-column column-weight">Weight</th>
			<th scope="col" class="manage-column column-section">Section</th>
		</tr>
	</tfoot>
	<tbody></tbody>
</table>
<script type="text/javascript">var tag_database = <?php require_once(__DIR__.'/../_classes/Tags.class.php'); $Tags = new AH\Tags(); echo json_encode($Tags->get()); ?></script>