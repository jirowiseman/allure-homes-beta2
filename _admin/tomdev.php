<style type="text/css">
a{cursor:pointer;}
.log-menu { float:right; margin: 0; }
.log-menu li { display:inline-block; margin: 0 .25em; }
#logfile-viewer {
	background: #eee;
	border: 1px solid #ddd;
	height: 250px;
	overflow-y: scroll;
	padding: .25em .5em;
	width: 100%;
	-ms-box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}
</style>
<p><strong>DANGER: Clicking these links could cause irreversible damage to the database. Don't click them.</strong></p>
<ul id="tables">
	<li><strong>Tables:</strong> <a data-query="check-all-tables">Check all</a> | <a data-query="clean-extra-rows">Clean extra rows</a></li>
	<li><strong>Image clean:</strong> <a data-query="clean-images">Open Image Cleaner</a> <!-- <a data-query="generate-clean-images">Generate file</a> | <a data-query="generate-clean-images" data-type="listings" data-dir="80x80">Generate file for Listings - 80x80</a> | <a data-query="run-clean-images">Clean</a> --></li>
	<li><strong>Listings:</strong> <a data-query="geocode-listings">Geocode</a>
	<li><strong>Cities:</strong> <a data-query="import-cities-from-listings">Import from Listings</a> | <a data-query="count-cities">Count</a> | <a data-query="geocode-cities">Geocode</a> | <a data-query="listings-cities-pivot">Create Listings Pivot</a></li>
	<li><a data-query="log-cities">View cities log</a> | <a data-query="clear-base-log">Clear Base.class log</a> | <a data-query="clear-exception-log">Clear Exception log</a></li>
</ul>