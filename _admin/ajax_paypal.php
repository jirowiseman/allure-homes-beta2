<?php
namespace AH;
// require_once(__DIR__.'../../../../../wp-includes/pluggable.php');
// require_once(__DIR__.'../../../../../wp-config.php');
// require_once(__DIR__.'../../../../../wp-load.php');
// require_once(__DIR__.'../../../../../wp-includes/wp-db.php');
require_once(__DIR__.'/../_classes/Paypal.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php');
require_once(__DIR__.'/../_classes/Logger.class.php');
require_once(__DIR__.'/../../../../wp-load.php');

// require_once(__DIR__.'/../../../../wp-includes/ms-functions.php');

$Logger = new Logger(1, 'paypal');
$Logger->log("ajax_payal entered, uri:".$_SERVER['REQUEST_URI']);

use PaypalIPN;
$ipn = new PaypalIPN();


global $myPost;
// Use the sandbox endpoint during testing.
$homeURL = get_home_url();
if (strpos($homeURL, 'alpha') !== false)
	$ipn->useSandbox();

$verified = $ipn->verifyIPN();
if ($verified) {
    /*
     * Process IPN
     * A list of variables is available here:
     * https://developer.paypal.com/webapps/developer/docs/classic/ipn/integration-guide/IPNandPDTVariables/
     */
    
    $ppApiContext = strpos($homeURL, 'alpha') !== false ? $apiContextSandbox : $apiContext;
    $Logger->log("Got IPN:".print_r($myPost, true));
}
else
	$Logger->log("IPN verification failed");

// Reply with an empty 200 response to indicate to paypal the IPN was received correctly.
header("HTTP/1.1 200 OK");