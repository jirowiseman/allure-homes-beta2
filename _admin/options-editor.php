<?php
require_once(__DIR__."/../_classes/Options.class.php"); $Options = new AH\Options;
require_once(__DIR__."/../_classes/Sellers.class.php"); $Sellers = new AH\Sellers(1);
require_once(__DIR__."/../_classes/QuizSlides.class.php"); $QuizSlides = new AH\QuizSlides();
require_once(__DIR__."/../_classes/QuizActivity.class.php"); $QuizActivity = new AH\QuizActivity();
require_once(__DIR__."/../_classes/Sellers.class.php"); $Sellers = new AH\Sellers(1);
require_once(__DIR__."/../_classes/Utility.class.php"); 

$agents = $Sellers->get((object)['where'=>['author_id'=>'notnull'],
								 'what'=>['id','first_name','last_name','author_id','flags']]);
// $Sellers->log("got agents:".print_r($agents, true));
$portalAgents = [];
foreach($agents as $agent)
	if ($agent->flags & SELLER_IS_PREMIUM_LEVEL_1)
		$portalAgents[$agent->author_id] = $agent;

uasort($portalAgents, function($a, $b) {
	return $a->author_id - $b->author_id;
});
$Sellers->log("got agents:".print_r($portalAgents, true));

if (strpos(get_home_url(), 'local') !== false)
	$specialAgents = [3,4,5,8,10];
elseif (strpos(get_home_url(), 'alpha') !== false)
	$specialAgents = [8,80,113,180];
elseif (strpos(get_home_url(), 'mobile') !== false)
	$specialAgents = [3,4,5,8];
else
	$specialAgents = [3,4,5,8,284,381];

$specialAgents = $Sellers->get((object)['where'=>['author_id'=>$specialAgents],
								 		'what'=>['id','first_name','last_name','author_id','flags']]);
$x = [];
foreach($specialAgents as $agent)
	$x[$agent->author_id] = $agent;
unset($specialAgents);
$specialAgents = $x;

uasort($agents, function($a, $b) {
	return $a->author_id - $b->author_id;
});
$x = [];
foreach($agents as $agent)
	$x[$agent->author_id] = $agent;
unset($agents);
$agents = $x;

$activitySlide = strpos(get_home_url(), 'localhost') !== false ? 33 : 34;
$opt = $Options->get((object)['where'=>['opt'=>'PredefinedQuizList']]);
if (!empty($opt)) {
	$opt = json_decode($opt[0]->value);
	// $Sellers->log('QuizOptions:'.print_r($opt, true));
	$activitySlide = !isset($opt[0]->predefinedQuizSlide) || empty($opt[0]->predefinedQuizSlide) ? 
		(!isset($opt[0]->activitySlide) || empty($opt[0]->activitySlide) ? $activitySlide : $opt[0]->activitySlide) : $opt[0]->predefinedQuizSlide;
}

$sql = 'SELECT a.file,a.title,b.tag_id,c.tag,c.icon FROM '.$QuizSlides->getTableName().' AS a ';
$sql.= 'INNER JOIN '.$QuizSlides->getTableName('quiz-tags').' AS b ON b.slide_id = a.id ';
$sql.= 'INNER JOIN '.$QuizSlides->getTableName('tags').' AS c ON b.tag_id = c.id ';
$sql.= 'WHERE a.question_id = '.$activitySlide;

$activitySlides = $Options->rawQuery($sql);
// $Sellers->log("sql:$sql, activitySlides:".print_r($activitySlides, true));
$x = [];
$activityList = ['isOption'=>1];
$baseActivities = [0,0];
foreach($activitySlides as $slide) {
	$x[intval($slide->tag_id)] = $slide;
	if (!$baseActivities[0])
		$baseActivities[0] = intval($slide->tag_id);
	elseif (!$baseActivities[1])
		$baseActivities[1] = intval($slide->tag_id);
	$activityList[$slide->tag] = intval($slide->tag_id);
}
unset($activitySlides);
$activitySlides = $x;

$firstQuiz = $QuizActivity->getFirst();
$firstQuiz = !empty($firstQuiz) ? $firstQuiz[0]->id : 0;


$agentList = [$specialAgents,
		      $portalAgents,
		      $agents];

$captureOptions = ['isOption'=>1,
				   'NOT_USED'=>0,
				   'OPTIONAL'=>1,
				   'MUST_HAVE'=>2,
				   'USE_AS_PASSWORD'=>3];

$captureMode = ['isOption'=>1,
				'NOT_USED'=>0,
				'OPTIONAL'=>1,
				'MUST_HAVE'=>2];

$boolean = ['isOption'=>1,
			'True'=>'true',
		    'False'=>'false'];

$zeroOne = ['isOption'=>1,
			'Yes'=>1,
		    'No'=>0];

$queryMode = ['isOption'=>1,
			'Split dialog'=>1,
		    'Single dialog'=>0];

$fbMode = ['isOption'=>1,
			'Use FB'=>1,
		    'Not use FB'=>0];

$markerMode = [	'isOption'=>1,
				'Show symbols'=>1,
			    'Hide symbols'=>0];

$bxSliderMode = ['isOption'=>1,
				 'fade'=>'fade',
				 'horizontal'=>'horizontal',
				 'vertical'=>'vertical'];

$htmlType = ['isOption'=>1,
			 'span'=>'span',
			 'img'=>'img',
			 'a'=>'a',
			 'input'=>'input',
			 'button'=>'button',
			 'checkbox'=>'checkbox'];

$quizSlideImgSize = ['isOption'=>1,
					 '265x170'=>'265x170',
					 '325x265'=>'325x265',
					 '350x450'=>'350x450',
					 '530x340'=>'530x340',
					 '534x400'=>'534x400',
					 '580x550'=>'580x550',
					 '650x530'=>'650x530',
					 'custom'=>'custom'
					 ];

$quizMode = ['isOption'=>1,
			 'Use Quiz ID'=>0,
			 'Use SEO style'=>1];

$portalUserCaptureMode = ['isOption'=>1,
						  'Not Used'=>0,
						  'Optional'=>1,
						  'Hard Stop'=>2];


// create options data structure
$options = ['QuizOptions'=>(object)	['structure'=>['agentID'=>'int',
												   'showListingFilter'=>$zeroOne,
												   'doPredefinedQuiz'=>$zeroOne,
												   'forcePopupPredefinedQuiz'=>$boolean,
												   'forcePopupPredefinedQuizWhenUsedAsLandingPage'=>$boolean,
												   'disablePopupPredefinedQuizWhenReferrerIsPortalLanding'=>$boolean,],
									 'default'=>['agentID'=>'any',
									 			 'showListingFilter'=>0,
									 			 'doPredefinedQuiz'=>0,
									 			 'forcePopupPredefinedQuiz'=>'false',
									 			 'forcePopupPredefinedQuizWhenUsedAsLandingPage'=>'false',
									 			 'disablePopupPredefinedQuizWhenReferrerIsPortalLanding'=>'false'],
									 'agentList'=>0
								   	],
			'HomePageOptions'=>(object)	['structure'=>[
												   'showAgentBenefits'=>$zeroOne],
									 'default'=>[
									 			 'showAgentBenefits'=>1],
									 'agentList'=>-1
								   	],
			'PortalLandingAgentOptions'=>(object)['structure'=>['agentID'=>'int',
															    'nameMode'=>$captureOptions,
															    'emailMode'=>$captureOptions,
															    'phoneMode'=>$captureOptions],
												  'default'=>['agentID'=>'any',
															  'nameMode'=>$captureOptions['MUST_HAVE'],
															  'emailMode'=>$captureOptions['MUST_HAVE'],
															  'phoneMode'=>$captureOptions['NOT_USED']],
												  'agentList'=>1],
			'PortalLandingAgentOptionsOne'=>(object)['structure'=>['agentID'=>'int',
															    'nameMode'=>$captureOptions,
															    'emailMode'=>$captureOptions,
															    'phoneMode'=>$captureOptions],
												  'default'=>['agentID'=>'any',
															  'nameMode'=>$captureOptions['MUST_HAVE'],
															  'emailMode'=>$captureOptions['MUST_HAVE'],
															  'phoneMode'=>$captureOptions['NOT_USED']],
												  'agentList'=>1],
			'PortalLandingAgentOptionsTwo'=>(object)['structure'=>['agentID'=>'int',
															    'nameMode'=>$captureOptions,
															    'emailMode'=>$captureOptions,
															    'phoneMode'=>$captureOptions],
												  'default'=>['agentID'=>'any',
															  'nameMode'=>$captureOptions['MUST_HAVE'],
															  'emailMode'=>$captureOptions['MUST_HAVE'],
															  'phoneMode'=>$captureOptions['NOT_USED']],
												  'agentList'=>1],
			'QuizResultsAgentOptions'=>  (object)['structure'=>['agentID'=>'int',
															    'nameMode'=>$captureOptions,
															    'emailMode'=>$captureOptions,
															    'phoneMode'=>$captureOptions,
															    'queryMode'=>$queryMode,
															    'fbMode'=>$fbMode,
															    'markerMode'=>$markerMode,
															    'textMessageOnEmailCapture'=>$zeroOne,
															    'forcePhoneAsPasswordIfUsed'=>$zeroOne],
												  'default'=>['agentID'=>'any',
															  'nameMode'=>$captureOptions['NOT_USED'],
															  'emailMode'=>$captureOptions['NOT_USED'],
															  'phoneMode'=>$captureOptions['NOT_USED'],
															  'queryMode'=>0,
															  'fbMode'=>0,
															  'markerMode'=>0,
															  'textMessageOnEmailCapture'=>1,
															  'forcePhoneAsPasswordIfUsed'=>0],
												  'agentList'=>1],
			'QuizResultsPortalUserCaptureOptions'=>(object)[
												'structure'=>['agentID'=>'int',
															'forceHardStopOnViewAllListingAtQuizLoad'=>$zeroOne,
															'informUserNotFullyRegisteredUserAfterSignUp'=>$zeroOne,
															'byPassCount'=>(object)['quizLoad'=>0,
																					'viewListing'=>0],
															'quizLoad'=>(object)['captureMode'=>$captureMode,
																				 'delay'=>0,
																				 'mobileDelay'=>0],
															'quizLoadLeadIn'=>(object)['captureMode'=>$captureMode,
																				 'delay'=>0,
																				 'mobileDelay'=>0,
																				 'title'=>'string',
																				 'message'=>'string'],
															'cityScroll'=>(object)['captureMode'=>$captureMode,
																				 'page'=>0,
																				 'delay'=>0,
																				 'mobileDelay'=>0],
															'cityScrollLeadIn'=>(object)['captureMode'=>$captureMode,
																				 'page'=>0,
																				 'delay'=>0,
																				 'mobileDelay'=>0,
																				 'title'=>'string',
																				 'message'=>'string'],
															'enterListings'=>(object)['captureMode'=>$captureMode,
																				 'delay'=>0,
																				 'mobileDelay'=>0],
															'enterListingsLeadIn'=>(object)['captureMode'=>$captureMode,
																				 'delay'=>0,
																				 'mobileDelay'=>0,
																				 'title'=>'string',
																				 'message'=>'string'],
															'listingScroll'=>(object)['captureMode'=>$captureMode,
																				 'page'=>0,
																				 'delay'=>0,
																				 'mobileDelay'=>0],
															'listingScrollLeadIn'=>(object)['captureMode'=>$captureMode,
																				 'page'=>0,
																				 'delay'=>0,
																				 'mobileDelay'=>0,
																				 'title'=>'string',
																				 'message'=>'string'],
															'viewListing'=>(object)['captureMode'=>$captureMode,
																				 'delay'=>0,
																				 'mobileDelay'=>0],
															'viewListingLeadIn'=>(object)['captureMode'=>$captureMode,
																				 'delay'=>0,
																				 'mobileDelay'=>0,
																				 'title'=>'string',
																				 'message'=>'string']],
												'default'=>['agentID'=>'any',
															'forceHardStopOnViewAllListingAtQuizLoad'=>0,
															'informUserNotFullyRegisteredUserAfterSignUp'=>0,
															'byPassCount'=>(object)['quizLoad'=>0,
																					'viewListing'=>0],
															'quizLoad'=>(object)['captureMode'=>$captureMode['NOT_USED'],
																				 'delay'=>3000,
																				 'mobileDelay'=>3000],
															'quizLoadLeadIn'=>(object)['captureMode'=>$captureMode['NOT_USED'],
																				 'delay'=>3000,
																				 'mobileDelay'=>3000,
																				 'title'=>'',
																				 'message'=>''],
															'cityScroll'=>(object)['captureMode'=>$captureMode['NOT_USED'],
																				 'page'=>0,
																				 'delay'=>0,
																				 'mobileDelay'=>0],
															'cityScrollLeadIn'=>(object)['captureMode'=>$captureMode['NOT_USED'],
																				 'page'=>0,
																				 'delay'=>0,
																				 'mobileDelay'=>0,
																				 'title'=>'',
																				 'message'=>''],
															'enterListings'=>(object)['captureMode'=>$captureMode['NOT_USED'],
																				 'delay'=>3000,
																				 'mobileDelay'=>3000],
															'enterListingsLeadIn'=>(object)['captureMode'=>$captureMode['NOT_USED'],
																				 'delay'=>3000,
																				 'mobileDelay'=>3000,
																				 'title'=>'',
																				 'message'=>''],
															'listingScroll'=>(object)['captureMode'=>$captureMode['NOT_USED'],
																				 'page'=>0,
																				 'delay'=>0,
																				 'mobileDelay'=>0],
															'listingScrollLeadIn'=>(object)['captureMode'=>$captureMode['NOT_USED'],
																				 'page'=>0,
																				 'delay'=>0,
																				 'mobileDelay'=>0,
																				 'title'=>'',
																				 'message'=>''],
															'viewListing'=>(object)['captureMode'=>$captureMode['NOT_USED'],
																				 'delay'=>0,
																				 'mobileDelay'=>0],
															'viewListingLeadIn'=>(object)['captureMode'=>$captureMode['NOT_USED'],
																				 'delay'=>0,
																				 'mobileDelay'=>0,
																				 'title'=>'',
																				 'message'=>'']
														   ],
												'agentList'=>1],
			'PortalLandingStrings'=>(object)['structure'=>['agentID'=>'int',
														   'repeating'=>1,	
														   'data0'=>(object)['type'=>$htmlType,
														 				  	'id'=>'string',
														 				  	'html'=>'string',
														 				  	'style'=>'string',
														 				  	'optional_src'=>'string',
														 				  	'optional_href'=>'string',
														 				  	'optional_placeholder'=>'string',
														 				  	'optional_onclick'=>'string',
														 				  	'optional_ischecked'=>$boolean]],
											'default'=>['agentID'=>'int',
														'data0'=>(object)['type'=>'span',
														 				  'id'=>'title',
														 				  'html'=>'Retiring<span class=&quot;dot&quot;>|</span>Selling<span class=&quot;dot&quot;>|</span>Vacation home?',
														 				  'style'=>''],
														'data1'=>(object)['type'=>'span',
														 				  'id'=>'subtext',
														 				  'html'=>'Live somewhere with better lifestyle',
														 				  'style'=>''],
														'data2'=>(object)['type'=>'span',
														 				  'id'=>'subtitle',
														 				  'html'=>'Find Cities &amp; Homes that match your lifestyle perfectly',
														 				  'style'=>'']],
											'agentList'=>1,
											'blank_data'=>(object)['type'=>'span',
												 				  'id'=>'',
												 				  'html'=>'',
												 				  'style'=>'',
											 				   	  'src'=>'',
											 				   	  'href'=>'',
											 				   	  'placeholder'=>'',
											 				   	  'onclick'=>'',
											 				   	  'ischecked'=>false]],
			'PortalLandingStringsOne'=>(object)['structure'=>['agentID'=>'int',
														   'repeating'=>1,	
														   'data0'=>(object)['type'=>$htmlType,
														 				  	'id'=>'string',
														 				  	'html'=>'string',
														 				  	'style'=>'string',
														 				  	'optional_src'=>'string',
														 				  	'optional_href'=>'string',
														 				  	'optional_placeholder'=>'string',
														 				  	'optional_onclick'=>'string',
														 				  	'optional_ischecked'=>$boolean]],
											'default'=>['agentID'=>'int',
														'data0'=>(object)['type'=>'span',
														 				  'id'=>'title',
														 				  'html'=>'Retiring<span class=&quot;dot&quot;>|</span>Selling<span class=&quot;dot&quot;>|</span>Vacation home?',
														 				  'style'=>''],
														'data1'=>(object)['type'=>'span',
														 				  'id'=>'subtext',
														 				  'html'=>'Live somewhere with better lifestyle',
														 				  'style'=>''],
														'data2'=>(object)['type'=>'span',
														 				  'id'=>'subtitle',
														 				  'html'=>'Find Cities &amp; Homes that match your lifestyle perfectly',
														 				  'style'=>'']],
											'agentList'=>1,
											'blank_data'=>(object)['type'=>'span',
												 				  'id'=>'',
												 				  'html'=>'',
												 				  'style'=>'',
											 				   	  'src'=>'',
											 				   	  'href'=>'',
											 				   	  'placeholder'=>'',
											 				   	  'onclick'=>'',
											 				   	  'ischecked'=>false]],
			'PortalLandingStringsTwo'=>(object)['structure'=>['agentID'=>'int',
														   'repeating'=>1,	
														   'data0'=>(object)['type'=>$htmlType,
														 				  	'id'=>'string',
														 				  	'html'=>'string',
														 				  	'style'=>'string',
														 				  	'optional_src'=>'string',
														 				  	'optional_href'=>'string',
														 				  	'optional_placeholder'=>'string',
														 				  	'optional_onclick'=>'string',
														 				  	'optional_ischecked'=>$boolean]],
											'default'=>['agentID'=>'int',
														'data0'=>(object)['type'=>'span',
														 				  'id'=>'title',
														 				  'html'=>'Retiring<span class=&quot;dot&quot;>|</span>Selling<span class=&quot;dot&quot;>|</span>Vacation home?',
														 				  'style'=>''],
														'data1'=>(object)['type'=>'span',
														 				  'id'=>'subtext',
														 				  'html'=>'Live somewhere with better lifestyle',
														 				  'style'=>''],
														'data2'=>(object)['type'=>'span',
														 				  'id'=>'subtitle',
														 				  'html'=>'Find Cities &amp; Homes that match your lifestyle perfectly',
														 				  'style'=>'']],
											'agentList'=>1,
											'blank_data'=>(object)['type'=>'span',
												 				  'id'=>'',
												 				  'html'=>'',
												 				  'style'=>'',
											 				   	  'src'=>'',
											 				   	  'href'=>'',
											 				   	  'placeholder'=>'',
											 				   	  'onclick'=>'',
											 				   	  'ischecked'=>false]],
			'PortalLandingSlides'=>(object)['structure'=>['desktop'=>['csv_strings'],
														  'mobile'=>['csv_strings']],
											'default'=>['desktop'=>["slide1.png","slide1.png","slide2.png","slide3.png","slide4.png","slide5.png","slide6.png","slide7.png","slide8.png"],
														'mobile'=>["slide1.png","slide1.png","slide2.png","slide3.png","slide4.png","slide5.png","slide6.png","slide7.png","slide8.png"]],
											'agentList'=>-1],
			'PortalLandingBxSlider'=>(object)['structure'=>['desktop'=>(object)['pause'=>0,
																			  'speed'=>0,
																			  'auto'=>$boolean,
																			  'controls'=>$boolean,
																			  'pager'=>$boolean,
																			  'mode'=>$bxSliderMode],
														    'mobile'=>(object)['pause'=>0,
																			  'speed'=>0,
																			  'auto'=>$boolean,
																			  'controls'=>$boolean,
																			  'pager'=>$boolean,
																			  'mode'=>$bxSliderMode]],
											  'default'=>['desktop'=>(object)['pause'=>3000,
																			  'speed'=>500,
																			  'auto'=>true,
																			  'controls'=>false,
																			  'pager'=>false,
																			  'mode'=>'fade'],
														  'mobile'=>(object)['pause'=>3000,
																			  'speed'=>500,
																			  'auto'=>true,
																			  'controls'=>false,
																			  'pager'=>false,
																			  'mode'=>'fade']],
											  'agentList'=>-1],
			'PredefinedQuizList' =>	(object)['structure'=>['agentID'=>'int',
														   'imgPath'=>'string',
														   'activitySlide'=>0,
														   'predefinedQuizSlide'=>0,
														   'desktop'=>(object)[
																			   'slideSizeDialog'=>$quizSlideImgSize,
																			   'slideSizeOnPage'=>$quizSlideImgSize,
																			   'widthDialog'=>0,
																			   'heightDialog'=>0,
																			   'widthOnPage'=>0,
																			   'heightOnPage'=>0,
																			   'buttonString' =>'string',
																			   'intro'=>(object)['repeating'=>1,	
																							   	 'data0'=>(object)[ 'type'=>$htmlType,
																								 				  	'id'=>'string',
																								 				  	'html'=>'string',
																								 				  	'style'=>'string',
																								 				  	'optional_src'=>'string',
																								 				  	'optional_href'=>'string',
																								 				  	'optional_placeholder'=>'string',
																								 				  	'optional_onclick'=>'string',
														 				  											'optional_ischecked'=>$boolean]],
																			   'quiz_list'=>(object)['repeating'=>1,
															   										 'data0'=>(object)['quizMode'=>$quizMode,
															   										 				   'quizID'=>0,
															   										 				   'quizSEO'=>'string',
															   														   'img'=>'string',
															   														   'title'=>'string',
															   														   'icon'=>'string',
															   														   'tag'=>$activityList]
																			   						]
																			   ],
															'mobile'=>(object)[
																			   'slideSize'=>$quizSlideImgSize,
																			   'width'=>0,
																			   'height'=>0,
																			   'buttonString' =>'string',
																			   'intro'=>(object)['repeating'=>1,	
																							   	 'data0'=>(object)[ 'type'=>$htmlType,
																								 				  	'id'=>'string',
																								 				  	'html'=>'string',
																								 				  	'style'=>'string',
																								 				  	'optional_src'=>'string',
																								 				  	'optional_href'=>'string',
																								 				  	'optional_placeholder'=>'string',
																								 				  	'optional_onclick'=>'string',
														 				  											'optional_ischecked'=>$boolean]],
																			   'quiz_list'=>(object)['repeating'=>1,
															   										 'data0'=>(object)['quizMode'=>$quizMode,
															   										 				   'quizID'=>0,
															   										 				   'quizSEO'=>'string',
															   														   'img'=>'string',
															   														   'title'=>'string',
															   														   'icon'=>'string',
															   														   'tag'=>$activityList]
																			   						]
																			   ],
														  ],
											 'default'=>['agentID'=>-1,
											 			 'imgPath'=>'_img/_quiz',
											 			 'activitySlide'=>$activitySlide,
											 			 'predefinedQuizSlide'=>0,
											 			 'desktop'=>(object)[
																 			 'slideSizeDialog'=>'534x400',
																 			 'slideSizeOnPage'=>'534x400',
																 			 'widthDialog'=>350,
																 			 'heightDialog'=>450,
																 			 'widthOnPage'=>265,
																 			 'heightOnPage'=>170,
																 			 'buttonString' => 'Create your own custom quiz',
																 			 'intro'=>(object)['data0'=>(object)['type'=>'span',
																							 				   	'id'=>'title',
																							 				   	'html'=>'Choose from one of our sample quizzes.',
																							 				   	'style'=>'',
																						 				   	    'src'=>'',
																						 				   	    'href'=>'',
																						 				   	    'placeholder'=>'',
																						 				   	    'onclick'=>'',
																			 				   	  				'ischecked'=>false],
																							   'data1'=>(object)['type'=>'span',
																							 				   	'id'=>'subtitle',
																							 				   	'html'=>'Open your eyes to unlimited possibilities!',
																							 				   	'style'=>'',
																						 				   	    'src'=>'',
																						 				   	    'href'=>'',
																						 				   	    'placeholder'=>'',
																						 				   	    'onclick'=>'',
																			 				   	  				'ischecked'=>false]],
																 			 'quiz_list'=>(object)['data0'=>(object)['quizMode'=>0,
																 			 										 'quizID'=>$firstQuiz,
															   										 				 'quizSEO'=>'',
																					 			 					 'img'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->file : 'golf.png'),
																					 			 					 'title'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->title : 'Golfing'),
																					 			 					 'tag'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->tag : 'golf'),
																					 			 					 'icon'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->icon : 'Golf.png')],
																					 			   'data1'=>(object)['quizMode'=>1,
																					 			   					 'quizID'=>$firstQuiz,
															   										 				 'quizSEO'=>'beach/CA/Santa Cruz/2000000/pool/fireplace',
																					 			 					 'img'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[1]]->file : 'beach.png'),
																					 			 					 'title'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[1]]->title : 'Relax on the beach'),
																					 			 					 'tag'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[1]]->tag : 'beach'),
																					 			 					 'icon'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[1]]->icon : 'Beach.png')]]
																			],
														 'mobile'=>(object)[
																 			 'slideSize'=>'350x450',
																 			 'width'=>350,
																 			 'height'=>450,
																 			 'buttonString' => 'Create your own custom quiz',
																 			 'intro'=>(object)['data0'=>(object)['type'=>'span',
																							 				   	'id'=>'title',
																							 				   	'html'=>'Choose from one of our sample quizzes.',
																							 				   	'style'=>'',
																						 				   	    'src'=>'',
																						 				   	    'href'=>'',
																						 				   	    'placeholder'=>'',
																						 				   	    'onclick'=>'',
																			 				   	  				'ischecked'=>false],
																							   'data1'=>(object)['type'=>'span',
																							 				   	'id'=>'subtitle',
																							 				   	'html'=>'Open your eyes to unlimited possibilities!',
																							 				   	'style'=>'',
																						 				   	    'src'=>'',
																						 				   	    'href'=>'',
																						 				   	    'placeholder'=>'',
																						 				   	    'onclick'=>'',
																			 				   	  				'ischecked'=>false]],
																 			 'quiz_list'=>(object)['data0'=>(object)['quizMode'=>0,
																 			  										 'quizID'=>$firstQuiz,
															   										 				 'quizSEO'=>'',
																					 			 					 'img'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->file : 'golf.png'),
																					 			 					 'title'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->title : 'Golfing'),
																					 			 					 'tag'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->tag : 'golf'),
																					 			 					 'icon'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->icon : 'Golf.png')],
																					 			   'data1'=>(object)['quizMode'=>1,
																					 			   					 'quizID'=>$firstQuiz,
															   										 				 'quizSEO'=>'beach/CA/Santa Cruz/2000000/pool/fireplace',
																					 			 					 'img'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[1]]->file : 'beach.png'),
																					 			 					 'title'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[1]]->title : 'Relax on the beach'),
																					 			 					 'tag'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[1]]->tag : 'beach'),
																					 			 					 'icon'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[1]]->icon : 'Beach.png')]]
																			],
											 			],
											 'agentList'=>1,
											 // maintain depth of blank data
											 'blank_data'=>['desktop'=>(object)['intro'=>(object)['type'=>'span',
																			 				   	  'id'=>'',
																			 				   	  'html'=>'',
																			 				   	  'style'=>'',
																			 				   	  'src'=>'',
																			 				   	  'href'=>'',
																			 				   	  'placeholder'=>'',
																			 				   	  'onclick'=>'',
																			 				   	  'ischecked'=>false],
											 									'quiz_list'=>(object)['quizMode'=>0,
											 														  'quizID'=>$firstQuiz,
															   										  'quizSEO'=>'',
																				 					  'img'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->file : 'golf.png'),
																	 			 					  'title'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->title : 'Golfing'),
																	 			 					  'tag'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->tag : 'golf'),
																					 			 	  'icon'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->icon : 'Golf.png')
																	 					 			 ]
															 				   ],
															'mobile'=>(object)['intro'=>(object)['type'=>'span',
																			 				   	  'id'=>'',
																			 				   	  'html'=>'',
																			 				   	  'style'=>'',
																			 				   	  'src'=>'',
																			 				   	  'href'=>'',
																			 				   	  'placeholder'=>'',
																			 				   	  'onclick'=>'',
																			 				   	  'ischecked'=>false],
																			   'quiz_list'=>(object)['quizMode'=>0,
																									  'quizID'=>$firstQuiz,
															   										  'quizSEO'=>'',
																				 					  'img'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->file : 'golf.png'),
																	 			 					  'title'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->title : 'Golfing'),
																	 			 					  'tag'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->tag : 'golf'),
																					 			 	  'icon'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->icon : 'Golf.png')
																	 					 			 ]
															 				   ]
															]
											],
			'PredefinedQuizListHome' =>	(object)['structure'=>['agentID'=>'int',
															   'imgPath'=>'string',
															   'showPredefinedQuiz'=>$zeroOne,
															   'home'=>(object)[
																			   'slideSize'=>$quizSlideImgSize,
																			   'width'=>0,
																			   'height'=>0,
																			   'intro'=>(object)['repeating'=>1,	
																							   	 'data0'=>(object)[ 'type'=>$htmlType,
																								 				  	'id'=>'string',
																								 				  	'html'=>'string',
																								 				  	'style'=>'string',
																								 				  	'optional_src'=>'string',
																								 				  	'optional_href'=>'string',
																								 				  	'optional_placeholder'=>'string',
																								 				  	'optional_onclick'=>'string']],
																			   'quiz_list'=>(object)['repeating'=>1,
															   										 'data0'=>(object)['quizMode'=>$quizMode,
															   										 				   'quizID'=>0,
															   										 				   'quizSEO'=>'string',
															   														   'img'=>'string',
															   														   'title'=>'string',
															   														   'icon'=>'string',
															   														   'tag'=>$activityList]
																			   						]
																			]
														  ],
												 'default'=>['agentID'=>-1,
												 			 'imgPath'=>'_img/_quiz',
												 			 'showPredefinedQuiz'=>0,
												 			 'home'=>(object)[
																 			 'slideSize'=>'350x450',
																 			 'width'=>350,
																 			 'height'=>450,
																 			 'intro'=>(object)['data0'=>(object)['type'=>'span',
																							 				   	'id'=>'title',
																							 				   	'html'=>'Choose from one of our sample quizzes.',
																							 				   	'style'=>'',
																						 				   	    'src'=>'',
																						 				   	    'href'=>'',
																						 				   	    'placeholder'=>'',
																						 				   	    'onclick'=>'',
																			 				   	  				'ischecked'=>false],
																							   'data1'=>(object)['type'=>'span',
																							 				   	'id'=>'subtitle',
																							 				   	'html'=>'Open your eyes to unlimited possibilities!',
																							 				   	'style'=>'',
																						 				   	    'src'=>'',
																						 				   	    'href'=>'',
																						 				   	    'placeholder'=>'',
																						 				   	    'onclick'=>'',
																			 				   	  				'ischecked'=>false]],
																 			 'quiz_list'=>(object)['data0'=>(object)['quizMode'=>0,
																 			 										 'quizID'=>$firstQuiz,
															   										 				 'quizSEO'=>'',
																					 			 					 'img'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->file : 'golf.png'),
																					 			 					 'title'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->title : 'Golfing'),
																					 			 					 'tag'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->tag : 'golf'),
																					 			 					 'icon'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->icon : 'Golf.png')],
																					 			   'data1'=>(object)['quizMode'=>1,
																					 			   					 'quizID'=>$firstQuiz,
															   										 				 'quizSEO'=>'beach/CA/Santa Cruz/2000000/pool/fireplace',
																					 			 					 'img'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[1]]->file : 'beach.png'),
																					 			 					 'title'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[1]]->title : 'Relax on the beach'),
																					 			 					 'tag'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[1]]->tag : 'beach'),
																					 			 					 'icon'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[1]]->icon : 'Beach.png')]]
																			]
											 			],
											 'agentList'=>1,
											 // maintain depth of blank data
											 'blank_data'=>['home'=>(object)['intro'=>(object)['type'=>'span',
																			 				   	  'id'=>'',
																			 				   	  'html'=>'',
																			 				   	  'style'=>'',
																			 				   	  'src'=>'',
																			 				   	  'href'=>'',
																			 				   	  'placeholder'=>'',
																			 				   	  'onclick'=>'',
																			 				   	  'ischecked'=>false],
											 									'quiz_list'=>(object)['quizMode'=>0,
											 														  'quizID'=>$firstQuiz,
															   										  'quizSEO'=>'',
																				 					  'img'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->file : 'golf.png'),
																	 			 					  'title'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->title : 'Golfing'),
																	 			 					  'tag'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->tag : 'golf'),
																					 			 	  'icon'=>(!empty($activitySlides) ? $activitySlides[$baseActivities[0]]->icon : 'Golf.png')
																	 					 			 ]
															 				   ]
															]
											],
			'SellerLeadCaptureOption' => (object) ['structure'=>['showLeadCapture'=>$zeroOne,
																 'questions'=>(object)['repeating'=>1,
																  						'data0'=>(object)['text'=>'textarea',
																  										  'buttons'=>['repeating'=>1,
																  										  			  'data0'=>(object)[ 'type'=>$htmlType,
																													 				  	 'id'=>'string',
																													 				  	 'html'=>'string',
																													 				   	 'style'=>'string',
																													 				  	 'optional_src'=>'string',
																													 				  	 'optional_href'=>'string',
																													 				  	 'optional_placeholder'=>'string',
																													 				  	 'optional_onclick'=>'string',
																			 				  											 'optional_ischecked'=>$boolean]
																  										  			 ]
																  										 ]
																  						],
						  										  'capture_options'=>(object)['text'=>'textarea',
						  										  							  'quizLoad'=>(object)[	'mode'=>$portalUserCaptureMode,
																			 				  						'delay'=>'number',
								  										  			  							  ],
								  										  			  		  'cityScroll'=>(object)[
								  										  			  		  						'mode'=>$portalUserCaptureMode,
																			 				  						'pageNumber'=>'number',
								  										  			  							  ],
								  										  			  		  'listingScroll'=>(object)[
								  										  			  		  						'mode'=>$portalUserCaptureMode,
																			 				  						'pageNumber'=>'number',
								  										  			  							  ],
								  										  			  		  'viewListing'=>(object)[
																			 				  						'mode'=>$portalUserCaptureMode
								  										  			  							  ],
								  										  			  		  'buttons'=>['repeating'=>1,
													  										  			  'data0'=>(object)[ 'type'=>$htmlType,
																										 				  	 'id'=>'string',
																										 				  	 'html'=>'string',
																										 				   	 'style'=>'string',
																										 				  	 'optional_src'=>'string',
																										 				  	 'optional_href'=>'string',
																										 				  	 'optional_placeholder'=>'string',
																										 				  	 'optional_onclick'=>'string',
																 				  											 'optional_ischecked'=>$boolean]
													  										  			 ]
						  										  			 		  ],
																 ], // end structure
													'default'=>['showLeadCapture'=>1,
																'questions'=>(object)['initial_ask'=>(object)['text'=>'1: Basic question first.  Do you want any lead capture?',
																										'buttons'=>['capture_lead'=>(object)[	'type'=>'button',
																																		'id'=>'yes',
																																		'html'=>'Yes',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNext('choose_page')",
																									 				   	  				'ischecked'=>false ],
																									 				'no_capture'=>(object)[	'type'=>'button',
																																		'id'=>'no',
																																		'html'=>'No',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNoCapture()",
																									 				   	  				'ischecked'=>false ]
																									 			   ]
																									   ],
																					  'choose_page'=>(object)['text'=>'Do you want to capture the lead on the Portal Landing or the Quiz Results page?',
																										'buttons'=>['back'=>(object)[	'type'=>'button',
																																		'id'=>'back',
																																		'html'=>'Back',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onBack('initial_ask')",
																									 				   	  				'ischecked'=>false ],
																									 				'portal_landing'=>(object)[	'type'=>'button',
																																		'id'=>'landing',
																																		'html'=>'Landing',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNext('use_password')",
																									 				   	  				'ischecked'=>false ],
																									 				'quiz_results'=>(object)[	'type'=>'button',
																																		'id'=>'quiz-results',
																																		'html'=>'Quiz Results',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNext('choose_path')",
																									 				   	  				'ischecked'=>false ]
																									 			   ]
																									   ],
																					  'use_password'=>(object)['text'=>'Do you want their phone number? (will be used as their password as a fully registered user)',
																										'buttons'=>['back'=>(object)[	'type'=>'button',
																																		'id'=>'back',
																																		'html'=>'Back',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onBack('choose_page')",
																									 				   	  				'ischecked'=>false ],
																									 				'yes'=>(object)[	'type'=>'button',
																																		'id'=>'yes',
																																		'html'=>'Yes',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onPassword(1)",
																									 				   	  				'ischecked'=>false ],
																									 				'no'=>(object)[	'type'=>'button',
																																		'id'=>'no',
																																		'html'=>'No',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onPassword(0)",
																									 				   	  				'ischecked'=>false ],
																									 			   ]
																									   ],
																					  'choose_path' =>(object)['text'=>'Do you want to use a setup that has worked well for other agents (choose Optimal *subject to change) or define your own setup (choose Advanced)?',
																					  							'buttons'=>['back'=>(object)[	'type'=>'button',
																																		'id'=>'back',
																																		'html'=>'Back',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onBack('choose_page')",
																									 				   	  				'ischecked'=>false ],
																									 				'optimal'=>(object)[	'type'=>'button',
																																		'id'=>'optimal',
																																		'html'=>'Optimal',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onChooseOptimal()",
																									 				   	  				'ischecked'=>false ],
																									 				'advanced'=>(object)[	'type'=>'button',
																																		'id'=>'advanced',
																																		'html'=>'Advanced',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNext('desc_triggers')",
																									 				   	  				'ischecked'=>false ],
																									 			   ]
																					  				   ],
																					  'desc_triggers'=>(object)['text'=>'In the Quiz Results page, there are four events (triggers) that can popup the capture dialog.  You can use all, any or none of those events.  You can also set them individually to be idle, optional, or hard stop.  Hard stop means the user must enter all information before continuing.',
																										'buttons'=>['back'=>(object)[	'type'=>'button',
																																		'id'=>'back',
																																		'html'=>'Back',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onBack('choose_path')",
																									 				   	  				'ischecked'=>false ],
																									 				'next'=>(object)[	'type'=>'button',
																																		'id'=>'next',
																																		'html'=>'Next',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNext('show_events')",
																									 				   	  				'ischecked'=>false ],
																									 				'setup'=>(object)[	'type'=>'button',
																																		'id'=>'setup',
																																		'html'=>'Go to Setup',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNext('capture_options')",
																									 				   	  				'ischecked'=>false ],
																									 				'exit'=>(object)[	'type'=>'button',
																																		'id'=>'exit',
																																		'html'=>'Exit',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNoCapture()",
																									 				   	  				'ischecked'=>false ]
																									 			   ]
																									   ],
																					  'show_events'=>(object)['text'=>'The four events:<br/>1: Quiz Load<br/>2: City Scroll<br/>3: Listing Scroll<br/>4: Listing View',
																										'buttons'=>['back'=>(object)[	'type'=>'button',
																																		'id'=>'back',
																																		'html'=>'Back',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onBack('desc_triggers')",
																									 				   	  				'ischecked'=>false ],
																									 				'next'=>(object)[	'type'=>'button',
																																		'id'=>'next',
																																		'html'=>'Next',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNext('desc_quizLoad')",
																									 				   	  				'ischecked'=>false ],
																									 				'setup'=>(object)[	'type'=>'button',
																																		'id'=>'setup',
																																		'html'=>'Go to Setup',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNext('capture_options')",
																									 				   	  				'ischecked'=>false ],
																									 				'exit'=>(object)[	'type'=>'button',
																																		'id'=>'exit',
																																		'html'=>'Exit',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNoCapture()",
																									 				   	  				'ischecked'=>false ]
																									 			   ]
																									   ],
																					  'desc_quizLoad'=>(object)['text'=>'1: Quiz Load<br/>This event triggers when the results first load.  You can set the amount of time (in milliseconds), you can delay before making the capture dialog appear.',
																										'buttons'=>['back'=>(object)[	'type'=>'button',
																																		'id'=>'back',
																																		'html'=>'Back',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onBack('show_events')",
																									 				   	  				'ischecked'=>false ],
																									 				'next'=>(object)[	'type'=>'button',
																																		'id'=>'next',
																																		'html'=>'Next',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNext('desc_scrolling')",
																									 				   	  				'ischecked'=>false ],
																									 				'setup'=>(object)[	'type'=>'button',
																																		'id'=>'setup',
																																		'html'=>'Go to Setup',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNext('capture_options')",
																									 				   	  				'ischecked'=>false ],
																									 				'exit'=>(object)[	'type'=>'button',
																																		'id'=>'exit',
																																		'html'=>'Exit',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNoCapture()",
																									 				   	  				'ischecked'=>false ]
																									 			   ]
																									   ],
																					  'desc_scrolling'=>(object)['text'=>'2: City Scroll &amp; 3: Listing Scroll<br/>These events trigger when the user scrolls to see more city or listings.  The page number you chose (starting from zero), will trigger the event when that page is going to transition to the next page.',
																										'buttons'=>['back'=>(object)[	'type'=>'button',
																																		'id'=>'back',
																																		'html'=>'Back',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onBack('desc_quizLoad')",
																									 				   	  				'ischecked'=>false ],
																									 				'next'=>(object)[	'type'=>'button',
																																		'id'=>'next',
																																		'html'=>'Next',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNext('desc_listingView')",
																									 				   	  				'ischecked'=>false ],
																									 				'setup'=>(object)[	'type'=>'button',
																																		'id'=>'setup',
																																		'html'=>'Go to Setup',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNext('capture_options')",
																									 				   	  				'ischecked'=>false ],
																									 				'exit'=>(object)[	'type'=>'button',
																																		'id'=>'exit',
																																		'html'=>'Exit',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNoCapture()",
																									 				   	  				'ischecked'=>false ]
																									 			   ]
																									   ],
																					  'desc_listingView'=>(object)['text'=>'4: Listing View<br/>The capture dialog will appear before the transition to the listing page.',
																										'buttons'=>['back'=>(object)[	'type'=>'button',
																																		'id'=>'back',
																																		'html'=>'Back',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onBack('desc_scrolling')",
																									 				   	  				'ischecked'=>false ],
																									 				'next'=>(object)[	'type'=>'button',
																																		'id'=>'next',
																																		'html'=>'Next',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNext('capture_options')",
																									 				   	  				'ischecked'=>false ],
																									 				'exit'=>(object)[	'type'=>'button',
																																		'id'=>'exit',
																																		'html'=>'Exit',
																																		'style'=>'',
																																		'src'=>'',
																																		'href'=>'',
																												 				   	    'placeholder'=>'',
																												 				   	    'onclick'=>"controller.onNoCapture()",
																									 				   	  				'ischecked'=>false ]
																									 			   ]
																									   ],
																					  ],
																'capture_options'=>(object)['text'=>'Specify portal user capture event triggers.	',
																							'quizLoad'=>(object)['mode'=>1,
																												 'delay'=>8000],
																							'cityScroll'=>(object)['mode'=>0,
																												   'pageNumber'=>0],
																							'listingScroll'=>(object)['mode'=>0,
																												   'pageNumber'=>0],
																							'viewListing'=>(object)['mode'=>2],
																							'buttons'=>['back'=>(object)[	'type'=>'button',
																																	'id'=>'back',
																																	'html'=>'Back',
																																	'style'=>'',
																																	'src'=>'',
																																	'href'=>'',
																											 				   	    'placeholder'=>'',
																											 				   	    'onclick'=>"controller.onBack('desc_listingView')",
																								 				   	  				'ischecked'=>false ],
																									 	'save'=>(object)[	'type'=>'button',
																																	'id'=>'save',
																																	'html'=>'Save',
																																	'style'=>'',
																																	'src'=>'',
																																	'href'=>'',
																											 				   	    'placeholder'=>'',
																											 				   	    'onclick'=>'controller.onSetCapture()',
																								 				   	  				'ischecked'=>false ],
																									 	'exit'=>(object)[	'type'=>'button',
																																	'id'=>'exit',
																																	'html'=>'Exit',
																																	'style'=>'',
																																	'src'=>'',
																																	'href'=>'',
																											 				   	    'placeholder'=>'',
																											 				   	    'onclick'=>"controller.onNoCapture()",
																								 				   	  				'ischecked'=>false ]
																									 			   ]
																						   ]
															   ],
													'agentList'=>-1,
													'blank_data'=>['questions'=>(object)['text'=>'',
																						 'buttons'=>['type'=>'button',
																						 			 'id'=>'',
																				 				   	  'html'=>'',
																				 				   	  'style'=>'',
																				 				   	  'src'=>'',
																				 				   	  'href'=>'',
																				 				   	  'placeholder'=>'',
																				 				   	  'onclick'=>'',
																				 				   	  'ischecked'=>false]
																				 		],
																   'capture_options'=>(object)['buttons'=>['type'=>'button',
																							 			  'id'=>'',
																					 				   	  'html'=>'',
																					 				   	  'style'=>'',
																					 				   	  'src'=>'',
																					 				   	  'href'=>'',
																					 				   	  'placeholder'=>'',
																					 				   	  'onclick'=>'',
																					 				   	  'ischecked'=>false]
																					 		  ]
																  ]
												  ],
			'SiteMasterOption' => (object) ['structure'=>['agentID'=>'int',
														  'showGoogleTranslate'=>$zeroOne],
											'default'=>['agentID'=>'any',
														'showGoogleTranslate'=>0],
											'agentList'=>2
										   ],
			'AgentPageOption' => (object) ['structure'=>['agentID'=>'int',
														  'allowAdvancedAgentCompanyEdits'=>$zeroOne,
														  'showFindCompanyButton'=>$zeroOne,
														  'showExtendedCompanyFields'=>$zeroOne,
														  'showCompanyMorePulldownTab'=>$zeroOne,
														  'displayStatsPagePhoneNumbers'=>$zeroOne],
											'default'=>['agentID'=>'any',
														'allowAdvancedAgentCompanyEdits'=>0,
														'showFindCompanyButton'=>0,
														'showExtendedCompanyFields'=>0,
														'showCompanyMorePulldownTab'=>0,
														'displayStatsPagePhoneNumbers'=>1],
											'agentList'=>2
										   ]

		];

	$optionsList = array_keys($options);

	// gather current options from Options table
	$currentOptionsFromDb = [];
	foreach($optionsList as $option) {
		$opt = $Options->get((object)['where'=>['opt'=>$option]]);
		$currentOptionsFromDb[$option] = !empty($opt) ? json_decode($opt[0]->value) : [];
	}

?>

<script type="text/javascript">
var optionsList = <?php echo json_encode($optionsList); ?>;
var currentOptionsFromDb = <?php echo json_encode($currentOptionsFromDb); ?>;
var options = <?php echo json_encode($options); ?>;
var agentList = <?php echo json_encode($agentList); ?>;
var captureOptions = <?php echo json_encode($captureOptions); ?>;
var captureMode = <?php echo json_encode($captureMode); ?>;
var booleanMode = <?php echo json_encode($boolean); ?>;
var bxSliderMode = <?php echo json_encode($bxSliderMode); ?>;
var htmlType = <?php echo json_encode($htmlType); ?>;
var activitySlides = <?php echo json_encode($activitySlides); ?>;
var activityList = <?php echo json_encode($activityList); ?>;
var firstOption = '<?php echo "QuizOptions"; ?>';
</script>

<section id="options-editor">
	<div id="status">
		<span>Welcome to the Options Editor</span>
	</div>
	<div id="save" class="first">
		<button id="save">Save</button>
	</div>
	<div id="main-selections">
		<div id="option-type">
			<span class="title">Option Type</span>
			<select class="selection-list">
			<?php foreach($optionsList as $option)
				echo '<option value="'.$option.'" '.($option == 'QuizOptions' ? 'selected="selected"' : '').'>'.$option.'</option>';
			?>
			</select>
		</div>
		<div id="agent-list">
			<span class="title">Agent List</span>
			<select class="selection-list">
			<?php
				$option = $currentOptionsFromDb['QuizOptions'];
				foreach($option as $set) {
					if ($set->agentID != -1) {
						if (isset($agents[$set->agentID]))
							echo '<option value="'.$set->agentID.'" '.($set->agentID == -1 ? 'selected="selected"' : '').'>ID:'.$set->agentID.' - '.$agents[$set->agentID]->first_name.' '.$agents[$set->agentID]->last_name.'</option>';
						else
							echo '<option value="'.$set->agentID.'" '.($set->agentID == -1 ? 'selected="selected"' : '').'>ID:'.$set->agentID.' - special agent'.'</option>';
					}
					else
						echo '<option value="-1" selected>Any</option>';
				}
			?>
			</select>
		</div>
		<div id="makeNew">
			<span class="title">New Option for Agent</span>
			<input id="doNew" type="checkbox" />
			<div id="newAgentList" class="hidden">
				<span class="title">Assign to:</span>
				<?php
					$option = $currentOptionsFromDb['QuizOptions'];
					$optionAgents = [];
					foreach($option as $set) 
						$optionAgents[] = $set->agentID;
				?>
				<select class="new-agent-list">
				<?php
					if (!in_array(-1, $optionAgents))
						echo '<option value="-1" selected="selected">Any</option>';

					$theList = $agentList[ $options['QuizOptions']->agentList ];

					foreach($theList as $agent)
						if (!in_array($agent->author_id, $optionAgents))
							echo '<option value="'.$agent->author_id.'" >'."ID:$agent->author_id - $agent->first_name $agent->last_name".'</option>';
				?>
				</select>
				<button id="makeNew">Make One</button>
			</div>
		</div>
	</div>
	<div id="options-edit"></div>
	<div id="save" class="second">
		<button id="save">Save</button>
	</div>
	<div id="status">
		<span>Status changes will appear here</span>
	</div>
</section>