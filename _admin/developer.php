<?php
$_POST['query'] = 'versionId';
require_once(__DIR__."/ajax_developer.php");
require_once(__DIR__."/../_classes/Options.class.php"); $Options = new AH\Options;
require_once(__DIR__."/../_classes/Utility.class.php"); 

$opt = $Options->get((object)['where'=>['opt'=>'NO_ASK_ALLOW_MIN_PRICE']]);
$noAskAllowMinPrice = !empty($opt) ? intval($opt[0]->value) : 800000;
$opt = $Options->get((object)['where'=>['opt'=>'BACKUP_AFTER_FULL_PARSE']]);
$backupAfterFullParse = !empty($opt) ? AH\is_true($opt[0]->value) : 0;

$version = $_POST['myVersion'];
?>
<script>
var noAskAllowMinPrice = <?php echo $noAskAllowMinPrice; ?>;
var backupAfterFullParse = <?php echo $backupAfterFullParse ? 1 : 0; ?>;
</script>
<style type="text/css">a{cursor:pointer;}</style>
<p><strong>DANGER: Clicking these links could cause irreversible damage to the database. Don't click them.</strong></p>
<ul id="tables">
	<li><a data-query="test-alert">Test Alert</a></li>
	<li><a data-query="test-or">Test OR</a></li>
	<li><a data-query="test-ipinfo">Test IP Info</a></li>
	<li><a data-query="test-email">Test Email</a></li>
	<li><a data-query="clean-images">Clean Images</a></li>
	<li><a data-query="get-table-info">Get table info</a></li>
	<!-- <li><a data-query="check-table-options">Check options table</a></li> -->
	<li><a data-query="check-all-tables">Check all tables</a></li>
	<li><a data-query="migrate">Migrate</a></li>
	<li><a data-query="fix-image-borders">Fix image borders</a></li>
	<li><a data-query="fix-one-image-border">Fix one image border</a></li>
	<li><a data-query="sim-fix-image-borders">Simulate fix image borders, like after a parse</a></li>
	<li><a data-query="get-active-listings-count">Delete all processed images (this will delete all existing processed images and reset image file to point to the URL again)</a></li>
	<li><a data-query="check-repair-listing-image-data">Repair image origins (this will check for missing processed images and reset image file to point to the URL again)</a></li>
	<li><a data-query="remove-listings">Remove Listings (permanent!!!)</a></li>
	<li><a data-query="update-most-viewed-listings">Update Most Clicked Listings</a></li>
	<li><a data-query="optimize-table">Optimize table</a></li>
	<li><a data-query="restart-apache">Restart Apache</a></li>
	<li><a data-query="count-cities">Count cities</a></li>
	<li><a data-query="get-seller-count">Update Sellers Email DB</a></li>
	<li><a data-query="invitation-list">Invitation List</a></li>
	<li><a data-query="clean-inactive-listings">Clean out inactive listings</a></li>
	<?php
	if ( strpos(get_home_url(), 'alpha.') === false ) : ?>
	<li>
		<a data-query="clean-listings-img-folder">Clean out img listings folder (remove images not part of any listings)</a>
		<button id="begin-image-cleaning">Clean image folder using existing data</button>
		<button id="begin-image-removing">Remove images using existing data</button>
	</li>
	<li><a data-query="check-image-tracker">Get Current Image Tracker Count</a></li>
	<?php endif; ?>
	<!-- <li><a data-query="remove-images-with-old-naming-scheme">Remove images with obsolete filenames</a></li> -->
	<li><a data-query="import-active-listing-images">Localize active listing images</a>&nbsp;<input type="checkbox" id="impressions">Use impressions</input></li>
	<li><a data-query="create-images-from-imported">Create Images from imported</a></li>
	<li><a data-query="start-set-first-image">Set First Image for active listings</a></li>
	<li><a data-query="check-authored-listing-images">Maker sure author-has-account listings has all images, if it is localized already</a></li>
	<li><a data-query="delete-cookie">Delete cookie</a></li>
	<li><a data-query="write-site-maps">Write SiteMaps</a></li>
	<li><a data-query="listings-update-from-tag-space">Use Tag Spaces to update all listings</a></li>
	<li><a data-query="cities-update-from-tag-space">Use Tag Spaces to update all cities</a></li> 
	<li><a data-query="write-site-maps">Write SiteMaps</a></li>
	<li><a data-query="fix-portal-owners">Fix portal user flag and order</a></li>
	<li><a data-query="read-temp-data">Fetch Temp Data</a></li>
	<li><a href="<?php echo wp_logout_url(get_bloginfo('wpurl')); ?>;" title="Log Out">Log Out</a></li>
	<li><a href="<?php echo wp_logout_url(get_bloginfo('wpurl').'/jd'); ?>;" title="Log Out">Log Out with JD</a></li>
	<li class="AWSStart"><a data-query="start-aws">Start AWS</a>
		<select name="EC2Start" id="ec2Start">
			  <option value="1">1</option>
			  <option value="2">2</option>
			  <option value="3">3</option>
			  <option value="4">4</option>
			  <option value="5" selected="selected">5</option>
			  <option value="6">6</option>
			  <option value="7">7</option>
			  <option value="8">8</option>
			  <option value="9">9</option>
			  <option value="10">10</option>
			  <option value="11">11</option>
			  <option value="12">12</option>
			  <option value="13">13</option>
			  <option value="14">14</option>
			  <option value="15">15</option>
			  <option value="16">16</option>
			  <option value="17">17</option>
			  <option value="18">18</option>
			  <option value="19">19</option>
			  <option value="20">20</option>
		</select>
		<div class="spin-wrap">
		    <div class="spinner" style="width:32px; height:32px">
		        <div class="cube1"></div>
		        <div class="cube2"></div>
		  	</div>
		</div>
	</li>
	<li class="AWS"><a data-query="stop-aws">Stop AWS</a></li>
	<li class="AWS"><a data-query="terminate-aws">Terminate AWS</a></li>
	<li class="AWS"><a data-query="get-aws-data">Get AWS Data</a></li>
	<li class="AWS">
		<div style="display:block; width:100%">
			<a data-query="ping-aws">Ping AWS   </a><label>EC2: </label>
			<select name="EC2Ping" id="ec2Ping">
			  <option value="1" selected="selected">1</option>
			  <option value="2">2</option>
			  <option value="3">3</option>
			  <option value="4">4</option>
			  <option value="5">5</option>
			</select>
		</div>
	</li>
	<!-- <li><a data-query="listhub">List Hub</a></li> -->
	<li><form id="listhub" action="javascript:dev.uploadXmlFiles();" display="inline">
		<span>List Hub File:</span> 
		<input type="file" id="listHubFile" value="List Hub File">  </input>
		<input type="submit" float="left"/>
	</form></li>

	<li><form id="quandl" action="javascript:dev.uploadQuandlFiles();" display="inline">
		<span>Quandl City Code File:</span> 
		<input type="file" id="quandlFile" value="Quandl City Code File">  </input>
		<input type="submit" float="left"/>
	</form></li>

	<li><form id="imageBorder" action="javascript:dev.uploadImageFiles();" display="inline">
		<span>Image Border to process File:</span> 
		<input type="file" id="imageBorder" value="Image Border File">  </input>
		<input type="submit" float="left"/>
	</form></li>

	<li><form id="runCommand" action="javascript:dev.runCommand();" display="inline">
		<label>Command</label>&nbsp;<input type="text" id="cmd" />&nbsp;<input type="submit" float="left"/>
	</form></li>

	<li><form id="runCommandFromRemote" action="javascript:dev.runCommandFromRemote();" display="inline">
		<label>Command from remote</label>&nbsp;<input type="text" id="cmd" />&nbsp;<input type="submit" float="left"/>
	</form></li>

	<li><a data-query="safe-ip">Mark safe IP</a></li>
	<li><a data-query="get-active-cities-count">Update median home values</a></li>
	<li><a data-query="get-active-zip-count">Update median home values using ZIP</a></li>
	<li><a data-query="consolidate-median-prices">Consolidate median home prices (uses daily values to update monthly and annual)</a></li>
	<li><a data-query="reset-daily-prices">Reset daily home values to zero</a></li>
	<!-- <li><a data-query="getlisthubdata">List Hub Data</a></li> -->
	<li>
		<div style="display:inline; width:100%">
			<a data-query="getlisthubdataParallel">Process List Hub Data</a>&nbsp;
			<input type="checkbox" id="onlyParse" /><span>Only read feed, no processing</span>&nbsp;&nbsp;
			<label>Units per Chunk: </label>
			<select name="Chunks" id="chunks">
			  <option value="2">2</option>
			  <option value="4">4</option>
			  <option value="8">8</option>
			  <option value="10">10</option>
			  <option value="15">15</option>
			  <option value="20">20</option>
			  <option value="25">25</option>
			  <option value="30">30</option>
			  <option value="40">40</option>
			  <option value="50" selected>50</option>
			</select>
			<label>Number of SubDomains: </label>
			<select name="SubDomain" id="subdomain">
			  <option value="1">1</option>
			  <option value="2">2</option>
			  <option value="3">3</option>
			  <option value="4">4</option>
			  <option value="5">5</option>
			  <option value="6">6</option>
			  <option value="7">7</option>
			  <option value="8">8</option>
			  <option value="9">9</option>
			  <option value="10" selected="selected">10</option>
			  <option value="11">11</option>
			  <option value="12">12</option>
			</select>
			<label>Threads per SubDomain: </label>
			<select name="Threads" id="threads">
			  <option value="2">2</option>
			  <option value="3">3</option>
			  <option value="4">4</option>
			  <option value="5" selected="selected">5</option>
			  <option value="6">6</option>
			</select>
			<label>Load limit:</label> <input type="text" id="loadlimit" style="width: 3em;"/>
			<button id="enableCpuGovernor">Enable Governor</button>
			<br/>
			<label>Max Cpu Delay (Secs)</label> <input type="text" id="cpuDelay" style="width: 3em;"/>
			<label>Max Running SubDomains</label>
			<select name="MaxSubDomain" id="maxsubdomain">
			  <option value="1">1</option>
			  <option value="2">2</option>
			  <option value="3">3</option>
			  <option value="4">4</option>
			  <option value="5">5</option>
			  <option value="6">6</option>
			  <option value="7">7</option>
			  <option value="8">8</option>
			  <option value="9">9</option>
			  <option value="10">10</option>
			  <option value="11">11</option>
			  <option value="12">12</option>
			</select>
			<label>Ajax Limiter</label>
			<select name="AjaxLimiter" id="ajaxlimiter">
			  <option value="1">1</option>
			  <option value="2">2</option>
			  <option value="3">3</option>
			  <option value="4">4</option>
			  <option value="5">5</option>
			  <option value="6">6</option>
			  <option value="7">7</option>
			  <option value="8">8</option>
			  <option value="9">9</option>
			  <option value="10">10</option>
			  <option value="11">11</option>
			  <option value="12">12</option>
			</select>
			<label>CpuAvailablity Gate (point when constraint is lifted)</label>
			<select name="CpuAvailablityGate" id="cpuAvailablityGate">
			<?php
			for($i = 100; $i > 0; $i--)
				echo '<option value="'.$i.'">'.number_format(($i/100), 2).'</option>';
			?>
			</select>
			<br/>
			<label>Save Rejected:</label> <input type="checkbox" id="saveRejected" />
			<label>Min # of Tags: </label>
			<select name="Tags" id="tags">
			  <option value="0">0</option>
			  <option value="1">1</option>
			  <option value="2">2</option>
			  <option value="3" selected="selected">3</option>
			  <option value="4">4</option>
			  <option value="5">5</option>
			  <option value="6">6</option>
			</select>
			<label>Min # of Price: </label>
			<select name="Price" id="price">
			  <option value="500000" <?php echo ($noAskAllowMinPrice == 500000 ? 'selected="selected"' : '');  ?> >500000</option>
			  <option value="510000" <?php echo ($noAskAllowMinPrice == 510000 ? 'selected="selected"' : '');  ?> >510000</option>
			  <option value="520000" <?php echo ($noAskAllowMinPrice == 520000 ? 'selected="selected"' : '');  ?> >520000</option>
			  <option value="530000" <?php echo ($noAskAllowMinPrice == 530000 ? 'selected="selected"' : '');  ?> >530000</option>
			  <option value="540000" <?php echo ($noAskAllowMinPrice == 540000 ? 'selected="selected"' : '');  ?> >540000</option>
			  <option value="550000" <?php echo ($noAskAllowMinPrice == 550000 ? 'selected="selected"' : '');  ?> >550000</option>
			  <option value="560000" <?php echo ($noAskAllowMinPrice == 560000 ? 'selected="selected"' : '');  ?> >560000</option>
			  <option value="570000" <?php echo ($noAskAllowMinPrice == 570000 ? 'selected="selected"' : '');  ?> >570000</option>
			  <option value="580000" <?php echo ($noAskAllowMinPrice == 580000 ? 'selected="selected"' : '');  ?> >580000</option>
			  <option value="590000" <?php echo ($noAskAllowMinPrice == 590000 ? 'selected="selected"' : '');  ?> >590000</option>
			  <option value="600000" <?php echo ($noAskAllowMinPrice == 600000 ? 'selected="selected"' : '');  ?> >600000</option>
			  <option value="610000" <?php echo ($noAskAllowMinPrice == 610000 ? 'selected="selected"' : '');  ?> >610000</option>
			  <option value="620000" <?php echo ($noAskAllowMinPrice == 620000 ? 'selected="selected"' : '');  ?> >620000</option>
			  <option value="630000" <?php echo ($noAskAllowMinPrice == 630000 ? 'selected="selected"' : '');  ?> >630000</option>
			  <option value="640000" <?php echo ($noAskAllowMinPrice == 640000 ? 'selected="selected"' : '');  ?> >640000</option>
			  <option value="650000" <?php echo ($noAskAllowMinPrice == 650000 ? 'selected="selected"' : '');  ?> >650000</option>
			  <option value="660000" <?php echo ($noAskAllowMinPrice == 660000 ? 'selected="selected"' : '');  ?> >660000</option>
			  <option value="670000" <?php echo ($noAskAllowMinPrice == 670000 ? 'selected="selected"' : '');  ?> >670000</option>
			  <option value="680000" <?php echo ($noAskAllowMinPrice == 680000 ? 'selected="selected"' : '');  ?> >680000</option>
			  <option value="690000" <?php echo ($noAskAllowMinPrice == 690000 ? 'selected="selected"' : '');  ?> >690000</option>
			  <option value="700000" <?php echo ($noAskAllowMinPrice == 700000 ? 'selected="selected"' : '');  ?> >700000</option>
			  <option value="750000" <?php echo ($noAskAllowMinPrice == 750000 ? 'selected="selected"' : '');  ?> >750000</option>
			  <option value="800000" <?php echo ($noAskAllowMinPrice == 800000 ? 'selected="selected"' : '');  ?> >800000</option>
			  <option value="850000" <?php echo ($noAskAllowMinPrice == 850000 ? 'selected="selected"' : '');  ?> >850000</option>
			  <option value="900000" <?php echo ($noAskAllowMinPrice == 900000 ? 'selected="selected"' : '');  ?> >900000</option>
			  <option value="950000" <?php echo ($noAskAllowMinPrice == 950000 ? 'selected="selected"' : '');  ?> >950000</option>
			  <option value="1000000" <?php echo ($noAskAllowMinPrice == 1000000 ? 'selected="selected"' : '');  ?> >1000000</option>
			</select>
			&nbsp;&nbsp;<input type="checkbox" id="restartSQL">RestartSQL</input>
		</div>
	</li>
	<li><input type="button" id="stopParallel" value="Stop parallel list hub data processing"/></li>
	<li><input type="button" id="restartParallel" value="Restart parallel list hub data processing"/></li>
	<li><input type="button" id="restartParallelIP" value="Restart parallel image processing"/></li>
	<li><input type="button" id="restartParallelCustomIP" value="Restart custom image processing"/></li>
	<li><input type="button" id="monitorParallel" value="Monitor parallel processing"/></li>
	<li><input type="button" id="replayParallel" value="Replay last parallel list hub data processing"/></li>
	<li><input type="button" id="pauseParallel" value="Pause parallel list hub data processing"/></li>
	<li><input type="button" id="haltParallel" value="Halt parallel list hub data processing"/></li>
	<li><input type="button" id="registerAsRemotePP" value="Register this to be able to run parallel list hub data processing"/></li>
	<li><input type="button" id="startRemoteParallel" value="Initiate remote parallel list hub data processing"/></li>
	<li><input type="button" id="startListHubDataProcessing" value="Initiate complete list hub data processing"/></li>
	<li><input type="button" id="startRemoteImageProcessingProcessing" value="Initiate remote image processing processing"/></li>
	<li><input type="button" id="reloadPage" value="Force reload page of registered remote"/></li>
	<li><input type="button" id="lockSql" value="Force lock SQL of registered remote"/></li>
	<li><input type="button" id="unlockSql" value="Force unlock SQL of registered remote"/></li>
	<li><input type="button" id="pingremote" value="Ping registered remote"/></li>
	<li><input type="button" id="statsremote" value="Stats from registered remote"/></li>
	<li><input type="button" id="sendFlagsToRemote" value="Send Flag/Value pair to remote"/></li>
	<li><div id="oneChunkSpan"><input type="button" id="onechunk" value="Do one chunk"/>Starting with block#:   <input type="text" id="oneChunkStart"/></div></li>
</ul>
<div><label>Buffer Size:</label>&nbsp;<label id="buffer-size">0</label>&nbsp;
	<label>Buffer Index:</label>&nbsp;<label id="buffer-index">0</label>
	<button id="buffer-dump">Write buffer</button>&nbsp;&nbsp;<input type="checkbox" id="recordChunkChecking">Record Chunk Status Checking</input>
</div>
<div>Percentage:<input type="text" value="0.00" id="percentage" readonly="readonly" width="50px">  
	 Estimate:<input type="text" value="" id="estimate" readonly="readonly" width="100px">
	 Load:<input type="text" value="" id="load" readonly="readonly" style="width: 6em;">
	 CpuAvailablity:<input type="text" value="" id="cpuAvailability" readonly="readonly" style="width: 5em;">
	 Chunk Delay Msec:<input type="text" value="" id="chunkDelay" readonly="readonly" style="width: 6em;">
	 Last Image Retrieval PerSec:<input type="text" value="" id="imageRetrievalPerSec" readonly="readonly" style="width: 6em;">
	 <br/>Lock SQL during parse&nbsp;&nbsp;<input type="checkbox" id="locksql"/><br/>
</div>

<div class="progress">Progress:
	<span id="progress" />
</div>
<div>Version:<?php echo $version; ?></div>
<br/>
<br/>
<div style="width:100%">
	<div>
		<canvas id="chart" height="150" width="600"></canvas>
	</div>
</div>
<br/>
<div style="display:inline" id="legend-div">
	<form id="legend" style="width:12%; float:left">Legend:
	<ul class="line-legend" >
		<li><span style="background-color:rgba(220,220,220,1)">active</span>        <label id="currentActive">0</label></li>
		<li><span style="background-color:rgba(151,187,205,1)">load</span>        <label id="currentLoad">0</label></li>
		<li><span style="background-color:rgba(251,187,205,1)">denied</span></li>
		<li><span style="background-color:rgba(251,135,135,1)">networkerror </span></li>
		<li><span style="background-color:rgba(135,135,255,1)" id="added">added</span>        <label id="addedTotal">0</label></li>
		<li><span style="background-color:rgba(135,255,135,1)" id="dups">dups</span>        <label id="dupsTotal">0</label></li>
		<li><span style="color:rgba(200,200,200,1); background:rgba(120,120,120,1)" id="rejected">rejected</span>        <label id="rejectedTotal">0</label></li>
		<li><span style="color:rgba(200,200,200,1); background:rgba(255,50,50,1)" id="belowMinPrice">belowMininum</span>        <label id="belowMinimumAcceptedPrice">0</label></li>
	</ul>
	</form>
	<form id="tableForm" style="width:52%; float: right; top: 10px; margin-right: 17%">

	</form>
	<div id="extra-info" style="height: 200px; overflow: scroll; background-color: white;"></div>
</div>
<!--
<div id="tesetArea">
	<b>Pick your favorite color</b><br>
    <form method="POST" action="<?php echo get_template_directory_uri(); ?>/_admin/ajax_paywhirl.php">
    <input type="RADIO" name="color" value="red"> Red<br>
    <input type="RADIO" name="color" value="green"> Green<br>
    <input type="RADIO" checked name="color" value="blue"> Blue<br>
    <input type="RADIO" name="color" value="cyan"> Cyan<br>
    <input type="RADIO" name="color" value="magenta"> Magenta<br>
    <input type="RADIO" name="color" value="yellow"> Yellow<br>
    <br><b>On the scale 1 - 3, how favorite is it?</b><br><br>
    <select name="scale" size=1>
    <option>1
    <option selected>2
    <option>3
    </select>
    <br>
    <input type="HIDDEN" name="favorite color" size="32">
    <input type="Submit" value="I'm learning" name="Attentive student">
    <input type="Submit" value="Give me a break!" name="Overachiever">
    <input type="Reset" name="Reset">
    </form>
</div>
-->


